﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoDocumentoRef
    Private _IdTipoDocumento As Integer
    Private _IdTipoDocumentoRef As Integer

    Public Sub New()
    End Sub

    Public Sub New(ByVal Id_TipoDoc As Integer, ByVal Id_TipoDocRef As Integer)
        Me.IdTipoDoc = Id_TipoDoc
        Me.IdTipoDocRef = Id_TipoDocRef
    End Sub

    Public Property IdTipoDoc() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property
    Public Property IdTipoDocRef() As Integer
        Get
            Return Me._IdTipoDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumentoRef = value
        End Set
    End Property
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** JUEVES 25 FEB 2010 HORA 05_59 PM
'**************** VIERNES 19 FEB 2010 HORA 02_58 PM


Public Class Anexo_MovBanco

    Protected _IdMovBanco As Integer
    Protected _IdCuentaBancaria As Integer
    Protected _IdAnexo_MovBanco As Integer
    Protected _IdDocumentoRef As Integer
    Protected _IdDocumentoRef2 As Integer
    Protected _IdBanco As Integer

    Protected _NroDocumento As String
    Protected _NomTipoDocumento As String
    Protected _NroDocumento2 As String
    Protected _NomTipoDocumento2 As String

    Public Sub New()
    End Sub

    Public Sub New(ByVal IdMovBanco As Integer, ByVal IdCuentaBancaria As Integer, ByVal IdAnexoMovBanco As Integer, ByVal IdDocumentoRef As Integer, ByVal IdBanco As Integer)
        Me.IdMovBanco = IdMovBanco
        Me.IdCuentaBancaria = IdCuentaBancaria
        Me.IdAnexo_MovBanco = IdAnexoMovBanco
        Me.IdDocumentoRef = IdDocumentoRef
        Me.IdBanco = IdBanco
    End Sub

    Public Sub New(ByVal IdMovBanco As Integer, ByVal IdCuentaBancaria As Integer, ByVal IdAnexoMovBanco As Integer, ByVal IdDocumentoRef As Integer, ByVal IdDocumentoRef2 As Integer, ByVal IdBanco As Integer)
        Me.IdMovBanco = IdMovBanco
        Me.IdCuentaBancaria = IdCuentaBancaria
        Me.IdAnexo_MovBanco = IdAnexoMovBanco
        Me.IdDocumentoRef = IdDocumentoRef
        Me.IdBanco = IdBanco
        Me.IdDocumentoRef2 = IdDocumentoRef2
    End Sub

    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property

    Public Property IdMovBanco() As Integer
        Get
            Return Me._IdMovBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdMovBanco = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property

    Public Property IdAnexo_MovBanco() As Integer
        Get
            Return Me._IdAnexo_MovBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdAnexo_MovBanco = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property

    Public Property IdDocumentoRef2() As Integer
        Get
            Return Me._IdDocumentoRef2
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef2 = value
        End Set
    End Property

    Public Property NroDocumento() As String
        Get
            Return Me._NroDocumento
        End Get
        Set(ByVal value As String)
            Me._NroDocumento = value
        End Set
    End Property

    Public Property NomTipoDocumento() As String
        Get
            Return _NomTipoDocumento
        End Get
        Set(ByVal value As String)
            Me._NomTipoDocumento = value
        End Set
    End Property

    Public Property NroDocumento2() As String
        Get
            Return Me._NroDocumento2
        End Get
        Set(ByVal value As String)
            Me._NroDocumento2 = value
        End Set
    End Property

    Public Property NomTipoDocumento2() As String
        Get
            Return _NomTipoDocumento2
        End Get
        Set(ByVal value As String)
            Me._NomTipoDocumento2 = value
        End Set
    End Property

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class EstiloLinea
    Inherits Estilo
    Private _IdEstilo As Integer
    Private _NomEstilo As String
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _elEstado As Boolean
    Private _elOrden As Integer
    Private _IdEstiloOrden As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idestilo As Integer, ByVal nomestilo As String, ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal estado As Boolean, ByVal orden As Integer)
        _IdEstilo = idestilo
        _NomEstilo = nomestilo
        _IdLinea = idlinea
        _IdTipoExistencia = idtipoexistencia
        _elEstado = estado
        _elOrden = orden
    End Sub
    Public Sub New(ByVal idestilo As Integer, ByVal nomestilo As String, ByVal estado As Boolean)
        _IdEstilo = idestilo
        _NomEstilo = nomestilo
        _elEstado = estado
    End Sub
    Public Property IdEstilo() As Integer
        Get
            Return _IdEstilo
        End Get
        Set(ByVal value As Integer)
            _IdEstilo = value
        End Set
    End Property
    Public Property NomEstilo() As String
        Get
            Return _NomEstilo
        End Get
        Set(ByVal value As String)
            _NomEstilo = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _elEstado
        End Get
        Set(ByVal value As Boolean)
            _elEstado = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _elOrden
        End Get
        Set(ByVal value As Integer)
            _elOrden = value
        End Set
    End Property
    Public Property IdEstiloOrden() As String
        Get
            Return _IdEstiloOrden
        End Get
        Set(ByVal value As String)
            _IdEstiloOrden = value
        End Set
    End Property
End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

''03082010

Public Class CuentaBancaria
    Private _IdBanco As Integer
    Private _IdPersona As Integer
    Private _IdCuentaBancaria As Integer
    Private _cb_numero As String
    Private _cb_CuentaContable As String
    Private _IdMoneda As Integer
    Private _cb_Estado As String
    Private _Empresa As String
    Private _monSimbolo As String
    Private _banco As String
    Private _SaldoContable As Decimal
    Private _Saldodisponible As Decimal
    Private _Edita As Int16
    Private _cb_CuentaInterbanca As String
    Private _swiftbanca As String
    Private _esCuentaDetraccion As String
    Public Property NroCuentaInterbanca() As String
        Get
            Return _cb_CuentaInterbanca
        End Get
        Set(ByVal value As String)
            _cb_CuentaInterbanca = value
        End Set
    End Property
    Public Property SwiftBancaria() As String
        Get
            Return _swiftbanca
        End Get
        Set(ByVal value As String)
            _swiftbanca = value
        End Set
    End Property
    Public Property Edita() As Decimal
        Get
            Return _Edita
        End Get
        Set(ByVal value As Decimal)
            _Edita = value
        End Set
    End Property
    Public Property Saldocontable() As Decimal
        Get
            Return _SaldoContable
        End Get
        Set(ByVal value As Decimal)
            _SaldoContable = value
        End Set
    End Property
    Public Property SaldoDisponible() As Decimal
        Get
            Return _Saldodisponible
        End Get
        Set(ByVal value As Decimal)
            _Saldodisponible = value
        End Set
    End Property

    Public Property Banco() As String
        Get
            Return _banco
        End Get
        Set(ByVal value As String)
            _banco = value
        End Set
    End Property
    Public Property MonSimbolo() As String
        Get
            Return _monSimbolo
        End Get
        Set(ByVal value As String)
            _monSimbolo = value
        End Set
    End Property
    Public Property Empresa() As String
        Get
            Return _Empresa
        End Get
        Set(ByVal value As String)
            _Empresa = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._cb_Estado
        End Get
        Set(ByVal value As String)
            Me._cb_Estado = value
        End Set
    End Property


    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property

    Public Property NroCuentaBancaria() As String
        Get
            Return Me._cb_numero
        End Get
        Set(ByVal value As String)
            Me._cb_numero = value
        End Set
    End Property

    Public Property NroCuentaContable() As String
        Get
            Return Me._cb_CuentaContable
        End Get
        Set(ByVal value As String)
            Me._cb_CuentaContable = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property esCuentaDetraccion() As String
        Get
            Return _esCuentaDetraccion
        End Get
        Set(ByVal value As String)
            _esCuentaDetraccion = value
        End Set
    End Property
End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class AreaEmpresa
    Private _IdEmpresa As Integer
    Private _IdArea As Integer
    Private _ar_NombreCorto As String
    Private _Id As Integer
    Private _EstadoAE As Boolean
    Public Sub New()
    End Sub
    Public Sub New(ByVal Id As Integer, ByVal nombre As String, ByVal idempresa As Integer, ByVal estadoae As Boolean)
        Me._Id = Id
        Me._ar_NombreCorto = nombre
        Me._IdEmpresa = idempresa
        Me._EstadoAE = estadoae
    End Sub
    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdArea() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._Id
        End Get
        Set(ByVal value As Integer)
            Me._Id = value
        End Set
    End Property
    Public Property DescripcionCorta() As String
        Get
            Return Me._ar_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._ar_NombreCorto = value
        End Set
    End Property
    Public Property EstadoAE() As Boolean
        Get
            Return _EstadoAE
        End Get
        Set(ByVal value As Boolean)
            _EstadoAE = value
        End Set
    End Property
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoTarjeta
    Implements ICloneable

    Private _IdTipoTarjeta As Integer
    Private _ttarjeta_Nombre As String
    Private _ttarjeta_Estado As Nullable(Of Boolean)

    Public Sub New()
        Me._IdTipoTarjeta = -1
        Me._ttarjeta_Nombre = ""
        Me._ttarjeta_Estado = Nothing
    End Sub

    Public Sub New(ByVal Id As Integer, ByVal Nombre As String, ByVal Estado As Boolean)
        Me._IdTipoTarjeta = Id
        Me._ttarjeta_Nombre = Nombre
        Me._ttarjeta_Estado = Estado
    End Sub

    Public Property Id() As Integer
        Get
            Return Me._IdTipoTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoTarjeta = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._ttarjeta_Nombre
        End Get
        Set(ByVal value As String)
            Me._ttarjeta_Nombre = value
        End Set
    End Property
    Public Property Estado() As Boolean?
        Get
            Return Me._ttarjeta_Estado
        End Get
        Set(ByVal value As Boolean?)
            Me._ttarjeta_Estado = value
        End Set
    End Property

    Public ReadOnly Property DescEstado() As String
        Get
            If Me._ttarjeta_Estado Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public Function Clone() As Object Implements ICloneable.Clone
        Dim x As New TipoTarjeta
        x.Id = Me.Id
        x.Nombre = Me.Nombre
        x.Estado = Me.Estado
        Return x
    End Function

    Public Function getClone() As TipoTarjeta
        Return CType(Me.Clone, TipoTarjeta)
    End Function

End Class

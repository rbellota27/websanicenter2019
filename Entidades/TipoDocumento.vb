'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'Modificado 05 ABR 2010 4:19 PM

Public Class TipoDocumento
    Private _IdTipoDocumento As Integer
    Private _tdoc_NombreLargo As String
    Private _tdoc_NombreCorto As String
    Private _tdoc_CodigoSunat As String
    Private _tdoc_ExportarConta As String
    Private _tdoc_Estado As String
    Private _descEstado As String
    Private _descExpConta As String

    Private _tdoc_Externo As Boolean
    Private _tdoc_Cancelable As Boolean

    Public Sub New()
    End Sub

    Public Sub New(ByVal id As Integer, ByVal descripcion As String)
        Me.Id = id
        Me.DescripcionCorto = descripcion
    End Sub

    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._tdoc_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public ReadOnly Property DescExportConta() As String
        Get
            Me._descExpConta = "S�"
            If Me._tdoc_ExportarConta = "0" Then
                Me._descExpConta = "---"
            End If
            Return Me._descExpConta
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._tdoc_NombreLargo
        End Get
        Set(ByVal value As String)
            Me._tdoc_NombreLargo = value
        End Set
    End Property

    Public Property DescripcionCorto() As String
        Get
            Return Me._tdoc_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._tdoc_NombreCorto = value
        End Set
    End Property

    Public Property CodigoSunat() As String
        Get
            Return Me._tdoc_CodigoSunat
        End Get
        Set(ByVal value As String)
            Me._tdoc_CodigoSunat = value
        End Set
    End Property

    Public Property ExportarConta() As String
        Get
            Return Me._tdoc_ExportarConta
        End Get
        Set(ByVal value As String)
            Me._tdoc_ExportarConta = value
        End Set
    End Property
    Public Property ExportarContaBool() As Boolean
        Get
            Return IIf(Me._tdoc_ExportarConta = "1", True, False)
        End Get
        Set(ByVal value As Boolean)
            Me._tdoc_ExportarConta = IIf(value, "1", "0")
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return Me._tdoc_Estado
        End Get
        Set(ByVal value As String)
            Me._tdoc_Estado = value
        End Set
    End Property

    Public Property EstadoBool() As Boolean
        Get
            Return IIf(Me._tdoc_Estado = "1", True, False)
        End Get
        Set(ByVal value As Boolean)
            Me._tdoc_Estado = IIf(value, "1", "0")
        End Set
    End Property

    Public Property Externo() As Boolean
        Get
            Return Me._tdoc_Externo
        End Get
        Set(ByVal value As Boolean)
            Me._tdoc_Externo = value
        End Set
    End Property

    Public Property Cancelable() As Boolean
        Get
            Return Me._tdoc_Cancelable
        End Get
        Set(ByVal value As Boolean)
            Me._tdoc_Cancelable = value
        End Set
    End Property
End Class

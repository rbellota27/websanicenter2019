﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'******************     SABADO 03 JULIO 2010



Public Class Botella
    Private _IdBotella As Integer
    Private _bot_NroBotella As String
    Private _bot_Capacidad As Decimal
    Private _IdUnidadMedida_Capacidad As Integer
    Private _IdContenido As Integer
    Private _IdPropietario As Integer
    Private _IdUbicacion As Integer
    Private _bot_Estado As Boolean
    Private _IdUsuarioInsert As Integer
    Private _bot_FechaInsert As Date
    Private _IdUsuarioUpdate As Integer
    Private _bot_FechaUpdate As Date
    Private _Contenido As String
    Private _ListaMagnitudUnidad As List(Of Entidades.MagnitudUnidad)
    Private _Observacion As String
    Private _IdTienda As Integer

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property Observacion() As String
        Get
            Return Me._Observacion
        End Get
        Set(ByVal value As String)
            Me._Observacion = value
        End Set
    End Property

    Public Property Contenido() As String
        Get
            Return Me._Contenido
        End Get
        Set(ByVal value As String)
            Me._Contenido = value
        End Set
    End Property

    Public Property ListaMagnitudUnidad() As List(Of Entidades.MagnitudUnidad)
        Get
            Return Me._ListaMagnitudUnidad
        End Get
        Set(ByVal value As List(Of Entidades.MagnitudUnidad))
            Me._ListaMagnitudUnidad = value
        End Set
    End Property


    Public Property IdBotella() As Integer
        Get
            Return Me._IdBotella
        End Get
        Set(ByVal value As Integer)
            Me._IdBotella = value
        End Set
    End Property

    Public Property NroBotella() As String
        Get
            Return Me._bot_NroBotella
        End Get
        Set(ByVal value As String)
            Me._bot_NroBotella = value
        End Set
    End Property

    Public Property Capacidad() As Decimal
        Get
            Return Me._bot_Capacidad
        End Get
        Set(ByVal value As Decimal)
            Me._bot_Capacidad = value
        End Set
    End Property

    Public Property IdUnidadMedida_Capacidad() As Integer
        Get
            Return Me._IdUnidadMedida_Capacidad
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida_Capacidad = value
        End Set
    End Property

    Public Property IdContenido() As Integer
        Get
            Return Me._IdContenido
        End Get
        Set(ByVal value As Integer)
            Me._IdContenido = value
        End Set
    End Property

    Public Property IdPropietario() As Integer
        Get
            Return Me._IdPropietario
        End Get
        Set(ByVal value As Integer)
            Me._IdPropietario = value
        End Set
    End Property

    Public Property IdUbicacion() As Integer
        Get
            Return Me._IdUbicacion
        End Get
        Set(ByVal value As Integer)
            Me._IdUbicacion = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._bot_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._bot_Estado = value
        End Set
    End Property

    Public Property IdUsuarioInsert() As Integer
        Get
            Return Me._IdUsuarioInsert
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioInsert = value
        End Set
    End Property

    Public Property FechaInsert() As Date
        Get
            Return Me._bot_FechaInsert
        End Get
        Set(ByVal value As Date)
            Me._bot_FechaInsert = value
        End Set
    End Property

    Public Property IdUsuarioUpdate() As Integer
        Get
            Return Me._IdUsuarioUpdate
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioUpdate = value
        End Set
    End Property

    Public Property FechaUpdate() As Date
        Get
            Return Me._bot_FechaUpdate
        End Get
        Set(ByVal value As Date)
            Me._bot_FechaUpdate = value
        End Set
    End Property
End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

' 11/02/2011

Public Class Producto
    Private _IdProducto As Integer
    Private _prod_CodigoBarras As String
    Private _IdContenido As Integer
    Private _prod_Nombre As String
    Private _prod_FechaAlta As Date
    Private _prod_StockMin As Integer
    Private _prod_StockMax As Integer
    Private _prod_StockActual As Decimal
    Private _prod_StockReposicion As Integer
    Private _IdSubLInea As Integer
    Private _IdProveedor As Integer
    Private _IdPropietario As Integer
    Private _IdMarca As Integer
    Private _IdMaterial As Integer
    Private _IdFabricante As Integer
    Private _IdSabor As Integer
    Private _IdColor As Integer
    Private _IdTamanio As Integer
    Private _IdTalla As Integer
    Private _IdEstadoProd As Integer
    Private _IdPerUbicacion As Integer
    Private _IdMoraEnvase As Integer
    Private _IdUsuario As Integer
    Private _DescEstado As String
    Private _IdFormaProducto As Integer
    Private _IdTipoProducto As Integer
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _prod_PrecioCompra As Decimal
    Private _prod_Orden As Integer
    Private _IdMoneda As Integer
    Private _IdPais As String
    Private _IdProcedencia As String
    Private _IdModelo As String
    Private _IdEstilo As String
    Private _IdTransito As String
    Private _IdCalidad As String
    Private _CodAntiguo As String
    Private _CodProveedor As String
    Private _IdSubSubLinea As Integer
    Private _NoVisible As String
    Private _Codigo As String
    Private _prod_Atributos As String
    Private _Kit As Boolean
    Private _prod_CalculoFlete As String
    Private _LongitudProd As Integer
    Private _prod_SinUso As String
    Private _prod_CodbarraFabricante As String
    Private _prod_AfectoPercepcion As Integer
    Private _prod_Longitud_AutoGen As Integer
    Private _flagTG As Integer
    Private _LeadTime As Decimal
    Private _VolMinCompra As Decimal
    Private _ReposicionWeeks As Decimal


    Public Property ReposicionWeeks() As Decimal
        Get
            Return _ReposicionWeeks
        End Get
        Set(ByVal value As Decimal)
            Me._ReposicionWeeks = value
        End Set
    End Property

    Public Property LeadTime() As Decimal
        Get
            Return Me._LeadTime
        End Get
        Set(ByVal value As Decimal)
            Me._LeadTime = value
        End Set
    End Property

    Public Property VolMinCompra() As Decimal
        Get
            Return Me._VolMinCompra
        End Get
        Set(ByVal value As Decimal)
            Me._VolMinCompra = value
        End Set
    End Property

    Public Property prod_Longitud_AutoGen() As Integer
        Get
            Return _prod_Longitud_AutoGen
        End Get
        Set(ByVal value As Integer)
            _prod_Longitud_AutoGen = value
        End Set
    End Property

    Public Property flagTG() As Integer
        Get
            Return _flagTG
        End Get
        Set(ByVal value As Integer)
            _flagTG = value
        End Set
    End Property

    Public Property prod_AfectoPercepcion() As Integer
        Get
            Return _prod_AfectoPercepcion
        End Get
        Set(ByVal value As Integer)
            _prod_AfectoPercepcion = value
        End Set
    End Property

    Public Property CodBarrasFabricante() As String
        Get
            Return _prod_CodbarraFabricante
        End Get
        Set(ByVal value As String)
            _prod_CodbarraFabricante = value
        End Set
    End Property
    Public Property CalculoFlete() As String
        Get
            Return Me._prod_CalculoFlete
        End Get
        Set(ByVal value As String)
            Me._prod_CalculoFlete = value
        End Set
    End Property



    Public Sub New()
    End Sub

    Public Property Kit() As Boolean
        Get
            Return Me._Kit
        End Get
        Set(ByVal value As Boolean)
            Me._Kit = value
        End Set
    End Property


    Public Sub New(ByVal idproducto As Integer, ByVal nombre As String)
        Me._IdProducto = idproducto
        Me._prod_Nombre = nombre
    End Sub
    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return Me._prod_Orden
        End Get
        Set(ByVal value As Integer)
            Me._prod_Orden = value
        End Set
    End Property



    Public Property PrecioCompra() As Decimal
        Get
            Return Me._prod_PrecioCompra
        End Get
        Set(ByVal value As Decimal)
            Me._prod_PrecioCompra = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return Me._IdLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdLinea = value
        End Set
    End Property

    Public Property IdTipoExistencia() As Integer
        Get
            Return Me._IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoExistencia = value
        End Set
    End Property
    Public Property IdFormaProducto() As Integer
        Get
            Return Me._IdFormaProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdFormaProducto = value
        End Set
    End Property

    Public Property IdTipoProducto() As Integer
        Get
            Return Me._IdTipoProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoProducto = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property CodigoBarras() As String
        Get
            Return Me._prod_CodigoBarras
        End Get
        Set(ByVal value As String)
            Me._prod_CodigoBarras = value
        End Set
    End Property

    Public Property IdContenido() As Integer
        Get
            Return Me._IdContenido
        End Get
        Set(ByVal value As Integer)
            Me._IdContenido = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._prod_Nombre
        End Get
        Set(ByVal value As String)
            Me._prod_Nombre = value
        End Set
    End Property

    Public Property FechaAlta() As Date
        Get
            Return Me._prod_FechaAlta
        End Get
        Set(ByVal value As Date)
            Me._prod_FechaAlta = value
        End Set
    End Property

    Public Property StockMinimo() As Integer
        Get
            Return Me._prod_StockMin
        End Get
        Set(ByVal value As Integer)
            Me._prod_StockMin = value
        End Set
    End Property

    Public Property StockMaximo() As Integer
        Get
            Return Me._prod_StockMax
        End Get
        Set(ByVal value As Integer)
            Me._prod_StockMax = value
        End Set
    End Property

    Public Property StockActual() As Decimal
        Get
            Return Me._prod_StockActual
        End Get
        Set(ByVal value As Decimal)
            Me._prod_StockActual = value
        End Set
    End Property

    Public Property IdSubLInea() As Integer
        Get
            Return Me._IdSubLInea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLInea = value
        End Set
    End Property

    Public Property IdProveedor() As Integer
        Get
            Return Me._IdProveedor
        End Get
        Set(ByVal value As Integer)
            Me._IdProveedor = value
        End Set
    End Property

    Public Property IdPropietario() As Integer
        Get
            Return Me._IdPropietario
        End Get
        Set(ByVal value As Integer)
            Me._IdPropietario = value
        End Set
    End Property

    Public Property IdMarca() As Integer
        Get
            Return Me._IdMarca
        End Get
        Set(ByVal value As Integer)
            Me._IdMarca = value
        End Set
    End Property

    Public Property IdMaterial() As Integer
        Get
            Return Me._IdMaterial
        End Get
        Set(ByVal value As Integer)
            Me._IdMaterial = value
        End Set
    End Property

    Public Property IdFabricante() As Integer
        Get
            Return Me._IdFabricante
        End Get
        Set(ByVal value As Integer)
            Me._IdFabricante = value
        End Set
    End Property

    Public Property IdSabor() As Integer
        Get
            Return Me._IdSabor
        End Get
        Set(ByVal value As Integer)
            Me._IdSabor = value
        End Set
    End Property

    Public Property IdColor() As Integer
        Get
            Return Me._IdColor
        End Get
        Set(ByVal value As Integer)
            Me._IdColor = value
        End Set
    End Property

    Public Property IdTamanio() As Integer
        Get
            Return Me._IdTamanio
        End Get
        Set(ByVal value As Integer)
            Me._IdTamanio = value
        End Set
    End Property

    Public Property IdTalla() As Integer
        Get
            Return Me._IdTalla
        End Get
        Set(ByVal value As Integer)
            Me._IdTalla = value
        End Set
    End Property

    Public Property IdEstadoProd() As Integer
        Get
            Return Me._IdEstadoProd
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoProd = value
        End Set
    End Property

    Public Property IdPerUbicacion() As Integer
        Get
            Return Me._IdPerUbicacion
        End Get
        Set(ByVal value As Integer)
            Me._IdPerUbicacion = value
        End Set
    End Property

    Public Property IdMoraEnvase() As Integer
        Get
            Return Me._IdMoraEnvase
        End Get
        Set(ByVal value As Integer)
            Me._IdMoraEnvase = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            Me._DescEstado = "Activo"
            If Me.IdEstadoProd = 2 Then
                Me._DescEstado = "Inactivo"
            End If
            Return Me._DescEstado
        End Get
    End Property
    Public Property IdPais() As String
        Get
            Return Me._IdPais
        End Get
        Set(ByVal value As String)
            Me._IdPais = value
        End Set
    End Property
    Public Property IdProcedencia() As String
        Get
            Return Me._IdProcedencia
        End Get
        Set(ByVal value As String)
            Me._IdProcedencia = value
        End Set
    End Property

    Public Property StockReposicion() As Integer
        Get
            Return Me._prod_StockReposicion
        End Get
        Set(ByVal value As Integer)
            Me._prod_StockReposicion = value
        End Set
    End Property
    Public Property IdModelo() As String
        Get
            Return _IdModelo
        End Get
        Set(ByVal value As String)
            _IdModelo = value
        End Set
    End Property
    Public Property IdEstilo() As String
        Get
            Return _IdEstilo
        End Get
        Set(ByVal value As String)
            _IdEstilo = value
        End Set
    End Property
    Public Property IdTransito() As String
        Get
            Return _IdTransito
        End Get
        Set(ByVal value As String)
            _IdTransito = value
        End Set
    End Property
    Public Property IdCalidad() As String
        Get
            Return _IdCalidad
        End Get
        Set(ByVal value As String)
            _IdCalidad = value
        End Set
    End Property
    Public Property CodAntiguo() As String
        Get
            Return _CodAntiguo
        End Get
        Set(ByVal value As String)
            _CodAntiguo = value
        End Set
    End Property
    Public Property CodProveedor() As String
        Get
            Return _CodProveedor
        End Get
        Set(ByVal value As String)
            _CodProveedor = value
        End Set
    End Property

    Private _IdProductov2 As String
    Private _Nombrev2 As String
    Public Property IdProductov2() As String
        Get
            Return _IdProductov2
        End Get
        Set(ByVal value As String)
            _IdProductov2 = value
        End Set
    End Property
    Public Property Nombrev2() As String
        Get
            Return _Nombrev2
        End Get
        Set(ByVal value As String)
            _Nombrev2 = value
        End Set
    End Property
    Public Property IdSubSubLinea() As Integer
        Get
            Return _IdSubSubLinea
        End Get
        Set(ByVal value As Integer)
            _IdSubSubLinea = value
        End Set
    End Property
    Public Property NoVisible() As String
        Get
            Return _NoVisible
        End Get
        Set(ByVal value As String)
            _NoVisible = value
        End Set
    End Property
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property
    Public Property CodAtributos() As String
        Get
            Return _prod_Atributos
        End Get
        Set(ByVal value As String)
            _prod_Atributos = value
        End Set
    End Property

    Public Property LongitudProd() As Integer
        Get
            Return _LongitudProd
        End Get
        Set(ByVal value As Integer)
            _LongitudProd = value
        End Set
    End Property

    Public Property prod_SinUso() As String
        Get
            Return _prod_SinUso
        End Get
        Set(ByVal value As String)
            _prod_SinUso = value
        End Set
    End Property

End Class


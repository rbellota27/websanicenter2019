﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TransitoLinea
    Inherits Transito
    Private _IdTransito As Integer
    Private _NomTransito As String
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _tlEstado As Boolean
    Private _tlOrden As Integer
    Private _IdTransitoOrden As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idtransito As Integer, ByVal nomtransito As String, ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal estado As Boolean, ByVal orden As Integer)
        _IdTransito = idtransito
        _NomTransito = nomtransito
        _IdLinea = idlinea
        _IdTipoExistencia = idtipoexistencia
        _tlEstado = estado
        _tlOrden = orden
    End Sub
    Public Sub New(ByVal idtransito As Integer, ByVal nomtransito As String, ByVal estado As Boolean)
        _IdTransito = idtransito
        _NomTransito = nomtransito
        _tlEstado = estado
    End Sub
    Public Property IdTransito() As Integer
        Get
            Return _IdTransito
        End Get
        Set(ByVal value As Integer)
            _IdTransito = value
        End Set
    End Property
    Public Property IdTransitoOrden() As String
        Get
            Return _IdTransitoOrden
        End Get
        Set(ByVal value As String)
            _IdTransitoOrden = value
        End Set
    End Property
    Public Property NomTransito() As String
        Get
            Return _NomTransito
        End Get
        Set(ByVal value As String)
            _NomTransito = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _tlEstado
        End Get
        Set(ByVal value As Boolean)
            _tlEstado = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _tlOrden
        End Get
        Set(ByVal value As Integer)
            _tlOrden = value
        End Set
    End Property
End Class

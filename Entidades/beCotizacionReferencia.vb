﻿Public Class beCotizacionReferencia

	Private _Documento As New Entidades.Documento
	Private _Observacion As New Entidades.Observacion
	Private _PuntoPartida As New Entidades.PuntoPartida
	Private _PuntoLlegada As New Entidades.PuntoLlegada
	Private _Persona As Entidades.PersonaView
	Private _Lista_CuentaPersona As List(Of Entidades.CuentaPersona)
	Private _ListaProvinciaPP As List(Of Entidades.beCboProvincia)
	Private _ListaDistritoPP As List(Of Entidades.beCboDistrito)
	Private _ListaProvinciaPL As List(Of Entidades.beCboProvincia)
	Private _ListaDistritoPL As List(Of Entidades.beCboDistrito)
	Private _ListaProvinciaPPD As List(Of Entidades.beCboProvincia)
	Private _ListaDistritoPPD As List(Of Entidades.beCboDistrito)
	Private _ListaProvinciaPLD As List(Of Entidades.beCboProvincia)
	Private _ListaDistritoPLD As List(Of Entidades.beCboDistrito)
	Private _ListaDetalleDocumento As List(Of Entidades.DetalleDocumento)
	Private _ListaMedioPago As List(Of Entidades.beCampoEntero)

	Public Property Documento() As Entidades.Documento
		Get
			Return _Documento
		End Get
		Set(ByVal value As Entidades.Documento)
			_Documento = value
		End Set
	End Property

	Public Property Observacion() As Entidades.Observacion
		Get
			Return _Observacion
		End Get
		Set(ByVal value As Entidades.Observacion)
			_Observacion = value
		End Set
	End Property

	Public Property PuntoPartida() As Entidades.PuntoPartida
		Get
			Return _PuntoPartida
		End Get
		Set(ByVal value As Entidades.PuntoPartida)
			_PuntoPartida = value
		End Set
	End Property

	Public Property PuntoLlegada() As Entidades.PuntoLlegada
		Get
			Return _PuntoLlegada
		End Get
		Set(ByVal value As Entidades.PuntoLlegada)
			_PuntoLlegada = value
		End Set
	End Property

	Public Property Persona() As Entidades.PersonaView
		Get
			Return _Persona
		End Get
		Set(ByVal value As Entidades.PersonaView)
			_Persona = value
		End Set
	End Property

	Public Property Lista_CuentaPersona() As List(Of Entidades.CuentaPersona)
		Get
			Return _Lista_CuentaPersona
		End Get
		Set(ByVal value As List(Of Entidades.CuentaPersona))
			_Lista_CuentaPersona = value
		End Set
	End Property

	Public Property ListaProvinciaPP() As List(Of Entidades.beCboProvincia)
		Get
			Return _ListaProvinciaPP
		End Get
		Set(ByVal value As List(Of Entidades.beCboProvincia))
			_ListaProvinciaPP = value
		End Set
	End Property

	Public Property ListaDistritoPP() As List(Of Entidades.beCboDistrito)
		Get
			Return _ListaDistritoPP
		End Get
		Set(ByVal value As List(Of Entidades.beCboDistrito))
			_ListaDistritoPP = value
		End Set
	End Property

	Public Property ListaProvinciaPL() As List(Of Entidades.beCboProvincia)
		Get
			Return _ListaProvinciaPL
		End Get
		Set(ByVal value As List(Of Entidades.beCboProvincia))
			_ListaProvinciaPL = value
		End Set
	End Property

	Public Property ListaDistritoPL() As List(Of Entidades.beCboDistrito)
		Get
			Return _ListaDistritoPL
		End Get
		Set(ByVal value As List(Of Entidades.beCboDistrito))
			_ListaDistritoPL = value
		End Set
	End Property

	Public Property ListaProvinciaPPD() As List(Of Entidades.beCboProvincia)
		Get
			Return _ListaProvinciaPP
		End Get
		Set(ByVal value As List(Of Entidades.beCboProvincia))
			_ListaProvinciaPP = value
		End Set
	End Property

	Public Property ListaDistritoPPD() As List(Of Entidades.beCboDistrito)
		Get
			Return _ListaDistritoPP
		End Get
		Set(ByVal value As List(Of Entidades.beCboDistrito))
			_ListaDistritoPP = value
		End Set
	End Property

	Public Property ListaProvinciaPLD() As List(Of Entidades.beCboProvincia)
		Get
			Return _ListaProvinciaPL
		End Get
		Set(ByVal value As List(Of Entidades.beCboProvincia))
			_ListaProvinciaPL = value
		End Set
	End Property

	Public Property ListaDistritoPLD() As List(Of Entidades.beCboDistrito)
		Get
			Return _ListaDistritoPL
		End Get
		Set(ByVal value As List(Of Entidades.beCboDistrito))
			_ListaDistritoPL = value
		End Set
	End Property

	Public Property ListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
		Get
			Return _ListaDetalleDocumento
		End Get
		Set(ByVal value As List(Of Entidades.DetalleDocumento))
			_ListaDetalleDocumento = value
		End Set
	End Property

	Public Property ListaMedioPago() As List(Of Entidades.beCampoEntero)
		Get
			Return _ListaMedioPago
		End Get
		Set(ByVal value As List(Of Entidades.beCampoEntero))
			_ListaMedioPago = value
		End Set
	End Property


End Class

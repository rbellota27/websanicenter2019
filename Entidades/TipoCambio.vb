'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class TipoCambio
    Private _IdTipoCambio As Integer
    Private _tc_CompraOf As Decimal
    Private _tc_CompraC As Decimal
    Private _tc_Fecha As String
    Private _IdMoneda As Integer
    Private _tc_VentaOf As Decimal
    Private _tc_VentaC As Decimal
    Private _IdUsuario As Integer

    Public Property Id() As Integer
        Get
            Return Me._IdTipoCambio
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoCambio = value
        End Set
    End Property

    Public Property CompraOf() As Decimal
        Get
            Return Me._tc_CompraOf
        End Get
        Set(ByVal value As Decimal)
            Me._tc_CompraOf = value
        End Set
    End Property

    Public Property CompraC() As Decimal
        Get
            Return Me._tc_CompraC
        End Get
        Set(ByVal value As Decimal)
            Me._tc_CompraC = value
        End Set
    End Property

    Public Property Fecha() As String
        Get
            Return Me._tc_Fecha
        End Get
        Set(ByVal value As String)
            Me._tc_Fecha = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property VentaOf() As Decimal
        Get
            Return Me._tc_VentaOf
        End Get
        Set(ByVal value As Decimal)
            Me._tc_VentaOf = value
        End Set
    End Property

    Public Property VentaC() As Decimal
        Get
            Return Me._tc_VentaC
        End Get
        Set(ByVal value As Decimal)
            Me._tc_VentaC = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property
End Class

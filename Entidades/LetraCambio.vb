'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.




'******************* LUNES 08 MARZO 2010 HORA 01_22 PM








Public Class LetraCambio
    Private _IdDocumento As Integer
    Private _lc_Ubigeo As String
    Private _IdAval As Integer
    Private _IdBanco As Integer
    Private _IdCuentaBancaria As Integer
    Private _IdOficina As Integer

    Private _lc_DebitoCuenta As Boolean
    Private _lc_Renegociado As Boolean
    Private _lc_Protestado As Boolean
    Private _lc_EnCartera As Boolean
    Private _lc_EnCobranza As Boolean
    Private _NroOperacion As String

    Public Property lc_EnCartera() As Boolean
        Get
            Return Me._lc_EnCartera
        End Get
        Set(ByVal value As Boolean)
            Me._lc_EnCartera = value
        End Set
    End Property

    Public Property lc_EnCobranza() As Boolean
        Get
            Return Me._lc_EnCobranza
        End Get
        Set(ByVal value As Boolean)
            Me._lc_EnCobranza = value
        End Set
    End Property

    Public Property DebitoCuenta() As Boolean
        Get
            Return Me._lc_DebitoCuenta
        End Get
        Set(ByVal value As Boolean)
            Me._lc_DebitoCuenta = value
        End Set
    End Property

    Public Property Renegociado() As Boolean
        Get
            Return Me._lc_Renegociado
        End Get
        Set(ByVal value As Boolean)
            Me._lc_Renegociado = value
        End Set
    End Property

    Public Property Protestado() As Boolean
        Get
            Return Me._lc_Protestado
        End Get
        Set(ByVal value As Boolean)
            Me._lc_Protestado = value
        End Set
    End Property




    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property Ubigeo() As String
        Get
            Return Me._lc_Ubigeo
        End Get
        Set(ByVal value As String)
            Me._lc_Ubigeo = value
        End Set
    End Property

    Public Property IdAval() As Integer
        Get
            Return Me._IdAval
        End Get
        Set(ByVal value As Integer)
            Me._IdAval = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property

    Public Property IdOficina() As Integer
        Get
            Return Me._IdOficina
        End Get
        Set(ByVal value As Integer)
            Me._IdOficina = value
        End Set
    End Property



    Public Property NroOperacion() As String
        Get
            Return Me._NroOperacion
        End Get
        Set(ByVal value As String)
            Me._NroOperacion = value
        End Set
    End Property


End Class

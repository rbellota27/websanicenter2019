﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Public Class Anexo_FacturaTransporte
    Private _IdDocumento As Integer
    Private _IdMoneda As Integer
    Private _IdValorReferencial As Integer
    Private _aft_ValorReferencial_Unit As Decimal
    Private _aft_Cantidad As Decimal
    Private _aft_ValorReferencia_Total As Decimal

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property IdValorReferencial() As Integer
        Get
            Return Me._IdValorReferencial
        End Get
        Set(ByVal value As Integer)
            Me._IdValorReferencial = value
        End Set
    End Property

    Public Property ValorReferencial_Unit() As Decimal
        Get
            Return Me._aft_ValorReferencial_Unit
        End Get
        Set(ByVal value As Decimal)
            Me._aft_ValorReferencial_Unit = value
        End Set
    End Property

    Public Property Cantidad() As Decimal
        Get
            Return Me._aft_Cantidad
        End Get
        Set(ByVal value As Decimal)
            Me._aft_Cantidad = value
        End Set
    End Property

    Public Property ValorReferencia_Total() As Decimal
        Get
            Return Me._aft_ValorReferencia_Total
        End Get
        Set(ByVal value As Decimal)
            Me._aft_ValorReferencia_Total = value
        End Set
    End Property

End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Asignacion
    Private _IdTienda As Integer
    Private _IdProducto As Integer
    Private _IdUnidadMedida As Integer
    Private _IdTipoPv As Integer
    Private _IdAsignacion As Integer
    Private _as_Valor As Decimal
    Private _as_PercentUtil As Decimal
    Private _as_PercentUtilVar As Decimal
    Private _as_Fecha As Date
    Private _as_Utilidad As Decimal
    Private _IdUsuario As Integer

    Public Property PercentUtilVar() As Decimal
        Get
            Return Me._as_PercentUtilVar
        End Get
        Set(ByVal value As Decimal)
            Me._as_PercentUtilVar = value
        End Set
    End Property


    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdTipoPv() As Integer
        Get
            Return Me._IdTipoPv
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPv = value
        End Set
    End Property

    Public Property IdAsignacion() As Integer
        Get
            Return Me._IdAsignacion
        End Get
        Set(ByVal value As Integer)
            Me._IdAsignacion = value
        End Set
    End Property

    Public Property Valor() As Decimal
        Get
            Return Me._as_Valor
        End Get
        Set(ByVal value As Decimal)
            Me._as_Valor = value
        End Set
    End Property

    Public Property PercentUtil() As Decimal
        Get
            Return Me._as_PercentUtil
        End Get
        Set(ByVal value As Decimal)
            Me._as_PercentUtil = value
        End Set
    End Property

    Public Property Fecha() As Date
        Get
            Return Me._as_Fecha
        End Get
        Set(ByVal value As Date)
            Me._as_Fecha = value
        End Set
    End Property

    Public Property Utilidad() As Decimal
        Get
            Return Me._as_Utilidad
        End Get
        Set(ByVal value As Decimal)
            Me._as_Utilidad = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property
End Class

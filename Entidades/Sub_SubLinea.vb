﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Sub_SubLinea
    Public _IdSub_SubLinea As Integer
    Public _Sub_SubLNombre As String
    Public _Sub_SubL_Abv As String
    Public _Sub_SubL_Estado As Boolean
    Public _IdSubLInea As Integer
    Private _sl_Nombre As String
    Public _Sub_SubLCodigo As String
    Public _Id As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal id As Integer, ByVal abv As String, ByVal estado As Boolean)
        _IdSub_SubLinea = id
        _Sub_SubL_Abv = abv
        _Sub_SubL_Estado = estado
    End Sub
    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal value As String)
            _Id = value
        End Set
    End Property
    Public Property IdSub_SubLinea() As Integer
        Get
            Return _IdSub_SubLinea
        End Get
        Set(ByVal value As Integer)
            _IdSub_SubLinea = value
        End Set
    End Property
    Public Property Sub_SubLNombre() As String
        Get
            Return _Sub_SubLNombre
        End Get
        Set(ByVal value As String)
            _Sub_SubLNombre = value
        End Set
    End Property
    Public Property Sub_SubL_Abv() As String
        Get
            Return _Sub_SubL_Abv
        End Get
        Set(ByVal value As String)
            _Sub_SubL_Abv = value
        End Set
    End Property
    Public Property Sub_SubL_Estado() As Boolean
        Get
            Return _Sub_SubL_Estado
        End Get
        Set(ByVal value As Boolean)
            _Sub_SubL_Estado = value
        End Set
    End Property
    Public Property IdSubLinea() As Integer
        Get
            Return _IdSubLInea
        End Get
        Set(ByVal value As Integer)
            _IdSubLInea = value
        End Set
    End Property
    Public Property sl_Nombre()
        Get
            Return _sl_Nombre
        End Get
        Set(ByVal value)
            _sl_Nombre = value
        End Set
    End Property
    Public Property Sub_SubLCodigo() As String
        Get
            Return _Sub_SubLCodigo
        End Get
        Set(ByVal value As String)
            _Sub_SubLCodigo = value
        End Set
    End Property
End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class ProfesionPersona
    Private _IdProfesion As Integer
    Private _profp_Principal As Boolean
    Private _IdPersona As Integer


    Public Property IdProfesion() As Integer
        Get
            Return Me._IdProfesion
        End Get
        Set(ByVal value As Integer)
            Me._IdProfesion = value
        End Set
    End Property

    Public Property Principal() As Boolean
        Get
            Return Me._profp_Principal
        End Get
        Set(ByVal value As Boolean)
            Me._profp_Principal = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Private _objProfesion As Object
    Public Property objProfesion() As Object
        Get
            Return _objProfesion
        End Get
        Set(ByVal value As Object)
            _objProfesion = value
        End Set
    End Property

End Class

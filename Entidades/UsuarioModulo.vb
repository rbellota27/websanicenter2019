﻿Public Class UsuarioModulo
    Private _idusuario As Integer
    Private _usuario As Integer
    Private _modulo As Integer
    Private _usu As String
    Private _mod As String

    Public Sub New()
    End Sub

    Public Property idusuario() As Integer
        Get
            Return Me._idusuario
        End Get
        Set(ByVal value As Integer)
            Me._idusuario = value
        End Set
    End Property

    Public Property usuario() As Integer
        Get
            Return Me._usuario
        End Get
        Set(ByVal value As Integer)
            Me._usuario = value
        End Set
    End Property

    Public Property modulo() As Integer
        Get
            Return Me._modulo
        End Get
        Set(ByVal value As Integer)
            Me._modulo = value
        End Set
    End Property


    Public Property usu() As String
        Get
            Return Me._usu
        End Get
        Set(ByVal value As String)
            Me._usu = value
        End Set
    End Property

    Public Property mod1() As String
        Get
            Return Me._mod
        End Get
        Set(ByVal value As String)
            Me._mod = value
        End Set
    End Property

    
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoTablaValor
    Private _IdTipoTablaValor As Integer
    Private _IdTipoTabla As Integer
    Private _ttv_Codigo As String
    Private _ttv_Nombre As String
    Private _ttv_Abv As String
    Private _ttv_Estado As Boolean
    Private _TipoUso As String
    Private _NomTabla As String

    Public Sub New()
    End Sub
    Public Property IdTipoTablaValor() As Integer
        Get
            Return _IdTipoTablaValor
        End Get
        Set(ByVal value As Integer)
            _IdTipoTablaValor = value
        End Set
    End Property
    Public Property IdTipoTabla() As Integer
        Get
            Return _IdTipoTabla
        End Get
        Set(ByVal value As Integer)
            _IdTipoTabla = value
        End Set
    End Property
    Public Property Codigo() As String
        Get
            Return _ttv_Codigo
        End Get
        Set(ByVal value As String)
            _ttv_Codigo = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _ttv_Nombre
        End Get
        Set(ByVal value As String)
            _ttv_Nombre = value
        End Set
    End Property
    Public Property Abv() As String
        Get
            Return _ttv_Abv
        End Get
        Set(ByVal value As String)
            _ttv_Abv = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _ttv_Estado
        End Get
        Set(ByVal value As Boolean)
            _ttv_Estado = value
        End Set
    End Property
    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "Activo"
            If Estado = False Then Return "Inactivo"
        End Get
    End Property
    Public Property TipoUso() As String
        Get
            Return _TipoUso
        End Get
        Set(ByVal value As String)
            _TipoUso = value
        End Set
    End Property

    Public Property NomTabla() As String
        Get
            Return _NomTabla
        End Get
        Set(ByVal value As String)
            _NomTabla = value
        End Set
    End Property

End Class

﻿Public Class beCotizacionListas

    Private _valorParametrosGenerales As String
    Public Property valorParametrosGenerales() As String
        Get
            Return _valorParametrosGenerales
        End Get
        Set(ByVal value As String)
            _valorParametrosGenerales = value
        End Set
    End Property

    Private _listaEmpresa As List(Of beCampoEntero)
    Public Property listaEmpresa() As List(Of beCampoEntero)
        Get
            Return _listaEmpresa
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaEmpresa = value
        End Set
    End Property

    Private _listaTienda As List(Of beCampoEntero)
    Public Property listaTienda() As List(Of beCampoEntero)
        Get
            Return _listaTienda
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaTienda = value
        End Set
    End Property

    Private _listaSerie As List(Of beCampoEntero)
    Public Property listaSerie() As List(Of beCampoEntero)
        Get
            Return _listaSerie
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaSerie = value
        End Set
    End Property

    Private _listaTipoDocumento As List(Of beCampoEntero2)
    Public Property listaTipoDocumento() As List(Of beCampoEntero2)
        Get
            Return _listaTipoDocumento
        End Get
        Set(ByVal value As List(Of beCampoEntero2))
            _listaTipoDocumento = value
        End Set
    End Property

    Private _listaMoneda As List(Of beCampoEntero)
    Public Property listaMoneda() As List(Of beCampoEntero)
        Get
            Return _listaMoneda
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaMoneda = value
        End Set
    End Property

    Private _listaEstadoDocumento As List(Of beCampoEntero)
    Public Property listaEstadoDocumento() As List(Of beCampoEntero)
        Get
            Return _listaEstadoDocumento
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaEstadoDocumento = value
        End Set
    End Property

    Private _listaTipoOperacion As List(Of beCampoEntero)
    Public Property listaTipoOperacion() As List(Of beCampoEntero)
        Get
            Return _listaTipoOperacion
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaTipoOperacion = value
        End Set
    End Property

    Private _listaMotivoTraslado As List(Of beCampoEntero)
    Public Property listaMotivoTraslado() As List(Of beCampoEntero)
        Get
            Return _listaMotivoTraslado
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaMotivoTraslado = value
        End Set
    End Property

    Private _listaCondicionPago As List(Of beCampoEntero)
    Public Property listaCondicionPago() As List(Of beCampoEntero)
        Get
            Return _listaCondicionPago
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaCondicionPago = value
        End Set
    End Property

    Private _listaMedioPago As List(Of beCampoEntero)
    Public Property listaMedioPago() As List(Of beCampoEntero)
        Get
            Return _listaMedioPago
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaMedioPago = value
        End Set
    End Property

    Private _listaTipoPrecio As List(Of beCampoEntero)
    Public Property listaTipoPrecio() As List(Of beCampoEntero)
        Get
            Return _listaTipoPrecio
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaTipoPrecio = value
        End Set
    End Property

    Private _listaRolCliente As List(Of beCampoEntero2)
    Public Property listaRolCliente() As List(Of beCampoEntero2)
        Get
            Return _listaRolCliente
        End Get
        Set(ByVal value As List(Of beCampoEntero2))
            _listaRolCliente = value
        End Set
    End Property

    Private _listaTipoAgente As List(Of beCampoEntero)
    Public Property listaTipoAgente() As List(Of beCampoEntero)
        Get
            Return _listaTipoAgente
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaTipoAgente = value
        End Set
    End Property

    Private _listaDepartamento As List(Of beCboDepartamento)
    Public Property listaDepartamento() As List(Of beCboDepartamento)
        Get
            Return _listaDepartamento
        End Get
        Set(ByVal value As List(Of beCboDepartamento))
            _listaDepartamento = value
        End Set
    End Property

    Private _listaProvincia As List(Of beCboProvincia)
    Public Property listaProvincia() As List(Of beCboProvincia)
        Get
            Return _listaProvincia
        End Get
        Set(ByVal value As List(Of beCboProvincia))
            _listaProvincia = value
        End Set
    End Property

    Private _listaDistrito As List(Of beCboDistrito)
    Public Property listaDistrito() As List(Of beCboDistrito)
        Get
            Return _listaDistrito
        End Get
        Set(ByVal value As List(Of beCboDistrito))
            _listaDistrito = value
        End Set
    End Property

    Private _listaAlmacen As List(Of beCampoEntero)
    Public Property listaAlmacen() As List(Of beCampoEntero)
        Get
            Return _listaAlmacen
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaAlmacen = value
        End Set
    End Property

    Private _listaTipoExistencia As List(Of beCampoEntero)
    Public Property listaTipoExistencia() As List(Of beCampoEntero)
        Get
            Return _listaTipoExistencia
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaTipoExistencia = value
        End Set
    End Property

    Private _listaLinea As List(Of beCampoEntero)
    Public Property listaLinea() As List(Of beCampoEntero)
        Get
            Return _listaLinea
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaLinea = value
        End Set
    End Property

    'Private _listaSubLinea As List(Of beCampoEntero)
    'Public Property listaSubLinea() As List(Of beCampoEntero)
    '    Get
    '        Return _listaSubLinea
    '    End Get
    '    Set(ByVal value As List(Of beCampoEntero))
    '        _listaSubLinea = value
    '    End Set
    'End Property

    'Private _listaTipoTabla As List(Of beCampoEntero)
    'Public Property listaTipoTabla() As List(Of beCampoEntero)
    '    Get
    '        Return _listaTipoTabla
    '    End Get
    '    Set(ByVal value As List(Of beCampoEntero))
    '        _listaTipoTabla = value
    '    End Set
    'End Property

    Private _listaPersona As List(Of beCampoEntero)
    Public Property listaPersona() As List(Of beCampoEntero)
        Get
            Return _listaPersona
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaPersona = value
        End Set
    End Property

    Private _listaTipoAlmacen As List(Of beCampoEntero)
    Public Property listaTipoAlmacen() As List(Of beCampoEntero)
        Get
            Return _listaTipoAlmacen
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaTipoAlmacen = value
        End Set
    End Property

    Private _listaMagnitud As List(Of beCampoEntero)
    Public Property listaMagnitud() As List(Of beCampoEntero)
        Get
            Return _listaMagnitud
        End Get
        Set(ByVal value As List(Of beCampoEntero))
            _listaMagnitud = value
        End Set
    End Property

    Private _TipoDiseno As Integer
    Public Property TipoDiseno() As Integer
        Get
            Return _TipoDiseno
        End Get
        Set(ByVal value As Integer)
            _TipoDiseno = value
        End Set
    End Property

    Private _IdMedioPago As Integer
    Public Property IdMedioPago() As Integer
        Get
            Return _IdMedioPago
        End Get
        Set(ByVal value As Integer)
            _IdMedioPago = value
        End Set
    End Property

    Private _TasaIGV As Decimal
    Public Property TasaIGV() As Decimal
        Get
            Return _TasaIGV
        End Get
        Set(ByVal value As Decimal)
            _TasaIGV = value
        End Set
    End Property

    Private _FechaActual As Date
    Public Property FechaActual() As Date
        Get
            Return _FechaActual
        End Get
        Set(ByVal value As Date)
            _FechaActual = value
        End Set
    End Property

    Private _TipoPrecio As Integer
    Public Property TipoPrecio() As Integer
        Get
            Return _TipoPrecio
        End Get
        Set(ByVal value As Integer)
            _TipoPrecio = value
        End Set
    End Property

    Private _listaParametros As List(Of Decimal)
    Public Property listaParametros() As List(Of Decimal)
        Get
            Return _listaParametros
        End Get
        Set(ByVal value As List(Of Decimal))
            _listaParametros = value
        End Set
    End Property

    Private _TiendaUbigeo As beCampoCadena
    Public Property TiendaUbigeo() As beCampoCadena
        Get
            Return _TiendaUbigeo
        End Get
        Set(ByVal value As beCampoCadena)
            _TiendaUbigeo = value
        End Set
    End Property

    Private _NroDocumento As String
    Public Property NroDocumento() As String
        Get
            Return _NroDocumento
        End Get
        Set(ByVal value As String)
            _NroDocumento = value
        End Set
    End Property

    Private _listaDepartamentoPartida As List(Of beCboDepartamento)
    Public Property listaDepartamentoPartida() As List(Of beCboDepartamento)
        Get
            Return _listaDepartamentoPartida
        End Get
        Set(ByVal value As List(Of beCboDepartamento))
            _listaDepartamentoPartida = value
        End Set
    End Property

    Private _listaProvinciaPartida As List(Of beCboProvincia)
    Public Property listaProvinciaPartida() As List(Of beCboProvincia)
        Get
            Return _listaProvinciaPartida
        End Get
        Set(ByVal value As List(Of beCboProvincia))
            _listaProvinciaPartida = value
        End Set
    End Property

    Private _listaDistritoPartida As List(Of beCboDistrito)
    Public Property listaDistritoPartida() As List(Of beCboDistrito)
        Get
            Return _listaDistritoPartida
        End Get
        Set(ByVal value As List(Of beCboDistrito))
            _listaDistritoPartida = value
        End Set
    End Property

    Private _listaPermisos As List(Of Integer)
    Public Property listaPermisos() As List(Of Integer)
        Get
            Return _listaPermisos
        End Get
        Set(ByVal value As List(Of Integer))
            _listaPermisos = value
        End Set
    End Property

End Class

Public Class beCampoEntero

    Private _campo1 As Integer
    Public Property campo1() As Integer
        Get
            Return _campo1
        End Get
        Set(ByVal value As Integer)
            _campo1 = value
        End Set
    End Property

    Private _campo2 As String
    Public Property campo2() As String
        Get
            Return _campo2
        End Get
        Set(ByVal value As String)
            _campo2 = value
        End Set
    End Property

End Class

Public Class beCampoEntero2

    Private _campo1 As Integer
    Public Property campo1() As Integer
        Get
            Return _campo1
        End Get
        Set(ByVal value As Integer)
            _campo1 = value
        End Set
    End Property

    Private _campo2 As String
    Public Property campo2() As String
        Get
            Return _campo2
        End Get
        Set(ByVal value As String)
            _campo2 = value
        End Set
    End Property

    Private _campo3 As String
    Public Property campo3() As String
        Get
            Return _campo3
        End Get
        Set(ByVal value As String)
            _campo3 = value
        End Set
    End Property

End Class

Public Class beCampoCadena

    Private _campo1 As String
    Public Property campo1() As String
        Get
            Return _campo1
        End Get
        Set(ByVal value As String)
            _campo1 = value
        End Set
    End Property

    Private _campo2 As String
    Public Property campo2() As String
        Get
            Return _campo2
        End Get
        Set(ByVal value As String)
            _campo2 = value
        End Set
    End Property

End Class

Public Class beCboDepartamento

    Private _CodDpto As String
    Public Property CodDpto() As String
        Get
            Return _CodDpto
        End Get
        Set(ByVal value As String)
            _CodDpto = value
        End Set
    End Property

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

End Class

Public Class beCboProvincia

    Private _CodProv As String
    Public Property CodProv() As String
        Get
            Return _CodProv
        End Get
        Set(ByVal value As String)
            _CodProv = value
        End Set
    End Property

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

End Class

Public Class beCboDistrito
    Private _CodDist As String
    Public Property CodDist() As String
        Get
            Return _CodDist
        End Get
        Set(ByVal value As String)
            _CodDist = value
        End Set
    End Property

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

End Class
﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class EstadoEntrega
    Private _IdEstadoEnt As Integer
    Private _eent_Nombre As String
    Private _eent_Estado As String
    Private _descEstado As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idestadoentrega As Integer, ByVal nombre As String)
        Me.Id = idestadoentrega
        Me.Descripcion = nombre
    End Sub
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._eent_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdEstadoEnt
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoEnt = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._eent_Nombre
        End Get
        Set(ByVal value As String)
            Me._eent_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._eent_Estado
        End Get
        Set(ByVal value As String)
            Me._eent_Estado = value
        End Set
    End Property
End Class



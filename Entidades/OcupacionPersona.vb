'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class OcupacionPersona
    Private _IdOcupacion As Integer
    Private _op_Principal As Boolean
    Private _IdGiro As Integer
    Private _IdPersona As Integer


    Public Property IdOcupacion() As Integer
        Get
            Return Me._IdOcupacion
        End Get
        Set(ByVal value As Integer)
            Me._IdOcupacion = value
        End Set
    End Property

    Public Property Principal() As Boolean
        Get
            Return Me._op_Principal
        End Get
        Set(ByVal value As Boolean)
            Me._op_Principal = value
        End Set
    End Property

    Public Property IdGiro() As Integer
        Get
            Return Me._IdGiro
        End Get
        Set(ByVal value As Integer)
            Me._IdGiro = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Private _objocupacion As Object
    Public Property objOcupacion() As Object
        Get
            Return _objocupacion
        End Get
        Set(ByVal value As Object)
            _objocupacion = value
        End Set
    End Property

    Private _objGiro As Object
    Public Property objGiro() As Object
        Get
            Return _objGiro
        End Get
        Set(ByVal value As Object)
            _objGiro = value
        End Set
    End Property

End Class

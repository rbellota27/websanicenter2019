﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class MovCuentaPorPagar

    Protected _IdMovCtaPP As Integer
    Protected _IdCuentaProv As Integer
    Protected _IdProveedor As Integer
    Protected _mcp_Fecha As Date
    Protected _mcp_FechaCanc As Date
    Protected _mcp_Monto As Decimal
    Protected _mcp_Saldo As Decimal
    Protected _mcp_Factor As Integer
    Protected _IdDocumento As Integer
    Protected _IdDetalleConcepto As Integer
    Protected _IdMovCuentaTipo As Integer
    Protected _IdCargoCuentaP As Integer

    Protected _Codigo As String
    Protected _Serie As String
    Protected _NomTipoDocumento As String
    Protected _IdMoneda As Integer
    Protected _MonedaSimbolo As String
    Private _flagsustento As Integer
    Protected _TipoDoc_NroDocumento As String
    Protected _doc_FechaEmision As Date
    Protected _doc_FechaVenc As Date
    Protected _Moneda_Monto As String
    Protected _Moneda_Saldo As String
    Protected _Ruc As String
    Protected _Empresa As String
    Protected _DescripcionPersona As String
    Protected _ChequesAsociados As String
    Protected _PagoProgramado As String
    Protected _NroDocumentoExterno As String

    Private _NroDocumento As String
    Protected _banco As String
    Protected _nrocuenta As String
    Protected _dcan_NumeroCheque As String
    Protected _moneda As String
    Protected _dcan_monto As Decimal
    Public Property NroDocumento() As String
        Get
            Return Me._NroDocumento
        End Get
        Set(ByVal value As String)
            Me._NroDocumento = value
        End Set
    End Property
    Public Property banco() As String
        Get
            Return Me._banco
        End Get
        Set(ByVal value As String)
            Me._banco = value
        End Set
    End Property
    Public Property nrocuenta() As String
        Get
            Return Me._nrocuenta
        End Get
        Set(ByVal value As String)
            Me._nrocuenta = value
        End Set
    End Property
    Public Property dcan_NumeroCheque() As String
        Get
            Return Me._dcan_NumeroCheque
        End Get
        Set(ByVal value As String)
            Me._dcan_NumeroCheque = value
        End Set
    End Property
    Public Property moneda() As String
        Get
            Return Me._moneda
        End Get
        Set(ByVal value As String)
            Me._moneda = value
        End Set
    End Property
    Public Property dcan_monto() As Decimal
        Get
            Return Me._dcan_monto
        End Get
        Set(ByVal value As Decimal)
            Me._dcan_monto = value
        End Set
    End Property

    Public Property NroDocumentoExterno() As String
        Get
            Return Me._NroDocumentoExterno
        End Get
        Set(ByVal value As String)
            Me._NroDocumentoExterno = value
        End Set
    End Property
    Public Property PagoProgramado() As String
        Get
            Return Me._PagoProgramado
        End Get
        Set(ByVal value As String)
            Me._PagoProgramado = value
        End Set
    End Property

    Public Property DescripcionPersona() As String
        Get
            Return Me._DescripcionPersona
        End Get
        Set(ByVal value As String)
            Me._DescripcionPersona = value
        End Set
    End Property

    Public Property ChequesAsociados() As String
        Get
            Return Me._ChequesAsociados
        End Get
        Set(ByVal value As String)
            Me._ChequesAsociados = value
        End Set
    End Property


    Public Property Ruc() As String
        Get
            Return Me._Ruc
        End Get
        Set(ByVal value As String)
            Me._Ruc = value
        End Set
    End Property
    Public Property Empresa() As String
        Get
            Return Me._Empresa
        End Get
        Set(ByVal value As String)
            Me._Empresa = value
        End Set
    End Property
    Public Property Moneda_Saldo() As String
        Get
            Return Me._Moneda_Saldo
        End Get
        Set(ByVal value As String)
            Me._Moneda_Saldo = value
        End Set
    End Property

    Public Property Moneda_Monto() As String
        Get
            Return Me._Moneda_Monto
        End Get
        Set(ByVal value As String)
            Me._Moneda_Monto = value
        End Set
    End Property

    Public Property TipoDoc_NroDocumento() As String
        Get
            Return Me._TipoDoc_NroDocumento
        End Get
        Set(ByVal value As String)
            Me._TipoDoc_NroDocumento = value
        End Set
    End Property
    Public Property doc_FechaEmision() As Date
        Get
            Return Me._doc_FechaEmision
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaEmision = value
        End Set
    End Property
    Public Property doc_FechaVenc() As Date
        Get
            Return Me._doc_FechaVenc
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaVenc = value
        End Set
    End Property
    Public Property FlagSustento() As Integer
        Get
            Return Me._flagsustento
        End Get
        Set(ByVal value As Integer)
            Me._flagsustento = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdMovCtaPP
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCtaPP = value
        End Set
    End Property
    Public Property IdCuentaProv() As Integer
        Get
            Return Me._IdCuentaProv
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaProv = value
        End Set
    End Property
    Public Property IdProveedor() As Integer
        Get
            Return Me._IdProveedor
        End Get
        Set(ByVal value As Integer)
            Me._IdProveedor = value
        End Set
    End Property
    Public Property Fecha() As Date
        Get
            Return Me._mcp_Fecha
        End Get
        Set(ByVal value As Date)
            Me._mcp_Fecha = value
        End Set
    End Property
    Public ReadOnly Property DescFecha() As String
        Get
            If Me._mcp_Fecha = Nothing Then
                Return ""
            Else
                Return Format(Me._mcp_Fecha, "dd/MM/yyyy")
            End If
        End Get
    End Property
    Public Property FechaCancelacion() As Date
        Get
            Return Me._mcp_FechaCanc
        End Get
        Set(ByVal value As Date)
            Me._mcp_FechaCanc = value
        End Set
    End Property
    Public ReadOnly Property DescFechaCancelacion() As String
        Get
            If Me._mcp_FechaCanc = Nothing Then
                Return ""
            Else
                Return Format(Me._mcp_FechaCanc, "dd/MM/yyyy")
            End If
        End Get
    End Property
    Public Property Monto() As Decimal
        Get
            Return Me._mcp_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._mcp_Monto = value
        End Set
    End Property
    Public ReadOnly Property DescMonto() As String
        Get
            Return _mcp_Monto.ToString("F")
        End Get
    End Property
    Public Property Saldo() As Decimal
        Get
            Return Me._mcp_Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._mcp_Saldo = value
        End Set
    End Property
    Public ReadOnly Property DescSaldo() As String
        Get
            Return _mcp_Saldo.ToString("F")
        End Get
    End Property
    Public Property Factor() As Integer
        Get
            Return Me._mcp_Factor
        End Get
        Set(ByVal value As Integer)
            Me._mcp_Factor = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
    Public Property IdDetalleConcepto() As Integer
        Get
            Return Me._IdDetalleConcepto
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleConcepto = value
        End Set
    End Property
    Public Property IdMovCuentaTipo() As Integer
        Get
            Return Me._IdMovCuentaTipo
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuentaTipo = value
        End Set
    End Property
    Public Property IdCargoCuentaP() As Integer
        Get
            Return Me._IdCargoCuentaP
        End Get
        Set(ByVal value As Integer)
            Me._IdCargoCuentaP = value
        End Set
    End Property


    '************************
    Public Property Codigo() As String
        Get
            Return Me._Codigo
        End Get
        Set(ByVal value As String)
            Me._Codigo = value
        End Set
    End Property

    Public Property Serie() As String
        Get
            Return Me._Serie
        End Get
        Set(ByVal value As String)
            Me._Serie = value
        End Set
    End Property

    Public Property NomTipoDocumento() As String
        Get
            Return Me._NomTipoDocumento
        End Get
        Set(ByVal value As String)
            Me._NomTipoDocumento = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property MonedaSimbolo() As String
        Get
            Return Me._MonedaSimbolo
        End Get
        Set(ByVal value As String)
            Me._MonedaSimbolo = value
        End Set
    End Property

    Public ReadOnly Property getNroDocumento() As String
        Get
            Return Serie + " - " + Codigo
        End Get
    End Property

End Class

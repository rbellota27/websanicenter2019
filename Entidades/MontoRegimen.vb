'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class MontoRegimen
    Private _IdDocumento As Integer
    Private _mr_Monto As Decimal
    Private _ro_Id As Integer

    Public Sub New()
    End Sub

    Public Sub New(ByVal IdDocumento As Integer, ByVal monto As Decimal, ByVal IdRegimenOperacion As Integer)
        Me.IdDocumento = IdDocumento
        Me.Monto = monto
        Me.ro_Id = IdRegimenOperacion
    End Sub

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._mr_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._mr_Monto = value
        End Set
    End Property

    Public Property ro_Id() As Integer
        Get
            Return Me._ro_Id
        End Get
        Set(ByVal value As Integer)
            Me._ro_Id = value
        End Set
    End Property
End Class

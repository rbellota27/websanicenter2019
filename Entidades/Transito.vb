﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Transito
    Private _IdTransito As Integer
    Private _TranNombre As String
    Private _TranAbv As String
    Private _descEstado As String
    Private _TranEstado As Boolean
    Private _Tran_Codigo As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idtransito As Integer, ByVal nombre As String, ByVal estado As Boolean)
        Me._IdTransito = idtransito
        Me._TranNombre = nombre
        Me._TranEstado = estado
    End Sub
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._TranEstado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property IdTransito() As Integer
        Get
            Return Me._IdTransito
        End Get
        Set(ByVal value As Integer)
            Me._IdTransito = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._TranNombre
        End Get
        Set(ByVal value As String)
            Me._TranNombre = value
        End Set
    End Property
    Public Property Abv() As String
        Get
            Return Me._TranAbv
        End Get
        Set(ByVal value As String)
            Me._TranAbv = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return Me._TranEstado
        End Get
        Set(ByVal value As Boolean)
            Me._TranEstado = value
        End Set
    End Property
    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "1"
            If Estado = False Then Return "0"
        End Get
    End Property
    Public Property Tran_Codigo() As String
        Get
            Return _Tran_Codigo
        End Get
        Set(ByVal value As String)
            _Tran_Codigo = value
        End Set
    End Property
End Class

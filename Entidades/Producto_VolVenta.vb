﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM

Public Class Producto_VolVenta
    Inherits VolumenVenta
    'Esta clase trabaja a veces junto con la clase VolumenVenta
    Private _IdLinea As Integer
    Private _IdSubLinea As Integer
    Private _prod_Codigo As String
    Private _prod_Nombre As String
    Private _um_NombreLargo As String
    Private _um_NombreCorto As String

    Public Property um_NombreCorto() As String
        Get
            Return Me._um_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._um_NombreCorto = value
        End Set
    End Property


    Public Property IdLinea() As Integer
        Get
            Return Me._IdLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdLinea = value
        End Set
    End Property
    Public Property IdSubLinea() As Integer
        Get
            Return Me._IdSubLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLinea = value
        End Set
    End Property
    Public Property prod_Codigo() As String
        Get
            Return Me._prod_Codigo
        End Get
        Set(ByVal value As String)
            Me._prod_Codigo = value
        End Set
    End Property
    Public Property prod_Nombre() As String
        Get
            Return Me._prod_Nombre
        End Get
        Set(ByVal value As String)
            Me._prod_Nombre = value
        End Set
    End Property
    Public Property um_NombreLargo() As String
        Get
            Return Me._um_NombreLargo
        End Get
        Set(ByVal value As String)
            Me._um_NombreLargo = value
        End Set
    End Property

End Class

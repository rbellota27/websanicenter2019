﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 10-Noviembre-2009
'Hora    : 07:30 pm
'*************************************************
Public Class TipoDocumentoAE

    Private _tdae_CantidadFilas As Integer
    Private _tdae_MontoMaximoNoAfecto As Decimal
    Private _tdae_MontoMaximoAfecto As Decimal
    Private _IdEmpresa As Integer
    Private _IdArea As Integer
    Private _IdTipoDocumento As Integer
    '****** Propietario - Área - Tipo Documento - Moneda
    Private _NomEmpresa As String
    Private _NomArea As String
    Private _NomTipoDocumento As String
    Private _IdMoneda As Integer
    Private _NomMoneda As String
    Public Property CantidadFilas() As Integer
        Get
            Return Me._tdae_CantidadFilas
        End Get
        Set(ByVal value As Integer)
            Me._tdae_CantidadFilas = value
        End Set
    End Property

    Public Property MontoMaximoNoAfecto() As Decimal
        Get
            Return Me._tdae_MontoMaximoNoAfecto
        End Get
        Set(ByVal value As Decimal)
            Me._tdae_MontoMaximoNoAfecto = value
        End Set
    End Property

    Public Property MontoMaximoAfecto() As Decimal
        Get
            Return Me._tdae_MontoMaximoAfecto
        End Get
        Set(ByVal value As Decimal)
            Me._tdae_MontoMaximoAfecto = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdArea() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property
    '****** Propietario - Área - Tipo Documento - Moneda
    Public Property NomEmpresa() As String
        Get
            Return _NomEmpresa
        End Get
        Set(ByVal value As String)
            _NomEmpresa = value
        End Set
    End Property
    Public Property NomArea() As String
        Get
            Return _NomArea
        End Get
        Set(ByVal value As String)
            _NomArea = value
        End Set
    End Property
    Public Property NomTipoDocumento() As String
        Get
            Return _NomTipoDocumento
        End Get
        Set(ByVal value As String)
            _NomTipoDocumento = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property
    Public Property NomMoneda() As String
        Get
            Return _NomMoneda
        End Get
        Set(ByVal value As String)
            _NomMoneda = value
        End Set
    End Property
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************** '29Marzo 2010 01_00
Public Class TipoAlmacen

    Private _IdTipoAlmacen As Integer
    Private _Tipoalm_Nombre As String
    Private _Tipoalm_NombreCorto As String
    Private _Tipoalm_Principal As String
    Private _Tipoalm_Estado As Int16
    Private _cadenaTipoAlmacen As String
    Private _anio As String
    Private _mesinicial As String
    Private _mesfinal As String
    Private _valorizado As Decimal
    Private _OrderID As Integer
    Private _fecha As String
    Private _mes As String
    Private _almacen As String

    Private _stockini As Decimal
    Private _stockfinal As Decimal
    Private _compras As Decimal
    Private _transferencias As Decimal
    Private _transito As Decimal
    Private _totalcompras As Decimal
    Private _iri As Decimal
    Private _ndi As Decimal
    Private _dias As Decimal

    Private _tienda As String
    Private _Escala As String
    Private _objetivoxtienda As Integer
    Public Property ObjetivoxTienda() As Integer
        Get
            Return _objetivoxtienda
        End Get
        Set(ByVal value As Integer)
            _objetivoxtienda = value
        End Set
    End Property
    Public Property Escala() As String
        Get
            Return _Escala
        End Get
        Set(ByVal value As String)
            _Escala = value
        End Set
    End Property
#Region "Meses"

    Private _En As Decimal
    Private _Feb As Decimal
    Private _Mar As Decimal
    Private _Abr As Decimal
    Private _Mayo As Decimal
    Private _Jun As Decimal
    Private _Jul As Decimal
    Private _Agos As Decimal
    Private _Set As Decimal
    Private _Oct As Decimal
    Private _Nov As Decimal
    Private _Dici As Decimal

    Public Property Enero() As Decimal
        Get
            Return _En
        End Get
        Set(ByVal value As Decimal)
            _En = value
        End Set
    End Property
    Public Property Febrero() As Decimal
        Get
            Return _Feb
        End Get
        Set(ByVal value As Decimal)
            _Feb = value
        End Set
    End Property
    Public Property Marzo() As Decimal
        Get
            Return _Mar
        End Get
        Set(ByVal value As Decimal)
            _Mar = value
        End Set
    End Property
    Public Property Abril() As Decimal
        Get
            Return _Abr
        End Get
        Set(ByVal value As Decimal)
            _Abr = value
        End Set
    End Property
    Public Property Mayo() As Decimal
        Get
            Return _Mayo
        End Get
        Set(ByVal value As Decimal)
            _Mayo = value
        End Set
    End Property
    Public Property Junio() As Decimal
        Get
            Return _Jun
        End Get
        Set(ByVal value As Decimal)
            _Jun = value
        End Set
    End Property
    Public Property Julio() As Decimal
        Get
            Return _Jul
        End Get
        Set(ByVal value As Decimal)
            _Jul = value
        End Set
    End Property
    Public Property Agosto() As Decimal
        Get
            Return _Agos
        End Get
        Set(ByVal value As Decimal)
            _Agos = value
        End Set
    End Property
    Public Property Septiembre() As Decimal
        Get
            Return _Set
        End Get
        Set(ByVal value As Decimal)
            _Set = value
        End Set
    End Property
    Public Property Octubre() As Decimal
        Get
            Return _Oct
        End Get
        Set(ByVal value As Decimal)
            _Oct = value
        End Set
    End Property
    Public Property Noviembre() As Decimal
        Get
            Return _Nov
        End Get
        Set(ByVal value As Decimal)
            _Nov = value
        End Set
    End Property

    Public Property Diciembre() As Decimal
        Get
            Return _Dici
        End Get
        Set(ByVal value As Decimal)
            _Dici = value
        End Set
    End Property
#End Region
    Public Property Numdias() As Decimal
        Get
            Return _dias
        End Get
        Set(ByVal value As Decimal)
            _dias = value
        End Set
    End Property
    Public Property Tienda() As String
        Get
            Return _tienda
        End Get
        Set(ByVal value As String)
            _tienda = value
        End Set
    End Property
    Public Property Transferencias() As Decimal
        Get
            Return _transferencias
        End Get
        Set(ByVal value As Decimal)
            _transferencias = value
        End Set
    End Property

    Public Property Transito() As Decimal
        Get
            Return _transito
        End Get
        Set(ByVal value As Decimal)
            _transito = value
        End Set
    End Property

    Public Property Iri() As Decimal
        Get
            Return _iri
        End Get
        Set(ByVal value As Decimal)
            _iri = value
        End Set
    End Property
    Public Property Ndi() As Decimal
        Get
            Return _ndi
        End Get
        Set(ByVal value As Decimal)
            _ndi = value
        End Set
    End Property
    Public Property TotalCompras() As Decimal
        Get
            Return _totalcompras
        End Get
        Set(ByVal value As Decimal)
            _totalcompras = value
        End Set
    End Property
    Public Property Compras() As Decimal
        Get
            Return _compras
        End Get
        Set(ByVal value As Decimal)
            _compras = value
        End Set
    End Property

    Public Property StockInicial() As Decimal
        Get
            Return _stockini
        End Get
        Set(ByVal value As Decimal)
            _stockini = value
        End Set
    End Property
    Public Property StockFinal() As Decimal
        Get
            Return _stockfinal
        End Get
        Set(ByVal value As Decimal)
            _stockfinal = value
        End Set
    End Property
    Public Property CadenaTipoAlmacen() As String
        Get
            Return _cadenaTipoAlmacen
        End Get
        Set(ByVal value As String)
            _cadenaTipoAlmacen = value
        End Set
    End Property
    Public Property Anio() As String
        Get
            Return _anio
        End Get
        Set(ByVal value As String)
            _anio = value
        End Set
    End Property
    Public Property MesInicial() As String
        Get
            Return _mesinicial
        End Get
        Set(ByVal value As String)
            _mesinicial = value
        End Set
    End Property
    Public Property MesFinal() As String
        Get
            Return _mesfinal
        End Get
        Set(ByVal value As String)
            _mesfinal = value
        End Set
    End Property
    Public Property Almacen() As String
        Get
            Return _almacen
        End Get
        Set(ByVal value As String)
            _almacen = value
        End Set
    End Property
    Public Property Mes() As String
        Get
            Return _mes
        End Get
        Set(ByVal value As String)
            _mes = value
        End Set
    End Property
    Public Property OrderId() As Integer
        Get
            Return _OrderID
        End Get
        Set(ByVal value As Integer)
            _OrderID = value
        End Set
    End Property
    Public Property Fecha() As String
        Get
            Return _fecha
        End Get
        Set(ByVal value As String)
            _fecha = value
        End Set
    End Property

    Public Property Valorizado() As Decimal
        Get
            Return _valorizado
        End Get
        Set(ByVal value As Decimal)
            _valorizado = value
        End Set
    End Property
    Public Sub New()

    End Sub
    Public Sub New(ByVal idtipoalmacen As Integer, ByVal nombreAlamcen As String)
        Me.IdTipoAlmacen = idtipoalmacen
        Me.Tipoalm_Nombre = nombreAlamcen
    End Sub
    Public Property IdTipoAlmacen() As Integer
        Get
            Return _IdTipoAlmacen
        End Get
        Set(ByVal value As Integer)
            _IdTipoAlmacen = value
        End Set
    End Property

    Public Property Tipoalm_Nombre() As String
        Get
            Return _Tipoalm_Nombre
        End Get
        Set(ByVal value As String)
            _Tipoalm_Nombre = value
        End Set
    End Property
    Public Property Tipoalm_NombreCorto() As String
        Get
            Return _Tipoalm_NombreCorto
        End Get
        Set(ByVal value As String)
            _Tipoalm_NombreCorto = value
        End Set
    End Property

    Public Property Tipoalm_Principal() As String
        Get
            Return _Tipoalm_Principal
        End Get
        Set(ByVal value As String)
            _Tipoalm_Principal = value
        End Set
    End Property
    Public Property Tipoalm_Estado() As Int16
        Get
            Return _Tipoalm_Estado
        End Get
        Set(ByVal value As Int16)
            _Tipoalm_Estado = value
        End Set
    End Property

    Public ReadOnly Property strTipoalm_Principal() As String
        Get
            Dim str As String = ""
            If Tipoalm_Principal = True Or Tipoalm_Principal = "1" Then
                str = "Principal"
            End If
            Return str
        End Get
    End Property
    Public ReadOnly Property strTipoalm_Estado() As String
        Get
            Dim str As String = "Inactivo"
            If Tipoalm_Estado = "1" Then
                str = "Activo"
            End If
            Return str
        End Get

    End Property


End Class

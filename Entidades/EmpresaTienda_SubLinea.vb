﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'******************** MARTES 19 MAYO 2010 HORA 11_35 AM

Public Class EmpresaTienda_SubLinea

    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _IdSubLinea As Integer
    Private _ets_CtaDebeCompra As String
    Private _ets_CtaHaberCompra As String
    Private _ets_CtaCostoCompra As String
    Private _ets_CtaDebeVenta As String
    Private _ets_CtaHaberVenta As String
    Private _ets_CtaCostoVenta As String

    Private _IdLinea As Integer

    Public Property IdLinea() As Integer
        Get
            Return Me._IdLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdLinea = value
        End Set
    End Property



    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdSubLinea() As Integer
        Get
            Return Me._IdSubLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLinea = value
        End Set
    End Property

    Public Property CtaDebeCompra() As String
        Get
            Return Me._ets_CtaDebeCompra
        End Get
        Set(ByVal value As String)
            Me._ets_CtaDebeCompra = value
        End Set
    End Property

    Public Property CtaHaberCompra() As String
        Get
            Return Me._ets_CtaHaberCompra
        End Get
        Set(ByVal value As String)
            Me._ets_CtaHaberCompra = value
        End Set
    End Property

    Public Property CtaCostoCompra() As String
        Get
            Return Me._ets_CtaCostoCompra
        End Get
        Set(ByVal value As String)
            Me._ets_CtaCostoCompra = value
        End Set
    End Property

    Public Property CtaDebeVenta() As String
        Get
            Return Me._ets_CtaDebeVenta
        End Get
        Set(ByVal value As String)
            Me._ets_CtaDebeVenta = value
        End Set
    End Property

    Public Property CtaHaberVenta() As String
        Get
            Return Me._ets_CtaHaberVenta
        End Get
        Set(ByVal value As String)
            Me._ets_CtaHaberVenta = value
        End Set
    End Property

    Public Property CtaCostoVenta() As String
        Get
            Return Me._ets_CtaCostoVenta
        End Get
        Set(ByVal value As String)
            Me._ets_CtaCostoVenta = value
        End Set
    End Property



End Class

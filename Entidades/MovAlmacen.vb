'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class MovAlmacen
    Private _IdDetalleDocumento As Integer
    Private _IdTienda As Integer
    Private _IdAlmacen As Integer
    Private _IdEmpresa As Integer
    Private _IdProducto As Integer
    Private _IdUnidadMedida As Integer
    Private _ma_Fecha As Date
    Private _ma_UMPrincipal As String
    Private _IdUMPrincipal As Integer
    Private _ma_CantidadMov As Decimal
    Private _ma_CantxAtender As Decimal
    Private _ma_Comprometido As Boolean
    Private _IdDocumento As Integer
    Private _ma_Factor As Integer
    Private _IdTipoOperacion As Integer
    Private _IdMetodoV As Integer
    Private _NomProducto As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal _IdEmpresa1 As Integer, ByVal _IdAlmacen1 As Integer, ByVal _IdTienda1 As Integer, ByVal _IdDocumento1 As Integer, ByVal _IdProducto1 As Integer, Optional ByVal _IdDetalleDocumento1 As Integer = Nothing, _
                          Optional ByVal _ma_Fecha1 As Date = Nothing, Optional ByVal _ma_UMPrincipal1 As String = Nothing, Optional ByVal _ma_CantidadMov1 As Decimal = Nothing, _
                         Optional ByVal _ma_CantxAtender1 As Decimal = Nothing, Optional ByVal _ma_Comprometido1 As Boolean = Nothing, Optional ByVal _ma_Factor1 As Integer = Nothing, _
                         Optional ByVal _IdTipoOperacion1 As Integer = Nothing, Optional ByVal _IdMetodoV1 As Integer = Nothing)
        Me._IdAlmacen = _IdAlmacen1
        Me._IdDetalleDocumento = _IdDetalleDocumento1
        Me._IdDocumento = _IdDocumento1
        Me._IdProducto = _IdProducto1
        Me._IdTienda = _IdTienda1
        Me._IdTipoOperacion = _IdTipoOperacion1
        Me._IdMetodoV = _IdMetodoV1
        Me._ma_CantidadMov = _ma_CantidadMov1
        Me._ma_CantxAtender = _ma_CantxAtender1
        Me._ma_Comprometido = _ma_Comprometido1
        Me._ma_Factor = _ma_Factor1
        Me._ma_Fecha = _ma_Fecha1
        Me._ma_UMPrincipal = _ma_UMPrincipal1

    End Sub
    Public Property NomProducto() As String
        Get
            Return Me._NomProducto
        End Get
        Set(ByVal value As String)
            Me._NomProducto = value
        End Set
    End Property
    Public Property UMPrincipal() As String
        Get
            Return Me._ma_UMPrincipal
        End Get
        Set(ByVal value As String)
            Me._ma_UMPrincipal = value
        End Set
    End Property
    Public Property IdUMPrincipal() As Integer
        Get
            Return _IdUMPrincipal
        End Get
        Set(ByVal value As Integer)
            _IdUMPrincipal = value
        End Set
    End Property
    Public Property IdDetalleDocumento() As Integer
        Get
            Return Me._IdDetalleDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleDocumento = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property
    Public Property IdUnidadMedida() As Integer
        Get
            Return _IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            _IdUnidadMedida = value
        End Set
    End Property
    Public Property Fecha() As Date
        Get
            Return Me._ma_Fecha
        End Get
        Set(ByVal value As Date)
            Me._ma_Fecha = value
        End Set
    End Property

    Public Property CantidadMov() As Decimal
        Get
            Return Me._ma_CantidadMov
        End Get
        Set(ByVal value As Decimal)
            Me._ma_CantidadMov = value
        End Set
    End Property
    Public Property CantxAtender() As Decimal
        Get
            Return Me._ma_CantxAtender
        End Get
        Set(ByVal value As Decimal)
            Me._ma_CantxAtender = value
        End Set
    End Property
    Public Property Comprometido() As Boolean
        Get
            Return Me._ma_Comprometido
        End Get
        Set(ByVal value As Boolean)
            Me._ma_Comprometido = value
        End Set
    End Property
    Public Property Factor() As Integer
        Get
            Return Me._ma_Factor
        End Get
        Set(ByVal value As Integer)
            Me._ma_Factor = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property
    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTipoOperacion() As Integer
        Get
            Return Me._IdTipoOperacion
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoOperacion = value
        End Set
    End Property
    Public Property IdMetodoV() As Integer
        Get
            Return Me._IdMetodoV
        End Get
        Set(ByVal value As Integer)
            Me._IdMetodoV = value
        End Set
    End Property
End Class

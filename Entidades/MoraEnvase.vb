'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class MoraEnvase
    Private _IdMoraEnvase As Integer
    Private _moe_TasaDia As Decimal
    Private _IdPersona As Integer
    Private _IdMoneda As Integer
    Public Property Id() As Integer
        Get
            Return Me._IdMoraEnvase
        End Get
        Set(ByVal value As Integer)
            Me._IdMoraEnvase = value
        End Set
    End Property

    Public Property TasaDia() As Decimal
        Get
            Return Me._moe_TasaDia
        End Get
        Set(ByVal value As Decimal)
            Me._moe_TasaDia = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
End Class

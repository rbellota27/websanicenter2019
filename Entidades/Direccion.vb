'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Direccion
    Private _IdPersona As Integer
    Private _IdTipoDireccion As Integer
    Private _IdDireccion As Integer
    Private _IdZonaTipo As Integer
    Private _dir_ZonaNombre As String
    Private _IdViaTipo As Integer
    Private _dir_ViaNumero As String
    Private _dir_ViaNombre As String
    Private _dir_Etapa As String
    Private _dir_Mz As String
    Private _dir_Lt As String
    Private _dir_Interior As String
    Private _dir_Direccion As String
    Private _dir_Referencia As String
    Private _dir_Ubigeo As String
    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Private _TipoDireccion
    Public Property TipoDireccion() As String
        Get
            Return _TipoDireccion
        End Get
        Set(ByVal value As String)
            _TipoDireccion = value
        End Set
    End Property

    Public Property IdTipoDireccion() As Integer
        Get
            Return Me._IdTipoDireccion
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDireccion = value
        End Set
    End Property

    Public Property IdDireccion() As Integer
        Get
            Return Me._IdDireccion
        End Get
        Set(ByVal value As Integer)
            Me._IdDireccion = value
        End Set
    End Property

    Public Property IdZonaTipo() As Integer
        Get
            Return Me._IdZonaTipo
        End Get
        Set(ByVal value As Integer)
            Me._IdZonaTipo = value
        End Set
    End Property

    Public Property ZonaNombre() As String
        Get
            Return Me._dir_ZonaNombre
        End Get
        Set(ByVal value As String)
            Me._dir_ZonaNombre = value
        End Set
    End Property

    Public Property IdViaTipo() As Integer
        Get
            Return Me._IdViaTipo
        End Get
        Set(ByVal value As Integer)
            Me._IdViaTipo = value
        End Set
    End Property

    Public Property ViaNumero() As String
        Get
            Return Me._dir_ViaNumero
        End Get
        Set(ByVal value As String)
            Me._dir_ViaNumero = value
        End Set
    End Property
    Public Property ViaNombre() As String
        Get
            Return _dir_ViaNombre
        End Get
        Set(ByVal value As String)
            _dir_ViaNombre = value
        End Set
    End Property
    Public Property Etapa() As String
        Get
            Return Me._dir_Etapa
        End Get
        Set(ByVal value As String)
            Me._dir_Etapa = value
        End Set
    End Property

    Public Property Mz() As String
        Get
            Return Me._dir_Mz
        End Get
        Set(ByVal value As String)
            Me._dir_Mz = value
        End Set
    End Property

    Public Property Lt() As String
        Get
            Return Me._dir_Lt
        End Get
        Set(ByVal value As String)
            Me._dir_Lt = value
        End Set
    End Property

    Public Property Interior() As String
        Get
            Return Me._dir_Interior
        End Get
        Set(ByVal value As String)
            Me._dir_Interior = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return Me._dir_Direccion
        End Get
        Set(ByVal value As String)
            Me._dir_Direccion = value
        End Set
    End Property

    Public Property Referencia() As String
        Get
            Return Me._dir_Referencia
        End Get
        Set(ByVal value As String)
            Me._dir_Referencia = value
        End Set
    End Property

    Public Property Ubigeo() As String
        Get
            Return Me._dir_Ubigeo
        End Get
        Set(ByVal value As String)
            Me._dir_Ubigeo = value
        End Set
    End Property

    Private _idOculto As String
    Public Property idOculto() As String
        Get
            Return _idOculto
        End Get
        Set(ByVal value As String)
            _idOculto = value
        End Set
    End Property

    Private _textOculto As String
    Public Property textOculto() As String
        Get
            Return _textOculto
        End Get
        Set(ByVal value As String)
            _textOculto = value
        End Set
    End Property

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 29/01/2010 4:50 pm

Public Class Caja_Post

    'Nota: No modificar esta clase, 
    'esta mapeada con la tabla correspondiente de la BD; 
    'utilicen para agregar mas atributos la Entidad Caja_PostView

    Protected _IdCaja As Integer
    Protected _IdPost As Integer
    Protected _cpo_estado As Nullable(Of Boolean)

    Public Sub New()
        'No cambiar estos datos, son usados por el sp SelectxParams
        Me._IdCaja = -1
        Me._IdPost = -1
        Me._cpo_estado = Nothing
    End Sub

    Public Sub New(ByVal IdCaja As Integer, ByVal IdPost As Integer, ByVal Estado As Boolean)
        Me._IdCaja = IdCaja
        Me._IdPost = IdPost
        Me._cpo_estado = Estado
    End Sub

    Public Property IdCaja() As Integer
        Get
            Return Me._IdCaja
        End Get
        Set(ByVal value As Integer)
            Me._IdCaja = value
        End Set
    End Property
    Public Property IdPost() As Integer
        Get
            Return Me._IdPost
        End Get
        Set(ByVal value As Integer)
            Me._IdPost = value
        End Set
    End Property

    Public Property Estado() As Boolean?
        Get
            Return Me._cpo_estado
        End Get
        Set(ByVal value As Boolean?)
            Me._cpo_estado = value
        End Set
    End Property

    Public ReadOnly Property DescEstado() As String
        Get
            If Me._cpo_estado Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property
End Class

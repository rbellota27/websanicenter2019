﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*********************   LUNES 24052010

Public Class CostoFletexUnd
    Private _IdTiendaOrigen As Integer
    Private _IdTiendaDestino As Integer
    Private _IdMoneda As Integer
    Private _cfu_costoxUnd As Decimal
    Private _IdUnidadMedida As Integer
    Private _IdProducto As Integer
    Private _Producto As String
    Private _Moneda As String
    Private _UnidadMedida As String
    Private _CodigoProducto As String
    Private _Linea As String
    Private _SubLinea As String

    Private _Cadena_IdUnidadMedida_Prod As String
    Private _Cadena_UnidadMedida_Prod As String

    Public Property Cadena_IdUnidadMedida_Prod() As String
        Get
            Return Me._Cadena_IdUnidadMedida_Prod
        End Get
        Set(ByVal value As String)
            Me._Cadena_IdUnidadMedida_Prod = value
        End Set
    End Property

    Public Property Cadena_UnidadMedida_Prod() As String
        Get
            Return Me._Cadena_UnidadMedida_Prod
        End Get
        Set(ByVal value As String)
            Me._Cadena_UnidadMedida_Prod = value
        End Set
    End Property



    Public Property IdTiendaOrigen() As Integer
        Get
            Return Me._IdTiendaOrigen
        End Get
        Set(ByVal value As Integer)
            Me._IdTiendaOrigen = value
        End Set
    End Property

    Public Property IdTiendaDestino() As Integer
        Get
            Return Me._IdTiendaDestino
        End Get
        Set(ByVal value As Integer)
            Me._IdTiendaDestino = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property CostoxUnd() As Decimal
        Get
            Return Me._cfu_costoxUnd
        End Get
        Set(ByVal value As Decimal)
            Me._cfu_costoxUnd = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property Producto() As String
        Get
            Return Me._Producto
        End Get
        Set(ByVal value As String)
            Me._Producto = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property

    Public Property UnidadMedida() As String
        Get
            Return Me._UnidadMedida
        End Get
        Set(ByVal value As String)
            Me._UnidadMedida = value
        End Set
    End Property

    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property

    Public Property Linea() As String
        Get
            Return Me._Linea
        End Get
        Set(ByVal value As String)
            Me._Linea = value
        End Set
    End Property

    Public Property SubLinea() As String
        Get
            Return Me._SubLinea
        End Get
        Set(ByVal value As String)
            Me._SubLinea = value
        End Set
    End Property

    Public ReadOnly Property getListaUnidadMedida() As List(Of Entidades.UnidadMedida)
        Get

            Dim lista As New List(Of Entidades.UnidadMedida)

            Dim lista_IdUnidadMedida() As String = Cadena_IdUnidadMedida_Prod.Split(",")
            Dim lista_UnidadMedida() As String = Cadena_UnidadMedida_Prod.Split(",")

            For i As Integer = 0 To lista_IdUnidadMedida.Length - 1

                Dim objUnidadMedida As New Entidades.UnidadMedida
                With objUnidadMedida

                    .DescripcionCorto = lista_UnidadMedida(i)
                    .Id = CInt(lista_IdUnidadMedida(i))

                End With
                lista.Add(objUnidadMedida)

            Next

            Return lista

        End Get
    End Property

End Class

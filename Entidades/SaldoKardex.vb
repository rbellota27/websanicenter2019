'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class SaldoKardex
    Private _IdTienda As Integer
    Private _IdAlmacen As Integer
    Private _IdMovAlmacen As Integer
    Private _IdEmpresa As Integer
    Private _IdProducto As Integer
    Private _IdSaldoKardex As Integer
    Private _sk_CantidadSaldo As Decimal
    Private _sk_CostoSaldo As Decimal
    Private _sk_TotalSaldo As Decimal
    Private _IdDocumento As Integer

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property IdSaldoKardex() As Integer
        Get
            Return Me._IdSaldoKardex
        End Get
        Set(ByVal value As Integer)
            Me._IdSaldoKardex = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property TotalSaldo() As Decimal
        Get
            Return Me._sk_TotalSaldo
        End Get
        Set(ByVal value As Decimal)
            Me._sk_TotalSaldo = value
        End Set
    End Property


    Public Property CantidadSaldo() As Decimal
        Get
            Return Me._sk_CantidadSaldo
        End Get
        Set(ByVal value As Decimal)
            Me._sk_CantidadSaldo = value
        End Set
    End Property

    Public Property CostoSaldo() As Decimal
        Get
            Return Me._sk_CostoSaldo
        End Get
        Set(ByVal value As Decimal)
            Me._sk_CostoSaldo = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property

    Public Property IdMovAlmacen() As Integer
        Get
            Return Me._IdMovAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdMovAlmacen = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property
End Class

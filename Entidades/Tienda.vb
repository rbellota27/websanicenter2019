'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Tienda
    Private _IdTienda As Integer
    Private _tie_Nombre As String
    Private _tie_Ubigeo As String
    Private _tie_Direccion As String
    Private _tie_Referencia As String
    Private _tie_Telefono As String
    Private _tie_Fax As String
    Private _tie_Estado As String
    Private _IdTipoTienda As Integer
    Private _Dpto As String
    Private _Prov As String
    Private _Dist As String
    Private _TipoTienda As String
    Private _CtaDebeVenta As String
    Private _CtaHaberVenta As String
    Private _Email As String
    Private _obj As Integer
    Private _mes As String
    Private _anio As String
    Private _idescala As Integer
    Private _nomescala As String
    Private _resultado As String
    Private _idobjetivo As Integer
    Private _tie_Nombrex As String
    Private _IdTiendax As Integer
    Public Property IdTienda() As Integer
        Get
            Return _IdTiendax
        End Get
        Set(ByVal value As Integer)
            _IdTiendax = value
        End Set
    End Property
    Public Property tie_Nombre() As String
        Get
            Return _tie_Nombrex
        End Get
        Set(ByVal value As String)
            _tie_Nombrex = value
        End Set
    End Property
    Public Property IdObjetivo() As Integer
        Get
            Return _idobjetivo
        End Get
        Set(ByVal value As Integer)
            _idobjetivo = value
        End Set
    End Property
    Public Property ResultadoEscala() As String
        Get
            Return _resultado
        End Get
        Set(ByVal value As String)
            _resultado = value
        End Set
    End Property

    Public Property Mes() As String
        Get
            Return _mes
        End Get
        Set(ByVal value As String)
            _mes = value
        End Set
    End Property
    Public Property Anio() As String
        Get
            Return _anio
        End Get
        Set(ByVal value As String)
            _anio = value
        End Set
    End Property
    Public Property NombreEscala() As String
        Get
            Return _nomescala
        End Get
        Set(ByVal value As String)
            _nomescala = value
        End Set
    End Property
    Public Property IdEscala() As Integer
        Get
            Return _idescala
        End Get
        Set(ByVal value As Integer)
            _idescala = value
        End Set
    End Property


    Public Sub New()
    End Sub

    Public Sub New(ByVal idtienda As Integer, ByVal nombre As String)
        Me.Id = idtienda
        Me.Nombre = nombre
    End Sub
    Public Property ObjetivoxTienda() As Integer
        Get
            Return _obj
        End Get
        Set(ByVal value As Integer)
            _obj = value
        End Set
    End Property


    Public Property CtaHaberVenta() As String
        Get
            Return _CtaHaberVenta
        End Get
        Set(ByVal value As String)
            _CtaHaberVenta = value
        End Set
    End Property

    Public Property CtaDebeVenta() As String
        Get
            Return _CtaDebeVenta
        End Get
        Set(ByVal value As String)
            _CtaDebeVenta = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
        End Set
    End Property


    Public Property Id() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._tie_Nombre
        End Get
        Set(ByVal value As String)
            Me._tie_Nombre = value
        End Set
    End Property

    Public Property Ubigeo() As String
        Get
            Return Me._tie_Ubigeo
        End Get
        Set(ByVal value As String)
            Me._tie_Ubigeo = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return Me._tie_Direccion
        End Get
        Set(ByVal value As String)
            Me._tie_Direccion = value
        End Set
    End Property

    Public Property Referencia() As String
        Get
            Return Me._tie_Referencia
        End Get
        Set(ByVal value As String)
            Me._tie_Referencia = value
        End Set
    End Property

    Public Property Telefono() As String
        Get
            Return Me._tie_Telefono
        End Get
        Set(ByVal value As String)
            Me._tie_Telefono = value
        End Set
    End Property

    Public Property Fax() As String
        Get
            Return Me._tie_Fax
        End Get
        Set(ByVal value As String)
            Me._tie_Fax = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._tie_Estado
        End Get
        Set(ByVal value As String)
            Me._tie_Estado = value
        End Set
    End Property

    Public Property IdTipoTienda() As Integer
        Get
            Return Me._IdTipoTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoTienda = value
        End Set
    End Property
    Public Property Dpto() As String
        Get
            Return _Dpto
        End Get
        Set(ByVal value As String)
            Me._Dpto = value
        End Set
    End Property
    Public Property Prov() As String
        Get
            Return _Prov
        End Get
        Set(ByVal value As String)
            _Prov = value
        End Set
    End Property
    Public Property Dist() As String
        Get
            Return _Dist
        End Get
        Set(ByVal value As String)
            _Dist = value
        End Set
    End Property
    Public Property TipoTienda() As String
        Get
            Return _TipoTienda
        End Get
        Set(ByVal value As String)
            _TipoTienda = value
        End Set
    End Property
End Class

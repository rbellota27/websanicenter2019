'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Ocupacion
    Private _IdOcupacion As Integer
    Private _ocu_Nombre As String
    Private _ocu_Estado As String
    Private _IdGiro As Integer
    Public Property Id() As Integer
        Get
            Return Me._IdOcupacion
        End Get
        Set(ByVal value As Integer)
            Me._IdOcupacion = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._ocu_Nombre
        End Get
        Set(ByVal value As String)
            Me._ocu_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._ocu_Estado
        End Get
        Set(ByVal value As String)
            Me._ocu_Estado = value
        End Set
    End Property

    Public Property IdGiro() As Integer
        Get
            Return Me._IdGiro
        End Get
        Set(ByVal value As Integer)
            Me._IdGiro = value
        End Set
    End Property
End Class

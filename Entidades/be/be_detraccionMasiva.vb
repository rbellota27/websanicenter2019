﻿Public Class be_detraccionMasiva

    Private _IdDocumento As Integer = 0

    Public Property IdDocumento() As String
        Get
            Return _IdDocumento
        End Get
        Set(value As String)
            _IdDocumento = value
        End Set
    End Property
    Private _doc_FechaEmision As Date = Date.Today

    Public Property doc_FechaEmision() As String
        Get
            Return _doc_FechaEmision
        End Get
        Set(value As String)
            _doc_FechaEmision = value
        End Set
    End Property
    Private _doc_NroVoucherConta As String = String.Empty

    Public Property doc_NroVoucherConta() As String
        Get
            Return _doc_NroVoucherConta
        End Get
        Set(value As String)
            _doc_NroVoucherConta = value
        End Set
    End Property
    Private _Ruc As String = String.Empty
    Public Property Ruc() As String
        Get
            Return _Ruc
        End Get
        Set(value As String)
            _Ruc = value
        End Set
    End Property
    Private _NombreProveedor As String = String.Empty
    Public Property NombreProveedor() As String
        Get
            Return _NombreProveedor
        End Get
        Set(value As String)
            _NombreProveedor = value
        End Set
    End Property
    Private _Serie As String = String.Empty
    Public Property Serie() As String
        Get
            Return _Serie
        End Get
        Set(value As String)
            _Serie = value
        End Set
    End Property
    Private _codigo As String = String.Empty
    Public Property codigo() As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
        End Set
    End Property
    Private _Moneda As String = String.Empty
    Public Property Moneda() As String
        Get
            Return _Moneda
        End Get
        Set(value As String)
            _Moneda = value
        End Set
    End Property
    Private _montoRegimen As Decimal
    Public Property montoRegimen() As String
        Get
            Return _montoRegimen
        End Get
        Set(value As String)
            _montoRegimen = value
        End Set
    End Property
    Private _TC As Decimal
    Public Property TC() As String
        Get
            Return _TC
        End Get
        Set(value As String)
            _TC = value
        End Set
    End Property

    Private _MontoEnSoles As Decimal

    Public Property MontoEnSoles() As String
        Get
            Return _MontoEnSoles
        End Get
        Set(value As String)
            _MontoEnSoles = value
        End Set
    End Property
    Private _can_nroOperacion As String = String.Empty
    Public Property can_nroOperacion() As String
        Get
            Return _can_nroOperacion
        End Get
        Set(value As String)
            _can_nroOperacion = value
        End Set
    End Property
    Private _can_montoCancelado As Decimal

    Public Property can_montoCancelado() As String
        Get
            Return _can_montoCancelado
        End Get
        Set(value As String)
            _can_montoCancelado = value
        End Set
    End Property

    Private _NumeroCuenta As String = String.Empty
    Public Property NumeroCuenta() As String
        Get
            Return _NumeroCuenta
        End Get
        Set(value As String)
            _NumeroCuenta = value
        End Set
    End Property


    Private _reg_Nombre As String = String.Empty
    Public Property reg_Nombre() As String
        Get
            Return _reg_Nombre
        End Get
        Set(value As String)
            _reg_Nombre = value
        End Set
    End Property

    Private _reg_Id As Integer = 0

    Public Property reg_Id() As String
        Get
            Return _reg_Id
        End Get
        Set(value As String)
            _reg_Id = value
        End Set
    End Property

    Private _can_fechaCancelacion As String = String.Empty
    Public Property can_fechaCancelacion() As String
        Get
            Return _can_fechaCancelacion
        End Get
        Set(value As String)
            _can_fechaCancelacion = value
        End Set
    End Property
    Private _ban_Abv As String = String.Empty

    Public Property ban_Abv() As String
        Get
            Return _ban_Abv
        End Get
        Set(value As String)
            _ban_Abv = value
        End Set
    End Property
    Private _IdPersona As Integer = 0

    Public Property IdPersona() As String
        Get
            Return _IdPersona
        End Get
        Set(value As String)
            _IdPersona = value
        End Set
    End Property
    Private _IdCuenta As Integer = 0
    Public Property IdCuenta() As String
        Get
            Return _IdCuenta
        End Get
        Set(value As String)
            _IdCuenta = value
        End Set
    End Property

    Private _IdTipoOperacion As Integer = 0
    Public Property IdTipoOperacion() As String
        Get
            Return _IdTipoOperacion
        End Get
        Set(value As String)
            _IdTipoOperacion = value
        End Set
    End Property

    Private _IdConcepto As Integer = 0
    Public Property IdConcepto() As String
        Get
            Return _IdConcepto
        End Get
        Set(value As String)
            _IdConcepto = value
        End Set
    End Property



End Class

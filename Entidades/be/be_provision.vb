﻿Public Class be_provision
    Private _ruc As String = String.Empty
    Private _razonSocial As String = String.Empty
    Private _numero As String = String.Empty
    Private _fecha As Date = Date.Today
    Private _tc As Decimal = 0
    Private _concepto As String = String.Empty
    Private _detalle As String = String.Empty
    Private _periodo As String = String.Empty
    Private _voucher As String = String.Empty
    Private _detraccion As Decimal = 0
    Private _idDocumento As Decimal = 0

    Public Property ruc() As String
        Get
            Return _ruc
        End Get
        Set(value As String)
            _ruc = value
        End Set
    End Property

    Public Property razonSocial() As String
        Get
            Return _razonSocial
        End Get
        Set(value As String)
            _razonSocial = value
        End Set
    End Property

    Public Property numero() As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property fecha() As String
        Get
            Return _fecha
        End Get
        Set(value As String)
            _fecha = value
        End Set
    End Property

    Public Property tc() As String
        Get
            Return _tc
        End Get
        Set(value As String)
            _tc = value
        End Set
    End Property

    Public Property concepto() As String
        Get
            Return _concepto
        End Get
        Set(value As String)
            _concepto = value
        End Set
    End Property

    Public Property detalle() As String
        Get
            Return _detalle
        End Get
        Set(value As String)
            _detalle = value
        End Set
    End Property

    Public Property periodo() As String
        Get
            Return _periodo
        End Get
        Set(value As String)
            _periodo = value
        End Set
    End Property

    Public Property voucher() As String
        Get
            Return _voucher
        End Get
        Set(value As String)
            _voucher = value
        End Set
    End Property

    Public Property detraccion() As String
        Get
            Return _detraccion
        End Get
        Set(value As String)
            _detraccion = value
        End Set
    End Property

    Public Property idDocumento() As String
        Get
            Return _idDocumento
        End Get
        Set(value As String)
            _idDocumento = value
        End Set
    End Property
End Class

﻿Public Class be_MontoRegimenDet

    '    Public Sub New()
    '    End Sub
    '    Public Sub New(ByVal IdDocumento As Integer, ByVal monto As Decimal, ByVal IdRegimenOperacion As Integer)
    '    Me.IdDocumento = IdDocumento
    '    Me.Monto = monto
    '    Me.ro_Id = IdRegimenOperacion
    '    Me.Id = Id
    '    IdServicio
    '    IdOperacion
    '    Serie
    '    Codigo
    '    MontoDetraccion
    '    EstadoDet
    '    IDDocumentoGenera
    '    End Sub

    Private _IdDocumento As Integer
    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Private _mr_Monto As Decimal
    Public Property Monto() As Decimal
        Get
            Return Me._mr_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._mr_Monto = value
        End Set
    End Property


    Private _ro_Id As Integer
    Public Property ro_Id() As Integer
        Get
            Return Me._ro_Id
        End Get
        Set(ByVal value As Integer)
            Me._ro_Id = value
        End Set
    End Property

    Private _IdServicio As Integer
    Public Property IdServicio() As Integer
        Get
            Return Me._IdServicio
        End Get
        Set(ByVal value As Integer)
            Me._IdServicio = value
        End Set
    End Property

    Private _IdOperacion As Integer
    Public Property IdOperacion() As Integer
        Get
            Return Me._IdOperacion
        End Get
        Set(ByVal value As Integer)
            Me._IdOperacion = value
        End Set
    End Property

    Private _Serie As String
    Public Property Serie() As String
        Get
            Return Me._Serie
        End Get
        Set(ByVal value As String)
            Me._Serie = value
        End Set
    End Property

    Private _Codigo As Integer
    Public Property Codigo() As String
        Get
            Return Me._Codigo
        End Get
        Set(ByVal value As String)
            Me._Codigo = value
        End Set
    End Property

    Private _MontoDetraccion As Integer
    Public Property MontoDetraccion() As Decimal
        Get
            Return Me._MontoDetraccion
        End Get
        Set(ByVal value As Decimal)
            Me._MontoDetraccion = value
        End Set
    End Property

    Private _EstadoDet As Integer
    Public Property EstadoDet() As Integer
        Get
            Return Me._EstadoDet
        End Get
        Set(ByVal value As Integer)
            Me._EstadoDet = value
        End Set
    End Property

End Class




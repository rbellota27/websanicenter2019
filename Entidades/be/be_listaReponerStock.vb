﻿Public Class be_listaReponerStock
    Private _idProducto As Integer = 0
    Public Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(value As Integer)
            _idProducto = value
        End Set
    End Property
    Private _idUnidadMedida As Integer = 0
    Public Property idUnidadMedida() As Integer
        Get
            Return _idUnidadMedida
        End Get
        Set(value As Integer)
            _idUnidadMedida = value
        End Set
    End Property
    Private _codigo As String = String.Empty
    Public Property codigo() As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
        End Set
    End Property
    Private _descripcion As String = String.Empty
    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property
    Private _semana1 As Decimal = 0
    Public Property s1() As Decimal
        Get
            Return _semana1
        End Get
        Set(value As Decimal)
            _semana1 = value
        End Set
    End Property
    Private _semana2 As Decimal = 0
    Public Property s2() As Decimal
        Get
            Return _semana2
        End Get
        Set(value As Decimal)
            _semana2 = value
        End Set
    End Property
    Private _semana3 As Decimal = 0
    Public Property s3() As Decimal
        Get
            Return _semana3
        End Get
        Set(value As Decimal)
            _semana3 = value
        End Set
    End Property
    Private _semana4 As Decimal = 0
    Public Property s4() As Decimal
        Get
            Return _semana4
        End Get
        Set(value As Decimal)
            _semana4 = value
        End Set
    End Property
    Private _semana5 As Decimal = 0
    Public Property s5() As Decimal
        Get
            Return _semana5
        End Get
        Set(value As Decimal)
            _semana5 = value
        End Set
    End Property
    Private _semana6 As Decimal = 0
    Public Property s6() As Decimal
        Get
            Return _semana6
        End Get
        Set(value As Decimal)
            _semana6 = value
        End Set
    End Property
    Private _semana7 As Decimal = 0
    Public Property s7() As Decimal
        Get
            Return _semana7
        End Get
        Set(value As Decimal)
            _semana7 = value
        End Set
    End Property
    Private _semana8 As Decimal = 0
    Public Property s8() As Decimal
        Get
            Return _semana8
        End Get
        Set(value As Decimal)
            _semana8 = value
        End Set
    End Property
    Private _semana9 As Decimal = 0
    Public Property s9() As Decimal
        Get
            Return _semana9
        End Get
        Set(value As Decimal)
            _semana9 = value
        End Set
    End Property
    Private _semana10 As Decimal = 0
    Public Property s10() As Decimal
        Get
            Return _semana10
        End Get
        Set(value As Decimal)
            _semana10 = value
        End Set
    End Property
    Private _semana11 As Decimal = 0
    Public Property s11() As Decimal
        Get
            Return _semana11
        End Get
        Set(value As Decimal)
            _semana11 = value
        End Set
    End Property
    Private _semana12 As Decimal = 0
    Public Property s12() As Decimal
        Get
            Return _semana12
        End Get
        Set(value As Decimal)
            _semana12 = value
        End Set
    End Property
    Private _semana13 As Decimal = 0
    Public Property s13() As Decimal
        Get
            Return _semana13
        End Get
        Set(value As Decimal)
            _semana13 = value
        End Set
    End Property
    Private _semana14 As Decimal = 0
    Public Property s14() As Decimal
        Get
            Return _semana14
        End Get
        Set(value As Decimal)
            _semana14 = value
        End Set
    End Property
    Private _semana15 As Decimal = 0
    Public Property s15() As Decimal
        Get
            Return _semana15
        End Get
        Set(value As Decimal)
            _semana15 = value
        End Set
    End Property
    'Private _semana16 As Decimal = 0
    'Public Property semana16() As Decimal
    '    Get
    '        Return _semana16
    '    End Get
    '    Set(value As Decimal)
    '        _semana16 = value
    '    End Set
    'End Property
    'Private _semana17 As Decimal = 0
    'Public Property semana17() As Decimal
    '    Get
    '        Return _semana17
    '    End Get
    '    Set(value As Decimal)
    '        _semana17 = value
    '    End Set
    'End Property
    Public _promedioVenta As Decimal = 0
    Public Property promVenta() As Decimal
        Get
            Return _promedioVenta
        End Get
        Set(value As Decimal)
            _promedioVenta = value
        End Set
    End Property
    Public _almacenTienda As Decimal = 0
    Public Property almacenTienda() As Decimal
        Get
            Return _almacenTienda
        End Get
        Set(value As Decimal)
            _almacenTienda = value
        End Set
    End Property
    Public _almacenComparativo As Decimal = 0
    Public Property almacenCentral() As Decimal
        Get
            Return _almacenComparativo
        End Get
        Set(value As Decimal)
            _almacenComparativo = value
        End Set
    End Property
    Public _cobertura As String = String.Empty
    Public Property cobertura() As String
        Get
            Return _cobertura
        End Get
        Set(ByVal value As String)
            _cobertura = value
        End Set
    End Property
    Public _max As Decimal = 0
    Public Property max() As Decimal
        Get
            Return _max
        End Get
        Set(value As Decimal)
            _max = value
        End Set
    End Property
    Public _min As Decimal = 0
    Public Property min() As Decimal
        Get
            Return _min
        End Get
        Set(ByVal value As Decimal)
            _min = value
        End Set
    End Property
    Public _reposicion As String = String.Empty
    Public Property reposicion() As String
        Get
            Return _reposicion
        End Get
        Set(value As String)
            _reposicion = value
        End Set
    End Property
    Public _multiplo As Decimal
    Public Property multiplo() As Decimal
        Get
            Return _multiplo
        End Get
        Set(ByVal value As Decimal)
            _multiplo = value
        End Set
    End Property

    Public _linea As String = String.Empty
    Public Property linea() As String
        Get
            Return _linea
        End Get
        Set(ByVal value As String)
            _linea = value
        End Set
    End Property

    Public _sublinea As String = String.Empty
    Public Property sublinea() As String
        Get
            Return _sublinea
        End Get
        Set(ByVal value As String)
            _sublinea = value
        End Set
    End Property

    Public _pais As String = String.Empty
    Public Property pais() As String
        Get
            Return _pais
        End Get
        Set(ByVal value As String)
            _pais = value
        End Set
    End Property


End Class


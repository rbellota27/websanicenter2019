﻿Public Class be_RequerimientoProgramados
   
    Private _Marcar As Integer = 0
    Public Property Marcar() As Integer
        Get
            Return _Marcar
        End Get
        Set(value As Integer)
            _Marcar = value
        End Set
    End Property

    Private _NroDocumento As String = String.Empty
    Public Property NroDocumento() As String
        Get
            Return _NroDocumento
        End Get
        Set(ByVal value As String)
            _NroDocumento = value
        End Set
    End Property
    Private _Proveedor As String = String.Empty
    Public Property Proveedor() As String
        Get
            Return _Proveedor
        End Get
        Set(ByVal value As String)
            _Proveedor = value
        End Set
    End Property
    Private _FechaPago As Date = Date.Today

    Public Property FechaPago() As String
        Get
            Return _FechaPago
        End Get
        Set(value As String)
            _FechaPago = value
        End Set
    End Property
    Private _Simbolo As String = String.Empty
    Public Property Simbolo() As String
        Get
            Return _Simbolo
        End Get
        Set(ByVal value As String)
            _Simbolo = value
        End Set
    End Property

    Private _mcp_Monto As Decimal = 0
    Public Property mcp_Monto() As Decimal
        Get
            Return _mcp_Monto
        End Get
        Set(ByVal value As Decimal)
            _mcp_Monto = value
        End Set
    End Property

    Private _mcp_Saldo As Decimal = 0
    Public Property mcp_Saldo() As Decimal
        Get
            Return _mcp_Saldo
        End Get
        Set(ByVal value As Decimal)
            _mcp_Saldo = value
        End Set
    End Property
    Private _doc_TotalApagar As Decimal = 0
    Public Property doc_TotalApagar() As Decimal
        Get
            Return _doc_TotalApagar
        End Get
        Set(ByVal value As Decimal)
            _doc_TotalApagar = value
        End Set
    End Property
    Private _doc_FechaVenc As Date = Date.Today

    Public Property doc_FechaVenc() As String
        Get
            Return _doc_FechaVenc
        End Get
        Set(value As String)
            _doc_FechaVenc = value
        End Set
    End Property
    'Private _IdDetalleConcepto As Integer = 0
    'Public Property IdDetalleConcepto() As Integer
    '    Get
    '        Return _IdDetalleConcepto
    '    End Get
    '    Set(value As Integer)
    '        _IdDetalleConcepto = value
    '    End Set
    'End Property
    Private _IdMoneda As Integer = 0
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(value As Integer)
            _IdMoneda = value
        End Set
    End Property
    Private _Tienda As String = String.Empty
    Public Property Tienda() As String
        Get
            Return _Tienda
        End Get
        Set(ByVal value As String)
            _Tienda = value
        End Set
    End Property

    Private _IdTienda As Integer = 0
    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(value As Integer)
            _IdTienda = value
        End Set
    End Property

    Private _IdTipoDocumento As Integer = 0
    Public Property IdTipoDocumento() As Integer
        Get
            Return _IdTipoDocumento
        End Get
        Set(value As Integer)
            _IdTipoDocumento = value
        End Set
    End Property
    Private _iddocumento As Integer = 0
    Public Property iddocumento() As Integer
        Get
            Return _iddocumento
        End Get
        Set(value As Integer)
            _iddocumento = value
        End Set
    End Property


    

End Class

﻿Public Class be_asientoContable
    Private _idProgramacion As Integer = 0
    Public Property idProgramacion() As Integer
        Get
            Return _idProgramacion
        End Get
        Set(value As Integer)
            _idProgramacion = value
        End Set
    End Property

    Private _idEmpresa As Integer = 0
    Public Property idEmpresa() As Integer
        Get
            Return _idEmpresa
        End Get
        Set(value As Integer)
            _idEmpresa = value
        End Set
    End Property

    Private _fechaEmision As Date
    Public Property fechaEmision() As Date
        Get
            Return _fechaEmision
        End Get
        Set(value As Date)
            _fechaEmision = value
        End Set
    End Property

    Private _idTipoDocumento As Integer = 0
    Public Property idTipoDocumento() As Integer
        Get
            Return _idTipoDocumento
        End Get
        Set(value As Integer)
            _idTipoDocumento = value
        End Set
    End Property

    Private _usuarioCreacion As String = String.Empty
    Public Property usuarioCreacion() As String
        Get
            Return _usuarioCreacion
        End Get
        Set(value As String)
            _usuarioCreacion = value
        End Set
    End Property

    Private _serieDoc As String = String.Empty
    Public Property serieDoc() As String
        Get
            Return _serieDoc
        End Get
        Set(value As String)
            _serieDoc = value
        End Set
    End Property

    Private _nroDoc As String = String.Empty
    Public Property nroDoc() As String
        Get
            Return _nroDoc
        End Get
        Set(value As String)
            _nroDoc = value
        End Set
    End Property

    Private _idBanco As Integer = 0
    Public Property idBanco() As Integer
        Get
            Return _idBanco
        End Get
        Set(value As Integer)
            _idBanco = value
        End Set
    End Property

    Private _idMoneda As Integer = 0
    Public Property idMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(value As Integer)
            _idMoneda = value
        End Set
    End Property

    Private _ctaCancelacion As String = String.Empty
    Public Property ctaCancelacion() As String
        Get
            Return _ctaCancelacion
        End Get
        Set(value As String)
            _ctaCancelacion = value
        End Set
    End Property

    Private _glosa As String = String.Empty
    Public Property glosa() As String
        Get
            Return _glosa
        End Get
        Set(value As String)
            _glosa = value
        End Set
    End Property

    Private _idproveedor As Integer = 0
    Public Property idproveedor() As Integer
        Get
            Return _idproveedor
        End Get
        Set(value As Integer)
            _idproveedor = value
        End Set
    End Property

    Private _importeAPagar As Decimal = 0
    Public Property importeAPagar() As Decimal
        Get
            Return _importeAPagar
        End Get
        Set(value As Decimal)
            _importeAPagar = value
        End Set
    End Property

    Private _idDocumentoxCancelar As Integer = 0
    Public Property idDocumentoxCancelar() As Integer
        Get
            Return _idDocumentoxCancelar
        End Get
        Set(value As Integer)
            _idDocumentoxCancelar = value
        End Set
    End Property
End Class

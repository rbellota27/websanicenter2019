﻿Public Class be_comision

    Private _idComisionista As Integer = 0
    Public Property idComisionista() As Integer
        Get
            Return _idComisionista
        End Get
        Set(value As Integer)
            _idComisionista = value
        End Set
    End Property

    Private _idTienda As Integer = 0
    Public Property idTienda() As Integer
        Get
            Return _idTienda
        End Get
        Set(value As Integer)
            _idTienda = value
        End Set
    End Property

    Private _nomComisionista As String = String.Empty
    Public Property nomComisionista() As String
        Get
            Return _nomComisionista
        End Get
        Set(value As String)
            _nomComisionista = value
        End Set
    End Property

    Private _nomtienda As String = String.Empty
    Public Property nomtienda() As String
        Get
            Return _nomtienda
        End Get
        Set(value As String)
            _nomtienda = value
        End Set
    End Property

    Private _primerPorcPago As Decimal = 0
    Public Property primerPorcPago() As Decimal
        Get
            Return _primerPorcPago
        End Get
        Set(value As Decimal)
            _primerPorcPago = value
        End Set
    End Property

    Private _segundoPorcPago As Decimal = 0
    Public Property segundoPorcPago() As Decimal
        Get
            Return _segundoPorcPago
        End Get
        Set(value As Decimal)
            _segundoPorcPago = value
        End Set
    End Property

    Private _subTotalPagoComision As Decimal = 0
    Public Property subTotalPagoComision() As Decimal
        Get
            Return _subTotalPagoComision
        End Get
        Set(value As Decimal)
            _subTotalPagoComision = value
        End Set
    End Property

    Private _totalComisionPremium As Decimal = 0
    Public Property totalComisionPremium() As Decimal
        Get
            Return _totalComisionPremium
        End Get
        Set(value As Decimal)
            _totalComisionPremium = value
        End Set
    End Property

    Private _Pagoadicional As Decimal = 0
    Public Property Pagoadicional() As Decimal
        Get
            Return _Pagoadicional
        End Get
        Set(value As Decimal)
            _Pagoadicional = value
        End Set
    End Property

    Private _pagoTotalComision As Decimal = 0
    Public Property pagoTotalComision() As Decimal
        Get
            Return _pagoTotalComision
        End Get
        Set(value As Decimal)
            _pagoTotalComision = value
        End Set
    End Property
End Class

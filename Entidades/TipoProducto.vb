﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoProducto
    Private _IdTipoProducto As Integer
    Private _tp_Nombre As String
    Private _tp_Abv As String
    Private _tp_Estado As String
    Private _descEstado As String

    Public Property Id() As Integer
        Get
            Return Me._IdTipoProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoProducto = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._tp_Nombre
        End Get
        Set(ByVal value As String)
            Me._tp_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._tp_Abv
        End Get
        Set(ByVal value As String)
            Me._tp_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._tp_Estado
        End Get
        Set(ByVal value As String)
            Me._tp_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._tp_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
End Class

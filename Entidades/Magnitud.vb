'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 22-Oct-2009
'Hora    : 10:02 am
'*************************************************
Public Class Magnitud
    Private _IdMagnitud As Integer
    Private _mag_Nombre As String
    Private _mag_Abv As String
    Private _mag_Estado As String
    Private _descEstado As String
    Private _magUnidad_Equivalencia
    Private _UnidadMedida_Nombre

    Private _listaUM As List(Of Entidades.UnidadMedida)

    Private _Escalar As Decimal

    Private _IdUnidadMedida As Integer
    Private _pm_principal As Boolean

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property Escalar() As Decimal
        Get
            Return Me._Escalar
        End Get
        Set(ByVal value As Decimal)
            Me._Escalar = value
        End Set
    End Property


    Public Property ListaUM() As List(Of Entidades.UnidadMedida)
        Get
            Return Me._listaUM
        End Get
        Set(ByVal value As List(Of Entidades.UnidadMedida))
            Me._listaUM = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._mag_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdMagnitud
        End Get
        Set(ByVal value As Integer)
            Me._IdMagnitud = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._mag_Nombre
        End Get
        Set(ByVal value As String)
            Me._mag_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._mag_Abv
        End Get
        Set(ByVal value As String)
            Me._mag_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._mag_Estado
        End Get
        Set(ByVal value As String)
            Me._mag_Estado = value
        End Set
    End Property
    Public Property Equivalencia() As Decimal
        Get
            Return Me._magUnidad_Equivalencia
        End Get
        Set(ByVal value As Decimal)
            Me._magUnidad_Equivalencia = value
        End Set
    End Property
    Public Property NombreUnidadMedida() As String
        Get
            Return Me._UnidadMedida_Nombre
        End Get
        Set(ByVal value As String)
            Me._UnidadMedida_Nombre = value
        End Set
    End Property
    Public Property pm_principal() As Boolean
        Get
            Return _pm_principal
        End Get
        Set(ByVal value As Boolean)
            _pm_principal = value
        End Set
    End Property
End Class

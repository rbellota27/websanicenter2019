'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Retazo
    Private _IdProducto As Integer
    Private _IdRetazo As Integer
    Private _ret_Escalar As Decimal
    Private _IdUnidadMedida As Integer
    Private _ret_Estado As String

    Public Property Estado() As String
        Get
            Return Me._ret_Estado
        End Get
        Set(ByVal value As String)
            Me._ret_Estado = value
        End Set
    End Property


    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdRetazo() As Integer
        Get
            Return Me._IdRetazo
        End Get
        Set(ByVal value As Integer)
            Me._IdRetazo = value
        End Set
    End Property

    Public Property Escalar() As Decimal
        Get
            Return Me._ret_Escalar
        End Get
        Set(ByVal value As Decimal)
            Me._ret_Escalar = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Propietario
    Dim _IdPersona As Integer
    Dim _per_NComercial As String

    Public Sub New()
    End Sub
    Public Sub New(ByVal idpersona As Integer, ByVal nomComercial As String)
        Me.Id = idpersona
        Me.NombreComercial = nomComercial
    End Sub
    Public Property Id() As Integer
        Get
            Return _IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property
    Public Property NombreComercial() As String
        Get
            Return Me._per_NComercial
        End Get
        Set(ByVal value As String)
            Me._per_NComercial = value
        End Set
    End Property
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ProductoMedidaView
    Private _IdProducto As Integer
    Private _IdUnidadMedida As Integer
    Private _IdMagnitud As Integer
    Private _pm_Escalar As Decimal
    Private _mag_Nombre As String
    Private _um_NombreCorto As String
    Private _pm_principal As Boolean
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property IdMagnitud() As Integer
        Get
            Return Me._IdMagnitud
        End Get
        Set(ByVal value As Integer)
            Me._IdMagnitud = value
        End Set
    End Property

    Public Property Escalar() As Decimal
        Get
            Return Me._pm_Escalar
        End Get
        Set(ByVal value As Decimal)
            Me._pm_Escalar = value
        End Set
    End Property

    Public Property NombreMagnitud() As String
        Get
            Return Me._mag_Nombre
        End Get
        Set(ByVal value As String)
            Me._mag_Nombre = value
        End Set
    End Property

    Public Property NombreCortoUMedida() As String
        Get
            Return Me._um_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._um_NombreCorto = value
        End Set
    End Property
    Public Property pm_principal() As Boolean
        Get
            Return _pm_principal
        End Get
        Set(ByVal value As Boolean)
            _pm_principal = value
        End Set
    End Property
End Class

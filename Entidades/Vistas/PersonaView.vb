﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'************  VIERNES 04 06 2010

Public Class PersonaView

    Private _IdPersona As Integer
    Private _per_NComercial As String
    Private _jur_Rsocial As String
    Private _Nombre As String
    Private _Dni As String
    Private _Ruc As String
    Private _dir_Direccion As String
    Private _dir_Ubigeo As String
    Private _dir_Referencia As String
    Private _tel_Numero As String
    Private _tel_Numero_Movil As String
    Private _per_estado As String
    Private _IdTipoPV As Integer
    Private _Propietario As Boolean
    Private _PorcentPercepcion As Decimal
    Private _PorcentRetencion As Decimal

    Private _departamento As String
    Private _provincia As String
    Private _distrito As String

    Private _ApPaterno As String
    Private _ApMaterno As String
    Private _Nombres As String
    Private _IdTipoAgente As Integer
    Private _TipoPersona As Integer  '**** 1: Natural     2: Juridica
    Private _Descripcion As String
    Private _Rol As String
    Private _IdRol As Integer

    Private _login As String

    Private _Correo As String
    Private _PorcentTipoAgente As Decimal
    Private _NroLicencia As String
    Private _TipoPV As String

    Private _SujetoAPercepcion As Boolean
    Private _SujetoARetencion As Boolean
    Private _FechaNac As Date
    Private _EMail As String
    Private _Nacionalidad As String
    Private _idAño As Integer
    Private _año As Integer

    Public Property FechaNac() As Date
        Get
            Return Me._FechaNac
        End Get
        Set(ByVal value As Date)
            Me._FechaNac = value
        End Set
    End Property


    Public Property EMail() As String
        Get
            Return Me._EMail
        End Get
        Set(ByVal value As String)
            Me._EMail = value
        End Set
    End Property



    Public Property Rol() As String
        Get
            Return Me._Rol
        End Get
        Set(ByVal value As String)
            Me._Rol = value
        End Set
    End Property


    Public Property Nacionalidad() As String
        Get
            Return _Nacionalidad
        End Get
        Set(ByVal value As String)
            _Nacionalidad = value
        End Set
    End Property

    Public Property SujetoAPercepcion() As Boolean
        Get
            Return Me._SujetoAPercepcion
        End Get
        Set(ByVal value As Boolean)
            Me._SujetoAPercepcion = value
        End Set
    End Property

    Public Property SujetoARetencion() As Boolean
        Get
            Return Me._SujetoARetencion
        End Get
        Set(ByVal value As Boolean)
            Me._SujetoARetencion = value
        End Set
    End Property

    Public Sub New()
    End Sub
    Public Sub New(ByVal IdAnio As Integer, ByVal anio As String)
        Me.IdAño = IdAnio
        Me.Año = anio
    End Sub


    Public Sub New(ByVal IdPersona As Integer, ByVal descripcion As String, ByVal ruc As String, ByVal dni As String)
        Me.IdPersona = IdPersona
        Me.RazonSocial = descripcion
        Me.Ruc = ruc
        Me.Dni = dni
    End Sub



    Public Property TipoPV() As String
        Get
            Return Me._TipoPV
        End Get
        Set(ByVal value As String)
            Me._TipoPV = value
        End Set
    End Property



    Public Property NroLicencia() As String
        Get
            Return Me._NroLicencia
        End Get
        Set(ByVal value As String)
            Me._NroLicencia = value
        End Set
    End Property


    Public Property PorcentTipoAgente() As Decimal
        Get
            Return Me._PorcentTipoAgente
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentTipoAgente = value
        End Set
    End Property

    Public Property Correo() As String
        Get
            Return _Correo
        End Get
        Set(ByVal value As String)
            _Correo = value
        End Set
    End Property


    Public Property Login() As String
        Get
            Return _login
        End Get
        Set(ByVal value As String)
            _login = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._Descripcion
        End Get
        Set(ByVal value As String)
            Me._Descripcion = value
        End Set
    End Property

    Public Property TipoPersona() As Integer
        Get
            Return Me._TipoPersona
        End Get
        Set(ByVal value As Integer)
            Me._TipoPersona = value
        End Set
    End Property

    Public Property IdAño() As Integer
        Get
            Return Me._idAño
        End Get
        Set(ByVal value As Integer)
            Me._idAño = value
        End Set
    End Property
    Public Property Año() As Integer
        Get
            Return Me._año
        End Get
        Set(ByVal value As Integer)
            Me._año = value
        End Set
    End Property

    Public Property IdRol() As Integer
        Get
            Return Me._IdRol
        End Get
        Set(ByVal value As Integer)
            Me._IdRol = value
        End Set
    End Property

    Public Property IdTipoAgente() As Integer
        Get
            Return Me._IdTipoAgente
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoAgente = value
        End Set
    End Property



    Public Property ApPaterno() As String
        Get
            Return Me._ApPaterno
        End Get
        Set(ByVal value As String)
            Me._ApPaterno = value
        End Set
    End Property

    Public Property ApMaterno() As String
        Get
            Return Me._ApMaterno
        End Get
        Set(ByVal value As String)
            Me._ApMaterno = value
        End Set
    End Property

    Public Property Nombres() As String
        Get
            Return Me._Nombres
        End Get
        Set(ByVal value As String)
            Me._Nombres = value
        End Set
    End Property


    Public Property departamento() As String
        Get
            Return _departamento
        End Get
        Set(ByVal value As String)
            _departamento = value
        End Set
    End Property

    Public Property provincia() As String
        Get
            Return _provincia
        End Get
        Set(ByVal value As String)
            _provincia = value
        End Set
    End Property

    Public Property distrito() As String
        Get
            Return _distrito
        End Get
        Set(ByVal value As String)
            _distrito = value
        End Set
    End Property


    Public Property PorcentPercepcion() As Decimal
        Get
            Return Me._PorcentPercepcion
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentPercepcion = value
        End Set
    End Property

    Public Property PorcentRetencion() As Decimal
        Get
            Return Me._PorcentRetencion
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentRetencion = value
        End Set
    End Property

    Public Property Propietario() As Boolean
        Get
            Return Me._Propietario
        End Get
        Set(ByVal value As Boolean)
            Me._Propietario = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property
    Public Property NombreComercial() As String
        Get
            Return Me._per_NComercial
        End Get
        Set(ByVal value As String)
            Me._per_NComercial = value
        End Set
    End Property
    Public Property RazonSocial() As String
        Get
            Return _jur_Rsocial
        End Get
        Set(ByVal value As String)
            _jur_Rsocial = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property Dni() As String
        Get
            Return _Dni
        End Get
        Set(ByVal value As String)
            _Dni = value
        End Set
    End Property
    Public Property Ruc() As String
        Get
            Return _Ruc
        End Get
        Set(ByVal value As String)
            _Ruc = value
        End Set
    End Property
    Public Property Direccion() As String
        Get
            Return _dir_Direccion
        End Get
        Set(ByVal value As String)
            _dir_Direccion = value
        End Set
    End Property
    Public Property Ubigeo() As String
        Get
            Return _dir_Ubigeo
        End Get
        Set(ByVal value As String)
            _dir_Ubigeo = value
        End Set
    End Property
    Public Property Referencia() As String
        Get
            Return _dir_Referencia
        End Get
        Set(ByVal value As String)
            _dir_Referencia = value
        End Set
    End Property
    Public Property Telefeono() As String
        Get
            Return _tel_Numero
        End Get
        Set(ByVal value As String)
            _tel_Numero = value
        End Set
    End Property

    Public Property TelefeonoMovil() As String
        Get
            Return _tel_Numero_Movil
        End Get
        Set(ByVal value As String)
            _tel_Numero_Movil = value
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return _per_estado
        End Get
        Set(ByVal value As String)
            _per_estado = value
        End Set
    End Property
    Public Property IdTipoPV() As Integer
        Get
            Return _IdTipoPV
        End Get
        Set(ByVal value As Integer)
            _IdTipoPV = value
        End Set
    End Property
    Public ReadOnly Property getNombreParaMostrar() As String
        Get
            If Me.RazonSocial.Trim.Length > 0 Then
                Return Me.RazonSocial
            ElseIf Me.NombreComercial.Trim.Length > 0 Then
                Return Me.NombreComercial
            ElseIf Me.Nombre.Trim.Length > 0 Then
                Return Me.Nombre
            Else
                Return ""
            End If
        End Get
    End Property


End Class

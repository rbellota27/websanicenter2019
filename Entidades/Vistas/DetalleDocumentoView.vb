﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'********** MIERCOLES 10 MARZO 2010 HORA 05_22 PM




Public Class DetalleDocumentoView
    Private _IdProducto As Integer
    Private _dc_Cantidad, _dc_PrecioCD, _dc_Descuento, _PDescuento, _dc_PrecioSD, _dc_Importe, _dc_Utilidad, _StockDisponible As Decimal
    Private _dc_UMedida, _prod_Nombre, _dc_UMedidaPeso As String
    'campos utilizado en el detalle de la Guia de Recepcion
    Private _dc_peso As Decimal
    Private _listaProdUM As List(Of Entidades.ProductoUMView) 'lista de las unidades de medida segun IDProducto
    Private _listaUMedida As List(Of Entidades.UnidadMedida) 'lista de todas las unidades de Medida activo
    '-------
    Private _Kit As Boolean
    Private _dc_IdUMedida As Integer
    Private _dc_IdUMedidaPeso As String
    Private _NomMoneda As String
    Private _IdMoneda As Integer
    Private _Percepcion, _Detraccion As Decimal
    Private _TienePrecioCompra As Integer
    Private _IdDetalleDocumento As Integer
    Private _IdUMPrincipal As Integer
    Private _NomUMPrincipal As String
    Private _IdDocumento As Integer
    Private _serie As String
    Private _Glosa As String
    Private _IdSupervisor As Integer
    Private _codBarraFabricante As String
    Private _idAuxiliar As Integer
    Private _dc_Descuento1, _dc_Descuento2, _dc_Descuento3, _dc_Costo_Imp As Decimal
    Private _Tono As List(Of Entidades.Tono)
    Private _dc_CantxAtender As Decimal
    Private _dc_TasaPercepcion As Decimal
    Private _dc_TasaDetraccion As Decimal
    Private _IdDetalleAfecto As Integer
    Private _DetalleGlosa As String
    Private _PrecioLista As Decimal
    Private _idTono As Integer
    Private _dc_CantidadTransito, _dc_PrecioSinIGV As Decimal
    'Variables para Detalle Informe

    Private _idInforme As Integer
    Private _codigo As String
    Private _nan As Boolean
    Private _CAD As Boolean
    Private _PesoCaja As Decimal
    Private _MCaja As Decimal
    Private _PzaCaja As Decimal
    Private _NroCaja As Decimal
    Private _Pzas As Decimal
    Private _JGO As Decimal
    Private _CalibRe As Boolean
    Private _NPallet As Integer
    Private _Total As Decimal
    Private _Sucursal As String
    Private _TipoEmpaque As String
    Private _EstadoPallet As String
    Private _Rotulado As Boolean
    Private _Observaciones As String


    Public Property dc_Costo_Imp() As Decimal
        Get
            Return _dc_Costo_Imp
        End Get
        Set(ByVal value As Decimal)
            _dc_Costo_Imp = value
        End Set
    End Property

    Public Property codBarraFabricante() As String
        Get
            Return _codBarraFabricante
        End Get
        Set(ByVal value As String)
            _codBarraFabricante = value
        End Set
    End Property


    Public Property dc_Descuento3() As Decimal
        Get
            Return _dc_Descuento3
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento3 = value
        End Set
    End Property

    Public Property dc_Descuento2() As Decimal
        Get
            Return _dc_Descuento2
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento2 = value
        End Set
    End Property

    Public Property dc_CantidadTransito() As Decimal
        Get
            Return _dc_CantidadTransito
        End Get
        Set(ByVal value As Decimal)
            _dc_CantidadTransito = value
        End Set
    End Property

    Public Property dc_Descuento1() As Decimal
        Get
            Return _dc_Descuento1
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento1 = value
        End Set
    End Property

    Private _CodigoProducto As String

    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property


    Public Property dc_PrecioSinIGV() As Decimal
        Get
            Return _dc_PrecioSinIGV
        End Get
        Set(ByVal value As Decimal)
            _dc_PrecioSinIGV = value
        End Set
    End Property

    Public Property idAuxiliar() As Integer
        Get
            Return _idAuxiliar
        End Get
        Set(ByVal value As Integer)
            _idAuxiliar = value
        End Set
    End Property


    Private _IdTienda_PV As Integer

    Public ReadOnly Property HasGlosa() As Boolean
        Get

            If Me.Glosa IsNot Nothing Then

                If Me.Glosa.Trim.Length > 0 Then

                    Return True

                End If

            End If

            Return False

        End Get
    End Property

    Public Property serie() As String
        Get
            Return _serie
        End Get
        Set(ByVal value As String)
            _serie = value
        End Set
    End Property



    Public Property IdTienda_PV() As Integer
        Get
            Return Me._IdTienda_PV
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda_PV = value
        End Set
    End Property




    Public Property Glosa() As String
        Get
            Return Me._Glosa
        End Get
        Set(ByVal value As String)
            Me._Glosa = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
    Public Property IdUMPrincipal() As Integer
        Get
            Return Me._IdUMPrincipal
        End Get
        Set(ByVal value As Integer)
            Me._IdUMPrincipal = value
        End Set
    End Property

    Public Property NomUMPrincipal() As String
        Get
            Return Me._NomUMPrincipal
        End Get
        Set(ByVal value As String)
            Me._NomUMPrincipal = value
        End Set
    End Property

    Public Property IdDetalleDocumento() As Integer
        Get
            Return Me._IdDetalleDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleDocumento = value
        End Set
    End Property
    Public Property TienePrecioCompra() As Integer
        Get
            Return Me._TienePrecioCompra
        End Get
        Set(ByVal value As Integer)
            Me._TienePrecioCompra = value
        End Set
    End Property

    Public Property NomMoneda() As String
        Get
            Return Me._NomMoneda
        End Get
        Set(ByVal value As String)
            Me._NomMoneda = value
        End Set
    End Property

    Public Property IdMoneda() As Integer ' Este Id se usa para la Venta
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property IdUMedida() As Integer
        Get
            Return Me._dc_IdUMedida
        End Get
        Set(ByVal value As Integer)
            Me._dc_IdUMedida = value
        End Set
    End Property

    Public Property IdInforme() As Integer

        Get
            Return Me._idInforme
        End Get
        Set(ByVal value As Integer)
            Me._idInforme = value
        End Set
    End Property



    Public Property IdUMedidaPeso() As String
        Get
            Return Me._dc_IdUMedidaPeso
        End Get
        Set(ByVal value As String)
            Me._dc_IdUMedidaPeso = value
        End Set
    End Property


    Public Property ListaProdUM() As List(Of Entidades.ProductoUMView)
        Get
            Return Me._listaProdUM
        End Get
        Set(ByVal value As List(Of Entidades.ProductoUMView))
            Me._listaProdUM = value
        End Set
    End Property


    Public Property ListaUMedida() As List(Of Entidades.UnidadMedida)
        Get
            Return Me._listaUMedida
        End Get
        Set(ByVal value As List(Of Entidades.UnidadMedida))
            Me._listaUMedida = value
        End Set
    End Property


    Public Property Peso() As Decimal
        Get
            Return _dc_peso
        End Get
        Set(ByVal value As Decimal)
            _dc_peso = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Public Property Cantidad() As Decimal
        Get
            Return _dc_Cantidad
        End Get
        Set(ByVal value As Decimal)
            _dc_Cantidad = value
        End Set
    End Property
    Public Property StockDisponible() As Decimal
        Get
            Return _StockDisponible
        End Get
        Set(ByVal value As Decimal)
            _StockDisponible = value
        End Set
    End Property
    Public Property UM() As String
        Get
            Return _dc_UMedida
        End Get
        Set(ByVal value As String)
            _dc_UMedida = value
        End Set
    End Property
    Public Property UMPeso() As String
        Get
            Return _dc_UMedidaPeso
        End Get
        Set(ByVal value As String)
            _dc_UMedidaPeso = value
        End Set
    End Property
    Public Property CantxAtender() As Decimal
        Get
            Return Me._dc_CantxAtender
        End Get
        Set(ByVal value As Decimal)
            Me._dc_CantxAtender = value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return _prod_Nombre
        End Get
        Set(ByVal value As String)
            _prod_Nombre = value
        End Set
    End Property
    Public Property PrecioSD() As Decimal
        Get
            Return _dc_PrecioSD
        End Get
        Set(ByVal value As Decimal)
            _dc_PrecioSD = value
        End Set
    End Property
    Public Property Descuento() As Decimal
        Get
            Return _dc_Descuento
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento = value
        End Set
    End Property
    Public Property PDescuento() As Decimal
        Get
            If _dc_PrecioSD <> 0 And _PDescuento = 0 Then
                Return (_dc_Descuento / _dc_PrecioSD) * 100
            End If
        End Get
        Set(ByVal value As Decimal)
            _PDescuento = value
        End Set
    End Property

    Private _Pdscto As Decimal
    Public Property Pdscto() As Decimal
        Get
            Return _Pdscto
        End Get
        Set(ByVal value As Decimal)
            _Pdscto = value
        End Set
    End Property

    Public Property PrecioCD() As Decimal
        Get
            Return _dc_PrecioCD
        End Get
        Set(ByVal value As Decimal)
            _dc_PrecioCD = value
        End Set
    End Property
    Public Property Importe() As Decimal
        Get
            Return _dc_Importe
        End Get
        Set(ByVal value As Decimal)
            _dc_Importe = value
        End Set
    End Property
    Public Property Utilidad() As Decimal
        Get
            Return _dc_Utilidad
        End Get
        Set(ByVal value As Decimal)
            _dc_Utilidad = value
        End Set
    End Property
    Public Property Percepcion() As Decimal
        Get
            Return _Percepcion
        End Get
        Set(ByVal value As Decimal)
            _Percepcion = value
        End Set
    End Property
    Public Property Detraccion() As Decimal
        Get
            Return _Detraccion
        End Get
        Set(ByVal value As Decimal)
            _Detraccion = value
        End Set
    End Property
    Public ReadOnly Property getNomProducto() As String
        Get
            If Me._Glosa Is Nothing Then
                Return Me._prod_Nombre
            ElseIf Me._Glosa.Trim.Length > 0 Then
                Return Me._prod_Nombre + " (RETAZO) [" + Me._Glosa + "]"
            Else
                Return Me._prod_Nombre
            End If
        End Get
    End Property


    Public Property DetalleGlosa() As String
        Get
            Return Me._DetalleGlosa
        End Get
        Set(ByVal value As String)
            Me._DetalleGlosa = value
        End Set
    End Property
    Public Property Kit() As Boolean
        Get
            Return Me._Kit
        End Get
        Set(ByVal value As Boolean)
            Me._Kit = value
        End Set
    End Property

    Public Property listaTo() As List(Of Entidades.Tono)
        Get
            Return Me._Tono
        End Get
        Set(ByVal value As List(Of Entidades.Tono))
            Me._Tono = value
        End Set
    End Property

    Public Property TasaPercepcion() As Decimal
        Get
            Return Me._dc_TasaPercepcion
        End Get
        Set(ByVal value As Decimal)
            Me._dc_TasaPercepcion = value
        End Set
    End Property

    Public Property PrecioLista() As Decimal
        Get
            Return Me._PrecioLista
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioLista = value
        End Set
    End Property

    Public Property IdUsuarioSupervisor() As Integer
        Get
            Return Me._IdSupervisor
        End Get
        Set(ByVal value As Integer)
            Me._IdSupervisor = value
        End Set
    End Property
    Public Property TasaDetraccion() As Decimal
        Get
            Return Me._dc_TasaDetraccion
        End Get
        Set(ByVal value As Decimal)
            Me._dc_TasaDetraccion = value
        End Set
    End Property


    Public Property IdDetalleAfecto() As Integer
        Get
            Return Me._IdDetalleAfecto
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleAfecto = value
        End Set
    End Property


    Public Property idTono() As Integer
        Get
            Return _idTono
        End Get
        Set(ByVal value As Integer)
            _idTono = value
        End Set
    End Property


    Public Property codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

    Public Property nan() As Boolean
        Get
            Return _nan
        End Get
        Set(ByVal value As Boolean)
            _nan = value
        End Set
    End Property

    Public Property CAD() As Boolean
        Get
            Return _CAD
        End Get
        Set(ByVal value As Boolean)
            _CAD = value
        End Set
    End Property


    Public Property PesoCaja() As Decimal
        Get
            Return _PesoCaja
        End Get
        Set(ByVal value As Decimal)
            _PesoCaja = value
        End Set
    End Property


    Public Property MCaja() As Decimal
        Get
            Return _MCaja
        End Get
        Set(ByVal value As Decimal)
            _MCaja = value
        End Set
    End Property


    Public Property PzaCaja() As Decimal
        Get
            Return _PzaCaja
        End Get
        Set(ByVal value As Decimal)
            _PzaCaja = value
        End Set
    End Property


    Public Property NroCaja() As Decimal
        Get
            Return _NroCaja
        End Get
        Set(ByVal value As Decimal)
            _NroCaja = value
        End Set
    End Property

    Public Property Pzas() As Decimal
        Get
            Return _Pzas
        End Get
        Set(ByVal value As Decimal)
            _Pzas = value
        End Set
    End Property

    Public Property JGO() As Decimal
        Get
            Return _JGO
        End Get
        Set(ByVal value As Decimal)
            _JGO = value
        End Set
    End Property

    Public Property CalibRe() As Boolean
        Get
            Return _CalibRe
        End Get
        Set(ByVal value As Boolean)
            _CalibRe = value
        End Set
    End Property

    Public Property NPallet() As Decimal
        Get
            Return _NPallet
        End Get
        Set(ByVal value As Decimal)
            _NPallet = value
        End Set
    End Property

    Public Property Total() As Decimal
        Get
            Return _Total
        End Get
        Set(ByVal value As Decimal)
            _Total = value
        End Set
    End Property


    Public Property Sucursal() As String
        Get
            Return _Sucursal
        End Get
        Set(ByVal value As String)
            _Sucursal = value
        End Set
    End Property


    Public Property TipoEmpaque() As String
        Get
            Return _TipoEmpaque
        End Get
        Set(ByVal value As String)
            _TipoEmpaque = value
        End Set
    End Property


    Public Property EstadoPallet() As String
        Get
            Return _EstadoPallet
        End Get
        Set(ByVal value As String)
            _EstadoPallet = value
        End Set
    End Property

    Public Property Rotulado() As Boolean
        Get
            Return _Rotulado
        End Get
        Set(ByVal value As Boolean)
            _Rotulado = value
        End Set
    End Property

    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property





End Class

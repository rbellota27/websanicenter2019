﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** VIERNES 19 FEB 2010 HORA 02_58 PM



Public Class ProductoView
    Inherits Entidades.Producto

    Private _IdUnidadMedida As Integer
    Private _UMPrincipal As Boolean
    Private _Linea As String
    Private _SubLinea As String
    Private _UnidadMedida As String
    Private _Moneda As String
    Private _StockDisponible As Decimal
    Private _StockDisponible_Cadena As String
    Private _cadena As String
    Private _prod_codigo As String
    Private _idSector As Integer = 0
    Private _StockReal As Decimal

    Public Property StockReal() As Decimal
        Get
            Return _StockReal
        End Get
        Set(ByVal value As Decimal)
            _StockReal = value
        End Set
    End Property

    Public Property Prod_Codigo() As String
        Get
            Return _prod_codigo
        End Get
        Set(ByVal value As String)
            _prod_codigo = value
        End Set
    End Property

    Public Property idSector() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property


    Public Property cadenaUM() As String
        Get
            Return _cadena
        End Get
        Set(ByVal value As String)
            _cadena = value
        End Set
    End Property

    Public ReadOnly Property ListaUM() As List(Of Entidades.ProductoUMView)
        Get

            Dim Lista As New List(Of Entidades.ProductoUMView)

            Dim IdDescripcionUM() As String = cadenaUM.Split("=")

            For x As Integer = 0 To IdDescripcionUM.Length - 1
                Dim id_UM() As String = IdDescripcionUM(x).Split(",")
                Dim obj As New Entidades.ProductoUMView
                obj.IdUnidadMedida = CInt(id_UM(0))
                obj.NombreCortoUM = id_UM(1)
                Lista.Add(obj)
            Next

            Return Lista
        End Get
    End Property


    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property UMPrincipal() As Boolean
        Get
            Return Me._UMPrincipal
        End Get
        Set(ByVal value As Boolean)
            Me._UMPrincipal = value
        End Set
    End Property

    Public Property Linea() As String
        Get
            Return Me._Linea
        End Get
        Set(ByVal value As String)
            Me._Linea = value
        End Set
    End Property

    Public Property SubLinea() As String
        Get
            Return Me._SubLinea
        End Get
        Set(ByVal value As String)
            Me._SubLinea = value
        End Set
    End Property

    Public Property UnidadMedida() As String
        Get
            Return Me._UnidadMedida
        End Get
        Set(ByVal value As String)
            Me._UnidadMedida = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property



    Public Property StockDisponible() As Decimal
        Get
            Return Me._StockDisponible
        End Get
        Set(ByVal value As Decimal)
            Me._StockDisponible = value
        End Set
    End Property

    Public Property StockDisponible_Cadena() As String
        Get
            Return Me._StockDisponible_Cadena
        End Get
        Set(ByVal value As String)
            Me._StockDisponible_Cadena = value
        End Set
    End Property


End Class

﻿Public Class be_Requerimiento_x_pagar
    Private _idMovCtaPP As Integer = 0
    Private _idCuentaProv As Integer = 0
    Private _idProveedor As Integer = 0
    Private _idProveedor2 As Integer = 0
    Private _proveedor As String = String.Empty
    Private _mcp_Fecha As DateTime = Date.Now
    Private _mcp_FechaCanc As String = String.Empty
    Private _mcp_Monto As Decimal = 0
    Private _mcp_saldo As Decimal = 0
    Private _mcp_Abono As Decimal = 0
    Private _idDocumento As Integer = 0
    Private _idDetalleConcepto As Integer = 0
    Private _doc_serie As String = String.Empty
    Private _doc_codigo As String = String.Empty
    Private _doc_FechaEmision As DateTime = Date.Now
    Private _doc_FechaVenc As DateTime = Date.Now
    Private _doc_TotalAPagar As Decimal = 0
    Private _idMoneda As Integer = 0
    Private _idEmpresa As Integer = 0
    Private _tienda As String = String.Empty
    Private _idTienda As Integer = 0
    Private _idTipoDocumento As Integer = 0
    Private _tipoDocumento As String = String.Empty
    Private _edoc_Nombre As String = String.Empty
    Private _PagoProgramado As Integer = 0
    Private _nroDocumento As String = String.Empty
    Private _nom_simbolo As String = String.Empty
    Private _NroItem As Integer = 0
    Private _idUsuario As Integer = 0
    Private _idDocumentoReferencia As Integer = 0
    Private _documentoReferenciado As String = String.Empty
    Private _totalAPagarDocReferenciado As Decimal = 0
    Private _monedaReferenciado As String = String.Empty
    Private _ruc As String = String.Empty

    Private _listaAgentes As List(Of TipoAgente)
    Private _listaRequerimientos As List(Of be_Requerimiento_x_pagar)

    Public Property listaRequerimientos() As List(Of be_Requerimiento_x_pagar)
        Get
            Return _listaRequerimientos
        End Get
        Set(ByVal value As List(Of be_Requerimiento_x_pagar))
            _listaRequerimientos = value
        End Set
    End Property

    Public Property listaAgentes() As List(Of TipoAgente)
        Get
            Return _listaAgentes
        End Get
        Set(ByVal value As List(Of TipoAgente))
            _listaAgentes = value
        End Set
    End Property


    Public Property idMovCtaPP() As Integer
        Get
            Return _idMovCtaPP
        End Get
        Set(ByVal value As Integer)
            _idMovCtaPP = value
        End Set
    End Property
    Public Property idCuentaProv() As Integer
        Get
            Return _idCuentaProv
        End Get
        Set(ByVal value As Integer)
            _idCuentaProv = value
        End Set
    End Property
    Public Property idProveedor() As Integer
        Get
            Return _idProveedor
        End Get
        Set(ByVal value As Integer)
            _idProveedor = value
        End Set
    End Property
    Public Property idProveedor2() As Integer
        Get
            Return _idProveedor2
        End Get
        Set(ByVal value As Integer)
            _idProveedor2 = value
        End Set
    End Property
    Public Property proveedor() As String
        Get
            Return _proveedor
        End Get
        Set(ByVal value As String)
            _proveedor = value
        End Set
    End Property
    Public Property mcp_Fecha() As DateTime
        Get
            Return _mcp_Fecha
        End Get
        Set(ByVal value As DateTime)
            _mcp_Fecha = value
        End Set
    End Property
    Public Property mcp_FechaCanc() As String
        Get
            Return _mcp_FechaCanc
        End Get
        Set(ByVal value As String)
            _mcp_FechaCanc = value
        End Set
    End Property
    Public Property mcp_Monto() As Decimal
        Get
            Return _mcp_Monto
        End Get
        Set(ByVal value As Decimal)
            _mcp_Monto = value
        End Set
    End Property
    Public Property mcp_saldo() As Decimal
        Get
            Return _mcp_saldo
        End Get
        Set(ByVal value As Decimal)
            _mcp_saldo = value
        End Set
    End Property
    Public Property mcp_abono() As Decimal
        Get
            Return _mcp_Abono
        End Get
        Set(ByVal value As Decimal)
            _mcp_Abono = value
        End Set
    End Property
    Public Property idDocumento() As Integer
        Get
            Return _idDocumento
        End Get
        Set(ByVal value As Integer)
            _idDocumento = value
        End Set
    End Property
    Public Property idDetalleConcepto() As Integer
        Get
            Return _idDetalleConcepto
        End Get
        Set(ByVal value As Integer)
            _idDetalleConcepto = value
        End Set
    End Property
    Public Property doc_serie() As String
        Get
            Return _doc_serie
        End Get
        Set(ByVal value As String)
            _doc_serie = value
        End Set
    End Property
    Public Property doc_codigo() As String
        Get
            Return _doc_codigo
        End Get
        Set(ByVal value As String)
            _doc_codigo = value
        End Set
    End Property
    Public Property doc_FechaEmision() As DateTime
        Get
            Return _doc_FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _doc_FechaEmision = value
        End Set
    End Property
    Public Property doc_FechaVenc() As DateTime
        Get
            Return _doc_FechaVenc
        End Get
        Set(ByVal value As DateTime)
            _doc_FechaVenc = value
        End Set
    End Property
    Public Property doc_TotalAPagar() As Decimal
        Get
            Return _doc_TotalAPagar
        End Get
        Set(ByVal value As Decimal)
            _doc_TotalAPagar = value
        End Set
    End Property
    Public Property idMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property
    Public Property idEmpresa() As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    Public Property tienda() As String
        Get
            Return _tienda
        End Get
        Set(ByVal value As String)
            _tienda = value
        End Set
    End Property
    Public Property idTienda() As Integer
        Get
            Return _idTienda
        End Get
        Set(ByVal value As Integer)
            _idTienda = value
        End Set
    End Property
    Public Property idTipoDocumento() As Integer
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As Integer)
            _idTipoDocumento = value
        End Set
    End Property
    Public Property tipoDocumento() As String
        Get
            Return _tipoDocumento
        End Get
        Set(ByVal value As String)
            _tipoDocumento = value
        End Set
    End Property
    Public Property edoc_Nombre() As String
        Get
            Return _edoc_Nombre
        End Get
        Set(ByVal value As String)
            _edoc_Nombre = value
        End Set
    End Property
    Public Property PagoProgramado() As Integer
        Get
            Return _PagoProgramado
        End Get
        Set(ByVal value As Integer)
            _PagoProgramado = value
        End Set
    End Property
    Public Property nroDocumento() As String
        Get
            Return _nroDocumento
        End Get
        Set(ByVal value As String)
            _nroDocumento = value
        End Set
    End Property
    Public Property nom_simbolo() As String
        Get
            Return _nom_simbolo
        End Get
        Set(ByVal value As String)
            _nom_simbolo = value
        End Set
    End Property
    Public Property NroItem() As Integer
        Get
            Return _NroItem
        End Get
        Set(ByVal value As Integer)
            _NroItem = value
        End Set
    End Property
    Public Property idUsuario() As Integer
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As Integer)
            _idUsuario = value
        End Set
    End Property

    Public Property idDocumentoReferencia() As Integer
        Get
            Return _idDocumentoReferencia
        End Get
        Set(ByVal value As Integer)
            _idDocumentoReferencia = value
        End Set
    End Property

    Public Property documentoReferenciado() As String
        Get
            Return _documentoReferenciado
        End Get
        Set(ByVal value As String)
            _documentoReferenciado = value
        End Set
    End Property

    Public Property totalAPagarDocReferenciado() As Decimal
        Get
            Return _totalAPagarDocReferenciado
        End Get
        Set(ByVal value As Decimal)
            _totalAPagarDocReferenciado = value
        End Set
    End Property

    Public Property monedaReferenciado() As String
        Get
            Return _monedaReferenciado
        End Get
        Set(ByVal value As String)
            _monedaReferenciado = value
        End Set
    End Property

    Public Property ruc() As String
        Get
            Return _ruc
        End Get
        Set(ByVal value As String)
            _ruc = value
        End Set
    End Property
End Class

﻿Public Class be_tonoXProducto
    Private _idProducto As Integer = 0
    Private _idAlmacen As Integer = 0
    Private _idSector As Integer = 0
    Private _idTono As Integer = 0
    Private _nomTono As String = String.Empty
    Private _descTono As String = String.Empty
    Private _cantidadTono As Decimal = 0

    Public Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property

    Public Property idAlmacen() As Integer
        Get
            Return _idAlmacen
        End Get
        Set(ByVal value As Integer)
            _idAlmacen = value
        End Set
    End Property

    Public Property idSector() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property

    Public Property idTono() As Integer
        Get
            Return _idTono
        End Get
        Set(ByVal value As Integer)
            _idTono = value
        End Set
    End Property

    Public Property nomtono() As String
        Get
            Return _nomTono
        End Get
        Set(ByVal value As String)
            _nomTono = value
        End Set
    End Property

    Public Property descTono() As String
        Get
            Return _descTono
        End Get
        Set(ByVal value As String)
            _descTono = value
        End Set
    End Property

    Public Property cantidadTono() As Decimal
        Get
            Return _cantidadTono
        End Get
        Set(ByVal value As Decimal)
            _cantidadTono = value
        End Set
    End Property
End Class

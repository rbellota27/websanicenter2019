﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.







'********************  MARTES 13 ABRIL 2010 HORA 02_26 PM






Public Class ProductoUMView
    Private _IdProducto As Integer
    Private _IdUnidadMedida As Integer
    Private _um_NombreCorto As String
    Private _pum_Equivalencia As Decimal
    Private _pum_UnidadPrincipal As Boolean
    Private _pum_Retazo As Boolean
    Private _descEstadoUP As String
    Private _descEstadoRetazo As String
    Private _Estado As String

    Private _UnidadMedida As String
    Private _codBarraXUm As String

    Private _pum_PorcentRetazo As Decimal


    Public Property pum_PorcentRetazo() As Decimal
        Get
            Return _pum_PorcentRetazo
        End Get
        Set(ByVal value As Decimal)
            _pum_PorcentRetazo = value
        End Set
    End Property

    Public Property CodBarraXUm() As String
        Get
            Return _codBarraXUm
        End Get
        Set(ByVal value As String)
            _codBarraXUm = value
        End Set
    End Property

    Public Property UnidadMedida() As String
        Get
            Return Me._UnidadMedida
        End Get
        Set(ByVal value As String)
            Me._UnidadMedida = value
        End Set
    End Property



    Public Property Estado() As String
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As String)
            Me._Estado = value
        End Set
    End Property
    Public ReadOnly Property EstadoBool() As Boolean
        Get
            If Me._Estado = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    Public ReadOnly Property DescEstadoUP()
        Get
            Me._descEstadoUP = "-"
            If _pum_UnidadPrincipal Then
                Me._descEstadoUP = "Sí"
            End If
            Return Me._descEstadoUP
        End Get
    End Property
    Public ReadOnly Property DescEstadoRetazo()
        Get
            Me._descEstadoRetazo = "-"
            If _pum_Retazo Then
                Me._descEstadoRetazo = "Sí"
            End If
            Return Me._descEstadoRetazo
        End Get
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property NombreCortoUM() As String
        Get
            Return Me._um_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._um_NombreCorto = value
        End Set
    End Property

    Public Property Equivalencia() As Decimal
        Get
            Return Me._pum_Equivalencia
        End Get
        Set(ByVal value As Decimal)
            Me._pum_Equivalencia = value
        End Set
    End Property

    Public Property UnidadPrincipal() As Boolean
        Get
            Return Me._pum_UnidadPrincipal
        End Get
        Set(ByVal value As Boolean)
            Me._pum_UnidadPrincipal = value
        End Set
    End Property

    Public Property Retazo() As Boolean
        Get
            Return Me._pum_Retazo
        End Get
        Set(ByVal value As Boolean)
            Me._pum_Retazo = value
        End Set
    End Property

End Class

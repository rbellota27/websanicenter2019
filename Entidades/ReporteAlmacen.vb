﻿Public Class ReporteAlmacen

    Private _CodigoSige As String
    Private _Pais As String
    Private _NombreProducto As String
    Private _IdProducto As Integer
    Private _Cantidad As Decimal
    Private _Costo As Decimal
    Private _TipoExistencia As String
    Private _Linea As String
    Private _SubLinea As String
    Private _Tienda As String
    Private _PVP As Decimal
    Private _UM As String

    Public Property CodigoSige() As String
        Get
            Return Me._CodigoSige
        End Get
        Set(ByVal value As String)
            Me._CodigoSige = value
        End Set
    End Property

    Public Property Pais() As String
        Get
            Return Me._Pais
        End Get
        Set(ByVal value As String)
            Me._Pais = value
        End Set
    End Property

    Public Property NombreProducto() As String
        Get
            Return Me._NombreProducto
        End Get
        Set(ByVal value As String)
            Me._NombreProducto = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property Cantidad() As Decimal
        Get
            Return Me._Cantidad
        End Get
        Set(ByVal value As Decimal)
            Me._Cantidad = value
        End Set
    End Property

    Public Property Costo() As Decimal
        Get
            Return Me._Costo
        End Get
        Set(ByVal value As Decimal)
            Me._Costo = value
        End Set
    End Property
    Public Property TipoExistencia() As String
        Get
            Return Me._TipoExistencia
        End Get
        Set(ByVal value As String)
            Me._TipoExistencia = value
        End Set
    End Property
    Public Property Linea() As String
        Get
            Return Me._Linea
        End Get
        Set(ByVal value As String)
            Me._Linea = value
        End Set
    End Property


    Public Property SubLinea() As String
        Get
            Return Me._SubLinea
        End Get
        Set(ByVal value As String)
            Me._SubLinea = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property

    Public Property PVP() As Decimal
        Get
            Return Me._PVP
        End Get
        Set(ByVal value As Decimal)
            Me._PVP = value
        End Set
    End Property

    Public Property UM() As String
        Get
            Return Me._UM
        End Get
        Set(ByVal value As String)
            Me._UM = value
        End Set
    End Property
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class DetVehiculoOt
    Private _idvehiculoOt As Integer
    Private _iddocumento As Integer
    Private _kilometraje As String
    Private _tipoMotor As String
    Private _nrocilindros As String

    Public Property idvehiculoOt() As Integer
        Get
            Return _idvehiculoOt
        End Get
        Set(ByVal value As Integer)
            _idvehiculoOt = value
        End Set
    End Property
    Public Property iddocumento() As Integer
        Get
            Return _iddocumento
        End Get
        Set(ByVal value As Integer)
            _iddocumento = value
        End Set
    End Property

    Public Property kilometraje() As Integer
        Get
            Return _kilometraje
        End Get
        Set(ByVal value As Integer)
            _kilometraje = value
        End Set
    End Property

    Public Property tipomotor() As Integer
        Get
            Return _tipoMotor
        End Get
        Set(ByVal value As Integer)
            _tipoMotor = value
        End Set
    End Property

    Public Property nrocilindros() As Integer
        Get
            Return nrocilindros
        End Get
        Set(ByVal value As Integer)
            _nrocilindros = value
        End Set
    End Property

End Class

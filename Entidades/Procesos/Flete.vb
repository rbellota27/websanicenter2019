﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Flete
#Region "variables privadas"
    Private _IdFlete As Integer
    Private _pp_UbigeoInicio As String
    Private _pp_UbigeoDestino As String
    Private _IdMoneda As Integer
    Private _fle_CostoxKilo As Decimal
    Private _fle_CostoFijo As Decimal
    Private _strInicio As String
    Private _strDestino As String
    Private _strMoneda As String
#End Region

#Region "Propiedades publicas"
    Public Property IdFlete() As Integer
        Get
            Return _IdFlete
        End Get
        Set(ByVal value As Integer)
            _IdFlete = value
        End Set
    End Property
    Public Property pp_UbigeoInicio() As String
        Get
            Return _pp_UbigeoInicio
        End Get
        Set(ByVal value As String)
            _pp_UbigeoInicio = value
        End Set
    End Property
    Public Property pp_UbigeoDestino() As String
        Get
            Return _pp_UbigeoDestino
        End Get
        Set(ByVal value As String)
            _pp_UbigeoDestino = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property
    Public Property fle_CostoxKilo() As Decimal
        Get
            Return _fle_CostoxKilo
        End Get
        Set(ByVal value As Decimal)
            _fle_CostoxKilo = value
        End Set
    End Property
    Public Property fle_CostoFijo() As Decimal
        Get
            Return _fle_CostoFijo
        End Get
        Set(ByVal value As Decimal)
            _fle_CostoFijo = value
        End Set
    End Property
    Public Property strInicio() As String
        Get
            Return _strInicio
        End Get
        Set(ByVal value As String)
            _strInicio = value
        End Set
    End Property
    Public Property strDestino() As String
        Get
            Return _strDestino
        End Get
        Set(ByVal value As String)
            _strDestino = value
        End Set
    End Property
    Public Property strMoneda() As String
        Get
            Return _strMoneda
        End Get
        Set(ByVal value As String)
            _strMoneda = value
        End Set
    End Property

#End Region

End Class

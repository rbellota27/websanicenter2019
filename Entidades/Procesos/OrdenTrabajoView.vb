﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class OrdenTrabajoView

    Private _IdTipoPV As Integer
    Public Property IdTipoPV() As Integer
        Get
            Return _IdTipoPV
        End Get
        Set(ByVal value As Integer)
            _IdTipoPV = value
        End Set
    End Property

    Private _PoseeOrdenDespacho As Integer

    Public Property PoseeOrdenDespacho() As Integer
        Get
            Return _PoseeOrdenDespacho
        End Get
        Set(ByVal value As Integer)
            _PoseeOrdenDespacho = value
        End Set
    End Property

    Private _idalmacen As Integer
    Public Property IdAlmacen() As Integer
        Get
            Return _idalmacen
        End Get
        Set(ByVal value As Integer)
            _idalmacen = value
        End Set
    End Property

    Private _IdEstadoDoc As Integer
    Public Property IdEstadoDoc() As Integer
        Get
            Return _IdEstadoDoc
        End Get
        Set(ByVal value As Integer)
            _IdEstadoDoc = value
        End Set
    End Property

    Private _idvehiculoOt As Integer
    Public Property idvehiculoOt() As Integer
        Get
            Return _idvehiculoOt
        End Get
        Set(ByVal value As Integer)
            _idvehiculoOt = value
        End Set
    End Property

    Private _idfinanciamientoOT As Integer
    Public Property idfinanciamientoOT() As Integer
        Get
            Return _idfinanciamientoOT
        End Get
        Set(ByVal value As Integer)
            _idfinanciamientoOT = value
        End Set
    End Property


    Private _idPersona As Integer
    Public Property idPersona() As Integer
        Get
            Return _idPersona
        End Get
        Set(ByVal value As Integer)
            _idPersona = value
        End Set
    End Property

    Private _iddocumento As Integer
    Public Property iddocumento() As Integer
        Get
            Return _iddocumento
        End Get
        Set(ByVal value As Integer)
            _iddocumento = value
        End Set
    End Property

    Private _modelo As String
    Public Property modelo() As String
        Get
            Return _modelo
        End Get
        Set(ByVal value As String)
            _modelo = value
        End Set
    End Property

    Private _marca As String
    Public Property marca() As String
        Get
            Return _marca
        End Get
        Set(ByVal value As String)
            _marca = value
        End Set
    End Property

    Private _numero As String
    Public Property numero() As String
        Get
            Return _numero
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property

    Private _numeroDoc As String
    Public Property numeroDoc() As String
        Get
            Return _numeroDoc
        End Get
        Set(ByVal value As String)
            _numeroDoc = value
        End Set
    End Property

    Private _serie As String
    Public Property serie() As String
        Get
            Return _serie
        End Get
        Set(ByVal value As String)
            _serie = value
        End Set
    End Property

    Private _fechaDoc As String
    Public Property fechaDoc() As String
        Get
            Return _fechaDoc
        End Get
        Set(ByVal value As String)
            _fechaDoc = value
        End Set
    End Property

    Private _nombres As String
    Public Property nombres() As String
        Get
            Return _nombres
        End Get
        Set(ByVal value As String)
            _nombres = value
        End Set
    End Property

    Private _dni As String
    Public Property dni() As String
        Get
            Return _dni
        End Get
        Set(ByVal value As String)
            _dni = value
        End Set
    End Property

    Private _ruc As String
    Public Property ruc() As String
        Get
            Return _ruc
        End Get
        Set(ByVal value As String)
            _ruc = value
        End Set
    End Property

    Private _placa As String
    Public Property placa() As String
        Get
            Return _placa
        End Get
        Set(ByVal value As String)
            _placa = value
        End Set
    End Property

    Private _servicio As String
    Public Property servicio() As String
        Get
            Return _servicio
        End Get
        Set(ByVal value As String)
            _servicio = value
        End Set
    End Property

    Private _idtrabajoarealizar As Integer
    Public Property trabajoarealizar() As Integer
        Get
            Return _idtrabajoarealizar
        End Get
        Set(ByVal value As Integer)
            _idtrabajoarealizar = value
        End Set
    End Property

    Private _idtiposervicio As Integer
    Public Property tiposervicio() As Integer
        Get
            Return _idtiposervicio
        End Get
        Set(ByVal value As Integer)
            _idtiposervicio = value
        End Set
    End Property

    Private _idTiposerviciostr As String
    Public Property tiposerviciostr() As String
        Get
            Return _idTiposerviciostr
        End Get
        Set(ByVal value As String)
            _idTiposerviciostr = value
        End Set
    End Property

    Private _idservicio As Integer
    Public Property idservicio() As Integer
        Get
            Return _idservicio
        End Get
        Set(ByVal value As Integer)
            _idservicio = value
        End Set
    End Property

    Private _observaciones As String
    Public Property observaciones() As String
        Get
            Return _observaciones
        End Get
        Set(ByVal value As String)
            _observaciones = value
        End Set
    End Property

    Private _fechadesposito As String
    Public Property fechadeposito() As String
        Get
            Return _fechadesposito
        End Get
        Set(ByVal value As String)
            _fechadesposito = value
        End Set
    End Property

    Private _monto As Decimal
    Public Property monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

    Private _idempresa As Integer
    Public Property idempresa() As Integer
        Get
            Return _idempresa
        End Get
        Set(ByVal value As Integer)
            _idempresa = value
        End Set
    End Property

    Private _idtienda As Integer
    Public Property idtienda() As Integer
        Get
            Return _idtienda
        End Get
        Set(ByVal value As Integer)
            _idtienda = value
        End Set
    End Property

    Private _direccion As String
    Public Property direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

    Private _telefono As String
    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

    Private _email As String
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    Private _idmoneda As Integer
    Public Property idmoneda() As Integer
        Get
            Return _idmoneda
        End Get
        Set(ByVal value As Integer)
            _idmoneda = value
        End Set
    End Property

    Private _nrocilindros As String
    Public Property nrocilindros() As String
        Get
            Return _nrocilindros
        End Get
        Set(ByVal value As String)
            _nrocilindros = value
        End Set
    End Property

    Private _idformapago As Integer
    Public Property idformapago() As Integer
        Get
            Return _idformapago
        End Get
        Set(ByVal value As Integer)
            _idformapago = value
        End Set
    End Property

    Private _idfinanciera As Integer
    Public Property idfinanciera() As Integer
        Get
            Return _idfinanciera
        End Get
        Set(ByVal value As Integer)
            _idfinanciera = value
        End Set
    End Property

    Private _tipomotor As String
    Public Property tipomotor() As String
        Get
            Return _tipomotor
        End Get
        Set(ByVal value As String)
            _tipomotor = value
        End Set
    End Property

    Private _nrochasis As String
    Public Property nrochasis() As String
        Get
            Return _nrochasis
        End Get
        Set(ByVal value As String)
            _nrochasis = value
        End Set
    End Property

    Private _clase As String
    Public Property clase() As String
        Get
            Return _clase
        End Get
        Set(ByVal value As String)
            _clase = value
        End Set
    End Property

    Private _anoFabric As String
    Public Property anoFabric() As String
        Get
            Return _anoFabric
        End Get
        Set(ByVal value As String)
            _anoFabric = value
        End Set
    End Property

    Private _tipocombustible As String
    Public Property tipocombustible() As String
        Get
            Return _tipocombustible
        End Get
        Set(ByVal value As String)
            _tipocombustible = value
        End Set
    End Property

    Private _idestadoCan As Integer
    Public Property idestadoCan() As Integer
        Get
            Return _idestadoCan
        End Get
        Set(ByVal value As Integer)
            _idestadoCan = value
        End Set
    End Property


End Class

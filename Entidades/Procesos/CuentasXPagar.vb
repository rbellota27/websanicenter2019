﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class CuentasXPagar
    Private _doc_Serie As String
    Private _doc_Codigo As String
    Private _IdDocumento As Integer
    Private _doc_FechaEmision As Date
    Private _doc_FechaVenc As Date
    Private _IdPersona As Integer
    Private _IdEstadoDoc As Integer
    Private _IdEstadoCancelacion As Integer
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _NomComercial As String
    Private _RSocial As String
    Private _Nombre As String
    Private _NomTipoDocumento As String
    Private _NomEstadoCancelacion As String
    Private _NomEmpresa As String
    Private _NomTienda As String
    Private _IdMovCuenta As Integer
    Private _IdCuentaPersona As Integer
    Private _Monto As Decimal
    Private _Factor As Integer
    Private _IdMoneda As Integer
    Private _SimboloMoneda As String
    Private _MontoAmortizado As Decimal

    Public Property doc_Serie() As String
        Get
            Return Me._doc_Serie
        End Get
        Set(ByVal value As String)
            Me._doc_Serie = value
        End Set
    End Property

    Public Property doc_Codigo() As String
        Get
            Return Me._doc_Codigo
        End Get
        Set(ByVal value As String)
            Me._doc_Codigo = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property doc_FechaEmision() As Date
        Get
            Return Me._doc_FechaEmision
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaEmision = value
        End Set
    End Property

    Public Property doc_FechaVenc() As Date
        Get
            Return Me._doc_FechaVenc
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaVenc = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdEstadoDoc() As Integer
        Get
            Return Me._IdEstadoDoc
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoDoc = value
        End Set
    End Property

    Public Property IdEstadoCancelacion() As Integer
        Get
            Return Me._IdEstadoCancelacion
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoCancelacion = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property NomComercial() As String
        Get
            Return Me._NomComercial
        End Get
        Set(ByVal value As String)
            Me._NomComercial = value
        End Set
    End Property

    Public Property RSocial() As String
        Get
            Return Me._RSocial
        End Get
        Set(ByVal value As String)
            Me._RSocial = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._Nombre
        End Get
        Set(ByVal value As String)
            Me._Nombre = value
        End Set
    End Property

    Public Property NomTipoDocumento() As String
        Get
            Return Me._NomTipoDocumento
        End Get
        Set(ByVal value As String)
            Me._NomTipoDocumento = value
        End Set
    End Property

    Public Property NomEstadoCancelacion() As String
        Get
            Return Me._NomEstadoCancelacion
        End Get
        Set(ByVal value As String)
            Me._NomEstadoCancelacion = value
        End Set
    End Property

    Public Property NomEmpresa() As String
        Get
            Return Me._NomEmpresa
        End Get
        Set(ByVal value As String)
            Me._NomEmpresa = value
        End Set
    End Property

    Public Property NomTienda() As String
        Get
            Return Me._NomTienda
        End Get
        Set(ByVal value As String)
            Me._NomTienda = value
        End Set
    End Property

    Public Property IdMovCuenta() As Integer
        Get
            Return Me._IdMovCuenta
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuenta = value
        End Set
    End Property

    Public Property IdCuentaPersona() As Integer
        Get
            Return Me._IdCuentaPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaPersona = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._Monto
        End Get
        Set(ByVal value As Decimal)
            Me._Monto = value
        End Set
    End Property

    Public Property Factor() As Integer
        Get
            Return Me._Factor
        End Get
        Set(ByVal value As Integer)
            Me._Factor = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property SimboloMoneda() As String
        Get
            Return Me._SimboloMoneda
        End Get
        Set(ByVal value As String)
            Me._SimboloMoneda = value
        End Set
    End Property

    Public Property MontoAmortizado() As Decimal
        Get
            Return Me._MontoAmortizado
        End Get
        Set(ByVal value As Decimal)
            Me._MontoAmortizado = value
        End Set
    End Property
    Public ReadOnly Property getSaldo() As Decimal
        Get
            Return Me._Monto - Me._MontoAmortizado
        End Get
    End Property
    Public ReadOnly Property getNomCliente() As String
        Get
            If Me._RSocial.Trim.Length > 0 Then
                Return Me._RSocial
            ElseIf Me._Nombre.Trim.Length > 0 Then
                Return Me._Nombre
            Else
                Return Me._NomComercial
            End If
        End Get
    End Property


End Class

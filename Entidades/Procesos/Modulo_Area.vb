﻿Public Class Modulo_Area
    Private _IdModulo As Integer
    Private _IdTipoExistencia As Integer
    Private _IdArea As Integer
    Private _LAEstado As Boolean
    Private _objArea As Object

    Public Property IdLinea() As Integer
        Get
            Return _IdModulo
        End Get
        Set(ByVal value As Integer)
            _IdModulo = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property IdArea() As Integer
        Get
            Return _IdArea
        End Get
        Set(ByVal value As Integer)
            _IdArea = value
        End Set
    End Property
    Public Property LAEstado() As Boolean
        Get
            Return _LAEstado
        End Get
        Set(ByVal value As Boolean)
            _LAEstado = value
        End Set
    End Property

    Public Property objArea() As Object
        Get
            Return _objArea
        End Get
        Set(ByVal value As Object)
            _objArea = value
        End Set
    End Property

End Class

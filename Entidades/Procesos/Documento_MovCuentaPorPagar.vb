﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM


Public Class Documento_MovCuentaPorPagar
    Inherits Entidades.Documento

    Protected _IdMovCtaPP As Integer
    Protected _MontoTotal As Decimal
    Protected _Saldo As Decimal
    Protected _NroDiasMora As Integer
    Private _SaldoNew As Decimal
    Private _FlagSustento As Integer
    Private _NroDocumento As String
    Public Property NroDocumentoz() As String
        Get
            Return Me._NroDocumento
        End Get
        Set(ByVal value As String)
            Me._NroDocumento = value
        End Set
    End Property
    Public Property Sustento() As Integer
        Get
            Return Me._FlagSustento
        End Get
        Set(ByVal value As Integer)
            Me._FlagSustento = value
        End Set
    End Property

    Public Property SaldoNew() As Decimal
        Get
            Return Me._SaldoNew
        End Get
        Set(ByVal value As Decimal)
            Me._SaldoNew = value
        End Set
    End Property


    'Ya esta declarado en la Entidad Documento
    'Protected _NroDocumento As String

    'Public Property NroDocumento() As String
    '    Get
    '        Return Me._NroDocumento
    '    End Get
    '    Set(ByVal value As String)
    '        Me._NroDocumento = value
    '    End Set
    'End Property


    Public ReadOnly Property getNroDocumento() As String

        Get

            Return Serie + " - " + Codigo

        End Get

    End Property




    Public Property IdMovCtaPP() As Integer
        Get
            Return Me._IdMovCtaPP
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCtaPP = value
        End Set
    End Property

    Public Property MontoTotal() As Decimal
        Get
            Return Me._MontoTotal
        End Get
        Set(ByVal value As Decimal)
            Me._MontoTotal = value
        End Set
    End Property

    Public Property Saldo() As Decimal
        Get
            Return Me._Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._Saldo = value
        End Set
    End Property



    Public Property NroDiasMora() As Integer
        Get
            Return Me._NroDiasMora
        End Get
        Set(ByVal value As Integer)
            Me._NroDiasMora = value
        End Set
    End Property

End Class

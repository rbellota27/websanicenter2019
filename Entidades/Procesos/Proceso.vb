﻿Public Class Proceso
    Private _id As Integer
    Private _codigo As String
    Private _Descripcion As String
    Private _nombre As String
    Private _estado As Integer
    Private _modulo As Integer
    Private _mod As String


    Public Sub New()
    End Sub

    Sub New(ByVal id As Integer, ByVal nombre As String)
        ' TODO: Complete member initialization 
        Me.id = _id
        Me.nombre = _nombre
    End Sub

    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property


    Public Property codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property


    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property estado() As Integer
        Get
            Return _estado
        End Get
        Set(ByVal value As Integer)
            _estado = value
        End Set
    End Property

    Public Property modulo() As Integer
        Get
            Return _modulo
        End Get
        Set(ByVal value As Integer)
            _modulo = value
        End Set
    End Property


    Public Property nomModulo() As String
        Get
            Return _mod
        End Get
        Set(ByVal value As String)
            _mod = value
        End Set
    End Property

End Class

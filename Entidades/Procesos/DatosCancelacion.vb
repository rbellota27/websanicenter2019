﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class DatosCancelacion

    Protected _IdDatosCanc As Integer
    Protected _IdDocumento As Integer
    Protected _IdMedioPago As Integer
    Protected _IdDocumentoRef As Integer
    Protected _IdBanco As Integer
    Protected _IdCuentaBancaria As Integer
    Protected _IdTipoMovimiento As Integer
    Protected _dcan_NroOperacion As String
    Protected _dcan_FechaCanc As Date
    Protected _dcan_monto As Decimal
    Protected _IdMoneda As Integer
    Protected _IdCaja As Integer
    Protected _dcan_Observacion As String
    Protected _dcan_Factor As Integer
    Protected _dcan_FechaACobrar As Date
    Protected _dcan_NumeroCheque As String

    Protected _Banco As String
    Protected _CuentaBancaria As String
    Protected _TipoMovimiento As String
    Protected _Moneda As String
    Protected _MedioPago As String

    Protected _IdMedioPagoInterfaz As Integer
    Protected _IdConceptoMovBanco As Integer
    Protected _ConceptoMovBanco As String

    Protected _IdMonedaDestino As Integer
    Protected _MonedaSimboloDestino As String
    Protected _MontoEquivalenteDestino As Decimal

    Protected _Descripcion As String

    Protected _idProgramacion As Integer

    Public Sub New()

    End Sub

    Public Property idProgrmacion() As Integer
        Get
            Return _idProgramacion
        End Get
        Set(value As Integer)
            _idProgramacion = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return _IdDatosCanc
        End Get
        Set(ByVal value As Integer)
            _IdDatosCanc = value
        End Set
    End Property
    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDocumento = value
        End Set
    End Property
    Public Property IdMedioPago() As Integer
        Get
            Return _IdMedioPago
        End Get
        Set(ByVal value As Integer)
            _IdMedioPago = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return _IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            _IdDocumentoRef = value
        End Set
    End Property
    Public Property IdBanco() As Integer
        Get
            Return _IdBanco
        End Get
        Set(ByVal value As Integer)
            _IdBanco = value
        End Set
    End Property
    Public Property IdCuentaBancaria() As Integer
        Get
            Return _IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            _IdCuentaBancaria = value
        End Set
    End Property

    Public Property IdTipoMovimiento() As Integer
        Get
            Return _IdTipoMovimiento
        End Get
        Set(ByVal value As Integer)
            _IdTipoMovimiento = value
        End Set
    End Property
    Public Property NroOperacion() As String
        Get
            Return _dcan_NroOperacion
        End Get
        Set(ByVal value As String)
            _dcan_NroOperacion = value
        End Set
    End Property

    Public Property FechaCanc() As Date
        Get
            Return _dcan_FechaCanc
        End Get
        Set(ByVal value As Date)
            _dcan_FechaCanc = value
        End Set
    End Property
    Public Property Monto() As Decimal
        Get
            Return _dcan_monto
        End Get
        Set(ByVal value As Decimal)
            _dcan_monto = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property
    Public Property IdCaja() As Integer
        Get
            Return _IdCaja
        End Get
        Set(ByVal value As Integer)
            _IdCaja = value
        End Set
    End Property
    Public Property Observacion() As String
        Get
            Return _dcan_Observacion
        End Get
        Set(ByVal value As String)
            _dcan_Observacion = value
        End Set
    End Property
    Public Property Factor() As Integer
        Get
            Return _dcan_Factor
        End Get
        Set(ByVal value As Integer)
            _dcan_Factor = value
        End Set
    End Property
    Public Property FechaACobrar() As Date
        Get
            Return _dcan_FechaACobrar
        End Get
        Set(ByVal value As Date)
            _dcan_FechaACobrar = value
        End Set
    End Property
    Public Property NumeroCheque() As String
        Get
            Return _dcan_NumeroCheque
        End Get
        Set(ByVal value As String)
            _dcan_NumeroCheque = value
        End Set
    End Property


    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property
    Public Property CuentaBancaria() As String
        Get
            Return _CuentaBancaria
        End Get
        Set(ByVal value As String)
            _CuentaBancaria = value
        End Set
    End Property
    Public Property TipoMovimiento() As String
        Get
            Return _TipoMovimiento
        End Get
        Set(ByVal value As String)
            _TipoMovimiento = value
        End Set
    End Property
    Public Property Moneda() As String
        Get
            Return _Moneda
        End Get
        Set(ByVal value As String)
            _Moneda = value
        End Set
    End Property
    Public Property MedioPago() As String
        Get
            Return _MedioPago
        End Get
        Set(ByVal value As String)
            _MedioPago = value
        End Set
    End Property

    Public Property IdMedioPagoInterfaz() As Integer
        Get
            Return _IdMedioPagoInterfaz
        End Get
        Set(ByVal value As Integer)
            _IdMedioPagoInterfaz = value
        End Set
    End Property

    Public Property IdConceptoMovBanco() As Integer
        Get
            Return _IdConceptoMovBanco
        End Get
        Set(ByVal value As Integer)
            _IdConceptoMovBanco = value
        End Set
    End Property

    Public Property ConceptoMovBanco() As String
        Get
            Return _ConceptoMovBanco
        End Get
        Set(ByVal value As String)
            _ConceptoMovBanco = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Public Property IdMonedaDestino() As Integer
        Get
            Return _IdMonedaDestino
        End Get
        Set(ByVal value As Integer)
            _IdMonedaDestino = value
        End Set
    End Property

    Public Property MonedaSimboloDestino() As String
        Get
            Return _MonedaSimboloDestino
        End Get
        Set(ByVal value As String)
            _MonedaSimboloDestino = value
        End Set
    End Property

    Public Property MontoEquivalenteDestino() As Decimal
        Get
            Return _MontoEquivalenteDestino
        End Get
        Set(ByVal value As Decimal)
            _MontoEquivalenteDestino = value
        End Set
    End Property

End Class


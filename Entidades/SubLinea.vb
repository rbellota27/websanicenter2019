'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'********************  JUEVES 11 FEB 2010 HORA 10_00 AM










Public Class SubLinea
    Private _IdSubLInea As Integer
    Private _sl_Nombre As String
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _sl_AfectoIgv As String
    Private _sl_CtaDebeCompra As String
    Private _sl_CtaHaberCompra As String
    Private _sl_CostoCompra As String
    Private _sl_CostoVenta As String
    Private _sl_CtaDebeVenta As String
    Private _sl_CtaHaberVenta As String
    Private _sl_Estado As String
    Private _sl_AfectoPercepcion As String
    Private _sl_AfectoDetraccion As String
    Public _descEstado As String
    Private _NomLinea As String
    Private _CodigoSubLinea As String
    Private _estado As Boolean
    Private _sl_NroOrden As Integer
    Private _l_NroOrden As Integer
    Private _sl_ParteNombre As Boolean
    Private _l_ParteNombre As Boolean
    Private _lin_Codigo As String
    Private _lin_TipoUso As String
    Private _sl_TipoUso As String
    Private _sl_Longitud As Integer
    Private _Cantidad As Integer

    Public Sub New()
    End Sub
    Public Sub New(ByVal idsublinea As Integer, ByVal nombre As String)
        Me.Id = idsublinea
        Me.Nombre = nombre
    End Sub
    Public Sub New(ByVal idsublinea As Integer, ByVal nombre As String, ByVal estado As Boolean)
        Me.Id = idsublinea
        Me.Nombre = nombre
        Me.EstadoBit = estado
    End Sub
    Public Sub New(ByVal codigosublinea As String, ByVal nombre As String, ByVal estado As Boolean)
        Me.CodigoSubLinea = codigosublinea
        Me.Nombre = nombre
        Me.EstadoBit = estado
    End Sub
    Public Property CodigoSubLinea() As String
        Get
            Return Me._CodigoSubLinea
        End Get
        Set(ByVal value As String)
            Me._CodigoSubLinea = value
        End Set
    End Property


    Public Property NomLinea() As String
        Get
            Return Me._NomLinea
        End Get
        Set(ByVal value As String)
            Me._NomLinea = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._sl_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property

    Public Property AfectoPercepcion() As String
        Get
            Return Me._sl_AfectoPercepcion
        End Get
        Set(ByVal value As String)
            Me._sl_AfectoPercepcion = value
        End Set
    End Property
    Public Property AfectoDetraccion() As String
        Get
            Return Me._sl_AfectoDetraccion
        End Get
        Set(ByVal value As String)
            Me._sl_AfectoDetraccion = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdSubLInea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLInea = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._sl_Nombre
        End Get
        Set(ByVal value As String)
            Me._sl_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._sl_Estado
        End Get
        Set(ByVal value As String)
            Me._sl_Estado = value
        End Set
    End Property

    Public Property IdLinea() As Integer
        Get
            Return Me._IdLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdLinea = value
        End Set
    End Property

    Public Property IdTipoExistencia() As Integer
        Get
            Return Me._IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoExistencia = value
        End Set
    End Property

    Public Property AfectoIgv() As String
        Get
            Return Me._sl_AfectoIgv
        End Get
        Set(ByVal value As String)
            Me._sl_AfectoIgv = value
        End Set
    End Property

    Public Property CtaDebeCompra() As String
        Get
            Return Me._sl_CtaDebeCompra
        End Get
        Set(ByVal value As String)
            Me._sl_CtaDebeCompra = value
        End Set
    End Property

    Public Property CtaHaberCompra() As String
        Get
            Return Me._sl_CtaHaberCompra
        End Get
        Set(ByVal value As String)
            Me._sl_CtaHaberCompra = value
        End Set
    End Property

    Public Property CostoCompra() As String
        Get
            Return Me._sl_CostoCompra
        End Get
        Set(ByVal value As String)
            Me._sl_CostoCompra = value
        End Set
    End Property

    Public Property CtaDebeVenta() As String
        Get
            Return Me._sl_CtaDebeVenta
        End Get
        Set(ByVal value As String)
            Me._sl_CtaDebeVenta = value
        End Set
    End Property

    Public Property CtaHaberVenta() As String
        Get
            Return Me._sl_CtaHaberVenta
        End Get
        Set(ByVal value As String)
            Me._sl_CtaHaberVenta = value
        End Set
    End Property

    Public Property CostoVenta() As String
        Get
            Return Me._sl_CostoVenta
        End Get
        Set(ByVal value As String)
            Me._sl_CostoVenta = value
        End Set
    End Property
    Public Property EstadoBit() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property
    Public Property NroOrdenSubLinea() As Integer
        Get
            Return _sl_NroOrden
        End Get
        Set(ByVal value As Integer)
            _sl_NroOrden = value
        End Set
    End Property
    Public Property NroOrdenLinea() As Integer
        Get
            Return _l_NroOrden
        End Get
        Set(ByVal value As Integer)
            _l_NroOrden = value
        End Set
    End Property
    Public Property ParteNombreSubLinea() As Boolean
        Get
            Return _sl_ParteNombre
        End Get
        Set(ByVal value As Boolean)
            _sl_ParteNombre = value
        End Set
    End Property
    Public Property ParteNombreLinea() As Boolean
        Get
            Return _l_ParteNombre
        End Get
        Set(ByVal value As Boolean)
            _l_ParteNombre = value
        End Set
    End Property
    Public Property CodigoLinea() As String
        Get
            Return _lin_Codigo
        End Get
        Set(ByVal value As String)
            _lin_Codigo = value
        End Set
    End Property
    Public Property TipoUsoLinea() As String
        Get
            Return _sl_TipoUso
        End Get
        Set(ByVal value As String)
            _sl_TipoUso = value
        End Set
    End Property
    Public Property TipoUsoSubLinea() As String
        Get
            Return _sl_TipoUso
        End Get
        Set(ByVal value As String)
            _sl_TipoUso = value
        End Set
    End Property

    Public Property Longitud() As Integer
        Get
            Return _sl_Longitud
        End Get
        Set(ByVal value As Integer)
            _sl_Longitud = value
        End Set
    End Property
    Public Property Cantidad() As Integer
        Get
            Return _Cantidad
        End Get
        Set(ByVal value As Integer)
            _Cantidad = value
        End Set
    End Property
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Kardex

    Private _idregistro As Nullable(Of Integer)
    Private _Documento As String
    Private _Nro As String
    Private _IdDetalleDocumento As Nullable(Of Integer)
    Private _doc_FechaEmision As Nullable(Of Date)
    Private _EntSal As String
    Private _dma_Cantidad As Nullable(Of Decimal)
    Private _dma_Costo As Nullable(Of Decimal)
    Private _dma_Total As Nullable(Of Decimal)
    Private _sk_CantidadSaldo As Nullable(Of Decimal)
    Private _sk_CostoSaldo As Nullable(Of Decimal)
    Private _sk_TotalSaldo As Nullable(Of Decimal)
    Private _DocRef As String
    Private _DocRefNumero As String
    Private _IdDocumento As Nullable(Of Integer)

    Public Property IdDocumento() As Nullable(Of Integer)
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _IdDocumento = value
        End Set
    End Property

    Public Property idregistro() As Nullable(Of Integer)
        Get
            Return _idregistro
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _idregistro = value
        End Set
    End Property
    Public Property Documento() As String
        Get
            Return _Documento
        End Get
        Set(ByVal value As String)
            _Documento = value
        End Set
    End Property
    Public Property Nro() As String
        Get
            Return _Nro
        End Get
        Set(ByVal value As String)
            _Nro = value
        End Set
    End Property
    Public Property IdDetalleDocumento() As Nullable(Of Integer)
        Get
            Return _IdDetalleDocumento
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _IdDetalleDocumento = value
        End Set
    End Property
    Public Property doc_FechaEmision() As Nullable(Of Date)
        Get
            Return _doc_FechaEmision
        End Get
        Set(ByVal value As Nullable(Of Date))
            _doc_FechaEmision = value
        End Set
    End Property
    Public Property EntSal() As String
        Get
            Return _EntSal
        End Get
        Set(ByVal value As String)
            _EntSal = value
        End Set
    End Property
    Public Property dma_Cantidad() As Nullable(Of Decimal)
        Get
            Return _dma_Cantidad
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            _dma_Cantidad = value
        End Set
    End Property
    Public Property dma_Costo() As Nullable(Of Decimal)
        Get
            Return _dma_Costo
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            _dma_Costo = value
        End Set
    End Property
    Public Property dma_Total() As Nullable(Of Decimal)
        Get
            Return _dma_Total
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            _dma_Total = value
        End Set
    End Property
    Public Property sk_CantidadSaldo() As Nullable(Of Decimal)
        Get
            Return _sk_CantidadSaldo
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            _sk_CantidadSaldo = value
        End Set
    End Property
    Public Property sk_CostoSaldo() As Nullable(Of Decimal)
        Get
            Return _sk_CostoSaldo
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            _sk_CostoSaldo = value
        End Set
    End Property
    Public Property sk_TotalSaldo() As Nullable(Of Decimal)
        Get
            Return _sk_TotalSaldo
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            _sk_TotalSaldo = value
        End Set
    End Property
    Public Property DocRef() As String
        Get
            Return _DocRef
        End Get
        Set(ByVal value As String)
            _DocRef = value
        End Set
    End Property
    Public Property DocRefNumero() As String
        Get
            Return _DocRefNumero
        End Get
        Set(ByVal value As String)
            _DocRefNumero = value
        End Set
    End Property

End Class

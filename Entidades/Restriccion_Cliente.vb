﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Public Class Restriccion_Cliente
    Inherits Entidades.PersonaView

    Private _IdCliente As Integer
    Private _IdComisionCab As Integer
    Private _com_Estado As Boolean

    Private _Empresa As String
    Private _Tienda As String
    Private _Perfil As String

    Private _Descripcion As String

    Public Property Descripcion() As String
        Get
            Return Me._Descripcion
        End Get
        Set(ByVal value As String)
            Me._Descripcion = value
        End Set
    End Property


    Public Property Empresa() As String
        Get
            Return Me._Empresa
        End Get
        Set(ByVal value As String)
            Me._Empresa = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property

    Public Property Perfil() As String
        Get
            Return Me._Perfil
        End Get
        Set(ByVal value As String)
            Me._Perfil = value
        End Set
    End Property



    Public Property IdCliente() As Integer
        Get
            Return Me._IdCliente
        End Get
        Set(ByVal value As Integer)
            Me._IdCliente = value
        End Set
    End Property

    Public Property IdComisionCab() As Integer
        Get
            Return Me._IdComisionCab
        End Get
        Set(ByVal value As Integer)
            Me._IdComisionCab = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._com_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._com_Estado = value
        End Set
    End Property


End Class

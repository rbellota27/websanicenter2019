﻿Public Class TomaInventarioReporteador
    Private _Idtinventario As Integer
    Private _FechaCreacion As Date
    Private _FechaTermino As Date
    Private _IdTipoInventario As Integer
    Private _IdDescToma As Integer
    Private _IdTienda As Integer
    Private _IdAlmacen As Integer
    Private _Estado As Integer
    Private _Activo As Boolean
    Private _Obs As String
    Private _IdAlmacenConsolidado As Integer
    Private _UserCreate As String
    Private _FechaCreate As Date
    Private _UserUpdate As String
    Private _FechaUpdate As Date


    Public Property Idtinventario() As Integer
        Get
            Return _Idtinventario
        End Get
        Set(ByVal value As Integer)
            _Idtinventario = value
        End Set
    End Property

    Public Property FechaCreacion() As Date
        Get
            Return _FechaCreacion
        End Get
        Set(ByVal value As Date)
            _FechaCreacion = value
        End Set
    End Property

    Public Property FechaTermino() As Date
        Get
            Return _FechaTermino
        End Get
        Set(ByVal value As Date)
            _FechaTermino = value
        End Set
    End Property

    Public Property IdTipoInventario() As Integer
        Get
            Return _IdTipoInventario
        End Get
        Set(ByVal value As Integer)
            _IdTipoInventario = value
        End Set
    End Property

    Public Property IdDescToma() As Integer
        Get
            Return _IdDescToma
        End Get
        Set(ByVal value As Integer)
            _IdDescToma = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(ByVal value As Integer)
            _IdTienda = value
        End Set
    End Property

    Public Property IdAlmacen() As Integer
        Get
            Return _IdAlmacen
        End Get
        Set(ByVal value As Integer)
            _IdAlmacen = value
        End Set
    End Property

    Public Property Estado() As Integer
        Get
            Return _Estado
        End Get
        Set(ByVal value As Integer)
            _Estado = value
        End Set
    End Property

    Public Property Activo() As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property

    Public Property Obs() As String
        Get
            Return _Obs
        End Get
        Set(ByVal value As String)
            _Obs = value
        End Set
    End Property

    Public Property IdAlmacenConsolidado() As Integer
        Get
            Return _IdAlmacenConsolidado
        End Get
        Set(ByVal value As Integer)
            _IdAlmacenConsolidado = value
        End Set
    End Property

    Public Property UserCreate() As String
        Get
            Return _UserCreate
        End Get
        Set(ByVal value As String)
            _UserCreate = value
        End Set
    End Property

    Public Property FechaCreate() As Date
        Get
            Return _FechaCreate
        End Get
        Set(ByVal value As Date)
            _FechaCreate = value
        End Set
    End Property

    Public Property UserUpdate() As String
        Get
            Return _UserUpdate
        End Get
        Set(ByVal value As String)
            _UserUpdate = value
        End Set
    End Property

    Public Property FechaUpdate() As Date
        Get
            Return _FechaUpdate
        End Get
        Set(ByVal value As Date)
            _FechaUpdate = value
        End Set
    End Property

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************  JUEVES 11 FEB 2010 HORA 10_00 AM









'***************   LUNES 18 ENERO 2010 HORA 12_11 pm

Public Class ZonaUbigeo
    Private _IdZona As String
    Private _zo_Nombre As String
    Private _zo_Descripcion As String
    Private _zo_Estado As Boolean
    Private _Moneda As String
    Private _zo_MontoMin As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idpais As String, ByVal nombre As String, ByVal descripcion As String, ByVal moneda As String, ByVal monto As String)
        Me._IdZona = idpais
        Me._zo_Nombre = nombre
        Me._zo_Descripcion = descripcion
        Me._Moneda = moneda
        Me._zo_MontoMin = monto
    End Sub
    Public Property IdZona() As String
        Get
            Return Me._IdZona
        End Get
        Set(ByVal value As String)
            Me._IdZona = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._zo_Nombre
        End Get
        Set(ByVal value As String)
            Me._zo_Nombre = value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return Me._zo_Descripcion
        End Get
        Set(ByVal value As String)
            Me._zo_Descripcion = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return Me._zo_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._zo_Estado = value
        End Set
    End Property
    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property
    Public Property MontoMin() As String
        Get
            Return Me._zo_MontoMin
        End Get
        Set(ByVal value As String)
            Me._zo_MontoMin = value
        End Set
    End Property

    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "Activo"
            If Estado = False Then Return "Inactivo"
        End Get
    End Property

End Class

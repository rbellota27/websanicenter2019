﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'************  VIERNES 04 06 2010

Public Class ComisionCab_TipoPrecioV

    Private _IdComisionCab As Integer
    Private _IdTipoPV As Integer
    Private _com_Estado As Boolean

    Private _TipoPrecioV As String

    Public Property TipoPrecioV() As String
        Get
            Return Me._TipoPrecioV
        End Get
        Set(ByVal value As String)
            Me._TipoPrecioV = value
        End Set
    End Property


    Public Property IdComisionCab() As Integer
        Get
            Return Me._IdComisionCab
        End Get
        Set(ByVal value As Integer)
            Me._IdComisionCab = value
        End Set
    End Property

    Public Property IdTipoPV() As Integer
        Get
            Return Me._IdTipoPV
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._com_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._com_Estado = value
        End Set
    End Property

End Class

﻿Public Class beCotizacionBuscar
	Inherits Entidades.Documento

	Private objPersonaView As New Entidades.PersonaView
	Private objTipoAgente As New Entidades.TipoAgente
	Private objAnexo_Documento As New Entidades.Anexo_Documento
	Private objMaestroObra As New Entidades.PersonaView
	Private objImpuesto As New Entidades.Impuesto
	Private _IdDocumentoRef As Integer

	Private _listaProductos As New List(Of DetalleDocumento)
	Private _listaDetalleConcepto As New List(Of DetalleConcepto)
	Private _PuntoPartida As New Entidades.PuntoPartida
	Private _PuntoLlegada As New Entidades.PuntoLlegada
	Private _Observacion As New Entidades.Observacion
	Private _lista_RelacionDocumento As New List(Of Entidades.Documento)
	Private _IGV As Decimal
	Private _lista_MotivoTraslado As New List(Of Entidades.beCampoEntero)
	Private _lista_MedioPago As New List(Of Entidades.beCampoEntero)
	Private _lista_CuentaPersona As New List(Of Entidades.CuentaPersona)
	Private _lista_ProvinciaPP As New List(Of Entidades.beCboProvincia)
	Private _lista_DistritoPP As New List(Of Entidades.beCboDistrito)
	Private _lista_ProvinciaPL As New List(Of Entidades.beCboProvincia)
	Private _lista_DistritoPL As New List(Of Entidades.beCboDistrito)

	Public Property getObjImpuesto() As Entidades.Impuesto
		Get
			Return Me.objImpuesto
		End Get
		Set(ByVal value As Entidades.Impuesto)
			Me.objImpuesto = value
		End Set
	End Property

	Public Property IdDocumentoRef() As Integer
		Get
			Return Me._IdDocumentoRef
		End Get
		Set(ByVal value As Integer)
			Me._IdDocumentoRef = value
		End Set
	End Property

	Public Property getObjMaestroObra() As Entidades.PersonaView
		Get
			Return Me.objMaestroObra
		End Get
		Set(ByVal value As Entidades.PersonaView)
			Me.objMaestroObra = value
		End Set
	End Property

	Public Property getObjPersonaView() As Entidades.PersonaView
		Get
			Return Me.objPersonaView
		End Get
		Set(ByVal value As Entidades.PersonaView)
			Me.objPersonaView = value
		End Set
	End Property

	Public Property getObjTipoAgente() As Entidades.TipoAgente
		Get
			Return Me.objTipoAgente
		End Get
		Set(ByVal value As Entidades.TipoAgente)
			Me.objTipoAgente = value
		End Set
	End Property

	Public Property getObjAnexoDocumento() As Entidades.Anexo_Documento
		Get
			Return Me.objAnexo_Documento
		End Get
		Set(ByVal value As Entidades.Anexo_Documento)
			Me.objAnexo_Documento = value
		End Set
	End Property

	Public Property lista_Productos() As List(Of DetalleDocumento)
		Get
			Return Me._listaProductos
		End Get
		Set(ByVal value As List(Of DetalleDocumento))
			Me._listaProductos = value
		End Set
	End Property

	Public Property lista_DetalleConcepto() As List(Of DetalleConcepto)
		Get
			Return Me._listaDetalleConcepto
		End Get
		Set(ByVal value As List(Of DetalleConcepto))
			Me._listaDetalleConcepto = value
		End Set
	End Property

	Public Property PuntoPartida() As Entidades.PuntoPartida
		Get
			Return Me._PuntoPartida
		End Get
		Set(ByVal value As Entidades.PuntoPartida)
			Me._PuntoPartida = value
		End Set
	End Property

	Public Property PuntoLlegada() As Entidades.PuntoLlegada
		Get
			Return Me._PuntoLlegada
		End Get
		Set(ByVal value As Entidades.PuntoLlegada)
			Me._PuntoLlegada = value
		End Set
	End Property

	Public Property Observacion() As Entidades.Observacion
		Get
			Return Me._Observacion
		End Get
		Set(ByVal value As Entidades.Observacion)
			Me._Observacion = value
		End Set
	End Property

	Public Property lista_RelacionDocumento() As List(Of Entidades.Documento)
		Get
			Return Me._lista_RelacionDocumento
		End Get
		Set(ByVal value As List(Of Entidades.Documento))
			Me._lista_RelacionDocumento = value
		End Set
	End Property

	Public Property IGV() As Decimal
		Get
			Return Me._IGV
		End Get
		Set(ByVal value As Decimal)
			Me._IGV = value
		End Set
	End Property

	Public Property lista_MotivoTraslado() As List(Of Entidades.beCampoEntero)
		Get
			Return Me._lista_MotivoTraslado
		End Get
		Set(ByVal value As List(Of Entidades.beCampoEntero))
			Me._lista_MotivoTraslado = value
		End Set
	End Property

	Public Property lista_MedioPago() As List(Of Entidades.beCampoEntero)
		Get
			Return Me._lista_MedioPago
		End Get
		Set(ByVal value As List(Of Entidades.beCampoEntero))
			Me._lista_MedioPago = value
		End Set
	End Property

	Public Property lista_CuentaPersona() As List(Of Entidades.CuentaPersona)
		Get
			Return Me._lista_CuentaPersona
		End Get
		Set(ByVal value As List(Of Entidades.CuentaPersona))
			Me._lista_CuentaPersona = value
		End Set
	End Property

	Public Property lista_ProvinciaPP() As List(Of Entidades.beCboProvincia)
		Get
			Return Me._lista_ProvinciaPP
		End Get
		Set(ByVal value As List(Of Entidades.beCboProvincia))
			Me._lista_ProvinciaPP = value
		End Set
	End Property

	Public Property lista_DistritoPP() As List(Of Entidades.beCboDistrito)
		Get
			Return Me._lista_DistritoPP
		End Get
		Set(ByVal value As List(Of Entidades.beCboDistrito))
			Me._lista_DistritoPP = value
		End Set
	End Property

	Public Property lista_ProvinciaPL() As List(Of Entidades.beCboProvincia)
		Get
			Return Me._lista_ProvinciaPL
		End Get
		Set(ByVal value As List(Of Entidades.beCboProvincia))
			Me._lista_ProvinciaPL = value
		End Set
	End Property

	Public Property lista_DistritoPL() As List(Of Entidades.beCboDistrito)
		Get
			Return Me._lista_DistritoPL
		End Get
		Set(ByVal value As List(Of Entidades.beCboDistrito))
			Me._lista_DistritoPL = value
		End Set
	End Property

End Class

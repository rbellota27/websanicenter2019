﻿Public Class ImportacionExcel

    Private _NombreArchivo As String
    Private _NombreHoja As String
    Private _IdEstructuraBanco As Integer
    
    Public Sub New()
    End Sub

    Public Property NombreArchivo() As String
        Get
            Return Me._NombreArchivo
        End Get
        Set(ByVal value As String)
            Me._NombreArchivo = value
        End Set
    End Property
    Public Property NombreHoja() As String
        Get
            Return Me._NombreHoja
        End Get
        Set(ByVal value As String)
            Me._NombreHoja = value
        End Set
    End Property
    Public Property IdEstructuraBanco() As Integer
        Get
            Return Me._IdEstructuraBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdEstructuraBanco = value
        End Set
    End Property
End Class

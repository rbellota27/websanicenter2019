﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'************************   MIERCOLES 07  04  2010 HORA 04_10 PM



Public Class VolumenVenta

    Private _IdProducto As Integer
    Private _IdTipoPV As Integer
    Private _IdTienda As Integer
    Private _vven_CantidadMin_UMPr As Decimal
    Private _vven_FechaRegistro As Date

    Private _TipoPV As String
    Private _TipoPrecioV_Ref As String
    Private _Tienda As String

    Public Property TipoPrecioV_Ref() As String
        Get
            Return Me._TipoPrecioV_Ref
        End Get
        Set(ByVal value As String)
            Me._TipoPrecioV_Ref = value
        End Set
    End Property

    Public Property TipoPV() As String
        Get
            Return Me._TipoPV
        End Get
        Set(ByVal value As String)
            Me._TipoPV = value
        End Set
    End Property
    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property


    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property
    Public Property IdTipoPV() As Integer
        Get
            Return Me._IdTipoPV
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV = value
        End Set
    End Property
    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property
    Public Property vven_CantidadMin_UMPr() As Decimal
        Get
            Return Me._vven_CantidadMin_UMPr
        End Get
        Set(ByVal value As Decimal)
            Me._vven_CantidadMin_UMPr = value
        End Set
    End Property
    Public Property vven_FechaRegistro() As Date
        Get
            Return Me._vven_FechaRegistro
        End Get
        Set(ByVal value As Date)
            Me._vven_FechaRegistro = value
        End Set
    End Property

End Class


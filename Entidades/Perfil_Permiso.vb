﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Perfil_Permiso
    Private _IdPerfil As Integer
    Private _IdPermiso As Integer
    Private _Estado As Boolean


    Private _DescPermiso As String

    Public Property DescPermiso() As String
        Get
            Return Me._DescPermiso
        End Get
        Set(ByVal value As String)
            Me._DescPermiso = value
        End Set
    End Property





    Public Property IdPerfil() As Integer
        Get
            Return Me._IdPerfil
        End Get
        Set(ByVal value As Integer)
            Me._IdPerfil = value
        End Set
    End Property

    Public Property IdPermiso() As Integer
        Get
            Return Me._IdPermiso
        End Get
        Set(ByVal value As Integer)
            Me._IdPermiso = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As Boolean)
            Me._Estado = value
        End Set
    End Property


End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Modelo
    Private _IdModelo As Integer
    Private _m_Nombre As String
    Private _m_Estado As Boolean
    Private _descEstado As String
    Private _cct_Estado As String
    Private _m_Codigo As String


    Public Sub New()
    End Sub
    Public Sub New(ByVal IdModelo As Integer, ByVal Nombre As String)
        Me.Id = IdModelo
        Me.m_Nombre = Nombre
    End Sub
    Public Sub New(ByVal IdModelo As String, ByVal nommodelo As String, ByVal estado As Boolean)
        _IdModelo = IdModelo
        _m_Nombre = nommodelo
        _m_Estado = estado
    End Sub
    Public Property IdModelo() As String
        Get
            Return _IdModelo
        End Get
        Set(ByVal value As String)
            _IdModelo = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me.IdModelo
        End Get
        Set(ByVal value As Integer)
            Me.IdModelo = value
        End Set
    End Property
    Public Property m_Nombre() As String
        Get
            Return _m_Nombre
        End Get
        Set(ByVal value As String)
            _m_Nombre = value
        End Set
    End Property
    Public ReadOnly Property Estado_Mod()
        Get
            Me._descEstado = "0"
            If Me.Estado = True Then
                Me._descEstado = "1"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Estado() As Boolean
        Get
            Return _m_Estado
        End Get
        Set(ByVal value As Boolean)
            _m_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            _descEstado = "Inactivo"
            If Estado = True Then
                _descEstado = "Activo"
            End If
            Return _descEstado
        End Get
    End Property
    Public Property m_Codigo() As String
        Get
            Return _m_Codigo
        End Get
        Set(ByVal value As String)
            _m_Codigo = value
        End Set
    End Property

End Class

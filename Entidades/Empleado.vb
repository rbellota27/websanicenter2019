'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 30-Set-2009
'Hora    : 03:15 pm
'*************************************************


Public Class Empleado
    Private _IdPersona As Integer
    Private _emp_Sueldo As Decimal
    Private _IdCatEmpleado As Integer
    Private _emp_FechaBaja As Date
    Private _emp_FechaAlta As Date
    Private _emp_Estado As String
    Private _IdMotivoBaja As Integer
    Private _Nombre As String
    Private _DNI As String
    Private _NomCargo As String
    Private _EsEmpleado As Boolean
    Private _EstaReg As Integer

    Public Property EstaReg() As Integer
        Get
            Return _EstaReg
        End Get
        Set(ByVal value As Integer)
            _EstaReg = value
        End Set
    End Property

    Public ReadOnly Property FechadeAlta() As String
        Get
            If FechaAlta = Nothing Then
                Return ""
            Else
                Return FechaAlta.ToShortDateString
            End If
        End Get
    End Property

    Public ReadOnly Property FechadeBaja() As String
        Get
            If FechaBaja = Nothing Then
                Return ""
            Else
                Return FechaBaja.ToShortDateString
            End If
        End Get
    End Property

    Public Property EsEmpleado() As Boolean
        Get
            Return _EsEmpleado
        End Get
        Set(ByVal value As Boolean)
            _EsEmpleado = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._Nombre
        End Get
        Set(ByVal value As String)
            Me._Nombre = value
        End Set
    End Property

    Public Property DNI() As String
        Get
            Return Me._DNI
        End Get
        Set(ByVal value As String)
            Me._DNI = value
        End Set
    End Property

    Public Property NomCargo() As String
        Get
            Return Me._NomCargo
        End Get
        Set(ByVal value As String)
            Me._NomCargo = value
        End Set
    End Property





    Public Property IdMotivoBaja() As Integer
        Get
            Return Me._IdMotivoBaja
        End Get
        Set(ByVal value As Integer)
            Me._IdMotivoBaja = value
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return Me._emp_Estado
        End Get
        Set(ByVal value As String)
            Me._emp_Estado = value
        End Set
    End Property
    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property Sueldo() As Decimal
        Get
            Return Me._emp_Sueldo
        End Get
        Set(ByVal value As Decimal)
            Me._emp_Sueldo = value
        End Set
    End Property

    Public Property IdCatEmpleado() As Integer
        Get
            Return Me._IdCatEmpleado
        End Get
        Set(ByVal value As Integer)
            Me._IdCatEmpleado = value
        End Set
    End Property

    Public Property FechaBaja() As Date
        Get
            Return Me._emp_FechaBaja
        End Get
        Set(ByVal value As Date)
            Me._emp_FechaBaja = value
        End Set
    End Property
    Public Property FechaAlta() As Date
        Get
            Return Me._emp_FechaAlta
        End Get
        Set(ByVal value As Date)
            Me._emp_FechaAlta = value
        End Set
    End Property
End Class

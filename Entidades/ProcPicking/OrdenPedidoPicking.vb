﻿Public Class OrdenPedidoPicking
    Private _Marcar As Boolean = True
    Private _iddocumento As Integer = 0
    Private _NroOPV As String = String.Empty
    Private _doc_FechaEmision As String = String.Empty
    Private _tie_Nombre As String = String.Empty
    Private _FechaHoraRegistro As DateTime
    'Private _IdTipoOperacion As Integer = 0
    Private _CadTipoDocumentoRef As String = String.Empty
    Private _TipoOperacion As String = String.Empty


    Public Property Marcar() As Boolean
        Get
            Return _Marcar
        End Get
        Set(value As Boolean)
            _Marcar = value
        End Set

    End Property

    Public Property iddocumento() As Integer
        Get
            Return _iddocumento
        End Get
        Set(value As Integer)
            _iddocumento = value
        End Set
    End Property

    Public Property NroOPV() As String
        Get
            Return _NroOPV
        End Get
        Set(value As String)
            _NroOPV = value
        End Set
    End Property

    Public Property doc_FechaEmision() As String
        Get
            Return _doc_FechaEmision
        End Get
        Set(value As String)
            _doc_FechaEmision = value
        End Set
    End Property

    Public Property tie_Nombre() As String
        Get
            Return _tie_Nombre
        End Get
        Set(value As String)
            _tie_Nombre = value
        End Set
    End Property

    Public Property FechaHoraRegistro() As DateTime
        Get
            Return _FechaHoraRegistro
        End Get
        Set(value As DateTime)
            _FechaHoraRegistro = value
        End Set
    End Property

    'Public Property IdTipoOperacion() As Integer
    '    Get
    '        Return _IdTipoOperacion
    '    End Get
    '    Set(value As Integer)
    '        _IdTipoOperacion = value
    '    End Set
    'End Property


    Public Property CadTipoDocumentoRef() As String
        Get
            Return _CadTipoDocumentoRef
        End Get
        Set(value As String)
            _CadTipoDocumentoRef = value
        End Set
    End Property


    Public Property TipoOperacion() As String
        Get
            Return _TipoOperacion
        End Get
        Set(value As String)
            _TipoOperacion = value
        End Set
    End Property


End Class

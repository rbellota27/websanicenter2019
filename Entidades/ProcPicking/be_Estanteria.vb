﻿Public Class be_Estanteria
    Private _IdEstanteria As Integer = 0
    Private _NombreEstanteria As String = String.Empty


    Public Property IdEstanteria() As Integer
        Get
            Return _IdEstanteria
        End Get
        Set(value As Integer)
            _IdEstanteria = value
        End Set
    End Property


    Public Property NombreEstanteria() As String
        Get
            Return _NombreEstanteria
        End Get
        Set(value As String)
            _NombreEstanteria = value
        End Set
    End Property



End Class

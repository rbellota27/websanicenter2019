'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Caja
    Private _IdCaja As Integer
    Private _caja_Nombre As String
    Private _caja_Estado As String
    Private _IdTienda As Integer
    Private _IdEmpresa As Integer
    Private _NomTienda As String
    Private _Caja_Numero As String
    Private _EstadoTC As Boolean
    Private _descEstado As String
    Private _Caja_Chica As Boolean

    Public Sub New()
    End Sub
    Public Sub New(ByVal idCaja As Integer, ByVal nomCaja As String)
        Me.IdCaja = idCaja
        Me.Nombre = nomCaja
    End Sub
    Public Sub New(ByVal idcaja As Integer, ByVal nomcaja As String, ByVal idtienda As Integer, ByVal nomtienda As String, ByVal estado As String)
        Me.IdCaja = idcaja
        Me.Nombre = nomcaja
        Me.IdTienda = idtienda
        Me.NomTienda = nomtienda
        Me.Estado = estado
    End Sub
    Public Sub New(ByVal idcaja As Integer, ByVal idtienda As Integer, ByVal nomcaja As String, ByVal nrocaja As String, ByVal estado As String)
        Me.IdCaja = idcaja
        Me.IdTienda = idtienda
        Me.Nombre = nomcaja
        Me.Estado = estado
        Me.CajaNumero = nrocaja
    End Sub

    Public Sub New(ByVal idcaja As Integer, ByVal idtienda As Integer, ByVal nomcaja As String, ByVal nrocaja As String, ByVal EstadoTC As Boolean)
        Me.IdCaja = idcaja
        Me.IdTienda = idtienda
        Me.Nombre = nomcaja
        Me.EstadoTC = EstadoTC
        Me.CajaNumero = nrocaja
    End Sub

    Public Property Caja_Chica() As Boolean
        Get
            Return _Caja_Chica
        End Get
        Set(ByVal value As Boolean)
            Me._Caja_Chica = value
        End Set
    End Property

    Public Property IdCaja() As Integer
        Get
            Return Me._IdCaja
        End Get
        Set(ByVal value As Integer)
            Me._IdCaja = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._caja_Nombre
        End Get
        Set(ByVal value As String)
            Me._caja_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._caja_Estado
        End Get
        Set(ByVal value As String)
            Me._caja_Estado = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property
    Public Property NomTienda() As String
        Get
            Return Me._NomTienda
        End Get
        Set(ByVal value As String)
            Me._NomTienda = value
        End Set
    End Property
    Public ReadOnly Property getEstadoBoolean() As Boolean
        Get
            If Me.Estado = "0" Then
                Return False
            ElseIf Me.Estado = "1" Then
                Return True
            End If
        End Get
    End Property
    Public Property CajaNumero() As String
        Get
            Return _Caja_Numero
        End Get
        Set(ByVal value As String)
            _Caja_Numero = value
        End Set
    End Property
    Public Property EstadoTC() As Boolean
        Get
            Return _EstadoTC
        End Get
        Set(ByVal value As Boolean)
            _EstadoTC = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._caja_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
End Class

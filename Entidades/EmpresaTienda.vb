'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.




Public Class EmpresaTienda
    Private _IdTienda As Integer
    Private _IdEmpresa As Integer
    Private _et_CuentaContable As String
    Private _et_CentroCosto As String
    Private _Nombre As String
    Private _Estado As Boolean
    Public Sub New()
    End Sub

    Public Sub New(ByVal idtienda As Integer, ByVal nombre As String, ByVal idempresa As Integer, ByVal cuentacontable As String, ByVal centrocosto As String, ByVal estado As Boolean)
        Me.Id = idtienda
        Me.Nombre = nombre
        Me.IdEmpresa = idempresa
        Me.CuentaContable = cuentacontable
        Me.CentroCosto = centrocosto
        Me.Estado = estado
    End Sub
    Public Property Estado() As Boolean
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As Boolean)
            Me._Estado = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property
    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property CuentaContable() As String
        Get
            Return Me._et_CuentaContable
        End Get
        Set(ByVal value As String)
            Me._et_CuentaContable = value
        End Set
    End Property

    Public Property CentroCosto() As String
        Get
            Return Me._et_CentroCosto
        End Get
        Set(ByVal value As String)
            Me._et_CentroCosto = value
        End Set
    End Property
End Class

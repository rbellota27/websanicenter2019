'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Auditoria
    Private _IdAuditoria As Integer
    Private _IdPersona As Integer
    Private _aud_NomTabla As String
    Private _aud_IdTabla As String
    Private _aud_FechaTrans As Date
    Private _aud_TipoTransaccion As String

    Public Property IdAuditoria() As Integer
        Get
            Return Me._IdAuditoria
        End Get
        Set(ByVal value As Integer)
            Me._IdAuditoria = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property NomTabla() As String
        Get
            Return Me._aud_NomTabla
        End Get
        Set(ByVal value As String)
            Me._aud_NomTabla = value
        End Set
    End Property

    Public Property IdTabla() As String
        Get
            Return Me._aud_IdTabla
        End Get
        Set(ByVal value As String)
            Me._aud_IdTabla = value
        End Set
    End Property

    Public Property FechaTrans() As Date
        Get
            Return Me._aud_FechaTrans
        End Get
        Set(ByVal value As Date)
            Me._aud_FechaTrans = value
        End Set
    End Property
    Public Property TipoTransaccion() As String
        Get
            Return Me._aud_TipoTransaccion
        End Get
        Set(ByVal value As String)
            Me._aud_TipoTransaccion = value
        End Set
    End Property
End Class

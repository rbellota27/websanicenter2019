'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Persona
    Private _IdPersona As Integer
    Private _per_NComercial As String
    Private _per_WebSite As String
    Private _per_FechaAlta As Date
    Private _per_FechaBaja As Date
    Private _per_estado As String
    Private _IdGiro As Integer
    Private _IdCentroLaboral As Integer
    Private _IdNacionalidad As Integer
    Private _IdRepresentanteL As Integer
    Private _IdMotivoBaja As Integer
    Private _IdContacto As Integer
    Private _per_Propietario As Boolean
    Private _descEstado As String
    Private _IdTipoPV As Integer

    Public Property Propietario() As Boolean
        Get
            Return Me._per_Propietario
        End Get
        Set(ByVal value As Boolean)
            Me._per_Propietario = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdRepresentanteL() As Integer
        Get
            Return Me._IdRepresentanteL
        End Get
        Set(ByVal value As Integer)
            Me._IdRepresentanteL = value
        End Set
    End Property

    Public Property IdCentroLaboral() As Integer
        Get
            Return Me._IdCentroLaboral
        End Get
        Set(ByVal value As Integer)
            Me._IdCentroLaboral = value
        End Set
    End Property

    Public Property IdContacto() As Integer
        Get
            Return Me._IdContacto
        End Get
        Set(ByVal value As Integer)
            Me._IdContacto = value
        End Set
    End Property

    Public Property NComercial() As String
        Get
            Return Me._per_NComercial
        End Get
        Set(ByVal value As String)
            Me._per_NComercial = value
        End Set
    End Property



    Public Property WebSite() As String
        Get
            Return Me._per_WebSite
        End Get
        Set(ByVal value As String)
            Me._per_WebSite = value
        End Set
    End Property

    Public Property FechaAlta() As Date
        Get
            Return Me._per_FechaAlta
        End Get
        Set(ByVal value As Date)
            Me._per_FechaAlta = value
        End Set
    End Property

    Public Property FechaBaja() As Date
        Get
            Return Me._per_FechaBaja
        End Get
        Set(ByVal value As Date)
            Me._per_FechaBaja = value
        End Set
    End Property

    Public ReadOnly Property FechadeAlta() As String
        Get
            If FechaAlta = Nothing Then
                Return ""
            Else
                Return FechaAlta.ToShortDateString
            End If
        End Get
    End Property

    Public ReadOnly Property FechadeBaja() As String
        Get
            If FechaBaja = Nothing Then
                Return ""
            Else
                Return FechaBaja.ToShortDateString
            End If
        End Get
    End Property

    Public Property Estado() As String
        Get
            Return Me._per_estado
        End Get
        Set(ByVal value As String)
            Me._per_estado = value
        End Set
    End Property

    Public Property IdGiro() As Integer
        Get
            Return Me._IdGiro
        End Get
        Set(ByVal value As Integer)
            Me._IdGiro = value
        End Set
    End Property

    Public Property IdNacionalidad() As Integer
        Get
            Return Me._IdNacionalidad
        End Get
        Set(ByVal value As Integer)
            Me._IdNacionalidad = value
        End Set
    End Property

    Public Property IdMotivoBaja() As Integer
        Get
            Return Me._IdMotivoBaja
        End Get
        Set(ByVal value As Integer)
            Me._IdMotivoBaja = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._per_estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property IdTipoPV() As Integer
        Get
            Return Me._IdTipoPV
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV = value
        End Set
    End Property
End Class

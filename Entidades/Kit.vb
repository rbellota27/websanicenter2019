﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************   MIERCOLES 02 06 2010

Public Class Kit

    '***************** ATRIBUTOS BD
    Private _IdKit As Integer
    Private _IdComponente As Integer
    Private _Cantidad_Comp As Decimal
    Private _IdUnidadMedida_Comp As Integer
    Private _IdMonedaPrecio_Comp As Integer
    Private _Precio_Comp As Decimal

    '********************  ATRIBUTOS ADICIONALES
    Private _Componente As String
    Private _UnidadMedida_Comp As String
    Private _CodigoProd_Comp As String
    Private _MonedaPrecio_Comp As String
    Private _CantidadTotal_Comp As Decimal
    Private _PrecioUnitEqKit_Comp As Decimal
    Private _PrecioListaUnitEqKit_Comp As Decimal
    Private _PrecioComercialUnitEqKit_Comp As Decimal
    Private _listaProductoUM As List(Of Entidades.ProductoUMView)
    Private _listaMoneda As List(Of Entidades.Moneda)
    Private _porcentkit As Integer

    Public Sub New()

    End Sub

    Public Sub New(ByVal xIdKit As Integer)
        Me.IdKit = xIdKit
    End Sub


    Public Property ListaMoneda() As List(Of Entidades.Moneda)
        Get
            Return Me._listaMoneda
        End Get
        Set(ByVal value As List(Of Entidades.Moneda))
            Me._listaMoneda = value
        End Set
    End Property
    Public Property ListaProductoUM() As List(Of Entidades.ProductoUMView)
        Get
            Return Me._listaProductoUM
        End Get
        Set(ByVal value As List(Of Entidades.ProductoUMView))
            Me._listaProductoUM = value
        End Set
    End Property

    Public Property CantidadTotal_Comp() As Decimal
        Get
            Return Me._CantidadTotal_Comp
        End Get
        Set(ByVal value As Decimal)
            Me._CantidadTotal_Comp = value
        End Set
    End Property
    Public Property PorcentajeKit() As Integer
        Get
            Return Me._porcentkit
        End Get
        Set(ByVal value As Integer)
            Me._porcentkit = value
        End Set
    End Property

    Public Property PrecioUnitEqKit_Comp() As Decimal
        Get
            Return Me._PrecioUnitEqKit_Comp
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioUnitEqKit_Comp = value
        End Set
    End Property

    Public Property PrecioListaUnitEqKit_Comp() As Decimal
        Get
            Return Me._PrecioListaUnitEqKit_Comp
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioListaUnitEqKit_Comp = value
        End Set
    End Property

    Public Property PrecioComercialUnitEqKit_Comp() As Decimal
        Get
            Return Me._PrecioComercialUnitEqKit_Comp
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioComercialUnitEqKit_Comp = value
        End Set
    End Property

    Public Property MonedaPrecio_Comp() As String
        Get
            Return Me._MonedaPrecio_Comp
        End Get
        Set(ByVal value As String)
            Me._MonedaPrecio_Comp = value
        End Set
    End Property


    Public Property IdMonedaPrecio_Comp() As Integer
        Get
            Return Me._IdMonedaPrecio_Comp
        End Get
        Set(ByVal value As Integer)
            Me._IdMonedaPrecio_Comp = value
        End Set
    End Property

    Public Property Precio_Comp() As Decimal
        Get
            Return Me._Precio_Comp
        End Get
        Set(ByVal value As Decimal)
            Me._Precio_Comp = value
        End Set
    End Property
    Public Property CodigoProd_Comp() As String
        Get
            Return Me._CodigoProd_Comp
        End Get
        Set(ByVal value As String)
            Me._CodigoProd_Comp = value
        End Set
    End Property


    Public Property IdKit() As Integer
        Get
            Return Me._IdKit
        End Get
        Set(ByVal value As Integer)
            Me._IdKit = value
        End Set
    End Property

    Public Property IdComponente() As Integer
        Get
            Return Me._IdComponente
        End Get
        Set(ByVal value As Integer)
            Me._IdComponente = value
        End Set
    End Property

    Public Property Cantidad_Comp() As Decimal
        Get
            Return Me._Cantidad_Comp
        End Get
        Set(ByVal value As Decimal)
            Me._Cantidad_Comp = value
        End Set
    End Property

    Public Property IdUnidadMedida_Comp() As Integer
        Get
            Return Me._IdUnidadMedida_Comp
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida_Comp = value
        End Set
    End Property

    Public Property Componente() As String
        Get
            Return Me._Componente
        End Get
        Set(ByVal value As String)
            Me._Componente = value
        End Set
    End Property

    Public Property UnidadMedida_Comp() As String
        Get
            Return Me._UnidadMedida_Comp
        End Get
        Set(ByVal value As String)
            Me._UnidadMedida_Comp = value
        End Set
    End Property

End Class

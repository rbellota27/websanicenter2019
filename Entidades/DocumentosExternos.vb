﻿

Public Class DocumentosExternos


    Private _IdDocumento As Integer = 0
    Private _Serie As String = String.Empty
    Private _Codigo As String = String.Empty
    Private _FechaEmision As String = String.Empty
    Private _TipoDoc As String = String.Empty
    Private _nroVoucher As String = String.Empty
    Private _DocTotal As Decimal = 0
    Private _Montoafecto As Decimal = 0
    Private _MontoInafecto As Decimal=0
    Private _Amortizado As Decimal = 0
    Private _Deuda As Decimal = 0
    Private _Proveedor As String = String.Empty
    Private _Usuario As String = String.Empty
    Private _Requerimiento As String = String.Empty



    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDocumento = value
        End Set
    End Property



    Public Property Serie() As String
        Get
            Return _Serie
        End Get
        Set(ByVal value As String)
            _Serie = value
        End Set
    End Property



    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property

    Public Property tipoDoc() As String
        Get
            Return _TipoDoc
        End Get
        Set(ByVal value As String)
            _TipoDoc = value
        End Set
    End Property

    Public Property FechaEmision() As String
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As String)
            _FechaEmision = value
        End Set
    End Property


    Public Property nroVoucher() As String
        Get
            Return _nroVoucher
        End Get
        Set(ByVal value As String)
            _nroVoucher = value
        End Set
    End Property

    Public Property DocTotal() As Decimal
        Get
            Return _DocTotal
        End Get
        Set(ByVal value As Decimal)
            _DocTotal = value
        End Set
    End Property

    Public Property Montoafecto() As Decimal
        Get
            Return _Montoafecto
        End Get
        Set(ByVal value As Decimal)
            _Montoafecto = value
        End Set
    End Property

    Public Property MontoInafecto() As Decimal
        Get
            Return _MontoInafecto
        End Get
        Set(ByVal value As Decimal)
            _MontoInafecto = value
        End Set
    End Property

    Public Property Amortizado() As Decimal
        Get
            Return _Amortizado
        End Get
        Set(ByVal value As Decimal)
            _Amortizado = value
        End Set
    End Property

    Public Property Deuda() As Decimal
        Get
            Return _Deuda
        End Get
        Set(ByVal value As Decimal)
            _Deuda = value
        End Set
    End Property

    Public Property Proveedor() As String
        Get
            Return _Proveedor
        End Get
        Set(ByVal value As String)
            _Proveedor = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return _Usuario
        End Get
        Set(ByVal value As String)
            _Usuario = value
        End Set
    End Property

    Public Property Reuerimiento() As String
        Get
            Return _Requerimiento
        End Get
        Set(ByVal value As String)
            _Requerimiento = value
        End Set
    End Property

    

End Class
﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 03/03/2010 3:14 pm

'No cambiar esta clase, esta mapeada con la tabla correspondiente en la BD,
'para agregar cosas utilice la clase SerieChequeView

Public Class SerieCheque
    Implements System.ICloneable

    Protected _IdSerieCheque As Integer
    Protected _IdBanco As Integer
    Protected _IdCuentaBancaria As Integer
    Protected _sc_Serie As String        'tamaño 5
    Protected _sc_NroInicio As Integer
    Protected _sc_NroFin As Integer
    Protected _sc_Longitud As Integer
    'Para trabajar con este estado utilicen la propiedad Estado, que trabaja solo con Boolean (esta validado)
    Protected _sc_Estado As Boolean?
    Protected _sc_Descripcion As String     'tamaño 100

    Sub New()
        Me._IdSerieCheque = 0
        Me._IdBanco = 0
        Me._IdCuentaBancaria = 0
        Me._sc_Serie = ""
        Me._sc_NroInicio = 0
        Me._sc_NroFin = 0
        Me._sc_Longitud = 0
        Me._sc_Estado = Nothing
        Me._sc_Descripcion = ""
    End Sub

    Sub New(ByVal IdSerieCheque As Integer, _
            ByVal IdBanco As Integer, _
            ByVal IdCuentaBancaria As Integer, _
            ByVal sc_Serie As String, _
            ByVal sc_NroInicio As Integer, _
            ByVal sc_NroFin As Integer, _
            ByVal sc_Longitud As Integer, _
            ByVal sc_Estado As Boolean, _
            ByVal sc_Descripcion As String)

        Me._IdSerieCheque = IdSerieCheque
        Me._IdBanco = IdBanco
        Me._IdCuentaBancaria = IdCuentaBancaria
        Me._sc_Serie = sc_Serie
        Me._sc_NroInicio = sc_NroInicio
        Me._sc_NroFin = sc_NroFin
        Me._sc_Longitud = sc_Longitud
        Me._sc_Estado = sc_Estado
        Me._sc_Descripcion = sc_Descripcion
    End Sub

    Public Property Id() As Integer
        Get
            Return Me._IdSerieCheque
        End Get
        Set(ByVal value As Integer)
            Me._IdSerieCheque = value
        End Set
    End Property
    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property
    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property

    Public Property Serie() As String
        Get
            Return Me._sc_Serie
        End Get
        Set(ByVal value As String)
            Me._sc_Serie = value
        End Set
    End Property
    Public Property NroInicio() As Integer
        Get
            Return Me._sc_NroInicio
        End Get
        Set(ByVal value As Integer)
            Me._sc_NroInicio = value
        End Set
    End Property

    Public Property NroFin() As Integer
        Get
            Return Me._sc_NroFin
        End Get
        Set(ByVal value As Integer)
            Me._sc_NroFin = value
        End Set
    End Property

    Public ReadOnly Property DescNroInicio() As String
        Get
            Try
                'Return String.Format("{0:" + Me.Longitud.ToString + "}", Me.NroInicio)
                Return Me.NroInicio.ToString.PadLeft(Me.Longitud, "0"c)
            Catch ex As Exception
                Return "Error: Longitud= " + Me.Longitud
            End Try

        End Get
    End Property
    Public ReadOnly Property DescNroFin() As String
        Get
            Try
                Return Me.NroFin.ToString.PadLeft(Me.Longitud, "0"c)
                'Return String.Format("{0:" + Me.Longitud.ToString + "}", Me.NroFin)
            Catch ex As Exception
                Return "Error: Longitud= " + Me.Longitud
            End Try
        End Get
    End Property

    Public Property SerieYNumeracion() As String
        Get
            Return Me._sc_Serie + " / " + DescNroInicio + " - " + DescNroFin
        End Get
        Set(ByVal value As String)
            Me._sc_Serie = value
        End Set
    End Property
    Public Property Longitud() As Integer
        Get
            Return Me._sc_Longitud
        End Get
        Set(ByVal value As Integer)
            Me._sc_Longitud = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            If Not Me._sc_Estado.HasValue Then
                Me._sc_Estado = True    'La mayoria de los registros tienen el estado activado
            End If
            Return Me._sc_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._sc_Estado = value
        End Set
    End Property
    Public Property EstadoNullable() As Boolean?
        Get
            Return Me._sc_Estado
        End Get
        Set(ByVal value As Boolean?)
            Me._sc_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            If Estado Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public ReadOnly Property DescEstadoNum() As String
        Get
            If Estado Then
                Return "1"
            Else
                Return "0"
            End If
        End Get
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._sc_Descripcion
        End Get
        Set(ByVal value As String)
            Me._sc_Descripcion = value
        End Set
    End Property

    Public Overridable Function Clone() As Object Implements System.ICloneable.Clone
        Dim x As New SerieCheque

        x._IdSerieCheque = Me._IdSerieCheque
        x._IdBanco = Me._IdBanco
        x._IdCuentaBancaria = Me._IdCuentaBancaria
        x._sc_Serie = Me._sc_Serie
        x._sc_NroInicio = Me._sc_NroInicio
        x._sc_NroFin = Me._sc_NroFin
        x._sc_Longitud = Me._sc_Longitud
        x._sc_Estado = Me._sc_Estado
        x._sc_Descripcion = Me._sc_Descripcion

        Return x
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmDocCancelacionCaja
    Inherits System.Web.UI.Page


    Public hddIdDocumentoExt As Integer = 0

#Region "Atributos"

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo
    Private ListaDetalleConcepto As List(Of Entidades.DetalleConcepto)
    Private NegFechaActual As Negocio.FechaActual ' = Nothing
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private Const _Opcion_Otros As Integer = 2
    Private listaDocumentoRef_Sustento As List(Of Entidades.Documento)

    Private Property ListaCancelacion() As List(Of Entidades.DatosCancelacion)
        Get
            Return CType(Session.Item("ListaCancelacion"), List(Of Entidades.DatosCancelacion))
        End Get
        Set(ByVal value As List(Of Entidades.DatosCancelacion))
            Session.Remove("ListaCancelacion")
            If value IsNot Nothing Then
                Session.Add("ListaCancelacion", value)
            End If
        End Set
    End Property

    Private Property ListaPagoCaja() As List(Of Entidades.PagoCaja)
        Get
            Return CType(Session.Item("ListaPagoCaja"), List(Of Entidades.PagoCaja))
        End Get
        Set(ByVal value As List(Of Entidades.PagoCaja))
            Session.Remove("ListaPagoCaja")
            If value IsNot Nothing Then
                Session.Add("ListaPagoCaja", value)
            End If
        End Set
    End Property

    Private Property IdMedioPagoPrincipal() As Integer
        Get
            Return CInt(hddIdMedioPagoPrincipal.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdMedioPagoPrincipal.Value = CStr(value)
        End Set
    End Property

    Private Property IdMedioPagoInterfaz() As Integer
        Get
            Return CInt(hddIdMedioPagoInterfaz.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdMedioPagoInterfaz.Value = CStr(value)
        End Set
    End Property

    'Private Property ListaCheques() As List(Of Entidades.DocumentoCheque)
    '    Get
    '        Return CType(Session.Item("ListaCheques"), List(Of Entidades.DocumentoCheque))
    '    End Get
    '    Set(ByVal value As List(Of Entidades.DocumentoCheque))
    '        Session.Remove("ListaCheques")
    '        If value IsNot Nothing Then
    '            Session.Add("ListaCheques", value)
    '        End If
    '    End Set
    'End Property
    'Private Property ChequeEgreso() As Entidades.DocumentoCheque
    '    Get
    '        Return CType(Session.Item("ChequeEgreso"), Entidades.DocumentoCheque)
    '    End Get
    '    Set(ByVal value As Entidades.DocumentoCheque)
    '        Session.Remove("ChequeEgreso")
    '        If value IsNot Nothing Then
    '            Session.Add("ChequeEgreso", value)
    '        End If
    '    End Set
    'End Property
    Private Property IdPersona() As Integer
        Get
            Return CInt(hddIdPersona.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdPersona.Value = CStr(value)
        End Set
    End Property
    Private Property IdPersona02() As Integer
        Get
            Return CInt(hddIdPersona02.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdPersona02.Value = CStr(value)
        End Set
    End Property
    'Private Property IdCheque() As Integer
    '    Get
    '        Return CInt(ViewState("IdCheque"))
    '    End Get
    '    Set(ByVal value As Integer)
    '        ViewState.Remove("IdCheque")
    '        ViewState.Add("IdCheque", value)
    '    End Set
    'End Property
    Private Property Modo() As Integer
        Get
            Return CInt(hddFrmModo.Value)
        End Get
        Set(ByVal value As Integer)
            hddFrmModo.Value = CStr(value)
        End Set
    End Property
    Private Property IdDocumento() As Integer
        Get
            Return CInt(hddIdDocumento.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdDocumento.Value = CStr(value)
        End Set
    End Property

    'Private Property IdConceptoMovBanco() As Integer
    '    Get
    '        Return CInt(ViewState("IdConceptoMovBanco"))
    '    End Get
    '    Set(ByVal value As Integer)
    '        ViewState.Remove("IdConceptoMovBanco")
    '        ViewState.Add("IdConceptoMovBanco", value)
    '    End Set
    'End Property

    'Cambio 2010-04-20 1922
    'ini
    'p significa parametro, program y en otros casos persistencia (sesiones, viewstate)
    Private Property pIdTipoOperacion() As Integer
        Get
            Return CInt(cboTipoOperacion.SelectedValue)
        End Get
        Set(ByVal value As Integer)
            cboTipoOperacion.SelectedValue = CStr(value)
        End Set
    End Property
    'Private Const _IDTIPOOPERACION As Integer = 33      'Cancelacion Caja
    'fin

    Private Const _IDCONDICIONPAGO As Integer = 1       'Contado
    Private Const _IDTIPODOCUMENTO As Integer = 18      'RECIBO DE EGRESO
    Private Const _FACTOR As Integer = -1               'Egreso
    Private Const _IDTIPOMOVIMIENTO As Integer = 2      'Egreso

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
        Documento_Sustento = 7
    End Enum

#End Region

#Region "Funciones Auxiliares"
    Private Function DescFechaActual() As String
        Return Format(FechaActual, "dd/MM/yyyy")
    End Function
    Private Function FechaActual() As Date
        If NegFechaActual Is Nothing Then
            NegFechaActual = New Negocio.FechaActual
        End If
        Return NegFechaActual.SelectFechaActual
    End Function

    Private Sub GenerarCodigoDocumento()
        Try
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), _IDTIPODOCUMENTO)

            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'Private Sub actualizarControles_Caja()

    '    With objCbo
    '        .LlenarCboBanco(Me.cboBanco, False)
    '        .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
    '    End With
    'End Sub
#End Region

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Frm_OnLoad()
    End Sub

    Private Sub Frm_OnLoad()
        Try
            If (Not Me.IsPostBack) Then
                ConfigurarDatos()
                inicializarFrm()
                validarRequest()

                Validar_AperturaCierreCaja(False)

            End If
            visualizarMoneda()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub validarRequest()

        ' SUSTENTO 
        If (Request.QueryString("Modo") IsNot Nothing) And (Request.QueryString("IdDocumento_Sustentar") IsNot Nothing) Then
            If CInt(Request.QueryString("Modo")) = 7 Then
                buscarDocumento(Nothing, Nothing, CInt(Request.QueryString("IdDocumento_Sustentar")))
                OnClick_Sustentar()
            End If
        End If

        ' DOCUMENTO DE REFERENCIA
        If (Request.QueryString("IdDocumentoRef") IsNot Nothing) Then

            Dim IdDocumentoRef As Integer = CInt(Request.QueryString("IdDocumentoRef"))

            cargarDocumentoReferencia(IdDocumentoRef)

        End If

        ' NOTA CREDITO
        If (Request.QueryString("IdDocumentoNC") IsNot Nothing) Then

            Dim IdDocumentoNC As Integer = CInt(Request.QueryString("IdDocumentoNC"))
            Dim montoNC As Decimal = CDec(Request.QueryString("montoNC"))
            Dim IdMonedaNC As Integer = CInt(Request.QueryString("IdMonedaNC"))

            cargarDocumento_NC(IdDocumentoNC, montoNC, IdMonedaNC)

        End If

    End Sub

    Private Sub cargarDocumento_NC(ByVal IdDocumentoNC As Integer, ByVal montoNC As Decimal, ByVal IdMonedaNC As Integer)

        Try

            '*************** CARGAMOS EL DOCUMENTO DE REFERENCIA
            cargarDocumentoReferencia(IdDocumentoNC)

            '************** MONEDA
            If (Me.cboMoneda.Items.FindByValue(CStr(IdMonedaNC)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(IdMonedaNC)
                visualizarMoneda()
            End If

            Dim listaParametroGral() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {12, 13})  '*** Concepto / Tipo Operación
            Dim IdConcepto As Integer = CInt(listaParametroGral(0))
            Dim IdTipoOperacion As Integer = CInt(listaParametroGral(1))

            '*************** TIPO OPERACION
            If (Me.cboTipoOperacion.Items.FindByValue(CStr(IdTipoOperacion)) IsNot Nothing) Then

                Me.cboTipoOperacion.SelectedValue = CStr(IdTipoOperacion)

            End If

            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(True, 1)
            Me.ListaDetalleConcepto(0).IdConcepto = IdConcepto
            Me.ListaDetalleConcepto(0).IdMoneda = IdMonedaNC
            Me.ListaDetalleConcepto(0).Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
            Me.ListaDetalleConcepto(0).Monto = montoNC

            Me.lblMessageHelp.Visible = False

            If (Me.cboMotivoDocCancelacionCaja.Items.FindByValue(CStr(_Opcion_Otros)) IsNot Nothing) Then
                Me.cboMotivoDocCancelacionCaja.SelectedValue = CStr(_Opcion_Otros)
            End If

            Me.btnAddConcepto_Otros.Visible = True
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

            '********** Recalculamos el total a pagar y los datos de cancelación
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotalAPagar();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRefCC"), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRefCC")
        Session.Add("listaDocumentoRefCC", lista)
    End Sub

    Private Sub visualizarMoneda()
        '************ Mostramos las Monedas
        Me.lblMonedaFaltante.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalAPagar.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalRecibido.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMoneda_TotalSustentado.Text = Me.cboMoneda.SelectedItem.ToString
    End Sub


    Private Sub inicializarFrm()

        Try

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

                .LLenarCboEstadoDocumento(Me.cboEstado)
                .LlenarCboMoneda(Me.cboMoneda)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, _IDTIPODOCUMENTO, False)

                '**************** DATOS DE CANCELACION
                .LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago, _IDTIPODOCUMENTO)
                .LlenarCboBanco(Me.cboBanco, False)
                '.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
                .LlenarCboMoneda(Me.cboMoneda_DatoCancelacion, False)
                '.LlenarCboPostxIdCaja(Me.cboPost_DC, CInt(Me.cboCaja.SelectedValue), False)
                .LlenarCboTipoTarjetaxIdCaja(Me.cboPost_DC, CInt(Me.cboCaja.SelectedValue), False)
                'llenar tipo documento----
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(hddIdTipoDocumento.Value), 2, False)
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento2, CInt(hddIdTipoDocumento.Value), 2, False)

                '-------------------------
                Me.txtFechaEmision.Text = DescFechaActual()
                'Me.txtFechaACobrar.Text = Me.txtFechaEmision.Text

                '*************** MEDIO DE PAGO PRINCIPAL
                Me.IdMedioPagoPrincipal = (New Negocio.MedioPago).SelectIdMedioPagoPrincipal()
                actualizarControlesMedioPago(_IDCONDICIONPAGO, CInt(Me.cboMedioPago.SelectedValue))

                If (Me.cboPost_DC.SelectedValue.ToString() <> "") Then
                    actualizarControles_POST()
                End If

                '**********Busqueda Persona
                .LlenarCboRol(cboRol, True)
                .LlenarCboRol(cboRol02, True)

                Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
                Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

                Me.txtFechaInicio_DocRef2.Text = Me.txtFechaEmision.Text
                Me.txtFechaFin_DocRef2.Text = Me.txtFechaEmision.Text

                actualizarOpcionesBusquedaDocRef(0, False)
                actualizarOpcionesBusquedaDocRef2(0, False)

                ValidarPermisos()
                verFrm(FrmModo.Nuevo, True, True, True, True, True, True)

                ListaPagoCaja = New List(Of Entidades.PagoCaja)

            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {185, 186, 187, 188, 189})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub

    Private Sub limpiarControlesFrm(Optional ByVal limpiarDatosPersona As Boolean = True, Optional ByVal procesoEdicion As Boolean = False)


        If Not procesoEdicion Then
            Me.txtCodigoDocumento.Text = ""
            Me.txtFechaEmision.Text = DescFechaActual()
            Me.IdDocumento = 0

        End If

        If limpiarDatosPersona Then
            Me.txtDescripcionPersona.Text = ""
            Me.txtDNI.Text = ""
            Me.txtRUC.Text = ""
            Me.IdPersona = 0

            Me.txtDescripcionPersona02.Text = ""
            Me.txtDNI02.Text = ""
            Me.txtRUC02.Text = ""
            Me.IdPersona02 = 0
        End If

        '*************** DOCUMENTO REF
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        'Me.txtFechaACobrar.Text = DescFechaActual()
        Me.txtFechaInicio_BA.Text = DescFechaActual()
        Me.txtFechaFin_BA.Text = DescFechaActual()


        Me.txtFaltante.Text = "0"
        Me.txtTotalAPagar.Text = "0"
        Me.txtTotalRecibido.Text = "0"
        Me.txtVuelto.Text = "0"

        Me.chb_SujetoRendicionCuenta.Checked = True



        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()
        Me.GV_Deudas.DataSource = Nothing
        Me.GV_Deudas.DataBind()
        Me.GV_Otros.DataSource = Nothing
        Me.GV_Otros.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.lblMessageHelp.Visible = False

        Me.txtObservaciones.Text = ""

        Me.hddCodigoDocumento.Value = txtCodigoDocumento.Text
        Me.cboMotivoDocCancelacionCaja.Enabled = True
        Me.btnAddConcepto_Otros.Enabled = True


        'actualizarControlesMedioPago(_IDCONDICIONPAGO, IdMedioPagoPrincipal)
        actualizarControlesMedioPago(_IDCONDICIONPAGO, CInt(cboMedioPago.SelectedValue))

    End Sub
    Private Sub verFrm(ByVal modoFrm As Integer, ByVal generarCodigoDoc As Boolean, ByVal limpiarSessionViewState As Boolean, ByVal initListasSession As Boolean, ByVal limpiarControles As Boolean, ByVal initMotivoReciboI As Boolean, ByVal limpiarDatosPersona As Boolean, Optional ByVal procesoEdicion As Boolean = False)

        If (limpiarControles) Then
            limpiarControlesFrm(limpiarDatosPersona, procesoEdicion)
        End If


        If (limpiarSessionViewState) Then
            ListaCancelacion = Nothing

            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            'Session.Remove("ListaCancelacion")
            'ViewState.Clear()
        End If


        If (initListasSession) Then
            Me.ListaCancelacion = New List(Of Entidades.DatosCancelacion)

            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

        End If

        If (generarCodigoDoc) Then
            GenerarCodigoDocumento()
        End If

        If (initMotivoReciboI) Then
            Me.cboMotivoDocCancelacionCaja.SelectedValue = "0"
            Me.btnAddConcepto_Otros.Visible = False
        End If

        '************** MODO FRM
        Modo = modoFrm
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Me.Panel_Sustento.Visible = False
        Me.Panel_SustentoResumen.Visible = False

        Me.Panel_DocRef.Enabled = False

        Select Case Modo

            Case 0 '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente02.Enabled = False
                Me.Panel_Cliente.Enabled = False

                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True
                Me.btnSustentar.Visible = False
                Me.btnDocumentoRef.Visible = True

            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Cliente02.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_DocRef.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True
                Me.btnSustentar.Visible = False
                Me.btnDocumentoRef.Visible = True

            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Cliente02.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_DocRef.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True
                Me.btnSustentar.Visible = False
                Me.btnDocumentoRef.Visible = True

            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = True

                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()

                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False

                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Cliente02.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True
                Me.btnSustentar.Visible = False
                Me.btnDocumentoRef.Visible = False

            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                '********** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Cliente02.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False
                Me.btnSustentar.Visible = True
                Me.btnDocumentoRef.Visible = False
                Me.Panel_SustentoResumen.Visible = True

            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Cliente02.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False
                Me.btnDocumentoRef.Visible = False

                Me.btnSustentar.Visible = True

                Me.Panel_SustentoResumen.Visible = True

            Case 6  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Cliente02.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False
                Me.btnSustentar.Visible = False
                Me.Panel_SustentoResumen.Visible = True

            Case 7 ' ******************* Sustentar Documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False

                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Cliente02.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DocRef.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False
                Me.btnSustentar.Visible = False
                Me.btnDocumentoRef.Visible = False

                Me.Panel_Detalle.Enabled = True
                Me.cboMotivoDocCancelacionCaja.Enabled = False
                Me.btnAddConcepto_Otros.Enabled = False
                Me.Panel_SustentoResumen.Visible = True

        End Select
    End Sub



#Region "Eventos"
    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try


            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            '
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboMotivoDocCancelacionCaja_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMotivoDocCancelacionCaja.SelectedIndexChanged

        If (txtDescripcionPersona02.Text <> "") Then
            actualizarMotivoPago(CInt(Me.cboMotivoDocCancelacionCaja.SelectedValue))
        Else
            objScript.mostrarMsjAlerta(Me, "Seleccione un beneficiario para proceder con la lista de Doc. Cancelacion")
        End If



    End Sub
    'Private Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
    '    Try
    '        objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub
    'Private Sub cboPost_DC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPost_DC.SelectedIndexChanged
    '    Try
    '        actualizarControles_POST()
    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub

#Region "Botones"
    Protected Sub btnVerHistorial_Click(ByVal sender As Object, ByVal e As EventArgs)
        verHistorialAbonos(sender)
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
    End Sub

    Private Sub btnBuscarDocumentoxcodigo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            buscarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        HabilitarEdicionDocumento()
    End Sub
    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumentoDocCancelacionCaja(IdDocumento)
    End Sub

    Private Sub btnAddConcepto_Otros_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddConcepto_Otros.Click
        addNewDetalle_Otros()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If (GV_DocumentoRef.Rows.Count = 0) Then
            objScript.mostrarMsjAlerta(Me, "Es obligatorio referenciar un R/Q como mínimo. No procede la operación.")
        Else
            OnClick_Guardar()
        End If

    End Sub
    'qwe
    Private Sub OnClick_Guardar()
        Select Case Modo

            Case FrmModo.Documento_Sustento

                registrarSustento()

            Case Else

                If Validar_AperturaCierreCaja() Then

                    If (IdPersona <> 0 Or IdPersona <> Nothing) Then
                        registrarDocCancelacionCaja()
                    Else
                        objScript.mostrarMsjAlerta(Me, "Debe de ingresar un beneficiario para continuar con el registro. No procede la operación.")
                    End If

                End If

        End Select
    End Sub

    Private Sub registrarSustento()
        Try

            Dim index As Integer = -1

            If (IsNumeric(Me.hddIndex.Value) And Me.hddIndex.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIndex.Value) >= 0) Then
                    index = CInt(Me.hddIndex.Value)
                End If
            End If

            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)
            Me.listaDocumentoRef_Sustento = getListaDocumentoRef_Sustento()

            Dim listaDetalleConcepto_Anexo As List(Of Entidades.DetalleConcepto_Anexo) = obtenerListaDetalleConcepto_Anexo(index)

            '***************** OBSERVACIONES
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)
            Dim IdDetalleConcepto As Integer = Nothing
            If (index >= 0) Then

                IdDetalleConcepto = Me.ListaDetalleConcepto(index).IdDetalleConcepto

            End If

            If ((New Negocio.DocCancelacion).actualizarSustento_Documento(IdDocumento, IdDetalleConcepto, listaDetalleConcepto_Anexo, objObservaciones)) Then
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

                Me.GV_Documento_Sustento.DataSource = Nothing
                Me.GV_Documento_Sustento.DataBind()

                Me.listaDocumentoRef_Sustento = New List(Of Entidades.Documento)
                setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

                Me.hddIndex.Value = ""
                Me.Panel_Sustento.Visible = False

                '*******************  MANEJO DE LOS BOTONES DE REC INGRESO Y EGRESO
                actualizarControles_ReciboIE()

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If

            Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(IdDocumento)
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()
            setlistaDocumentoRef(Me.listaDocumentoRef)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerListaDetalleConcepto_Anexo(ByVal index As Integer) As List(Of Entidades.DetalleConcepto_Anexo)

        Dim lista As New List(Of Entidades.DetalleConcepto_Anexo)
        Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)
        Me.listaDocumentoRef_Sustento = getListaDocumentoRef_Sustento()

        For i As Integer = 0 To Me.listaDocumentoRef_Sustento.Count - 1

            Dim obj As New Entidades.DetalleConcepto_Anexo

            With obj
                .IdDetalleConcepto = Me.listaDetalleConcepto(index).IdDetalleConcepto
                .IdDetalleConcepto_Anexo = Nothing
                .IdDocumento = CInt(Me.hddIdDocumento.Value)
                .IdDocumentoRef = Me.listaDocumentoRef_Sustento(i).Id
            End With

            lista.Add(obj)

        Next

        Return lista

    End Function

#End Region
#Region "Grillas"
    Private Sub GV_Otros_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Otros.SelectedIndexChanged
        eliminarDetalleRecibo_Otros(Me.GV_Otros.SelectedIndex)
    End Sub
    Private Sub GV_Otros_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Otros.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim gvrow As GridViewRow = CType(e.Row.Cells(1).NamingContainer, GridViewRow)
                Dim combo As DropDownList = CType(gvrow.FindControl("cboConcepto"), DropDownList)

                If (Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto <> Nothing) Then

                    If (combo.Items.FindByValue(CStr(Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto)) IsNot Nothing) Then
                        combo.SelectedValue = CStr(Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto)
                    End If
                End If

                '**************** BOTON DE SUSTENTO
                Dim botonSustento As Button = CType(e.Row.FindControl("btnAddSustento"), Button)

                If (Modo = FrmModo.Documento_Sustento) Then
                    botonSustento.Visible = True
                    combo.Enabled = False
                    CType(e.Row.FindControl("txtAbono"), TextBox).Enabled = False
                    CType(e.Row.FindControl("txtDescripcionConcepto"), TextBox).Enabled = False
                Else
                    botonSustento.Visible = False
                End If

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region
#End Region

    Private Sub actualizarMotivoPago(ByVal Opcion As Integer)
        Try

            '********* Limpiamos la grilla Deudas
            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()
            Me.GV_Otros.DataSource = Nothing
            Me.GV_Otros.DataBind()

            Me.lblMessageHelp.Visible = False
            Me.btnAddConcepto_Otros.Visible = False

            If (Me.cboMotivoDocCancelacionCaja.Items.FindByValue(CStr(Opcion)) IsNot Nothing) Then
                Me.cboMotivoDocCancelacionCaja.SelectedValue = CStr(Opcion)
            End If

            Select Case Opcion

                Case 0 '************ Seleccione una Opción

                Case 1 '************ Pago de deudas
                    Dim lista As List(Of Entidades.Documento_MovCuentaPorPagar) = (New Negocio.MovCuentaPorPagar).SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelRC(IdPersona02, CInt(cboMoneda.SelectedValue), _IDTIPODOCUMENTO, cboTienda.SelectedValue)
                    If (lista.Count <= 0) Then
                        Me.cboMotivoDocCancelacionCaja.SelectedValue = "0"
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "alert('La Persona seleccionada no posee deudas.'); calcularTotalAPagar();", True)
                        Return
                    Else
                        Me.lblMessageHelp.Visible = True
                        Me.GV_Deudas.DataSource = lista
                        Me.GV_Deudas.DataBind()
                    End If

                Case 2 '************ Otros                    
                    Me.btnAddConcepto_Otros.Visible = True
                    Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(True, 1)
                    Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
                    Me.GV_Otros.DataBind()

            End Select

            '********** Recalculamos el total a pagar y los datos de cancelación
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotalAPagar();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub verHistorialAbonos(ByVal sender As Object)
        Try

            Dim IdMovCtaPP As Integer = Nothing

            For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1

                If (CType(sender, ImageButton).ClientID = CType(Me.GV_Deudas.Rows(i).FindControl("btnVerHistorialPago"), ImageButton).ClientID) Then

                    IdMovCtaPP = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdMovCtaPP"), HiddenField).Value)
                    Exit For

                End If

            Next

            If (IdMovCtaPP = Nothing) Then
                Throw New Exception("No se halló el Registro. Error del Sistema.")
            Else

                '************ MOSTRAMOS EL HISTORIAL DE ABONOS
                Dim listaAbonos As List(Of Entidades.Documento_MovCuentaPorPagar) = (New Negocio.MovCuentaPorPagar).SelectAbonosxIdMovCtaPP(IdMovCtaPP)
                If (listaAbonos.Count <= 0) Then
                    Throw New Exception("No se hallaron abonos para el documento seleccionado.")
                Else
                    Me.GV_Abonos.DataSource = listaAbonos
                    Me.GV_Abonos.DataBind()
                    objScript.onCapa(Me, "capaAbonos")
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub





    Private Sub addNewDetalle_Otros()
        Try

            '********* Obtengo la Lista
            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)

            '*********** Creo un Nuevo Detalle
            Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(_IDTIPODOCUMENTO)


            '************ Creo el objeto y lo agrego a la Lista
            Dim obj As New Entidades.DetalleConcepto
            With obj
                .Moneda = Me.cboMoneda.SelectedItem.ToString
                .Monto = 0
                .Concepto = ""
                .ListaConcepto = listaAux
                .IdConcepto = Nothing
            End With
            Me.ListaDetalleConcepto.Add(obj)

            '************ Grilla
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub




    Private Sub eliminarDetalleRecibo_Otros(ByVal index As Integer)
        Try

            If (Modo = FrmModo.Documento_Sustento) Then
                Throw New Exception("NO SE PERMITE LA OPERACIÓN.")
            End If

            '****** Elimino el detalle
            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)
            Me.ListaDetalleConcepto.RemoveAt(index)

            '******** Grilla
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      calcularTotalAPagar();       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Sub validarFrm()

        If (Me.GV_Cancelacion.Rows.Count <= 0) Then
            Throw New Exception("NO SE HAN INGRESADO DATOS DE CANCELACIÓN. NO SE PERMITE LA OPERACIÓN.")
        End If

        'Me.ListaCancelacion = getListaCancelacion()
        If CDec(Me.txtVuelto.Text) > 0 And chb_SujetoRendicionCuenta.Checked = False Then
            Throw New Exception("NO SE DEBE GENERAR VUELTO. NO SE PERMITE LA OPERACIÓN.")
        End If
        'For i As Integer = 0 To Me.ListaCancelacion.Count - 1
        '    If (Me.ListaCancelacion(i).IdMedioPago <> IdMedioPagoPrincipal) And CDec(Me.txtVuelto.Text) > 0 Then
        '        Throw New Exception("SE HAN INGRESADO MEDIOS DE PAGO QUE NO DEBEN GENERAR VUELTO. NO SE PERMITE LA OPERACIÓN.")
        '    End If
        'Next

    End Sub

    Private Function obtenerAnexo_Documento() As Entidades.Anexo_Documento

        Dim obj As New Entidades.Anexo_Documento
        With obj

            .Liquidado = False

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            If (Me.chb_SujetoRendicionCuenta.Checked) Then
                .MontoxSustentar = CDec(Me.txtTotalAPagar.Text)
            Else
                .MontoxSustentar = 0
            End If

        End With
        Return obj

    End Function

    Private Sub registrarDocCancelacionCaja()

        Try

            validarFrm()

            '****** cabecera Documento
            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            Dim objAnexo_Documento As Entidades.Anexo_Documento = obtenerAnexo_Documento()

            '******* Detalle CONCEPTO
            Me.ListaDetalleConcepto = obtenerDetalleConcepto_Save()

            ListaPagoCaja = obtenerListaPagoCaja_Save()

            '******** obtengo la lista pago caja a insertar
            ListaCancelacion = obtenerListaDatosCancelacion_Save()
            Dim objMovCaja As Entidades.MovCaja = obtenerMovCaja()

            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

            'Dim listaMovBanco As List(Of Entidades.MovBancoView) = obtenerListaMovBanco(listaDatosCancelacion, objDocumento)

            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()


            Select Case Modo
                Case 1 '******* NUEVO
                    'Dim IdDocumento As Integer = (New Negocio.DocReciboCaja).ReciboCajaIngresoInsert(objDocumento, Me.ListaDetalleConcepto, objMovCaja, listaDatosCancelacion, objObservaciones, listaMovBanco)
                    'Dim IdDocumento2 As Integer = (New Negocio.DocCancelacionCaja).Insert(objDocumento, Me.ListaDetalleConcepto, listaDatosCancelacion, objObservaciones, listaMovBanco)
                    Dim IdDocumento2 As Integer = (New Negocio.DocCancelacion).Insert(objDocumento, Me.ListaDetalleConcepto, ListaCancelacion, objObservaciones, Nothing, objMovCaja, ListaPagoCaja, objAnexo_Documento, listaRelacionDocumento)
                    If (IdDocumento2 > 0) Then

                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)
                        IdDocumento = IdDocumento2
                        objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")
                        Me.hddIdDocumento.Value = CStr(IdDocumento2)
                        limpiar_datos()
                    Else
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If


                Case 2 '******* EDITAR
                    'If ((New Negocio.DocReciboCaja).Update(objDocumento, Me.ListaDetalleConcepto, objMovCaja, listaDatosCancelacion, objObservaciones, listaMovBanco) > 0) Then
                    'If ((New Negocio.DocCancelacionCaja).Update(objDocumento, Me.ListaDetalleConcepto, listaDatosCancelacion, objObservaciones, listaMovBanco) > 0) Then
                    If ((New Negocio.DocCancelacion).Update(objDocumento, Me.ListaDetalleConcepto, ListaCancelacion, objObservaciones, Nothing, objMovCaja, ListaPagoCaja, objAnexo_Documento, listaRelacionDocumento) > 0) Then
                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)

                        objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")
                        limpiar_datos()
                    Else
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If
            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Public Sub limpiar_datos()
        ListaCancelacion.Clear()
        ListaPagoCaja.Clear()
        ListaDetalleConcepto.Clear()
    End Sub

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim listaRelacionDocumento As New List(Of Entidades.RelacionDocumento)

        Me.listaDocumentoRef = getlistaDocumentoRef()

        '************************ OBTENEMOS LOS DOCUMENTOS DE REFERENCIA SEGUN LA LISTA DE DOCUMENTOS DE REFERENCIA
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim obj As New Entidades.RelacionDocumento
            With obj

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

            End With
            listaRelacionDocumento.Add(obj)

        Next

        '********************** OBTENEMOS LA LISTA DE DOCUMENTOS DE REFERENCIA SEGUN LOS PAGOS (DEUDAS)
        For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1

            If (CType(Me.GV_Deudas.Rows(i).FindControl("chbSelectDeuda"), CheckBox).Checked) Then

                Dim add As Boolean = True

                For j As Integer = 0 To listaRelacionDocumento.Count - 1

                    If (CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdDocumentoDeuda"), HiddenField).Value) = listaRelacionDocumento(j).IdDocumento1) Then
                        add = False
                        Exit For
                    End If

                Next

                If (add) Then

                    Dim obj As New Entidades.RelacionDocumento
                    With obj
                        Select Case CInt(Me.hddFrmModo.Value)
                            Case FrmModo.Nuevo
                                .IdDocumento2 = Nothing
                            Case FrmModo.Editar
                                .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                        End Select
                        .IdDocumento1 = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdDocumentoDeuda"), HiddenField).Value)
                    End With
                    listaRelacionDocumento.Add(obj)

                End If

            End If

        Next

        Return listaRelacionDocumento

    End Function

#Region "Generación de Objetos"

    Private Function obtenerDetalleConcepto_Save() As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        Select Case CInt(Me.cboMotivoDocCancelacionCaja.SelectedValue)
            Case 0 '********** Elegir motivo
                Throw New Exception("NO SE HA AGREGADO DETALLES AL DOCUMENTO. NO SE PERMITE LA OPERACIÓN.")

            Case 1 '************ Pago deudas
                For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1
                    If (CType(Me.GV_Deudas.Rows(i).FindControl("chbSelectDeuda"), CheckBox).Checked) Then
                        Dim obj As New Entidades.DetalleConcepto
                        With obj
                            .Monto = CDec(CType(Me.GV_Deudas.Rows(i).FindControl("txtAbono"), TextBox).Text)
                            .Concepto = Me.GV_Deudas.Rows(i).Cells(1).Text + " Nro. " + CType(Me.GV_Deudas.Rows(i).FindControl("lblNroDocumentoDeuda"), Label).Text
                            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)

                            '******** Los documentos a los cuales se les va a afectar
                            .IdDocumentoRef = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdDocumentoDeuda"), HiddenField).Value)
                            .IdMovCuentaRef = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdMovCtaPP"), HiddenField).Value)

                        End With
                        lista.Add(obj)
                    End If
                Next
            Case 2  '******** otros
                lista = obtenerListaDetalleConcepto(False, Nothing)
        End Select
        Return lista
    End Function

    Private Function obtenerListaDetalleConcepto(ByVal generarNewLista As Boolean, ByVal cantidadInitNew As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        '********** Obtengo la lista de conceptos segun el tipo de Recibo
        Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(_IDTIPODOCUMENTO)

        If (generarNewLista) Then  '********** Generamos la Lista de DetalleRecibo // Otros

            '************ Genero la lista
            For i As Integer = 0 To cantidadInitNew - 1

                Dim obj As New Entidades.DetalleConcepto

                obj.Moneda = Me.cboMoneda.SelectedItem.ToString
                obj.Monto = 0
                obj.Concepto = ""
                obj.ListaConcepto = listaAux
                obj.IdConcepto = Nothing
                lista.Add(obj)

            Next
        Else

            '**************** DEVOLVEMOS LA LISTA DEL DETALLE CONCEPTO (ACTUALIZAMOS)

            For i As Integer = 0 To Me.GV_Otros.Rows.Count - 1

                Dim obj As New Entidades.DetalleConcepto
                obj.IdDetalleConcepto = CInt(CType(Me.GV_Otros.Rows(i).FindControl("hddIdDetalleConcepto"), HiddenField).Value)
                obj.Moneda = CType(Me.GV_Otros.Rows(i).FindControl("lblMoneda"), Label).Text
                obj.Monto = CDec(CType(Me.GV_Otros.Rows(i).FindControl("txtAbono"), TextBox).Text)
                obj.Concepto = CType(Me.GV_Otros.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text
                obj.ListaConcepto = listaAux
                obj.IdConcepto = CInt(CType(Me.GV_Otros.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                lista.Add(obj)

            Next

        End If

        Return lista

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = Me.cboSerie.SelectedItem.ToString
            .Codigo = Me.txtCodigoDocumento.Text
            .IdTipoDocumento = _IDTIPODOCUMENTO
            .IdEstadoDoc = 1  '************ Siempre es ACTIVO
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdPersona = IdPersona
            .TotalAPagar = CDec(Me.txtTotalAPagar.Text)
            .Total = CDec(Me.txtTotalAPagar.Text)
            .IdTipoOperacion = pIdTipoOperacion
            .IdCondicionPago = _IDCONDICIONPAGO
            .FechaCancelacion = FechaActual()

            Select Case Modo
                Case FrmModo.Nuevo  '******* Nuevo
                    .Id = Nothing
                Case FrmModo.Editar '***** Editar
                    .Id = IdDocumento
            End Select

            .Vuelto = CDec(Me.txtVuelto.Text)
            .IdEstadoCancelacion = 2   '******** CANCELADO
            .TotalLetras = (New Aletras).Letras(CStr(objDocumento.TotalAPagar)) + " /100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion

        End With

        Return objDocumento

    End Function

    'Private Function obtenerListaMovBanco(ByVal lista As List(Of Entidades.DatosCancelacion), ByVal objDocumento As Entidades.Documento) As List(Of Entidades.MovBancoView)

    '    Dim listaMovBanco As New List(Of Entidades.MovBancoView)
    '    For i As Integer = 0 To lista.Count - 1
    '        'Si el datos cancelacion esta asociado a un cheque egreso, no se crea un mov banco
    '        'porque este ya se creo a la hora de crear ese cheque.
    '        If lista(i).IdMedioPagoInterfaz <> 7 Then

    '            Dim obj As New Entidades.MovBancoView
    '            With obj
    '                .IdBanco = lista(i).IdBanco
    '                .IdCuentaBancaria = lista(i).IdCuentaBancaria
    '                .Monto = lista(i).Monto
    '                .IdMoneda = lista(i).IdMoneda
    '                .IdMedioPago = lista(i).IdMedioPago
    '                .IdUsuario = objDocumento.IdUsuario
    '                .IdPersonaRef = objDocumento.IdPersona
    '                .IdCaja = objDocumento.IdCaja
    '                .IdTienda = objDocumento.IdTienda
    '                .FechaMov = objDocumento.FechaEmision
    '                .NroOperacion = lista(i).NroOperacion
    '                .IdConceptoMovBanco = IdConceptoMovBanco
    '                .Factor = _FACTOR
    '                .EstadoAprobacion = False    'Revisar
    '                .EstadoMov = True

    '            End With

    '            listaMovBanco.Add(obj)
    '        End If
    '    Next

    '    Return listaMovBanco

    'End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservaciones As Entidades.Observacion = Nothing
        If (Me.txtObservaciones.Text.Trim.Length > 0) Then

            objObservaciones = New Entidades.Observacion
            objObservaciones.Observacion = Me.txtObservaciones.Text.Trim

        End If
        Return objObservaciones
    End Function

    Private Function obtenerListaDatosCancelacion_Save() As List(Of Entidades.DatosCancelacion)

        If (Me.ListaCancelacion.Count = 0) Then

            Dim objDatosCancelacion As New Entidades.DatosCancelacion
            With objDatosCancelacion
                .Monto = CDec(Me.txtTotalAPagar.Text)
                .Factor = _FACTOR
                .IdMedioPago = Me.IdMedioPagoPrincipal
                .IdMedioPagoInterfaz = IdMedioPagoInterfaz
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .IdTipoMovimiento = _IDTIPOMOVIMIENTO
                .IdCaja = CInt(cboCaja.SelectedValue)

            End With

            Me.ListaCancelacion.Add(objDatosCancelacion)
        End If

        Return Me.ListaCancelacion
    End Function
    Private Function obtenerListaPagoCaja_Save() As List(Of Entidades.PagoCaja)

        If (Me.ListaPagoCaja.Count = 0) Then  '**** Creamos un Pago Caja EFECTIVO

            Dim objPagoCaja As New Entidades.PagoCaja
            With objPagoCaja
                .Efectivo = CDec(Me.txtTotalAPagar.Text)
                .Factor = _FACTOR
                .IdMedioPago = IdMedioPagoPrincipal 'CInt(Me.cboMedioPago.SelectedValue) '
                .IdMedioPagoInterfaz = IdMedioPagoInterfaz
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .IdTipoMovimiento = _IDTIPOMOVIMIENTO

            End With

            Me.ListaPagoCaja.Add(objPagoCaja)
        End If

        Return Me.ListaPagoCaja

    End Function
    Private Function obtenerMovCaja() As Entidades.MovCaja  'Revisar
        Dim objMovCaja As New Entidades.MovCaja

        With objMovCaja

            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdPersona = CInt(Session("IdUsuario"))
            .IdTipoMovimiento = _IDTIPOMOVIMIENTO

        End With

        Return objMovCaja
    End Function

#End Region

    Private Sub cargarDocumentoCab(ByVal objDocumento As Entidades.Documento)

        Me.txtCodigoDocumento.Text = objDocumento.Codigo
        Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)
        Me.IdDocumento = objDocumento.Id
        Me.txtTotalAPagar.Text = CStr(Math.Round(objDocumento.TotalAPagar, 2))
        Me.hddIdDocumento.Value = CStr(objDocumento.Id)

        If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
            Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
        End If

        If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
        End If

    End Sub


    Private Sub HabilitarEdicionDocumento()
        Try

            actualizarControles_ReciboIE()

            If IsNumeric(Me.txtTotalSustento.Text) Then
                If CDec(Me.txtTotalSustento.Text) > 0 Then
                    Throw New Exception("EL RECIBO DE EGRESO HA SIDO SUSTENTADO POR UN MONTO " + cboMoneda.SelectedItem.Text + " [ " + Me.txtTotalSustento.Text + " ].")
                    Return
                End If
            End If

            If (New Negocio.DatosCancelacion).DeshacerMov_Edicion(IdDocumento) Then

                verFrm(FrmModo.Editar, False, True, True, True, True, False, True)
                objScript.mostrarMsjAlerta(Me, "El Proceso resultó con éxito.")

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub anularDocumentoDocCancelacionCaja(ByVal IdDoc As Integer)
        Try
            If ((New Negocio.DatosCancelacion).Anular(IdDoc)) Then
                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El Proceso resultó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub buscarDocumento(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)
        Try
            Dim objDocumento As Entidades.Documento = Nothing

            If (IdDocumento = Nothing) Then
                objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
            Else
                objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            End If

            '******** Si paso es xq si existe el documento
            verFrm(FrmModo.Documento_Buscar_Exito, False, True, True, True, True, True)

            cargarDocumentoCab(objDocumento)
            cargarPersona(objDocumento.IdPersona, False)

            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)
            If (objObservaciones IsNot Nothing) Then
                Me.txtObservaciones.Text = objObservaciones.Observacion
            End If

            Me.ListaDetalleConcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumento.Id)

            If Not Me.ListaDetalleConcepto Is Nothing Then
                If Me.ListaDetalleConcepto.Count > 0 Then

                    Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(_IDTIPODOCUMENTO)
                    For i As Integer = 0 To Me.ListaDetalleConcepto.Count - 1
                        Dim var As Integer = i
                        If Me.ListaDetalleConcepto(var).IdConcepto > 0 Then
                            If listaAux.Find(Function(k As Entidades.Concepto) k.Id = Me.ListaDetalleConcepto(var).IdConcepto) Is Nothing Then
                                Dim ListaConcepto As List(Of Entidades.Concepto)
                                ListaConcepto = (New Negocio.Concepto).SelectxId(Me.ListaDetalleConcepto(var).IdConcepto)
                                If Not ListaConcepto Is Nothing Then
                                    listaAux.AddRange(ListaConcepto)
                                End If
                            End If
                            Me.ListaDetalleConcepto(i).ListaConcepto = listaAux
                        End If
                    Next

                End If
            End If



            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

            Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumento.Id)
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()
            setlistaDocumentoRef(Me.listaDocumentoRef)

            Me.ListaCancelacion = obtenerListaCancelacion_Load(objDocumento.Id)
            'setListaCancelacion(Me.ListaCancelacion)
            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            '*******************  MANEJO DE LOS BOTONES DE REC INGRESO Y EGRESO
            actualizarControles_ReciboIE()

            Validar_AperturaCierreCaja(False)

            '************ Calculamos los datos de cancelación
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#Region "**************************** DATOS DE CANCELACION"

    Private Function obtenerListaCancelacion_Load(ByVal IdDocumento As Integer) As List(Of Entidades.DatosCancelacion)
        Dim lista As List(Of Entidades.DatosCancelacion) = (New Negocio.DatosCancelacion).SelectListxIdDocumento(IdDocumento)

        For i As Integer = 0 To lista.Count - 1

            Select Case lista(i).IdMedioPagoInterfaz


                Case 1  '*********** EFECTIVO

                    lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ]"

                Case 2  '*********** BANCO  (NRO OPERACION)

                    lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Banco: [ " + lista(i).Banco + " ] Cuenta: [ " + lista(i).CuentaBancaria + " ]"


                Case 3  '************** BANCO CHEQUE

                    Dim fecha As String = ""

                    If (lista(i).FechaACobrar <> Nothing) Then
                        fecha = Format(lista(i).FechaACobrar, "dd/MM/yyyy")
                    Else
                        fecha = "---"
                    End If

                    lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Banco: [ " + lista(i).Banco + " ] Fecha a Cobrar: [ " + fecha + " ]"


                    'Case 4 '************* TARJETA

                    '    lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Banco: [ " + lista(i).Banco + " ] Cuenta: [ " + lista(i).CuentaBancaria + " ]"

                    ' Case 5   '*********** NOTA DE CREDITO

                    ' lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Nro. Documento: [ " + lista(i).NroOperacion + " ] "

                    'Case 6  '************* COMP RETENCION

                    '    lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Nro. Documento: [ " + lista(i).NroOperacion + " ] "

            End Select


        Next

        Return lista
    End Function

    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged
        Try
            actualizarControlesMedioPago(_IDCONDICIONPAGO, CInt(Me.cboMedioPago.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub actualizarControles_POST()

        Dim objcbo As New Combo

        With objcbo
            ' Se cambio
            '.LlenarCboTarjetaxIdPost(Me.cboTarjeta_DC, CInt(Me.cboPost_DC.SelectedValue), False) 
            .LlenarCboTarjetaxTipoTarjetaCaja(Me.cboTarjeta_DC, CInt(Me.cboPost_DC.SelectedValue), CInt(Me.cboCaja.SelectedValue))

            Dim objPost As Entidades.Post = (New Negocio.PostView).SelectxIdPost(CInt(Me.cboPost_DC.SelectedValue))






            '************* Banco Cuenta bancaria
            Me.cboBanco.SelectedValue = CStr(objPost.IdBanco)

            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
            Me.cboCuentaBancaria.SelectedValue = CStr(objPost.IdCuentaBancaria)

        End With

    End Sub



    Private Sub actualizarControlesMedioPago(ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer)

        '************ Limpiar Datos de Cancelación
        'Me.txtNro_DC.Text = ""
        Me.txtMonto_DatoCancelacion.Text = "0"
        'Me.txtFechaACobrar.Text = ""

        Select Case IdCondicionPago
            Case 1  '**************** CONTADO
                Me.Panel_CP_Contado.Visible = True

                '*************** MEDIO DE PAGO
                Me.lblMonto_DC.Visible = False
                Me.txtMonto_DatoCancelacion.Visible = False
                Me.cboMoneda_DatoCancelacion.Visible = False
                Me.btnAddDatoCancelacion.Visible = False

                'Me.lblPost_DC.Visible = False
                'Me.cboPost_DC.Visible = False
                'Me.lblTarjeta_DC.Visible = False
                'Me.cboTarjeta_DC.Visible = False
                'Me.lblBanco_DC.Visible = False
                'Me.cboBanco.Visible = False
                'Me.lblCuentaBancaria_DC.Visible = False
                'Me.cboCuentaBancaria.Visible = False
                'Me.lblNro_DC.Visible = False
                'Me.txtNro_DC.Visible = False
                'Me.lblFechaACobrar_DC.Visible = False
                'Me.txtFechaACobrar.Visible = False
                'Me.btnSeleccionarCheque.Visible = False

                'Me.Panel_DatosCancelacion_Add.Enabled = True
                Me.txtMonto_DatoCancelacion.Enabled = True
                Me.cboMoneda_DatoCancelacion.Enabled = True

                'Me.btnVer_NotaCredito.Visible = False

                Dim objMedioPago As Entidades.MedioPago = (New Negocio.MedioPago).SelectxId(IdMedioPago)(0)

                'Me.cboBanco.Enabled = True
                'Me.cboCuentaBancaria.Enabled = True

                Select Case objMedioPago.IdMedioPagoInterfaz

                    Case 1  '*************** EFECTIVO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True



                        Me.lblTarjeta_DC.Visible = False
                        Me.cboTarjeta_DC.Visible = False
                        Me.lblBanco_DC.Visible = False
                        Me.cboBanco.Visible = False
                        Me.lblCuentaBancaria_DC.Visible = False
                        Me.cboCuentaBancaria.Visible = False
                        Me.lblPost_DC.Visible = False
                        Me.cboPost_DC.Visible = False

                    Case 2  '*************** BANCO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        'Me.lblBanco_DC.Visible = True
                        'Me.cboBanco.Visible = True
                        'Me.lblCuentaBancaria_DC.Visible = True
                        'Me.cboCuentaBancaria.Visible = True
                        'Me.lblNro_DC.Visible = True
                        'Me.txtNro_DC.Visible = True

                        'actualizarControles_Caja()

                        Me.lblTarjeta_DC.Visible = False
                        Me.cboTarjeta_DC.Visible = False
                        Me.lblBanco_DC.Visible = False
                        Me.cboBanco.Visible = False
                        Me.lblCuentaBancaria_DC.Visible = False
                        Me.cboCuentaBancaria.Visible = False
                        Me.lblPost_DC.Visible = False
                        Me.cboPost_DC.Visible = False

                    Case 3  '**************** BANCO CHEQUE
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        'Me.lblBanco_DC.Visible = True
                        'Me.cboBanco.Visible = True
                        'Me.lblNro_DC.Visible = True
                        'Me.txtNro_DC.Visible = True
                        'Me.lblFechaACobrar_DC.Visible = True
                        'Me.txtFechaACobrar.Visible = True
                        'actualizarControles_Caja()
                        Me.lblTarjeta_DC.Visible = False
                        Me.cboTarjeta_DC.Visible = False
                        Me.lblBanco_DC.Visible = False
                        Me.cboBanco.Visible = False
                        Me.lblCuentaBancaria_DC.Visible = False
                        Me.cboCuentaBancaria.Visible = False
                        Me.lblPost_DC.Visible = False
                        Me.cboPost_DC.Visible = False

                    Case 4  '*********** TARJETA
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        'Me.lblBanco_DC.Visible = True
                        'Me.cboBanco.Visible = True
                        'Me.lblCuentaBancaria_DC.Visible = True
                        'Me.cboCuentaBancaria.Visible = True
                        'Me.lblNro_DC.Visible = True
                        'Me.txtNro_DC.Visible = True
                        'Me.lblPost_DC.Visible = True
                        'Me.cboPost_DC.Visible = True
                        'Me.lblTarjeta_DC.Visible = True
                        'Me.cboTarjeta_DC.Visible = True
                        '************ Configuramos los cambios por POST


                        Me.lblTarjeta_DC.Visible = True
                        Me.cboTarjeta_DC.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblPost_DC.Visible = True
                        Me.cboPost_DC.Visible = True

                        'Me.cboBanco.Enabled = False
                        'Me.cboCuentaBancaria.Enabled = False

                    Case 5  '************* NOTA CREDITO
                        Me.lblTarjeta_DC.Visible = False
                        Me.cboTarjeta_DC.Visible = False
                        Me.lblBanco_DC.Visible = False
                        Me.cboBanco.Visible = False
                        Me.lblCuentaBancaria_DC.Visible = False
                        Me.cboCuentaBancaria.Visible = False
                        Me.lblPost_DC.Visible = False
                        Me.cboPost_DC.Visible = False
                        'Me.btnVer_NotaCredito.Visible = True

                    Case 6  '************** COMPROBANTE DE RETENCION

                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        'Me.lblNro_DC.Visible = True
                        'Me.txtNro_DC.Visible = True
                        Me.lblTarjeta_DC.Visible = False
                        Me.cboTarjeta_DC.Visible = False
                        Me.lblBanco_DC.Visible = False
                        Me.cboBanco.Visible = False
                        Me.lblCuentaBancaria_DC.Visible = False
                        Me.cboCuentaBancaria.Visible = False
                        Me.lblPost_DC.Visible = False
                        Me.cboPost_DC.Visible = False

                    Case 7  '************** CHEQUE EGRESO 
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        'Me.lblBanco_DC.Visible = True
                        'Me.cboBanco.Visible = True
                        'Me.lblCuentaBancaria_DC.Visible = True
                        'Me.cboCuentaBancaria.Visible = True
                        'Me.lblNro_DC.Visible = True
                        'Me.txtNro_DC.Visible = True
                        'Me.lblFechaACobrar_DC.Visible = True
                        'Me.txtFechaACobrar.Visible = True
                        'actualizarControles_Caja()
                        'Me.btnSeleccionarCheque.Visible = True
                        'Me.Panel_DatosCancelacion_Add.Enabled = False
                        'Me.txtMonto_DatoCancelacion.Enabled = False
                        Me.cboMoneda_DatoCancelacion.Enabled = False

                        Me.lblTarjeta_DC.Visible = False
                        Me.cboTarjeta_DC.Visible = False
                        Me.lblBanco_DC.Visible = False
                        Me.cboBanco.Visible = False
                        Me.lblCuentaBancaria_DC.Visible = False
                        Me.cboCuentaBancaria.Visible = False
                        Me.lblPost_DC.Visible = False
                        Me.cboPost_DC.Visible = False


                End Select

                IdMedioPagoInterfaz = objMedioPago.IdMedioPagoInterfaz
                'IdConceptoMovBanco = objMedioPago.IdConceptoMovBanco
            Case 2  '**************** CREDITO
                Me.Panel_CP_Contado.Visible = False

        End Select

    End Sub

    Protected Sub btnAddDatoCancelacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDatoCancelacion.Click

        addDatoCancelacion()

    End Sub

    Private Sub addDatoCancelacion()
        Try

            'Me.ListaCancelacion = getListaCancelacion()

            Dim objDatosCancelacion As New Entidades.DatosCancelacion

            objDatosCancelacion.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objDatosCancelacion.Factor = _FACTOR
            objDatosCancelacion.IdTipoMovimiento = _IDTIPOMOVIMIENTO
            objDatosCancelacion.IdMedioPagoInterfaz = IdMedioPagoInterfaz

            Select Case IdMedioPagoInterfaz

                Case 1  '*********** EFECTIVO

                    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ]"
                    objDatosCancelacion.IdCaja = CInt(cboCaja.SelectedValue)
                Case 2  '*********** BANCO  (NRO OPERACION)

                    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)

                    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objDatosCancelacion.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objDatosCancelacion.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)

                    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedItem.ToString + " ]"
                    'objDatosCancelacion.NroOperacion = (Me.txtNro_DC.Text)

                    'Case 3, 7  '************** BANCO CHEQUE y por ahora CHEQUE EGRESO

                    '    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    '    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    '    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    '    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    '    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    '    objDatosCancelacion.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    '    'Se agrego la sgte linea
                    '    objDatosCancelacion.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    '    Dim fecha As String = ""
                    '    Try
                    '        objDatosCancelacion.FechaACobrar = CDate(Me.txtFechaACobrar.Text)
                    '        fecha = Me.txtFechaACobrar.Text
                    '    Catch ex As Exception
                    '        objDatosCancelacion.FechaACobrar = Nothing
                    '        fecha = "---"
                    '    End Try

                    '    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Fecha a Cobrar: [ " + fecha + " ]"
                    '    objDatosCancelacion.NumeroCheque = Me.txtNro_DC.Text
                    '    'objDatosCancelacion.IdDocumentoRef = ChequeEgreso.Id

                Case 4 '************* TARJETA

                    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objDatosCancelacion.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objDatosCancelacion.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    'objDatosCancelacion.IdPost = CInt(Me.cboPost_DC.SelectedValue)
                    'objDatosCancelacion.IdTarjeta = CInt(Me.cboTarjeta_DC.SelectedValue)

                    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ] Banco: [ " + Me.cboTarjeta_DC.SelectedItem.ToString + " ] " 'Cuenta: [ " + Me.cboCuentaBancaria.SelectedValue.ToString + " ]"


                    'Case 5   '*********** NOTA DE CREDITO

                    '*************** NO SE UTILIZA

                    'Case 6   '************** COMP RETENCION

                    '    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    '    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    '    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    '    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    '    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    '    'objDatosCancelacion.NroOperacion = CStr(Me.txtNro_DC.Text.Trim)

                    '    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ] "
                    '    'Case 7  '*************** CHEQUE EGRESO


            End Select

            Me.ListaCancelacion.Add(objDatosCancelacion)
            'setListaCancelacion(Me.ListaCancelacion)

            Dim objPagoCaja As New Entidades.PagoCaja
            With objPagoCaja
                .Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                .Factor = _FACTOR
                .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                .IdMedioPagoInterfaz = IdMedioPagoInterfaz
                .IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                .IdTipoMovimiento = _IDTIPOMOVIMIENTO
            End With
            Me.ListaPagoCaja.Add(objPagoCaja)


            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            Me.txtMonto_DatoCancelacion.Text = "0"
            'Me.txtNro_DC.Text = ""
            'Me.txtFechaACobrar.Text = ""

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Protected Sub GV_Cancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Cancelacion.SelectedIndexChanged
        quitarDatoCancelacion()
    End Sub
    Private Sub quitarDatoCancelacion()
        Try
            Me.ListaCancelacion.RemoveAt(Me.GV_Cancelacion.SelectedRow.RowIndex)
            Me.ListaPagoCaja.RemoveAt(Me.GV_Cancelacion.SelectedRow.RowIndex)

            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region

#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = Me.IdPersona
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaInicio_BA.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaFin_BA.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(_IDTIPODOCUMENTO, IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            If (Me.GV_BusquedaAvanzado.Rows.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaBusquedaAvanzado');  alert('No se hallaron registros.');    ", True)
            Else
                objScript.onCapa(Me, "capaBusquedaAvanzado")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            '************  cargarDocumentoNotaCredito(0, 0, )
            buscarDocumento(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region


#Region "****************Buscar Personas Mantenimiento"
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()

            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.ListaCancelacion = New List(Of Entidades.DatosCancelacion)
            'setListaCancelacion(Me.ListaCancelacion)

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   offCapa('capaPersona');    calcularDatosCancelacion();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub gvBuscar02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar02.SelectedIndexChanged

        Try

            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()

            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.ListaCancelacion = New List(Of Entidades.DatosCancelacion)
            'setListaCancelacion(Me.ListaCancelacion)

            cargarPersona02(CInt(Me.gvBuscar02.SelectedRow.Cells(1).Text))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   offCapa('capaPersona02');    calcularDatosCancelacion();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer, Optional ByVal cerrarCapa As Boolean = True)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.IdPersona = objPersonaView.IdPersona

            If cerrarCapa Then
                objScript.offCapa(Me, "capaPersona")
            End If

        End If
    End Sub
    Private Sub cargarPersona02(ByVal IdPersona02 As Integer, Optional ByVal cerrarCapa As Boolean = True)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona02)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona02.Text = objPersonaView.Descripcion
            Me.txtDNI02.Text = objPersonaView.Dni
            Me.txtRUC02.Text = objPersonaView.Ruc
            Me.IdPersona02 = objPersonaView.IdPersona

            If cerrarCapa Then
                objScript.offCapa(Me, "capaPersona02")
            End If

        End If
    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)

        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub
    Private Sub Buscar02(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                      ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                      ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex02.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex02.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO02.Text) - 1)
        End Select

        Dim listaPersona02 As New List(Of Entidades.PersonaView)

        listaPersona02 = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona02.Count > 0 Then
            gv.DataSource = listaPersona02
            gv.DataBind()
            tbPageIndex02.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona02")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona02');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub


   
    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btAnterior02_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior02.Click
        Try
            Buscar(gvBuscar02, CStr(ViewState("dni02")), CStr(ViewState("ruc02")), CStr(ViewState("pat02")), CInt(ViewState("tipo02")), CInt(ViewState("rol02")), CInt(ViewState("estado02")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente02_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente02.Click
        Try
            Buscar(gvBuscar02, CStr(ViewState("dni02")), CStr(ViewState("ruc02")), CStr(ViewState("pat02")), CInt(ViewState("tipo02")), CInt(ViewState("rol02")), CInt(ViewState("estado02")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr02_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr02.Click
        Try
            Buscar(gvBuscar02, CStr(ViewState("dni02")), CStr(ViewState("ruc02")), CStr(ViewState("pat02")), CInt(ViewState("tipo02")), CInt(ViewState("rol02")), CInt(ViewState("estado02")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
    '#Region "Busqueda de Cheques"

    '    Protected Sub btnSeleccionarCheque_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSeleccionarCheque.Click
    '        objScript.onCapa(Me, "capaCheques")
    '        ListaCheques = (New Negocio.DocumentoCheque).SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(IdPersona, False)
    '        GV_Cheques.DataSource = ListaCheques
    '        GV_Cheques.DataBind()

    '    End Sub

    '    Protected Sub lbtnSeleccionarCheque_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '        Try
    '            Dim ind As Integer 'indice de la lista correspondiente al registro a modificar

    '            'Se obtiene el Id del Registro a editar 
    '            'ini
    '            Dim lbtn As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
    '            Dim fila As GridViewRow = CType(lbtn.NamingContainer, GridViewRow)
    '            IdCheque = CInt(CType(fila.Cells(0).FindControl("hddId"), HiddenField).Value.Trim())
    '            'fin

    '            'se obtiene el indice de la lista, para asi obtener la entidad a editar 
    '            ind = ListaChequesGetIndiceEntidadxId(IdCheque)
    '            ChequeEgreso = ListaCheques(ind).getClone
    '            'se carga los controles
    '            'ini
    '            With ChequeEgreso

    '                Me.txtNro_DC.Text = .ChequeNumero
    '                Me.txtMonto_DatoCancelacion.Text = .DescMonto
    '                Me.cboMoneda_DatoCancelacion.SelectedValue = CStr(.IdMoneda)
    '                Me.cboBanco.SelectedValue = CStr(.SerieCheque.IdBanco)
    '                actualizarControles_Caja()
    '                Me.cboCuentaBancaria.SelectedValue = CStr(.SerieCheque.IdCuentaBancaria)
    '                Me.txtFechaACobrar.Text = .DescFechaACobrar

    '                'Me.txtMontoCheque.Text = .DescMonto
    '            End With
    '            'fin

    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub
    '    Protected Function ListaChequesGetIndiceEntidadxId(ByVal id As Integer) As Integer
    '        For i As Integer = 0 To ListaCheques.Count - 1
    '            If ListaCheques(i).Id = id Then
    '                Return i
    '            End If
    '        Next
    '        Return -1
    '    End Function
    '#End Region
#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"



    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim listaDocRef As List(Of Entidades.DocumentoView)

            If Me.ckTienda.Checked Then
                listaDocRef = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(Me.CboTipoDocumento.SelectedValue), fechaInicio, fechafin, False)
            Else
                listaDocRef = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), 0, IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(Me.CboTipoDocumento.SelectedValue), fechaInicio, fechafin, False)
            End If

            If (listaDocRef.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = listaDocRef
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRef.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)

            '*************** GUARDAMOS EN SESSION
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** 
            cargarDocumentoRef_GUI(Me.listaDocumentoRef, objPersona)

            '************** COSULTA DE LOS CONCEPTOS DEL DOCUMENTO DE REFERENCIA 
            Me.ListaDetalleConcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumento.Id)

            If Not Me.ListaDetalleConcepto Is Nothing Then
                If Me.ListaDetalleConcepto.Count > 0 Then

                    Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(_IDTIPODOCUMENTO)
                    For i As Integer = 0 To Me.ListaDetalleConcepto.Count - 1
                        Dim var As Integer = i
                        If listaAux.Find(Function(k As Entidades.Concepto) k.Id = Me.ListaDetalleConcepto(var).IdConcepto) Is Nothing Then
                            Dim ListaConcepto As List(Of Entidades.Concepto)
                            ListaConcepto = (New Negocio.Concepto).SelectxId(Me.ListaDetalleConcepto(var).IdConcepto)
                            If Not ListaConcepto Is Nothing Then
                                listaAux.AddRange(ListaConcepto)
                            End If
                        End If
                        Me.ListaDetalleConcepto(i).ListaConcepto = listaAux
                    Next

                    Me.cboMotivoDocCancelacionCaja.SelectedValue = "2"
                    Me.btnAddConcepto_Otros.Visible = True

                End If
            End If

            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularTotalAPagar(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function

    Private Function obtenerListaDetalleDocumento_Ref(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
        Dim objDocumentoRef As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)

        For i As Integer = (listaDetalle.Count - 1) To 0 Step -1

            If ((listaDetalle(i).IdTipoDocumento = 16 Or listaDetalle(i).IdTipoDocumento = 6)) Then  '**** SI EL DOCUMENTO DE REFERENCIA ES ORDEN DE COMPRA O ES UNA GUIA DE REMISION

                listaDetalle(i).Cantidad = listaDetalle(i).CantidadTransito

            End If

            If (listaDetalle(i).Cantidad = 0) Then

                listaDetalle.RemoveAt(i)

            Else

                listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
                listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
                listaDetalle(i).CantxAtender = Nothing
                listaDetalle(i).CantidadTransito = Nothing

            End If

        Next

        Return listaDetalle

    End Function
    Private Sub cargarDocumentoRef_GUI(ByVal listaDocumento As List(Of Entidades.Documento), ByVal objPersona As Entidades.PersonaView)

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '******************* PERSONA
        If (objPersona IsNot Nothing) Then

            With objPersona
                Me.txtDescripcionPersona02.Text = .Descripcion
                Me.txtDNI02.Text = .Dni
                Me.txtRUC02.Text = .Ruc
                Me.hddIdPersona02.Value = CStr(.IdPersona)
            End With

        End If


    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region

    Protected Sub btnSustentar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSustentar.Click
        OnClick_Sustentar()
    End Sub
    Private Sub OnClick_Sustentar()

        verFrm(FrmModo.Documento_Sustento, False, False, False, False, False, False)


        Me.ListaDetalleConcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(IdDocumento)

        If Not Me.ListaDetalleConcepto Is Nothing Then
            If Me.ListaDetalleConcepto.Count > 0 Then

                Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(_IDTIPODOCUMENTO)
                For i As Integer = 0 To Me.ListaDetalleConcepto.Count - 1
                    Dim var As Integer = i
                    If listaAux.Find(Function(k As Entidades.Concepto) k.Id = Me.ListaDetalleConcepto(var).IdConcepto) Is Nothing Then
                        Dim ListaConcepto As List(Of Entidades.Concepto)
                        ListaConcepto = (New Negocio.Concepto).SelectxId(Me.ListaDetalleConcepto(var).IdConcepto)
                        If Not ListaConcepto Is Nothing Then
                            listaAux.AddRange(ListaConcepto)
                        End If
                    End If
                    Me.ListaDetalleConcepto(i).ListaConcepto = listaAux
                Next

            End If
        End If

        '******** Grilla
        Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
        Me.GV_Otros.DataBind()

    End Sub

    Protected Sub btnAddSustento_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        OnClick_AddSustento(CType(CType(sender, Button).NamingContainer, GridViewRow).RowIndex)

    End Sub
    Private Sub OnClick_AddSustento(ByVal index As Integer)
        Try

            Me.hddIndex.Value = CStr(index)

            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)

            Me.listaDocumentoRef_Sustento = (New Negocio.DetalleConcepto_Anexo).DetalleDocumento_Anexo_SelectDocumentoxIdDocumentoxIdDetalleConcepto(CInt(Me.hddIdDocumento.Value), Me.ListaDetalleConcepto(index).IdDetalleConcepto)
            setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

            Me.GV_Documento_Sustento.DataSource = Me.listaDocumentoRef_Sustento
            Me.GV_Documento_Sustento.DataBind()

            Me.Panel_Sustento.Visible = True

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rdbBuscarDocRef2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef2.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef2(CInt(Me.rdbBuscarDocRef2.SelectedValue))
    End Sub
    Private Sub actualizarOpcionesBusquedaDocRef2(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Tr_fecha.Visible = True
                Me.Tr_serie_codigo.Visible = False
            Case 1
                Me.Tr_fecha.Visible = False
                Me.Tr_serie_codigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumento_Sustento")
        End If

    End Sub

    Private Sub btnConsultarDocumentoExterno_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarDocumentoExterno.Click
        Try
            If Not Session("IdDocumentoExt") Is Nothing Then
                cargarDocumentoRef_Sustento(CInt(Session("IdDocumentoExt")))
            Else
                objScript.mostrarMsjAlerta(Me, "No ha registrado un documento.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarDocumentoRef_Sustento(ByVal IdDocumento As Integer)

        Try

            Me.listaDocumentoRef_Sustento = getListaDocumentoRef_Sustento()

            For i As Integer = 0 To Me.listaDocumentoRef_Sustento.Count - 1
                If (Me.listaDocumentoRef_Sustento(i).Id = IdDocumento) Then
                    Throw New Exception("EL DOCUMENTO YA HA SIDO AGREGADO. NO SE PERMITE LA OPERACIÓN.")
                End If
            Next

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)

            Me.listaDocumentoRef_Sustento.Add(objDocumento)

            setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

            Me.GV_Documento_Sustento.DataSource = Me.listaDocumentoRef_Sustento
            Me.GV_Documento_Sustento.DataBind()

            actualizarControles_ReciboIE()

            objScript.onCapa(Me, "capaDocumento_Sustento")

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function getListaDocumentoRef_Sustento() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef_Sustento"), List(Of Entidades.Documento))
    End Function
    Private Sub setListaDocumentoRef_Sustento(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef_Sustento")
        Session.Add("listaDocumentoRef_Sustento", lista)
    End Sub

    Private Sub actualizarControles_ReciboIE()

        '*******************  MANEJO DE LOS BOTONES DE REC INGRESO Y EGRESO
        Dim totalSustentado As Decimal = (New Negocio.RequerimientoGasto).fx_getTotalSustentadoxIdDocumentoReqGasto(CInt(Me.hddIdDocumento.Value))
        Dim totalDocumento As Decimal = CDec(Me.txtTotalAPagar.Text)
        Me.txtTotalSustento.Text = Format(totalSustentado, "F3")

        Me.btn_EmitirReciboIngreso.Visible = False
        Me.btn_EmitirReciboEgreso.Visible = False

        If (totalDocumento > totalSustentado) Then
            Me.btn_EmitirReciboIngreso.Visible = True
        ElseIf (totalDocumento < totalSustentado) Then
            Me.btn_EmitirReciboEgreso.Visible = True
        End If

    End Sub


    Protected Sub btnBuscarDocRef2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarDocRef2.Click

        buscarDocumentoReferenciaxParams()

    End Sub

    Private Sub btnAceptarBuscarDocRef2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRef2.Click

        buscarDocumentoReferenciaxParams()

    End Sub

    Private Sub buscarDocumentoReferenciaxParams()
        Try

            Dim serie As String = ""
            Dim codigo As String = ""
            Dim fechaIni As Date = Nothing
            Dim fechaFin As Date = Nothing

            '*******  0: por fechas
            '*******  1: por nro documento

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0
                    fechaIni = CDate(Me.txtFechaInicio_DocRef2.Text.Trim)
                    fechaFin = CDate(Me.txtFechaFin_DocRef2.Text.Trim)
                Case 1
                    serie = Me.txtSerie_BuscarDocRef2.Text.Trim
                    codigo = Me.txtCodigo_BuscarDocRef2.Text.Trim
            End Select

            '*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            '****  Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocGuiaRecepcion).DocumentoGuiaRecepcion_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), 0, CInt(Me.cboAlmacen.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, fechaInicio, fechafin)
            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef_Externo(0, 0, 0, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(Me.cboTipoDocumento2.SelectedValue), fechaIni, fechaFin, False)

            If (lista.Count > 0) Then
                Me.GV_DocumentoCab_BuscarDocumento.DataSource = lista
                Me.GV_DocumentoCab_BuscarDocumento.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumento_Sustento")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub GV_DocumentoCab_BuscarDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_DocumentoCab_BuscarDocumento.SelectedIndexChanged

        cargarDocumentoRef_Sustento(CInt(CType(Me.GV_DocumentoCab_BuscarDocumento.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value))

    End Sub

    Protected Sub GV_Documento_Sustento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Documento_Sustento.SelectedIndexChanged
        quitarDocumento_Sustento(Me.GV_Documento_Sustento.SelectedIndex)
    End Sub
    Private Sub quitarDocumento_Sustento(ByVal index As Integer)
        Try


            Me.listaDocumentoRef_Sustento = getListaDocumentoRef_Sustento()
            Me.listaDocumentoRef_Sustento.RemoveAt(index)

            setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

            Me.GV_Documento_Sustento.DataSource = Me.listaDocumentoRef_Sustento
            Me.GV_Documento_Sustento.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Function Validar_AperturaCierreCaja(Optional ByVal showMessage As Boolean = True) As Boolean

        Dim objCombo As New Combo
        objCombo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
        If Me.cboCaja.Items.Count > 0 Then lblCaja.Text = (New Negocio.Caja_AperturaCierre).Caja_AperturaCierre_SelectEstado(CInt(Me.cboCaja.SelectedValue), Me.txtFechaEmision.Text.Trim)

        If Me.lblCaja.Text <> "Caja Aperturada" And showMessage Then
            objScript.mostrarMsjAlerta(Me, "La caja no esta apertutada.\nNo se permite la operación.")
            Return False
        End If

        Return True

    End Function

 
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiar.Click
        txtDescripcionPersona.Text = ""
        txtDNI.Text = ""
        txtRUC.Text = ""
        hddIdPersona.Value = "0"
    End Sub

    Protected Sub btnLimpiarBeneciario_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiarBeneciario.Click
        txtDescripcionPersona02.Text = ""
        txtDNI02.Text = ""
        txtRUC02.Text = ""
        hddIdPersona02.Value = "0"
        cboMotivoDocCancelacionCaja.SelectedValue = "0"
        Me.GV_Deudas.DataSource = Nothing
        Me.GV_Deudas.DataBind()
        Me.GV_Otros.DataSource = Nothing
        Me.GV_Otros.DataBind()
    End Sub

    Private Sub btBuscarPersonaGrillas_Click(sender As Object, e As System.EventArgs) Handles btBuscarPersonaGrillas.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", CInt(cboRol.SelectedValue))  '******************** CLIENTE

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btBuscarPersonaGrillas02_Click(sender As Object, e As System.EventArgs) Handles btBuscarPersonaGrillas02.Click
        Try
            ViewState.Add("dni02", tbbuscarDni02.Text.Trim)
            ViewState.Add("ruc02", tbbuscarRuc02.Text.Trim)
            ViewState.Add("pat02", tbRazonApe02.Text.Trim)
            ViewState.Add("tipo02", CInt(IIf(Me.rdbTipoPersona02.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado02", 1) '******************* ACTIVO
            ViewState.Add("rol02", CInt(cboRol02.SelectedValue))  '******************** CLIENTE

            Buscar02(gvBuscar02, CStr(ViewState("dni02")), CStr(ViewState("ruc02")), CStr(ViewState("pat02")), CInt(ViewState("tipo02")), CInt(ViewState("rol02")), CInt(ViewState("estado02")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboPost_DC_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboPost_DC.SelectedIndexChanged
        actualizarControles_POST()
    End Sub
End Class
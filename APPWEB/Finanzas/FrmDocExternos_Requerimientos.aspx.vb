﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador
Imports System.Linq
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports
Imports Entidades.RequerimientoGasto

Imports System.IO

Public Class FrmDocExternos_Requerimientos
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
        End If

        Dim flag As String = Request.QueryString("flag")
        Select Case flag

            Case "ConsultaDocumentos"

                Dim tipoDoc As String = Request.QueryString("tipodoc")
                Dim proveedor As String = Request.QueryString("proveedor")
                Dim serie As String = Request.QueryString("serie")
                Dim codigo As String = Request.QueryString("codigo")
                Dim anio As String = Request.QueryString("anio")


                Dim rptaDoc As String = ""
                Dim listaDoc As New List(Of DocumentosExternos)
                Try

                    listaDoc = (New bl_Pagosdocumentos).selectDocumentosExternos(tipoDoc, proveedor, anio, serie, codigo)

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If listaDoc.Count > 0 Then
                    Try
                        rptaDoc = (New objeto).SerializarLista(listaDoc, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rptaDoc)
                    Response.End()
                Else
                    Response.Write(rptaDoc)
                    Response.End()
                End If
        End Select

    End Sub

    Public Sub inicializarFrm()
        Try
            Dim obj As New Combo

            With obj
                .LlenarCboTipoDocumentoExterno(Me.cboDocumento, True)
                .LlenarCboProveedor1(Me.cboProveedor, 1, True)
                .LlenarCboAnio(Me.cboAnio, False)
            End With
            cboAnio.SelectedValue = 2019


        Catch ex As Exception

        End Try
    End Sub

    Private Sub Natural_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Natural.CheckedChanged
        Try
            Dim obj As New Combo

            If Natural.Checked = True Then

                obj.LlenarCboProveedor1(Me.cboProveedor, 2, True)

            Else

                obj.LlenarCboProveedor1(Me.cboProveedor, 1, True)

            End If

        Catch ex As Exception

        End Try
    End Sub


    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
End Class
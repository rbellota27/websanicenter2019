﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmCheque_Ingreso_Aprobacion
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad_Frm()
    End Sub
    Private Sub valOnLoad_Frm()
        Try

            If (Not Me.IsPostBack) Then
                inicializarFrm()
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub inicializarFrm()

        With objCbo
            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
        End With

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer(255) {})
        If (listaPermisos(0) > 0) Then
            Me.cboTienda.Items.Insert(0, New ListItem("< Todos >", "0"))
        End If

        Dim objProgramacionSemana As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxFecha((New Negocio.FechaActual).SelectFechaActual)
        Me.txtFechaInicio.Text = Format(objProgramacionSemana.cal_FechaIni, "dd/MM/yyyy")
        Me.txtFechaFin.Text = Format(objProgramacionSemana.cal_FechaFin, "dd/MM/yyyy")

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try

            With objCbo
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            End With

            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer(255) {})
            If (listaPermisos(0) > 0) Then
                Me.cboTienda.Items.Insert(0, New ListItem("< Todos >", "0"))
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        buscar_AprobacionCheque()
    End Sub

    Private Sub buscar_AprobacionCheque()
        Try

            Dim estado As Boolean = False
            If (Me.rdbEstado.SelectedValue = "1") Then
                estado = True
            End If

            Dim estadoMov As String = Me.rdbEstadoAprob.SelectedValue

            Me.GV_Cheque.DataSource = (New Negocio.Cheque).Cheque_SelectxParams_DT(Nothing, Nothing, Nothing, Nothing, Nothing, CDate(Me.txtFechaInicio.Text), CDate(Me.txtFechaFin.Text), Nothing, Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), estado, estadoMov)
            Me.GV_Cheque.DataBind()

            If (Me.GV_Cheque.Rows.Count <= 0) Then
                Throw New Exception("NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub GV_Cheque_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Cheque.SelectedIndexChanged

        valOnChange_GV_Cheque(Me.GV_Cheque.SelectedIndex)

    End Sub
    Private Sub valOnChange_GV_Cheque(ByVal index As Integer)
        Try

            Dim IdCheque As Integer = CInt(CType(Me.GV_Cheque.Rows(index).FindControl("hddIdCheque"), HiddenField).Value)
            Dim objCheque As Entidades.Cheque = (New Negocio.Cheque).Cheque_SelectxIdCheque(IdCheque)

            If (objCheque IsNot Nothing) Then
                '***************** cargamos el frm
                With objCheque
                    Me.lblBanco_Update.Text = .Banco
                    Me.lblNro_Update.Text = .Numero
                    Me.lblMoneda_Update.Text = .Moneda
                    Me.lblMonto_Update.Text = Format(.Monto, "F3")

                    If (.FechaCobrar = Nothing) Then
                        Me.lblFechaCobrar_Update.Text = "-----"
                    Else
                        Me.lblFechaCobrar_Update.Text = Format(.FechaCobrar, "dd/MM/yyyy")
                    End If

                    If (Me.rdbEstadoAprob_Update.Items.FindByValue(CStr(.EstadoMov)) IsNot Nothing) Then
                        Me.rdbEstadoAprob_Update.SelectedValue = CStr(.EstadoMov)
                    End If

                    If (.Estado) Then
                        If (Me.rdbEstado_Update.Items.FindByValue("1") IsNot Nothing) Then
                            Me.rdbEstado_Update.SelectedValue = "1"
                        End If
                    Else
                        If (Me.rdbEstado_Update.Items.FindByValue("0") IsNot Nothing) Then
                            Me.rdbEstado_Update.SelectedValue = "0"
                        End If
                    End If

                    Me.txtObservacion.Text = .Observacion
                    Me.hddIdCheque.Value = CStr(.IdCheque)

                End With

                objScript.onCapa(Me, "capaUpdateCheque")

            Else
                Throw New Exception("NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar_Update.Click
        registrar()
    End Sub
    Private Sub registrar()
        Try

            Dim IdCheque As Integer = CInt(Me.hddIdCheque.Value)
            Dim estado As Boolean = False

            Select Case CInt(Me.rdbEstado_Update.SelectedValue)
                Case 0
                    estado = False
                Case 1
                    estado = True
                Case Else
                    Throw New Exception("NO SE HALLÓ EL ESTADO SELECCIONADO")
            End Select

            Dim estadoMov As String = Me.rdbEstadoAprob_Update.SelectedValue
            Dim observacion As String = Me.txtObservacion.Text

            If ((New Negocio.Cheque).Update_Estado_EstadoMovxIdCheque(IdCheque, estado, estadoMov, observacion)) Then
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")
            End If

            '***************** REALIZAMOS LA BUSQUEDA
            Dim estadoFind As Boolean = False
            If (Me.rdbEstado.SelectedValue = "1") Then
                estadoFind = True
            End If
            Dim estadoMovFind As String = Me.rdbEstadoAprob.SelectedValue
            Me.GV_Cheque.DataSource = (New Negocio.Cheque).Cheque_SelectxParams_DT(Nothing, Nothing, Nothing, Nothing, Nothing, CDate(Me.txtFechaInicio.Text), CDate(Me.txtFechaFin.Text), Nothing, Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), estadoFind, estadoMovFind)
            Me.GV_Cheque.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmEmisionCheque
    Inherits System.Web.UI.Page

#Region "Atributos"

#Region "Variables"

    Private objCbo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegDocumentocheque As New Negocio.DocumentoCheque
    Private objNegSeriecheque As New Negocio.SerieChequeView
    Private p As New Entidades.DocumentoCheque
    Private p2 As New Entidades.SerieChequeView
    Private Util As New Util
    Private FechaACobrarInicial, FechaACobrarFinal As New Date
    Private listaDocumentoRef As List(Of Entidades.Documento)

#End Region

#Region "Constantes y Enumeraciones"
    'Este medio de Pago esta asociado a los cheques generados por la empresa (egresos)
    Private Const _IDMEDIOPAGO As Integer = 11
    Private Const _IDTIPODOCUMENTO As Integer = 42
    Private Const _IDCONCEPTOMOVBANCO As Integer = 13

    Enum modo_menu
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Insertar = 3
        Actualizar = 4
    End Enum

#End Region

#End Region

#Region "Propiedades"

    Public Property SListaSerieCheque() As List(Of Entidades.SerieChequeView)
        Get
            Return CType(Session.Item("ListSerieCheque"), List(Of Entidades.SerieChequeView))
        End Get
        Set(ByVal value As List(Of Entidades.SerieChequeView))
            Session.Remove("ListSerieCheque")
            Session.Add("ListSerieCheque", value)
        End Set
    End Property
    Public Property SListaDocumentoCheque() As List(Of Entidades.DocumentoCheque)
        Get
            Return CType(Session.Item("ListaDocumentoCheque"), List(Of Entidades.DocumentoCheque))
        End Get
        Set(ByVal value As List(Of Entidades.DocumentoCheque))
            Session.Remove("ListaDocumentoCheque")
            Session.Add("ListaDocumentoCheque", value)
        End Set
    End Property
    Public Property SModo() As modo_menu
        Get
            Return CType(Session.Item("Modo"), modo_menu)
        End Get
        Set(ByVal value As modo_menu)
            Session.Remove("Modo")
            Session.Add("Modo", value)
        End Set
    End Property
    Public Property SId() As Integer
        Get
            'Return CType(Session.Item("IdRegistroFila"), Integer)
            Return CType(hddIdDocumento.Value, Integer)
        End Get
        Set(ByVal value As Integer)
            'Session.Remove("IdRegistroFila")
            'Session.Add("IdRegistroFila", value)
            hddIdDocumento.Value = CStr(value)
        End Set
    End Property
    Public Property SIdBeneficiario() As Integer
        Get
            Return CType(Session.Item("IdBeneficiario"), Integer)
        End Get
        Set(ByVal value As Integer)
            Session.Remove("IdBeneficiario")
            Session.Add("IdBeneficiario", value)
        End Set
    End Property
    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item(CStr(ViewState("listaDocumentoRef"))), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(CStr(ViewState("listaDocumentoRef")))
        Session.Add(CStr(ViewState("listaDocumentoRef")), lista)
    End Sub
    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property
    Public Property VSPermisoEdicion() As Boolean
        Get
            Return CType(ViewState("permisoEdicion"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.Remove("permisoEdicion")
            ViewState("permisoEdicion") = value
        End Set
    End Property
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ValidarPermisos()
            SModo = modo_menu.Inicio
            inicializarFRM()
            HabilitarControles(SModo)
        Else
            actualizar_atributos()
        End If
    End Sub

    Protected Function ListaDocumentoChequeGetIndiceEntidadxId(ByVal id As Integer) As Integer
        For i As Integer = 0 To SListaDocumentoCheque.Count - 1
            If SListaDocumentoCheque(i).Id = id Then
                Return i
            End If
        Next
        Return -1
    End Function

    Protected Function ListaSerieChequeGetIndiceEntidadxId(ByVal id As Integer) As Integer
        For i As Integer = 0 To SListaSerieCheque.Count - 1
            If SListaSerieCheque(i).Id = id Then
                Return i
            End If
        Next
        Return -1
    End Function

    Private Sub ValidarPermisos()

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(SIdUsuario, New Integer() {149, 150})

        If listaPermisos(0) > 0 Then
            Me.btnNuevo.Enabled = True
            'Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            'Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            VSPermisoEdicion = True
        Else
            VSPermisoEdicion = False
        End If


    End Sub

    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try
            '---Listar TipoDocumento---
            objCbo.LLenarCboTipoDocumentoRefxIdTipoDocumento(cbotipoDocumento, _IDTIPODOCUMENTO, 2, False)
            cargar_controles()
            cargar_recargar_datos()
            cargar_dgv()
            cargar_dgv_info()
            btnAbrirCapaProgramacionPago.Visible = False
            With objCbo
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa_PP, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda_PP, CInt(Me.cboEmpresa_PP.SelectedValue), CInt(Session("IdUsuario")), True)
                .LlenarCboMoneda(Me.cboMoneda_PP, True)
            End With
            actualizarOpcionesBusquedaDocRef(0, False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problema en la inicialización")
        End Try
    End Sub

    Private Sub actualizar_atributos()
        Try
            'permisoEstadoAprob = CType(ViewState("permisoEstadoAprob"), Boolean)

            If SId > 0 Then
                Dim ind As Integer
                ind = ListaDocumentoChequeGetIndiceEntidadxId(SId)
                p = CType(SListaDocumentoCheque(ind).Clone, Entidades.DocumentoCheque)
            End If
            p.IdBeneficiario = SIdBeneficiario
            'p.Beneficiario =

            p.Id = SId
            p.SerieCheque.IdBanco = CInt(Me.cboBanco.SelectedValue)
            p.SerieCheque.IdCuentaBancaria = CInt(IIf(CStr(Me.cboCuentaBancaria.SelectedValue) <> "", Me.cboCuentaBancaria.SelectedValue, 0))

            'p.Observacion = Util.decodificarTexto(txtObservacion.Text)
            If txtMonto.Text <> "" Then
                p.Monto = CDec(Util.decodificarTexto(txtMonto.Text))
            Else
                p.Monto = 0D
            End If
            p.IdUsuario = CType(Session.Item("IdUsuario"), Integer)
            'p.IdSupervisor = CType(Session.Item("IdSupervisor"), Integer)

            p.IdSerieCheque = CInt(Me.cboSerieNumeracion.SelectedValue)
            If txtFechaEmision.Text <> "" Then
                p.FechaEmision = CDate(txtFechaEmision.Text)
            Else
                p.FechaEmision = Nothing
            End If
            If txtFechaACobrar.Text <> "" Then
                p.FechaACobrar = CDate(txtFechaACobrar.Text)
            Else
                p.FechaACobrar = Nothing
            End If

            p.FechaRegistro = Now.Date()
            'Cambiar ini
            'p.FechaAprobacion = Nothing
            'Cambiar fin

            If txtFechaACobrarInicial.Text <> "" Then
                FechaACobrarInicial = CDate(txtFechaACobrarInicial.Text)
            Else
                FechaACobrarInicial = Nothing
            End If

            If txtFechaACobrarFinal.Text <> "" Then
                FechaACobrarFinal = CDate(txtFechaACobrarFinal.Text)
            Else
                FechaACobrarFinal = Nothing
            End If

            p.AnexoDoc.IdMedioPago = _IDMEDIOPAGO
            Dim i As Integer
            If p.IdSerieCheque <> 0 Then
                i = ListaSerieChequeGetIndiceEntidadxId(p.IdSerieCheque)
                With SListaSerieCheque(i)
                    p.IdMoneda = .IdMoneda
                    p.Serie = .Serie
                    'p.Codigo =.
                End With
            End If

            p.IdEstadoDoc = CInt(cboEstadoDoc.SelectedValue)
            p.IdEstadoEntrega = CInt(cboEstadoEntregado.SelectedValue)
            p.IdEstadoCancelacion = CInt(cboEstadoCancelacion.SelectedValue)

            p.IdTipoDocumento = _IDTIPODOCUMENTO
            p.TotalLetras = (New Negocio.ALetras).Letras(Me.txtMonto.Text)
            p.ChequeNumero = txtNumeroCheque.Text
            p.AnexoDoc.NroOperacion = txtNumeroVoucher.Text
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub cargar_recargar_datos()
        SListaDocumentoCheque = objNegDocumentocheque.SelectAll()
        SListaSerieCheque = objNegSeriecheque.SelectAll()
    End Sub


    Private Sub cargar_controles()
        objCbo.LlenarCboBanco(cboBanco, True)
        objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(cboBanco.SelectedValue), True)
        objCbo.LlenarCboSerieChequexIdCuentaBancaria(cboSerieNumeracion, CInt(cboCuentaBancaria.SelectedValue), True)

        objCbo.LLenarCboEstadoDocumento(cboEstadoDoc)
        objCbo.LLenarCboEstadoEntrega(cboEstadoEntregado)
        objCbo.LLenarCboEstadoCancelacion(cboEstadoCancelacion, False)

        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

        Me.txtFechaACobrarInicial.Text = Me.txtFechaEmision.Text
        Me.txtFechaACobrarFinal.Text = Me.txtFechaEmision.Text

        Me.txtFechaACobrar.Text = Me.txtFechaEmision.Text


        Me.txtFechaInicio_DocRef.Text = Me.txtFechaACobrar.Text
        Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text


        Me.txt_FechaIni_PP.Text = Me.txtFechaEmision.Text
        Me.txt_FechaFin_PP.Text = Me.txtFechaEmision.Text
        Me.txtMonto.ReadOnly = False


        'Analizar ¿IdPropietario = IdUsuario? y mas.

        'objCbo.LlenarCboTipoDocumentoIndep(cboTipoDocumento)

        'ListCuentaBancaria=(new Negocio.CuentaBancaria).

        'Capa Persona ini
        objCbo.LlenarCboRol(cboRol, True)
        'Capa Persona fin

    End Sub
    Protected Sub cargar_dgv_info()
        p2.IdBanco = CInt(cboBanco.SelectedValue)
        p2.IdCuentaBancaria = CInt(cboCuentaBancaria.SelectedValue)
        p2.Id = CInt(cboSerieNumeracion.SelectedValue)
        dgvSerieCheque.DataSource = objNegSeriecheque.FiltrarLista(p2, SListaSerieCheque)
        dgvSerieCheque.DataBind()
    End Sub
    Protected Sub cargar_dgv()
        actualizar_atributos()
        If SModo = modo_menu.Inicio Then
            dgvDatos.DataSource = objNegDocumentocheque.FiltrarListaxRangoFechas(p, SListaDocumentoCheque, FechaACobrarInicial, FechaACobrarFinal)
        Else
            dgvDatos.DataSource = objNegDocumentocheque.FiltrarLista(p, SListaDocumentoCheque)
        End If
        dgvDatos.DataBind()
    End Sub

    Public Sub limpiarCtrl()

        Me.cboBanco.SelectedIndex = 0
        Me.cboCuentaBancaria.SelectedIndex = 0
        Me.cboSerieNumeracion.SelectedIndex = 0

        Me.txtNumeroCheque.Text = ""
        Me.txtNumeroVoucher.Text = ""
        Me.txtObservacion.Text = ""
        Me.txtMonto.Text = "0.0"
        'Me.txtFechaACobrar.Text = (New Negocio.FechaActual).SelectFechaActual.ToString
        Me.txtFechaACobrar.Text = Now.Date.ToString()
        Me.txtFechaACobrarInicial.Text = Me.txtFechaACobrar.Text
        Me.txtFechaACobrarFinal.Text = Me.txtFechaACobrar.Text
        Me.txtMonto.ReadOnly = False

        Me.txtFechaEmision.Text = Now.Date.ToString

        'Me.rbtlEstado.SelectedValue = "1" 'Activos
        Me.cboEstadoDoc.SelectedValue = "1"
        'Me.chkEstadoAprobacion.Checked = False

        cboEstadoEntregado.SelectedIndex = 0
        cboEstadoCancelacion.SelectedIndex = 0

        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        Me.GV_ProgramacionPago.DataSource = Nothing
        Me.GV_ProgramacionPago.DataBind()

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        Session.Remove(CStr(ViewState("listaDocumentoRef")))

        limpiarDatosBeneficiario()

    End Sub

    Function getMensajeInformativo(ByVal modo As modo_menu) As String

        Select Case modo
            Case modo_menu.Nuevo
                Return "* El Nro. de Cheque se autogenera, según la Cuenta Bancaria y la Serie-Numeración; pero es editable"
            Case Else
                Return ""
        End Select
    End Function

    Public Sub HabilitarControles(ByVal x As modo_menu)

        Me.btnBuscarDocumento_Ref.Visible = False

        Select Case x
            Case modo_menu.Inicio   'Búsqueda
                Me.lblTituloPnlDatos.Text = "Búsqueda"
                'Me.btnAbrir_ProgramacionPago.Visible = False
                Me.btnNuevo.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscar.Visible = True
                Me.btnImprimir.Visible = False
                btnAbrirCapaProgramacionPago.Visible = False
                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True
                Me.lblMonto.Visible = False
                Me.lblMoneda.Visible = False
                Me.txtMonto.Visible = False

                Me.lblFechaACobrar.Visible = False
                Me.txtFechaACobrar.Visible = False
                Me.lblFechaACobrarInicial.Visible = True
                Me.txtFechaACobrarInicial.Visible = True
                Me.lblFechaACobrarFinal.Visible = True
                Me.txtFechaACobrarFinal.Visible = True

                Me.lblFechaEmision.Visible = False
                Me.txtFechaEmision.Visible = False

                txtFechaACobrarInicial.Text = Now.Date.ToString
                txtFechaACobrarFinal.Text = txtFechaACobrarInicial.Text

                Me.dgvDatos.Enabled = True
                Panel_PersonaBenef.Visible = False
                Panel_Observacion.Visible = False

                Me.lblMensajeInformativo.Visible = False
                Me.lblNumeroVoucher.Visible = False
                Me.txtNumeroVoucher.Visible = False

            Case modo_menu.Nuevo
                Me.lblTituloPnlDatos.Text = "Nuevo"
                'Me.btnAbrir_ProgramacionPago.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumento_Ref.Visible = True
                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True
                Me.txtMonto.ReadOnly = False
                btnAbrirCapaProgramacionPago.Visible = False
                Me.lblMonto.Visible = True
                Me.lblMoneda.Visible = True
                Me.txtMonto.Visible = True

                Me.lblFechaACobrar.Visible = True
                Me.txtFechaACobrar.Visible = True
                Me.lblFechaACobrarInicial.Visible = False
                Me.txtFechaACobrarInicial.Visible = False
                Me.lblFechaACobrarFinal.Visible = False
                Me.txtFechaACobrarFinal.Visible = False

                Me.lblFechaEmision.Visible = True
                Me.txtFechaEmision.Visible = True

                txtFechaACobrarInicial.Text = ""
                txtFechaACobrarFinal.Text = ""

                Me.dgvDatos.Enabled = False


                Panel_PersonaBenef.Visible = True
                Panel_Observacion.Visible = True

                Me.lblMensajeInformativo.Visible = True
                Me.lblMensajeInformativo.Text = getMensajeInformativo(modo_menu.Nuevo)
                Me.lblNumeroVoucher.Visible = True
                Me.txtNumeroVoucher.Visible = True
            Case modo_menu.Editar
                Me.lblTituloPnlDatos.Text = "Edición"
                'Me.btnAbrir_ProgramacionPago.Visible = False
                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumento_Ref.Visible = True
                Me.cboBanco.Enabled = False
                btnAbrirCapaProgramacionPago.Visible = False
                Me.cboCuentaBancaria.Enabled = False

                Me.lblMonto.Visible = True
                Me.lblMoneda.Visible = True
                Me.txtMonto.Visible = True

                Me.lblFechaACobrar.Visible = True
                Me.txtFechaACobrar.Visible = True
                Me.lblFechaACobrarInicial.Visible = False
                Me.txtFechaACobrarInicial.Visible = False
                Me.lblFechaACobrarFinal.Visible = False
                Me.txtFechaACobrarFinal.Visible = False

                Me.lblFechaEmision.Visible = True
                Me.txtFechaEmision.Visible = True

                txtFechaACobrarInicial.Text = ""
                txtFechaACobrarFinal.Text = ""

                Me.dgvDatos.Enabled = False

                Panel_PersonaBenef.Visible = True
                Panel_Observacion.Visible = True

                Me.lblMensajeInformativo.Visible = False
                Me.lblNumeroVoucher.Visible = True
                Me.txtNumeroVoucher.Visible = True

            Case modo_menu.Actualizar
            Case modo_menu.Insertar
        End Select
    End Sub


#Region "Eventos"
#Region "Botones"

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        SId = 0
        If CDate(txtFechaACobrarInicial.Text) <= CDate(txtFechaACobrarFinal.Text) Then
            cargar_dgv()
            cargar_dgv_info()
            btnImprimir.Visible = True
        Else
            objScript.mostrarMsjAlerta(Me, "La fecha inicial es mayor que la fecha final")
        End If
    End Sub


    Protected Sub lbtnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            btnAbrirCapaProgramacionPago.Visible = False
            Dim ind As Integer 'indice de la lista correspondiente al registro a modificar
            SModo = modo_menu.Editar
            'Se obtiene el Id del Registro a editar 
            'ini
            Dim lbtnEditar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnEditar.NamingContainer, GridViewRow)
            SId = CInt(CType(fila.Cells(0).FindControl("hddId"), HiddenField).Value.Trim())
            'fin

            'se obtiene el indice de la lista, para asi obtener la entidad a editar 
            ind = ListaDocumentoChequeGetIndiceEntidadxId(SId)

            With SListaDocumentoCheque(ind)

                Me.cboBanco.SelectedValue = CStr(.SerieCheque.IdBanco)
                objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), True)
                Me.cboCuentaBancaria.SelectedValue = CStr(.SerieCheque.IdCuentaBancaria)
                objCbo.LlenarCboSerieChequexIdCuentaBancaria(cboSerieNumeracion, CInt(Me.cboCuentaBancaria.SelectedValue), True)
                Me.cboSerieNumeracion.SelectedValue = CStr(.SerieCheque.Id)

                Me.cboEstadoDoc.SelectedValue = CStr(.IdEstadoDoc)
                Me.cboEstadoEntregado.SelectedValue = CStr(.IdEstadoEntrega)
                Me.cboEstadoCancelacion.SelectedValue = CStr(.IdEstadoCancelacion)

                'Me.rbtlEstado.SelectedValue = CStr()
                Dim objObs As Entidades.Observacion
                objObs = (New Negocio.Observacion).SelectxIdDocumento(.Id)

                If objObs IsNot Nothing Then
                    Me.txtObservacion.Text = objObs.Observacion
                End If

                Me.txtMonto.Text = CStr(FormatNumber(.Monto, 2))
                Me.txtMonto.ReadOnly = True

                Me.txtFechaACobrar.Text = .DescFechaACobrar
                If Me.txtFechaACobrar.Text.Trim = "" Then
                    'Me.txtFechaACobrar.Text = (New Negocio.FechaActual).SelectFechaActual.ToString
                    Me.txtFechaACobrar.Text = Now.Date.ToString
                End If

                Me.txtFechaEmision.Text = .DescFechaEmision
                If Me.txtFechaEmision.Text.Trim = "" Then
                    'Me.txtFechaACobrar.Text = (New Negocio.FechaActual).SelectFechaActual.ToString
                    Me.txtFechaEmision.Text = Now.Date.ToString
                End If

                Me.txtNumeroCheque.Text = .ChequeNumero
                Me.txtNumeroVoucher.Text = .AnexoDoc.NroOperacion

                cargarPersona(.IdBeneficiario)

            End With

            '******************** MOSTRAMOS LA LISTA DE DOCUMENTOS DE REFERENCIA
            Dim listaDocumentoRef As List(Of Entidades.Documento) = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(SListaDocumentoCheque(ind).Id)
            Me.GV_DocumentoRef.DataSource = listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            HabilitarControles(SModo)
            cargar_dgv_info()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        SModo = modo_menu.Nuevo
        HabilitarControles(SModo)
        'limpiarCtrl()  'se deben dejar los datos escritos
        cargar_dgv()
        obtenerNumeroCheque(SModo)
        btnAbrirCapaProgramacionPago.Visible = True
        Me.GV_ProgramacionPago.DataSource = Nothing
        Me.GV_ProgramacionPago.DataBind()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        SModo = modo_menu.Inicio
        SId = 0   'Limpia el ID
        HabilitarControles(SModo)
        limpiarCtrl()
        btnAbrirCapaProgramacionPago.Visible = False
        cargar_dgv()
    End Sub

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)

        Dim listaDocumentoRef As List(Of Entidades.Documento) = obtenerListaDocumentoRef()

        For i As Integer = 0 To listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento

            With objRelacionDocumento
                .IdDocumento1 = listaDocumentoRef(i).Id
                .IdDocumento2 = Nothing

            End With
            lista.Add(objRelacionDocumento)

        Next


        Return lista

    End Function


    Private Function obtenerListaRelacionDocumentoProgramacion() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)

        Dim objRelacionDocumento As New Entidades.RelacionDocumento

        With objRelacionDocumento
            .IdDocumento1 = CInt(hddIdProgramacionPago.Value)
            .IdDocumento2 = Nothing

        End With
        lista.Add(objRelacionDocumento)


        Return lista

    End Function

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        'qwe
        If (IsNumeric(txtNumeroCheque.Text) And IsNumeric(txtNumeroVoucher.Text)) Then

            Try
                Dim valorparapasar As Integer = 0
                Dim valor As Decimal = 0
                Dim valorMonto As Decimal = 0
                Dim mensaje As String
                Dim exito As Boolean
                exito = True
                mensaje = "Se Guardó Correctamente"
                'Solo entran los modos editar y nuevo, en esta zona
                'opcional ini
                cargar_recargar_datos() 'Los datos disponibles se actualizan antes de realizar la validacion
                'opcional fin

                '******************* OBTENEMOS LA LISTA RELACION DOCUMENTO
                Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

                Dim listaRelacionDoc2Prog As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumentoProgramacion()


                For Each row As GridViewRow In Me.GV_DocumentoRef.Rows
                    valor += CDec(CType(row.FindControl("lblsaldo"), Label).Text)
                Next

                For Each row As GridViewRow In Me.GV_DocumentoRef.Rows
                    valorMonto += CDec(CType(row.FindControl("lbltotal"), Label).Text)
                Next




                Select Case SModo



                    Case modo_menu.Nuevo

                        'If objNegDocumentocheque.ValidarEntidad(p, SListaDocumentoCheque) Then
                        'p.FechaRegistro = Now--Este se hace en la bd
                        If (CDec(txtMonto.Text) <= valorMonto) Then
                            valorparapasar = 1
                        Else
                            mensaje = "El Monto ingresado supera el Monto del documento. No procede la operación."
                        End If

                        If (valorparapasar = 1) Then

                            If objNegDocumentocheque.ValidarNumeroCheque(p.Id, p.IdSerieCheque, p.ChequeNumero) Then

                                If Not CBool(objNegDocumentocheque.RegistrarDocumentoCheque(CType(p, Entidades.Documento), obtenerObservacion(), obtenerMovBanco(p), p.AnexoDoc, listaRelacionDocumento, listaRelacionDoc2Prog)) Then
                                    'If objNegDocumentocheque.InsertAll(p, ListaDocRef) Then
                                    exito = False
                                    mensaje = "Problema en la Inserción"
                                End If


                            Else
                                exito = False
                                mensaje = "El número de Cheque ya esta siendo utilizado"
                            End If
                            'Else
                            '    objScript.mostrarMsjAlerta(Me, "Ya existe un registro con ese Banco, Cuenta Bancaria y Tipo de Tarjeta")
                            'End If
                        End If

                    Case modo_menu.Editar


                        If (txtMonto.Text.Length > 0) Then
                            valorparapasar = 1
                        End If

                        If (valorparapasar = 1) Then
                            If objNegDocumentocheque.ValidarNumeroCheque(p.Id, p.IdSerieCheque, p.ChequeNumero) Then
                                If Not CBool(objNegDocumentocheque.ActualizarDocumentoCheque(CType(p, Entidades.Documento), obtenerObservacion(), obtenerMovBanco(p, True), p.AnexoDoc, listaRelacionDocumento)) Then
                                    'If objNegDocumentocheque.UpdateAll(p, ListaDocRef) Then
                                    exito = False
                                    mensaje = "Problema en la Actualización"
                                End If
                            Else
                                exito = False
                                mensaje = "El número de Cheque ya esta siendo utilizado"
                            End If
                        End If

                End Select


                objScript.mostrarMsjAlerta(Me, mensaje)
                If (valorparapasar = 1) Then
                    If exito Then
                        'cargar_recargar_Id(0)   'Limpia el ID
                        SId = 0                 'Limpia el ID
                        cargar_recargar_datos()
                        'cargar_recargar_modo(modo_menu.Inicio)
                        SModo = modo_menu.Inicio
                        HabilitarControles(SModo)
                        cargar_dgv()    'Muestra el registro ingresado
                        cargar_dgv_info()
                        limpiarCtrl()   'Limpia los controles
                    End If
                Else
                End If

            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try
        Else
            objScript.mostrarMsjAlerta(Me, "Ingrese un codigo correcto para continuar con el registro. No procede la operación.")
        End If

    End Sub

#End Region

#Region "Carga de Combos"

    Protected Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboBanco.SelectedIndexChanged
        objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(cboBanco.SelectedValue), True)
        cboSerieNumeracion.Items.Clear()
        objCbo.LlenarCboSerieChequexIdCuentaBancaria(cboSerieNumeracion, CInt(cboCuentaBancaria.SelectedValue), True)
        cargar_dgv_info()
    End Sub

    Protected Sub cboCuentaBancaria_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCuentaBancaria.SelectedIndexChanged

        OnChange_cboCuentaBancaria()
    End Sub

    Private Sub OnChange_cboCuentaBancaria()
        If SModo = modo_menu.Nuevo Or SModo = modo_menu.Editar Then
            Dim i As New Integer
            i = cboCuentaBancaria.SelectedItem.Text.IndexOf(" "c)
            If i > -1 Or CInt(cboCuentaBancaria.SelectedValue) <> 0 Then
                Me.lblMoneda.Text = cboCuentaBancaria.SelectedItem.Text.Substring(0, i)
            Else
                Me.lblMoneda.Text = ""
            End If
        End If
        objCbo.LlenarCboSerieChequexIdCuentaBancaria(cboSerieNumeracion, CInt(cboCuentaBancaria.SelectedValue), True)
        cargar_dgv_info()
        obtenerNumeroCheque(SModo)
    End Sub


    Protected Sub cboSerieNumeracion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSerieNumeracion.SelectedIndexChanged

        cargar_dgv_info()
        obtenerNumeroCheque(SModo)
    End Sub


#End Region
#End Region
    Sub obtenerNumeroCheque(ByVal modo As modo_menu)
        If modo = modo_menu.Nuevo Then
            Dim x As Integer
            x = CInt(cboSerieNumeracion.SelectedValue)
            If x <> 0 Then
                txtNumeroCheque.Text = objNegDocumentocheque.GenerarDocCodigoChequexIdSerieCheque(x)
            Else
                txtNumeroCheque.Text = ""
            End If
        End If
    End Sub
    Protected Sub dgvDatos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvDatos.RowDataBound
        'LinkButton Editar esta en un UpdatePanel y para que este no atrape su postback, se registra en el ScriptManager de la página Maestra
        Try
            If VSPermisoEdicion Then
                If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
                e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
                    Dim sm As ScriptManager
                    sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                    sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("lbtnEditar"), LinkButton))
                End If
            Else
                'e.Row.Cells(0).Visible = False
                dgvDatos.Columns.Item(0).Visible = False
            End If
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Function obtenerObservacion() As Entidades.Observacion
        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservacion.Text.Trim.Length > 0) Then

            objObservacion = New Entidades.Observacion

            With objObservacion

                .Id = Nothing
                Select Case SModo
                    Case modo_menu.Nuevo
                        .IdDocumento = Nothing
                    Case modo_menu.Editar
                        .IdDocumento = SId
                End Select
                .Observacion = Me.txtObservacion.Text.Trim

            End With
        End If

        Return objObservacion
    End Function

    Private Function obtenerMovBanco(ByVal x As Entidades.DocumentoCheque, Optional ByVal actualizar As Boolean = False) As Entidades.MovBanco
        'Esta funcion genera o actualiza un movbanco.
        Dim obj As Entidades.MovBanco
        If Not actualizar Then
            obj = New Entidades.MovBanco
        Else
            Dim objDAO As New Negocio.MovBancoView
            Dim objDAOAnexo As New Negocio.Anexo_MovBanco
            Dim L As List(Of Entidades.Anexo_MovBanco)
            L = objDAOAnexo.SelectxIdDocumento(SId)

            Dim i As Integer
            Dim obj1 As Entidades.MovBanco

            For i = 0 To L.Count - 1
                obj1 = objDAO.SelectxId(L(i).IdMovBanco)
                If obj1.IdConceptoMovBanco = _IDCONCEPTOMOVBANCO Then
                    Exit For
                End If
            Next
            If obj1 IsNot Nothing Then
                obj = obj1
            Else
                obj = New Entidades.MovBanco
            End If

            'obj=objDAO.SelectxId(objDAOAnexo.

        End If

        If x.IdEstadoCancelacion = 2 Then
            obj.EstadoAprobacion = True
            obj.IdSupervisor = SIdUsuario
        Else
            obj.EstadoAprobacion = False
        End If

        obj.IdBanco = x.SerieCheque.IdBanco
        obj.IdCuentaBancaria = x.SerieCheque.IdCuentaBancaria
        obj.EstadoMov = True
        obj.Factor = -1
        obj.IdPersonaRef = x.IdBeneficiario
        obj.Monto = x.Monto
        obj.IdConceptoMovBanco = _IDCONCEPTOMOVBANCO
        obj.FechaMov = Now()

        Return obj

    End Function


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)

        'If (CInt(Me.hddOpcionBusquedaPersona.Value) = 3) Then   '*********** CHOFER
        '    listaPersona = (New Negocio.Chofer).ChoferSelectActivoxParams(dni, ruc, RazonApe, tipo, index, gv.PageSize, estado)
        'Else
        '    listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        'End If

        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO

            ViewState.Add("rol", CInt(cboRol.SelectedValue))
            'Select Case CInt(Me.hddOpcionBusquedaPersona.Value)

            '    Case 0  '****** REMITENTE
            '        ViewState.Add("rol", 0)
            '    Case 1  '****** DESTINATARIO
            '        ViewState.Add("rol", 0)  '*** TODOS LOS ROLES
            '    Case 2 '******* TRANSPORTISTA
            '        ViewState.Add("rol", 4)
            '    Case 3 '******* CHOFER

            'End Select

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            'objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            'objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            'objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            'objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try
            limpiarDatosBeneficiario()
            'Me.lblMensajeDocumento.Text = ""
            'Me.gvwCxCxDocumento.DataSource = Nothing
            'gvwCxCxDocumento.DataBind()
            'Me.gvwDetalleAbonos.DataSource = Nothing
            'gvwDetalleAbonos.DataBind()

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            'cargarDatosCuentaPersona(dgvCuentaxPersona)
            'informacion_cuentas_empresa_y_tienda()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la búsqueda de personas")

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")


        SIdBeneficiario = objPersona.IdPersona
        'Me.txtNombrePersonaBenef.Text = objPersona.Descripcion
        'Me.txtDNI.Text = objPersona.Dni
        'Me.txtRUC.Text = objPersona.Ruc

        Me.lblBeneficiario.Text = objPersona.Descripcion
        Me.lblDNI.Text = objPersona.Dni
        Me.lblRUC.Text = objPersona.Ruc
        'Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        objScript.offCapa(Me, "capaPersona")

    End Sub
    Public Sub limpiarDatosBeneficiario()
        SIdBeneficiario = 0 'Se limpia el id del beneficiario

        Me.lblBeneficiario.Text = ""
        Me.lblDNI.Text = ""
        Me.lblRUC.Text = ""
    End Sub

#End Region

#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        txtMonto.ReadOnly = False

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (IsNumeric(SIdBeneficiario)) Then
                If (CInt(SIdBeneficiario) > 0) Then
                    IdPersona = CInt(SIdBeneficiario)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            '*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            '****  Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocGuiaRecepcion).DocumentoGuiaRecepcion_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), 0, CInt(Me.cboAlmacen.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, fechaInicio, fechafin)

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(0, 0, IdPersona, _IDTIPODOCUMENTO, serie, codigo, CInt(Me.cbotipoDocumento.SelectedValue), fechaInicio, fechafin, False)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub



    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        For i As Integer = 0 To Me.GV_DocumentoRef.Rows.Count - 1

            If (CInt(CType(Me.GV_DocumentoRef.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value) = CInt(IdDocumentoRef)) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function
    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumentoReferenciaCheckes(IdDocumentoRef)

            If (objDocumento IsNot Nothing) Then

                Dim lista As List(Of Entidades.Documento) = obtenerListaDocumentoRef()
                lista.Add(objDocumento)

                Me.GV_DocumentoRef.DataSource = lista
                Me.GV_DocumentoRef.DataBind()

                Me.GV_DocumentosReferencia_Find.DataSource = Nothing
                Me.GV_DocumentosReferencia_Find.DataBind()

            Else

                Throw New Exception("NO SE HALLARON REGISTROS.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaDocumentoRef() As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)
        Dim objUtil As New Util

        For i As Integer = 0 To Me.GV_DocumentoRef.Rows.Count - 1

            Dim obj As New Entidades.Documento

            With obj

                .NomTipoDocumento = objUtil.decodificarTexto(Me.GV_DocumentoRef.Rows(i).Cells(1).Text)
                .NroDocumento = CStr(CType(Me.GV_DocumentoRef.Rows(i).FindControl("lblNroDocumento"), Label).Text)
                .Id = CInt(CType(Me.GV_DocumentoRef.Rows(i).FindControl("hddIddo"), HiddenField).Value)
                If (.Id = 0) Then
                    .Id = CInt(CType(Me.GV_DocumentoRef.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)
                End If
                .Saldo = CDec(CType(Me.GV_DocumentoRef.Rows(i).FindControl("lblsaldo"), Label).Text)
                .Total = CDec(CType(Me.GV_DocumentoRef.Rows(i).FindControl("lblTotal"), Label).Text)
                .NomMoneda = CStr(CType(Me.GV_DocumentoRef.Rows(i).FindControl("lblMoneda2"), Label).Text)
                .FechaEmision = CDate(objUtil.decodificarTexto(Me.GV_DocumentoRef.Rows(i).Cells(3).Text))
                .DescripcionPersona = CStr(objUtil.decodificarTexto(Me.GV_DocumentoRef.Rows(i).Cells(7).Text))
                .NomEstadoDocumento = CStr(objUtil.decodificarTexto(Me.GV_DocumentoRef.Rows(i).Cells(8).Text))

            End With

            lista.Add(obj)

        Next

        Return lista

    End Function




    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Dim lista As List(Of Entidades.Documento) = obtenerListaDocumentoRef()
            lista.RemoveAt(index)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = lista
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

   
#End Region


    Private Sub cboEmpresa_PP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa_PP.SelectedIndexChanged


        With objCbo
            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa_PP, CInt(Session("IdUsuario")), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda_PP, CInt(Me.cboEmpresa_PP.SelectedValue), CInt(Session("IdUsuario")), False)
            .LlenarCboMoneda(Me.cboMoneda_PP, True)
        End With

        'ModalPopup_ProgramacionPago.Show()



    End Sub

    Private Sub btnBuscar_PP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar_PP.Click
        Try
            Me.GV_ProgramacionPago.DataSource = Nothing
            Me.GV_ProgramacionPago.DataBind()
       
            Me.GV_ProgramacionPago.DataSource = (New Negocio.MovCuentaPorPagar).SelectProgramacionPagoRQxFechas(CDate(Me.txt_FechaIni_PP.Text), CDate(Me.txt_FechaFin_PP.Text)) '' 1''El ultimo 0 se agrego por el idtipoDocumento
            ''(New Negocio.MovCuentaPorPagar)._MovCuentaPorPagarRQCheckes(CInt(Me.cboEmpresa_PP.SelectedValue), CInt(cboTienda_PP.SelectedValue), CInt(cboMoneda_PP.SelectedValue), Me.SIdBeneficiario, CDate(Me.txt_FechaIni_PP.Text), CDate(Me.txt_FechaFin_PP.Text), 0, 0, 1, 0, 0, 100, 0) '' 1''El ultimo 0 se agrego por el idtipoDocumento
            Me.GV_ProgramacionPago.DataBind()

            If (GV_ProgramacionPago.Rows.Count > 0) Then
                objScript.onCapa(Me, "CapaProgramacionpPago")
                Me.Panel_ProgramacionPago.Visible = True
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('CapaProgramacionpPago');  alert('No se hallaron registros.');  ", True)
                Me.Panel_ProgramacionPago.Visible = True
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Error")
        End Try
    End Sub

    Private Sub GV_ProgramacionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_ProgramacionPago.SelectedIndexChanged

        'Dim IdMovCtaPP As Integer = CInt(CType(Me.GV_ProgramacionPago.Rows(Me.GV_ProgramacionPago.SelectedRow.RowIndex).FindControl("hddIdMovCtaPP"), HiddenField).Value)

        'Dim lista As List(Of Entidades.ProgramacionPago_CXP) = (New Negocio.ProgramacionPago_CXP).SelectxIdMovCuentaCXP2(IdMovCtaPP)

        ' ''PONIENDO READONLY AL TXTMONTO PARA QUE NO SE MODIFIQUE.
        'txtMonto.ReadOnly = True


        'Me.GV_CXP_PP.DataSource = lista
        'Me.GV_CXP_PP.DataBind()

        'ModalPopup_ListaPP.Show()

        cargarDatos_GV_ProgramacionPago(Me.GV_ProgramacionPago.SelectedIndex)



    End Sub

    'Private Sub GV_CXP_PP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_CXP_PP.SelectedIndexChanged

    '    cargarDatos_CXP_PP(Me.GV_CXP_PP.SelectedIndex)

    'End Sub

    Private Sub cargarDatos_GV_ProgramacionPago(ByVal index As Integer)

        Try

            Dim objCbo As New Combo
            Dim IdProgramacionPago_CXP As Integer = CInt(CType(Me.GV_ProgramacionPago.Rows(index).FindControl("hddIdProgramacionPago_CXP"), HiddenField).Value)
            Me.hddIdProgramacionPago.Value = CStr(IdProgramacionPago_CXP)

            Dim objProgramacionPago As Entidades.ProgramacionPago_CXP = (New Negocio.ProgramacionPago_CXP).SelectxIdProgramacionPago(IdProgramacionPago_CXP)

            If (objProgramacionPago IsNot Nothing) Then

                With objProgramacionPago

                    If (Me.cboBanco.Items.FindByValue(CStr(.IdBanco)) IsNot Nothing) Then
                        Me.cboBanco.SelectedValue = CStr(.IdBanco)
                        ' deberia incluirse la empresa ya es que multi-empresa 
                        'objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(.IdBanco), CInt(Me.cboEmpresa.SelectedValue), True)
                        objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(cboBanco.SelectedValue), True)
                    End If

                    If (Me.cboCuentaBancaria.Items.FindByValue(CStr(.IdCuentaBancaria)) IsNot Nothing) Then
                        Me.cboCuentaBancaria.SelectedValue = CStr(.IdCuentaBancaria)
                        OnChange_cboCuentaBancaria()
                    End If

                    txtFechaACobrar.Text = CStr(.FechaPagoProg)

                    Dim monto As Decimal = 0

                    If IsNumeric(Me.txtMonto.Text) Then
                        monto = CDec(Me.txtMonto.Text)
                    End If

                    Me.txtMonto.Text = CStr(Math.Round(monto + (.MontoPagoProg * 1), 3))




                    If (.Observacion <> Nothing) Then
                        Me.txtObservacion.Text = .Observacion
                    End If

                End With

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try



        Try
            Dim IdMovCtaPP As Integer = CInt(CType(Me.GV_ProgramacionPago.Rows(index).FindControl("hddIdMovCtaPP"), HiddenField).Value)

            For Each row As GridViewRow In Me.GV_ProgramacionPago.Rows

                If CInt(CType(row.FindControl("hddIdMovCtaPP"), HiddenField).Value) = IdMovCtaPP Then

                    cargarPersona(CInt(CType(row.FindControl("hddIdProveedor"), HiddenField).Value))

                    cargarDocumentoReferencia(CInt(CType(row.FindControl("hddIdDocumento"), HiddenField).Value))

                End If

            Next

        Catch ex As Exception

        End Try

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  offCapa('CapaProgramacionpPago');  ", True)
        'Me.GV_CXP_PP.DataBind()
        'ModalPopup_ListaPP.Hide()
        'ModalPopup_ProgramacionPago.Hide()

    End Sub
End Class
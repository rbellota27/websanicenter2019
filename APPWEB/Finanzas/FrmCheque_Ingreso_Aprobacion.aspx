﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmCheque_Ingreso_Aprobacion.aspx.vb" Inherits="APPWEB.FrmCheque_Ingreso_Aprobacion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda">
                CHEQUES - APROBACIÓN DE INGRESO A CAJA
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Empresa:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto" style="font-weight: bold">
                                        Tienda:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTienda" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Fecha Inicio:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicio" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicio" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td class="Texto" style="font-weight: bold">
                                        Fecha Fin:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Estado Aprob.:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="rdbEstadoAprob" runat="server" Font-Bold="true" CssClass="Texto"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Value="PA" Selected="True">Por Aprobar</asp:ListItem>
                                            <asp:ListItem Value="AP">Aprobados</asp:ListItem>
                                            <asp:ListItem Value="RE">Rechazados</asp:ListItem>
                                            <asp:ListItem Value="">Todos</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="80px" ToolTip="Buscar" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Estado:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="rdbEstado" runat="server" Font-Bold="true" CssClass="Texto"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                            <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_Cheque" runat="server" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" SelectText="Editar"
                            ShowSelectButton="true" />
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Empresa/Tienda" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblEmpresa_Tienda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Empresa_Tienda")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdCheque" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCheque")%>' />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago")%>' />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdBanco" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdBanco")%>' />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="HiddenField2" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTienda")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Cliente" HeaderStyle-HorizontalAlign="Center" HeaderText="Cliente"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Banco" HeaderStyle-HorizontalAlign="Center" HeaderText="Banco"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="NroCheque" HeaderStyle-HorizontalAlign="Center" HeaderText="Nro."
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="FechaMov" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Fecha Reg." ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="FechaCobrar" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Fecha Cobrar" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="EstadoMov_Desc" HeaderStyle-HorizontalAlign="Center" HeaderText="Estado"
                            ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Usuario" HeaderStyle-HorizontalAlign="Center" HeaderText="Solicitante"
                            ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdCheque" runat="server" />
            </td>
        </tr>
    </table>
    <div id="capaUpdateCheque" style="border: 3px solid blue; padding: 8px; width: 700px;
        height: auto; position: absolute; top: 150px; left: 100px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaUpdateCheque')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Banco:
                            </td>
                            <td>
                                <asp:Label ID="lblBanco_Update" CssClass="LabelRojo" runat="server" Text="-"></asp:Label>
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Nro.
                            </td>
                            <td>
                                <asp:Label ID="lblNro_Update" CssClass="LabelRojo" runat="server" Text="-"></asp:Label>
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Monto:
                            </td>
                            <td>
                                <asp:Label ID="lblMoneda_Update" CssClass="LabelRojo" runat="server" Text="-"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblMonto_Update" CssClass="LabelRojo" runat="server" Text="0"></asp:Label>
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Fecha Cobrar:
                            </td>
                            <td>
                                <asp:Label ID="lblFechaCobrar_Update" CssClass="LabelRojo" runat="server" Text="-"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Estado Mov.:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rdbEstadoAprob_Update" runat="server" Font-Bold="true" CssClass="Texto"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="PA" Selected="True">Por Aprobar</asp:ListItem>
                                    <asp:ListItem Value="AP">Aprobado</asp:ListItem>
                                    <asp:ListItem Value="RE">Rechazado</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td style="width: 20px">
                                &nbsp;
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Estado:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rdbEstado_Update" runat="server" Font-Bold="true" CssClass="Texto"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" Width="100%"
                        Height="75px"></asp:TextBox>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnGuardar_Update" OnClientClick=" return(  confirm('Desea continuar con la Operación ?')  ); "
                                    runat="server" Text="Guardar" Width="80px" ToolTip="Guardar" />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_Update" runat="server" Text="Cerrar" Width="80px" ToolTip="Cerrar"
                                    OnClientClick=" return(offCapa('capaUpdateCheque')); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

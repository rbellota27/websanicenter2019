﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration

Partial Public Class FrmReporteProgramacionPago
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Limpiar()
        End If

    End Sub
    Private Sub Limpiar()

        Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaFin.Text = Me.txtFechaInicio.Text
        Me.GV_Inventrio.DataSource = Nothing
        Me.GV_Inventrio.DataBind()

    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
        If (GV_Inventrio.Rows.Count = 0) Then
            objScript.mostrarMsjAlerta(Me, "Por Favor Ingrese los Criterios de Búsqueda")
        Else
            ExportToExcel("ReporteProgramacionPago.xls", GV_Inventrio)
        End If
    End Sub
    Private Sub ExportToExcel(ByVal nameReport As String, ByVal wControl As GridView)
        Dim responsePage As HttpResponse = Response
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim pageToRender As New Page()
        Dim form As New HtmlForm()
        Dim form2 As New HtmlForm()
        form.Controls.Add(wControl)
        pageToRender.Controls.Add(form)
        responsePage.Clear()
        responsePage.Buffer = True
        responsePage.ContentType = "application/vnd.ms-excel"
        responsePage.AddHeader("Content-Disposition", "attachment;filename=" & nameReport)
        responsePage.Charset = "UTF-8"
        responsePage.ContentEncoding = Encoding.Default
        pageToRender.RenderControl(htw)
        responsePage.Write(sw.ToString())
        responsePage.End()
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click

        Me.GV_Inventrio.DataSource = Nothing
        Me.GV_Inventrio.DataBind()

        Dim obj As Entidades.ProgramacionReporte = New Entidades.ProgramacionReporte
        obj.FechaInicio = CDate(txtFechaInicio.Text)
        obj.FechaFin = CDate(txtFechaFin.Text)
        'obj.IdProgramacionPago = CInt(cboFiltro_Opcion_Programacion.SelectedValue)

        Me.GV_Inventrio.DataSource = (New Negocio.MovCuentaPorPagar).ReporteProgramaciones(CDate(obj.FechaInicio), CDate(obj.FechaFin))
        Me.GV_Inventrio.DataBind()

    End Sub
End Class
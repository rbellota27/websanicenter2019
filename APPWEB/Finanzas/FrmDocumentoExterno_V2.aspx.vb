﻿Imports System.IO

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Partial Public Class FrmDocumentoExterno_V2
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaCentroCosto As List(Of Entidades.GastoCentroCosto)
    Private listaCentroCosto2 As List(Of Entidades.GastoCentroCosto)


    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
        End If
    End Sub

    Private Sub inicializarFrm()

        Try
            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.ddlTiendaReporte, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboTipoDocumentoExterno(Me.cboTipoDocumento, False)
                .LlenarCboMoneda(Me.cboMoneda, False)
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.cboTipoDocumento.SelectedValue), False)

                '*************** COMBOS CENTRO DE COSTO
                .LlenarCboUnidadNegocio(Me.cboUnidadNegocio, False)
                .LlenarCboDeptoFuncional(Me.cboDeptoFuncional, Me.cboUnidadNegocio.SelectedValue, True)
                .LlenarCboSubArea1(Me.cboSubArea1, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, True)
                .LlenarCboSubArea2(Me.cboSubArea2, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, True)
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
                'Llenar tipo documento
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(cbotipodocumento1, CInt(cboTipoDocumento.SelectedValue), 2, False)
                '---
                .LlenarCboRol(ddl_Rol, True)
            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaVcto.Text = Me.txtFechaEmision.Text

            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFinDocExterno.Text = Me.txtFechaEmision.Text
            Me.txtInicioDocExterno.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

            Me.lblPorcentIgv.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV, 2) * 100)

            actualizarOpcionesBusquedaDocRef(1, False)
            PnlDocumento.Visible = False
            verFrm(FrmModo.Nuevo, True, True, True, True)

            visualizarMoneda()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub
    Private Function getListaCentroCosto2() As List(Of Entidades.GastoCentroCosto)
        Return CType(Session.Item("listaCentroCosto2"), List(Of Entidades.GastoCentroCosto))
    End Function
    Private Sub setListaCentroCosto2(ByVal lista As List(Of Entidades.GastoCentroCosto))
        Session.Remove("listaCentroCosto2")
        Session.Add("listaCentroCosto2", lista)
    End Sub

    Private Function getListaCentroCosto() As List(Of Entidades.GastoCentroCosto)
        Return CType(Session.Item("listaCentroCosto"), List(Of Entidades.GastoCentroCosto))
    End Function
    Private Sub setListaCentroCosto(ByVal lista As List(Of Entidades.GastoCentroCosto))
        Session.Remove("listaCentroCosto")
        Session.Add("listaCentroCosto", lista)
    End Sub

    Private Sub visualizarMoneda()

        If (Me.cboMoneda.SelectedItem IsNot Nothing) Then

            Me.lblMoneda_MontoAfectoIgv.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMoneda_MontoNoAfectoIgv_Monto.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMoneda_SujetoDetraccion.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMoneda_SujetoPercepcion.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMoneda_SujetoRetencion.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaTotalAPagar.Text = Me.cboMoneda.SelectedItem.ToString
        End If

    End Sub
    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove("listaDocumentoRef")
            Session.Remove("listaCentroCosto")

        End If

        If (initSession) Then
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            Me.listaCentroCosto = New List(Of Entidades.GastoCentroCosto)
            setListaCentroCosto(Me.listaCentroCosto)

        End If

        If (initParametrosGral) Then

        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
#Region "****************Buscar Personas Mantenimiento"
    Private Sub Buscar2(ByVal gv2 As GridView, ByVal dni2 As String, ByVal ruc2 As String, _
                     ByVal RazonApe2 As String, ByVal tipo2 As Integer, ByVal idrol2 As Integer, _
                     ByVal estado2 As Integer, ByVal tipoMov2 As Integer)

        Dim index As Integer = 0
        Select Case tipoMov2
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex2.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex2.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO2.Text) - 1)
        End Select

        Dim listaPersona2 As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni2, ruc2, RazonApe2, tipo2, index, gv2.PageSize, idrol2, estado2)
        If listaPersona2.Count > 0 Then
            gv2.DataSource = listaPersona2
            gv2.DataBind()
            tbPageIndex2.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona2")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona2');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

   
    Private Sub btAnterior2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior2.Click
        Try
            Buscar2(gvBuscar2, CStr(ViewState("dni2")), CStr(ViewState("ruc2")), CStr(ViewState("pat2")), CInt(ViewState("tipo2")), CInt(ViewState("rol2")), CInt(ViewState("estado2")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente2.Click
        Try
            Buscar2(gvBuscar2, CStr(ViewState("dni2")), CStr(ViewState("ruc2")), CStr(ViewState("pat2")), CInt(ViewState("tipo2")), CInt(ViewState("rol2")), CInt(ViewState("estado2")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr2.Click
        Try
            Buscar2(gvBuscar2, CStr(ViewState("dni2")), CStr(ViewState("ruc2")), CStr(ViewState("pat2")), CInt(ViewState("tipo2")), CInt(ViewState("rol2")), CInt(ViewState("estado2")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            Me.GV_DocumentoRef.DataSource = Nothing
            Me.GV_DocumentoRef.DataBind()

            'Me.GV_DocumentoRef_Cab.DataSource = Nothing
            'Me.GV_DocumentoRef_Cab.DataBind()

            Me.GV_DocumentoRef_Detalle.DataSource = Nothing
            Me.GV_DocumentoRef_Detalle.DataBind()

            Me.GV_DocumentoRef_DetalleConcepto.DataSource = Nothing
            Me.GV_DocumentoRef_DetalleConcepto.DataBind()

            Me.GV_DocumentoRef_CondicionC.DataSource = Nothing
            Me.GV_DocumentoRef_CondicionC.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub
    Private Sub gvBuscar2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar2.SelectedIndexChanged

        Try

            cargarPersona2(CInt(Me.gvBuscar2.SelectedRow.Cells(1).Text))

            'Me.GV_DocumentoRef.DataSource = Nothing
            'Me.GV_DocumentoRef.DataBind()

            ''Me.GV_DocumentoRef_Cab.DataSource = Nothing
            ''Me.GV_DocumentoRef_Cab.DataBind()

            'Me.GV_DocumentoRef_Detalle.DataSource = Nothing
            'Me.GV_DocumentoRef_Detalle.DataBind()

            'Me.GV_DocumentoRef_DetalleConcepto.DataSource = Nothing
            'Me.GV_DocumentoRef_DetalleConcepto.DataBind()

            'Me.GV_DocumentoRef_CondicionC.DataSource = Nothing
            'Me.GV_DocumentoRef_CondicionC.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub
    Private Sub cargarPersona2(ByVal IdPersona As Integer, Optional ByVal offcapa As Boolean = True)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)


        If (objPersona IsNot Nothing) Then

            Me.txtApellidoBen.Text = objPersona.Descripcion
            Me.txtDniBene.Text = objPersona.Dni
            Me.txtRucBene.Text = objPersona.Ruc
            Me.hddIdPersona2.Value = CStr(objPersona.IdPersona)
        Else
            Throw New Exception("NO SE HALLARON REGISTROS.")
        End If
        If offcapa Then
            objScript.offCapa(Me, "capaPersona2")
        End If

    End Sub
    Private Sub cargarPersona(ByVal IdPersona As Integer, Optional ByVal offcapa As Boolean = True)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)


        If (objPersona IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtDni.Text = objPersona.Dni
            Me.txtRuc.Text = objPersona.Ruc
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

            '***************** CARGAMOS LA LINEA DE CREDITO DEL PROVEEDOR
            Me.GV_CuentaProveedor.DataSource = (New Negocio.CuentaProveedor).SelectxIdProv_IdPropCuentaProveedor(objPersona.IdPersona, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_CuentaProveedor.DataBind()

            '****************** CARGAMOS CUENTA BANCARIA PROVEEDOR
            Me.GV_CuentaBancaria.DataSource = (New Negocio.CuentaBancaria).SelectActivoxIdPersona_DT(objPersona.IdPersona)
            Me.GV_CuentaBancaria.DataBind()

            Me.GV_TipoAgente.DataSource = (New Negocio.PersonaTipoAgente).PersonaTipoAgente_Select(objPersona.IdPersona)
            Me.GV_TipoAgente.DataBind()
        Else

            Throw New Exception("NO SE HALLARON REGISTROS.")

        End If

        If offcapa Then
            objScript.offCapa(Me, "capaPersona")
        End If

    End Sub

#End Region

    Private Sub valOnChange_rdbAutorizado()

        Try

            Select Case CInt(Me.rdbAutorizado.SelectedValue)
                Case 0  '*********** NO APROBAR

                    Me.txtFechaAprobacion.Text = ""
                    Me.chbEnviarCxP.Checked = False
                    Me.chbEnviarCxP.Enabled = False

                Case 1

                    Me.txtFechaAprobacion.Text = CStr((New Negocio.FechaActual).SelectFechaActual)
                    Me.chbEnviarCxP.Enabled = False
                    Me.chbEnviarCxP.Checked = True
            End Select


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumento()
    End Sub

    Private Sub anularDocumento()
        Try

            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

            If ((New Negocio.DocumentoExterno).anularDocumento(IdDocumento)) Then

                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False)

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            '.Serie = CStr(Me.txtSerie.Text)
            '.Codigo = CStr(Me.txtNroDocumento.Text)
            .FechaEmision = CDate(Me.txtFechaEmision.Text)

            Try
                .FechaVenc = CDate(Me.txtFechaVcto.Text)
            Catch ex As Exception
                .FechaVenc = Nothing
            End Try

            .NroDocumento = HttpUtility.HtmlDecode(Me.txtSerie.Text).Trim + " - " + HttpUtility.HtmlDecode(Me.txtNroDocumento.Text).Trim
            .NomMoneda = CStr(Me.cboMoneda.SelectedItem.ToString)
            .NomTipoDocumento = Me.cboTipoDocumento.SelectedItem.ToString
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdPersona = CInt(Me.hddIdPersona.Value)
            .IdTipoDocumento = CInt(Me.cboTipoDocumento.SelectedValue)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdRemitente = CInt(Me.hddIdPersona.Value)
            .IdDestinatario = CInt(Me.cboEmpresa.SelectedValue)

            '*****************   TOTALES
            .SubTotal = CDec(Me.txtSubTotal.Text)
            .IGV = CDec(Me.txtIgv.Text)
            .Total = CDec(Me.txtTotal.Text)
            .TotalAPagar = CDec(Me.txtTotalAPagar.Text)
            .TotalLetras = (New Negocio.ALetras).Letras(CStr(Math.Round(.Total, 2))) + "/100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion



        End With

        Return objDocumento

    End Function

    Private Function obtenerAnexoDocumento() As Entidades.Anexo_Documento

        Dim obj As New Entidades.Anexo_Documento


        With obj

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            Select Case CInt(Me.rdbAutorizado.SelectedValue)
                Case 0  '************ NO APROBADO

                    .Aprobar = False
                    .FechaAprobacion = Nothing

                Case 1  '************* APROBADO

                    .Aprobar = True
                    .FechaAprobacion = CDate(Me.txtFechaAprobacion.Text)

                Case Else
                    Throw New Exception("SE HA SELECCIONADO UNA OPCIÓN DE APROBACIÓN NO EXISTENTE.")
            End Select

            .MontoAfectoIgv = CDec(Me.txtMontoAfectoIgv.Text)
            .MontoISC = CDec(Me.txtIsc.Text)

            .MontoNoAfectoIgv = CDec(Me.txtMontoNoAfectoIgv.Text)
            .MontoOtroTributo = CDec(Me.txtOtrosTributos.Text)


            .MontoxSustentar = CDec(Me.txtRetencionHonorarios.Text) '' ******** BORRAR Y ACTIVAR EL REGISTRO DE ABAJO
            '.RetencionHonorarios = CDec(Me.txtRetencionHonorarios.Text)

            .Serie = CStr(Me.txtSerie.Text)
            .Codigo = CStr(Me.txtNroDocumento.Text)
        End With

        Return obj

    End Function

    Private Function obtenerMovCuentaxPagar() As Entidades.MovCuentaPorPagar

        Dim objMovCuentaCxP As Entidades.MovCuentaPorPagar = Nothing

        If (Me.chbEnviarCxP.Checked) Then
            objMovCuentaCxP = New Entidades.MovCuentaPorPagar

            With objMovCuentaCxP

                .Monto = CDec(Me.txtTotal.Text)

                If (Me.GV_Documento_Cab_CXP.Rows.Count = 1) Then

                    .Saldo = CDec(CType(Me.GV_Documento_Cab_CXP.Rows(0).FindControl("txtSaldo"), TextBox).Text)

                Else

                    Throw New Exception("EL NRO. DE FILAS DE LA GRILLA DOCUMENTO CAB NO POSEE EL NRO DE FILAS CORRECTO. NO SE PERMITE LA OPERACIÓN.")

                End If

                .Factor = 1
                .IdProveedor = CInt(Me.hddIdPersona.Value)
                .IdCuentaProv = obtenerIdCuentaProveedor()


                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo

                        .IdDocumento = Nothing

                    Case FrmModo.Editar

                        .IdDocumento = CInt(Me.hddIdDocumento.Value)

                End Select

                .IdMovCuentaTipo = 1

            End With

        End If
        Return objMovCuentaCxP

    End Function

    Private Function obtenerIdCuentaProveedor() As Integer

        Dim IdCuentaProveedor As Integer = Nothing

        For i As Integer = 0 To Me.GV_CuentaProveedor.Rows.Count - 1

            If (CInt(CType(Me.GV_CuentaProveedor.Rows(i).FindControl("hddIdMoneda"), HiddenField).Value) = CInt(Me.cboMoneda.SelectedValue) And CInt(CType(Me.GV_CuentaProveedor.Rows(i).FindControl("hddIdPersona"), HiddenField).Value) = CInt(Me.hddIdPersona.Value) And CInt(CType(Me.GV_CuentaProveedor.Rows(i).FindControl("hddIdEmpresa"), HiddenField).Value) = CInt(Me.cboEmpresa.SelectedValue)) Then

                IdCuentaProveedor = CInt(CType(Me.GV_CuentaProveedor.Rows(i).FindControl("hddIdCuentaProveedor"), HiddenField).Value)
                Return IdCuentaProveedor

            End If

        Next

        Return IdCuentaProveedor

    End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservaciones As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservaciones = New Entidades.Observacion

            With objObservaciones

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .Observacion = Me.txtObservaciones.Text.Trim

            End With
        End If

        Return objObservaciones


    End Function
    Protected Sub rdbAutorizado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbAutorizado.SelectedIndexChanged
        valOnChange_rdbAutorizado()
    End Sub

    Private Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoDocumento.SelectedIndexChanged
        valOnChange_CboTipoDocumento()
    End Sub
    Private Sub valOnChange_CboTipoDocumento()

        Try

            Dim objCbo As New Combo

            With objCbo
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(cbotipodocumento1, CInt(cboTipoDocumento.SelectedValue), 2, False)

                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.cboTipoDocumento.SelectedValue), False)
            End With

            Me.GV_DocumentoRef.DataSource = Nothing
            Me.GV_DocumentoRef.DataBind()

            'Me.GV_DocumentoRef_Cab.DataSource = Nothing
            'Me.GV_DocumentoRef_Cab.DataBind()

            Me.GV_DocumentoRef_Detalle.DataSource = Nothing
            Me.GV_DocumentoRef_Detalle.DataBind()

            Me.GV_DocumentoRef_DetalleConcepto.DataSource = Nothing
            Me.GV_DocumentoRef_DetalleConcepto.DataBind()

            Me.GV_DocumentoRef_CondicionC.DataSource = Nothing
            Me.GV_DocumentoRef_CondicionC.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub



#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona2.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona2.Value)) Then
                If (CInt(Me.hddIdPersona2.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona2.Value)
                End If
            End If


            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            '*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            '****  Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocGuiaRecepcion).DocumentoGuiaRecepcion_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), 0, CInt(Me.cboAlmacen.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, fechaInicio, fechafin)
            'Modificar aqui para FCG  que serie y codigo llega a como int y deberia llegar como varchar*****'
            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento)._Documento_BuscarDocumentoRefDocExterno(0, 0, IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, CInt(Me.cbotipodocumento1.SelectedValue), fechaInicio, fechafin, False)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub
    'Protected Sub ddl_TipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'If CType(GV_DocumentosReferencia_Find.Rows(GV_DocumentosReferencia_Find.SelectedIndex).Cells(8).FindControl("ddl_TipoDocumentoRef"), DropDownList).SelectedIndex = "0" Then
    '    '    For i As Integer = 3 To 33
    '    '        GridView1.Rows(GridView1.SelectedIndex).Cells(i).Text = 0
    '    '    Next
    '    'End If
    '    'If CType(GridView1.Rows(GridView1.SelectedIndex).Cells(0).FindControl("DropDownList1"), DropDownList).SelectedValue = 1 Then
    '    '    For i As Integer = 3 To 33
    '    '        GridView1.Rows(GridView1.SelectedIndex).Cells(i).Text = 1
    '    '    Next
    '    'End If

    '    Dim ddl_TipoDocumentoRef As DropDownList = CType(sender, DropDownList)
    '    Dim GV_DocumentosReferencia_Find As GridViewRow = CType(CType(sender, Control).NamingContainer, GridViewRow)
    '    Dim ddl_IdDocumentoRef As DropDownList = Nothing
    '    If GV_DocumentosReferencia_Find IsNot Nothing Then
    '        ddl_IdDocumentoRef = CType(GV_DocumentosReferencia_Find.FindControl("ddl_IdDocumentoRef"), DropDownList)
    '        ddl_TipoDocumentoRef.SelectedIndex = ddl_IdDocumentoRef.SelectedIndex
    '        ddl_IdDocumentoRef.DataBind()
    '        objScript.onCapa(Me, "capaDocumentosReferencia")
    '    End If

    '    objScript.onCapa(Me, "capaDocumentosReferencia")

    'End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        mostrarDocumentoRelaciones(Me.GV_DocumentosReferencia_Find.SelectedIndex)

        cargarDocumentoReferencia(CStr(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub mostrarDocumentoRelaciones(ByVal index As Integer)

        Try

            Dim IdDocumentoReferencia As String = CStr(CType(Me.GV_DocumentosReferencia_Find.Rows(index).FindControl("lblIdDocumentoRef"), Label).Text)


            'If IdDocumentoReferencia <> "" Then
            '    Dim lista As New List(Of Entidades.Documento)
            '    Dim IdDocumentoRef() As String
            '    IdDocumentoRef = IdDocumentoReferencia.Split(CChar(","))
            '    For x As Integer = 0 To IdDocumentoRef.Length - 1
            '        If IdDocumentoRef(x) <> "" Then
            '            lista(x).IdDocRelacionado = CInt(IdDocumentoRef(x))
            '        End If
            '    Next
            '    listaDocId = lista
            'End If
            If (IdDocumentoReferencia <> "") Then
                IdDocumentoReferencia = IdDocumentoReferencia.Remove(IdDocumentoReferencia.Length - 1, 1)
                Dim objDocumento As List(Of Entidades.Documento) = (New Negocio.Documento).SelectxIdDocumentoExternoLista(IdDocumentoReferencia)

                If (objDocumento IsNot Nothing) Then
                    Me.listaDocumentoRef = getlistaDocumentoRef()
                    Me.listaDocumentoRef.AddRange(objDocumento)
                    setlistaDocumentoRef(Me.listaDocumentoRef)
                    'Else
                    '    Me.listaDocumentoRef = getlistaDocumentoRef()
                End If

            Else

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function
    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As String)

        Try

            validar_AddDocumentoRef(CInt(IdDocumentoRef))

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumentoExternotwo(IdDocumentoRef)

            If (objDocumento IsNot Nothing) Then

                Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)

                Me.listaDocumentoRef = getlistaDocumentoRef()
                Me.listaDocumentoRef.Add(objDocumento)
                setlistaDocumentoRef(Me.listaDocumentoRef)

                listaCentroCosto = (New Negocio.GastoCentroCosto).SelectxIdDocumento(objDocumento.Id)

                cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objPersona, listaCentroCosto)

                Me.GV_DocumentosReferencia_Find.DataSource = Nothing
                Me.GV_DocumentosReferencia_Find.DataBind()

            Else
                Throw New Exception("NO SE HALLARON REGISTROS.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoRef_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento As List(Of Entidades.Documento), ByVal objPersona As Entidades.PersonaView, ByVal listaCentroCostoDocRef As List(Of Entidades.GastoCentroCosto))

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            End If

            If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
                valOnChange_cboMoneda()
            End If

        End With

        actualizarListaCentroCosto()
        Me.listaCentroCosto = getListaCentroCosto()
        For Each obj As Entidades.GastoCentroCosto In listaCentroCostoDocRef
            listaCentroCosto.Add(obj)
        Next
        Me.GV_CentroCosto.DataSource = Me.listaCentroCosto
        Me.GV_CentroCosto.DataBind()
        setListaCentroCosto(Me.listaCentroCosto)

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '******************* REMITENTE
        'If (objPersona IsNot Nothing) Then

        '    cargarPersona(objPersona.IdPersona)

        '    'Me.txtDescripcionPersona.Text = objPersona.Descripcion
        '    'Me.txtDni.Text = objPersona.Dni
        '    'Me.txtRuc.Text = objPersona.Ruc
        '    'Me.hddIdPersona.Value = CStr(objPersona.IdPersona)
        '    'Me.GV_CuentaProveedor.DataSource = (New Negocio.CuentaProveedor).SelectxIdProv_IdPropCuentaProveedor(objPersona.IdPersona, CInt(Me.cboEmpresa.SelectedValue))
        '    'Me.GV_CuentaProveedor.DataBind()

        'End If

    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            'Me.GV_DocumentoRef_Cab.DataSource = Nothing
            'Me.GV_DocumentoRef_Cab.DataBind()

            Me.GV_DocumentoRef_Detalle.DataSource = Nothing
            Me.GV_DocumentoRef_Detalle.DataBind()

            Me.GV_DocumentoRef_DetalleConcepto.DataSource = Nothing
            Me.GV_DocumentoRef_DetalleConcepto.DataBind()

            Me.GV_DocumentoRef_CondicionC.DataSource = Nothing
            Me.GV_DocumentoRef_CondicionC.DataBind()

            Dim ListaGastoCentroCosto As List(Of Entidades.GastoCentroCosto) = Nothing
            listaCentroCosto = New List(Of Entidades.GastoCentroCosto)
            For Each row As GridViewRow In Me.GV_DocumentoRef.Rows
                ListaGastoCentroCosto = (New Negocio.GastoCentroCosto).SelectxIdDocumento(CInt(CType(row.FindControl("hddIdDocumento"), HiddenField).Value))
                listaCentroCosto.AddRange(ListaGastoCentroCosto)
            Next
            Me.GV_CentroCosto.DataSource = Me.listaCentroCosto
            Me.GV_CentroCosto.DataBind()
            setListaCentroCosto(Me.listaCentroCosto)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region

    Protected Sub btnMostrarDetalleDocumentoRef(ByVal sender As Object, ByVal e As System.EventArgs)

        visualizarDetalleDocumentoRef(CInt(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex))

    End Sub

    Private Sub visualizarDetalleDocumentoRef(ByVal Index As Integer)
        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Dim IdDocumentoRef As Integer = Me.listaDocumentoRef(Index).Id

            'Me.GV_DocumentoRef_Cab.DataSource = obtenerDocumentoRef_Cab(IdDocumentoRef)
            'Me.GV_DocumentoRef_Cab.DataBind()

            Me.GV_DocumentoRef_Detalle.DataSource = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumentoRef)
            Me.GV_DocumentoRef_Detalle.DataBind()

            Me.GV_DocumentoRef_DetalleConcepto.DataSource = (New Negocio.DetalleConcepto).SelectxIdDocumento(IdDocumentoRef)
            Me.GV_DocumentoRef_DetalleConcepto.DataBind()

            Me.GV_DocumentoRef_CondicionC.DataSource = (New Negocio.Documento_CondicionC).SelectxIdDocumento(IdDocumentoRef)
            Me.GV_DocumentoRef_CondicionC.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDocumentoRef_Cab(ByVal IdDocumentoRef As Integer) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)

        Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
        lista.Add(objDocumento)

        Return lista

    End Function

    Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectedIndexChanged
        valOnChange_cboMoneda()
    End Sub
    Private Sub valOnChange_cboMoneda()
        Try
            visualizarMoneda()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnLimpiarDetalle_DocumentoRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiarDetalle_DocumentoRef.Click

        limpiarDetalle_DocumentoRef()

    End Sub

    Private Sub limpiarDetalle_DocumentoRef()

        Try

            'Me.GV_DocumentoRef_Cab.DataSource = Nothing
            'Me.GV_DocumentoRef_Cab.DataBind()

            Me.GV_DocumentoRef_Detalle.DataSource = Nothing
            Me.GV_DocumentoRef_Detalle.DataBind()

            Me.GV_DocumentoRef_DetalleConcepto.DataSource = Nothing
            Me.GV_DocumentoRef_DetalleConcepto.DataBind()

            Me.GV_DocumentoRef_CondicionC.DataSource = Nothing
            Me.GV_DocumentoRef_CondicionC.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Protected Sub chb_SujetoPercepcion_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chb_SujetoPercepcion.CheckedChanged
        valOnChange_chb_SujetoPercepcion()
    End Sub
    Private Sub valOnChange_chb_SujetoPercepcion()
        Try
            Dim ExiteSujetoPercepcion As Boolean = False
            For i As Integer = 0 To Me.GV_TipoAgente.Rows.Count - 1
                Dim SujetoAPercepcion As Boolean = CBool(CType(Me.GV_TipoAgente.Rows(i).FindControl("hddSujetoAPercepcion"), HiddenField).Value)
                If SujetoAPercepcion Then
                    ExiteSujetoPercepcion = True
                End If
            Next

            If ExiteSujetoPercepcion = False Then
                Me.chb_SujetoPercepcion.Checked = False
                Throw New Exception("El PROVEEDOR NO ES AGENTE DE PERCEPCIÓN. NO SE PERMITE LA OPERACIÓN.")
                Return
            End If

            If (Me.chb_SujetoPercepcion.Checked) Then
                Me.txtPercepcion_Monto.Enabled = True
            Else
                Me.txtPercepcion_Monto.Enabled = False
            End If

            Me.txtPercepcion_Monto.Text = "0"
            Me.txtPercepcion.Text = "0"

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "    calcularTotales();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub chb_SujetoRetención_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chb_SujetoRetención.CheckedChanged
        valOnChange_chb_SujetoRetención()
    End Sub
    Private Sub valOnChange_chb_SujetoRetención()
        Try


            Dim ExiteSujetoRetención As Boolean = False
            For i As Integer = 0 To Me.GV_TipoAgente.Rows.Count - 1

                Dim SujetoRetención As Boolean = CBool(CType(Me.GV_TipoAgente.Rows(i).FindControl("hddSujetoARetencion"), HiddenField).Value)

                If SujetoRetención Then
                    ExiteSujetoRetención = True
                End If

            Next

            If ExiteSujetoRetención = False Then
                Me.chb_SujetoRetención.Checked = False
                Throw New Exception("El PROVEEDOR NO ES AGENTE DE RETENCIÓN. NO SE PERMITE LA OPERACIÓN.")
                Return
            End If

            If (Me.chb_SujetoRetención.Checked) Then
                Me.txtRetencion_Monto.Enabled = True
            Else
                Me.txtRetencion_Monto.Enabled = False
            End If

            Me.txtRetencion_Monto.Text = "0"

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "    calcularTotales();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub chb_SujetoDetraccion_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chb_SujetoDetraccion.CheckedChanged
        valOnChange_chb_chb_SujetoDetraccion()
    End Sub
    Private Sub valOnChange_chb_chb_SujetoDetraccion()
        Try
            If (Me.chb_SujetoDetraccion.Checked) Then
                Dim ExiteDetraccion As Boolean = False
                For i As Integer = 0 To Me.GV_TipoAgente.Rows.Count - 1
                    Dim IdAgente As Integer = CInt(CType(Me.GV_TipoAgente.Rows(i).FindControl("hddIdAgente"), HiddenField).Value)

                    If IdAgente = 5 Or IdAgente = 6 Or IdAgente = 7 Or IdAgente = 300020001 Or IdAgente = 1700000001 Then
                        ExiteDetraccion = True
                    End If

                Next
                If ExiteDetraccion = False Then
                    chb_SujetoDetraccion.Checked = False
                    Throw New Exception("El PROVEEDOR NO ES AGENTE DE DETRACCIÓN. NO SE PERMITE LA OPERACIÓN.")
                    Return
                End If

                Me.txtDetraccion_Monto.Enabled = True
                Me.GV_ConceptoDetraccion.DataSource = (New Negocio.Concepto_TipoDocumento).SelectConceptoDetraccionxIdTipoDocumento(CInt(Me.cboTipoDocumento.SelectedValue))
                Me.GV_ConceptoDetraccion.DataBind()
            Else
                Me.txtDetraccion_Monto.Enabled = False
                Me.GV_ConceptoDetraccion.DataSource = Nothing
                Me.GV_ConceptoDetraccion.DataBind()
            End If

            Me.txtDetraccion_Monto.Text = "0"

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "    calcularTotales();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub chb_ConceptoDetraccion_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        valOnChange_chb_ConceptoDetraccion(CType(CType(sender, CheckBox).NamingContainer, GridViewRow).RowIndex)

    End Sub
    Private Sub valOnChange_chb_ConceptoDetraccion(ByVal index As Integer)

        Try

            Dim detraccionTotal As Decimal = 0

            If (CType(Me.GV_ConceptoDetraccion.Rows(index).FindControl("chb_ConceptoDetraccion"), CheckBox).Checked) Then
                CType(Me.GV_ConceptoDetraccion.Rows(index).FindControl("txtMonto"), TextBox).Enabled = True
            Else
                CType(Me.GV_ConceptoDetraccion.Rows(index).FindControl("txtMonto"), TextBox).Enabled = False
            End If

            CType(Me.GV_ConceptoDetraccion.Rows(index).FindControl("txtMonto"), TextBox).Text = "0"

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularConceptoDetraccion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        Dim validando As Entidades.Documento = New Negocio.Documento().ValidarDocExterno(CStr(txtSerie.Text), CStr(txtNroDocumento.Text), CInt(cboTipoDocumento.SelectedValue), CInt(Me.hddIdPersona.Value))

        If (validando.Contador = 0) Then

            Dim MontoRef As Decimal = 0
            For i As Integer = 0 To Me.GV_DocumentoRef.Rows.Count - 1
                MontoRef = CDec(CType(Me.GV_DocumentoRef.Rows(i).FindControl("lblMontoCab"), Label).Text)
                Exit For
            Next
            If (CDec(txtTotalCentroCostro.Text) <= MontoRef) Then
                valOnClick_btnGuardar()
            Else
                objScript.mostrarMsjAlerta(Me, "El monto ingresado no puede ser mayor al Monto total del Doc.RQ.")
            End If
        Else
            objScript.mostrarMsjAlerta(Me, "El documento a registrar ya existe. No procede la operación.")
        End If




    End Sub
    Private Sub valOnClick_btnGuardar()

        If (Me.chbEnviarCxP.Checked) Then

            mostrarCapa_CxP()
        Else
            registrarDocumento()
        End If

    End Sub

    Private Sub mostrarCapa_CxP()
        Try

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            objDocumento.Saldo = objDocumento.TotalAPagar

            Dim valorAprob As Integer = 0
            '******************** CABECERA DEL DOCUMENTO
            Dim listaDocumentoCab_CXP As New List(Of Entidades.Documento)
            listaDocumentoCab_CXP.Add(objDocumento)

            Dim listaCancelacionBanco As List(Of Entidades.MovCuentaPorPagar) = New List(Of Entidades.MovCuentaPorPagar)
            '******************** CABECERA DOCUMENTO REF
            Dim listaDocumentoRef_CXP As List(Of Entidades.Documento_MovCuentaPorPagar) = obtenerListaDocumentoRef_CXP()

            Dim CadenaIdDocumento As String = ""

            If listaDocumentoRef_CXP.Count > 0 Then

                Dim Saldo As Decimal = objDocumento.Saldo
                valorAprob = 1

                For i As Integer = 0 To listaDocumentoRef_CXP.Count - 1
                    Dim SaldoNew As Decimal = listaDocumentoRef_CXP(i).SaldoNew

                    If Saldo > SaldoNew Then
                        listaDocumentoRef_CXP(i).SaldoNew = 0
                    Else
                        If Saldo > 0 Then
                            listaDocumentoRef_CXP(i).SaldoNew = SaldoNew + (Saldo * -1)
                        End If
                    End If
                    Saldo = Saldo + (SaldoNew * -1)
                    CadenaIdDocumento += CStr(listaDocumentoRef_CXP(i).Id) + ","
                Next
            Else
                valorAprob = -1
            End If

            If (CadenaIdDocumento <> "" Or CadenaIdDocumento <> Nothing) Then
                ''obtenemos los iddocumento de los documentos referenciados y verificamos si se encuentran en la tabla MovCuentasPorPagar.
                CadenaIdDocumento = Mid(CadenaIdDocumento, 1, Len(CadenaIdDocumento) - 1)
                listaCancelacionBanco = (New Negocio.MovCuentaPorPagar).SelectDatosCancelacion(CadenaIdDocumento)
                ''Si encuentran registros en la búsqueda entonces cambiamos todos los saldos nuevo a 0
                If (listaCancelacionBanco.Count > 0) Then
                    For i As Integer = 0 To listaDocumentoRef_CXP.Count - 1
                        listaDocumentoRef_CXP(i).SaldoNew = 0
                    Next
                End If
            Else
                CadenaIdDocumento = "0"
            End If


            ''Se agrego para volver todos los saldos a 0 si los montos son iguales a los doc. de referencia.
            Dim irx As Integer = 0
            For i As Integer = 0 To listaDocumentoRef_CXP.Count - 1
                If (CDec(listaDocumentoRef_CXP(i).TotalAPagar) = CDec(listaDocumentoCab_CXP(irx).Total)) Then
                    listaDocumentoRef_CXP(i).SaldoNew = 0
                End If
            Next


            '' Buscamos si el RQ tiene Recibos de Egreso Relacionado antes de emitir el Doc. externo
            Dim ListaEgreso As List(Of Entidades.Documento) = New Negocio.Documento().SelectReciboEgresoxIdRQ(CadenaIdDocumento)
            Dim ListaCancelacionBancos As List(Of Entidades.Documento) = New Negocio.Documento().SelectDocCancelacionBxIdRQ(CadenaIdDocumento)


            ''Si es que existe el Recibo de Egreso Entonces modificamos el saldo del nuevo Doc.Externo
            If (ListaEgreso IsNot Nothing And ListaCancelacionBancos Is Nothing) Then
                For i As Integer = 0 To ListaEgreso.Count - 1
                    For ix As Integer = 0 To listaDocumentoCab_CXP.Count - 1
                        listaDocumentoCab_CXP(ix).Saldo = ListaEgreso(i).Total - listaDocumentoCab_CXP(i).Saldo
                    Next
                    Exit For
                Next
            ElseIf (ListaEgreso Is Nothing And ListaCancelacionBancos IsNot Nothing) Then
                For i As Integer = 0 To ListaCancelacionBancos.Count - 1
                    For ix As Integer = 0 To listaDocumentoCab_CXP.Count - 1
                        listaDocumentoCab_CXP(ix).Saldo = listaDocumentoCab_CXP(i).Saldo - ListaCancelacionBancos(i).Total
                    Next
                    Exit For
                Next
            End If

            '******************** MOSTRAMOS EN LAS GRILLAS
            Me.GV_Documento_Cab_CXP.DataSource = listaDocumentoCab_CXP
            Me.GV_Documento_Cab_CXP.DataBind()

            Me.GV_DocumentoRef_CXP.DataSource = listaDocumentoRef_CXP
            Me.GV_DocumentoRef_CXP.DataBind()

            Me.GV_MovBanco.DataSource = listaCancelacionBanco
            Me.GV_MovBanco.DataBind()

            Me.GV_ReciboEgreso.DataSource = ListaEgreso
            Me.GV_ReciboEgreso.DataBind()

            Me.GV_CancelacionBancos.DataSource = ListaCancelacionBancos
            Me.GV_CancelacionBancos.DataBind()


            For Each gvRow As GridViewRow In GV_DocumentoRef_CXP.Rows
                Dim chkItem As CheckBox = DirectCast(gvRow.FindControl("chksustento"), CheckBox)
                Dim hddSustento As HiddenField = DirectCast(gvRow.FindControl("hddSustento"), HiddenField)

                'If (hddSustento.Value = "0") Then
                '    chkItem.Checked = False
                'Else
                'chkItem.Visible=false
                chkItem.Checked = True
                chkItem.Enabled = False
                'End If
            Next


            If (valorAprob = -1) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaCxP_Saldos'); alert('No se encontro ningún documento referenciado. Verifique si el documento de ref. esta aprobado.'); ", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaCxP_Saldos');  ", True)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerListaDocumentoRef_CXP() As List(Of Entidades.Documento_MovCuentaPorPagar)

        Dim lista As New List(Of Entidades.Documento_MovCuentaPorPagar)

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim listaMovCuentaCXP As List(Of Entidades.MovCuentaPorPagar) = (New Negocio.MovCuentaPorPagar).SelectxIdDocumento(Me.listaDocumentoRef(i).Id)

            If (listaMovCuentaCXP.Count = 1) Then

                Dim obj As New Entidades.Documento_MovCuentaPorPagar

                With obj


                    .Id = Me.listaDocumentoRef(i).Id
                    .NomTipoDocumento = Me.listaDocumentoRef(i).NomTipoDocumento
                    .NroDocumento = Me.listaDocumentoRef(i).NroDocumento
                    .TotalAPagar = Me.listaDocumentoRef(i).TotalAPagar
                    .NomMoneda = Me.listaDocumentoRef(i).NomMoneda
                    .Saldo = listaMovCuentaCXP(0).Saldo
                    .SaldoNew = listaMovCuentaCXP(0).Saldo
                    .IdMovCtaPP = listaMovCuentaCXP(0).Id
                    .Sustento = listaMovCuentaCXP(0).FlagSustento
                    '.IdDocRelacionado = Me.listaDocumentoRef(i). 
                End With

                lista.Add(obj)

            End If

        Next

        Return lista

    End Function

    Private Function obtenerListaDetalleConcepto() As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        For i As Integer = 0 To Me.GV_ConceptoDetraccion.Rows.Count - 1

            If (CType(Me.GV_ConceptoDetraccion.Rows(i).FindControl("chb_ConceptoDetraccion"), CheckBox).Checked) Then

                Dim obj As New Entidades.DetalleConcepto

                With obj

                    .IdConcepto = CInt(CType(Me.GV_ConceptoDetraccion.Rows(i).FindControl("hddIdConcepto"), HiddenField).Value)
                    .Monto = CDec(CType(Me.GV_ConceptoDetraccion.Rows(i).FindControl("txtMonto"), TextBox).Text)
                    .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                    .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)

                End With

                lista.Add(obj)

            End If

        Next

        Return lista

    End Function
    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)

        For i As Integer = 0 To Me.GV_DocumentoRef.Rows.Count - 1


            Dim obj As New Entidades.RelacionDocumento

            With obj

                .IdDocumento1 = CInt(CType(Me.GV_DocumentoRef.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

            End With
            lista.Add(obj)

        Next

        Return lista

    End Function
    Private Function obtenerListaMontoRegimen() As List(Of Entidades.MontoRegimen)

        Dim listaMontoRegimen As New List(Of Entidades.MontoRegimen)

        '******************* PERCEPCIÓN
        If (Me.chb_SujetoPercepcion.Checked) Then
            Dim objMontoRegimen_Percepcion As New Entidades.MontoRegimen
            With objMontoRegimen_Percepcion

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .ro_Id = 1 '*********************************   PERCEPCION
                .Monto = CDec(Me.txtPercepcion_Monto.Text)

            End With
            listaMontoRegimen.Add(objMontoRegimen_Percepcion)
        End If


        '******************* DETRACCION
        If (Me.chb_SujetoDetraccion.Checked) Then
            Dim objMontoRegimen_Detraccion As New Entidades.MontoRegimen
            With objMontoRegimen_Detraccion
                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .ro_Id = 2 '*********************************   DETRACCIÓN
                .Monto = CDec(Me.txtDetraccion_Monto.Text)

            End With
            listaMontoRegimen.Add(objMontoRegimen_Detraccion)
        End If

        '******************* RETENCIÓN
        If (Me.chb_SujetoRetención.Checked) Then
            Dim objMontoRegimen_Retencion As New Entidades.MontoRegimen
            With objMontoRegimen_Retencion

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .ro_Id = 3 '*********************************   RETENCION
                .Monto = CDec(Me.txtRetencion_Monto.Text)

            End With
            listaMontoRegimen.Add(objMontoRegimen_Retencion)
        End If

        Return listaMontoRegimen

    End Function
    Private Function obtenerListaDocumentoRef_CXP_Save() As List(Of Entidades.MovCuentaPorPagar)

        Dim lista As New List(Of Entidades.MovCuentaPorPagar)

        For i As Integer = 0 To Me.GV_DocumentoRef_CXP.Rows.Count - 1

            Dim obj As New Entidades.MovCuentaPorPagar
            With obj
                .Id = CInt(CType(Me.GV_DocumentoRef_CXP.Rows(i).FindControl("hddIdMovCuentaCXP"), HiddenField).Value)
                .Saldo = CDec(CType(Me.GV_DocumentoRef_CXP.Rows(i).FindControl("txtSaldo_New"), TextBox).Text)
                .IdDocumento = CInt(CType(Me.GV_DocumentoRef_CXP.Rows(i).FindControl("hddIdDocumentoRef"), HiddenField).Value)
                'If (CType(Me.GV_DocumentoRef_CXP.Rows(i).FindControl("chksustento"), CheckBox).Checked) Then
                .FlagSustento = 1
                'Else
                '.FlagSustento = 0
                'End If
            End With
            lista.Add(obj)
        Next
        Return lista
    End Function
    Private Sub registrarDocumento()

        Try
            Dim validacion As Integer = 0
            '**************   validarFrm()
            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim objAnexoDocumento As Entidades.Anexo_Documento = obtenerAnexoDocumento()
            Dim objMovCuentaCxP As Entidades.MovCuentaPorPagar = obtenerMovCuentaxPagar()
            '*******************   OBTENEMOS LA LISTA DE LOS CONCEPTOS DETRACCIÓN
            Dim listaDetalleConcepto As List(Of Entidades.DetalleConcepto) = obtenerListaDetalleConcepto()
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()
            Dim listaMontoRegimen As List(Of Entidades.MontoRegimen) = obtenerListaMontoRegimen()

            '******************* OBTENEMOS LA LISTA DE DOCUMENTOS REF CON SUS NUEVOS SALDOS
            Dim listaDocumentoRef_MovCuentaCXP As List(Of Entidades.MovCuentaPorPagar) = obtenerListaDocumentoRef_CXP_Save()

            '******************* OBTENEMOS LA LISTA DE CENTRO DE COSTO
            actualizarListaCentroCosto()
            Me.listaCentroCosto = getListaCentroCosto()


            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    If (listaDocumentoRef_MovCuentaCXP.Count > 0) Then
                        objDocumento.Id = (New Negocio.DocumentoExterno).registrarDocumento(objDocumento, objAnexoDocumento, listaDetalleConcepto, objMovCuentaCxP, objObservaciones, listaRelacionDocumento, listaMontoRegimen, listaDocumentoRef_MovCuentaCXP, listaCentroCosto)
                        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                        objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")
                        validacion = 1
                    Else
                        validacion = -1
                    End If

                Case FrmModo.Editar
                    If (listaDocumentoRef_MovCuentaCXP.Count > 0) Then
                        objDocumento.Id = (New Negocio.DocumentoExterno).actualizarDocumento(objDocumento, objAnexoDocumento, listaDetalleConcepto, objMovCuentaCxP, objObservaciones, listaRelacionDocumento, listaMontoRegimen, listaDocumentoRef_MovCuentaCXP, listaCentroCosto)
                        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                        objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")
                        validacion = 1
                    Else
                        validacion = -1
                    End If
            End Select

            If (validacion = 1) Then
                verFrm(FrmModo.Documento_Save_Exito, False, False, False, False)
                Session.Add("IdDocumentoExt", Me.hddIdDocumento.Value)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaCxP_Saldos'); alert('El documento de referencia no se encuentra aprobado o no se encuentra en Mov.Cuentas por Pagar. No prodece la operación.'); ", True)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        valOnChange_cboEmpresa()

    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try

            Dim objCbo As New Combo
            With objCbo
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtSerie.Text = ""
        Me.txtNroDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaVcto.Text = Me.txtFechaEmision.Text
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto

        '************ PROVEEDOR
        Me.txtDescripcionPersona.Text = ""
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""

        Me.txtApellidoBen.Text = ""
        Me.txtDniBene.Text = ""
        Me.txtRucBene.Text = ""

        '**************** APROBACION
        Me.rdbAutorizado.SelectedValue = "0"  '*********** NO APROBADO
        Me.txtFechaAprobacion.Text = ""
        Me.chbEnviarCxP.Checked = False
        Me.chbEnviarCxP.Enabled = False

        '*************** CUENTA PROVEEDOR
        Me.GV_CuentaProveedor.DataSource = Nothing
        Me.GV_CuentaProveedor.DataBind()

        '*************** TIPO AGENTE
        Me.GV_TipoAgente.DataSource = Nothing
        Me.GV_TipoAgente.DataBind()

        '*********** TOTALES
        Me.txtMontoAfectoIgv_Monto.Text = "0"
        Me.txtMontoNoAfectoIgv_Monto.Text = "0"
        Me.txtMontoAfectoIgv.Text = "0"
        Me.txtMontoNoAfectoIgv.Text = "0"
        Me.txtIgv.Text = "0"
        Me.txtSubTotal.Text = "0"
        Me.txtTotal.Text = "0"
        Me.txtTotalAPagar.Text = "0"
        Me.txtPercepcion_Monto.Text = "0"
        Me.txtPercepcion.Text = "0"
        Me.txtRetencion_Monto.Text = "0"
        Me.txtDetraccion_Monto.Text = "0"
        Me.txtRetencionHonorarios.Text = "0"
        Me.txtRetencionHonorariosPercent.Text = "0"
        Me.txtTotalCentroCostro.Text = "0"

        Me.chb_SujetoDetraccion.Checked = False
        Me.txtDetraccion_Monto.Enabled = False
        Me.chb_SujetoRetención.Checked = False
        Me.txtRetencion_Monto.Enabled = False
        Me.chb_SujetoPercepcion.Checked = False
        Me.txtPercepcion_Monto.Enabled = False

        Me.GV_ConceptoDetraccion.DataSource = Nothing
        Me.GV_ConceptoDetraccion.DataBind()

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""

        '**************** DETALLE CONCEPTO
        Me.GV_DetalleConcepto.DataSource = Nothing
        Me.GV_DetalleConcepto.DataBind()


        '**************** LISTA CENTRO COSTO
        Me.GV_CentroCosto.DataSource = Nothing
        Me.GV_CentroCosto.DataBind()

        '************* HIDDEN
        Me.hddIdDocumento.Value = ""
        Me.hddIndex.Value = ""
        Me.hddIdPersona.Value = ""

        '*************** DOCUMENTO REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        'Me.GV_DocumentoRef_Cab.DataSource = Nothing
        'Me.GV_DocumentoRef_Cab.DataBind()

        Me.GV_DocumentoRef_Detalle.DataSource = Nothing
        Me.GV_DocumentoRef_Detalle.DataBind()

        Me.GV_DocumentoRef_DetalleConcepto.DataSource = Nothing
        Me.GV_DocumentoRef_DetalleConcepto.DataBind()

        Me.GV_DocumentoRef_CondicionC.DataSource = Nothing
        Me.GV_DocumentoRef_CondicionC.DataBind()

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
        ''Documentos de Referencia en Capa
        Me.GV_Documento_Cab_CXP.DataSource = Nothing
        Me.GV_Documento_Cab_CXP.DataBind()

        Me.GV_DocumentoRef_CXP.DataSource = Nothing
        Me.GV_DocumentoRef_CXP.DataBind()

        Me.GV_MovBanco.DataSource = Nothing
        Me.GV_MovBanco.DataBind()

        Me.GV_ReciboEgreso.DataSource = Nothing
        Me.GV_ReciboEgreso.DataBind()

        Me.GV_CancelacionBancos.DataSource = Nothing
        Me.GV_CancelacionBancos.DataBind()

        '************* OTROS        
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()

        Me.gvBuscar.DataSource = Nothing
        Me.gvBuscar.DataBind()

        Me.GV_NuevoCentroCosto.DataSource = Nothing
        Me.GV_NuevoCentroCosto.DataBind()

        '****************** CUENTA BANCARIA
        Me.GV_CuentaBancaria.DataSource = Nothing
        Me.GV_CuentaBancaria.DataBind()

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnCambiarCC.Visible = False
                Me.NewCC.Visible = False
                Me.Panel_Cab.Enabled = True
                Me.Panel_Persona.Enabled = False
                Me.PanelBeneficiario.Enabled = False
                Me.btnAgregarNuevoCentroC.Visible = False
                Me.btnGuardarCentroC.Visible = False
                Me.btnAdd_CentroCosto.Visible = True
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_Totales.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_CentroCosto.Enabled = False

                Me.cboEstado.SelectedValue = "1"

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnCambiarCC.Visible = False
                Me.NewCC.Visible = False
                Me.btnAgregarNuevoCentroC.Visible = False
                Me.btnGuardarCentroC.Visible = False
                Me.btnAdd_CentroCosto.Visible = True
                Me.Panel_Cab.Enabled = True
                Me.Panel_Persona.Enabled = True
                Me.PanelBeneficiario.Enabled = True
                Me.PanelBeneficiario.Enabled = True
                Me.Panel_Totales.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_Aprobacion.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_CentroCosto.Enabled = True


                Me.cboEstado.SelectedValue = "1"

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnCambiarCC.Visible = False
                Me.btnAgregarNuevoCentroC.Visible = False
                Me.NewCC.Visible = False
                Me.btnGuardarCentroC.Visible = False
                Me.Panel_Cab.Enabled = True
                Me.Panel_Persona.Enabled = True
                Me.PanelBeneficiario.Enabled = True
                Me.Panel_Totales.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_Aprobacion.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_CentroCosto.Enabled = True
                Me.btnAdd_CentroCosto.Visible = True
            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnCambiarCC.Visible = False
                Me.btnAgregarNuevoCentroC.Visible = False
                Me.NewCC.Visible = False
                Me.btnGuardarCentroC.Visible = False
                Me.btnAdd_CentroCosto.Visible = True
                Me.Panel_Cab.Enabled = True
                Me.Panel_Persona.Enabled = False
                Me.PanelBeneficiario.Enabled = False
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_Totales.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_CentroCosto.Enabled = False

            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True
                Me.btnCambiarCC.Visible = True
                Me.btnAgregarNuevoCentroC.Visible = False
                Me.NewCC.Visible = False
                Me.btnGuardarCentroC.Visible = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Persona.Enabled = False
                Me.PanelBeneficiario.Enabled = False
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_Totales.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_CentroCosto.Enabled = False

            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnCambiarCC.Visible = False
                Me.btnGuardarCentroC.Visible = False
                Me.btnAgregarNuevoCentroC.Visible = False
                Me.NewCC.Visible = False
                Me.btnImprimir.Visible = True
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Persona.Enabled = False
                Me.PanelBeneficiario.Enabled = False
                Me.Panel_Totales.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_CentroCosto.Enabled = False
                Me.btnAdd_CentroCosto.Visible = False

            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnCambiarCC.Visible = False
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Persona.Enabled = False
                Me.PanelBeneficiario.Enabled = False
                Me.Panel_Totales.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_CentroCosto.Enabled = False
                Me.btnAgregarNuevoCentroC.Visible = False
                Me.NewCC.Visible = False
                Me.cboEstado.SelectedValue = "2"
                Me.btnGuardarCentroC.Visible = False
                Me.btnAdd_CentroCosto.Visible = False
        End Select
    End Sub

    Protected Sub btnGuardar_CxP_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar_CxP.Click

        valOnClick_btnGuardar_CxP()

    End Sub
    Private Sub valOnClick_btnGuardar_CxP()

        registrarDocumento()

    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub
    Private Sub valOnClick_btnNuevo()
        Try
            verFrm(FrmModo.Nuevo, True, True, True, True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        valOnClick_btnEditar()
    End Sub
    Private Sub valOnClick_btnEditar()
        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.GV_CuentaProveedor.DataSource = (New Negocio.CuentaProveedor).SelectxIdProv_IdPropCuentaProveedor(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_CuentaProveedor.DataBind()

            verFrm(FrmModo.Editar, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub cboUnidadNegocio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnidadNegocio.SelectedIndexChanged
        valOnChange_cboUnidadNegocio()
    End Sub
    Private Sub valOnChange_cboUnidadNegocio()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboDeptoFuncional(Me.cboDeptoFuncional, Me.cboUnidadNegocio.SelectedValue, True)
                .LlenarCboSubArea1(Me.cboSubArea1, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, True)
                .LlenarCboSubArea2(Me.cboSubArea2, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, True)
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub valOnChange_cboDeptoFuncional()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboSubArea1(Me.cboSubArea1, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, True)
                .LlenarCboSubArea2(Me.cboSubArea2, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, True)
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub valOnChange_cboSubArea1()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboSubArea2(Me.cboSubArea2, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, True)
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub valOnChange_cboSubArea2()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboDeptoFuncional_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeptoFuncional.SelectedIndexChanged
        valOnChange_cboDeptoFuncional()
    End Sub

    Private Sub cboSubArea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubArea1.SelectedIndexChanged
        valOnChange_cboSubArea1()
    End Sub

    Private Sub cboSubArea2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubArea2.SelectedIndexChanged
        valOnChange_cboSubArea2()
    End Sub

    Private Sub btnAdd_CentroCosto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd_CentroCosto.Click
        add_CentroCosto()
    End Sub

    Private Sub valAddCentroCosto()

        Dim IdCentroCosto As String = Me.cboUnidadNegocio.SelectedValue + Me.cboDeptoFuncional.SelectedValue + Me.cboSubArea1.SelectedValue + Me.cboSubArea2.SelectedValue + Me.cboSubArea3.SelectedValue
        Me.listaCentroCosto = getListaCentroCosto()

        For i As Integer = 0 To Me.listaCentroCosto.Count - 1

            If (Me.listaCentroCosto(i).IdCentroCosto = IdCentroCosto) Then
                Throw New Exception("EL CENTRO DE COSTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next


    End Sub
    Private Sub valAddCentroCosto2()

        Dim IdCentroCosto2 As String = Me.cboUnidadNegocio.SelectedValue + Me.cboDeptoFuncional.SelectedValue + Me.cboSubArea1.SelectedValue + Me.cboSubArea2.SelectedValue + Me.cboSubArea3.SelectedValue
        Me.listaCentroCosto2 = getListaCentroCosto2()

        For i As Integer = 0 To Me.listaCentroCosto2.Count - 1

            If (Me.listaCentroCosto2(i).IdCentroCosto = IdCentroCosto2) Then
                Throw New Exception("EL CENTRO DE COSTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next


    End Sub
    Private Sub add_NuevoCentroCosto()
        Try

            valAddCentroCosto2()

            actualizarListaCentroCosto2()

            Me.listaCentroCosto2 = getListaCentroCosto2()

            Dim objCentroCosto As New Entidades.GastoCentroCosto
            With objCentroCosto

                .IdCentroCosto = Me.cboUnidadNegocio.SelectedValue + Me.cboDeptoFuncional.SelectedValue + Me.cboSubArea1.SelectedValue + Me.cboSubArea2.SelectedValue + Me.cboSubArea3.SelectedValue
                .Monto = 0
                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .UnidadNegocio = CStr(Me.cboUnidadNegocio.SelectedItem.ToString)
                .DeptoFuncional = CStr(Me.cboDeptoFuncional.SelectedItem.ToString)
                .SubArea1 = CStr(Me.cboSubArea1.SelectedItem.ToString)
                .SubArea2 = CStr(Me.cboSubArea2.SelectedItem.ToString)
                .SubArea3 = CStr(Me.cboSubArea3.SelectedItem.ToString)

            End With
            Me.listaCentroCosto2.Add(objCentroCosto)
            setListaCentroCosto2(Me.listaCentroCosto2)

            Me.GV_NuevoCentroCosto.DataSource = Me.listaCentroCosto2
            Me.GV_NuevoCentroCosto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularCentroCosto2(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub add_CentroCosto()
        Try

            valAddCentroCosto()

            actualizarListaCentroCosto()

            Me.listaCentroCosto = getListaCentroCosto()

            Dim objCentroCosto As New Entidades.GastoCentroCosto
            With objCentroCosto

                .IdCentroCosto = Me.cboUnidadNegocio.SelectedValue + Me.cboDeptoFuncional.SelectedValue + Me.cboSubArea1.SelectedValue + Me.cboSubArea2.SelectedValue + Me.cboSubArea3.SelectedValue
                .Monto = 0
                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .UnidadNegocio = CStr(Me.cboUnidadNegocio.SelectedItem.ToString)
                .DeptoFuncional = CStr(Me.cboDeptoFuncional.SelectedItem.ToString)
                .SubArea1 = CStr(Me.cboSubArea1.SelectedItem.ToString)
                .SubArea2 = CStr(Me.cboSubArea2.SelectedItem.ToString)
                .SubArea3 = CStr(Me.cboSubArea3.SelectedItem.ToString)

            End With
            Me.listaCentroCosto.Add(objCentroCosto)
            setListaCentroCosto(Me.listaCentroCosto)

            Me.GV_CentroCosto.DataSource = Me.listaCentroCosto
            Me.GV_CentroCosto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularCentroCosto(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_CentroCosto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_CentroCosto.SelectedIndexChanged
        quitarCentroCosto(Me.GV_CentroCosto.SelectedIndex)
    End Sub
    Private Sub quitarCentroCosto(ByVal index As Integer)
        Try

            actualizarListaCentroCosto()

            Me.listaCentroCosto = getListaCentroCosto()

            Me.listaCentroCosto.RemoveAt(index)

            Me.GV_CentroCosto.DataSource = Me.listaCentroCosto
            Me.GV_CentroCosto.DataBind()

            setListaCentroCosto(Me.listaCentroCosto)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub actualizarListaCentroCosto2()

        Me.listaCentroCosto2 = getListaCentroCosto2()

        For i As Integer = 0 To Me.GV_NuevoCentroCosto.Rows.Count - 1

            Me.listaCentroCosto2(i).Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
            Me.listaCentroCosto2(i).Monto = CDec(CType(Me.GV_NuevoCentroCosto.Rows(i).FindControl("txtMonto"), TextBox).Text)

        Next

        setListaCentroCosto2(Me.listaCentroCosto2)

    End Sub
    Private Sub actualizarListaCentroCosto()

        Me.listaCentroCosto = getListaCentroCosto()

        For i As Integer = 0 To Me.GV_CentroCosto.Rows.Count - 1

            Me.listaCentroCosto(i).Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
            Me.listaCentroCosto(i).Monto = CDec(CType(Me.GV_CentroCosto.Rows(i).FindControl("txtMonto"), TextBox).Text)

        Next

        setListaCentroCosto(Me.listaCentroCosto)

    End Sub


#Region "Busqueda de Documento Externos"



#End Region


    Private Sub cargarDocumentoExterno(ByVal IdDocumento As Integer)

        Try

            '****************** CABECERA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            If (objDocumento Is Nothing) Then
                Throw New Exception("NO SE HALLARON REGISTROS. NO SE PERMITE LA OPERACIÓN.")
            End If

            '*************  LISTA DOCUMENTO REF
            Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumento.Id)

            '***************** ANEXO DOCUMENTO
            Dim objAnexoDocumento As Entidades.Anexo_Documento = (New Negocio.Anexo_Documento).Anexo_DocumentoSelectxIdDocumento(objDocumento.Id)

            '*************************   REGIMEN
            Dim regimen_Percepcion As Decimal = (New Negocio.MontoRegimen).SelectPercepcionxIdDocumento(objDocumento.Id)
            Dim regimen_Detraccion As Decimal = (New Negocio.MontoRegimen).SelectDetraccionxIdDocumento(objDocumento.Id)
            Dim regimen_Retencion As Decimal = (New Negocio.MontoRegimen).SelectRetencionxIdDocumento(objDocumento.Id)

            '****************** PERSONA / PROVEEDOR
            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)

            '****************** OBSERVACIONES
            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

            '*********************** REGISTRADO EN MOV CUENTA POR PAGAR
            Dim movCuentaCXP As Boolean = False
            If (((New Negocio.Util).ValidarExistenciaxTablax1Campo("MovCuentaPorPagar", "IdDocumento", CStr(objDocumento.Id))) > 0) Then
                movCuentaCXP = True
            End If

            '***************** DETALLE CONCEPTO
            Dim listaDetalleConcepto As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumento.Id)

            '***************** CENTRO DE COSTO
            Me.listaCentroCosto = (New Negocio.GastoCentroCosto).SelectxIdDocumento(objDocumento.Id)


            cargarDocumentoExterno_GUI(objDocumento, objPersona, Me.listaDocumentoRef, objAnexoDocumento, objObservaciones, regimen_Percepcion, regimen_Detraccion, regimen_Retencion, movCuentaCXP, listaDetalleConcepto, listaCentroCosto)


            '********************* ALMACENA EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)
            setListaCentroCosto(Me.listaCentroCosto)

            '****************** EDITAR
            verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularCentroCosto(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub cargarDocumentoExterno_GUI(ByVal objDocumento As Entidades.Documento, ByVal objPersona As Entidades.PersonaView, ByVal listaDocumentoRef As List(Of Entidades.Documento), ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal objObservaciones As Entidades.Observacion, ByVal regimen_Percepcion As Decimal, ByVal regimen_Detraccion As Decimal, ByVal regimen_Retencion As Decimal, ByVal movCuentaCXP As Boolean, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal ListaCentroCosto As List(Of Entidades.GastoCentroCosto))

        Dim objCbo As New Combo

        '************************ CABECERA
        If (objDocumento IsNot Nothing) Then

            If (Me.cboEmpresa.Items.FindByValue(CStr(objDocumento.IdEmpresa)) IsNot Nothing) Then

                Me.cboEmpresa.SelectedValue = CStr(objDocumento.IdEmpresa)
                objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

            End If

            If (Me.cboTienda.Items.FindByValue(CStr(objDocumento.IdTienda)) IsNot Nothing) Then

                Me.cboTienda.SelectedValue = CStr(objDocumento.IdTienda)

            End If

            If (Me.cboTipoDocumento.Items.FindByValue(CStr(objDocumento.IdTipoDocumento)) IsNot Nothing) Then

                Me.cboTipoDocumento.SelectedValue = CStr(objDocumento.IdTipoDocumento)
                objCbo.llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.cboTipoDocumento.SelectedValue), False)

            End If

            'Me.txtSerie.Text = objDocumento.Serie
            'Me.txtNroDocumento.Text = objDocumento.Codigo

            If (objDocumento.FechaEmision <> Nothing) Then
                Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
            End If

            If (objDocumento.FechaVenc <> Nothing) Then
                Me.txtFechaVcto.Text = Format(objDocumento.FechaVenc, "dd/MM/yyyy")
            Else
                Me.txtFechaVcto.Text = ""
            End If

            If (Me.cboEstado.Items.FindByValue(CStr(objDocumento.IdEstadoDoc)) IsNot Nothing) Then

                Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)

            End If

            If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then

                Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
                valOnChange_cboMoneda()
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then

                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)

            End If


            '******************* TOTALES
            Me.txtSubTotal.Text = CStr(Math.Round(objDocumento.SubTotal, 3))
            Me.txtTotal.Text = CStr(Math.Round(objDocumento.Total, 3))
            Me.txtIgv.Text = CStr(Math.Round(objDocumento.IGV, 3))
            Me.txtTotalAPagar.Text = CStr(Math.Round(objDocumento.TotalAPagar, 3))


            '****************** REGIMEN   PERCEPCION
            If (regimen_Percepcion > 0) Then
                Me.chb_SujetoPercepcion.Checked = True
                Me.txtPercepcion_Monto.Enabled = True
            Else
                Me.chb_SujetoPercepcion.Checked = False
                Me.txtPercepcion_Monto.Enabled = False
            End If
            Me.txtPercepcion.Text = CStr(Math.Round(regimen_Percepcion, 3))
            Me.txtPercepcion_Monto.Text = CStr(Math.Round(regimen_Percepcion, 3))

            '****************** REGIMEN   DETRACCION
            If (regimen_Detraccion > 0) Then
                Me.chb_SujetoDetraccion.Checked = True
                Me.txtDetraccion_Monto.Enabled = True
            Else
                Me.chb_SujetoDetraccion.Checked = False
                Me.txtDetraccion_Monto.Enabled = False
            End If
            Me.txtDetraccion_Monto.Text = CStr(Math.Round(regimen_Detraccion, 3))

            '****************** REGIMEN   RETENCION
            If (regimen_Retencion > 0) Then
                Me.chb_SujetoRetención.Checked = True
                Me.txtRetencion_Monto.Enabled = True
            Else
                Me.chb_SujetoRetención.Checked = False
                Me.txtRetencion_Monto.Enabled = False
            End If
            Me.txtRetencion_Monto.Text = CStr(Math.Round(regimen_Retencion, 3))


            Me.hddIdDocumento.Value = CStr(objDocumento.Id)

        End If


        '****************** PROVEEDOR
        If (objPersona IsNot Nothing) Then

            cargarPersona(objPersona.IdPersona, False)

        End If

        '********************** ANEXO DOCUMENTO - APROBACION - MONTOS 
        If (objAnexoDocumento IsNot Nothing) Then

            With objAnexoDocumento

                If (.Aprobar) Then
                    Me.rdbAutorizado.SelectedValue = "1"
                    Me.txtFechaAprobacion.Text = CStr(.FechaAprobacion)
                    Me.chbEnviarCxP.Enabled = True
                Else
                    Me.rdbAutorizado.SelectedValue = "0"
                    Me.txtFechaAprobacion.Text = ""
                    Me.chbEnviarCxP.Enabled = False
                End If

                Me.txtMontoAfectoIgv.Text = CStr(Math.Round(.MontoAfectoIgv, 2))
                Me.txtMontoAfectoIgv_Monto.Text = CStr(Math.Round(.MontoAfectoIgv, 2))

                Me.txtMontoNoAfectoIgv.Text = CStr(Math.Round(.MontoNoAfectoIgv, 2))
                Me.txtMontoNoAfectoIgv_Monto.Text = CStr(Math.Round(.MontoNoAfectoIgv, 2))

                Me.chbEnviarCxP.Checked = movCuentaCXP

                Me.txtOtrosTributos.Text = CStr(Math.Round(.MontoOtroTributo, 2))
                Me.txtIsc.Text = CStr(Math.Round(.MontoISC, 2))

                Me.txtRetencionHonorarios.Text = CStr(Math.Round(.MontoxSustentar, 2))
                'Me.txtRetencionHonorarios.Text = CStr(Math.Round(.RetencionHonorarios , 2))
                Me.txtRetencionHonorariosPercent.Text = CStr(Math.Round(.RetencionHonorariosPercent, 2))


                Me.txtSerie.Text = .Serie
                Me.txtNroDocumento.Text = .Codigo

            End With

        End If

        '*********************** DOCUMENTOS DE REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumentoRef
        Me.GV_DocumentoRef.DataBind()

        '************************** LISTA DETALLE CONCEPTO
        Me.GV_DetalleConcepto.DataSource = listaDetalleConcepto
        Me.GV_DetalleConcepto.DataBind()

        '************************** LISTA CENTRO DE COSTO
        Me.GV_CentroCosto.DataSource = ListaCentroCosto
        Me.GV_CentroCosto.DataBind()

        '**************************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then

            Me.txtObservaciones.Text = objObservaciones.Observacion

        End If

    End Sub

    Protected Sub GV_DocumentoCab_BuscarDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_DocumentoCab_BuscarDocumento.SelectedIndexChanged

        cargarDocumentoExterno(CInt(CType(Me.GV_DocumentoCab_BuscarDocumento.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value))


    End Sub




    Private Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        OnClick_btnBuscarDocumentoxCodigo()
    End Sub

    Private Sub OnClick_btnBuscarDocumentoxCodigo()
        valOnClick_btnBuscarDocumentoxCodigo(0)
    End Sub

    Private Sub valOnClick_btnBuscarDocumentoxCodigo(ByVal TipoMov As Integer)

        Try

            Dim IdPersona As Integer = 0
            If (IsNumeric(Me.hddIdPersona.Value) And Me.hddIdPersona.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdPersona.Value) <> 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            Dim index As Integer = 0
            Select Case TipoMov
                Case 0 '********************************************************* INICIO
                    index = 0
                Case 1 '********************************************************* Anterior
                    index = (CInt(txtPageIndex_Documento.Text) - 1) - 1
                Case 2 '********************************************************* Posterior
                    index = (CInt(txtPageIndex_Documento.Text) - 1) + 1
                Case 3 '********************************************************* IR
                    index = (CInt(txtPageIndex_Documento.Text) - 1)
            End Select


            Dim ListaDocumentoExt As List(Of Entidades.Documento) = (New Negocio.Documento).SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExt(CInt(Me.cboTipoDocumento.SelectedValue), IdPersona, Me.txtSerie.Text.Trim, Me.txtNroDocumento.Text.Trim, index, GV_DocumentoCab_BuscarDocumento.PageSize)

            If ListaDocumentoExt Is Nothing Then ListaDocumentoExt = New List(Of Entidades.Documento)

            If ListaDocumentoExt.Count > 0 Then

                Me.GV_DocumentoCab_BuscarDocumento.DataSource = ListaDocumentoExt
                Me.GV_DocumentoCab_BuscarDocumento.DataBind()

                txtPageIndex_Documento.Text = CStr(index + 1)
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "OnLoad", "     onCapa('capaDocumentoCab_BuscarDocumento');       ", True)

            Else

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " alert('No se hallaron registros'); ", True)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAnterior_Documento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Documento.Click
        valOnClick_btnBuscarDocumentoxCodigo(1)
    End Sub

    Private Sub btnPosterior_Documento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Documento.Click
        valOnClick_btnBuscarDocumentoxCodigo(2)
    End Sub

    Private Sub btnIr_Documento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Documento.Click
        valOnClick_btnBuscarDocumentoxCodigo(3)
    End Sub
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiar.Click
        Me.txtDescripcionPersona.Text = " "
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""
        Me.hddIdPersona.Value = CStr("0")
    End Sub

    Protected Sub btnLimpiarBene_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiarBene.Click
        Me.txtApellidoBen.Text = " "
        Me.txtDniBene.Text = ""
        Me.txtRucBene.Text = ""
        Me.hddIdPersona2.Value = CStr("0")
    End Sub
    Protected Sub rdTipoBusquedaExt_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdTipoBusquedaExt.SelectedIndexChanged
        If (rdTipoBusquedaExt.SelectedValue = "0") Then
            PnlFecha.Visible = True
            PnlDocumento.Visible = False
            txtcodigoDocexterno.Text = ""
            txtserieDocexterno.Text = ""
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "OnLoad", " onCapa('capaReporteDocExternos');  ", True)
        Else
            PnlDocumento.Visible = True
            PnlFecha.Visible = False
            'Me.txtInicioDocExterno.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            'Me.txtFinDocExterno.Text = Me.txtInicioDocExterno.Text
            txtInicioDocExterno.Text = CStr(Date.Now)
            txtFinDocExterno.Text = CStr(Date.Now)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "OnLoad", " onCapa('capaReporteDocExternos');  ", True)
        End If

    End Sub
    Private Sub GenerarReporte()
        Dim listReportDocExterno As List(Of Entidades.Documento) = New List(Of Entidades.Documento)
        Dim codigo As Integer = 0
        Dim serie As Integer = 0
        Dim FechaIni As Date = Nothing
        Dim Fechafin As Date = Nothing
        Dim IdTienda As Integer = 0

        If (txtcodigoDocexterno.Text = "" And txtserieDocexterno.Text = "") Then
            txtcodigoDocexterno.Text = "0"
            txtserieDocexterno.Text = "0"
        End If

        codigo = CInt(txtcodigoDocexterno.Text)
        serie = CInt(txtserieDocexterno.Text)
        FechaIni = CDate(txtInicioDocExterno.Text)
        Fechafin = CDate(txtFinDocExterno.Text)
        IdTienda = CInt(ddlTiendaReporte.SelectedValue)


        If (codigo <> 0 And serie <> 0) Then
            listReportDocExterno = New Negocio.Documento().SelectReporteDocExterno(serie, codigo, Nothing, Nothing, IdTienda)
        Else
            listReportDocExterno = New Negocio.Documento().SelectReporteDocExterno(serie, codigo, FechaIni, Fechafin, IdTienda)
        End If
        GV_ReporteDocExterno.DataSource = listReportDocExterno
        GV_ReporteDocExterno.DataBind()
        txtcodigoDocexterno.Text = ""
        txtserieDocexterno.Text = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "OnLoad", " onCapa('capaReporteDocExternos');  ", True)
    End Sub
    Protected Sub btnGenerarReportefecha_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerarReportefecha.Click
        GenerarReporte()
    End Sub
    Protected Sub btnGenerarReportecodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerarReportecodigo.Click
        GenerarReporte()
    End Sub

    Protected Sub btnExportar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportar.Click
        If (GV_ReporteDocExterno.Rows.Count = 0) Then
            objScript.mostrarMsjAlerta(Me, "Por Favor Ingrese los Criterios de Búsqueda")
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "OnLoad", " onCapa('capaReporteDocExternos');  ", True)
        Else
            ExportToExcel("DocumentosExternos.xls", GV_ReporteDocExterno)
        End If
    End Sub
    Private Sub ExportToExcel(ByVal nameReport As String, ByVal wControl As GridView)

        Dim responsePage As HttpResponse = Response
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim pageToRender As New Page()
        Dim form As New HtmlForm()
        form.Controls.Add(wControl)
        pageToRender.Controls.Add(form)
        responsePage.Clear()
        responsePage.Buffer = True
        responsePage.ContentType = "application/vnd.ms-excel"
        responsePage.AddHeader("Content-Disposition", "attachment;filename=" & nameReport)
        responsePage.Charset = "UTF-8"
        responsePage.ContentEncoding = Encoding.Default
        pageToRender.RenderControl(htw)
        responsePage.Write(sw.ToString())
        responsePage.End()
    End Sub

    Protected Sub btnExportr2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportr2.Click
        If (GV_ReporteDocExterno.Rows.Count = 0) Then
            objScript.mostrarMsjAlerta(Me, "Por Favor Ingrese los Criterios de Búsqueda")
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "OnLoad", " onCapa('capaReporteDocExternos');  ", True)
        Else
            ExportToExcel("DocumentosExternos.xls", GV_ReporteDocExterno)
        End If
    End Sub

    Protected Sub btnCambiarCC_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCambiarCC.Click
        Session.Remove("listaCentroCosto2")

        Me.listaCentroCosto2 = New List(Of Entidades.GastoCentroCosto)
        setListaCentroCosto2(Me.listaCentroCosto2)

        Me.Panel_CentroCosto.Enabled = True
        Me.Panel_CentroCosto_Combos.Enabled = True
        Me.Panel_CentroCosto_Grilla.Enabled = True
        Me.NewCC.Visible = True
        Me.btnAdd_CentroCosto.Visible = False
        Me.GV_CentroCosto.Enabled = False
        Me.btnAgregarNuevoCentroC.Visible = True
        Me.btnCambiarCC.Visible = False
        'Me.cboUnidadNegocio.Visible = True
        'Me.cboSubArea1.Visible = True
        'Me.cboSubArea2.Visible = True
        'Me.cboSubArea3.Visible = True

    End Sub
    Protected Sub btnAgregarNuevoCentroC_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregarNuevoCentroC.Click
        add_NuevoCentroCosto()
        Me.btnGuardarCentroC.Visible = True

    End Sub

    Protected Sub btnGuardarCentroC_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarCentroC.Click
        Try

            actualizarListaCentroCosto2()
            Me.listaCentroCosto2 = getListaCentroCosto2()
            Dim resultado As Boolean = New Negocio.Centro_Costo().ActualizarCentroCostos(Me.listaCentroCosto2, CInt(hddIdDocumento.Value))
            objScript.mostrarMsjAlerta(Me, "Centro de Costo modificado correctamente.")
            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", ddl_Rol.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btBuscarPersonaGrillas2_Click(sender As Object, e As System.EventArgs) Handles btBuscarPersonaGrillas2.Click
        Try
            ViewState.Add("dni2", tbDni2.Text.Trim)
            ViewState.Add("ruc2", tbRuc2.Text.Trim)
            ViewState.Add("pat2", tbApellido2.Text.Trim)
            ViewState.Add("tipo2", CInt(IIf(Me.rdbTipoPersona2.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado2", 1) '******************* ACTIVO
            ViewState.Add("rol2", ddl_Rol.SelectedValue)

            Buscar2(gvBuscar2, CStr(ViewState("dni2")), CStr(ViewState("ruc2")), CStr(ViewState("pat2")), CInt(ViewState("tipo2")), CInt(ViewState("rol2")), CInt(ViewState("estado2")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
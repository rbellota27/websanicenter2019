﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Partial Public Class FrmPage_Load
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valFrm_OnLoad()
    End Sub

    Private Sub valFrm_OnLoad()

        If Not Me.IsPostBack Then

            Select Case CInt(Request.QueryString("Tipo"))

                Case 1
                    '**************** RECIBO DE INGRESO
                    Dim IdDocumentoRef As Integer = CInt(Request.QueryString("IdDocumentoRef"))
                    Response.Redirect("~/Caja/FrmReciboIngreso.aspx?IdDocumentoRef=" + CStr(IdDocumentoRef))
                Case 2
                    '**************** RECIBO DE EGRESO
                    Dim IdDocumentoRef As Integer = CInt(Request.QueryString("IdDocumentoRef"))
                    Response.Redirect("~/Finanzas/FrmDocCancelacionCaja.aspx?IdDocumentoRef=" + CStr(IdDocumentoRef))

                Case 3
                    '**************** RECIBO DE EGRESO
                    Response.Redirect("~/Mantenedores/BusquedaProducto.aspx")
            End Select

        End If

    End Sub
End Class
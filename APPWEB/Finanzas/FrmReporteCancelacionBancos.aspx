<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmReporteCancelacionBancos.aspx.vb" Inherits="APPWEB.FrmReporteCancelacionBancos" 
    title="P�gina sin t�tulo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <table class="style1" width="100%">


           <tr>
            <td class="TituloCelda">
               REPORTE DE DOC. CANCELACION BANCOS
            </td>
        </tr>
        <tr>
        <td>
        <asp:Panel ID="Panel_Cab" runat="server">
                   
                   <table >
                     <tr>    <td></td>
                            <td align="right" class="Texto">
                                Fecha Inicio:
                            </td>
                            <td >
                                <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="150px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                </cc1:CalendarExtender>
                           </td>
                           <td></td>
                          <td align="right" class="Texto">
                                Fecha de Fin:
                            </td>
                            <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="150px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                </cc1:CalendarExtender>
                            </td>
                            <td></td>
                            <td></td>
                             <td></td>
                                <td></td>
                                <td></td>
                        </tr>
                        <tr>
                        <td></td>
                     <%-- <td align="right" class="Texto">
                               Programaci�n:
                            </td>
                      <td>
                            <asp:DropDownList ID="cboFiltro_Opcion_Programacion" runat="server" Width="100%"
                                Font-Bold="true">
                                <asp:ListItem Value="0" Selected="True">Sin Programar</asp:ListItem>
                                <asp:ListItem Value="1">Programados</asp:ListItem>
                                <asp:ListItem Value="2">Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td></td>
                          <td></td>--%>
                    
                        
                          
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                        </tr>
                   </table>
                   <table>
                  
                   <tr>
                   <td></td>
                   <td></td>
         

                   <td>
                                <asp:Button id="btnBuscar" runat="server" Text="Buscar" Width="160px" />
                                </td>
                                <td></td>
                                <td>
                                <asp:Button id="btnExport" runat="server" Text="Exportar" Width="140px"/>
                                </td>
                   </tr>
                   </table>
                   <br />
                   <br />
                                <table width="100%">
                            
                                  <tr>
                                 
                                <td class="TituloCelda">
                                   RESULTADO DE LA B�SQUEDA
                                </td>
                            </tr>
                                <tr>
                                
                                <td>  
                                <asp:GridView ID="GV_Inventrio" runat="server" AutoGenerateColumns="False" 
                                        Width="96%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                                        BorderWidth="1px" CellPadding="3"  >
                                       
                                <Columns>
                                    <asp:TemplateField HeaderText="Tipo Documento"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lbltipodoc"  runat="server" ForeColor="#0B3861" Font-Bold="true" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label> </td>
                                        
                                            </tr>
                                           </table>
                                     </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                   <asp:TemplateField HeaderText="Documento"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblnroDocumento"  runat="server" ForeColor="Red" Font-Bold="true" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label> </td>
                                        
                                            </tr>
                                           </table>
                                     </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                   
                                   
                                   <asp:TemplateField HeaderText="Beneficiario" >
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblProveedor"  runat="server" ForeColor="#0B3861" Font-Bold="true" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label></td> 
                                             </tr>
                                             </table>                                        
                                   </ItemTemplate>
                                   </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Monto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                            <table>
                                            <tr>
                                            <td>   <asp:Label ID="lblmonedapago"  runat="server"  ForeColor="Red" Font-Bold="true" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label> </td>
                                           
                                                 <td>   <asp:Label ID="Label1"  runat="server"  ForeColor="Red" Font-Bold="true" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Total")%>'></asp:Label> </td>
                                            </table>
                                   </ItemTemplate>                      
                                         <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                         <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>
                                                                       <asp:TemplateField HeaderText="Fecha Emisi�n" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblpp_FechaPagoProg"  runat="server" ForeColor="#0B3861" Font-Bold="true" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"FechaEmision","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                                                       
                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                   
                                                                       
                                   </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Tienda "  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblTienda"  runat="server"  ForeColor="#0B3861" Font-Bold="true" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                                           </td>
                                           </tr>
                                   </table>
                                   </ItemTemplate>
                               
                                  </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Tipo Operaci�n"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lbloperacion"  runat="server"  ForeColor="#0B3861" Font-Bold="true" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoOperacion")%>'></asp:Label>
                                           </td>
                                           </tr>
                                   </table>
                                   </ItemTemplate>
                               
                                  </asp:TemplateField>
                                   
                                   
                                                     
                                </Columns>
                                <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
                                <PagerStyle CssClass="GrillaPager"
                                        HorizontalAlign="Left" BackColor="White" ForeColor="#000066" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
                                        ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
                                        ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView></td>
                                </tr>
                                
                                <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                              
                                </tr>
                                  <tr>
                                <td></td>
                                
                                
                                </tr>
                                    </table>
                                                       
             </asp:Panel>
        </td>
          
      
        </tr>
              
</table>


 <script language="javascript" type="text/javascript">

 
        
 </script>

</asp:Content>
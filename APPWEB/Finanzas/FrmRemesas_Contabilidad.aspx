﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmRemesas_Contabilidad.aspx.vb" Inherits="APPWEB.FrmRemesas_Contabilidad" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table width="100%">
        <tr>
            <td bgcolor="#0033CC" style="text-align: center; color: #FFFFFF; height: 23px;">
                <strong>CREACION DE ASIENTOS DE REMESAS</strong></td>
        </tr>
                <tr>
            <td style="height: 9px; background-color: #CCFFFF;">
                <table class="style1">
                    <tr>
                        <td style="width: 130px; text-align: right; color: #0033CC;">
                            Fecha Inicio :</td>
                        <td style="width: 132px; color: #0033CC;">
                             <asp:TextBox ID="txtFechaInicial" runat="server" Text="" Height="22px" 
                                 Width="244px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtenderFI" runat="server" TargetControlID="txtFechaInicial">
                        </cc1:CalendarExtender></td>
                        <td style="width: 37px; color: #0033CC;">
                            &nbsp;</td>
                        <td style="width: 96px; text-align: right; color: #0033CC;">
                            Fecha Fin:</td>
                        <td style="width: 141px">
                             <asp:TextBox ID="txtFechaFinal" runat="server" Text="" Height="20px" 
                                 Width="146px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtenderFF" runat="server" TargetControlID="txtFechaFinal">
                        </cc1:CalendarExtender></td>
                        <td class="style2" style="width: 47px">
                            &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 130px; text-align: right; color: #0033CC;">
                            Banco :</td>
                        <td style="width: 132px; color: #0033CC;">
                            <select id="cboBanco" name="D1" 
                                style="width: 247px; margin-left: 0px; height: 26px;">
                            </select></td>
                        <td style="width: 37px; color: #0033CC;">
                            &nbsp;</td>
                        <td style="width: 96px; text-align: right; color: #0033CC;">
                            Moneda :</td>
                        <td style="width: 141px">
                                <select ID="cboMoneda" style="width:147px; margin-left: 0px;" name="D2">
                                 <option value="1">SOLES</option>
                                 <option value="2">DOLARES</option>
                                        </select></td>
                        <td class="style2" style="width: 47px">
                            &nbsp;</td>
                                <td>
                                    <input id="btnBuscar" style="width: 190px" type="button" 
                                        value="Mostrar Movimientos"  onclick="return btnBuscar_onclick(this)"/></td>
                    </tr>
                </table>
            </td>
        </tr>
                <tr>
            <td style="height: 23px">
            </td>
        </tr>
                <tr>
            <td>
                <table class="style1">
                    <tr>
                        <td style="width: 136px; color: #0033CC; text-align: right; background-color: #CCFFCC;">
                            <strong>Fecha de Asiento :</strong></td>
                        <td style="width: 182px; background-color: #CCFFCC;">
                             <asp:TextBox ID="txtFechaAsiento" runat="server" Text="" Height="24px" 
                                 Width="237px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtenderFA" runat="server" TargetControlID="txtFechaAsiento">
                        </cc1:CalendarExtender></td></td>
                        <td style="width: 23px; background-color: #CCFFCC">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 136px; background-color: #CCFFCC;">
                            &nbsp;</td>
                        <td style="width: 182px; background-color: #CCFFCC;">
                            <input id="btnGenerarAsiento" style="width: 229px" type="button" 
                                value="Generar Asiento"  onclick="return btnGenerarAsiento_onclick(this)"/></td>
                        <td style="width: 23px; background-color: #CCFFCC">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
                <tr>
            <td style="height: 14px">
            </td>
        </tr>
                   <tr>
            <td>
            <div id="GrillaMovBancos">
            </div>

            </td>
        </tr>
    </table>

    <script type="text/javascript" language="javascript">
        window.onload = varios();
        
        function varios() {
            ConsultaCombos();
        }

        function btnBuscar_onclick(objeto) {
            BuscarMovimientosBancos(objeto);
        }


        function btnGenerarAsiento_onclick(objeto) {
            var FechaAsiento = document.getElementById('<%= txtFechaAsiento.ClientID%>').value;
            var r = confirm("Esta Seguro de Generar el Asiento con Fecha: " + FechaAsiento);
            if (r == true) {
                GenerarAsientoContable(objeto);
                return false;
            } else {
                alert("No se genero ningun Asiento");
                return false;
            }

        }

        function GenerarAsientoContable(objeto,flag) { 
            var opcion="GenerarAsientoContable"

            var IdEstado = null;

//            var fechaInicio = document.getElementById('<%= txtFechaInicial.ClientID%>').value;
//            var FechaFinal = document.getElementById('<%= txtFechaFinal.ClientID%>').value;
//            var Empresa = '001';
            var DataMov = new FormData();
            var banco = document.getElementById("cboBanco").value;
            var Moneda = document.getElementById("cboMoneda").value;
            var FechaAsiento = document.getElementById('<%= txtFechaAsiento.ClientID%>').value;
            var dataMov = new FormData();

            var parts = FechaAsiento.split('/');
            var mydate = parts[2];
            var periodo = mydate;

            DataMov.append('flag', 'GenerarAsientoContable');
            DataMov.append('periodo', periodo);
            DataMov.append('Moneda', Moneda);
            DataMov.append('banco', banco);
            DataMov.append('FechaAsiento', FechaAsiento);

//            dataMov.append('Empresa', Empresa);
//            dataMov.append('fechaInicio', fechaInicio);
//            dataMov.append('FechaFinal', FechaFinal);

            var xhrGenera = new XMLHttpRequest();
            xhrGenera.open("POST", "FrmRemesas_Contabilidad.aspx?flag=" + opcion + "&Periodo=" + periodo + "&Moneda=" + Moneda + "&banco=" + banco + "&FechaAsiento=" + FechaAsiento, true);

            xhrGenera.onreadystatechange = function () {
               
                if (xhrGenera.readyState == 4 && xhrGenera.status == 200)
                 {
                    fun_lista(xhrGenera.responseText);
                 }
            }
            xhrGenera.send(DataMov);
            return false;
        
        }



        function BuscarMovimientosBancos(objeto, flag) {
            var opcion = "BuscarMovimientosBancos";
            var IdEstado = null;
            var fechaInicio = document.getElementById('<%= txtFechaInicial.ClientID%>').value;
            var FechaFinal = document.getElementById('<%= txtFechaFinal.ClientID%>').value;
            var Empresa = '001';
            var banco=document.getElementById("cboBanco").value;
            var Moneda = document.getElementById("cboMoneda").value;
            var FechaAsiento = document.getElementById('<%= txtFechaAsiento.ClientID%>').value;
            var dataMovimientos = new FormData();

            dataMovimientos.append('flag', 'BuscarMovimientosBancos');
            dataMovimientos.append('Empresa', Empresa);
            dataMovimientos.append('Moneda', Moneda);
            dataMovimientos.append('banco', banco);
            dataMovimientos.append('fechaInicio', fechaInicio);
            dataMovimientos.append('FechaFinal', FechaFinal);
            dataMovimientos.append('FechaAsiento', FechaAsiento);

            var xhrGenera = new XMLHttpRequest();
            xhrGenera.open("POST", "FrmRemesas_Contabilidad.aspx?flag=" + opcion + "&Empresa=" + Empresa + "&Moneda=" + Moneda + "&banco=" + banco + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&FechaAsiento=" + FechaAsiento, true);


            xhrGenera.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
            xhrGenera.onloadend = function () { objeto.disabled = false; objeto.value = "Mostrar Movimiento"; }

            
            xhrGenera.onreadystatechange = function () {
                if (xhrGenera.readyState == 4 && xhrGenera.status == 200) {
                    fun_lista(xhrGenera.responseText);
                }
            }
            xhrGenera.send(dataMovimientos);
            return false;
        }

        function fun_lista(lista) {
            filas = lista.split("▼");
            crearTablaConsulta();
            crearMatrizConsulta();
            mostrarMatrizConsulta();
            configurarFiltros();
        }

        function configurarFiltros() {
            var textos = document.getElementsByClassName("texto");
            var nTextos = textos.length;
            var texto;
            for (var j = 0; j < nTextos; j++) {
                texto = textos[j];
                texto.onkeyup = function () {
                    crearMatrizConsulta();
                    mostrarMatrizConsulta();
                }
            }
        }

        function crearMatrizConsulta() {
            matrizConsulta = [];
            var nRegistros = filas.length;
            var nCampos; 
            var campos;
            var c = 0;
            var exito;
            var textos = document.getElementsByClassName("texto");
            var nTextos = textos.length;
            var texto;
            var x;
            for (var i = 0; i < nRegistros; i++) {
                exito = true;
                campos = filas[i].split("|");
                nCampos = campos.length;
                for (var j = 0; j < nTextos; j++) {
                    texto = textos[j];
                    x = j;

                    if (x < 13) {
                        if (texto.value.length > 0) {
                            if (campos[x] != campos[0]) {
                                exito = campos[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                            }
                        }
                    }


                    if (!exito) break;
                }
                if (exito) {
                    matrizConsulta[c] = [];
                    for (var j = 0; j < nCampos; j++) {
                        if (isNaN(campos[j])) matrizConsulta[c][j] = campos[j];

                        else matrizConsulta[c][j] = campos[j];
                    }
                    c++;
                }
            }
        }


        function mostrarMatrizConsulta() {
            var nRegistros = matrizConsulta.length;
            var contenido = "";
            if (nRegistros > 0) {
                var nCampos = matrizConsulta[0].length;
                for (var i = 0; i < nRegistros; i++) {
                     for (var j = 0; j < nCampos; j++) {
                        var cadenaid
                        var cadenareq
                        cadenaid = '' + matrizConsulta[i][1] + '';
                        cadenareq = '' + matrizConsulta[i][2] + '';

                        contenido += "<td>";
                        if (j == 0) {
                            if (matrizConsulta[i][0] == 0) {
                                contenido += "<input type='checkbox' value='checkbox'  onchange='return verificar(" + '"' + cadenaid + '"' + ");' />"
                            }
                            if (matrizConsulta[i][0] == 1) {
                                contenido += "<input type='checkbox' value='checkbox' checked = true onchange='return verificar(" + '"' + cadenaid + '"' + ");'/>"
                            }
                        }

                        if (j > 0) {
                            contenido += matrizConsulta[i][j];
                        }
                        contenido += "</td>";

                    }
                    contenido += "</tr>";
                }
            }

            var tabla = document.getElementById("tbDocumentosConsulta");
            tabla.innerHTML = contenido;
        }


//-----------------------------------------------------------------------------------------

function verificar(IdMovBanco) 

{
            var opcion = "UpdateDocumentoEgreso";
            var IdEstado = null;

            var FechaAsiento = document.getElementById('<%= txtFechaAsiento.ClientID%>').value;
            
                    if (FechaAsiento ==null){
                
                        alert("Ingrese la fecha de Asiento");
                        return false;

                    }

            var r = confirm("Esta Seguro de Actualizar Movimiento Asiento con Fecha "+FechaAsiento );
            if (r == true)

           {
               
                     var datarep = new FormData();
                     datarep.append('flag', 'UpdateDocumentoEgreso');
                     datarep.append('IdMovBanco', IdMovBanco);
                     datarep.append('FechaAsiento', FechaAsiento);
                     
                     var xhrrep = new XMLHttpRequest();

                     xhrrep.open("POST", "FrmRemesas_Contabilidad.aspx?flag=" + opcion + "&IdMovBanco=" + IdMovBanco + "&FechaAsiento=" + FechaAsiento, true);
                     xhrrep.onreadystatechange = function () {
                        if (xhrrep.readyState == 4 && xhrrep.status == 200) {                    
                            // fun_lista(xhrrep.responseText);
                        }
                    }
                    xhrrep.send(datarep);

               
	            return false;
            }
 
            else 
            {
                alert("No se guardo ningun dato.");
                return false;
            }


}
     

//-----------------------------------------------------------------------------------------


        function crearTablaConsulta() {
            var nRegistros = filas.length;

            var cabeceras = ["Marcar", "IdMovBanco", "NombreBanco", "CuentaBancaria", "cuentaContable", "CuentaCaja", "Glosa", "mban_FechaMov", "mban_Monto", "FechEmisReciboEgreso", "NroVoucherContable", "FechaAsiento"];
            var nCabeceras = cabeceras.length;
            var contenido = "<table><thead>";
            contenido += "<tr class='GrillaHeader'>";
            for (var j = 0; j < nCabeceras; j++) {

                contenido += "<th>";
                contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                contenido += "</th>";
            }
            contenido += "</tr></thead><tbody id='tbDocumentosConsulta' class='GrillaRow'></tbody>";
            contenido += "</table>";
            var div = document.getElementById("GrillaMovBancos");
            div.innerHTML = contenido;
        }


        function listarcombos() {
            var TipoServicio = [];
            var nRegistros = data1.length;
            var campos2;
            var TipoServ;
            for (var i = 0; i < nRegistros; i++) {
                campos2 = data1[i].split("|");
                {
                    TipoServ = campos2[0];
                    TipoServ += "|";
                    TipoServ += campos2[1];
                    TipoServicio.push(TipoServicio);
                }
            }
            crearCombo(data1, "cboBanco", "|");
        }


        function crearCombo(lista, nombreCombo, separador, nombreItem, valorItem) {
            var contenido = "";
            if (nombreItem != null && valorItem != null) {
                contenido += "<option value='";
                contenido += valorItem;
                contenido += "'>";
                contenido += nombreItem;
                contenido += "</option>";
            }
            var nRegistros = lista.length;
            if (nRegistros > 0) {
                var campos;
                for (var i = 0; i < nRegistros; i++) {
                    campos = lista[i].split(separador);
                    contenido += "<option value='";
                    contenido += campos[0];
                    contenido += "'>";
                    contenido += campos[1];
                    contenido += "</option>";
                }
            }
            var cbo = document.getElementById(nombreCombo);
            if (cbo != null) cbo.innerHTML = contenido;

        }


    function ConsultaCombos() {

      var opcion = "llenarcomboBanco";
      var IdEstadoc = 1;
      var datacombob = new FormData();
      datacombob.append('flag', 'llenarcomboBanco');
      datacombob.append('IdEstadoc', IdEstadoc);

      var xhrrep = new XMLHttpRequest();
      xhrrep.open("POST", "FrmRemesas_Contabilidad.aspx?flag=" + opcion + "&IdEstadoc=" + IdEstadoc, true);
      xhrrep.onreadystatechange = function () {
          if (xhrrep.readyState == 4 && xhrrep.status == 200) {
              mostrarListasCombos(xhrrep.responseText);
          }
      }
      xhrrep.send(datacombob);
      return false;
  }

  function mostrarListasCombos(rptacombo) {
      if (rptacombo != "") {
          var listas2 = rptacombo;
          data1 = crearArray(listas2, "▼");
          listarcombos();
      }
      return false;
  }

  function crearArray(lista, separador) {
      var data = lista.split(separador);
      if (data.length === 1 && data[0] === "") data = [];
      return data;
  }

  </script>
</asp:Content>
  
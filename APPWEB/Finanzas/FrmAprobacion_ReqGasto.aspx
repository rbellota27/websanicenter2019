﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmAprobacion_ReqGasto.aspx.vb" Inherits="APPWEB.FrmAprobacion_ReqGasto" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda" >
                REQUERIMIENTO DE GASTO - APROBACIÓN</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight:bold">
                            Empresa:</td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server"  AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold" >
                            Tienda:</td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server"   >
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold">
                            Área:</td>
                        <td>
                            <asp:DropDownList ID="cboArea" runat="server"  >
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold" >
                            Aprobación:</td>
                        <td>
                            <asp:DropDownList ID="cboOpcion_Aprobacion" runat="server">
                            <asp:ListItem Value="0" Selected="True">Por Aprobar</asp:ListItem>
                            <asp:ListItem Value="1" >Aprobados</asp:ListItem>
                            <asp:ListItem Value="2" >Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="70px" ToolTip="Buscar" CssClass="btnBuscar"
                            OnClientClick="this.disabled=true;this.value='Procesando...'"  UseSubmitBehavior="false"/>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight:bold">
                            Fecha Inicio:</td>
                        <td colspan="8">
                        <table>
                        <tr>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha_Blank(this) );" Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender0" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaInicio">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaInicio" >
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Texto" style="font-weight:bold" >Fecha Fin:</td>
                        <td>
                           <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha_Blank(this) );" Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFechaFin_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaFin">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaFin" >
                            </cc1:CalendarExtender>
                        </td>
                        <td></td>
                        <td><asp:Label class="Texto" style="font-weight:bold" ID="lble" runat="server" Text="Monto S/"></asp:Label></td>
                        <td><asp:TextBox ID="txtMonto" runat ="server" Text="0.00" ></asp:TextBox></td>
                        <td></td>
                        <td></td>
                        <td> <asp:Label class="Texto" style="font-weight:bold" id="lbltotalx" runat ="server" Text ="Total :"></asp:Label></td>
                        <td> <asp:Label class="Texto" style="font-weight:bold" id="valortotal" runat ="server" Text =""></asp:Label></td>
                        </tr>
                        </table>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_DocumentoReqGasto" runat="server" AutoGenerateColumns="False" 
                    Width="100%">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                          <asp:TemplateField ShowHeader="False" HeaderText="opciones">
                            <ItemTemplate>
                                <asp:Button ID="btnAprobar" runat="server" 
                                    Text="Aprobar" OnClientClick="return( valOnClick_btnGuardar());" CommandArgument='<%# Container.DataItemIndex%>'  CommandName="Insert"/>
                                      <asp:Button ID="btnDesaprobar" runat="server" 
                                    Text="Desaprobar" OnClientClick="return( valOnClick_btnGuardar());" CommandArgument='<%# Container.DataItemIndex%>' CommandName="InserDes" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TipoDocumento" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" HeaderText="Documento" 
                            ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px" 
                            ItemStyle-HorizontalAlign="Center" >
                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Height="25px"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Documento" 
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                            <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                            <asp:HiddenField ID="hddIdDocumento" runat="server" 
                                                Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                              <asp:HiddenField ID="hddIdPersona" runat="server"  Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                            <asp:HiddenField ID="hddNumDocu" runat="server" 
                                                Value='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>' />                                       
                                         <asp:HiddenField ID="hddsupervisor" runat="server" 
                                         Value='<%#DataBinder.Eval(Container.DataItem,"Supervisor")%>'/>

                                        
                                         <asp:HiddenField ID="hddbeneficiario" runat="server" 
                                         Value='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'/>
                                        
                                         <asp:HiddenField ID="hddestadoreque" runat="server" 
                                         Value='<%#DataBinder.Eval(Container.DataItem,"est_reque")%>'/>
                            </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" 
                                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" 
                                                HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center" >
                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FechaVcto" DataFormatString="{0:dd/MM/yyyy}" 
                                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" 
                                                HeaderText="Fecha Vcto." ItemStyle-HorizontalAlign="Center" >
                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderStyle-Height="25px" 
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" 
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                                <asp:Label ID="lblMonto" runat="server" Font-Bold="true" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                                </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                            
                                            <asp:BoundField DataField="DescripcionPersona"  ItemStyle-Font-Bold="true"
                                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" 
                                                HeaderText="Beneficiario" ItemStyle-HorizontalAlign="Center" >
                                                
                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center" Font-Bold="True"></ItemStyle>
                                            </asp:BoundField>
                                                
                                               <asp:TemplateField HeaderStyle-Height="25px" 
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Aprobado" 
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>

                                                                <asp:CheckBox ID="chb_Aprobado" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Aprobado")%>'  Enabled="false" />

                                                </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>                                                                      
                                          <asp:BoundField DataField="est_reque"
                                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" 
                                                HeaderText="Estado Reque" ItemStyle-HorizontalAlign="Center" >
                                            
                                                   <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            
                                           <%--     <asp:TemplateField HeaderStyle-Height="25px" 
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Estado Req." 
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="est_reque" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"est_reque")%>'/>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>    --%>
                                            
                                            
                                              <asp:BoundField DataField="FechaAprobacion"
                                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" 
                                                HeaderText="Fecha / Hora" ItemStyle-HorizontalAlign="Center" >
                                            
                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            
                     <asp:TemplateField HeaderStyle-Height="25px" 
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="" 
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                                <asp:ImageButton ID="btnMostrarDetalle" runat="server" 
                                                                    ImageUrl="~/Imagenes/search_add.ico"  OnClientClick=" return( valOnClick_btnMostrarDetalle(this)  ); "
                                                                    ToolTip="Visualizar Detalle" Width="20px" />
                                                </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" HeaderText="Impresión">
                            <ItemTemplate>
                                <asp:Button ID="btnimprimir" runat="server" 
                                    Text="Imprimir" OnClientClick="return(valOnClick_btnImprimir(this));" />
                            </ItemTemplate>
                        </asp:TemplateField>
                                                
                         <asp:TemplateField ShowHeader="False" HeaderText="Id Autog.">
                            <ItemTemplate>
                                <asp:Label ID="iddocumentox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                      
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                    </td>
                            </tr>
                            <tr>
                            
                                 
                                
                                <td style="height: 20px">
                                    </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                              <td>
                                    <asp:HiddenField ID="hddIdPersona" runat="server" Value="" />
                                </td>
                                <td>
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" Value="" />
                                </td>
                                <td>
                                  <asp:HiddenField ID="hddNumDocu" runat="server" Value=""/>
                                </td>
                                  <td>
                                  <asp:HiddenField ID="hddbeneficiario" runat="server" Value=""/>
                                </td>
                                 <td>
                                  <asp:HiddenField ID="hddsupervisor" runat="server" Value=""/>
                                </td>
                                <td>  <asp:HiddenField ID="hddestadoreque" runat="server" Value=""/></td>
                                
                            </tr>
                        </table>
                          <div  id="capaAprobarReqGasto"  
                            style="border: 3px solid blue; padding: 8px; width:680px; height:auto; position:absolute; background-color:white; z-index:2; display :none; top: 160px; left: 150px;">
                                                    <table style="width: 100%;">
                                                    <tr><td align="right">
                                                           <asp:ImageButton ID="btnCerrar_capaAprobarReqGasto" runat="server"  ToolTip="Cerrar"
                                                                    ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaAprobarReqGasto')   );" />   
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                          <asp:Panel ID="Panel_Aprobacion" runat="server">
                                <table width="100%" >
                            <tr>
                            <td class="SubTituloCelda">DATOS DE APROBACIÓN</td>
                            </tr>
                            <tr>
                            <td>
                            <table>
                            <tr>
                            <td>
                                <asp:RadioButtonList ID="rdbAutorizado" runat="server" 
                                    RepeatDirection="Horizontal" TextAlign="Right" CssClass="Texto" 
                                    Font-Bold="true" AutoPostBack="True" >
                                <asp:ListItem  Value="1" >Sí</asp:ListItem>
                                <asp:ListItem  Value="0" Selected="True" >No</asp:ListItem>            
                                </asp:RadioButtonList>
                            </td>
                            <td style="width:30px" ></td>
                            <td class="Texto" style="font-weight:bold">Fecha Aprobación:</td>
                            <td>
                            <asp:TextBox ID="txtFechaAprobacion" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Width="180px" runat="server"></asp:TextBox>
                            </td>
                            <td  style="width:30px"  ></td>
                            <td>
                                <asp:CheckBox ID="chbEnviarCxP" runat="server" Text="Enviar a Cuenta por Pagar" CssClass="Texto" Font-Bold="true" Enabled="false" /></td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            </table>
                                </asp:Panel>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <td style="text-align: center">
                                                    <table>
                                                    <tr>
                                                    <td>
                                                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="70px" ToolTip="Guardar" OnClientClick="return(  valOnClick_btnGuardar()  );" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="70px" ToolTip="Cerrar" OnClientClick=" return( offCapa('capaAprobarReqGasto') ); " />
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    </td>
                                                    </tr>
                                     </table>                                  
                            </div>       
        <script language="javascript"  type="text/javascript" >
        
                  function ReplaceTags(xStr){
                   var regExp = /<\/?[^>]+>/gi;
                    xStr = xStr.replace(regExp,"");
                    return xStr;
                  }
                  
                  function valOnClick_btnImprimir(control) {
                    var IdDocumento = "";
                    var row = control.parentNode.parentNode;
                        var rowIndex = row.rowIndex - 1;
                        var  hddIdDocumento = row.cells[12].innerHTML;
                        IdDocumento = ReplaceTags(hddIdDocumento);
                        
                       var iddoc = parseInt(IdDocumento);

                     window.open('../../Caja/Reportes/Visor1.aspx?iReporte=9&IdDocumento=' + iddoc, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                        return false;
        } 

            function valOnClick_btnGuardar() {                
                return confirm('Desea continuar con la Operación ?');
            }

            function valOnClick_btnMostrarDetalle(control) {
            
                var grilla = document.getElementById('<%=GV_DocumentoReqGasto.ClientID%>');
                var IdDocumento = 0;
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        var boton = rowElem.cells[10].children[0];
                        var hddIdDocumento = rowElem.cells[2].children[1];

                        if (control.id == boton.id) {
                            IdDocumento = parseInt(hddIdDocumento.value);
                            break;
                        }
                        
                        
                    }
                }

                window.open('FrmRequerimientoGasto_V2.aspx?IdDocumento=' + IdDocumento, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                return false;
            }
        
        </script>       
</asp:Content>

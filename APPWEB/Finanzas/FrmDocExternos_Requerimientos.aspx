﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmDocExternos_Requerimientos.aspx.vb" Inherits="APPWEB.FrmDocExternos_Requerimientos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    
    <table class="style1">
        <tr>
            <td class="TituloCelda" style="width: 950px">
                Reporte de Información de Documentos Externos
            </td>
        </tr>
        <tr>
            <td style="width: 950px">
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold; width: 150px;">
                            Documento :
                        </td>
                        <td style="width: 250px;">
                            <asp:DropDownList ID="cboDocumento" runat="server" AutoPostBack="true" CssClass="LabelRojo"
                                Font-Bold="true" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold; width: 150px;">
                            Proveedor:
                        </td>
                        <td style="width: 250px;">
                            <asp:DropDownList ID="cboProveedor" runat="server" AutoPostBack="true" CssClass="LabelRojo"
                                Font-Bold="true" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td style="color: #0000FF; font-weight: bold; width: 150px;">
                            <asp:CheckBox ID="Natural" runat="server" AutoPostBack="True" Text="N" TextAlign="Right" />
                        </td>
                        <td style="width: 350px; text-align: left">
                            <input id="btnBuscar" style="width: 190px" type="button" value="Buscar" onclick="return btnBuscar_onclick(this)" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold; width: 85px; background-color: #FFFFCC;">
                            Serie :
                        </td>
                        <td style="background-color: #FFFFCC; width: 42px;">                            
                            <asp:TextBox ID="txtSerie" runat="server" style="width: 130px" />
                        </td>
                        <td class="Texto" style="font-weight: bold; width: 85px; background-color: #FFFFCC;">
                            Codigo :
                        </td>
                        <td style="background-color: #FFFFCC">                           
                             <asp:TextBox ID="txtCodigo" runat="server" style="width: 130px" />
                        </td>
                        <td class="Texto" style="font-weight: bold; width: 85px; background-color: #FFFFCC;">
                            Año :
                        </td>
                        <td style="background-color: #FFFFCC">
                            
                            <asp:DropDownList ID="cboAnio" runat="server" AutoPostBack="true" CssClass="LabelRojo"
                                Font-Bold="true"  style="width: 190px">
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px; width: 870px;" colspan="6">
                            <div id="CapaGrillaDetalle">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
    </table>

     <script language="javascript"  type="text/javascript" >

         function btnBuscar_onclick(objeto) {
             ConsultarRegistros(objeto);
         }


         function ConsultarRegistros(objeto) {

             var opcion = "ConsultaDocumentos";

             var tipoDoc = document.getElementById('<%=cboDocumento.ClientID %>').value;
             var proveedor = document.getElementById('<%=cboProveedor.ClientID %>').value;
             var serie = document.getElementById('<%= txtSerie.ClientID%>').value;
             var codigo = document.getElementById('<%= txtCodigo.ClientID%>').value;
             var anio = document.getElementById('<%=cboAnio.ClientID %>').value;


             var periodo = anio;


             var datarep = new FormData();
             datarep.append('flag', 'ConsultaDocumentos');
             datarep.append('tipoDoc', tipoDoc);
             datarep.append('proveedor', proveedor);
             datarep.append('serie', serie);
             datarep.append('codigo', codigo);
             datarep.append('anio', anio);

             var xhr = new XMLHttpRequest();
             xhr.open("POST", "FrmDocExternos_Requerimientos.aspx?flag=" + opcion + "&tipoDoc=" + tipoDoc + "&proveedor=" + proveedor + "&serie=" + serie + "&codigo=" + codigo + "&anio=" + anio, true);
             xhr.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
             xhr.onloadend = function () { objeto.disabled = false; objeto.value = "Consultar"; }

             xhr.onreadystatechange = function () {
                 if (xhr.readyState == 4 && xhr.status == 200) {
                     fun_lista(xhr.responseText);
                 }
             }
             xhr.send(null);
             return false;
         }


         function fun_lista(lista) {
             filas = lista.split("▼");
             crearTabla();
             crearMatriz();
             mostrarMatriz();
             configurarFiltros();
         }



         function crearTabla() {
             var nRegistros = filas.length;
             var cabeceras = ["IdDocumento", "Serie", "Codigo", "TipoDoc", "FechaEmision", "nroVoucher", "DocTotal", "Montoafecto", "MontoInafecto", "Amortizado", "Deuda", "Proveedor", "Usuario", "Requerimiento"];
             var nCabeceras = cabeceras.length;
             var contenido = "<table><thead>";
             contenido += "<tr class='GrillaHeader'>";
             for (var j = 0; j < nCabeceras; j++) {
                 contenido += "<th>";
                 contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                 contenido += "</th>";
             }
             contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
             contenido += "</table>";
             var div = document.getElementById("CapaGrillaDetalle");
             div.innerHTML = contenido;
         }



         function crearMatriz() {
             matriz = [];
             var nRegistros = filas.length;
             var nCampos;
             var campos;
             var c = 0;
             var exito;
             var textos = document.getElementsByClassName("texto");
             var nTextos = textos.length;
             var texto;
             var x;
             for (var i = 0; i < nRegistros; i++) {
                 exito = true;
                 campos = filas[i].split("|");
                 console.log(campos);
                 nCampos = campos.length;
                 for (var j = 0; j < nTextos; j++) {
                     texto = textos[j];
                     x = j;

                     if (texto.value.length > 0) {
                         exito = campos[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                     }


                     if (!exito) break;
                 }
                 if (exito) {
                     matriz[c] = [];
                     for (var j = 0; j < nCampos; j++) {
                         if (isNaN(campos[j])) matriz[c][j] = campos[j];

                         else matriz[c][j] = campos[j];
                     }
                     c++;
                 }
             }
         }


         function mostrarMatriz() {
             var nRegistros = matriz.length;
             console.log(matriz);
             var contenido = "";
             if (nRegistros > 0) {
                 var nCampos = matriz[0].length;
                 for (var i = 0; i < nRegistros; i++) {


                     for (var j = 0; j < nCampos; j++) {
                         var cadenaid
                         var cadenareq
                         cadenaid = '' + matriz[i][17] + '';
                         cadenareq = '' + matriz[i][16] + '';
                         contenido += "<td>";
                        

                         if (j >= 0) {
                             contenido += matriz[i][j];
                         }
                         contenido += "</td>";

                     }
                     contenido += "</tr>";
                 }
             }
             var spnMensaje = document.getElementById("nroFilas");
             var tabla = document.getElementById("tbDocumentos");
             tabla.innerHTML = contenido;
         }


         function configurarFiltros() {
             var textos = document.getElementsByClassName("texto");
             var nTextos = textos.length;
             var texto;
             for (var j = 0; j < nTextos; j++) {
                 texto = textos[j];
                 texto.onkeyup = function () {
                     crearMatriz();
                     mostrarMatriz();
                 }
             }
         }



     </script>
</asp:Content>

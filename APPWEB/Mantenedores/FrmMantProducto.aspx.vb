﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Partial Public Class FrmMantProducto
    Inherits System.Web.UI.Page

#Region " ++++++++++++++++++++ Variables"
    Private listaUMedida As List(Of Entidades.ProductoUMView)
    Private listaMagnitud As List(Of Entidades.Magnitud)
    Private objScript As New ScriptManagerClass
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Private objCombo As New Combo
    Private objNegUsu As New Negocio.UsuarioView
    Private listaDetalle As List(Of Entidades.DetalleDocumentoView)
    Private listaDetalleRelacionado As List(Of Entidades.DetalleDocumento)
    Private listaDetalleDocRel_AddProd As List(Of Entidades.DetalleDocumento)
    Private listaProductoView As List(Of Entidades.ProductoView)
    Private listaDetalleProductov As List(Of Entidades.ProductoView)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private objproducto As New Negocio.Producto
    Private listaKit As List(Of Entidades.Kit)
    Dim drop As Combo
    Enum operativo
        none = 0
        Insert = 1
        Update = 2
    End Enum

#End Region

#Region " Al Iniciar el formulario"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ValidarPermisos()
            inicializarFrm()
            ConfigurarDatos()
            cargar_kit(False)
            Me.btnEliminar.Visible = False
        End If
    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / NRO DIAS VIGENCIA / REGISTRAR CLIENTE / EDITAR CLIENTE
        '**** HABILITAR GRILLA UM / HABILITAR GRILLA EQUIVALENCIA
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {130, 131, 2102866601, 2102866602})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* BUSCAR
            Me.M_B_cmbLinea.Enabled = True
        Else
            Me.M_B_cmbLinea.Enabled = False
        End If

        If listaPermisos(2) > 0 Then    '******HABILITAR AGREGAR U.M
            Me.PanelUM.Enabled = True
        Else
            Me.PanelUM.Enabled = False            
        End If

        If listaPermisos(3) > 0 Then    '*****HABILITAR AGREGAR EQUIVALENCIA
            Me.Panel_ProductoMedida.Enabled = True
        Else
            Me.Panel_ProductoMedida.Enabled = False
        End If
    End Sub

    Private Sub inicializarFrm()
        drop = New Combo
        mostrarMensaje(False, "")
        hddModo.Value = CStr(operativo.none)
        drop.llenarCboTipoExistencia(cmbTipoExistencia)

        Panel_M_1.Visible = False
        Panel_M_2.Visible = True
        listaUMedida = New List(Of Entidades.ProductoUMView)
        listaMagnitud = New List(Of Entidades.Magnitud)
        Session.Add("listaUM", listaUMedida)
        Session.Add("listaMagnitud", listaMagnitud)
        cargarControles()
        cargarPanelBusqueda_M()
        verBotonesControl()
        M_B_lbl1.Visible = False : M_B_cmbSubLinea.Visible = False : PanelFiltro1.Visible = False
        'Me.Panel_Kit.Visible = False
        btnSaveNewFromUpdate.Visible = False
        Panel_B.Visible = False
    End Sub
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    'kit
    Private Sub cargar_kit(ByVal valor As Boolean)

        If valor = True Then
            Me.Panel_Kit.Visible = True
            chb_HabilitarKit.Enabled = True
            chb_HabilitarKit.Visible = True
            btnBuscarComponenteKit.Visible = True
            btnBuscarComponenteKit.Enabled = True
        Else
            Me.Panel_Kit.Visible = False
            Me.listaKit = New List(Of Entidades.Kit)
            setListaKit(Me.listaKit)

            Me.GV_ComponenteKit.DataSource = Me.listaKit
            Me.GV_ComponenteKit.DataBind()

            chb_HabilitarKit.Enabled = False
            chb_HabilitarKit.Visible = False
            btnBuscarComponenteKit.Visible = False
            btnBuscarComponenteKit.Enabled = False

        End If

    End Sub

#End Region

    Private Sub cargarControles()
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxUMedida(M_cmbUMedida)
        nComboBox.llenarComboBoxTipoProd(M_cmbTipoProd)
        Dim objCombo As New Combo
        objCombo.LlenarCboMagnitud(Me.cmbMagnitud)


        drop = New Combo
        drop.llenarCboTipoExistencia(cbo_TipoExistencia)
        drop.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cbo_TipoExistencia.SelectedValue), True)
        drop.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cmbSubLinea_AddProd, CInt(cmbLinea_AddProd.SelectedValue), CInt(cbo_TipoExistencia.SelectedValue), True)
        drop.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

    End Sub
    Private Sub verBotonesControl()
        Select Case hddModo.Value
            Case CStr(operativo.none)
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
            Case Else
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
        End Select
    End Sub
    Private Sub HabilitarTipoProducto(ByVal habilitar As Boolean)
        cmbTipoExistencia.Enabled = habilitar
    End Sub
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
        Me.btnEliminar.Visible = False
    End Sub

    Private Sub valOnClick_btnNuevo()
        Try
            verFrmNuevo()
            ClearAtributos()
            PanelGenCodigo.Enabled = True

            Me.listaKit = New List(Of Entidades.Kit)
            setListaKit(Me.listaKit)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub limpiarControles()
        drop = New Combo
        drop.LlenarProveedorxLinea(CInt(0), CInt(cmbTipoExistencia.SelectedValue), Me.M_cmbProveedor, True)

        M_txtCodigo.Text = ""
        M_txtDescripcion.Text = ""
        M_txtCodBarras.Text = ""
        M_txtCodBarrasFabricante.Text = ""
        M_txtCodigoAnterior.Text = ""


        M_txtStockMin.Text = ""
        M_txtStockMax.Text = ""
        Me.txtStockReposición.Text = ""
        Me.listaUMedida = getListaUM()
        Me.listaUMedida.Clear()
        setListaUM(Me.listaUMedida)
        DGV_UM.DataSource = Nothing
        DGV_UM.DataBind()
        DGV_Magnitud.DataSource = Nothing
        DGV_Magnitud.DataBind()

        MostrarGrillaTipoTabla(CInt(0), CInt(0), "OR", dgvOrigen)
        MostrarGrillaTipoTabla(CInt(0), CInt(0), "CO", dgvCodigo)
        MostrarGrillaTipoTabla(CInt(0), CInt(0), "AT", dgvAtributos)
        MostrarGrillaTipoTabla(CInt(0), CInt(0), "SU", dgvSinUso)
        hddCodAtributos.Value = ""
        txtCodOrigen.Text = ""
        txtCodCodigo.Text = ""
        hddIndexGrillaOR.Value = "0"
        hddIndexSubGrillaOR.Value = "0"
        hddIndexGrillaCO.Value = "0"
        hddIndexSubGrillaCO.Value = "0"
        hddIndexGrillaAT.Value = "0"
        hddIndexSubGrillaAT.Value = "0"
        hddIndexGrillaSU.Value = "0"
        hddIndexSubGrillaSU.Value = "0"
        hddValCodigo.Value = ""

        Me.GV_ComponenteKit.DataSource = Nothing
        Me.GV_ComponenteKit.DataBind()

        Me.chb_HabilitarKit.Checked = False

        Me.rdbOpcion_CalculoCostoFlete.SelectedValue = "NAN"
        Me.rb_flagTG.SelectedValue = "0"
        Me.txtPlazoEntrega.Text = "0"
        Me.txtVolMinCompra.Text = "0"
        Me.txtReposicioWeeks.Text = "0"

        ViewState.Add("ProductoOrigen", Nothing)
        ViewState.Add("ProductoCodigo", Nothing)

    End Sub
    Private Sub verFrmNuevo()
        hddModo.Value = CStr(operativo.Insert)
        mostrarMensaje(False, "")
        Panel_M_1.Visible = True
        Panel_M_2.Enabled = False
        cargarDatosM()
        verBotonesControl()
        DGV_UM_EDITAR.Visible = False
        DGV_UM.Visible = True
        HabilitarTipoProducto(False)
        Panel_B.Enabled = False
        verBotonesControl()
        M_txtCodProveedor.Text = ""
        hddvalUM.Value = "0"

        btnSaveNewFromUpdate.Visible = False
        'Me.Panel_Kit.Visible = True

    End Sub
    Private Sub cargarDatosUMedida(ByVal combo As DropDownList)
        Dim nUM As New Negocio.UnidadMedida
        combo.DataSource = nUM.SelectCbo
        combo.DataBind()
    End Sub
    Private Sub cargarDatosMagnitud(ByVal cbo As DropDownList)
        Dim nUM As New Negocio.Magnitud
        cbo.DataSource = nUM.SelectCbo
        cbo.DataBind()
    End Sub
    Private Sub cargarDatosContenido(ByVal comboBox As DropDownList, ByVal idSubLinea As Integer)
        Dim nProducto As New Negocio.Producto
        Dim lista As List(Of Entidades.Producto) = nProducto.SelectxSubLinea_Cbo(idSubLinea)
        Dim objProd As New Entidades.Producto
        objProd.Descripcion = "-----"
        objProd.Id = 0
        lista.Insert(0, objProd)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosM()
        drop = New Combo
        drop.llenarCboLineaxTipoExistencia(M_cmbLinea, CInt(cmbTipoExistencia.SelectedValue), True)
        drop.LlenarCboSubLineaxIdLinea(M_cmbSubLinea, CInt(M_cmbLinea.SelectedValue), True)
        cargarDatosEstadoProd(M_cmbEstadoProd)
        LlenarCboProveedor(M_cmbProveedor, True)
        cargarDatosUMedida(M_cmbUMedida)
        cargarDatosTipoProd(M_cmbTipoProd)
        cargarDatosMagnitud(cmbMagnitud)
    End Sub
    Private Sub cargarDatosFormaProd(ByVal comboBox As DropDownList)
        Dim nFormaProd As New Negocio.FormaProducto
        Dim lista As List(Of Entidades.FormaProducto) = nFormaProd.SelectCbo
        Dim objFP As New Entidades.FormaProducto
        objFP.Nombre = "-----"
        objFP.Id = 0
        lista.Insert(0, objFP)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosTipoProd(ByVal comboBox As DropDownList)
        Dim nTipoProd As New Negocio.TipoProducto
        Dim lista As List(Of Entidades.TipoProducto) = nTipoProd.SelectCbo
        Dim objTP As New Entidades.TipoProducto
        objTP.Nombre = "-----"
        objTP.Id = 0
        lista.Insert(0, objTP)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosSabor(ByVal comboBox As DropDownList)
        Dim nSabor As New Negocio.Sabor
        Dim lista As List(Of Entidades.Sabor) = nSabor.SaborSelectCbo
        Dim objSabor As New Entidades.Sabor
        objSabor.Descripcion = "-----"
        objSabor.Id = 0
        lista.Insert(0, objSabor)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosTalla(ByVal comboBox As DropDownList)
        Dim nTalla As New Negocio.Talla
        Dim lista As List(Of Entidades.Talla) = nTalla.SelectAllActivo_Cbo
        Dim objTalla As New Entidades.Talla
        objTalla.Nombre = "-----"
        objTalla.Id = 0
        lista.Insert(0, objTalla)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosTamanio(ByVal comboBox As DropDownList)
        Dim nTamanio As New Negocio.Tamanio
        Dim lista As List(Of Entidades.Tamanio) = nTamanio.SelectAllActivo_Cbo
        Dim objTamanio As New Entidades.Tamanio
        objTamanio.Nombre = "-----"
        objTamanio.Id = 0
        lista.Insert(0, objTamanio)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosLinea(ByVal comboBox As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxLinea(comboBox)
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectActivoxTipoExistencia(CInt(cmbTipoExistencia.SelectedValue))
        If hddModo.Value <> CStr(operativo.Insert) And hddModo.Value <> CStr(operativo.Update) Then
            Dim objLinea As New Entidades.Linea
            objLinea.Id = 0
            objLinea.IdTipoExistencia = 0
            objLinea.Descripcion = "-----"
            lista.Insert(0, objLinea)
        End If
        comboBox.DataSource = lista
        comboBox.DataTextField = "Descripcion"
        comboBox.DataValueField = "Id"
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosLinea_Cont(ByVal comboBox As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxLinea(comboBox)
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo
        Dim objLinea As New Entidades.Linea
        objLinea.Id = 0
        objLinea.IdTipoExistencia = 0
        objLinea.Descripcion = "-----"
        lista.Insert(0, objLinea)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal comboBox As DropDownList, ByVal idTipoExistencia As Integer, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxSubLinea(comboBox)
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectActivoxLineaxTipoExistencia(idLinea, idTipoExistencia)
        If hddModo.Value <> CStr(operativo.Insert) And hddModo.Value <> CStr(operativo.Update) Then
            Dim objSubLinea As New Entidades.SubLinea
            objSubLinea.Id = 0
            objSubLinea.IdLinea = 0
            objSubLinea.Nombre = "-----"
            lista.Insert(0, objSubLinea)
        End If
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea_Cont(ByVal comboBox As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxSubLinea(comboBox)
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        Dim objSubLinea As New Entidades.SubLinea
        objSubLinea.Id = 0
        objSubLinea.IdLinea = 0
        objSubLinea.Nombre = "-----"
        lista.Insert(0, objSubLinea)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosFabricante(ByVal comboBox As DropDownList)
        Dim nFabricante As New Negocio.Fabricante
        Dim lista As List(Of Entidades.Fabricante) = nFabricante.SelectAllActivo
        Dim objFab As New Entidades.Fabricante
        objFab.Id = 0
        objFab.NombreLargo = "-----"
        lista.Insert(0, objFab)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosEstadoProd(ByVal comboBox As DropDownList)
        Dim nEstadoProd As New Negocio.EstadoProd
        Dim lista As List(Of Entidades.EstadoProd) = nEstadoProd.SelectAllActivo
        If hddModo.Value <> CStr(operativo.Insert) And hddModo.Value <> CStr(operativo.Update) Then
            Dim objEstadoP As New Entidades.EstadoProd
            objEstadoP.Id = 0
            objEstadoP.Descripcion = "-----"
            lista.Insert(0, objEstadoP)
        End If
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxEstadoProd(comboBox)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        verFrmInicio()
        Me.btnEliminar.Visible = False
    End Sub
    Private Sub verFrmInicio()
        cargar_kit(False)
        Session.Remove("listaMagnitud")
        Session.Remove("listaUM")
        'Session.Remove("listaUMedida_Editar")
        Me.listaMagnitud = New List(Of Entidades.Magnitud)
        Me.listaUMedida = New List(Of Entidades.ProductoUMView)
        setlistaMagnitud(listaMagnitud)
        setListaUM(listaUMedida)
        hddModo.Value = CStr(operativo.none)
        HabilitarTipoProducto(True)
        limpiarControles()
        verBotonesControl()
        Panel_B.Enabled = True
        Panel_B.Visible = False
        Panel_M_1.Visible = False
        Panel_M_2.Enabled = True
        Panel_M_2.Visible = True
        hddvalUM.Value = "0"
        btnSaveNewFromUpdate.Visible = False
        'Me.Panel_Kit.Visible = False
    End Sub
    Private Sub guardarProducto(Optional ByVal NewInsert As Integer = operativo.none)
        Try
            Dim idprod As Integer
            If M_txtCodigo.Text = "" Then
                idprod = 0
            Else
                idprod = CInt(M_txtCodigo.Text)
            End If

            'Dim nProducto As New Negocio.Producto
            'Dim objProd As Entidades.Producto = nProducto.ProductoValidadMovAlmacenIdProducto(CInt(idprod))
            'If CInt(objProd.Id) >= 1 Then
            'objScript.mostrarMsjAlerta(Me, "No puede modificar el producto porque tiene movimiento en almacen.")
            'ElseIf CInt(objProd.Id) < 1 Then
            'ClearAtributos()
            'ValidarAutogenerado("1", txtCodOrigen.Text, txtCodCodigo.Text, hddCodAtributos.Value)
            'If hddValCodigo.Value <> "" Then
            '    objScript.mostrarMsjAlerta(Me, "El código autogenerado para el producto ya existe.")
            'End If
            'If hddValCodigo.Value = "" Then
            '    obtenerDatos()
            'End If
            'End If

            Dim umprin As Boolean
            If NewInsert = operativo.Insert Then
                hddModo.Value = CStr(operativo.Insert)
            End If

            Select Case CInt(hddModo.Value)
                Case operativo.Insert
                    umprin = True
                Case operativo.Update
                    umprin = True ' quitar esto y descomentar abajo

                    'For x As Integer = 0 To DGV_UM.Rows.Count - 1
                    '    Dim chkprin As CheckBox = CType(DGV_UM.Rows(x).Cells(3).FindControl("chbPrincipal"), CheckBox)
                    '    If CBool(chkprin.Checked) = True Then
                    '        If CInt(hddvalUM.Value) = CInt(DGV_UM.Rows(x).Cells(6).Text) Then
                    '           umprin = True
                    '        End If
                    '    End If
                    'Next
            End Select

            Select Case umprin
                Case True
                    ClearAtributos()
                    ValidarAutogenerado("1", txtCodOrigen.Text, txtCodCodigo.Text, hddCodAtributos.Value)
                    If hddValCodigo.Value <> "" Then
                        objScript.mostrarMsjAlerta(Me, "El código autogenerado para el producto ya existe.")
                    End If

                    If hddValCodigo.Value = "" Then
                        obtenerDatos()
                        hddvalUM.Value = "0"
                        Me.btnEliminar.Visible = False
                    End If
                Case False
                    objScript.mostrarMsjAlerta(Me, "No se puede modificar la unidad medida principal porque tiene movimientos en almacen.")
            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        guardarProducto()
    End Sub
    'Private Sub btnGuardarComoNvo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardarComoNvo.Click
    '    'para cambiar el modo de actualizacion a insercion
    '    guardarProducto(1)
    'End Sub


    Private Sub obtenerDatos()
        Try


            Dim objProd As New Entidades.Producto
            objProd.Descripcion = M_txtDescripcion.Text.Trim
            objProd.CodigoBarras = M_txtCodBarras.Text.Trim
            objProd.CodBarrasFabricante = M_txtCodBarrasFabricante.Text.Trim
            objProd.CodAntiguo = M_txtCodigoAnterior.Text.Trim

            objProd.IdProveedor = getSelectedValueComboBox(M_cmbProveedor)
            objProd.IdEstadoProd = getSelectedValueComboBox(M_cmbEstadoProd)
            objProd.StockMinimo = CInt(IIf(IsNumeric(M_txtStockMin.Text.Trim) = True, M_txtStockMin.Text.Trim, 0))
            objProd.StockMaximo = CInt(IIf(IsNumeric(M_txtStockMax.Text.Trim) = True, M_txtStockMax.Text.Trim, 0))
            objProd.StockReposicion = CInt(IIf(IsNumeric(Me.txtStockReposición.Text.Trim) = True, Me.txtStockReposición.Text.Trim, 0))
            objProd.IdTipoProducto = getSelectedValueComboBox(M_cmbTipoProd)
            objProd.IdSubLInea = CInt(M_cmbSubLinea.SelectedValue)
            objProd.CodProveedor = M_txtCodProveedor.Text.Trim
            objProd.NoVisible = CStr(txtCodOrigen.Text).Trim
            objProd.Codigo = CStr(txtCodCodigo.Text).Trim
            objProd.CodAtributos = CStr(hddCodAtributos.Value)
            objProd.Id = CInt(IIf(IsNumeric(M_txtCodigo.Text.Trim) = True, M_txtCodigo.Text.Trim, 0))
            objProd.Kit = Me.chb_HabilitarKit.Checked
            objProd.CalculoFlete = Me.rdbOpcion_CalculoCostoFlete.SelectedValue
            objProd.prod_SinUso = CStr(hddCodSinUso.Value)
            objProd.prod_AfectoPercepcion = CInt(Me.rblAfectoPercepcion.SelectedValue)
            objProd.flagTG = CInt(Me.rb_flagTG.SelectedValue)

            objProd.LeadTime = CDec(Me.txtPlazoEntrega.Text)
            objProd.VolMinCompra = CDec(Me.txtVolMinCompra.Text)
            objProd.ReposicionWeeks = CDec(Me.txtReposicioWeeks.Text)

            If (objProd.CalculoFlete <> "P" And objProd.CalculoFlete <> "U") Then
                objProd.CalculoFlete = Nothing
            End If

            If dgvCodigo.Rows.Count = 0 And dgvOrigen.Rows.Count = 0 And _
            dgvAtributos.Rows.Count = 0 And dgvSinUso.Rows.Count = 0 Then

                objProd.prod_Longitud_AutoGen = 9
                cargar_kit(True)

            Else
                cargar_kit(False)
            End If


            registrarMercaderia(objProd)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaUM_Save() As List(Of Entidades.ProductoUMView)

        Dim lista As List(Of Entidades.ProductoUMView) = getListaUM()
        Dim objPumPrincipal As New Entidades.ProductoUMView
        For i As Integer = 0 To lista.Count - 1

            If (lista(i).UnidadPrincipal) Then

                objPumPrincipal = lista(i)
                lista.RemoveAt(i)
                Exit For

            End If

        Next

        lista.Insert(0, objPumPrincipal)


        Return lista

    End Function
    Private Sub registrarMercaderia(ByVal objProd As Entidades.Producto)
        Try

            saveListaUMedida()
            actualizarListaMagnitud()
            actualizarListaKit()

            Dim nProducto As New Negocio.Producto

            Me.listaUMedida = obtenerListaUM_Save()

            Dim listaProductoMedida As List(Of Entidades.ProductoMedida) = obtenerListaProductoMedida()

            Dim listaProdTTV As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaProductoTTV()

            Me.listaKit = getListaKit()

            If (Not objProd.Kit) Then
                '***************** ELIMINAMOS LOS COMPONENTES DEL KIT
                Me.listaKit = New List(Of Entidades.Kit)
            End If

            Dim idusuario As Integer
            idusuario = CType(Session("IdUsuario"), Integer)

            Select Case CInt(hddModo.Value)
                Case operativo.Insert
                    If nProducto.InsertaProductoTipoMercaderia(idusuario, objProd, Me.listaUMedida, listaProductoMedida, listaProdTTV, Me.listaKit) Then

                        verFrmInicio()

                        objScript.mostrarMsjAlerta(Me, "El Producto se registró con éxito.")
                    Else
                        'objScript.mostrarMsjAlerta(Me, ex)
                        Throw New Exception
                    End If

                Case operativo.Update
                    If nProducto.ActualizaProductoTipoMercaderia(idusuario, objProd, Me.listaUMedida, listaProductoMedida, listaProdTTV, Me.listaKit) Then
                        verFrmInicio()

                        objScript.mostrarMsjAlerta(Me, "El Producto se actualizó con éxito.")
                    Else
                        'objScript.mostrarMsjAlerta(Me, "Problemas en la Operación.")
                        Throw New Exception
                    End If

            End Select
        Catch ex As Exception
            Throw ex
        End Try


    End Sub
    Private Sub mostrarMensaje(ByVal isVisible As Boolean, ByVal texto As String)
        lblMensaje.Visible = isVisible
        lblMensaje.Text = texto
    End Sub
#Region "MANEJADOR SESSION"
    Private Sub setlistaMagnitud(ByVal lista As List(Of Entidades.Magnitud))
        Session.Remove("listaMagnitud")
        Session.Add("listaMagnitud", lista)
    End Sub
    Private Function getlistaMagnitud() As List(Of Entidades.Magnitud)
        Return CType(Session.Item("listaMagnitud"), List(Of Entidades.Magnitud))
    End Function
    Private Sub setlistaMagnitud_Editar(ByVal lista As List(Of Entidades.Magnitud))
        Session.Remove("listaUMedida_Editar")
        Session.Add("listaUMedida_Editar", lista)
    End Sub
    Private Function getlistaMagnitud_Editar() As List(Of Entidades.Magnitud)
        Return CType(Session.Item("listaUMedida_Editar"), List(Of Entidades.Magnitud))
    End Function
#End Region
    Private Function getSelectedValueComboBox(ByVal comboBox As DropDownList) As Integer
        Dim id As Integer = 0
        If comboBox.SelectedValue <> "0" And comboBox.SelectedValue <> "" Then
            id = CInt(comboBox.SelectedValue)
        End If
        Return id
    End Function
    Protected Sub cmbTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTipoExistencia.SelectedIndexChanged
        DGV_B.DataBind()
        verFrmInicio()
        cargarDatosLinea(M_B_cmbLinea)
        cargarDatosSubLinea(M_B_cmbSubLinea, CInt(cmbTipoExistencia.SelectedValue), CInt(M_B_cmbLinea.SelectedValue))
    End Sub
    Private Sub cargarPanelBusqueda_M()
        cargarDatosLinea(M_B_cmbLinea)
        cargarDatosSubLinea(M_B_cmbSubLinea, CInt(cmbTipoExistencia.SelectedValue), CInt(M_B_cmbLinea.SelectedValue))

        cargarDatosEstadoProd(M_B_cmbEstado)
        M_B_txtDescripcion.Text = ""

    End Sub
    Protected Sub M_B_cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles M_B_cmbLinea.SelectedIndexChanged
        If CInt(M_B_cmbLinea.SelectedValue) = 0 Then
            hddModoVal.Value = "0"
        End If

        cargarDatosSubLinea(M_B_cmbSubLinea, CInt(cmbTipoExistencia.SelectedValue), CInt(M_B_cmbLinea.SelectedValue))

        M_B_lbl1.Visible = True : M_B_cmbSubLinea.Visible = True
        PanelFiltro1.Visible = True

        M_lblTextoBusq.Visible = False : M_B_txtDescripcion.Visible = False
        rdbCodNombre.SelectedIndex = -1 : M_B_txtDescripcion.Text = ""

        LlenarCboTipoTablaxIdSubLinea(cboTipoTablaBus, CInt(0), True)
    End Sub
    

    Protected Sub M_cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles M_cmbLinea.SelectedIndexChanged
        drop = New Combo
        drop.LlenarCboSubLineaxIdLinea(M_cmbSubLinea, CInt(M_cmbLinea.SelectedValue), True)
    End Sub

    Protected Sub btnAgregarUM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarUM.Click
        cargarDatosGrillaUMedida()
    End Sub
    Private Sub saveListaUMedida()

        Me.listaUMedida = getListaUM()

        For i As Integer = 0 To DGV_UM.Rows.Count - 1

            Dim chbEstado_Editar As CheckBox = CType(DGV_UM.Rows(i).Cells(5).FindControl("chbEstado_ProdEditar1"), CheckBox)
            With listaUMedida.Item(i)
                .Estado = CStr(IIf(chbEstado_Editar.Checked = True, "1", "0"))
            End With

            Me.listaUMedida(i).Equivalencia = CDec(CType(DGV_UM.Rows(i).Cells(2).FindControl("txtEquivalencia_UM"), TextBox).Text)

            Me.listaUMedida(i).UnidadPrincipal = CType(Me.DGV_UM.Rows(i).FindControl("chbPrincipal"), CheckBox).Checked
            Me.listaUMedida(i).Retazo = CType(Me.DGV_UM.Rows(i).FindControl("chbRetazo"), CheckBox).Checked

            listaUMedida(i).CodBarraXUm = CType(DGV_UM.Rows(i).FindControl("txtCodBarraXUm"), TextBox).Text

        Next



        setListaUM(listaUMedida)
    End Sub
    Private Sub cargarDatosGrillaUMedida()



        '******************** Actualizamos la lista
        saveListaUMedida()

        Me.listaUMedida = getListaUM() 'obtengo una lista dinamica

        '************* Agrego un elemento
        Dim objPUMView As New Entidades.ProductoUMView
        objPUMView.IdUnidadMedida = CInt(M_cmbUMedida.SelectedValue)
        objPUMView.NombreCortoUM = M_cmbUMedida.SelectedItem.ToString
        objPUMView.Estado = "1"
        objPUMView.CodBarraXUm = ""

        listaUMedida.Add(objPUMView)
        setListaUM(Me.listaUMedida)

        DGV_UM.DataSource = listaUMedida
        DGV_UM.DataBind()
    End Sub
    Private Function validarUM_Repetido() As Boolean
        Dim flag As Boolean = False
        Me.listaUMedida = getListaUM()
        For i As Integer = 0 To Me.listaUMedida.Count - 1
            If Me.listaUMedida.Item(i).IdUnidadMedida = CInt(M_cmbUMedida.SelectedValue) Then
                Return True
            End If
        Next
        Return flag
    End Function
    Private Sub eliminarRegistroListaUMedida(ByVal index As Integer)
        Me.listaUMedida = getListaUM()
        If index >= 0 And index < listaUMedida.Count Then
            listaUMedida.RemoveAt(index)
            DGV_UM.DataSource = listaUMedida
            DGV_UM.DataBind()
        End If
        setListaUM(Me.listaUMedida)
    End Sub

    Private Sub DGV_UM_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_UM.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim chbEstado_Editar As CheckBox
                Dim gvrow As GridViewRow = CType(e.Row.Cells(5).NamingContainer, GridViewRow)
                chbEstado_Editar = CType(gvrow.FindControl("chbEstado_ProdEditar1"), CheckBox)
                Me.listaUMedida = Me.getListaUM
                If chbEstado_Editar IsNot DBNull.Value Then
                    chbEstado_Editar.Checked = Me.listaUMedida.Item(e.Row.RowIndex).EstadoBool
                End If
                'Dim valorSeleccionado As String = DirectCast(e.Row.FindControl("txtEquivalencia_UM"), TextBox).Text
                Dim valorSeleccionado As String = Me.listaUMedida.Item(e.Row.RowIndex).NombreCortoUM
                'qweqwe
                If valorSeleccionado = "PZA" Or valorSeleccionado = "CAJ" Or valorSeleccionado = "M2" Then
                    DirectCast(e.Row.FindControl("txtEquivalencia_UM"), TextBox).Enabled = False
                Else
                    DirectCast(e.Row.FindControl("txtEquivalencia_UM"), TextBox).Enabled = True
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub DGV_UM_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_UM.SelectedIndexChanged
        '********** validando la eliminacion de unidades de medida
        Try
            saveListaUMedida()

            Me.listaUMedida = getListaUM()

            Dim objUtil As New Negocio.Util

            Select Case hddModo.Value

                Case "1"
                    eliminarRegistroListaUMedida(DGV_UM.SelectedIndex)
                Case "2"
                    If (objUtil.ValidarxDosParametros("DetalleDocumento", "IdProducto", M_txtCodigo.Text, "IdUnidadMedida", Me.listaUMedida.Item(DGV_UM.SelectedIndex).IdUnidadMedida.ToString) > 0) Then
                        objScript.mostrarMsjAlerta(Me, "La Unidad de Medida no puede ser eliminada porque existen detalles de documentos relacionados.")
                    Else
                        eliminarRegistroListaUMedida(DGV_UM.SelectedIndex)
                    End If
            End Select

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminación de la Unidad de Medida.")

        End Try
    End Sub
    Protected Sub DGV_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_B.SelectedIndexChanged
        'Dim IdProducto As Integer = CInt(DGV_B.SelectedDataKey.Value)
        Dim hddIdProducto As HiddenField = CType(DGV_B.SelectedRow.Cells(1).FindControl("hddIdProducto"), HiddenField)
        llenarEntidades()
        verFrmEditar(CInt(hddIdProducto.Value))
        PanelGenCodigo.Enabled = True

        Me.btnEliminar.Visible = True
        Me.rblAfectoPercepcion.SelectedValue = CStr(objproducto.productoAfectoSublineaForUpdate(CInt(Me.M_cmbSubLinea.SelectedValue), CInt(hddIdProducto.Value)))
    End Sub
    Private Sub verFrmEditar(ByVal IdProducto As Integer)
        hddModo.Value = CStr(operativo.Update)
        mostrarMensaje(False, "")
        Panel_M_1.Visible = True
        Panel_M_2.Enabled = False
        cargarDatosM()
        cargarDatosEditar_Merc(IdProducto)
        DGV_UM.Visible = True
        DGV_UM_EDITAR.Visible = False
        HabilitarTipoProducto(False)
        Panel_B.Enabled = False
        verBotonesControl()
        'Me.Panel_Kit.Visible = True
    End Sub
    Private Sub cargarDatosEditar_Merc(ByVal IdProducto As Integer)
        Dim objScript As New ScriptManagerClass

        Dim nProducto As New Negocio.Producto
        Dim objProd As Entidades.Producto = nProducto.ProductoMercaderiaSelectxIdProducto(IdProducto)

        If objProd IsNot Nothing Then

            M_txtCodigo.Text = objProd.Id.ToString
            M_txtDescripcion.Text = objProd.Descripcion
            M_txtCodBarras.Text = objProd.CodigoBarras
            M_txtCodBarrasFabricante.Text = objProd.CodBarrasFabricante
            M_txtCodigoAnterior.Text = objProd.CodAntiguo
            drop = New Combo
            drop.llenarCboLineaxTipoExistencia(M_cmbLinea, CInt(objProd.IdTipoExistencia), True)
            M_cmbLinea.SelectedValue = CStr(objProd.IdLinea)
            drop.LlenarCboSubLineaxIdLinea(M_cmbSubLinea, CInt(objProd.IdLinea), True)
            M_cmbSubLinea.SelectedValue = CStr(objProd.IdSubLInea)
            drop.LlenarProveedorxLinea(objProd.IdLinea, objProd.IdTipoExistencia, Me.M_cmbProveedor, True)
            LlenarCboProveedor(M_cmbProveedor, True)
            If Me.M_cmbProveedor.Items.FindByValue(objProd.IdProveedor.ToString) IsNot Nothing Then
                M_cmbProveedor.SelectedValue = objProd.IdProveedor.ToString
            End If
            M_txtCodProveedor.Text = objProd.CodProveedor.ToString
            M_cmbEstadoProd.SelectedValue = objProd.IdEstadoProd.ToString
            M_cmbTipoProd.SelectedValue = objProd.IdTipoProducto.ToString
            M_txtStockMin.Text = objProd.StockMinimo.ToString
            M_txtStockMax.Text = objProd.StockMaximo.ToString
            M_txtStockActual.Text = objProd.StockActual.ToString
            txtStockReposición.Text = objProd.StockReposicion.ToString
            txtCodOrigen.Text = objProd.NoVisible
            txtCodCodigo.Text = objProd.Codigo
            hddCodAtributos.Value = objProd.CodAtributos
            rb_flagTG.SelectedValue = CStr(objProd.flagTG)
            Me.txtPlazoEntrega.Text = CStr(Math.Round(objProd.LeadTime, 3))
            Me.txtVolMinCompra.Text = CStr(Math.Round(objProd.VolMinCompra, 3))
            Me.txtReposicioWeeks.Text = CStr(Math.Round(objProd.ReposicionWeeks, 3))

            hddcodigoCompleto.Value = objProd.NoVisible + "" + objProd.Codigo

            ViewState.Add("ProductoOrigen", Me.txtCodOrigen.Text.Trim)
            ViewState.Add("ProductoCodigo", Me.txtCodCodigo.Text.Trim)

            ListarSubLinea(objProd.IdSubLInea)

            ''****''
            MostrarGrillaTipoTabla(CInt(objProd.IdLinea), CInt(objProd.IdSubLInea), "OR", dgvOrigen, objProd.Id)
            MostrarGrillaTipoTabla(CInt(objProd.IdLinea), CInt(objProd.IdSubLInea), "CO", dgvCodigo, objProd.Id)
            MostrarGrillaTipoTabla(CInt(objProd.IdLinea), CInt(objProd.IdSubLInea), "AT", dgvAtributos, objProd.Id)
            MostrarGrillaTipoTabla(CInt(objProd.IdLinea), CInt(objProd.IdSubLInea), "SU", dgvSinUso, objProd.Id)
            ''****''

            Dim nProductoUMView As New Negocio.ProductoUMView
            Me.listaUMedida = getListaUM()
            Me.listaUMedida = nProductoUMView.SelectxIdProducto(IdProducto)
            setListaUM(Me.listaUMedida)

            DGV_UM.DataSource = Me.listaUMedida
            DGV_UM.DataBind()
            For i As Integer = 0 To listaUMedida.Count - 1
                If listaUMedida.Item(i).UnidadPrincipal = True Then
                    hddvalUM.Value = CStr(listaUMedida.Item(i).IdUnidadMedida)
                End If
            Next

            Dim nProductoMedidaView As New Negocio.ProductoMedidaView

            Me.listaMagnitud = obtenerListaProductoMedidaxIdProducto(objProd.Id)
            setlistaMagnitud(Me.listaMagnitud)

            DGV_Magnitud.DataSource = Me.listaMagnitud
            DGV_Magnitud.DataBind()


            '*********************** KIT

            cargar_kit(objProd.Kit)

            Me.chb_HabilitarKit.Checked = objProd.Kit
            Me.listaKit = obtenerListaKit_Load(objProd.Id)
            setListaKit(Me.listaKit)

            Me.GV_ComponenteKit.DataSource = Me.listaKit
            Me.GV_ComponenteKit.DataBind()


            '************** TIPO COSTO FLETE
            If (Me.rdbOpcion_CalculoCostoFlete.Items.FindByValue(CStr(objProd.CalculoFlete)) IsNot Nothing) Then
                Me.rdbOpcion_CalculoCostoFlete.SelectedValue = objProd.CalculoFlete
            Else
                Me.rdbOpcion_CalculoCostoFlete.SelectedValue = "NAN"
            End If

        Else
            mostrarMensaje(True, "Problemas en la carga de datos.")
        End If

        'Catch ex As Exception
        '    objScript.mostrarMsjAlerta(Me, ex.Message)
        'End Try
    End Sub
    Private Function getListaUM() As List(Of Entidades.ProductoUMView)
        Return CType(Session.Item("listaUM"), List(Of Entidades.ProductoUMView))
    End Function
    Private Sub setListaUM(ByVal lista As List(Of Entidades.ProductoUMView))
        Session.Remove("listaUM")
        Session.Add("listaUM", lista)
    End Sub
  
    Protected Sub btnAgregarMagnitud_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarMagnitud.Click

        addMagnitud()


    End Sub
    Private Sub DGV_Magnitud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_Magnitud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim gvrow As GridViewRow = CType(e.Row.Cells(0).NamingContainer, GridViewRow)
            Dim cbo As DropDownList = CType(gvrow.FindControl("cboUnidadMedida_Magnitud"), DropDownList)
            'Dim chkestadoprin As CheckBox = CType(gvrow.FindControl("chbPrincipal"), CheckBox)
            cbo.SelectedValue = CStr(Me.listaMagnitud(e.Row.RowIndex).IdUnidadMedida)

            If cbo.SelectedItem.Text = "KG" Then
                DirectCast(e.Row.FindControl("txtEscalarMagnitud"), TextBox).Enabled = False
            Else
                DirectCast(e.Row.FindControl("txtEscalarMagnitud"), TextBox).Enabled = True
            End If
        End If
    End Sub

    Protected Sub DGV_Magnitud_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_Magnitud.SelectedIndexChanged
        Dim obj As New ScriptManagerClass
        actualizarListaMagnitud()
        Me.listaMagnitud = Me.getlistaMagnitud
        Try
            Me.listaMagnitud.RemoveAt(DGV_Magnitud.SelectedIndex)
            Me.setlistaMagnitud(Me.listaMagnitud)
            DGV_Magnitud.DataSource = Me.listaMagnitud
            DGV_Magnitud.DataBind()
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la eliminación de la Magnitud.")
        End Try
    End Sub
    Private Sub DGV_UM_EDITAR_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_UM_EDITAR.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim chbEstado_Editar As CheckBox
                Dim gvrow As GridViewRow = CType(e.Row.Cells(4).NamingContainer, GridViewRow)
                chbEstado_Editar = CType(gvrow.FindControl("chbEstado_ProdEditar"), CheckBox)
                Me.listaUMedida = Me.getListaUM
                If chbEstado_Editar IsNot DBNull.Value Then
                    'Me.listaUMedida.Item(e.Row.RowIndex).Estado = CStr(IIf(chbEstado_Editar.Checked = True, "1", "0"))
                    chbEstado_Editar.Checked = Me.listaUMedida.Item(e.Row.RowIndex).EstadoBool
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub addMagnitud()
        Try
            actualizarListaMagnitud()

            Me.listaMagnitud = getlistaMagnitud()

            Dim objMagnitud As New Entidades.Magnitud
            With objMagnitud

                .Id = CInt(Me.cmbMagnitud.SelectedValue)
                .Descripcion = Me.cmbMagnitud.SelectedItem.ToString
                .ListaUM = (New Negocio.MagnitudUnidad).SelectCboUM(.Id)
                .Escalar = 0
                .pm_principal = False


            End With

            Me.listaMagnitud.Add(objMagnitud)

            If Me.listaMagnitud.Count = 1 Then
                Me.listaMagnitud(0).pm_principal = True
            End If

            setlistaMagnitud(Me.listaMagnitud)

            DGV_Magnitud.DataSource = Me.listaMagnitud
            DGV_Magnitud.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Private Sub actualizarListaMagnitud()

        Try

            Me.listaMagnitud = getlistaMagnitud()
            For i As Integer = 0 To DGV_Magnitud.Rows.Count - 1

                Me.listaMagnitud(i).Escalar = CDec(CType(DGV_Magnitud.Rows(i).FindControl("txtEscalarMagnitud"), TextBox).Text)
                Me.listaMagnitud(i).pm_principal = CBool(CType(DGV_Magnitud.Rows(i).FindControl("chbPrincipal"), CheckBox).Checked)
            Next
            setlistaMagnitud(Me.listaMagnitud)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, "Problemas en la actualización de las Magnitudes.")

        End Try


    End Sub
    Private Function obtenerListaProductoMedida() As List(Of Entidades.ProductoMedida)

        Dim lista As New List(Of Entidades.ProductoMedida)



        For i As Integer = 0 To DGV_Magnitud.Rows.Count - 1

            Dim obj As New Entidades.ProductoMedida
            With obj

                .Escalar = CDec(CType(DGV_Magnitud.Rows(i).FindControl("txtEscalarMagnitud"), TextBox).Text)
                .IdMagnitud = CInt(CType(DGV_Magnitud.Rows(i).FindControl("hddIdMagnitud"), HiddenField).Value)
                .IdProducto = Nothing
                .IdUnidadMedida = CInt(CType(DGV_Magnitud.Rows(i).FindControl("cboUnidadMedida_Magnitud"), DropDownList).SelectedValue)
                .pm_principal = CBool(CType(DGV_Magnitud.Rows(i).FindControl("chbPrincipal"), CheckBox).Checked)
            End With

            lista.Add(obj)

        Next




        Return lista

    End Function
    Private Function obtenerListaProductoMedidaxIdProducto(ByVal IdProducto As Integer) As List(Of Entidades.Magnitud)

        Dim listaMagnitud As New List(Of Entidades.Magnitud)
        Dim lista As List(Of Entidades.ProductoMedidaView) = (New Negocio.ProductoMedidaView).SelectxIdProducto(IdProducto)

        For i As Integer = 0 To lista.Count - 1

            Dim obj As New Entidades.Magnitud
            With obj

                .Descripcion = lista(i).NombreMagnitud
                .Escalar = lista(i).Escalar
                .Id = lista(i).IdMagnitud
                .ListaUM = (New Negocio.MagnitudUnidad).SelectCboUM(.Id)
                .IdUnidadMedida = lista(i).IdUnidadMedida
                .pm_principal = lista(i).pm_principal
            End With

            listaMagnitud.Add(obj)

        Next

        Return listaMagnitud

    End Function
    Protected Sub rdbCodNombre_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbCodNombre.SelectedIndexChanged
        If rdbCodNombre.SelectedValue = "1" Then
            M_lblTextoBusq.Visible = True
            M_lblTextoBusq.Text = "Ingrese código:"
            M_B_txtDescripcion.Visible = True
            M_B_txtDescripcion.Text = ""
        ElseIf rdbCodNombre.SelectedValue = "2" Then
            M_lblTextoBusq.Visible = True
            M_lblTextoBusq.Text = "Ingrese Nombre:"
            M_B_txtDescripcion.Visible = True
            M_B_txtDescripcion.Text = ""
        ElseIf rdbCodNombre.SelectedValue = "3" Then
            M_lblTextoBusq.Visible = True
            M_lblTextoBusq.Text = "Ingrese Cod.Prov:"
            M_B_txtDescripcion.Visible = True
            M_B_txtDescripcion.Text = ""

        End If
        hddModoVal.Value = "0"
    End Sub
    Protected Sub M_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles M_cmbSubLinea.SelectedIndexChanged
        txtCodOrigen.Text = "" : txtCodCodigo.Text = "" : hddCodAtributos.Value = "" : M_txtDescripcion.Text = ""
        '**** MOSTRAR ATRIBUTOS ****
        MostrarGrillaTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), "OR", dgvOrigen)
        MostrarGrillaTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), "CO", dgvCodigo)
        MostrarGrillaTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), "AT", dgvAtributos)
        MostrarGrillaTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), "SU", dgvSinUso)

        ListarSubLinea(CInt(M_cmbSubLinea.SelectedValue))
        llenarEntidades()

        If dgvCodigo.Rows.Count = 0 And dgvOrigen.Rows.Count = 0 And _
            dgvAtributos.Rows.Count = 0 And dgvSinUso.Rows.Count = 0 Then

            txtCodCodigo.Text = (New Negocio.Producto).fx_Get_CodProd_Lin_SubLin_AutoGen(CInt(M_cmbSubLinea.SelectedValue), 9)
            cargar_kit(True)
        Else
            cargar_kit(False)
        End If


        'nuevo
        If CInt(hddModo.Value) = operativo.Insert Then
            Me.rblAfectoPercepcion.SelectedValue = CStr(objproducto.productoAfectoSublineaforInsert(CInt(Me.M_cmbSubLinea.SelectedValue)))
        End If

    End Sub
    Private Sub llenarEntidades()
        Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
        Dim obj As Entidades.SubLinea_TipoTabla

        For x As Integer = 0 To 20
            obj = New Entidades.SubLinea_TipoTabla
            Lista.Add(obj)
        Next
        setListaCadena(Lista)

        'ORIGEN'
        Dim ListaOR As New List(Of Entidades.SubLinea_TipoTabla)
        For orx As Integer = 0 To 20
            obj = New Entidades.SubLinea_TipoTabla
            ListaOR.Add(obj)
        Next
        setListaCadenaOR(ListaOR)
        'CODIGO'
        Dim ListaCO As New List(Of Entidades.SubLinea_TipoTabla)
        For cox As Integer = 0 To 20
            obj = New Entidades.SubLinea_TipoTabla
            ListaCO.Add(obj)
        Next
        setListaCadenaCO(ListaCO)
        'ATRIBUTOS'
        Dim ListaAT As New List(Of Entidades.SubLinea_TipoTabla)
        For atx As Integer = 0 To 20
            obj = New Entidades.SubLinea_TipoTabla
            ListaAT.Add(obj)
        Next
        setListaCadenaAT(ListaAT)

        'SIN USO'
        Dim ListaSU As New List(Of Entidades.SubLinea_TipoTabla)
        For sux As Integer = 0 To 20
            obj = New Entidades.SubLinea_TipoTabla
            ListaSU.Add(obj)
        Next
        setListaCadenaSU(ListaSU)

    End Sub

    Private Sub llenarEntidadesEnOrden(ByVal tipouso As String, ByVal listaSTT As List(Of Entidades.SubLinea_TipoTabla))

        Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
        Dim obj As Entidades.SubLinea_TipoTabla
        Lista = getListaCadena()

        Select Case tipouso
            Case "OR"
                'ORIGEN'
                Dim ListaOR As New List(Of Entidades.SubLinea_TipoTabla)
                ListaOR = getListaCadenaOR()

                For orxx As Integer = 0 To listaSTT.Count - 1
                    obj = New Entidades.SubLinea_TipoTabla
                    With obj
                        .NroOrden = listaSTT(orxx).NroOrden  ' CInt(hddOrdenTTVCO.Value)
                        .ParteNombre = listaSTT(orxx).ParteNombre ' CBool(hddParteNombreTTVCO.Value)
                        .Codigo = listaSTT(orxx).Codigo  'LTrim(RTrim(CStr(lblCodigoTTVCO.Text)))
                        If CBool(listaSTT(orxx).ParteNombre) = True Then
                            .Nombre = listaSTT(orxx).Nombre  'LTrim(RTrim(CStr(lblNomTTVCO.Text)))
                        End If

                        Lista.RemoveAt(listaSTT(orxx).NroOrden)
                        Lista.Insert(listaSTT(orxx).NroOrden, obj)

                        ListaOR.RemoveAt(listaSTT(orxx).NroOrden)
                        ListaOR.Insert(listaSTT(orxx).NroOrden, obj)

                    End With
                Next
                setListaCadenaOR(ListaOR)

            Case "CO"
                'CODIGO'
                Dim ListaCO As New List(Of Entidades.SubLinea_TipoTabla)
                ListaCO = getListaCadenaOR()
                For orxx As Integer = 0 To listaSTT.Count - 1
                    obj = New Entidades.SubLinea_TipoTabla
                    With obj
                        .NroOrden = listaSTT(orxx).NroOrden ' CInt(hddOrdenTTVCO.Value)
                        .ParteNombre = listaSTT(orxx).ParteNombre ' CBool(hddParteNombreTTVCO.Value)
                        .Codigo = listaSTT(orxx).Codigo  'LTrim(RTrim(CStr(lblCodigoTTVCO.Text)))
                        If CBool(listaSTT(orxx).ParteNombre) = True Then
                            .Nombre = listaSTT(orxx).Nombre  'LTrim(RTrim(CStr(lblNomTTVCO.Text)))
                        End If

                        Lista.RemoveAt(listaSTT(orxx).NroOrden)
                        Lista.Insert(listaSTT(orxx).NroOrden, obj)

                        ListaCO.RemoveAt(listaSTT(orxx).NroOrden)
                        ListaCO.Insert(listaSTT(orxx).NroOrden, obj)

                    End With
                Next
                setListaCadenaCO(ListaCO)

            Case "AT"

                'ATRIBUTOS'
                Dim ListaAT As New List(Of Entidades.SubLinea_TipoTabla)
                ListaAT = getListaCadenaOR()
                For orxx As Integer = 0 To listaSTT.Count - 1
                    obj = New Entidades.SubLinea_TipoTabla
                    With obj
                        .NroOrden = listaSTT(orxx).NroOrden ' CInt(hddOrdenTTVCO.Value)
                        .ParteNombre = listaSTT(orxx).ParteNombre ' CBool(hddParteNombreTTVCO.Value)
                        .Codigo = listaSTT(orxx).Codigo  'LTrim(RTrim(CStr(lblCodigoTTVCO.Text)))
                        If CBool(listaSTT(orxx).ParteNombre) = True Then
                            .Nombre = listaSTT(orxx).Nombre  'LTrim(RTrim(CStr(lblNomTTVCO.Text)))
                        End If

                        Lista.RemoveAt(listaSTT(orxx).NroOrden)
                        Lista.Insert(listaSTT(orxx).NroOrden, obj)

                        ListaAT.RemoveAt(listaSTT(orxx).NroOrden)
                        ListaAT.Insert(listaSTT(orxx).NroOrden, obj)

                    End With
                Next
                setListaCadenaAT(ListaAT)

            Case "SU"
                'SIN USO'
                Dim ListaSU As New List(Of Entidades.SubLinea_TipoTabla)
                ListaSU = getListaCadenaOR()
                For orxx As Integer = 0 To listaSTT.Count - 1
                    obj = New Entidades.SubLinea_TipoTabla
                    With obj
                        .NroOrden = listaSTT(orxx).NroOrden ' CInt(hddOrdenTTVCO.Value)
                        .ParteNombre = listaSTT(orxx).ParteNombre ' CBool(hddParteNombreTTVCO.Value)
                        .Codigo = listaSTT(orxx).Codigo  'LTrim(RTrim(CStr(lblCodigoTTVCO.Text)))
                        If CBool(listaSTT(orxx).ParteNombre) = True Then
                            .Nombre = listaSTT(orxx).Nombre  'LTrim(RTrim(CStr(lblNomTTVCO.Text)))
                        End If

                        Lista.RemoveAt(listaSTT(orxx).NroOrden)
                        Lista.Insert(listaSTT(orxx).NroOrden, obj)

                        ListaSU.RemoveAt(listaSTT(orxx).NroOrden)
                        ListaSU.Insert(listaSTT(orxx).NroOrden, obj)

                    End With
                Next
                setListaCadenaSU(ListaSU)

        End Select

    End Sub


#Region "Llenar combox de tablas asociadas"
    Private Sub LlenarCboProveedor(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.PersonaView) = (New Negocio.Proveedor).SelectCbo()
        cbo.DataSource = lista
        cbo.DataTextField = "RazonSocial"
        cbo.DataValueField = "IdPersona"
        cbo.DataBind()
        If addElement Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If
    End Sub

#End Region

    Private Sub LlenarCboSubLineaxIdTipoExistCodigo(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.SubLinea) = (New Negocio.SubLinea).SelectActivoxLineaxTipoExistenciaCodigo(idlinea, idtipoexistencia)
        If addElement Then
            Dim objSubLinea As New Entidades.SubLinea("0", "-----", True)
            lista.Insert(0, objSubLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "CodigoSubLinea"
        cbo.DataBind()
    End Sub

    Private Sub ValidarAutogenerado(ByVal modo As String, ByVal NoVisible As String, ByVal prod_Codigo As String, ByVal prod_Atributos As String)
        Try

            If hddModo.Value = CStr(modo) Then
                Dim ngcIndAtributos As New Negocio.Producto
                Dim objIndAtributos As New Entidades.Producto
                objIndAtributos = ngcIndAtributos.ValidarAtributos(NoVisible, prod_Codigo, prod_Atributos)
                With objIndAtributos
                    hddValCodigo.Value = CStr(.CodAtributos)
                End With
            End If
        Catch ex As Exception
            Me.hddValCodigo.Value = ""
        End Try
    End Sub

    Sub ClearAtributos()
        hddValCodigo.Value = ""

    End Sub


#Region "Mostrar Atributos en Grilla"
    Private Sub MostrarGrillaTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal tipouso As String, ByVal grilla As GridView)
        Dim ngcSubLineaTipoTabla As New Negocio.SubLinea_TipoTabla
        Dim lista As List(Of Entidades.SubLinea_TipoTabla) = ngcSubLineaTipoTabla.SelectxIdLineaxIdSubLinea(idlinea, idsublinea, tipouso)
        grilla.DataSource = lista
        grilla.DataBind()
    End Sub
    Private Sub MostrarGrillaTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal tipouso As String, ByVal grilla As GridView, ByVal idproducto As Integer)
        'aqui una lista instanciada hasta 20 listaInstacida 
        Dim listaInstanciada As List(Of Entidades.SubLinea_TipoTabla)
        Dim listaInstanciadax As List(Of Entidades.SubLinea_TipoTabla)
        listaInstanciadax = getListaCadena()

        Dim ngcSubLineaTipoTabla As New Negocio.SubLinea_TipoTabla
        'esta lista de la bd del producto buscado
        Dim lista As List(Of Entidades.SubLinea_TipoTabla) = ngcSubLineaTipoTabla.SelectxIdLineaxIdSubLineaxIdProducto(idlinea, idsublinea, tipouso, idproducto)
        'lista para obtener datos de la subgrilla
        Dim listSubGrilla As List(Of Entidades.SubLinea_TipoTablaValor)
        For x As Int32 = 0 To lista.Count - 1

            If tipouso = "OR" Then
                listaInstanciada = getListaCadenaOR()
                ' aqui se pregunta para recuperar datos de la subgrilla
                If lista(x).Indicador = True Then
                    'aqui ya se el nro order entonces inserto en la lista principal
                    'los la lista de la subgrilla correspondiente
                    'listSubGrilla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                    listSubGrilla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla02(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)

                    'If listSubGrilla(0).ParteNombre = False Then
                    '    listSubGrilla(0).NroOrden = 0
                    'End If

                    lista(x).objSubOrigen = listSubGrilla
                    listaInstanciada.RemoveAt(listSubGrilla(0).NroOrden)
                    listaInstanciada.Insert(listSubGrilla(0).NroOrden, lista(x))
                    If listSubGrilla(0).ParteNombre = True Then
                        listaInstanciadax(listSubGrilla(0).NroOrden).ParteNombre = True
                        listaInstanciadax(listSubGrilla(0).NroOrden).Nombre = listSubGrilla(0).Nombre
                    End If

                    'listaInstanciadax.RemoveAt(listSubGrilla(0).NroOrden)
                    'listaInstanciadax.Insert(listSubGrilla(0).NroOrden, lista(x))

                    'listaInstanciadax(listSubGrilla(0).NroOrden).Nombre = listSubGrilla(0).Nombre
                    'listaInstanciadax(listSubGrilla(0).NroOrden).ParteNombre = listSubGrilla(0).ParteNombre

                    'For sd As Integer = 0 To listaInstanciada.Count - 1
                    '    If listaInstanciada(sd).ParteNombre = False Then
                    '        listaInstanciada(sd).Nombre = ""
                    '        listaInstanciada(sd).NroOrden = 0
                    '    End If
                    'Next
                    setListaCadenaOR(listaInstanciada)

                End If
            ElseIf tipouso = "CO" Then
                'If lista(x).Indicador = True Then
                '    Dim ngcSubLineaTipoTablaValor As New Negocio.SubLinea_TipoTablaValor
                '    lista(x).objSubCodigo = ngcSubLineaTipoTablaValor.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                'End If
                listaInstanciada = getListaCadenaCO()
                ' aqui se pregunta para recuperar datos de la subgrilla
                If lista(x).Indicador = True Then
                    'aqui ya se el nro order entonces inserto en la lista principal
                    'los la lista de la subgrilla correspondiente
                    'listSubGrilla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                    listSubGrilla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla02(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)

                    'If listSubGrilla(0).ParteNombre = False Then
                    '    listSubGrilla(0).NroOrden = 0
                    'End If
                    lista(x).objSubCodigo = listSubGrilla

                    listaInstanciada.RemoveAt(listSubGrilla(0).NroOrden)
                    listaInstanciada.Insert(listSubGrilla(0).NroOrden, lista(x))
                    If listSubGrilla(0).ParteNombre = True Then
                        listaInstanciadax(listSubGrilla(0).NroOrden).ParteNombre = True
                        listaInstanciadax(listSubGrilla(0).NroOrden).Nombre = listSubGrilla(0).Nombre
                    End If
                    'listaInstanciadax.RemoveAt(listSubGrilla(0).NroOrden)
                    'listaInstanciadax.Insert(listSubGrilla(0).NroOrden, lista(x))

                    'listaInstanciadax(listSubGrilla(0).NroOrden).Nombre = listSubGrilla(0).Nombre
                    'listaInstanciadax(listSubGrilla(0).NroOrden).ParteNombre = listSubGrilla(0).ParteNombre

                    'For sd As Integer = 0 To listaInstanciada.Count - 1
                    '    If listaInstanciada(sd).ParteNombre = False Then
                    '        listaInstanciada(sd).Nombre = ""
                    '        listaInstanciada(sd).NroOrden = 0
                    '    End If
                    'Next
                    setListaCadenaCO(listaInstanciada)

                End If



            ElseIf tipouso = "AT" Then
                'If lista(x).Indicador = True Then
                '    Dim ngcSubLineaTipoTablaValor As New Negocio.SubLinea_TipoTablaValor
                '    lista(x).objSubAtributos = ngcSubLineaTipoTablaValor.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                'End If

                listaInstanciada = getListaCadenaAT()
                ' aqui se pregunta para recuperar datos de la subgrilla
                If lista(x).Indicador = True Then
                    'aqui ya se el nro order entonces inserto en la lista principal
                    'los la lista de la subgrilla correspondiente
                    'listSubGrilla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                    listSubGrilla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla02(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                    'If listSubGrilla(0).ParteNombre = False Then
                    '    listSubGrilla(0).NroOrden = 0
                    'End If
                    lista(x).objSubAtributos = listSubGrilla

                    listaInstanciada.RemoveAt(listSubGrilla(0).NroOrden)
                    listaInstanciada.Insert(listSubGrilla(0).NroOrden, lista(x))
                    If listSubGrilla(0).ParteNombre = True Then
                        listaInstanciadax(listSubGrilla(0).NroOrden).ParteNombre = True
                        listaInstanciadax(listSubGrilla(0).NroOrden).Nombre = listSubGrilla(0).Nombre
                    End If
                    'listaInstanciadax.RemoveAt(listSubGrilla(0).NroOrden)
                    'listaInstanciadax.Insert(listSubGrilla(0).NroOrden, lista(x))

                    'listaInstanciadax(listSubGrilla(0).NroOrden).Nombre = listSubGrilla(0).Nombre
                    'listaInstanciadax(listSubGrilla(0).NroOrden).ParteNombre = listSubGrilla(0).ParteNombre

                    'For sd As Integer = 0 To listaInstanciada.Count - 1
                    '    If listaInstanciada(sd).ParteNombre = False Then
                    '        listaInstanciada(sd).Nombre = ""
                    '        listaInstanciada(sd).NroOrden = 0
                    '    End If
                    'Next
                    setListaCadenaAT(listaInstanciada)

                End If


            ElseIf tipouso = "SU" Then
                'If lista(x).Indicador = True Then
                '    Dim ngcSubLineaTipoTablaValor As New Negocio.SubLinea_TipoTablaValor
                '    lista(x).objSubSinUso = ngcSubLineaTipoTablaValor.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                'End If

                listaInstanciada = getListaCadenaSU()
                ' aqui se pregunta para recuperar datos de la subgrilla
                If lista(x).Indicador = True Then
                    'aqui ya se el nro order entonces inserto en la lista principal
                    'los la lista de la subgrilla correspondiente
                    ' listSubGrilla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                    listSubGrilla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla02(idlinea, idsublinea, lista(x).IdTipoTabla, idproducto)
                    'If listSubGrilla(0).ParteNombre = False Then
                    '    listSubGrilla(0).NroOrden = 0
                    'End If
                    lista(x).objSubSinUso = listSubGrilla
                    listaInstanciada.RemoveAt(listSubGrilla(0).NroOrden)
                    listaInstanciada.Insert(listSubGrilla(0).NroOrden, lista(x))
                    If listSubGrilla(0).ParteNombre = True Then
                        listaInstanciadax(listSubGrilla(0).NroOrden).ParteNombre = True
                        listaInstanciadax(listSubGrilla(0).NroOrden).Nombre = listSubGrilla(0).Nombre
                    End If

                    'listaInstanciadax.RemoveAt(listSubGrilla(0).NroOrden)
                    'listaInstanciadax.Insert(listSubGrilla(0).NroOrden, lista(x))

                    'listaInstanciadax(listSubGrilla(0).NroOrden).Nombre = listSubGrilla(0).Nombre
                    'listaInstanciadax(listSubGrilla(0).NroOrden).ParteNombre = listSubGrilla(0).ParteNombre

                    'For sd As Integer = 0 To listaInstanciada.Count - 1
                    '    If listaInstanciada(sd).ParteNombre = False Then
                    '        listaInstanciada(sd).Nombre = ""
                    '        listaInstanciada(sd).NroOrden = 0
                    '    End If
                    'Next

                    setListaCadenaSU(listaInstanciada)

                End If

            End If
        Next

        'For sd As Integer = 0 To listaInstanciadax.Count - 1
        '    If listaInstanciadax(sd).ParteNombre = False Then
        '        listaInstanciadax(sd).Nombre = ""
        '        listaInstanciadax(sd).NroOrden = 0
        '    End If
        'Next

        setListaCadena(listaInstanciadax)
        'llenarEntidadesEnOrden(tipouso, lista)

        grilla.DataSource = lista
        grilla.DataBind()
    End Sub
    Private Sub MostrarGrillaTipoTablaValor(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal grilla As GridView)
        Dim ngcSubLineaTipoTablaValor As New Negocio.SubLinea_TipoTablaValor
        Dim lista As List(Of Entidades.SubLinea_TipoTablaValor) = ngcSubLineaTipoTablaValor.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, idtipotabla)
        grilla.DataSource = lista
        grilla.DataBind()
    End Sub
    Private Sub MostrarGrillaTipoTablaValor(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal grilla As GridView, ByVal idproducto As Integer)
        Dim ngcSubLineaTipoTablaValor As New Negocio.SubLinea_TipoTablaValor
        Dim lista As List(Of Entidades.SubLinea_TipoTablaValor) = ngcSubLineaTipoTablaValor.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, idtipotabla, idproducto)
        grilla.DataSource = lista
        grilla.DataBind()
    End Sub

#Region "Cuadricula Origen"
    Private list_Origen As List(Of Entidades.SubLinea_TipoTabla)
    Private obj_Origen As Entidades.SubLinea_TipoTabla
    Private Function getOrigen() As List(Of Entidades.SubLinea_TipoTabla)
        list_Origen = New List(Of Entidades.SubLinea_TipoTabla)

        For Each row As GridViewRow In dgvOrigen.Rows
            obj_Origen = New Entidades.SubLinea_TipoTabla
            With obj_Origen
                .Indicador = CType(row.Cells(0).FindControl("chkMostrarTTVOR"), CheckBox).Checked
                .Nombre = CStr(CType(row.Cells(1).FindControl("lblNomTipoTablaOR"), Label).Text)
                .IdTipoTabla = CInt(CType(row.Cells(1).FindControl("hddIdTipoTablaOR"), HiddenField).Value)
                If .Indicador = True Then
                    .objSubOrigen = getSubOrigen(CType(row.Cells(2).FindControl("dgvSubOrigen"), GridView))
                End If
            End With
            list_Origen.Add(obj_Origen)
        Next

        Return list_Origen
    End Function
    Function getSubOrigen(ByVal grid As GridView) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)

        For Each row As GridViewRow In grid.Rows
            Dim obj As New Entidades.SubLinea_TipoTablaValor
            With obj
                .Indicador = CType(row.Cells(0).FindControl("chkActivarOR"), CheckBox).Checked
                .Codigo = CType(row.Cells(1).FindControl("lblCodigoTTVOR"), Label).Text
                .NroOrden = CInt(CType(row.Cells(1).FindControl("hddOrdenTTVOR"), HiddenField).Value)
                .ParteNombre = CBool(CType(row.Cells(1).FindControl("hddParteNombreTTVOR"), HiddenField).Value)
                .TipoUso = CStr(CType(row.Cells(1).FindControl("hddTipoUsoTTVOR"), HiddenField).Value)

                .Nombre = CType(row.Cells(2).FindControl("lblNomTTVOR"), Label).Text
                .IdTipoTablaValor = CInt(CType(row.Cells(2).FindControl("hddIdTTVOR"), HiddenField).Value)
            End With
            Lista.Add(obj)
        Next
        Return Lista
    End Function
    'SELECT TRUE ORIGEN
    Private Function _getOrigen() As List(Of Entidades.SubLinea_TipoTabla)
        list_Origen = New List(Of Entidades.SubLinea_TipoTabla)

        For Each row As GridViewRow In dgvOrigen.Rows
            obj_Origen = New Entidades.SubLinea_TipoTabla
            With obj_Origen
                .Indicador = CType(row.Cells(0).FindControl("chkMostrarTTVOR"), CheckBox).Checked
                .Nombre = CStr(CType(row.Cells(1).FindControl("lblNomTipoTablaOR"), Label).Text)
                .IdTipoTabla = CInt(CType(row.Cells(1).FindControl("hddIdTipoTablaOR"), HiddenField).Value)
                If .Indicador = True Then
                    .objSubOrigen = _getSubOrigen(CType(row.Cells(2).FindControl("dgvSubOrigen"), GridView))
                End If
            End With
            list_Origen.Add(obj_Origen)
        Next

        Return list_Origen
    End Function
    Function _getSubOrigen(ByVal grid As GridView) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)

        For Each row As GridViewRow In grid.Rows
            Dim obj As New Entidades.SubLinea_TipoTablaValor
            With obj
                .Indicador = CType(row.Cells(0).FindControl("chkActivarOR"), CheckBox).Checked
                .Codigo = CType(row.Cells(1).FindControl("lblCodigoTTVOR"), Label).Text
                .NroOrden = CInt(CType(row.Cells(1).FindControl("hddOrdenTTVOR"), HiddenField).Value)
                .ParteNombre = CBool(CType(row.Cells(1).FindControl("hddParteNombreTTVOR"), HiddenField).Value)
                .TipoUso = CStr(CType(row.Cells(1).FindControl("hddTipoUsoTTVOR"), HiddenField).Value)

                .Nombre = CType(row.Cells(2).FindControl("lblNomTTVOR"), Label).Text
                .IdTipoTablaValor = CInt(CType(row.Cells(2).FindControl("hddIdTTVOR"), HiddenField).Value)
            End With
            If obj.Indicador = True Then
                Lista.Add(obj)
            End If
        Next
        Return Lista
    End Function
#End Region
#Region "Cuadricula Codigo"
    Private list_Codigo As List(Of Entidades.SubLinea_TipoTabla)
    Private obj_Codigo As Entidades.SubLinea_TipoTabla
    Private Function getCodigo() As List(Of Entidades.SubLinea_TipoTabla)
        list_Codigo = New List(Of Entidades.SubLinea_TipoTabla)
        For Each row As GridViewRow In dgvCodigo.Rows
            obj_Codigo = New Entidades.SubLinea_TipoTabla
            With obj_Codigo
                .Indicador = CType(row.Cells(0).FindControl("chkMostrarTTVCO"), CheckBox).Checked
                .Nombre = CStr(CType(row.Cells(1).FindControl("lblNomTipoTablaCO"), Label).Text)
                .IdTipoTabla = CInt(CType(row.Cells(1).FindControl("hddIdTipoTablaCO"), HiddenField).Value)
                If .Indicador = True Then
                    .objSubCodigo = getSubCodigo(CType(row.Cells(2).FindControl("dgvSubCodigo"), GridView))
                End If
            End With
            list_Codigo.Add(obj_Codigo)
        Next

        Return list_Codigo
    End Function
    Function getSubCodigo(ByVal grid As GridView) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)

        For Each row As GridViewRow In grid.Rows
            Dim obj As New Entidades.SubLinea_TipoTablaValor
            With obj
                .Indicador = CType(row.Cells(0).FindControl("chkActivarCO"), CheckBox).Checked
                .Codigo = CType(row.Cells(1).FindControl("lblCodigoTTVCO"), Label).Text
                .NroOrden = CInt(CType(row.Cells(1).FindControl("hddOrdenTTVCO"), HiddenField).Value)
                .ParteNombre = CBool(CType(row.Cells(1).FindControl("hddParteNombreTTVCO"), HiddenField).Value)
                .TipoUso = CStr(CType(row.Cells(1).FindControl("hddTipoUsoTTVCO"), HiddenField).Value)

                .Nombre = CType(row.Cells(2).FindControl("lblNomTTVCO"), Label).Text
                .IdTipoTablaValor = CInt(CType(row.Cells(2).FindControl("hddIdTTVCO"), HiddenField).Value)
            End With
            Lista.Add(obj)
        Next
        Return Lista
    End Function


    'SELECT TRUE CODIGO
    Private Function _getCodigo() As List(Of Entidades.SubLinea_TipoTabla)
        list_Codigo = New List(Of Entidades.SubLinea_TipoTabla)
        For Each row As GridViewRow In dgvCodigo.Rows
            obj_Codigo = New Entidades.SubLinea_TipoTabla
            With obj_Codigo
                .Indicador = CType(row.Cells(0).FindControl("chkMostrarTTVCO"), CheckBox).Checked
                .Nombre = CStr(CType(row.Cells(1).FindControl("lblNomTipoTablaCO"), Label).Text)
                .IdTipoTabla = CInt(CType(row.Cells(1).FindControl("hddIdTipoTablaCO"), HiddenField).Value)
                If .Indicador = True Then
                    .objSubCodigo = _getSubCodigo(CType(row.Cells(2).FindControl("dgvSubCodigo"), GridView))
                End If
            End With
            list_Codigo.Add(obj_Codigo)
        Next

        Return list_Codigo
    End Function
    Function _getSubCodigo(ByVal grid As GridView) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)

        For Each row As GridViewRow In grid.Rows
            Dim obj As New Entidades.SubLinea_TipoTablaValor
            With obj
                .Indicador = CType(row.Cells(0).FindControl("chkActivarCO"), CheckBox).Checked
                .Codigo = CType(row.Cells(1).FindControl("lblCodigoTTVCO"), Label).Text
                .NroOrden = CInt(CType(row.Cells(1).FindControl("hddOrdenTTVCO"), HiddenField).Value)
                .ParteNombre = CBool(CType(row.Cells(1).FindControl("hddParteNombreTTVCO"), HiddenField).Value)
                .TipoUso = CStr(CType(row.Cells(1).FindControl("hddTipoUsoTTVCO"), HiddenField).Value)

                .Nombre = CType(row.Cells(2).FindControl("lblNomTTVCO"), Label).Text
                .IdTipoTablaValor = CInt(CType(row.Cells(2).FindControl("hddIdTTVCO"), HiddenField).Value)
            End With
            If obj.Indicador = True Then
                Lista.Add(obj)
            End If

        Next
        Return Lista
    End Function
#End Region
#Region "Cuadricula Atributos"
    Private list_Atributos As List(Of Entidades.SubLinea_TipoTabla)
    Private obj_Atributos As Entidades.SubLinea_TipoTabla
    Private Function getAtributos() As List(Of Entidades.SubLinea_TipoTabla)
        list_Atributos = New List(Of Entidades.SubLinea_TipoTabla)
        For Each row As GridViewRow In dgvAtributos.Rows
            obj_Atributos = New Entidades.SubLinea_TipoTabla
            With obj_Atributos
                .Indicador = CType(row.Cells(0).FindControl("chkMostrarTTVAT"), CheckBox).Checked
                .Nombre = CStr(CType(row.Cells(1).FindControl("lblNomTipoTablaAT"), Label).Text)
                .IdTipoTabla = CInt(CType(row.Cells(1).FindControl("hddIdTipoTablaAT"), HiddenField).Value)
                If .Indicador = True Then
                    .objSubAtributos = getSubAtributos(CType(row.Cells(2).FindControl("dgvSubAtributos"), GridView))
                End If
            End With
            list_Atributos.Add(obj_Atributos)
        Next

        Return list_Atributos
    End Function
    Function getSubAtributos(ByVal grid As GridView) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)

        For Each row As GridViewRow In grid.Rows
            Dim obj As New Entidades.SubLinea_TipoTablaValor
            With obj
                .Indicador = CType(row.Cells(0).FindControl("chkActivarAT"), CheckBox).Checked
                .Codigo = CType(row.Cells(1).FindControl("lblCodigoTTVAT"), Label).Text
                .NroOrden = CInt(CType(row.Cells(1).FindControl("hddOrdenTTVAT"), HiddenField).Value)
                .ParteNombre = CBool(CType(row.Cells(1).FindControl("hddParteNombreTTVAT"), HiddenField).Value)
                .TipoUso = CStr(CType(row.Cells(1).FindControl("hddTipoUsoTTVAT"), HiddenField).Value)

                .Nombre = CType(row.Cells(2).FindControl("lblNomTTVAT"), Label).Text
                .IdTipoTablaValor = CInt(CType(row.Cells(2).FindControl("hddIdTTVAT"), HiddenField).Value)
            End With
            Lista.Add(obj)
        Next
        Return Lista
    End Function

    'SELECT TRUE ATRIBUTOS
    Private Function _getAtributos() As List(Of Entidades.SubLinea_TipoTabla)
        list_Atributos = New List(Of Entidades.SubLinea_TipoTabla)
        For Each row As GridViewRow In dgvAtributos.Rows
            obj_Atributos = New Entidades.SubLinea_TipoTabla
            With obj_Atributos
                .Indicador = CType(row.Cells(0).FindControl("chkMostrarTTVAT"), CheckBox).Checked
                .Nombre = CStr(CType(row.Cells(1).FindControl("lblNomTipoTablaAT"), Label).Text)
                .IdTipoTabla = CInt(CType(row.Cells(1).FindControl("hddIdTipoTablaAT"), HiddenField).Value)
                If .Indicador = True Then
                    .objSubAtributos = _getSubAtributos(CType(row.Cells(2).FindControl("dgvSubAtributos"), GridView))
                End If
            End With
            list_Atributos.Add(obj_Atributos)
        Next

        Return list_Atributos
    End Function
    Function _getSubAtributos(ByVal grid As GridView) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)

        For Each row As GridViewRow In grid.Rows
            Dim obj As New Entidades.SubLinea_TipoTablaValor
            With obj
                .Indicador = CType(row.Cells(0).FindControl("chkActivarAT"), CheckBox).Checked
                .Codigo = CType(row.Cells(1).FindControl("lblCodigoTTVAT"), Label).Text
                .NroOrden = CInt(CType(row.Cells(1).FindControl("hddOrdenTTVAT"), HiddenField).Value)
                .ParteNombre = CBool(CType(row.Cells(1).FindControl("hddParteNombreTTVAT"), HiddenField).Value)
                .TipoUso = CStr(CType(row.Cells(1).FindControl("hddTipoUsoTTVAT"), HiddenField).Value)

                .Nombre = CType(row.Cells(2).FindControl("lblNomTTVAT"), Label).Text
                .IdTipoTablaValor = CInt(CType(row.Cells(2).FindControl("hddIdTTVAT"), HiddenField).Value)
            End With
            If obj.Indicador = True Then
                Lista.Add(obj)
            End If
        Next
        Return Lista
    End Function

#End Region
#Region "Cuadricula Sin Uso"
    Private list_SinUso As List(Of Entidades.SubLinea_TipoTabla)
    Private obj_SinUso As Entidades.SubLinea_TipoTabla
    Private Function getSinUso() As List(Of Entidades.SubLinea_TipoTabla)
        list_SinUso = New List(Of Entidades.SubLinea_TipoTabla)

        For Each row As GridViewRow In dgvSinUso.Rows
            obj_SinUso = New Entidades.SubLinea_TipoTabla
            With obj_SinUso
                .Indicador = CType(row.Cells(0).FindControl("chkMostrarTTVSU"), CheckBox).Checked
                .Nombre = CStr(CType(row.Cells(1).FindControl("lblNomTipoTablaSU"), Label).Text)
                .IdTipoTabla = CInt(CType(row.Cells(1).FindControl("hddIdTipoTablaSU"), HiddenField).Value)
                If .Indicador = True Then
                    .objSubSinUso = getSubSinUso(CType(row.Cells(2).FindControl("dgvSubSinUso"), GridView))
                End If
            End With
            list_SinUso.Add(obj_SinUso)
        Next

        Return list_SinUso
    End Function
    Function getSubSinUso(ByVal grid As GridView) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)

        For Each row As GridViewRow In grid.Rows
            Dim obj As New Entidades.SubLinea_TipoTablaValor
            With obj
                .Indicador = CType(row.Cells(0).FindControl("chkActivarSU"), CheckBox).Checked
                .Codigo = CType(row.Cells(1).FindControl("lblCodigoTTVSU"), Label).Text
                .NroOrden = CInt(CType(row.Cells(1).FindControl("hddOrdenTTVSU"), HiddenField).Value)
                .ParteNombre = CBool(CType(row.Cells(1).FindControl("hddParteNombreTTVSU"), HiddenField).Value)
                .TipoUso = CStr(CType(row.Cells(1).FindControl("hddTipoUsoTTVSU"), HiddenField).Value)

                .Nombre = CType(row.Cells(2).FindControl("lblNomTTVSU"), Label).Text
                .IdTipoTablaValor = CInt(CType(row.Cells(2).FindControl("hddIdTTVSU"), HiddenField).Value)
            End With

            Lista.Add(obj)
        Next
        Return Lista
    End Function

    'SELEC TRUE SIN USO

    Private Function _getSinUso() As List(Of Entidades.SubLinea_TipoTabla)
        list_SinUso = New List(Of Entidades.SubLinea_TipoTabla)

        For Each row As GridViewRow In dgvSinUso.Rows
            obj_SinUso = New Entidades.SubLinea_TipoTabla
            With obj_SinUso
                .Indicador = CType(row.Cells(0).FindControl("chkMostrarTTVSU"), CheckBox).Checked
                .Nombre = CStr(CType(row.Cells(1).FindControl("lblNomTipoTablaSU"), Label).Text)
                .IdTipoTabla = CInt(CType(row.Cells(1).FindControl("hddIdTipoTablaSU"), HiddenField).Value)
                If .Indicador = True Then
                    .objSubSinUso = _getSubSinUso(CType(row.Cells(2).FindControl("dgvSubSinUso"), GridView))
                End If
            End With
            list_SinUso.Add(obj_SinUso)
        Next

        Return list_SinUso
    End Function
    Function _getSubSinUso(ByVal grid As GridView) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)

        For Each row As GridViewRow In grid.Rows
            Dim obj As New Entidades.SubLinea_TipoTablaValor
            With obj
                .Indicador = CType(row.Cells(0).FindControl("chkActivarSU"), CheckBox).Checked
                .Codigo = CType(row.Cells(1).FindControl("lblCodigoTTVSU"), Label).Text
                .NroOrden = CInt(CType(row.Cells(1).FindControl("hddOrdenTTVSU"), HiddenField).Value)
                .ParteNombre = CBool(CType(row.Cells(1).FindControl("hddParteNombreTTVSU"), HiddenField).Value)
                .TipoUso = CStr(CType(row.Cells(1).FindControl("hddTipoUsoTTVSU"), HiddenField).Value)

                .Nombre = CType(row.Cells(2).FindControl("lblNomTTVSU"), Label).Text
                .IdTipoTablaValor = CInt(CType(row.Cells(2).FindControl("hddIdTTVSU"), HiddenField).Value)
            End With
            If obj.Indicador = True Then
                Lista.Add(obj)
            End If
        Next
        Return Lista
    End Function


#End Region
    Protected Sub chkMostrarTTVOR_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        ViewState.Add("ProductoOrigen", Nothing)
        ViewState.Add("ProductoCodigo", Nothing)

        Dim subgrilla As GridView
        Dim row As GridViewRow = CType(CType(sender, CheckBox).NamingContainer, GridViewRow)
        Dim chkActivar As CheckBox = CType(row.Cells(0).FindControl("chkMostrarTTVOR"), CheckBox)
        subgrilla = CType(row.Cells(2).FindControl("dgvSubOrigen"), GridView)
        If chkActivar.Checked = True Then
            Dim hddIdTipoTabla As HiddenField = CType(row.Cells(1).FindControl("hddIdTipoTablaOR"), HiddenField)
            Dim caja As TextBox = CType(row.Cells(1).FindControl("txtFilOrigen"), TextBox)

            list_Origen = getOrigen()
            list_Origen(row.RowIndex).objSubOrigen = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Origen(row.RowIndex).IdTipoTabla, caja.Text)

            'Select Case CInt(hddModo.Value)
            '    Case operativo.Insert
            '        'list_Origen(row.RowIndex).objSubOrigen = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Origen(row.RowIndex).IdTipoTabla, caja.Text)
            '    Case operativo.Update
            '        list_Origen(row.RowIndex).objSubOrigen = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Origen(row.RowIndex).IdTipoTabla, CInt(M_txtCodigo.Text))
            'End Select

            dgvOrigen.DataSource = list_Origen
            dgvOrigen.DataBind()

            'MostrarGrillaTipoTablaValor(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), CInt(hddIdTipoTabla.Value), subgrilla)

        ElseIf chkActivar.Checked = False Then
            MostrarGrillaTipoTablaValor(CInt(0), CInt(0), CInt(0), subgrilla)
        End If
    End Sub
    Protected Sub chkMostrarTTVCO_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        ViewState.Add("ProductoOrigen", Nothing)
        ViewState.Add("ProductoCodigo", Nothing)

        Dim subgrilla As GridView
        Dim row As GridViewRow = CType(CType(sender, CheckBox).NamingContainer, GridViewRow)
        Dim chkActivar As CheckBox = CType(row.Cells(0).FindControl("chkMostrarTTVCO"), CheckBox)
        subgrilla = CType(row.Cells(2).FindControl("dgvSubCodigo"), GridView)
        If chkActivar.Checked = True Then
            Dim hddIdTipoTabla As HiddenField = CType(row.Cells(1).FindControl("hddIdTipoTablaCO"), HiddenField)
            Dim caja As TextBox = CType(row.Cells(1).FindControl("txtFilCodigo"), TextBox)

            list_Codigo = getCodigo()
            list_Codigo(row.RowIndex).objSubCodigo = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Codigo(row.RowIndex).IdTipoTabla, caja.Text)

            'Select Case CInt(hddModo.Value)
            '    Case operativo.Insert
            '        'list_Codigo(row.RowIndex).objSubCodigo = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Codigo(row.RowIndex).IdTipoTabla, caja.Text)
            '    Case operativo.Update
            '        list_Codigo(row.RowIndex).objSubCodigo = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Codigo(row.RowIndex).IdTipoTabla, CInt(M_txtCodigo.Text))
            'End Select

            dgvCodigo.DataSource = list_Codigo
            dgvCodigo.DataBind()
            'MostrarGrillaTipoTablaValor(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), CInt(hddIdTipoTabla.Value), subgrilla)
        ElseIf chkActivar.Checked = False Then
            MostrarGrillaTipoTablaValor(CInt(0), CInt(0), CInt(0), subgrilla)
        End If
    End Sub
    Protected Sub chkMostrarTTVAT_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        ViewState.Add("ProductoOrigen", Nothing)
        ViewState.Add("ProductoCodigo", Nothing)

        Dim subgrilla As GridView
        Dim row As GridViewRow = CType(CType(sender, CheckBox).NamingContainer, GridViewRow)
        Dim chkActivar As CheckBox = CType(row.Cells(0).FindControl("chkMostrarTTVAT"), CheckBox)
        subgrilla = CType(row.Cells(2).FindControl("dgvSubAtributos"), GridView)
        If chkActivar.Checked = True Then
            Dim hddIdTipoTabla As HiddenField = CType(row.Cells(1).FindControl("hddIdTipoTablaAT"), HiddenField)
            Dim caja As TextBox = CType(row.Cells(1).FindControl("txtFilAtributos"), TextBox)

            list_Atributos = getAtributos()
            list_Atributos(row.RowIndex).objSubAtributos = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Atributos(row.RowIndex).IdTipoTabla, caja.Text)

            'Select Case CInt(hddModo.Value)
            '    Case operativo.Insert
            '        'list_Atributos(row.RowIndex).objSubAtributos = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Atributos(row.RowIndex).IdTipoTabla, caja.Text)
            '    Case operativo.Update
            '        list_Atributos(row.RowIndex).objSubAtributos = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_Atributos(row.RowIndex).IdTipoTabla, CInt(M_txtCodigo.Text))
            'End Select
            dgvAtributos.DataSource = list_Atributos
            dgvAtributos.DataBind()

            'MostrarGrillaTipoTablaValor(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), CInt(hddIdTipoTabla.Value), subgrilla)
        ElseIf chkActivar.Checked = False Then
            MostrarGrillaTipoTablaValor(CInt(0), CInt(0), CInt(0), subgrilla)
        End If
    End Sub
    Protected Sub chkMostrarTTVSU_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim subgrilla As GridView
        Dim row As GridViewRow = CType(CType(sender, CheckBox).NamingContainer, GridViewRow)
        Dim chkActivar As CheckBox = CType(row.Cells(0).FindControl("chkMostrarTTVSU"), CheckBox)
        subgrilla = CType(row.Cells(2).FindControl("dgvSubSinUso"), GridView)
        If chkActivar.Checked = True Then
            Dim hddIdTipoTabla As HiddenField = CType(row.Cells(1).FindControl("hddIdTipoTablaSU"), HiddenField)
            Dim caja As TextBox = CType(row.Cells(1).FindControl("txtFilSinUso"), TextBox)

            list_SinUso = getSinUso()
            list_SinUso(row.RowIndex).objSubSinUso = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_SinUso(row.RowIndex).IdTipoTabla, caja.Text)

            'Select Case CInt(hddModo.Value)
            '    Case operativo.Insert
            '        'list_SinUso(row.RowIndex).objSubSinUso = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_SinUso(row.RowIndex).IdTipoTabla, caja.Text)
            '    Case operativo.Update
            '        list_SinUso(row.RowIndex).objSubSinUso = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), list_SinUso(row.RowIndex).IdTipoTabla, CInt(M_txtCodigo.Text))
            'End Select

            dgvSinUso.DataSource = list_SinUso
            dgvSinUso.DataBind()

            'MostrarGrillaTipoTablaValor(CInt(M_cmbLinea.SelectedValue), CInt(M_cmbSubLinea.SelectedValue), CInt(hddIdTipoTabla.Value), subgrilla)
        ElseIf chkActivar.Checked = False Then
            MostrarGrillaTipoTablaValor(CInt(0), CInt(0), CInt(0), subgrilla)



            Try
                Dim Lista_SU As List(Of Entidades.SubLinea_TipoTabla) = getListaCadenaSU()
                Dim hddIdTipoTabla As HiddenField = CType(row.Cells(1).FindControl("hddIdTipoTablaSU"), HiddenField)

                For i As Integer = 0 To Lista_SU.Count - 1

                    If Lista_SU(i).IdTipoTabla = CInt(hddIdTipoTabla.Value) Then
                        Dim SubLista As List(Of Entidades.SubLinea_TipoTablaValor) = CType(Lista_SU(i).objSubSinUso, List(Of Entidades.SubLinea_TipoTablaValor))

                        Dim ListaCadena As List(Of Entidades.SubLinea_TipoTabla) = Me.getListaCadena

                        For j As Integer = 0 To SubLista.Count - 1

                            Dim obj As New Entidades.SubLinea_TipoTabla

                            ListaCadena.RemoveAt(SubLista(j).NroOrden)
                            ListaCadena.Insert(SubLista(j).NroOrden, obj)

                        Next

                        setListaCadena(ListaCadena)
                    End If

                Next

                setListaCadenaSU(Lista_SU)

               
            Catch ex As Exception
            Finally
            End Try

            btnEvento_Click(sender, e)
        End If

        

    End Sub
    Private Function getListaKit() As List(Of Entidades.Kit)
        Return CType(Session.Item("listaKit"), List(Of Entidades.Kit))
    End Function
    Private Sub setListaKit(ByVal lista As List(Of Entidades.Kit))
        Session.Remove("listaKit")
        Session.Add("listaKit", lista)
    End Sub
    Private Function getListaCadena() As List(Of Entidades.SubLinea_TipoTabla)
        Return CType(Session.Item("listaCad"), List(Of Entidades.SubLinea_TipoTabla))
    End Function
    Private Sub setListaCadena(ByVal lista As List(Of Entidades.SubLinea_TipoTabla))
        Session.Remove("listaCad")
        Session.Add("listaCad", lista)
    End Sub
    Private Function getListaCadenaOR() As List(Of Entidades.SubLinea_TipoTabla)
        Return CType(Session.Item("listaCadOR"), List(Of Entidades.SubLinea_TipoTabla))
    End Function
    Private Sub setListaCadenaOR(ByVal lista As List(Of Entidades.SubLinea_TipoTabla))
        Session.Remove("listaCadOR")
        Session.Add("listaCadOR", lista)
    End Sub
    Private Function getListaCadenaCO() As List(Of Entidades.SubLinea_TipoTabla)
        Return CType(Session.Item("listaCadCO"), List(Of Entidades.SubLinea_TipoTabla))
    End Function
    Private Sub setListaCadenaCO(ByVal lista As List(Of Entidades.SubLinea_TipoTabla))
        Session.Remove("listaCadCO")
        Session.Add("listaCadCO", lista)
    End Sub
    Private Function getListaCadenaAT() As List(Of Entidades.SubLinea_TipoTabla)
        Return CType(Session.Item("listaCadAT"), List(Of Entidades.SubLinea_TipoTabla))
    End Function
    Private Sub setListaCadenaAT(ByVal lista As List(Of Entidades.SubLinea_TipoTabla))
        Session.Remove("listaCadAT")
        Session.Add("listaCadAT", lista)
    End Sub
    Private Function getListaCadenaSU() As List(Of Entidades.SubLinea_TipoTabla)
        Return CType(Session.Item("listaCadSU"), List(Of Entidades.SubLinea_TipoTabla))
    End Function
    Private Sub setListaCadenaSU(ByVal lista As List(Of Entidades.SubLinea_TipoTabla))
        Session.Remove("listaCadSU")
        Session.Add("listaCadSU", lista)
    End Sub


    Private Sub ListarSubLinea(ByVal idsublinea As Integer)
        Dim ngcSubLinea As New Negocio.SubLinea
        Dim objSubLinea As Entidades.SubLinea = ngcSubLinea.SelectxIdSubLinea(idsublinea)
        ViewState.Add("CodigoL", objSubLinea.CodigoLinea)
        ViewState.Add("NombreL", objSubLinea.NomLinea)
        ViewState.Add("OrdenL", objSubLinea.NroOrdenLinea)
        ViewState.Add("ParteNombreL", objSubLinea.ParteNombreLinea)
        ViewState.Add("TipoUsoL", objSubLinea.TipoUsoLinea)
        ViewState.Add("CodigoSL", objSubLinea.CodigoSubLinea)
        ViewState.Add("NombreSL", objSubLinea.Nombre)
        ViewState.Add("OrdenSL", objSubLinea.NroOrdenSubLinea)
        ViewState.Add("ParteNombreSL", objSubLinea.ParteNombreSubLinea)
        ViewState.Add("TipoUsoSL", objSubLinea.TipoUsoSubLinea)
    End Sub
    Protected Sub btnEvento_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEvento.Click


        If hddModo.Value = CStr(operativo.Update) Then

            btnSaveNewFromUpdate.Visible = True

        End If

        Dim obj As Entidades.SubLinea_TipoTabla
        Dim Lista As List(Of Entidades.SubLinea_TipoTabla) = getListaCadena()

        ' ORIGEN '        
        Dim ListaOR As List(Of Entidades.SubLinea_TipoTabla) = getListaCadenaOR()
        Try
            Dim chkMostrarOR As CheckBox = CType(dgvOrigen.Rows(CInt(hddIndexGrillaOR.Value)).Cells(0).FindControl("chkMostrarTTVOR"), CheckBox)
            Dim rowor As GridViewRow = CType(CType(chkMostrarOR, CheckBox).NamingContainer, GridViewRow)

            Dim SubGrillaOrigen As GridView = CType(rowor.Cells(2).FindControl("dgvSubOrigen"), GridView)
            If chkMostrarOR.Checked = True Then
                Dim chkActivarOR As CheckBox = CType(SubGrillaOrigen.Rows(CInt(hddIndexSubGrillaOR.Value)).Cells(0).FindControl("chkActivarOR"), CheckBox)
                Dim hddOrdenTTVOR As HiddenField = CType(SubGrillaOrigen.Rows(CInt(hddIndexSubGrillaOR.Value)).Cells(1).FindControl("hddOrdenTTVOR"), HiddenField)
                Dim hddParteNombreTTVOR As HiddenField = CType(SubGrillaOrigen.Rows(CInt(hddIndexSubGrillaOR.Value)).Cells(1).FindControl("hddParteNombreTTVOR"), HiddenField)
                Dim lblNomTTVOR As Label = CType(SubGrillaOrigen.Rows(CInt(hddIndexSubGrillaOR.Value)).Cells(2).FindControl("lblNomTTVOR"), Label)
                Dim lblCodigoTTVOR As Label = CType(SubGrillaOrigen.Rows(CInt(hddIndexSubGrillaOR.Value)).Cells(1).FindControl("lblCodigoTTVOR"), Label)
                Dim hddTipoUsoTTVOR As HiddenField = CType(SubGrillaOrigen.Rows(CInt(hddIndexSubGrillaOR.Value)).Cells(1).FindControl("hddTipoUsoTTVOR"), HiddenField)
                If chkActivarOR.Checked = True Then
                    obj = New Entidades.SubLinea_TipoTabla
                    With obj
                        .NroOrden = CInt(hddOrdenTTVOR.Value)
                        .ParteNombre = CBool(hddParteNombreTTVOR.Value)
                        .Codigo = LTrim(RTrim(CStr(lblCodigoTTVOR.Text)))
                        If CBool(hddParteNombreTTVOR.Value) = True Then
                            .Nombre = LTrim(RTrim(CStr(lblNomTTVOR.Text)))
                        End If
                        Lista.RemoveAt(.NroOrden)
                        Lista.Insert(.NroOrden, obj)

                        ListaOR.RemoveAt(.NroOrden)
                        ListaOR.Insert(.NroOrden, obj)

                        'remove

                        'fin remove


                    End With
                End If
            End If
        Catch ex As Exception
        Finally
        End Try

        ' CODIGO '
        Dim ListaCO As List(Of Entidades.SubLinea_TipoTabla) = getListaCadenaCO()
        Try
            Dim chkMostrarCO As CheckBox = CType(dgvCodigo.Rows(CInt(hddIndexGrillaCO.Value)).Cells(0).FindControl("chkMostrarTTVCO"), CheckBox)
            Dim rowco As GridViewRow = CType(CType(chkMostrarCO, CheckBox).NamingContainer, GridViewRow)
            Dim SubGrillaCodigo As GridView = CType(rowco.Cells(2).FindControl("dgvSubCodigo"), GridView)
            If chkMostrarCO.Checked = True Then
                Dim chkActivarCO As CheckBox = CType(SubGrillaCodigo.Rows(CInt(hddIndexSubGrillaCO.Value)).Cells(0).FindControl("chkActivarCO"), CheckBox)
                Dim hddOrdenTTVCO As HiddenField = CType(SubGrillaCodigo.Rows(CInt(hddIndexSubGrillaCO.Value)).Cells(1).FindControl("hddOrdenTTVCO"), HiddenField)
                Dim hddParteNombreTTVCO As HiddenField = CType(SubGrillaCodigo.Rows(CInt(hddIndexSubGrillaCO.Value)).Cells(1).FindControl("hddParteNombreTTVCO"), HiddenField)
                Dim lblNomTTVCO As Label = CType(SubGrillaCodigo.Rows(CInt(hddIndexSubGrillaCO.Value)).Cells(2).FindControl("lblNomTTVCO"), Label)
                Dim lblCodigoTTVCO As Label = CType(SubGrillaCodigo.Rows(CInt(hddIndexSubGrillaCO.Value)).Cells(1).FindControl("lblCodigoTTVCO"), Label)
                Dim hddTipoUsoTTVCO As HiddenField = CType(SubGrillaCodigo.Rows(CInt(hddIndexSubGrillaCO.Value)).Cells(1).FindControl("hddTipoUsoTTVCO"), HiddenField)
                If chkActivarCO.Checked = True Then
                    obj = New Entidades.SubLinea_TipoTabla
                    With obj
                        .NroOrden = CInt(hddOrdenTTVCO.Value)
                        .ParteNombre = CBool(hddParteNombreTTVCO.Value)
                        .Codigo = LTrim(RTrim(CStr(lblCodigoTTVCO.Text)))
                        If CBool(hddParteNombreTTVCO.Value) = True Then
                            .Nombre = LTrim(RTrim(CStr(lblNomTTVCO.Text)))
                        End If
                        Lista.RemoveAt(.NroOrden)
                        Lista.Insert(.NroOrden, obj)

                        ListaCO.RemoveAt(.NroOrden)
                        ListaCO.Insert(.NroOrden, obj)
                    End With
                End If
            End If
        Catch ex As Exception
        Finally
        End Try


        ' ATRIBUTOS '
        Dim ListaAT As List(Of Entidades.SubLinea_TipoTabla) = getListaCadenaAT()
        Try
            Dim chkMostrarAT As CheckBox = CType(dgvAtributos.Rows(CInt(hddIndexGrillaAT.Value)).Cells(0).FindControl("chkMostrarTTVAT"), CheckBox)
            Dim rowat As GridViewRow = CType(CType(chkMostrarAT, CheckBox).NamingContainer, GridViewRow)
            Dim SubGrillaAtributos As GridView = CType(rowat.Cells(2).FindControl("dgvSubAtributos"), GridView)
            If chkMostrarAT.Checked = True Then
                Dim chkActivarAT As CheckBox = CType(SubGrillaAtributos.Rows(CInt(hddIndexSubGrillaAT.Value)).Cells(0).FindControl("chkActivarAT"), CheckBox)
                Dim hddOrdenTTVAT As HiddenField = CType(SubGrillaAtributos.Rows(CInt(hddIndexSubGrillaAT.Value)).Cells(1).FindControl("hddOrdenTTVAT"), HiddenField)
                Dim hddParteNombreTTVAT As HiddenField = CType(SubGrillaAtributos.Rows(CInt(hddIndexSubGrillaAT.Value)).Cells(1).FindControl("hddParteNombreTTVAT"), HiddenField)
                Dim lblNomTTVAT As Label = CType(SubGrillaAtributos.Rows(CInt(hddIndexSubGrillaAT.Value)).Cells(2).FindControl("lblNomTTVAT"), Label)
                Dim lblCodigoTTVAT As Label = CType(SubGrillaAtributos.Rows(CInt(hddIndexSubGrillaAT.Value)).Cells(1).FindControl("lblCodigoTTVAT"), Label)
                Dim hddTipoUsoTTVAT As HiddenField = CType(SubGrillaAtributos.Rows(CInt(hddIndexSubGrillaAT.Value)).Cells(1).FindControl("hddTipoUsoTTVAT"), HiddenField)
                If chkActivarAT.Checked = True Then
                    obj = New Entidades.SubLinea_TipoTabla
                    With obj
                        .NroOrden = CInt(hddOrdenTTVAT.Value)
                        .ParteNombre = CBool(hddParteNombreTTVAT.Value)
                        .Codigo = LTrim(RTrim(CStr(lblCodigoTTVAT.Text)))
                        If CBool(hddParteNombreTTVAT.Value) = True Then
                            .Nombre = LTrim(RTrim(CStr(lblNomTTVAT.Text)))
                        End If
                        Lista.RemoveAt(.NroOrden)
                        Lista.Insert(.NroOrden, obj)

                        ListaAT.RemoveAt(.NroOrden)
                        ListaAT.Insert(.NroOrden, obj)
                    End With
                End If
            End If
        Catch ex As Exception
        Finally
        End Try

        ' SIN USO '
        Dim ListaSU As List(Of Entidades.SubLinea_TipoTabla) = getListaCadenaSU()
        Try
            Dim chkMostrarSU As CheckBox = CType(dgvSinUso.Rows(CInt(hddIndexGrillaSU.Value)).Cells(0).FindControl("chkMostrarTTVSU"), CheckBox)
            Dim rowsu As GridViewRow = CType(CType(chkMostrarSU, CheckBox).NamingContainer, GridViewRow)
            Dim SubGrillaSinUso As GridView = CType(rowsu.Cells(2).FindControl("dgvSubSinUso"), GridView)
            If chkMostrarSU.Checked = True Then
                Dim chkActivarSU As CheckBox = CType(SubGrillaSinUso.Rows(CInt(hddIndexSubGrillaSU.Value)).Cells(0).FindControl("chkActivarSU"), CheckBox)
                Dim hddOrdenTTVSU As HiddenField = CType(SubGrillaSinUso.Rows(CInt(hddIndexSubGrillaSU.Value)).Cells(1).FindControl("hddOrdenTTVSU"), HiddenField)
                Dim hddParteNombreTTVSU As HiddenField = CType(SubGrillaSinUso.Rows(CInt(hddIndexSubGrillaSU.Value)).Cells(1).FindControl("hddParteNombreTTVSU"), HiddenField)
                Dim lblNomTTVSU As Label = CType(SubGrillaSinUso.Rows(CInt(hddIndexSubGrillaSU.Value)).Cells(2).FindControl("lblNomTTVSU"), Label)
                Dim lblCodigoTTVSU As Label = CType(SubGrillaSinUso.Rows(CInt(hddIndexSubGrillaSU.Value)).Cells(1).FindControl("lblCodigoTTVSU"), Label)
                Dim hddTipoUsoTTVSU As HiddenField = CType(SubGrillaSinUso.Rows(CInt(hddIndexSubGrillaSU.Value)).Cells(1).FindControl("hddTipoUsoTTVSU"), HiddenField)
                If chkActivarSU.Checked = True Then
                    obj = New Entidades.SubLinea_TipoTabla
                    With obj
                        .NroOrden = CInt(hddOrdenTTVSU.Value)
                        .ParteNombre = CBool(hddParteNombreTTVSU.Value)
                        .Codigo = LTrim(RTrim(CStr(lblCodigoTTVSU.Text)))
                        .IdTipoTabla = CInt(CType(dgvSinUso.Rows(CInt(hddIndexGrillaSU.Value)).FindControl("hddIdTipoTablaSU"), HiddenField).Value)
                        If CBool(hddParteNombreTTVSU.Value) = True Then
                            .Nombre = LTrim(RTrim(CStr(lblNomTTVSU.Text)))
                        End If
                        Lista.RemoveAt(.NroOrden)
                        Lista.Insert(.NroOrden, obj)

                        obj.objSubSinUso = ListaSU(.NroOrden).objSubSinUso

                        ListaSU.RemoveAt(.NroOrden)
                        ListaSU.Insert(.NroOrden, obj)
                    End With
                End If

            End If
        Catch ex As Exception
        Finally
        End Try

        '************************'
        'ORIGEN'
        If CStr(ViewState("TipoUsoL")) = "OR" Then
            obj = New Entidades.SubLinea_TipoTabla
            With obj
                .NroOrden = CInt(ViewState("OrdenL"))
                .ParteNombre = CBool(ViewState("ParteNombreL"))
                .Codigo = LTrim(RTrim(CStr(ViewState("CodigoL"))))
                If CBool(ViewState("ParteNombreL")) = True Then
                    .Nombre = LTrim(RTrim(CStr(ViewState("NombreL"))))
                End If
                ListaOR.RemoveAt(.NroOrden)
                ListaOR.Insert(.NroOrden, obj)
                Lista.RemoveAt(.NroOrden)
                Lista.Insert(.NroOrden, obj)
            End With
        End If
        If CStr(ViewState("TipoUsoSL")) = "OR" Then
            obj = New Entidades.SubLinea_TipoTabla
            With obj
                .NroOrden = CInt(ViewState("OrdenSL"))
                .ParteNombre = CBool(ViewState("ParteNombreSL"))
                .Codigo = LTrim(RTrim(CStr(ViewState("CodigoSL"))))
                If CBool(ViewState("ParteNombreSL")) = True Then
                    .Nombre = LTrim(RTrim(CStr(ViewState("NombreSL"))))
                End If
                ListaOR.RemoveAt(.NroOrden)
                ListaOR.Insert(.NroOrden, obj)
                Lista.RemoveAt(.NroOrden)
                Lista.Insert(.NroOrden, obj)
            End With
        End If
        'CODIGO'
        If CStr(ViewState("TipoUsoL")) = "CO" Then
            obj = New Entidades.SubLinea_TipoTabla
            With obj
                .NroOrden = CInt(ViewState("OrdenL"))
                .ParteNombre = CBool(ViewState("ParteNombreL"))
                .Codigo = LTrim(RTrim(CStr(ViewState("CodigoL"))))
                If CBool(ViewState("ParteNombreL")) = True Then
                    .Nombre = LTrim(RTrim(CStr(ViewState("NombreL"))))
                End If
                ListaCO.RemoveAt(.NroOrden)
                ListaCO.Insert(.NroOrden, obj)
                Lista.RemoveAt(.NroOrden)
                Lista.Insert(.NroOrden, obj)
            End With
        End If
        If CStr(ViewState("TipoUsoSL")) = "CO" Then
            obj = New Entidades.SubLinea_TipoTabla
            With obj
                .NroOrden = CInt(ViewState("OrdenSL"))
                .ParteNombre = CBool(ViewState("ParteNombreSL"))
                .Codigo = LTrim(RTrim(CStr(ViewState("CodigoSL"))))
                If CBool(ViewState("ParteNombreSL")) = True Then
                    .Nombre = LTrim(RTrim(CStr(ViewState("NombreSL"))))
                End If
                ListaCO.RemoveAt(.NroOrden)
                ListaCO.Insert(.NroOrden, obj)
                Lista.RemoveAt(.NroOrden)
                Lista.Insert(.NroOrden, obj)
            End With
        End If
        'ATRIBUTOS'
        If CStr(ViewState("TipoUsoL")) = "AT" Then
            obj = New Entidades.SubLinea_TipoTabla
            With obj
                .NroOrden = CInt(ViewState("OrdenL"))
                .ParteNombre = CBool(ViewState("ParteNombreL"))
                .Codigo = LTrim(RTrim(CStr(ViewState("CodigoL"))))
                If CBool(ViewState("ParteNombreL")) = True Then
                    .Nombre = LTrim(RTrim(CStr(ViewState("NombreL"))))
                End If
                Lista.RemoveAt(.NroOrden)
                Lista.Insert(.NroOrden, obj)
            End With
        End If
        If CStr(ViewState("TipoUsoSL")) = "AT" Then
            obj = New Entidades.SubLinea_TipoTabla
            With obj
                .NroOrden = CInt(ViewState("OrdenSL"))
                .ParteNombre = CBool(ViewState("ParteNombreSL"))
                .Codigo = LTrim(RTrim(CStr(ViewState("CodigoSL"))))
                If CBool(ViewState("ParteNombreSL")) = True Then
                    .Nombre = LTrim(RTrim(CStr(ViewState("NombreSL"))))
                End If
                Lista.RemoveAt(.NroOrden)
                Lista.Insert(.NroOrden, obj)
            End With
        End If
        'SIN USO'
        If CStr(ViewState("TipoUsoL")) = "SU" Then
            obj = New Entidades.SubLinea_TipoTabla
            With obj
                .NroOrden = CInt(ViewState("OrdenL"))
                .ParteNombre = CBool(ViewState("ParteNombreL"))
                .Codigo = LTrim(RTrim(CStr(ViewState("CodigoL"))))
                If CBool(ViewState("ParteNombreL")) = True Then
                    .Nombre = LTrim(RTrim(CStr(ViewState("NombreL"))))
                End If
                Lista.RemoveAt(.NroOrden)
                Lista.Insert(.NroOrden, obj)
            End With
        End If
        If CStr(ViewState("TipoUsoSL")) = "SU" Then
            obj = New Entidades.SubLinea_TipoTabla
            With obj
                .NroOrden = CInt(ViewState("OrdenSL"))
                .ParteNombre = CBool(ViewState("ParteNombreSL"))
                .Codigo = LTrim(RTrim(CStr(ViewState("CodigoSL"))))
                If CBool(ViewState("ParteNombreSL")) = True Then
                    .Nombre = LTrim(RTrim(CStr(ViewState("NombreSL"))))
                End If
                Lista.RemoveAt(.NroOrden)
                Lista.Insert(.NroOrden, obj)
            End With
        End If
        '************************'

        If Lista IsNot Nothing Then

            Dim cad$ = String.Empty
            For x As Integer = 0 To Lista.Count - 1
                If Lista(x).NroOrden = 0 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 1 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 2 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 3 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 4 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 5 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 6 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 7 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 8 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 9 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 10 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 11 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 12 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 13 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 14 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 15 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 16 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 17 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 18 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 19 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 20 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 21 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 22 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 23 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 24 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 25 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 26 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 27 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 28 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 29 Then : cad = cad + Lista(x).Nombre + " " : End If
                If Lista(x).NroOrden = 30 Then : cad = cad + Lista(x).Nombre + " " : End If
            Next
            setListaCadena(Lista)
            M_txtDescripcion.Text = LTrim(RTrim(cad))

        End If

        'ORIGEN'

        If ListaOR IsNot Nothing Then

            Dim cadnroOR$ = String.Empty
            For x1 As Integer = 0 To ListaOR.Count - 1
                If ListaOR(x1).NroOrden = 0 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 1 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 2 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 3 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 4 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 5 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 6 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 7 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 8 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 9 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 10 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 11 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 12 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 13 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 14 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 15 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 16 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 17 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 18 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 19 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 20 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 21 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 22 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 23 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 24 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 25 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 26 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 27 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 28 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 29 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
                If ListaOR(x1).NroOrden = 30 Then : cadnroOR = cadnroOR + ListaOR(x1).Codigo : End If
            Next
            setListaCadenaOR(ListaOR)
            txtCodOrigen.Text = cadnroOR

        End If

        'CODIGO'

        If ListaCO IsNot Nothing Then

            Dim cadnroCO$ = String.Empty
            For x2 As Integer = 0 To ListaCO.Count - 1
                If ListaCO(x2).NroOrden = 0 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 1 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 2 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 3 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 4 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 5 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 6 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 7 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 8 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 9 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 10 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 11 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 12 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 13 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 14 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 15 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 16 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 17 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 18 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 19 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 20 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 21 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 22 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 23 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 24 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 25 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 26 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 27 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 28 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 29 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
                If ListaCO(x2).NroOrden = 30 Then : cadnroCO = cadnroCO + ListaCO(x2).Codigo : End If
            Next

            Dim objLongProd As New Entidades.Producto
            Dim ngcLongProd As New Negocio.Producto
            objLongProd = ngcLongProd.LongitudProducto()
            Dim ValLongProd As Integer = (objLongProd.LongitudProd - Len(cadnroCO))

            setListaCadenaCO(ListaCO)
            For xLong As Integer = 0 To ValLongProd - 1
                cadnroCO = cadnroCO + "0"
            Next
            txtCodCodigo.Text = cadnroCO
        End If

        'ATRIBUTOS'

        If ListaAT IsNot Nothing Then


            Dim cadnroAT$ = String.Empty
            For x3 As Integer = 0 To ListaAT.Count - 1
                If ListaAT(x3).NroOrden = 0 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 1 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 2 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 3 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 4 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 5 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 6 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 7 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 8 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 9 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 10 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 11 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 12 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 13 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 14 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 15 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 16 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 17 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 18 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 19 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 20 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 21 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 22 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 23 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 24 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 25 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 26 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 27 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 28 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 29 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
                If ListaAT(x3).NroOrden = 30 Then : cadnroAT = cadnroAT + ListaAT(x3).Codigo : End If
            Next
            setListaCadenaAT(ListaAT)
            hddCodAtributos.Value = cadnroAT + txtCodOrigen.Text

        End If

        'SIN USO'

        If ListaSU IsNot Nothing Then


            Dim cadnroSU$ = String.Empty
            For x4 As Integer = 0 To ListaSU.Count - 1
                If ListaSU(x4).NroOrden = 0 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 1 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 2 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 3 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 4 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 5 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 6 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 7 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 8 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 9 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 10 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 11 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 12 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 13 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 14 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 15 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 16 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 17 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 18 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 19 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 20 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 21 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 22 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 23 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 24 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 25 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 26 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 27 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 28 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 29 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
                If ListaSU(x4).NroOrden = 30 Then : cadnroSU = cadnroSU + ListaSU(x4).Codigo : End If
            Next
            setListaCadenaSU(ListaSU)
            hddCodSinUso.Value = cadnroSU
        End If

        'MOSTRANDO LOS CHECKBOX SELECCIONADOS
        'ORIGEN
        list_Origen = _getOrigen()
        dgvOrigen.DataSource = list_Origen
        dgvOrigen.DataBind()

        'CODIGO
        list_Codigo = _getCodigo()
        dgvCodigo.DataSource = list_Codigo
        dgvCodigo.DataBind()

        'ATRIBUTOS
        list_Atributos = _getAtributos()
        dgvAtributos.DataSource = list_Atributos
        dgvAtributos.DataBind()

        'SIN USO
        list_SinUso = _getSinUso()
        dgvSinUso.DataSource = list_SinUso
        dgvSinUso.DataBind()



        If hddModo.Value = CStr(operativo.Update) Then

            If Not IsNothing(ViewState("ProductoOrigen")) And Not IsNothing(ViewState("ProductoCodigo")) Then

                If ViewState("ProductoOrigen").ToString + ViewState("ProductoCodigo").ToString = Me.hddcodigoCompleto.Value Then

                    Me.txtCodCodigo.Text = ViewState("ProductoCodigo").ToString.Trim

                End If

            End If

        End If


    End Sub


    Private Function obtenerListaProductoTTV() As List(Of Entidades.ProductoTipoTablaValor)
        Dim obj As Entidades.ProductoTipoTablaValor
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)

        ' ORIGEN '        
        Try
            For or1 As Int32 = 0 To dgvOrigen.Rows.Count - 1
                Dim chkMostrarOR As CheckBox = CType(dgvOrigen.Rows(or1).Cells(0).FindControl("chkMostrarTTVOR"), CheckBox)
                Dim rowor As GridViewRow = CType(CType(chkMostrarOR, CheckBox).NamingContainer, GridViewRow)
                Dim SubGrillaOrigen As GridView = CType(rowor.Cells(2).FindControl("dgvSubOrigen"), GridView)
                Dim hddIdTipoTablaOR As HiddenField = CType(dgvOrigen.Rows(or1).Cells(1).FindControl("hddIdTipoTablaOR"), HiddenField)
                If chkMostrarOR.Checked = True Then
                    For sor1 As Int32 = 0 To SubGrillaOrigen.Rows.Count - 1
                        Dim chkActivarOR As CheckBox = CType(SubGrillaOrigen.Rows(sor1).Cells(0).FindControl("chkActivarOR"), CheckBox)
                        Dim hddIdTTVOR As HiddenField = CType(SubGrillaOrigen.Rows(sor1).Cells(2).FindControl("hddIdTTVOR"), HiddenField)
                        If chkActivarOR.Checked = True Then
                            obj = New Entidades.ProductoTipoTablaValor
                            With obj
                                .IdTipoTabla = CInt(hddIdTipoTablaOR.Value)
                                .IdTipoTablaValor = CInt(hddIdTTVOR.Value)
                            End With
                            lista.Add(obj)
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
        Finally
        End Try

        ' CODIGO '
        Try
            For co1 As Int32 = 0 To dgvCodigo.Rows.Count - 1
                Dim chkMostrarCO As CheckBox = CType(dgvCodigo.Rows(co1).Cells(0).FindControl("chkMostrarTTVCO"), CheckBox)
                Dim rowco As GridViewRow = CType(CType(chkMostrarCO, CheckBox).NamingContainer, GridViewRow)
                Dim SubGrillaCodigo As GridView = CType(rowco.Cells(2).FindControl("dgvSubCodigo"), GridView)
                Dim hddIdTipoTablaCO As HiddenField = CType(dgvCodigo.Rows(co1).Cells(1).FindControl("hddIdTipoTablaCO"), HiddenField)
                If chkMostrarCO.Checked = True Then
                    For sco1 As Int32 = 0 To SubGrillaCodigo.Rows.Count - 1
                        Dim chkActivarCO As CheckBox = CType(SubGrillaCodigo.Rows(sco1).Cells(0).FindControl("chkActivarCO"), CheckBox)
                        Dim hddIdTTVCO As HiddenField = CType(SubGrillaCodigo.Rows(sco1).Cells(2).FindControl("hddIdTTVCO"), HiddenField)
                        If chkActivarCO.Checked = True Then
                            obj = New Entidades.ProductoTipoTablaValor
                            With obj
                                .IdTipoTabla = CInt(hddIdTipoTablaCO.Value)
                                .IdTipoTablaValor = CInt(hddIdTTVCO.Value)
                            End With
                            lista.Add(obj)
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
        Finally
        End Try


        ' ATRIBUTOS '
        Try
            For at1 As Int32 = 0 To dgvAtributos.Rows.Count - 1
                Dim chkMostrarAT As CheckBox = CType(dgvAtributos.Rows(at1).Cells(0).FindControl("chkMostrarTTVAT"), CheckBox)
                Dim rowat As GridViewRow = CType(CType(chkMostrarAT, CheckBox).NamingContainer, GridViewRow)
                Dim SubGrillaAtributos As GridView = CType(rowat.Cells(2).FindControl("dgvSubAtributos"), GridView)
                Dim hddIdTipoTablaAT As HiddenField = CType(dgvAtributos.Rows(at1).Cells(1).FindControl("hddIdTipoTablaAT"), HiddenField)
                If chkMostrarAT.Checked = True Then
                    For sat1 As Int32 = 0 To SubGrillaAtributos.Rows.Count - 1
                        Dim chkActivarAT As CheckBox = CType(SubGrillaAtributos.Rows(sat1).Cells(0).FindControl("chkActivarAT"), CheckBox)
                        Dim hddIdTTVAT As HiddenField = CType(SubGrillaAtributos.Rows(sat1).Cells(2).FindControl("hddIdTTVAT"), HiddenField)
                        If chkActivarAT.Checked = True Then
                            obj = New Entidades.ProductoTipoTablaValor
                            With obj
                                .IdTipoTabla = CInt(hddIdTipoTablaAT.Value)
                                .IdTipoTablaValor = CInt(hddIdTTVAT.Value)
                            End With
                            lista.Add(obj)
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
        Finally
        End Try

        ' SIN USO '
        Try
            For su1 As Int32 = 0 To dgvSinUso.Rows.Count - 1
                Dim chkMostrarSU As CheckBox = CType(dgvSinUso.Rows(su1).Cells(0).FindControl("chkMostrarTTVSU"), CheckBox)
                Dim rowsu As GridViewRow = CType(CType(chkMostrarSU, CheckBox).NamingContainer, GridViewRow)
                Dim SubGrillaSinUso As GridView = CType(rowsu.Cells(2).FindControl("dgvSubSinUso"), GridView)

                Dim hddIdTipoTablaSU As HiddenField = CType(dgvSinUso.Rows(su1).Cells(1).FindControl("hddIdTipoTablaSU"), HiddenField)

                If chkMostrarSU.Checked = True Then
                    For ssu1 As Int32 = 0 To SubGrillaSinUso.Rows.Count - 1
                        Dim chkActivarSU As CheckBox = CType(SubGrillaSinUso.Rows(ssu1).Cells(0).FindControl("chkActivarSU"), CheckBox)
                        Dim hddIdTTVSU As HiddenField = CType(SubGrillaSinUso.Rows(ssu1).Cells(2).FindControl("hddIdTTVSU"), HiddenField)
                        If chkActivarSU.Checked = True Then
                            obj = New Entidades.ProductoTipoTablaValor
                            With obj
                                .IdTipoTabla = CInt(hddIdTipoTablaSU.Value)
                                .IdTipoTablaValor = CInt(hddIdTTVSU.Value)
                            End With
                            lista.Add(obj)
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
        Finally
        End Try
        Return lista
    End Function
    Public Sub LlenarCboTipoTablaxIdSubLinea(ByVal cbo As DropDownList, ByVal idsublinea As Integer, Optional ByVal addElemento As Boolean = False)
        Dim ngc As New Negocio.SubLinea_TipoTabla
        Dim lista As List(Of Entidades.SubLinea_TipoTabla) = ngc.SelectAllTipoTablaxIdSubLinea(idsublinea)
        If addElemento Then
            Dim obj As New Entidades.SubLinea_TipoTabla(0, "Seleccione Tipo Tabla", True)
            lista.Insert(0, obj)
        End If
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdTipoTabla"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles M_B_cmbSubLinea.SelectedIndexChanged
        LlenarCboTipoTablaxIdSubLinea(cboTipoTablaBus, CInt(M_B_cmbSubLinea.SelectedValue), True)
        If CInt(M_B_cmbSubLinea.SelectedValue) = 0 Then
            dgvFiltroTipoTabla.DataBind()
            hddModoVal.Value = "0"
        End If
    End Sub
    'Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
    '    ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
    '    For Each fila As GridViewRow In dgvFiltroTipoTabla.Rows
    '        objSLTT = New Entidades.SubLinea_TipoTabla
    '        With objSLTT
    '            .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
    '            .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
    '            Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
    '            .IdTipoTablaValor = CInt(cboArea.SelectedValue)
    '            ListaTTV = New List(Of Entidades.TipoTablaValor)
    '            For x As Integer = 0 To cboArea.Items.Count - 1
    '                ObjTTV = New Entidades.TipoTablaValor
    '                With ObjTTV
    '                    .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
    '                    .Nombre = cboArea.Items(x).Text
    '                End With
    '                ListaTTV.Add(ObjTTV)
    '            Next
    '            .objTipoTabla = ListaTTV
    '        End With
    '        ListaSLTT.Add(objSLTT)
    '    Next
    '    Return ListaSLTT
    'End Function
    'Private Sub LLenarGrillaTipoTablaValor()
    '    ListaSLTT = getListaTipoTablaValor()
    '    objSLTT = New Entidades.SubLinea_TipoTabla
    '    With objSLTT
    '        .IdTipoTablaValor = 0
    '        .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_B_cmbLinea.SelectedValue), CInt(M_B_cmbSubLinea.SelectedValue), CInt(cboTipoTablaBus.SelectedValue))
    '        .Nombre = CStr(cboTipoTablaBus.SelectedItem.Text)
    '        .IdTipoTabla = CInt(cboTipoTablaBus.SelectedValue)
    '    End With
    '    ListaSLTT.Add(objSLTT)
    '    dgvFiltroTipoTabla.DataSource = ListaSLTT
    '    dgvFiltroTipoTabla.DataBind()
    'End Sub
    Protected Sub btnAgregarBus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregarBus.Click
        Try
            hddModoVal.Value = "1"
            LLenarGrillaTipoTablaValor()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub dgvFiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvFiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub dgvFiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvFiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(dgvFiltroTipoTabla.SelectedIndex)
            dgvFiltroTipoTabla.DataSource = ListaSLTT
            dgvFiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To dgvFiltroTipoTabla.Rows.Count - 1
            With dgvFiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.dgvFiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.dgvFiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(M_B_cmbLinea.SelectedValue), CInt(M_B_cmbSubLinea.SelectedValue), CInt(cboTipoTablaBus.SelectedValue))
            .Nombre = CStr(cboTipoTablaBus.SelectedItem.Text)
            .IdTipoTabla = CInt(cboTipoTablaBus.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        dgvFiltroTipoTabla.DataSource = ListaSLTT
        dgvFiltroTipoTabla.DataBind()
    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In dgvFiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

#End Region


#Region "************************************     BUSCAR PRODUCTO COMPONENTE"

#Region "************************** BUSQUEDA PRODUCTO"

    Private Sub cbo_TipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_TipoExistencia.SelectedIndexChanged
        drop = New Combo
        drop.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cbo_TipoExistencia.SelectedValue), True)
        drop.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cmbSubLinea_AddProd, CInt(cmbLinea_AddProd.SelectedValue), CInt(cbo_TipoExistencia.SelectedValue), True)

        objScript.onCapa(Me, "capaBuscarProducto_AddProd")

    End Sub
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdTipoExistencia", CInt(Me.cbo_TipoExistencia.SelectedValue))
                    ViewState.Add("IdAlmacen_BuscarProducto", -1)
                    ViewState.Add("IdEmpresa_BuscarProducto", -1)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, _
                                          ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, _
                                          ByVal nomProducto As String, ByVal IdAlmacen As Integer, _
                                          ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, _
                                          ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, -1, -1, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)

        If Me.listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = Me.listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            drop = New Combo
            drop.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cmbSubLinea_AddProd, CInt(cmbLinea_AddProd.SelectedValue), CInt(cbo_TipoExistencia.SelectedValue), True)
            drop.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor_Kit()
        ListaSLTT = getListaTipoTablaValor_Kit()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor_Kit()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor_Kit()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla_Kit() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla0"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV0"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub cmbSubLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor_Kit() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla_Kit()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

#End Region


    Private Sub btnAddProductos_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos_AddProd.Click
        valOnClick_btnAddProductos_AddProd()
    End Sub
    Private Sub valOnClick_btnAddProductos_AddProd()

        Try

            actualizarListaKit()

            Dim listaMoneda As List(Of Entidades.Moneda) = (New Negocio.Moneda).SelectCbo

            Me.listaKit = getListaKit()

            For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

                If (CType(Me.DGV_AddProd.Rows(i).FindControl("chb_AddDetalle"), CheckBox).Checked) Then

                    Dim IdProducto As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                    'Dim idTienda As Integer = 1
                    For x As Integer = 0 To Me.listaKit.Count - 1

                        If (Me.listaKit(x).IdComponente = IdProducto) Then
                            Throw New Exception("EL COMPONENTE YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                        End If

                    Next

                    Dim Producto As String = HttpUtility.HtmlDecode(CStr(CType(Me.DGV_AddProd.Rows(i).FindControl("lblComponente"), Label).Text))
                    Dim CodigoProducto As String = HttpUtility.HtmlDecode(CStr(CType(Me.DGV_AddProd.Rows(i).FindControl("lblCodigoProducto"), Label).Text))
                    Dim listaUnidadMedida As List(Of Entidades.ProductoUMView) = (New Negocio.ProductoUMView).SelectCboxIdProducto(IdProducto)


                    'Dim precio As Decimal = (New Negocio.Producto).SelectPrecioVenta(IdProducto, idTienda)
                    Dim precio As Decimal = (New Negocio.Producto).SelectPrecioCompra(IdProducto)
                    Dim objKit As New Entidades.Kit
                    With objKit
                        .IdComponente = IdProducto
                        .Componente = Producto
                        .CodigoProd_Comp = CodigoProducto
                        .ListaProductoUM = listaUnidadMedida
                        .ListaMoneda = listaMoneda
                        .Cantidad_Comp = 0
                        .Precio_Comp = precio
                    End With

                    Me.listaKit.Add(objKit)

                End If

            Next

            setListaKit(Me.listaKit)

            Me.GV_ComponenteKit.DataSource = Me.listaKit
            Me.GV_ComponenteKit.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub actualizarListaKit()

        Me.listaKit = getListaKit()

        For i As Integer = 0 To Me.GV_ComponenteKit.Rows.Count - 1

            Me.listaKit(i).Cantidad_Comp = CDec(CType(Me.GV_ComponenteKit.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaKit(i).Precio_Comp = CDec(CType(Me.GV_ComponenteKit.Rows(i).FindControl("txtPrecioRef"), TextBox).Text)
            Me.listaKit(i).IdUnidadMedida_Comp = CInt(CType(Me.GV_ComponenteKit.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaKit(i).IdMonedaPrecio_Comp = CInt(CType(Me.GV_ComponenteKit.Rows(i).FindControl("cboMoneda"), DropDownList).SelectedValue)
            Me.listaKit(i).PorcentajeKit = CInt(CType(Me.GV_ComponenteKit.Rows(i).FindControl("txtporcentaje"), TextBox).Text)
        Next

        setListaKit(Me.listaKit)

    End Sub
    Private Sub GV_ComponenteKit_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ComponenteKit.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim cboUnidadMedida As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
            Dim IdUnidadMedida As Integer = Me.listaKit(e.Row.RowIndex).IdUnidadMedida_Comp

            If (cboUnidadMedida.Items.FindByValue(CStr(IdUnidadMedida)) IsNot Nothing) Then
                cboUnidadMedida.SelectedValue = CStr(IdUnidadMedida)
            End If

            Dim cboMoneda As DropDownList = CType(e.Row.FindControl("cboMoneda"), DropDownList)
            Dim IdMoneda As Integer = Me.listaKit(e.Row.RowIndex).IdMonedaPrecio_Comp

            If (cboMoneda.Items.FindByValue(CStr(IdMoneda)) IsNot Nothing) Then
                cboMoneda.SelectedValue = CStr(IdMoneda)
            End If

        End If

    End Sub
    Private Sub GV_ComponenteKit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_ComponenteKit.SelectedIndexChanged
        quitarComponente_Kit(Me.GV_ComponenteKit.SelectedIndex)
    End Sub
    Private Sub quitarComponente_Kit(ByVal index As Integer)

        Try

            actualizarListaKit()

            Me.listaKit = getListaKit()

            Me.listaKit.RemoveAt(index)

            setListaKit(Me.listaKit)

            Me.GV_ComponenteKit.DataSource = Me.listaKit
            Me.GV_ComponenteKit.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaKit_Load(ByVal IdProducto As Integer) As List(Of Entidades.Kit)

        Dim lista As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponentexIdKit(IdProducto)

        Dim listaMoneda As List(Of Entidades.Moneda) = (New Negocio.Moneda).SelectCbo()

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaMoneda = listaMoneda
            lista(i).ListaProductoUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(lista(i).IdComponente)

        Next

        Return lista

    End Function
    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEliminar.Click
        If M_txtCodigo.Text = "" Then
            objScript.mostrarMsjAlerta(Me, "Primero busque un producto para poder eliminar.")
            Exit Sub
        End If
        Dim ngcProd As New Negocio.Producto
        If ngcProd.EliminarxIdProducto(CInt(Me.M_txtCodigo.Text)) Then
            objScript.mostrarMsjAlerta(Me, "Se elimino correctamente el producto.")
            verFrmInicio()
            Me.btnEliminar.Visible = False
        Else
            objScript.mostrarMsjAlerta(Me, "Error al eliminar porque éxisten documentos asociados al producto.")
        End If
    End Sub
    Private Sub btnSaveNewFromUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNewFromUpdate.Click
        guardarProducto(operativo.Insert)
    End Sub


#Region "Busqueda de productos"
    Protected Sub M_btnFiltrar_B_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles M_btnFiltrar_B.Click
        Try
            mostrarMensaje(False, "")

            Dim IdTipoExistencia As Integer = 0
            Dim idLinea As Integer = 0
            Dim idSubLinea As Integer = 0
            Dim idEstadoProd As Integer = 0
            Dim nombre As String = ""
            Dim codigo As String = ""
            Dim codprov As String = ""
            If IsNumeric(cmbTipoExistencia.SelectedValue) Then IdTipoExistencia = CInt(cmbTipoExistencia.SelectedValue)
            If IsNumeric(M_B_cmbLinea.SelectedValue) Then idLinea = CInt(M_B_cmbLinea.SelectedValue)
            If IsNumeric(M_B_cmbSubLinea.SelectedValue) Then idSubLinea = CInt(M_B_cmbSubLinea.SelectedValue)
            If IsNumeric(M_B_cmbEstado.SelectedValue) Then idEstadoProd = CInt(M_B_cmbEstado.SelectedValue)

            Select Case CStr(Me.rdbCodNombre.SelectedValue)
                Case "3"
                    codprov = M_B_txtDescripcion.Text.Trim
                Case "2"
                    nombre = M_B_txtDescripcion.Text.Trim
                Case "1"
                    codigo = M_B_txtDescripcion.Text.Trim
            End Select

            Dim Tb As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

            ViewState.Add("_IdTipoExistencia", IdTipoExistencia)
            ViewState.Add("_IdLinea", idLinea)
            ViewState.Add("_IdSubLinea", idSubLinea)
            ViewState.Add("_Estado", idEstadoProd)
            ViewState.Add("_nombre", nombre)
            ViewState.Add("_codigo", codigo)
            ViewState.Add("_codprov", codprov)
            BuscarMantenimientoProducto(0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub BuscarMantenimientoProducto(ByVal tipoMov As Integer)
        BuscarMantenimientoProducto_Paginado(CInt(ViewState("_IdTipoExistencia")), CInt(ViewState("_IdLinea")), CInt(ViewState("_IdSubLinea")), CInt(ViewState("_Estado")), CStr(ViewState("_nombre")), CStr(ViewState("_codigo")), CStr(ViewState("_codprov")), tipoMov, DGV_B)
    End Sub
    Private Sub BuscarMantenimientoProducto_Paginado(ByVal IdTipoExistencia As Integer, ByVal idLinea As Integer, _
                                                     ByVal idSubLinea As Integer, ByVal idEstadoProd As Integer, _
                                                     ByVal nombre As String, ByVal codigo As String, ByVal codprov As String, _
                                                     ByVal TipoMov As Integer, ByVal Grilla As GridView)


        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txt_PageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txt_PageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txt_PageIndexGO.Text) - 1)
        End Select

        Dim Tb As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()
        Dim lista As List(Of Entidades.GrillaProducto_M) = (New Negocio.Producto).BuscarProducto_Paginado(IdTipoExistencia, idLinea, idSubLinea, idEstadoProd, nombre.Trim, codigo.Trim, codprov.Trim, Tb, index, Grilla.PageSize)
        If lista.Count > 0 Then
            Panel_B.Visible = True
            txt_PageIndex.Text = CStr(index + 1)
            Grilla.DataSource = lista
            Grilla.DataBind()
        Else
            objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
        End If

    End Sub

    Private Sub btn_anterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_anterior.Click
        BuscarMantenimientoProducto(1)
    End Sub
    Private Sub btn_siguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_siguiente.Click
        BuscarMantenimientoProducto(2)
    End Sub
    Private Sub btn_Ir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Ir.Click
        BuscarMantenimientoProducto(3)
    End Sub
#End Region


    
End Class

' Lunes, 07 de Febrero de 2011
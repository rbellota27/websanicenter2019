<%@ Page Title="Vehiculo" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantVehiculo.aspx.vb" Inherits="APPWEB.FrmMantVehiculo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <asp:UpdatePanel ID="upVehiculos" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td class="TituloCelda" style="height: 21px">
                        Vehiculo</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table style="width: 100%">
        <tr>
            <td style="height: 41px">
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='../Imagenes/Nuevo_A.JPG';"
                                onmouseout="this.src='../Imagenes/Nuevo_b.JPG';" />
                            &nbsp;
                            <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG" 
                                onmouseover="this.src='../Imagenes/Guardar_A.JPG';" OnClientClick="return(valSaveVehiculo());" onmouseout="this.src='../Imagenes/Guardar_B.JPG';"
                                CausesValidation="true" Visible="False" />
                            <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                onmouseover="this.src='../Imagenes/Cancelar_A.JPG';" onmouseout="this.src='../Imagenes/Cancelar_B.JPG';"
                                OnClientClick="return( confirm('Desea cancelar el proceso?')   );" Width="70px" />
                            </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel_Vehiculo" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 120px">
                                <asp:Label ID="LblTipoExistencia" runat="server" CssClass="Label" 
                                    Text="Tipo de Existencia:"></asp:Label>
                            </td>
                            <td style="width: 169px">
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="True" 
                                    Style="height: 22px;" Height="22px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 49px">
                                <asp:Label ID="lblLinea" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td style="width: 220px">
                                <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="True" 
                                    Height="22px" Style="height: 22px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 81px">
                                <asp:Label ID="LblSubLinea" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSubLinea" runat="server" 
                                    style="height: 22px;" 
                                    Height="22px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 120px">
                                <asp:Label ID="LblPlaca" runat="server" CssClass="Label" Text="Placa"></asp:Label>
                            </td>
                            <td style="width: 169px">
                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="TxtPlaca" runat="server" onKeypress="return(alfanumerico());" style="margin-left: 0px"></asp:TextBox>
                            </td>
                            <td style="width: 49px">
                                <asp:Label ID="LblModelo" runat="server" CssClass="Label" Text="Modelo"></asp:Label>
                            </td>
                            <td style="width: 220px">
                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="TxtModelo" runat="server" 
                                   style="margin-left: 0px"></asp:TextBox>
                            </td>
                            <td style="width: 81px">
                                <asp:Label ID="LblConstancia" runat="server" CssClass="Label" 
                                    Text="Constancia:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="TxtConstancia" runat="server"></asp:TextBox>
                            </td>
                            <td style="width: 81px">
                                <asp:Label ID="Label1" runat="server" CssClass="Label" 
                                    Text="Capacidad (Kg):"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCapacidad" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 120px">
                                <asp:Label ID="LblConfeccion" runat="server" CssClass="Label" 
                                    Text="Confeccion:"></asp:Label>
                            </td>
                            <td style="width: 169px">
                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="TxtConfeccion" runat="server"></asp:TextBox>
                            </td>
                            <td style="width: 49px">
                                <asp:Label ID="LblTara" runat="server" CssClass="Label" Text="Tara:"></asp:Label>
                            </td>
                            <td style="width: 220px">
                                <asp:TextBox ID="TxtTara" runat="server" 
                                    onKeypress="return(validarNumeroPunto(event));" style="margin-left: 0px"></asp:TextBox>
                            </td>
                            <td style="width: 81px">
                                <asp:Label ID="LblEstado" runat="server" CssClass="Label" Text="Estado:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" style="height: 22px;" 
                                    Height="22px" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
          <tr>
            <td> 
            <asp:Panel ID="Panel_Busqueda" runat="server">
                
                <table style="margin-bottom: 0px">
        <tr>
            <td style="width: 121px">
                <asp:Label ID="Label40" runat="server" CssClass="Label" Text="Filtrar por:"></asp:Label>
            </td>
            <td style="width: 183px">
                <asp:DropDownList ID="cboFiltro" runat="server" Width="109px">
                    <asp:ListItem>Placa</asp:ListItem>
                    <asp:ListItem>Modelo</asp:ListItem>
                    <asp:ListItem>Constancia</asp:ListItem>
                    <asp:ListItem>EstadoProducto</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 121px"> 
                                                <asp:Label ID="E_B_lblTextoBusq" runat="server" Text="Texto de B�squeda:" 
                                                    CssClass="Label"></asp:Label>
                                            </td>
            <td style="width: 183px">
                <asp:Panel ID="Panel1" runat="server" DefaultButton="E_btnFiltrar_B">                
                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="V_txtTextoBusqueda" runat="server" 
                                                    onkeypress="return(onKeyPressEnter(event));" Width="205px"></asp:TextBox>
                                                    </asp:Panel>
            </td>
            <td style="width: 43px">
                <asp:ImageButton ID="E_btnFiltrar_B" runat="server" 
                    ImageUrl="~/Imagenes/Busqueda_B.jpg" 
                    onmouseout="this.src='../Imagenes/Busqueda_B.JPG';" OnClientClick="return(validarBuscarVehiculo());"
                    onmouseover="this.src='../Imagenes/Busqueda_A.JPG';" />
            </td>
            <td style="width: 151px">
                <asp:Label ID="lbl_MensajeBusq" runat="server" CssClass="LabelRojo" 
                    Text="No se hallaron registros."></asp:Label>
            </td>
            <td>
                <asp:HiddenField ID="HDD_IdProducto" runat="server" />
            </td>
        </tr>
          </table>
               </asp:Panel>
          <table >
          <tr>
          <td>
                    &nbsp;</td>
          </tr>
          </table>
            </td>
        </tr>
                    <asp:GridView ID="gvVehiculos" runat="server" AutoGenerateColumns="False" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">                        
                        <Columns>
                            <asp:TemplateField HeaderText="Eliminar">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbEliminar" runat="server" OnClick ="Click_Eliminar" OnClientClick="return(confirm('Desea Eliminar el registro?'));">Eliminar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbEditar" runat="server" OnClick ="Click_Editar" >Editar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo de Existencia">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HDD_IdTipoExistencia" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoExistencia") %>' />
                                    <asp:Label ID="lbl_NomTipoExistencia" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"nombreTipoExistencia") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="L�nea">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_IdLinea" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdLinea") %>' />
                                    <asp:Label ID="lblNomLinea" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"NomLinea") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sub L�nea">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_IdSubLinea" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdSubLinea") %>' />
                                    <asp:Label ID="lblNomSubLinea" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"NomSubLinea") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Placa">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HDD_PlacaxIdProducto" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />
                                    <asp:Label ID="Lbl_NomPlaca" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"Placa") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Modelo" HeaderText="Modelo" />
                            <asp:BoundField DataField="ConstanciaIns" HeaderText="Constancia" />
                            <asp:BoundField DataField="ConfVeh" HeaderText="Confecci�n" />
                            <asp:BoundField DataField="Tara" HeaderText="Tara" />
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_IdEstadoProd" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdEstadoProd") %>' />
                                    <asp:Label ID="lblEstadoProd" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"NomEstadoProd") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="veh_Capacidad" HeaderText="Capacidad Kg." />
                        </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                                                                  <FooterStyle CssClass="GrillaFooter" />
                                                                  <PagerStyle CssClass="GrillaPager" />
                                                                  <RowStyle CssClass="GrillaRow" />
                                                                  <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                  <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                  <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
    </table>
    
    <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />

<script language="javascript" type="text/javascript">

  
    /*Solo puede ingresar N�MEROS*/
    function validarNumeroPunto(event) {
        var key = event.keyCode;
        if (key == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                return false;
            }
        }
    }


    function valSaveVehiculo() {

        var cbolinea = document.getElementById('<%=cbolinea.ClientID%>');
        if (isNaN(cbolinea.value) || parseInt(cbolinea.value) <= 0 || cbolinea.value.length <= 0) {
            alert('Ingrese la L�nea.');
            return false
        }
        var cboSubLinea = document.getElementById('<%=cboSubLinea.ClientID %>');
        if (isNaN(cboSubLinea.value) || parseInt(cboSubLinea.value) <= 0 || cboSubLinea.value.length <= 0) {
            alert('Ingrese una Sub L�nea.');
            return false;
        }

        //****************** Validamos el cliente
        var Txt1 = document.getElementById('<%=txtPlaca.ClientID %>');
        if (CajaEnBlanco(Txt1)) {
            alert('Debe ingresar el nombre de la PLACA.');
            return false;
        }

        var txtTara = document.getElementById('<%=TxtTara.ClientID %>');
        var valorTara = parseFloat(txtTara.value);
        if (txtTara.value.length > 0) {
            if (   isNaN( valorTara  )   ) {
                alert('Debe ingresar un valor v�lido.');
                txtTara.select();
                txtTara.focus();
                return false;
            }    
        }
        
        
        return (confirm('Desea Guardar?'));
    }


    function validarBuscarVehiculo() {
        var caja = document.getElementById('<%=V_txtTextoBusqueda.ClientID %>');
        if (CajaEnBlanco(caja)) {
            alert('Ingrese un c�digo de b�squeda.');
            caja.focus();
            return false;
        }
        return true;
    }

    
    function alfanumerico() {

   var tecla = window.event.keyCode;
     if(65<=tecla && tecla<=90 || 97<=tecla && tecla<=122 || 48<=tecla && tecla<=57){
     }else{
      window.event.keyCode=0;
  }
 
}



//////////////////////////////////////////
var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
/////////////////////////////////////////
   
    
    
  </script>

</asp:Content>


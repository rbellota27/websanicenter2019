﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmMantProcesos.aspx.vb" Inherits="APPWEB.FrmMantProcesos"  MasterPageFile="~/Principal.Master"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:content ID="ContentProceso" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table  width="100%">
        <tr>
            <td>
                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseout="this.src='/Imagenes/Nuevo_b.JPG';"
                    onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" />

                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                    onmouseout="this.src='/Imagenes/Guardar_B.JPG';" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                     />
              

                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                    onmouseout="this.src='/Imagenes/Arriba_B.JPG';" onmouseover="this.src='/Imagenes/Arriba_A.JPG';"
                    ToolTip="Regresar a Administración Sistema" />
               </td>
        </tr>
         <tr>
            <td class="TituloCelda">
                Procesos
            </td>
        </tr>

        <tr>
            <td>
                <asp:Panel ID="Panel_Procesos" runat="server" Width="100%">
                    <table cellpadding="0" cellspacing="3" width="80%">
                        <tr>
                            <td class="Texto" style="width: 34px">&nbsp;IdProceso:</td>
                            <td><asp:TextBox ID="txtId" runat="server" CssClass="TextBoxReadOnly" 
                                    ReadOnly="True" Width="51px"></asp:TextBox></td>
                        </tr>

                         <tr>
                            <td class="Texto" style="width: 34px">Codigo:</td>
                            <td><asp:TextBox ID="txtCodigo" runat="server" MaxLength="25" Width="91px" ></asp:TextBox></td>
                        </tr>

                        <tr>
                            <td class="Texto" style="width: 34px">Descripcion:</td>
                            <td><asp:TextBox ID="txtdescripcion" runat="server" MaxLength="25" Width="254px" ></asp:TextBox></td>
                        </tr>
                        
                        <tr> 
                            <td class="Texto" style="width: 34px">Nombre:</td>
                            <td><asp:TextBox ID="TxtNombre" runat="server" MaxLength="25" Width="94px" ></asp:TextBox> </td>                            
                        </tr>

                        <tr>
                        <td class="Texto" style="width: 34px">Estado:</td>
                        <td><asp:CheckBox ID="chkActivo" runat="server" Text="Activo" /></td>
                        </tr>                       
                        <tr>
                        <td class="Texto" style="width: 34px">Modulo:</td>
                        <td><asp:DropDownList ID="ddlModulo" runat="server"></asp:DropDownList></td>
                        </tr>
                        <tr>
                         <td colspan=2></td>
                        </tr>
                          </asp:Panel>
          <asp:Panel ID="PanelLista" runat="server" Width="100%">
                       <tr>
            <td style="width: 50px" colspan=2>
                <asp:GridView ID="DGV_ProcesosBusqueda" Width="70%" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="1" EmptyDataText="No existe información."
                    ForeColor="#333333" GridLines="None"  DatakeyNames="Id"  >
                    <RowStyle BackColor="#F7F6F3" CssClass="GrillaRow" ForeColor="#333333" />
                    <Columns>
                    
                         <asp:BoundField DataField="Id" HeaderText="IDProceso" NullDisplayText="0" >
                            <ItemStyle HorizontalAlign="Center" />
                         </asp:BoundField>
                          <asp:BoundField DataField="Codigo" HeaderText="Codigo" NullDisplayText="-----" />
                         <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" NullDisplayText="-----" />
                         <asp:BoundField DataField="Nombre" HeaderText="Nom_Proceso" NullDisplayText="-----" />
                         <asp:BoundField DataField="Estado" HeaderText="Estado" NullDisplayText="-----" />
                         <asp:BoundField DataField="Modulo" HeaderText="Modulo" NullDisplayText="-----" />
                       
                        <asp:CommandField  SelectText="Editar" ShowSelectButton="True" />
                        
                        <asp:ButtonField CommandName="Delete" HeaderText="" ShowHeader="True" 
                            Text="Eliminar" />
                       
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" CssClass="GrillaFooter" Font-Bold="True" ForeColor="White" />
                    
                    <SelectedRowStyle BackColor="#E2DED6" CssClass="GrillaSelectedRow" Font-Bold="True"
                        ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" CssClass="GrillaHeader" Font-Bold="True" ForeColor="White" />
                    
                    <AlternatingRowStyle BackColor="White" CssClass="GrillaRowAlternating" ForeColor="#284775" />
                </asp:GridView>
                </td>
           </tr>
            <tr>
            <td style="width: 34px">
            
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        </asp:Panel>
                    </table>
              
            </td>
        </tr>
</table>
</asp:content>
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantProducto.aspx.vb" Inherits="APPWEB.FrmMantProducto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='../Imagenes/Nuevo_A.JPG';"
                            onmouseout="this.src='../Imagenes/Nuevo_b.JPG';" />
                        <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                            OnClientClick="return(validarSave(0))" onmouseover="this.src='../Imagenes/Guardar_A.JPG';"
                            onmouseout="this.src='../Imagenes/Guardar_B.JPG';" ValidationGroup="valGuardar" />
                        <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                            onmouseover="this.src='../Imagenes/Cancelar_A.JPG';" onmouseout="this.src='../Imagenes/Cancelar_B.JPG';"
                            OnClientClick="return(confirm('Desea cancelar la Operaci�n?'));" />
                        <asp:ImageButton ID="btnEliminar" runat="server" ToolTip="Eliminar producto." OnClientClick="return(confirm('Esta seguro de eliminar el producto y todos los registros relacionados ?'));"
                            ImageUrl="~/Imagenes/imagesCAYDN0IW.jpg" />
                        <asp:Button ID="btnSaveNewFromUpdate" runat="server" Text="Guardar Nuevo" ToolTip="Guardar Nuevo"
                            OnClientClick="return(validarSave(1))" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Visible="true" CssClass="ControlValidacion"
                            ValidationGroup="valGuardar" />
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        PRODUCTO
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="Texto">
                                    Tipo de Existencia:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cmbTipoExistencia" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMensaje" runat="server" CssClass="LabelRojo" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_M_1" runat="server" Width="100%">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="Texto">
                                                    C&oacute;digo:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="M_txtCodigo" runat="server" ReadOnly="True" Width="85px" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                </td>
                                                <td class="Texto">
                                                    L&iacute;nea:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="M_cmbLinea" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Texto">
                                                    Sub L&iacute;nea:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="M_cmbSubLinea" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelAtributosProd" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td class="TituloCelda">
                                                        ESTRUCTURA DE C�DIGO
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="LabelTdLeft">
                                                                    ORIGEN
                                                                </td>
                                                                <td class="LabelTdLeft">
                                                                    C&Oacute;DIGO
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtCodOrigen" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="70px"></asp:TextBox>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtCodCodigo" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:HiddenField ID="hddCodAtributos" runat="server" Visible="true" Value="" />
                                                                </td>
                                                                <td>
                                                                    <asp:HiddenField ID="hddCodSinUso" runat="server" Visible="true" Value="" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="Texto" valign="top">
                                                                    Descripci&oacute;n:
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" ID="M_txtDescripcion"
                                                                        runat="server" onFocus="return(  aceptarFoco(this)  ); return ( onFocusTextTransform(this,configurarDatos) );"
                                                                        onkeypress="return(onKeyPressEnter(event));" Width="800px" MaxLength="150"></asp:TextBox>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="M_txtDescripcion"
                                                                        CssClass="Label" Display="Dynamic" ErrorMessage="El campo Descripci�n es un campo obligatorio."
                                                                        SetFocusOnError="True" ToolTip="Campo Requerido" ValidationGroup="valGuardar">(*)</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" align="right">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                C&oacute;digo Barras:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                                    ID="M_txtCodBarras" runat="server" onkeypress="return(onKeyPressEnter(event));" Width="209px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                C&oacute;digo Barras Fabricante:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                                    ID="M_txtCodBarrasFabricante" runat="server" onkeypress="return(onKeyPressEnter(event));"
                                                                                    Width="209px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                C�digo Anterior:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                                    ID="M_txtCodigoAnterior" runat="server" onkeypress="return(onKeyPressEnter(event));"
                                                                                    MaxLength="7" Width="209px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Panel ID="PanelGenCodigo" runat="server" Width="990px" ScrollBars="Horizontal">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="width: 25%;" align="center" valign="top">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td class="LabelTdLeft">
                                                                                    ORIGEN
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:GridView ID="dgvOrigen" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                        GridLines="Vertical" EmptyDataText="No existe informaci�n">
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkMostrarTTVOR" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Indicador") %>'
                                                                                                        AutoPostBack="true" OnCheckedChanged="chkMostrarTTVOR_CheckedChanged" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Tabla">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblNomTipoTablaOR" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hddIdTipoTablaOR" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                                    <br />
                                                                                                    <asp:TextBox ID="txtFilOrigen" runat="server"></asp:TextBox>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="">
                                                                                                <ItemTemplate>
                                                                                                    <asp:GridView ID="dgvSubOrigen" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                                        EmptyDataText="..." GridLines="Horizontal" DataSource='<%# DataBinder.Eval(Container.DataItem,"ObjSubOrigen") %>'>
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField HeaderText="">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:CheckBox ID="chkActivarOR" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Indicador") %>'
                                                                                                                        onClick="return(valSelectSubOrigen('0',this));" />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="C�digo">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblCodigoTTVOR" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hddOrdenTTVOR" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"NroOrden") %>' />
                                                                                                                    <asp:HiddenField ID="hddParteNombreTTVOR" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"ParteNombre") %>' />
                                                                                                                    <asp:HiddenField ID="hddTipoUsoTTVOR" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TipoUso") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Nombre">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblNomTTVOR" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hddIdTTVOR" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                                    </asp:GridView>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 25%;" align="center" valign="top">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td class="LabelTdLeft">
                                                                                    C&Oacute;DIGO
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:GridView ID="dgvCodigo" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                        GridLines="Vertical" EmptyDataText="No existe informaci�n">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="">
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkMostrarTTVCO" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Indicador") %>'
                                                                                                        AutoPostBack="true" OnCheckedChanged="chkMostrarTTVCO_CheckedChanged" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Tabla">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblNomTipoTablaCO" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hddIdTipoTablaCO" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                                    <br />
                                                                                                    <asp:TextBox ID="txtfilCodigo" runat="server"></asp:TextBox>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="">
                                                                                                <ItemTemplate>
                                                                                                    <asp:GridView ID="dgvSubCodigo" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                                        EmptyDataText="..." GridLines="Horizontal" DataSource='<%# DataBinder.Eval(Container.DataItem,"ObjSubCodigo") %>'>
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField HeaderText="">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:CheckBox ID="chkActivarCO" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Indicador") %>'
                                                                                                                        onClick="return(valSelectSubCodigo('0',this));" />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="C�digo">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblCodigoTTVCO" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hddOrdenTTVCO" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"NroOrden") %>' />
                                                                                                                    <asp:HiddenField ID="hddParteNombreTTVCO" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"ParteNombre") %>' />
                                                                                                                    <asp:HiddenField ID="hddTipoUsoTTVCO" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TipoUso") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Nombre">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblNomTTVCO" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hddIdTTVCO" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                                    </asp:GridView>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 25%;" align="center" valign="top">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td class="LabelTdLeft">
                                                                                    ATRIBUTOS
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:GridView ID="dgvAtributos" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                        GridLines="Vertical" EmptyDataText="No existe informaci�n">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="">
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkMostrarTTVAT" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Indicador") %>'
                                                                                                        AutoPostBack="true" OnCheckedChanged="chkMostrarTTVAT_CheckedChanged" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Tabla">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblNomTipoTablaAT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hddIdTipoTablaAT" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                                    <br />
                                                                                                    <asp:TextBox ID="txtFilAtributos" runat="server"></asp:TextBox>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="">
                                                                                                <ItemTemplate>
                                                                                                    <asp:GridView ID="dgvSubAtributos" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                                        EmptyDataText="..." GridLines="Horizontal" DataSource='<%# DataBinder.Eval(Container.DataItem,"ObjSubAtributos") %>'>
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField HeaderText="">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:CheckBox ID="chkActivarAT" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Indicador") %>'
                                                                                                                        onClick="return(valSelectSubAtributos('0',this));" />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="C�digo">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblCodigoTTVAT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hddOrdenTTVAT" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"NroOrden") %>' />
                                                                                                                    <asp:HiddenField ID="hddParteNombreTTVAT" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"ParteNombre") %>' />
                                                                                                                    <asp:HiddenField ID="hddTipoUsoTTVAT" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TipoUso") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Nombre">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblNomTTVAT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hddIdTTVAT" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                                    </asp:GridView>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 25%;" align="center" valign="top">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td class="LabelTdLeft">
                                                                                    SIN USO
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:GridView ID="dgvSinUso" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                        GridLines="Vertical" EmptyDataText="No existe informaci�n">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="">
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkMostrarTTVSU" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Indicador") %>'
                                                                                                        AutoPostBack="true" OnCheckedChanged="chkMostrarTTVSU_CheckedChanged" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Tabla">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblNomTipoTablaSU" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hddIdTipoTablaSU" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                                    <br />
                                                                                                    <asp:TextBox ID="txtFilSinUso" runat="server"></asp:TextBox>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="">
                                                                                                <ItemTemplate>
                                                                                                    <asp:GridView ID="dgvSubSinUso" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                                        EmptyDataText="..." GridLines="Horizontal" DataSource='<%# DataBinder.Eval(Container.DataItem,"ObjSubSinUso") %>'>
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField HeaderText="">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:CheckBox ID="chkActivarSU" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Indicador") %>'
                                                                                                                        onClick="return(valSelectSubSinUso('0',this));" />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="C�digo">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblCodigoTTVSU" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hddOrdenTTVSU" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"NroOrden") %>' />
                                                                                                                    <asp:HiddenField ID="hddParteNombreTTVSU" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"ParteNombre") %>' />
                                                                                                                    <asp:HiddenField ID="hddTipoUsoTTVSU" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TipoUso") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Nombre">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblNomTTVSU" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hddIdTTVSU" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                                    </asp:GridView>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Button ID="btnEvento" runat="server" Text="" Width="1px" Height="1px" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="Texto">
                                                    C&oacute;d. Proveedor:
                                                </td>
                                                <td>
                                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                        ID="M_txtCodProveedor" runat="server" onkeypress="return(onKeyPressEnter(event));"></asp:TextBox>
                                                </td>
                                                <td class="Texto">
                                                    Proveedor:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="M_cmbProveedor" runat="server" Width="500px" DataValueField="IdPersona"
                                                        DataTextField="NombreComercial">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Texto">
                                                    Estado:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="M_cmbEstadoProd" runat="server" DataTextField="Descripcion"
                                                        DataValueField="Id">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="Panel_Aux"
                                            Collapsed="true" ExpandControlID="Image2" CollapseControlID="Image2" TextLabelID="Label1"
                                            ImageControlID="Image2" CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                            CollapsedText="Atributos Opcionales" ExpandedText="Agregue Atributos Opcionales"
                                            ExpandDirection="Vertical" SuppressPostBack="True">
                                        </cc1:CollapsiblePanelExtender>
                                        <asp:Image ID="Image2" runat="server" />
                                        <asp:Label ID="Label1" runat="server" Text="Label" CssClass="Label"></asp:Label>
                                        <asp:Panel ID="Panel_Aux" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Grupos de Productos:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="M_cmbTipoProd" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Texto" style="font-weight: bold">
                                                        C�lculo del Costo de Flete por:
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdbOpcion_CalculoCostoFlete" runat="server" RepeatDirection="Horizontal"
                                                            CssClass="Texto">
                                                            <asp:ListItem Value="P">Peso</asp:ListItem>
                                                            <asp:ListItem Value="U">Unidad</asp:ListItem>
                                                            <asp:ListItem Value="NAN" Selected="True">Ninguno</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Percepcion:
                                                    </td>
                                                    <td class="Label">
                                                        <asp:RadioButtonList ID="rblAfectoPercepcion" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Afectar" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="No Afectar" Value="0"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Transferencia Gratuita:
                                                    </td>
                                                    <td class="Label">
                                                        <asp:RadioButtonList ID="rb_flagTG" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Afectar" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="No Afectar" Selected="True" Value="0"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:CollapsiblePanelExtender ID="cpanelCancelacion1" runat="server" TargetControlID="PanelStock"
                                            Collapsed="true" ExpandControlID="Image4" CollapseControlID="Image4" TextLabelID="lblCancelacion"
                                            ImageControlID="Image4" CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                            CollapsedText="Stock" ExpandedText="Agregue Stock" ExpandDirection="Vertical"
                                            SuppressPostBack="True" />
                                        <asp:Image ID="Image4" runat="server" />
                                        <asp:Label ID="lblCancelacion" runat="server" Text="Label" CssClass="Label"></asp:Label>
                                        <asp:Panel ID="PanelStock" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="Texto">
                                                        Stock M&iacute;nimo:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="M_txtStockMin" onkeypress="return(validarCajaNumero());" runat="server"
                                                            Width="124px"></asp:TextBox>
                                                    </td>
                                                    <td class="Texto">
                                                        Stock M&aacute;ximo:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="M_txtStockMax" onkeypress="return(validarCajaNumero());" runat="server"
                                                            Width="124px"></asp:TextBox>
                                                    </td>
                                                    <td class="Texto">
                                                        Stock Reposici&oacute;n:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtStockReposici�n" onkeypress="return(validarCajaNumero());" runat="server"
                                                            Width="124px"></asp:TextBox>
                                                    </td>
                                                    <td class="Texto">
                                                        Stock Actual:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="M_txtStockActual" runat="server" CssClass="TextBoxReadOnly" Width="124px"
                                                            ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto">
                                                        Plazo Entrega:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPlazoEntrega" runat="server" Width="124px" Font-Bold="true" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                                            onFocus="return(   aceptarFoco(this) );" onblur="return(   valBlur(event)     );">0</asp:TextBox>
                                                    </td>
                                                    <td class="Texto">
                                                        Vol. Min. Compra:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtVolMinCompra" runat="server" Width="124px" Font-Bold="true" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                                            onFocus="return(   aceptarFoco(this) );" onblur="return(   valBlur(event)     );">0</asp:TextBox>
                                                    </td>
                                                    <td class="Texto">
                                                        Semanas Reposici&oacute;n:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtReposicioWeeks" runat="server" Width="124px" Font-Bold="true"
                                                            onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" onFocus="return(   aceptarFoco(this) );"
                                                            onblur="return(   valBlur(event)     );">0</asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5">
                                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="M_txtStockMax"
                                                            ControlToValidate="M_txtStockMin" CssClass="ControlValidacion" Display="Dynamic"
                                                            ErrorMessage="El Stock M�nimo no puede ser menor al Stock M�ximo" SetFocusOnError="True"
                                                            Type="Integer" ValidationGroup="valGuardar" Operator="LessThanEqual" ToolTip="El Stock M�nimo no puede ser menor al Stock M�ximo">(*)</asp:CompareValidator>
                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="M_txtStockMin"
                                                            ControlToValidate="M_txtStockMax" CssClass="ControlValidacion" Display="Dynamic"
                                                            ErrorMessage="El Stock M�ximo no puede ser menor al Stock M�nimo" Operator="GreaterThanEqual"
                                                            SetFocusOnError="True" Type="Integer" ValidationGroup="valGuardar" ToolTip="El Stock M�ximo no puede ser menor al Stock M�nimo">(*)</asp:CompareValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="PanelUM"
                                            CollapsedSize="0" ExpandedSize="190" Collapsed="true" ExpandControlID="Image5"
                                            CollapseControlID="Image5" TextLabelID="lblUM" ImageControlID="Image5" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                            ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedText="Unidad de Medida" ExpandedText="Agregue Unidad de Medida"
                                            ExpandDirection="Vertical" SuppressPostBack="True" />
                                        <asp:Image ID="Image5" runat="server" />
                                        <asp:Label ID="lblUM" runat="server" Text="Label" CssClass="Label"></asp:Label>
                                        <asp:Panel ID="PanelUM" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label37" runat="server" CssClass="Label" Text="U. Medida:"></asp:Label>
                                                        <asp:DropDownList ID="M_cmbUMedida" runat="server">
                                                        </asp:DropDownList>
                                                        <asp:ImageButton ID="btnAgregarUM" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                            onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                            OnClientClick="return(validarEq());" ValidationGroup="valUM" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="Texto">Equivalencia en cajas</span>
                                                        <asp:TextBox ID="txt_EquivalenciaCaja" runat="server" Text="0" Width="50px" onkeyUp="txtCalcularEquivalencia();"></asp:TextBox>
                                                        <span class="Texto">Cantidad Piezas</span>
                                                        <asp:TextBox ID="txt_CantidadPiezas" runat="server" Text="0" Width="50px" onkeyUp="txtCalcularEquivalencia();"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        
                                                    </td>
                                                    <td>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="DGV_UM" runat="server" AutoGenerateColumns="False" Width="742px">
                                                            <Columns>
                                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                                <asp:BoundField DataField="NombreCortoUM" HeaderText="U. Medida" />
                                                                <asp:TemplateField HeaderText="Equivalencia">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtEquivalencia_UM" runat="server" onblur="return(valBlur(event));" onKeypress="return(validarNumeroPunto(event));" enabled="false"
                                                                            onFocus="return( aceptarFoco(this)  );" Text='<%#DataBinder.Eval(Container.DataItem,"Equivalencia")%>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Principal">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbPrincipal" onClick="return(  valSelectUM('0',this)   );" runat="server"
                                                                            Checked='<%#DataBinder.Eval(Container.DataItem,"UnidadPrincipal")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Retazo">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbRetazo" onClick="return(  valSelectUM('1',this)   );" runat="server"
                                                                            Checked='<%#DataBinder.Eval(Container.DataItem,"Retazo")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Activo">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbEstado_ProdEditar1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="IdUnidadMedida" />
                                                                <asp:TemplateField HeaderText="Cod Barra">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtCodBarraXUm" runat="server" onblur="return(valBlur(event));" onkeypress="return(onKeyPressEnter(event));"
                                                                            onFocus="return( aceptarFoco(this)  );" Text='<%#DataBinder.Eval(Container.DataItem,"CodBarraXUm","{0:F4}")%>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="GrillaRow" />
                                                            <FooterStyle CssClass="GrillaFooter" />
                                                            <PagerStyle CssClass="GrillaPager" />
                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="DGV_UM_EDITAR" runat="server" AutoGenerateColumns="False" Width="742px">
                                                            <Columns>
                                                                <asp:BoundField DataField="NombreCortoUM" HeaderText="U. Medida" />
                                                                <asp:BoundField DataField="Equivalencia" HeaderText="Equivalencia" />
                                                                <asp:BoundField DataField="DescEstadoUP" HeaderText="U.M. Principal" />
                                                                <asp:BoundField DataField="DescEstadoRetazo" HeaderText="U.M. Retazo" />
                                                                <asp:TemplateField HeaderText="Activo">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbEstado_ProdEditar" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="GrillaRow" />
                                                            <FooterStyle CssClass="GrillaFooter" />
                                                            <PagerStyle CssClass="GrillaPager" />
                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtenderMagnitud" runat="server"
                                            TargetControlID="Panel_ProductoMedida" CollapsedSize="0" ExpandedSize="190" Collapsed="true"
                                            ExpandControlID="ImageMagnitud" CollapseControlID="ImageMagnitud" TextLabelID="LabelMagnitud"
                                            ImageControlID="ImageMagnitud" CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                            CollapsedText="Magnitud" ExpandedText="Agregue Magnitud" ExpandDirection="Vertical"
                                            SuppressPostBack="True" />
                                        <asp:Image ID="ImageMagnitud" runat="server" />
                                        <asp:Label ID="LabelMagnitud" runat="server" Text="Label" CssClass="Label"></asp:Label>
                                        <asp:Panel ID="Panel_ProductoMedida" runat="server">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label47" runat="server" CssClass="Label" Text="Magnitud"></asp:Label>
                                                        <asp:DropDownList ID="cmbMagnitud" runat="server">
                                                        </asp:DropDownList>
                                                        <asp:ImageButton OnClientClick="return(validarAddMagnitud());" ID="btnAgregarMagnitud"
                                                            runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG" onmouseout="this.src='../Imagenes/Agregar_B.JPG';"
                                                            onmouseover="this.src='../Imagenes/Agregar_A.JPG';" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="Texto">Peso Caja</span>
                                                        <asp:TextBox ID="txt_PesoCaja" runat="server" Text="0" Width="60px" onkeyUp="txtCalcularPesos();"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="DGV_Magnitud" runat="server" AutoGenerateColumns="False" Width="742px">
                                                            <RowStyle CssClass="GrillaRow" />
                                                            <Columns>
                                                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                <asp:TemplateField HeaderText="Magnitud">
                                                                    <ItemTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:HiddenField ID="hddIdMagnitud" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblNomMagnitud" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Descripcion")%>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="U. M.">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="cboUnidadMedida_Magnitud" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>'
                                                                            runat="server" DataTextField="DescripcionCorto" DataValueField="Id">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtEscalarMagnitud" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));" Enabled="false"
                                                                            onblur="return(valBlur(event));" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Escalar","{0:F4}")%>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Principal">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbPrincipal" onClick="return(  valSelectMagnitud('0',this)   );"
                                                                            runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"pm_principal")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle CssClass="GrillaFooter" />
                                                            <PagerStyle CssClass="GrillaPager" />
                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Kit" runat="server">
                            <table width="100%">
                                <tr>
                                    <td class="SubTituloCelda">
                                        MANTENIMIENTO DE KIT
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chb_HabilitarKit" runat="server" Text="CATALOGAR COMO KIT" CssClass="Texto"
                                                        Font-Bold="true" />
                                                </td>
                                                <td style="width: 20px">
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnBuscarComponenteKit" runat="server" Text="Buscar Componentes"
                                                        Width="155px" OnClientClick=" return( valOnClick_btnBuscarComponenteKit()  ); " />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GV_ComponenteKit" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:CommandField ShowSelectButton="true" SelectText="Quitar" HeaderStyle-Height="25px"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                                            <asp:TemplateField HeaderText="C�digo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblCodigoProducto" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CodigoProd_Comp") %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdComponente") %>' />
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Componente" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblComponente" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Componente") %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="cboUnidadMedida" DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaProductoUM") %>'
                                                                        runat="server" DataTextField="NombreCortoUM" DataValueField="IdUnidadMedida"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCantidad" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                                        onFocus=" return( aceptarFoco(this) ); " onblur=" return(  valBlur(event) ); " runat="server"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F2}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Precio Ref." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DropDownList ID="cboMoneda" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaMoneda") %>'
                                                                                    DataValueField="Id" DataTextField="Simbolo">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtPrecioRef" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                                                    onFocus=" return( aceptarFoco(this) ); " onblur=" return(  valBlur(event) ); " runat="server"
                                                                                    Text='<%# DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F2}") %>'></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                              <asp:TemplateField HeaderText="Porcentaje" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtporcentaje" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                                        onFocus=" return( aceptarFoco(this) ); " onblur=" return(  valBlur(event) ); " runat="server"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"PorcentajeKit") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        FILTRO
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_M_2" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="Texto">
                                                    L&iacute;nea:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="M_B_cmbLinea" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="M_B_lbl1" runat="server" CssClass="Texto" Text="Sub L�nea:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="M_B_cmbSubLinea" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelFiltro1" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <td class="Texto">
                                                                Estado:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="M_B_cmbEstado" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="PanelBusAvanz"
                                                            Collapsed="true" ExpandControlID="Image1" CollapseControlID="Image1" TextLabelID="Label16"
                                                            ImageControlID="Image1" CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                            CollapsedText="Busqueda Avanzada" ExpandedText="Busqueda Avanzada">
                                                        </cc1:CollapsiblePanelExtender>
                                                        <asp:Image ID="Image1" runat="server" />
                                                        <asp:Label ID="Label16" runat="server" Text="Label" CssClass="Label"></asp:Label>
                                                        <asp:Panel ID="PanelBusAvanz" runat="server" Visible="true">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label15" runat="server" CssClass="Label" Text="Tipo Tabla:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="cboTipoTablaBus" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnAgregarBus" runat="server" Text="Agregar" OnClientClick="return(valAddTablaBusAvanz());" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:GridView ID="dgvFiltroTipoTabla" runat="server" AutoGenerateColumns="False">
                                                                                        <Columns>
                                                                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                                            <asp:TemplateField HeaderText="Tipo Tabla">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                                                                    </asp:DropDownList>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:RadioButtonList ID="rdbCodNombre" runat="server" CssClass="Texto" AutoPostBack="True"
                                                                        RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1">Codigo</asp:ListItem>
                                                                        <asp:ListItem Value="2">Descripci�n</asp:ListItem>
                                                                        <asp:ListItem Value="3">Cod.Prov</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton ID="M_btnFiltrar_B" runat="server" ImageUrl="~/Imagenes/Busqueda_B.jpg"
                                                                        onmouseout="this.src='../Imagenes/Busqueda_B.JPG';" onmouseover="this.src='../Imagenes/Busqueda_A.JPG';"
                                                                        Style="width: 27px" OnClientClick="return(valAddBusqueda());" />
                                                                </td>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Label ID="M_lblTextoBusq" runat="server" CssClass="Texto"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                            ID="M_B_txtDescripcion" runat="server" AutoPostBack="false" MaxLength="50" onkeypress="return(onKeyPressEnter(event));"
                                                                            Width="205px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_B" runat="server">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="DGV_B" runat="server" AutoGenerateColumns="False" PageSize="10"
                                            Width="99%">
                                            <Columns>
                                                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                                                <asp:TemplateField HeaderText="C�digo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblprodCodigo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"prod_Codigo")%>'></asp:Label>
                                                        <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Descripci�n" DataField="NomProducto" />
                                                <asp:BoundField HeaderText="Cod.Prov" DataField="Cod_Proveedor" />
                                                <asp:BoundField HeaderText="L�nea" DataField="NomLinea" />
                                                <asp:BoundField HeaderText="Sub L�nea" DataField="NomSubLinea" />
                                                <asp:BoundField HeaderText="Origen" DataField="NoVisible" />
                                            </Columns>
                                            <RowStyle CssClass="GrillaRow" />
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btn_anterior" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                            OnClientClick="return(valNavegacion('0'));" Style="cursor: hand;" />
                                        <asp:Button ID="btn_siguiente" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                            OnClientClick="return(valNavegacion('1'));" Style="cursor: hand;" />
                                        <asp:TextBox ID="txt_PageIndex" runat="server" Width="50px" CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                                        <asp:Button ID="btn_Ir" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                                            OnClientClick="return(valNavegacion('2'));" Style="cursor: hand;" />
                                        <asp:TextBox ID="txt_PageIndexGO" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hddModo" runat="server" />
                        <asp:HiddenField ID="hddIndexGrillaOR" runat="server" Value="0" />
                        <asp:HiddenField ID="hddIndexSubGrillaOR" runat="server" Value="0" />
                        <asp:HiddenField ID="hddIndexGrillaCO" runat="server" Value="0" />
                        <asp:HiddenField ID="hddIndexSubGrillaCO" runat="server" Value="0" />
                        <asp:HiddenField ID="hddIndexGrillaAT" runat="server" Value="0" />
                        <asp:HiddenField ID="hddIndexSubGrillaAT" runat="server" Value="0" />
                        <asp:HiddenField ID="hddIndexGrillaSU" runat="server" Value="0" />
                        <asp:HiddenField ID="hddIndexSubGrillaSU" runat="server" Value="0" />
                        <asp:HiddenField ID="hddValCodigo" runat="server" Value="" />
                        <asp:HiddenField ID="hddModoVal" runat="server" Value="0" />
                        <asp:HiddenField ID="hddvalUM" runat="server" Value="0" />
                        <asp:HiddenField ID="hddcodigoCompleto" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" ToolTip="Cerrar" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cbo_TipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbLinea_AddProd" runat="server" AutoPostBack="True" DataTextField="Descripcion"
                                    DataValueField="Id" TabIndex="201">
                                </asp:DropDownList>
                                <asp:Label ID="Label55" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                <asp:DropDownList ID="cmbSubLinea_AddProd" runat="server" AutoPostBack="true" DataTextField="Nombre"
                                    DataValueField="Id" TabIndex="202">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtDescripcionProd_AddProd" runat="server" onFocus="return( aceptarFoco(this) ); return ( onFocusTextTransform(this,configurarDatos) );"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onKeypress="return( valKeyPressDescripcionProd() );"
                                                TabIndex="204" Width="300px"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            C�d.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCodigoProducto" runat="server" Font-Bold="true" onFocus="return( aceptarFoco(this) ); return ( onFocusTextTransform(this,configurarDatos) );"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onKeypress="return( valKeyPressDescripcionProd() );"
                                                TabIndex="205" Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';"
                                                TabIndex="207" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                OnClientClick="return(valAddProductos());" onmouseout="this.src='../Imagenes/Agregar_B.JPG';"
                                                onmouseover="this.src='../Imagenes/Agregar_A.JPG';" TabIndex="208" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                OnClientClick="return(limpiarBuscarProducto());" onmouseout="this.src='../Imagenes/Limpiar_B.JPG';"
                                                onmouseover="this.src='../Imagenes/Limpiar_A.JPG';" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" CssClass="Label" Text="B�squeda Avanzada"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" OnClientClick="return( valAddTabla() );"
                                                    Text="Agregar" Width="80px" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-Width="75px" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                SelectText="Quitar" ShowSelectButton="True" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Atributo" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla0" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Valor" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV0" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>'
                                                        DataTextField="Nombre" DataValueField="IdTipoTablaValor" Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" CollapseControlID="Image21_11"
                        Collapsed="true" CollapsedImage="~/Imagenes/Mas_B.JPG" CollapsedSize="0" CollapsedText="B�squeda Avanzada"
                        ExpandControlID="Image21_11" ExpandDirection="Vertical" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        ExpandedSize="0" ExpandedText="B�squeda Avanzada" ImageControlID="Image21_11"
                        SuppressPostBack="true" TargetControlID="Panel_BusqAvanzadoProd" TextLabelID="Label21_11">
                    </cc1:CollapsiblePanelExtender>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" PageSize="20"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chb_AddDetalle" runat="server" />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderText="C�digo" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCodigoProducto" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderText="C�digo" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblComponente" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Descripcion") %>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnAnterior_Productos" runat="server" Font-Bold="true" OnClientClick="return(valNavegacionProductos('0'));"
                        TabIndex="212" Text="&lt;" ToolTip="P�gina Anterior" Width="50px" />
                    <asp:Button ID="btnPosterior_Productos" runat="server" Font-Bold="true" OnClientClick="return(valNavegacionProductos('1'));"
                        TabIndex="213" Text="&gt;" ToolTip="P�gina Posterior" Width="50px" />
                    <asp:TextBox ID="txtPageIndex_Productos" runat="server" CssClass="TextBoxReadOnly_Right"
                        ReadOnly="true" TabIndex="214" Width="50px"></asp:TextBox>
                    <asp:Button ID="btnIr_Productos" runat="server" Font-Bold="true" OnClientClick="return(valNavegacionProductos('2'));"
                        TabIndex="215" Text="Ir" ToolTip="Ir a la P�gina" Width="50px" />
                    <asp:TextBox ID="txtPageIndexGO_Productos" runat="server" CssClass="TextBox_ReadOnly"
                        onKeyPress="return(onKeyPressEsNumero('event'));" TabIndex="216" Width="50px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };

        function KeypressPesoCaja() {

            if (document.getElementById('<%= DGV_UM.ClientID %>')) {
                var grilla = document.getElementById('<%= DGV_UM.ClientID %>');

            } else { alert("Debe ingresar Equivalencia en caja para calcular el peso.")}
        }

        function txtCalcularPesos() {
            var grilla = document.getElementById('<%= DGV_Magnitud.ClientID %>');
            var txtPEso = document.getElementById('<%= txt_PesoCaja.ClientID %>')
            var combo;

             var grillaum = document.getElementById('<%=DGV_UM.ClientID%>');
             var hasUMPrincipalum;
             var EqPesoCaja;
             var UM;
             var contador = 0;
             if (grillaum != null) {

                 for (var i = 1; i < grillaum.rows.length; i++) {  //comienzo en 1 xq 0 es la cabecera
                     var rowElem = grillaum.rows[i];
                     if (rowElem.cells[3].children[0].checked == true) {
                         hasUMPrincipalum = (rowElem.cells[2].children[0].value);
                     }
                     UM = grillaum.rows[i].cells[1].innerHTML;
                     if (UM == "CAJ") {
                         EqPesoCaja = (rowElem.cells[2].children[0].value);
                         contador=contador+1
                     }
//                     if (UM != "CAJ") {
//                         EqPesoCaja = hasUMPrincipalum;
//                     }

                 }
             }

             var pesoum;
            for (var i = 1; i < grilla.rows.length; i++) {
                combo = grilla.rows[i].cells[2].children[0];
                UM = combo.options[combo.selectedIndex].text;
                if (UM == "KG" && contador==1) {
                    pesoum = (txtPEso.value * hasUMPrincipalum) / EqPesoCaja
//                  grilla.rows[i].cells[3].children[0].value = txtPEso.value;
                    grilla.rows[i].cells[3].children[0].value = pesoum.toFixed(6); ;

                }
                if (UM == "KG" && contador==0) {
                    pesoum = (txtPEso.value * hasUMPrincipalum)
                    // grilla.rows[i].cells[3].children[0].value = txtPEso.value;
                    grilla.rows[i].cells[3].children[0].value = pesoum.toFixed(6); ;

                }


            }
        }

        function txtCalcularEquivalencia() {
            var txt_equiCajas;
            var txt_pesoCajas;
            var txt_cantPiezas;

            var equiCajas;
            var pesoCajas;
            var cantPiezas;

            if (document.getElementById('<%= txt_EquivalenciaCaja.ClientID %>')) {
                txt_equiCajas = document.getElementById('<%= txt_EquivalenciaCaja.ClientID %>');
                equiCajas = txt_equiCajas.value;
            } else { equiCajas = 0; }

            if (document.getElementById('<%= txt_PesoCaja.ClientID %>')) {
                txt_pesoCajas = document.getElementById('<%= txt_PesoCaja.ClientID %>');
                pesoCajas = txt_pesoCajas.value;
            } else { pesoCajas = 0; }

            if (document.getElementById('<%= txt_CantidadPiezas.ClientID %>')) {
                txt_cantPiezas = document.getElementById('<%= txt_CantidadPiezas.ClientID %>');
                cantPiezas = txt_cantPiezas.value;
            } else { cantPiezas = 0; }



            if (document.getElementById('<%= DGV_UM.ClientID %>')) {
                var grilla = document.getElementById('<%= DGV_UM.ClientID %>');
                var nroFilas = grilla.rows.length;
                var UM;
                for (var i = 1; i < nroFilas; i++) {
                    UM = grilla.rows[i].cells[1].innerHTML;
                    if (UM == "CAJ") {
                        grilla.rows[i].cells[2].children[0].value = parseFloat(equiCajas).toFixed(6); 
                    }
                    if (UM == "PZA") {
                        if (cantPiezas != 0) {
                            grilla.rows[i].cells[2].children[0].value = (parseFloat(equiCajas) / parseFloat(cantPiezas)).toFixed(6);
                        }
                    }
                    if (UM == "M2") {
                        grilla.rows[i].cells[2].children[0].value = parseFloat(1).toFixed(6);
                    }
                }
            }
        }

        function validaNroTablasSelValor(grilla, tipouso) {
            var retorna = true;
            var checke = false;
            var nrotablas = 0;
            var subgrilla = grilla.getElementsByTagName('table'); //dgvSubOrigen
            if (grilla != null) {

                for (var x = 1; x < grilla.rows.length; x++) {
                    var rowElemt = grilla.rows[x];
                    if (rowElemt.cells[0].children[0].status == true) {
                        if (subgrilla[x - 1] != null) {

                            for (var i = 1; i < subgrilla[x - 1].rows.length; i++) {
                                var rowElemx = subgrilla[x - 1].rows[i];
                                if (rowElemx.cells[0].children[0].status == true) {
                                    checke = true;
                                    break;
                                }
                            }
                            if (checke == false) {

                                switch (tipouso) {
                                    case '1':
                                        if (nrotablas < 2 || nrotablas > 2) {
                                            alert("selecione un valor para las tablas de Origen")
                                            retorna = false;
                                        }
                                        break;
                                    case '2':
                                        if (nrotablas < 2 || nrotablas > 2) {
                                            alert("selecione un valor para las tablas de Codigo")
                                            retorna = false;

                                        }
                                        break;
                                    case '3':
                                        if (nrotablas < 3 || nrotablas > 3) {
                                            alert("selecione un valor para las tablas de atributos")
                                            retorna = false;
                                        }
                                        break;

                                }
                            }
                            nrotablas = nrotablas + 1;
                        }
                    }

                }


                switch (tipouso) {
                    case '1':
                        if (nrotablas < 2 || nrotablas > 2) {
                            alert("selecione 2 tablas para origen")
                            retorna = false;

                        }
                        break;
                    case '2':
                        if (nrotablas < 2 || nrotablas > 2) {
                            alert("selecione 2 tablas para codigo")
                            retorna = false;

                        }
                        break;
                    case '3':
                        if (nrotablas < 3 || nrotablas > 3) {
                            alert("selecione 3 tablas para atributos")
                            retorna = false;
                        }
                        break;
                }


            } //en if 

            return retorna;

        }



        function validarSave(tipo) {

            //******** Validamos Linea y SubLinea
            var cbo = document.getElementById('<%=M_cmbLinea.ClientID%>');
            if (cbo.value == 0) {
                alert('Debe seleccionar una L�nea.');
                return false;
            }

            var cboSB = document.getElementById('<%=M_cmbSubLinea.ClientID%>');
            if (cboSB.value == 0) {
                alert('Debe seleccionar una Sub L�nea.');
                return false;
            }

            var caja = document.getElementById('<%=M_txtDescripcion.ClientID%>');
            if (caja.value.length == 0) {
                alert('Debe ingresar una descripci�n al producto.');
                caja.select();
                caja.focus();
                return false;
            }
            if (caja.value.length > 150) {
                alert('La descripcion del producto no puede ser mayor a 150 caracteres.');
                return false;
            }

            var modo = document.getElementById('<%=hddModo.ClientID%>');


            var grillaor = document.getElementById('<%=dgvOrigen.ClientID%>');
            if (grillaor.rows.length > 1) {
                if (validaNroTablasSelValor(grillaor, '1') == false) {
                    return false;
                }
            }


            var grillaco = document.getElementById('<%=dgvCodigo.ClientID%>');
            if (grillaco.rows.length > 1) {
                if (validaNroTablasSelValor(grillaco, '2') == false) {
                    return false;
                }
            }


            var grillaat = document.getElementById('<%=dgvAtributos.ClientID%>');
            if (grillaat.rows.length > 1) {
                if (validaNroTablasSelValor(grillaat, '3') == false) {
                    return false;
                }
            }




            if (tipo == 1) {
                var codigoNoVisible = document.getElementById('<%=txtCodOrigen.ClientID%>');
                var codigoGenerado = document.getElementById('<%=txtCodCodigo.ClientID%>');
                var hddCodigoCompleto = document.getElementById('<%=hddcodigoCompleto.ClientID%>');
                var ncod;
                ncod = codigoNoVisible.value + "" + codigoGenerado.value;
                if (hddCodigoCompleto.value == ncod) {
                    alert('debe variar el codigo del producto,para crear uno nuevo.');
                    return false;
                }

            }
            if (modo.value == '2') {
                if (tipo == 0) {

                    var codigoNoVisible = document.getElementById('<%=txtCodOrigen.ClientID%>');
                    var codigoGenerado = document.getElementById('<%=txtCodCodigo.ClientID%>');
                    var hddCodigoCompleto = document.getElementById('<%=hddcodigoCompleto.ClientID%>');
                    var ncod;
                    ncod = codigoNoVisible.value + "" + codigoGenerado.value;
                    if (hddCodigoCompleto.value != ncod) {
                        alert('No se puede actualizar la estructura de [ORIGEN] y [C�DIGO].\nNo se permite la operaci�n.\nSe recomienda cancelar el proceso de actualizac�on del producto.');
                        return false;
                    }

                }
            }
            var codigoanterior = document.getElementById('<%=M_txtCodigoAnterior.ClientID%>');
            if (codigoanterior.value.length == 0) {
                alert('Debe ingresar el codigo anterior.');
                return false;
            }
            if (codigoanterior.value.length < 7 || codigoanterior.value.length > 7) {
                alert('El codigo anterior debe tener una longitud de 7 caracteres.');
                return false;
            }

            var grilla = document.getElementById('<%=DGV_Magnitud.ClientID%>');
            if (grilla != null) {

                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseFloat(rowElem.cells[3].children[0].value) <= 0 || isNaN(rowElem.cells[3].children[0].value)) {
                        alert('Valor no v�lido. El valor debe ser mayor a cero.');
                        rowElem.cells[3].children[0].select();
                        rowElem.cells[3].children[0].focus();
                        return false;
                    }
                    var cbo9 = rowElem.cells[2].children[0];
                    if (isNaN(cbo9.value) || cbo.value.length <= 0 || parseInt(cbo.value) <= 0) {
                        alert('Debe seleccionar una Unidad de Medida para la Magnitud : ' + rowElem.cells[1].children[0].cells[1].children[0].innerHTML);
                        return false;
                    }
                }

            }


            //*********** Validamos la UM Principal y cantidad Mayor a cero
            var grilla = document.getElementById('<%=DGV_UM.ClientID%>');
            var hasUMPrincipal = false;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];

                    for (var j = 1; j < grilla.rows.length; j++) {
                        var rowElemRep = grilla.rows[j];
                        if (i != j) {
                            if (((rowElem.cells[7].children[0].value) != '' & (rowElemRep.cells[7].children[0].value) != '') & (rowElem.cells[7].children[0].value) == (rowElemRep.cells[7].children[0].value)) {
                                alert('Los codigos de barra por unidad de medida deben ser distintas.');
                                return false;
                            }
                        }

                    }

                    if (rowElem.cells[3].children[0].getElemntsByTagName("input")[0].checked == true) {
                        hasUMPrincipal = true;
                    }
                    if (isNaN(rowElem.cells[2].children[0].value) || parseFloat(rowElem.cells[2].children[0].value) <= 0) {
                        alert('Ingrese un valor v�lido mayor a cero.');
                        rowElem.cells[2].children[0].select();
                        rowElem.cells[2].children[0].focus();
                        return false;
                    }
                }
                if (!(hasUMPrincipal)) {
                    alert('Seleccione una Unidad de Medida Principal.');
                    return false;
                }

            }

            //se comento para se puede insertar productos sin unidadmedida

            //            else {
            //                alert('Debe Ingresar una Unidad de Medida Principal para el Producto.');
            //                return false;
            //            }


            return (confirm('Desea continuar con la operaci�n?'));
        }

        function validarAddMagnitud() {
            var IdMagnitud = parseInt(document.getElementById('<%=cmbMagnitud.ClientID %>').value);

            //************* recorremos la grilla
            var grilla = document.getElementById('<%=DGV_Magnitud.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == IdMagnitud) {
                        alert('La Magnitud ya ha sido ingresado.');
                        return false;
                    }
                }
            }


            return true;
        }



        function valBlur(e) {
            //obtiene srcElement.Id
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;
            //termina
            var caja = document.getElementById(event_element.id);
            if (CajaEnBlanco(caja)) {
                caja.value = 0;
                //alert('Debe ingresar un valor.');
            }
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no v�lido.');
            }
        }


        function valCajaEscalar() {
            validarDecimal();
        }
        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }


        function validarKeyPressEq() {
            if (esEnter(event) == true) {
                return false;
            }
            return true;
        }
        function validarCajaNumero() {
            if (onKeyPressValidarEnter_EsNumero() == false) {
                alert('Caracter no v�lido');
                return false;
            }
            return true;
        }

        function validarEq() {
            try {
                //************* validamos UM repetido
                var grilla = document.getElementById('<%=DGV_UM.ClientID%>');
                var cboUM = document.getElementById('<%=M_cmbUMedida.ClientID%>');
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                        var rowElem = grilla.rows[i];
                        if (rowElem.cells[6].innerHTML == cboUM.value) {
                            alert('La Unidad de Medida ya ha sido ingresada.');
                            return false;
                        }
                    }
                }
                return true;
            } catch (er) {
                alert(er.value);
                return false;
            }
        }
        function valSelectUM(opcion, obj) {
            //****** 0: Principal
            //****** 1: Retazo
            var grilla = document.getElementById('<%=DGV_UM.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    switch (opcion) {
                        case '0':  //*** Principal

                            if (rowElem.cells[3].children[0].id == obj.id) {
                                rowElem.cells[4].children[0].status = false;
                            } else {
                                rowElem.cells[3].children[0].status = false;
                            }
                            break;
                        case '1':  //** Retazo                            
                            if (rowElem.cells[4].children[0].id == obj.id) {
                                rowElem.cells[3].children[0].status = false;
                            } else {
                                rowElem.cells[4].children[0].status = false;
                            }
                            break;
                    }
                }
            }
            return true;
        }


        function valSelectSubOrigen(opcion, obj) {
            var indextable = 0;
            var hddindexgrillaOR = document.getElementById('<%=hddIndexGrillaOR.ClientID%>');
            var hddindexsubgrillaOR = document.getElementById('<%=hddIndexSubGrillaOR.ClientID%>');
            var grilla = document.getElementById('<%=dgvOrigen.ClientID%>');
            if (grilla != null) {

                var subgrilla = grilla.getElementsByTagName('table'); //dgvSubOrigen
                for (var p = 1; p < subgrilla.length; p++) {
                    if (subgrilla[p] != null) {
                        for (var i = 1; i < subgrilla[p].rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                            var rowElemx = subgrilla[p].rows[i];
                            if (rowElemx.cells[0].children[0].id == obj.id) {
                                indextable = p;
                                break;
                            }
                        }
                    }
                }
                if (subgrilla[indextable] != null) {
                    for (var i = 1; i < subgrilla[indextable].rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                        var rowElemx = subgrilla[indextable].rows[i];
                        switch (opcion) {
                            case '0':
                                if (rowElemx.cells[0].children[0].id == obj.id) {
                                    hddindexsubgrillaOR.value = i - 1;
                                    rowElemx.cells[0].children[0].status = true;
                                } else {
                                    rowElemx.cells[0].children[0].status = false;
                                }
                                break;
                        }
                    }
                }
                for (var t = 1; t < grilla.rows.length; t++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElems = grilla.rows[t];
                    if ((indextable + 1) == t) {
                        hddindexgrillaOR.value = indextable;
                        document.getElementById('<%=btnEvento.ClientID%>').click();
                        return false;
                    }
                }
            }
            return true;
        }
        function valSelectSubCodigo(opcion, obj) {
            var indextable = 0;
            var hddindexgrillaCO = document.getElementById('<%=hddIndexGrillaCO.ClientID%>');
            var hddindexsubgrillaCO = document.getElementById('<%=hddIndexSubGrillaCO.ClientID%>');
            var grilla = document.getElementById('<%=dgvCodigo.ClientID%>');
            if (grilla != null) {
                var subgrilla = grilla.getElementsByTagName('table');
                for (var p = 1; p < subgrilla.length; p++) {
                    if (subgrilla[p] != null) {
                        for (var i = 1; i < subgrilla[p].rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                            var rowElemx = subgrilla[p].rows[i];

                            if (rowElemx.cells[0].children[0].id == obj.id) {
                                indextable = p;
                                break;
                            }
                        }
                    }
                }
                if (subgrilla[indextable] != null) {
                    for (var i = 1; i < subgrilla[indextable].rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                        var rowElemx = subgrilla[indextable].rows[i];
                        switch (opcion) {
                            case '0':
                                if (rowElemx.cells[0].children[0].id == obj.id) {
                                    hddindexsubgrillaCO.value = i - 1;
                                    rowElemx.cells[0].children[0].status = true;
                                } else {
                                    rowElemx.cells[0].children[0].status = false;
                                }
                                break;
                        }
                    }
                }
                for (var t = 1; t < grilla.rows.length; t++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElems = grilla.rows[t];
                    if (indextable + 1 == t) {
                        hddindexgrillaCO.value = indextable;
                        document.getElementById('<%=btnEvento.ClientID%>').click();
                        return true;
                    }
                }
            }
            return true;
        }
        function valSelectSubAtributos(opcion, obj) {
            var indextable = 0;
            var hddindexgrillaAT = document.getElementById('<%=hddIndexGrillaAT.ClientID%>');
            var hddindexsubgrillaAT = document.getElementById('<%=hddIndexSubGrillaAT.ClientID%>');
            var grilla = document.getElementById('<%=dgvAtributos.ClientID%>');
            if (grilla != null) {
                var subgrilla = grilla.getElementsByTagName('table');
                for (var p = 1; p < subgrilla.length; p++) {
                    if (subgrilla[p] != null) {
                        for (var i = 1; i < subgrilla[p].rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                            var rowElemx = subgrilla[p].rows[i];

                            if (rowElemx.cells[0].children[0].id == obj.id) {
                                indextable = p;
                                break;
                            }
                        }
                    }
                }
                if (subgrilla[indextable] != null) {
                    for (var i = 1; i < subgrilla[indextable].rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                        var rowElemx = subgrilla[indextable].rows[i];
                        switch (opcion) {
                            case '0':
                                if (rowElemx.cells[0].children[0].id == obj.id) {
                                    hddindexsubgrillaAT.value = i - 1;
                                    rowElemx.cells[0].children[0].status = true;
                                } else {
                                    rowElemx.cells[0].children[0].status = false;
                                }
                                break;
                        }
                    }
                }
                for (var t = 1; t < grilla.rows.length; t++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElems = grilla.rows[t];
                    if (indextable + 1 == t) {
                        hddindexgrillaAT.value = indextable;
                        document.getElementById('<%=btnEvento.ClientID%>').click();
                        return true;
                    }
                }
            }
            return true;
        }
        function valSelectSubSinUso(opcion, obj) {
            var indextable = 0;
            var hddindexgrillaSU = document.getElementById('<%=hddIndexGrillaSU.ClientID%>');
            var hddindexsubgrillaSU = document.getElementById('<%=hddIndexSubGrillaSU.ClientID%>');
            var grilla = document.getElementById('<%=dgvSinUso.ClientID%>');
            if (grilla != null) {
                var subgrilla = grilla.getElementsByTagName('table');
                for (var p = 1; p < subgrilla.length; p++) {
                    if (subgrilla[p] != null) {
                        for (var i = 1; i < subgrilla[p].rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                            var rowElemx = subgrilla[p].rows[i];

                            if (rowElemx.cells[0].children[0].id == obj.id) {
                                indextable = p;
                                break;
                            }
                        }
                    }
                }
                if (subgrilla[indextable] != null) {
                    for (var i = 1; i < subgrilla[indextable].rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                        var rowElemx = subgrilla[indextable].rows[i];
                        switch (opcion) {
                            case '0':
                                if (rowElemx.cells[0].children[0].id == obj.id) {
                                    hddindexsubgrillaSU.value = i - 1;
                                    rowElemx.cells[0].children[0].status = true;
                                } else {
                                    rowElemx.cells[0].children[0].status = false;
                                }
                                break;
                        }
                    }
                }
                for (var t = 1; t < grilla.rows.length; t++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElems = grilla.rows[t];
                    if (indextable + 1 == t) {
                        hddindexgrillaSU.value = indextable;
                        document.getElementById('<%=btnEvento.ClientID%>').click();
                        return true;
                    }
                }
            }
            return true;
        }


        function valAddTablaBusAvanz() {
            var cbotabla = document.getElementById('<%=cboTipoTablaBus.ClientID %>');
            if (parseFloat(cbotabla.value) == 0) {
                alert('Seleccione Tipo Tabla.');
                return false;
            }
            var grilla = document.getElementById('<%=dgvFiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valAddBusqueda() {
            var hddModoVal = document.getElementById('<%=hddModoVal.ClientID%>');
            if (parseFloat(hddModoVal.value) == 1) {
                var cbolinea = document.getElementById('<%=M_B_cmbLinea.ClientID %>');
                if (parseFloat(cbolinea.value) == 0) {
                    alert('Seleccione L�nea.');
                    return false;
                }
                var cbosublinea = document.getElementById('<%=M_B_cmbSubLinea.ClientID %>');
                if (parseFloat(cbosublinea.value) == 0) {
                    alert('Seleccione Sub L�nea.');
                    return false;
                }
            }
            return true;
        }

        //************************* BUSQUEDA PRODUCTO COMPONENTE

        function valKeyPressCodigoSL() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {


            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd() {
            if (event.keyCode == 13) {
                document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        //***************************************************** BUSQUEDA PERSONA

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valOnClick_btnBuscarComponenteKit() {
            var chb_HabilitarKit = document.getElementById('<%=chb_HabilitarKit.ClientID %>');
            var txtDescripcionProd_AddProd = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID %>');

            if (chb_HabilitarKit.status) {
                onCapa('capaBuscarProducto_AddProd');
                txtDescripcionProd_AddProd.select();
                txtDescripcionProd_AddProd.focus();
            } else {
                alert('EL PRODUCTO NO HA SIDO CATALOGADO COMO KIT. NO SE PERMITE LA OPERACI�N.');
            }
            return false;
        }
        function valSelectMagnitud(opcion, obj) {
            //****** 0: Principal
            //****** 1: Retazo
            var grilla = document.getElementById('<%=DGV_Magnitud.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    switch (opcion) {
                        case '0':  //*** Principal

                            if (rowElem.cells[4].children[0].id == obj.id) {
                                rowElem.cells[4].children[0].status = true;
                            } else {
                                rowElem.cells[4].children[0].status = false;
                            }
                            break;
                    }
                }
            }
            return true;
        }
        //
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=txt_PageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txt_PageIndexGo.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txt_PageIndexGO.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txt_PageIndexGO.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //    
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>

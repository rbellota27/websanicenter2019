<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantTablasGenerales.aspx.vb" Inherits="APPWEB.FrmMantTablasGenerales"
    Title="Tablas Generales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };

        function validaraddarea() {
            var total = document.getElementById('<%=hdd_tienerelacion.ClientID %>');

            if (total.value == '0') {
                alert('La existencia no tiene lineas relacionadas');
                return false;
            }
            return true;
        }

        function valOnClickAddTipoOperacion(opcion) {
            var grilla = null;
            var combo = null;
            switch (opcion) {
                case '0':  //****  Tipo Documento - Tipo operaci�n
                    combo = document.getElementById('<%=cmb_TipoOpera.ClientID%>');
                    grilla = document.getElementById('<%=DGV_TipoOpera.ClientID%>');
                    break;
                case '1':  //***** Motivo Traslado - Tipo operaci�n
                    combo = document.getElementById('<%=cmb_MotivoTipoOperacion.ClientID%>');
                    grilla = document.getElementById('<%=DGV_MotivoTipoOperacion.ClientID%>');
                    break;
            }


            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(combo.value)) {
                        alert('El Tipo de Operaci�n seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        /*--------------------------------------------*/
        function valOnClickAddTipoDocRef() {

            var combo = document.getElementById('<%=cmb_TipoDocRef.ClientID%>');
            var grilla = document.getElementById('<%=DGV_TipoDocRef.ClientID%>');

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(combo.value)) {
                        alert('El Tipo de Operaci�n seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        /*--------------------------------------------*/
        function valOnClickAddMedioP_CondicionP() {
            var cboCP = document.getElementById('<%=cbo_CondicionPago.ClientID%>');
            var grilla = document.getElementById('<%=DGV_CondicionPago.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(cboCP.value)) {
                        alert('El Medio de Pago ya ha sido ingresada.');
                        return false;
                    }
                }
            }
            return true;
        }
        /*---------------------------------------------*/
        function valOnClickAddTipoPrecioVenta() {
            var cboCP = document.getElementById('<%=cboPerfilTV.ClientID%>');
            var grilla = document.getElementById('<%=DGVPerfilTipoPV.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(cboCP.value)) {
                        alert('El Tipo Precio de Venta ya ha sido ingresada.');
                        return false;
                    }
                }
            }
            return true;
        }
        /*---------------------------------------------*/
        function valOnClickAddPerfilMedioPago() {
            var cboCP = document.getElementById('<%=cboPerfilMP.ClientID%>');
            var grilla = document.getElementById('<%=DGVPerfilmedioPago.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(cboCP.value)) {
                        alert('El Medio de Pago ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        /*---------------------------------------------*/
        function valOnClickAddConcepto_TipoDocumento() {
            var cboTD = document.getElementById('<%=cbo_TipoDocumento.ClientID%>');
            var grilla = document.getElementById('<%=DGV_TipoDocumento.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(cboTD.value)) {
                        alert('El Tipo de Documento ya ha sido ingresada.');
                        return false;
                    }
                }
            }
            return true;
        }
        /*---------------------------------------------*/

        function valOnClickAddUM_Magnitud() {
            var cboUM = document.getElementById('<%=M_cmbUMedida.ClientID%>');
            var grilla = document.getElementById('<%=DGV_UM.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(cboUM.value)) {
                        alert('La Unidad de Medida seleccionada ya ha sido ingresada.');
                        return false;
                    }
                }
            }
            return true;
        }
        function valSave() {

            //********** Validamos un Campo Requerido
            var txtDescripcion = document.getElementById('<%=txt1.ClientID%>');
            if (CajaEnBlanco(txtDescripcion)) {
                alert('Campo Requerido.');
                txtDescripcion.select();
                txtDescripcion.focus();
                return false;
            }


            //********** Validando < Magnitud >
            var grilla = document.getElementById('<%=DGV_UM.ClientID%>');
            if (grilla != null) {
                var selectPrincipal = false;
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (isNaN(rowElem.cells[2].children[0].value) || parseFloat(rowElem.cells[2].children[0].value) <= 0) {
                        alert('Ingrese una valor v�lido mayor a cero.');
                        rowElem.cells[2].children[0].select();
                        rowElem.cells[2].children[0].focus();
                        return false;
                    }
                    if (rowElem.cells[3].children[0].status) {
                        selectPrincipal = true;
                        break;
                    }
                }
                if (!selectPrincipal) {
                    alert('Seleccione una Unidad de Medida Principal para la Magnitud seleccionada. Tome en consideraci�n que las Equivalencias son respecto a la Unidad de Medida Principal seleccionada para la Magnitud.');
                    return false;
                }
            }

            //*********** Validando Oficina
            var grilla = document.getElementById('<%=DGV_Oficina.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (CajaEnBlanco(rowElem.cells[1].children[0].cells[0].children[0])) {
                        alert('Ingrese una descripci�n para la Oficina.');
                        rowElem.cells[1].children[0].cells[0].children[0].select();
                        rowElem.cells[1].children[0].cells[0].children[0].focus();
                        return false;
                    }
                }
            }

            var grillactd = document.getElementById('<%=DGV_TipoDocumento.ClientID %>');
            if (grillactd != null) {
                for (var i = 1; i < grillactd.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grillactd.rows[i];
                    if (rowElem.cells[3].children[0].value == '') {
                        alert('Ingrese un valor');
                        rowElem.cells[3].children[0].select();
                        rowElem.cells[3].children[0].focus();
                        return false;
                    }
                    if (isNaN(rowElem.cells[3].children[0].value)) {
                        alert('El valor ingresado no es correcto');
                        rowElem.cells[3].children[0].select();
                        rowElem.cells[3].children[0].focus();
                        return false;
                    }
                }
            }

            var conpTienda = document.getElementById('<%=gvConpTienda.ClientID %>');
            if (conpTienda != null) {
                var cont = 0;
                for (var i = 1; i < conpTienda.rows.length; i++) {
                    var rowElem = conpTienda.rows[i];
                    if (rowElem.cells[3].children[0].value == '') {
                        alert('Ingrese un valor');
                        rowElem.cells[3].children[0].select();
                        rowElem.cells[3].children[0].focus();
                        return false;
                    }
                    if (isNaN(rowElem.cells[3].children[0].value)) {
                        alert('El valor ingresado no es correcto');
                        rowElem.cells[3].children[0].select();
                        rowElem.cells[3].children[0].focus();
                        return false;
                    }
                    if (rowElem.cells[1].children[0].value == '') {

                        return false;
                    }

                    rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text
                    var cont = 0;
                    for (var r = 1; r < conpTienda.rows.length; r++) {
                        var row = conpTienda.rows[r];
                        if (rowElem.cells[1].children[0].value == row.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert("La tienda " + rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text + " ya esta agregada");
                            return false;
                        }
                    }
                    //++
                }
            }

            var grillaarea = document.getElementById('<%=gvarea.ClientID %>');
            if (grillaarea != null) {
                for (var i = 1; i < grillaarea.rows.length; i++) {
                    var filaarea = grillaarea.rows[i];
                    var contador = 0;

                    for (var p = 1; p < grillaarea.rows.length; p++) {
                        var fila = grillaarea.rows[p];
                        if (fila.cells[1].children[0].value == filaarea.cells[1].children[0].value) {
                            contador = contador + 1;
                        }
                        if (contador > 1) {

                            alert("La area " + fila.cells[1].children[0].options[fila.cells[1].children[0].selectedIndex].text + " ya esta agregada");
                            return false;
                        }

                    } //next

                } //next
            } //end if

            var grillaTipoDoc = document.getElementById('<%=gvtipodocmp.ClientID %>');
            if (grillaTipoDoc != null) {
                for (var i = 1; i < grillaTipoDoc.rows.length; i++) {
                    var filaarea = grillaTipoDoc.rows[i];
                    var contador = 0;

                    for (var p = 1; p < grillaTipoDoc.rows.length; p++) {
                        var fila = grillaTipoDoc.rows[p];
                        if (fila.cells[1].children[0].value == filaarea.cells[1].children[0].value) {
                            contador = contador + 1;
                        }
                        if (contador > 1) {

                            alert("La Tipo de documento " + fila.cells[1].children[0].options[fila.cells[1].children[0].selectedIndex].text + " ya esta agregada");
                            return false;
                        }

                    } //next

                } //next
            } //end if


            return confirm('Desea continuar con la Operaci�n ?');
        }


        function valOnClickPrincipal_Magnitud(obj) {
            var grilla = document.getElementById('<%=DGV_UM.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[0].id != obj.id) {
                        rowElem.cells[3].children[0].status = false;
                    }
                }
            }
            return true;
        }


        function validarKeyPressEq() {
            if (esEnter(event) == true) {
                return false;
            }
            return true;
        }


        function letras() {
            if (event.keyCode > 45 && event.keyCode < 58) event.returnValue = false;
        }

        function valCajaEscalar() {
            validarDecimal();
        }
        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }

        function valBlur(event) {
            var id = event.srcElement.id;
            var caja = document.getElementById(id);
            if (CajaEnBlanco(caja)) {
                caja.value = 0;
                //alert('Debe ingresar un valor.');
            }
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no v�lido.');
            }
        }
        function validarEq() {


            //************* validamos UM repetido
            var grilla = document.getElementById('<%=DGV_UM.ClientID%>');
            var cboUM = document.getElementById('<%=M_cmbUMedida.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].innerHTML == cboUM.value) {
                        alert('La Unidad de Medida ya ha sido ingresada.');
                        return false;
                    }
                }
            }


            return true;
        }

        //        function validar_grillaAreaxTExistencia() {

        //            //************* validamos Areas repetidas
        //            var grilla = document.getElementById('<%=gvarea.ClientID%>');            
        //            
        //            if (grilla != null) {
        //                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
        //                    for (var j = 1; i < grilla.rows.length; j++) {//comienzo en 1 xq 0 es la cabecera
        //                        if (i!=j ){
        //                            var rowElem = grilla.rows[i];
        //                            var rowElem2 = grilla.rows[j];
        //                            var index1 = rowElem.cells[1].children[0].selectedindex
        //                            var index2 = rowElem2.cells[1].children[0].selectedindex
        //                            if (index1 > -1 && index2>-1) {
        //                                if (rowElem.cells[1].children[0].options[index1].value == rowElem2.cells[1].children[0].options[index2].value) {
        //                                    alert('Esa �rea ya ha sido ingresada.');
        //                                    //var txtbox = document.getElementById('<%=LblMsjGrillaAreasxTExistencia.ClientID%>');
        //                                    
        //                                    return false;
        //                                }
        //                            }
        //                        }                     
        //                    }
        //                }
        //            }
        //            return true;
        //        }


        //************* validamos TxtBox en una grilla
        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }       

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td class="TituloCelda">
                        Mantenimiento Tablas Generales
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbltablaS" runat="server" CssClass="Texto" Text="Tabla:" Width="51px"></asp:Label>
                        <asp:DropDownList ID="cmbTabla" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbTabla_SelectedIndexChanged"
                            Width="223px">
                            <asp:ListItem Selected="True">Seleccione una Tabla</asp:ListItem>
                            <asp:ListItem>Area</asp:ListItem>
                            <asp:ListItem>Banco</asp:ListItem>
                            <%--<asp:ListItem>C�lculo</asp:ListItem>                            --%>
                            <%--<asp:ListItem>Calidad</asp:ListItem>--%>
                            <asp:ListItem>Cargo</asp:ListItem>
                            <asp:ListItem>Categor�a de Empleado</asp:ListItem>
                            <asp:ListItem>Chofer</asp:ListItem>
                            <%--<asp:ListItem>Color</asp:ListItem>--%>
                            <asp:ListItem>Concepto</asp:ListItem>
                            <asp:ListItem>Condici�n Comercial</asp:ListItem>
                            <asp:ListItem>Condici�n de Pago</asp:ListItem>
                            <asp:ListItem>Condici�n de Trabajo</asp:ListItem>
                            <%--<asp:ListItem>Estado Cargo Programado</asp:ListItem>--%>
                            <asp:ListItem>Estado Civil</asp:ListItem>
                            <asp:ListItem>Estado del Documento</asp:ListItem>
                            <asp:ListItem>Estado del Producto</asp:ListItem>
                            <%--<asp:ListItem>Estilo</asp:ListItem>--%>
                            <%--<asp:ListItem>Fabricante</asp:ListItem>--%>
                            <%--<asp:ListItem>Formato</asp:ListItem>--%>
                            <asp:ListItem>Giro</asp:ListItem>
                            <asp:ListItem>Impuesto</asp:ListItem>
                            <asp:ListItem>Magnitud</asp:ListItem>
                            <%--<asp:ListItem>Marca</asp:ListItem>--%>
                            <asp:ListItem>Medio de Pago</asp:ListItem>
                            <%--<asp:ListItem>Modelo</asp:ListItem>--%>
                            <asp:ListItem>Moneda</asp:ListItem>
                            <asp:ListItem>Motivo de Baja</asp:ListItem>
                            <asp:ListItem>Motivo Gasto</asp:ListItem>
                            <asp:ListItem>Motivo de Traslado</asp:ListItem>
                            <asp:ListItem>Movimiento Cuenta Tipo</asp:ListItem>
                            <asp:ListItem>Nacionalidad</asp:ListItem>
                            <asp:ListItem>Perfil</asp:ListItem>
                            <asp:ListItem>Profesi�n</asp:ListItem>
                            <asp:ListItem>R�gimen</asp:ListItem>
                            <asp:ListItem>Rol</asp:ListItem>
                            <asp:ListItem>Sector</asp:ListItem>
                            <asp:ListItem>Sector-Sublinea</asp:ListItem>
                            <%--<asp:ListItem>Talla</asp:ListItem>--%>
                            <%--<asp:ListItem>Tarjeta</asp:ListItem> Ya tiene Mantenimiento--%>
                            <%--<asp:ListItem>Sabor</asp:ListItem>--%>
                            <asp:ListItem>Tipo de Agente</asp:ListItem>
                            <%--<asp:ListItem>Tama�o</asp:ListItem>--%>
                            <asp:ListItem>Tipo de Comision</asp:ListItem>
                            <%--<asp:ListItem>Tipo de Concepto Banco</asp:ListItem>--%>
                            <asp:ListItem>Tipo de Correo</asp:ListItem>
                            <asp:ListItem>Tipo de Direcci�n</asp:ListItem>
                            <asp:ListItem>Tipo de Documento</asp:ListItem>
                            <asp:ListItem>Tipo de Documento Identidad</asp:ListItem>
                            <asp:ListItem>Tipo de Existencia</asp:ListItem>
                            <asp:ListItem>Tipo de Gasto</asp:ListItem>
                            <asp:ListItem>Tipo de Movimiento</asp:ListItem>
                            <asp:ListItem>Tipo de Operaci�n</asp:ListItem>
                            <asp:ListItem>Tipo de Precio de Importaci�n</asp:ListItem>
                            <asp:ListItem>Tipo de Precio de Venta</asp:ListItem>
                            <asp:ListItem>Tipo de Tel�fono</asp:ListItem>
                            <asp:ListItem>Tipo de Tienda</asp:ListItem>
                            <%--<asp:ListItem>Tr�nsito</asp:ListItem>--%>
                            <asp:ListItem>V�a Tipo</asp:ListItem>
                            <asp:ListItem>Unidad de Medida</asp:ListItem>
                            <asp:ListItem>Zona Tipo</asp:ListItem>
                            <asp:ListItem>Alias</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda" style="height: 21px">
                        <asp:Label ID="lblNomTabla" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblMensaje" CssClass="LabelRojo" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" OnClick="btnNuevo_Click" />
                                    <asp:ImageButton ID="btnGuardar" runat="server" OnClientClick="return( valSave() );"
                                        ImageUrl="~/Imagenes/Guardar_B.JPG" />
                                    <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                        OnClick="btnCancelar_Click" />
                                </td>
                            </tr>
                        </table>
                        <asp:Panel ID="Panel_Datos" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCodigo" runat="server" CssClass="Label" Text="C�digo:" Width="136px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCodigo" runat="server" onkeypress="return(onKeyPressEnter(event));"
                                            Width="111px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_Chofer">
                                    <td>
                                        <asp:Label ID="lblChofer" runat="server" Text="Chofer:" Width="136px" CssClass="Label"></asp:Label><br />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlChofer" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDescripcion" runat="server" Text="Descripci�n:" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                            ID="txt1" runat="server" AutoPostBack="false" onkeypress="return(onKeyPressEnter(event));"
                                            Width="289px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl4" runat="server" Text="Label" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                            ID="txt3" runat="server" onkeypress="return(onKeyPressEnter(event));" Width="289px"
                                            AutoPostBack="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDescBreve" runat="server" Text="Descripci�n Breve:" Width="136px"
                                            CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                            ID="txt2" onkeypress="return(onKeyPressEnter(event));" runat="server" AutoPostBack="false"
                                            Width="187px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAbrev" runat="server" Text="Abreviatura:" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAbrev" onkeypress="return(onKeyPressEnter(event));" runat="server"
                                            AutoPostBack="false" Width="187px"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr runat="server" id="tr_TipoGasto">
                                    <td>
                                        <asp:Label ID="lblTipoGasto" runat="server" Text="Tipo de Gasto:" Width="136px" CssClass="Label"></asp:Label><br />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="CboTipoGasto" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_CtaContablemp">
                                    <td>
                                        <asp:Label ID="lblcuentaContablemp" runat="server" Text="Cta Contable:" Width="136px"
                                            CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcuentacontablemp" MaxLength="12" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_TipoDocumento">
                                    <td>
                                        <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de Documento:" Width="136px"
                                            CssClass="Label">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="CboTipoDocumento" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_principalmp">
                                    <td>
                                        <asp:Label ID="lblprincipalmp" runat="server" Text="Principal:" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="ckprincipalmp" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_Regimen">
                                    <td>
                                        <asp:Label ID="lblRegimen" runat="server" Text="R�gimen:" Width="136px" CssClass="Label"></asp:Label><br />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboRegimen" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_TipoConceptoBanco">
                                    <td>
                                        <asp:Label ID="lblTipoConceptoBanco" runat="server" Text="Filtrar:" Width="136px"
                                            CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="dlTipoConceptoBanco" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_ConceptoMovBanco">
                                    <td>
                                        <asp:Label ID="lblConceptoMovBanco" runat="server" Text="Concepto Mov. Banco:" Width="136px"
                                            CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="dlConceptoMovBanco" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_Concepto">
                                    <td>
                                        <asp:Label ID="llbConcepto" runat="server" Text="Concepto de Gasto:" Width="136px"
                                            CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="dlConcepto" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_Pais">
                                    <td>
                                        <asp:Label ID="lblPais" runat="server" Text="Pa�s:" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="dlPais" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_TipoOperacion">
                                    <td>
                                        <asp:Label ID="lblTipoOperacion" runat="server" Text="Tipo de Operaci�n:" Width="136px"
                                            CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTipoOperacion" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_interfazmp">
                                    <td>
                                        <asp:Label ID="lblinterfazmp" runat="server" Text="Interfaz:" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="dlinterfazmp" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_centrocosto">
                                    <td>
                                        <asp:Label ID="lblcentrocosto" runat="server" Text="Centro Costo:" Width="136px"
                                            CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="LabelTdLeft">
                                                    &nbsp;Unidad Negocio:<br />
                                                    <asp:DropDownList ID="dlunidadnegocio" runat="server" Width="150px" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="LabelTdLeft">
                                                    &nbsp;Dpto. Funcional:<br />
                                                    <asp:DropDownList ID="dldptofuncional" runat="server" Width="150px" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="LabelTdLeft">
                                                    &nbsp;Sub-�rea 1:<br />
                                                    <asp:DropDownList ID="dlsubarea1" runat="server" Width="150px" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="LabelTdLeft">
                                                    &nbsp;Sub-�rea 2:<br />
                                                    <asp:DropDownList ID="dlsubarea2" runat="server" Width="150px" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="LabelTdLeft">
                                                    &nbsp;Sub-�rea 3:<br />
                                                    <asp:DropDownList ID="dlsubarea3" runat="server" Width="150px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_controlador">
                                    <td class="Label" Width="136px">
                                        Controlador:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlControlador" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_rel_Sector">
                                    <td class="Label"  width="136px">
                                        Sector:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSector_rel" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_rel_Sector_sublinea">
                                    <td class="Label" width="136px">
                                        Linea:
                                    </td>
                                    <td>
                                        <table align="left">
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlLinea_rel" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                </td>
                                                <td class="Label">
                                                    SubLinea:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlSubLinea_rel" runat="server"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAgreagarSublinea" runat="server" Text="Agregar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_gvSector_rel_Sublinea">
                                    <td></td>
                                    <td>
                                        <asp:GridView ID="gvSector_rel_Sublinea" runat="server" 
                                            AutoGenerateColumns="False">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                <asp:BoundField DataField="IdSubLInea" HeaderText="Id" />
                                                <asp:BoundField DataField="sl_Nombre" HeaderText="Sub Linea" />
                                            </Columns>
                                            <HeaderStyle CssClass="GrillaHeader" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_check1">
                                    <td>
                                        <asp:Label ID="lblCheck1" runat="server" Text="Check" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkCheck1" runat="server" CssClass="Texto" />
                                        <asp:Label ID="lblCheck1Message" runat="server" Text="" CssClass="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_check2">
                                    <td>
                                        <asp:Label ID="lblCheck2" runat="server" Text="Check" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkCheck2" runat="server" />
                                        <asp:Label ID="lblCheck2Message" runat="server" Text="" CssClass="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_check3">
                                    <td>
                                        <asp:Label ID="lblCheck3" runat="server" Text="Check" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkCheck3" runat="server" />
                                        <asp:Label ID="lblCheck3Message" runat="server" Text="" CssClass="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_check4">
                                    <td>
                                        <asp:Label ID="lblCheck4" runat="server" Text="Check" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkCheck4" runat="server" />
                                        <asp:Label ID="lblCheck4Message" runat="server" Text="" CssClass="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_caja1">
                                    <td>
                                        <asp:Label ID="lblcaja1" runat="server" Text="caja1" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcaja1" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_caja2">
                                    <td>
                                        <asp:Label ID="lblcaja2" runat="server" Text="caja2" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcaja2" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_combo1">
                                    <td>
                                        <asp:Label ID="lblCombo1" runat="server" Text="Combo1" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboCombo1" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblCombo1Message" runat="server" Text="" CssClass="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_estado">
                                    <td>
                                        <asp:Label ID="lblEstado" runat="server" Text="Estado:" Width="136px" CssClass="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rdbEstados" runat="server" RepeatDirection="Horizontal"
                                            CssClass="Label">
                                            <asp:ListItem Selected="True">Activo</asp:ListItem>
                                            <asp:ListItem>Inactivo</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_MonedaBase">
                                    <td>
                                        <asp:Label ID="lbl5" runat="server" Width="136px" CssClass="Label" Text="Moneda Base:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rdbBase" runat="server" CssClass="Label" RepeatDirection="Horizontal"
                                            AutoPostBack="True">
                                            <asp:ListItem Value="1">S�</asp:ListItem>
                                            <asp:ListItem Value="2" Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_gv_area">
                                    <td valign="top">
                                        <asp:Button ID="btaddarea" runat="server" OnClientClick="return(validaraddarea());"
                                            Text="Agregar Area" /><br />
                                        <asp:HiddenField ID="hdd_tienerelacion" runat="server" Value="0" />
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="gvarea" runat="server" GridLines="None" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                <asp:TemplateField HeaderText="Area">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="dlarea" runat="server" DataValueField="Id" DataTextField="DescripcionCorta"
                                                            DataSource='<%# DataBinder.Eval(Container.DataItem,"objArea") %>' Width="220px">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Estado">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ckestado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"LAEstado") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="GrillaRow" />
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaCabecera" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                        <asp:Label ID="LblMsjGrillaAreasxTExistencia" CssClass="LabelRojo" runat="server"
                                            Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_DocumentoReferente">
                                    <td>
                                        <asp:Label ID="lblDocumentoReferente" runat="server" CssClass="Label" Width="136px"
                                            Text="Stock en Transito:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rdbDocumentoReferente" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0">No </asp:ListItem>
                                            <asp:ListItem Value="1">Si </asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <table>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="LblMsj" CssClass="LabelRojo" runat="server" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="height: 32px">
                        <asp:Panel ID="Panel_MU" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda" style="height: 21px">
                                        Magnitud Unidad
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 70%; margin-bottom: 0px;">
                                            <tr>
                                                <td style="width: 129px">
                                                    <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Unidad de Medida:"></asp:Label>
                                                </td>
                                                <td style="width: 99px">
                                                    <asp:DropDownList ID="M_cmbUMedida" runat="server" onblur="return(validarEq())">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnAgregarUM" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        OnClientClick="return( valOnClickAddUM_Magnitud()  );" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" Width="61px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="DGV_UM" runat="server" AutoGenerateColumns="False" Width="470px">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                <asp:TemplateField HeaderText="U.Medida">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdUnidadMedida") %>' />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label41" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NombreCortoUM") %>'>
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Equivalencia">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtEquivalencia_UM" runat="server" onKeypress="return(validarNumeroPunto(event));"
                                                            onFocus="return(  aceptarFoco(this)   );" onblur="  return(  valBlurClear('0',event)  ); "
                                                            Text='<%# DataBinder.Eval(Container.DataItem,"Equivalencia","{0:F4}") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Principal">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chb_umPrincipal" runat="server" onClick="return(   valOnClickPrincipal_Magnitud(this)  );"
                                                            Checked='<%# DataBinder.Eval(Container.DataItem,"UnidadPrincipal") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_TipoDocOpera" runat="server" Width="994px">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda" style="height: 21px">
                                        Tipo de Operaci�n
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 48%; margin-bottom: 0px;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label42" runat="server" CssClass="Label" Text="Tipo de Operaci�n:"></asp:Label>
                                                </td>
                                                <td style="width: 99px">
                                                    <asp:DropDownList ID="cmb_TipoOpera" runat="server" onblur="return(validarEq())">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnAgregarTipoOpera" runat="server" OnClientClick="return(   valOnClickAddTipoOperacion('0')    );"
                                                        ImageUrl="~/Imagenes/Agregar_B.JPG" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" ValidationGroup="valUM" Width="61px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="DGV_TipoOpera" runat="server" AutoGenerateColumns="False" Width="470px">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                <asp:TemplateField HeaderText="Tipo de Operaci�n">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:HiddenField ID="HiddenField3" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoOperacion") %>' />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label43" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_TipoDocRef" runat="server" Width="994px">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda" style="height: 21px">
                                        Tipo de Documento de Referencia
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 48%; margin-bottom: 0px;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelTipoDocRef" runat="server" CssClass="Label" Text="Tipo de Documento de Referencia:"></asp:Label>
                                                </td>
                                                <td style="width: 99px">
                                                    <%--<asp:DropDownList ID="cmb_TipoDocRef" runat="server" onblur="return(validarEq())">
                                                    </asp:DropDownList>--%>
                                                    <asp:DropDownList ID="cmb_TipoDocRef" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <%--<asp:ImageButton ID="btnAgregarTipoDocRef" runat="server" OnClientClick="return(   valOnClickAddTipoOperacion('0')    );"
                                                        ImageUrl="~/Imagenes/Agregar_B.JPG" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" ValidationGroup="valUM" 
                                                        Width="61px" />--%>
                                                    <asp:ImageButton ID="btnAgregarTipoDocRef" runat="server" OnClientClick="return(  valOnClickAddTipoDocRef('0')  );"
                                                        ImageUrl="~/Imagenes/Agregar_B.JPG" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" ValidationGroup="valUM" Width="61px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="DGV_TipoDocRef" runat="server" AutoGenerateColumns="False" Width="470px">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                <asp:TemplateField HeaderText="Tipo de Documento de Referencia">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:HiddenField ID="HiddenFielTipodocRef" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoDocRef") %>' />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelTipodocRef" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Banco" runat="server" Width="994px">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda" style="height: 21px">
                                        Oficina
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="btnAgregarOficina" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                            onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';"
                                            ValidationGroup="valUM" Width="61px" />
                                        <asp:GridView ID="DGV_Oficina" runat="server" AutoGenerateColumns="False" Width="470px">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                <asp:TemplateField HeaderText="Oficina">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtBoxDescrOficina" onFocus="return(  aceptarFoco(this)   ); return ( onFocusTextTransform(this,configurarDatos) );"
                                                                        runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                        onKeypress="return(letras());" Text='<%# DataBinder.Eval(Container.DataItem,"Descripcion") %>'>
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:HiddenField ID="HddF_IdOficina" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Estado">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chb_EstadoOficina" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="C�digo" />
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_MotivoTraslado" runat="server" Width="994px">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda" style="height: 21px">
                                        Tipo de Operaci�n
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 60%; margin-bottom: 0px;">
                                            <tr>
                                                <td style="width: 124px">
                                                    <asp:Label ID="Label47" runat="server" CssClass="Label" Text="Tipo de Operaci�n:"></asp:Label>
                                                </td>
                                                <td style="width: 337px">
                                                    <asp:DropDownList ID="cmb_MotivoTipoOperacion" runat="server" onblur="return(validarEq())">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnAgregarMotivoTipoOperacion" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        OnClientClick="return(   valOnClickAddTipoOperacion('1')    );" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" Style="margin-bottom: 0px"
                                                        Width="61px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="DGV_MotivoTipoOperacion" runat="server" AutoGenerateColumns="False"
                                                    Width="470px">
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                        <asp:TemplateField HeaderText="Nombre">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:HiddenField ID="HiddenField4" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"idTipoOperac") %>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label46" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NombreTipoOperacion") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                </asp:GridView>
                                                <br />
                                            </td>
                                        </tr>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_CondicionPago" runat="server" Width="994px">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda" style="height: 21px">
                                        Medio de Pago
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 60%; margin-bottom: 0px;">
                                            <tr>
                                                <td style="width: 53px">
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                                                </td>
                                                <td style="width: 128px">
                                                    <asp:DropDownList ID="cbo_CondicionPago" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="BtonA�adir" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        OnClientClick="return(   valOnClickAddMedioP_CondicionP( )    );" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" Style="margin-bottom: 0px"
                                                        Width="61px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="DGV_CondicionPago" runat="server" AutoGenerateColumns="False" Width="470px">
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                        <asp:TemplateField HeaderText="Descripci�n">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:HiddenField ID="HiddenField4" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMedioPago") %>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label46" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomMedioPago") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                </asp:GridView>
                                                <br />
                                            </td>
                                        </tr>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_TipoDocumento" runat="server" Width="994px">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda">
                                        Tipo de Documento
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 120px">
                                                    <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Tipo de Documento:"></asp:Label>
                                                </td>
                                                <td style="width: 128px">
                                                    <asp:DropDownList ID="cbo_TipoDocumento" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="BotonA�adirTipoDoc" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        OnClientClick="return(   valOnClickAddConcepto_TipoDocumento( )    );" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" Style="margin-bottom: 0px"
                                                        Width="61px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="DGV_TipoDocumento" runat="server" AutoGenerateColumns="False" Width="969px">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                <asp:TemplateField HeaderText="Tipo de Documento">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:HiddenField ID="HddIdTipoDocumento" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoDocumento") %>' />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblNomTipoDoc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DescTipoDocumento") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Moneda">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="dlmoneda" runat="server" DataTextField="SimboloMoneda" DataValueField="IdMoneda"
                                                            Width="120px" DataSource='<%# DataBinder.Eval(Container.DataItem,"NombreMoneda") %>'>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valor del Concepto">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCtDocValor" runat="server" onfocus="return(aceptarFoco(this));"
                                                            onKeyPress="return(validarNumeroPuntoPositivo('event'));" Text='<%# DataBinder.Eval(Container.DataItem,"CtDoc_Valor","{0:F2}") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valor del Flete">
                                                    <ItemTemplate>
                                                        <asp:RadioButtonList ID="rb_fletexkilofijo" runat="server" RepeatDirection="Horizontal"
                                                            SelectedValue='<%# DataBinder.Eval(Container.DataItem,"fletexkilofijo") %>'>
                                                            <asp:ListItem Value="NULL">Ninguno</asp:ListItem>
                                                            <asp:ListItem Value="1">Calculado por Peso (kg)</asp:ListItem>
                                                            <asp:ListItem Value="0">Valor fijo</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tipo de C�lculo">
                                                    <ItemTemplate>
                                                        <asp:RadioButtonList ID="rb_con_TipoCalculo" runat="server" RepeatDirection="Horizontal"
                                                            SelectedValue='<%# DataBinder.Eval(Container.DataItem,"con_TipoCalculo") %>'>
                                                            <asp:ListItem Value="NULL">Ninguno</asp:ListItem>
                                                            <asp:ListItem Value="M">Valor Monetario</asp:ListItem>
                                                            <asp:ListItem Value="P">Por Peso (kg)</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Zona Ubigeo">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ckZona" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"con_ZonaUbigeo") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCelda">
                                        Tienda
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        &nbsp;
                                        <asp:ImageButton ID="btaddTiendaConcepto" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                            onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';"
                                            Style="margin-bottom: 0px" Width="61px" /><br />
                                        <asp:GridView ID="gvConpTienda" runat="server" AutoGenerateColumns="False" Width="552px">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                <asp:TemplateField HeaderText="Tienda">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="dlTiendaTie" runat="server" Width="220px" DataSource='<%# DataBinder.Eval(Container.DataItem,"ObjTienda") %>'
                                                            DataTextField="Nombre" DataValueField="Id">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Moneda">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="dlMonedaTie" runat="server" Width="65px" DataSource='<%# DataBinder.Eval(Container.DataItem,"ObjMoneda") %>'
                                                            DataTextField="Simbolo" DataValueField="Id">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Monto">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="tbmonto" runat="server" onfocus="return(aceptarFoco(this));" onKeyPress="return(validarNumeroPuntoPositivo('event'));"
                                                            Text='<%# DataBinder.Eval(Container.DataItem,"tie_monto") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Estado">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ckestado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"tie_estado") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel Width="994px" ID="pnltipodocumetomp" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="TituloCelda" colspan="2" style="height: 21px">
                                        Tipo de Documento
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 400px;">
                                        <asp:GridView ID="gvtipodocmp" runat="server" GridLines="None" AutoGenerateColumns="False">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                <asp:TemplateField HeaderText="Tipo de Documento">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="dltipodocumentomp" DataTextField="Descripcion" DataValueField="Id"
                                                            Width="220px" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"ObjTipoDocumento") %>'>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Estado">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ckestadomp" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaCabecera" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                    </td>
                                    <td valign="top">
                                        <asp:Button ID="btaddtipodocumento" runat="server" Text="Agregar" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelPerfilTipoPV" runat="server" Width="994px">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda" style="height: 21px">
                                        Tipo Precio de Venta
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 41%; margin-bottom: 0px;">
                                            <tr>
                                                <td style="width: 59px">
                                                    <asp:Label ID="Label3" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                                                </td>
                                                <td style="width: 214px">
                                                    <asp:DropDownList ID="cboPerfilTV" runat="server" Height="22px" Width="212px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        OnClientClick="return(   valOnClickAddTipoPrecioVenta( )    );" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" Style="margin-bottom: 0px"
                                                        Width="61px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="DGVPerfilTipoPV" runat="server" AutoGenerateColumns="False" Width="470px">
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                        <asp:TemplateField HeaderText="Tipo Precio de Venta">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:HiddenField ID="HddIdPerfil" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoPv") %>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblNombrePerfil" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NombreTipoPV") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Estado">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChboxEstado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"VerEstado") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                </asp:GridView>
                                                <br />
                                            </td>
                                        </tr>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelPerfilMedioPago" runat="server" Width="994px">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda" style="height: 21px">
                                        Medio de Pago
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 41%; margin-bottom: 0px;">
                                            <tr>
                                                <td style="width: 59px">
                                                    <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                                                </td>
                                                <td style="width: 214px">
                                                    <asp:DropDownList ID="cboPerfilMP" runat="server" Height="22px" Width="212px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnAgregarPerfilMedioPago" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        OnClientClick="return(   valOnClickAddPerfilMedioPago( )    );" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='/Imagenes/Agregar_A.JPG';" Style="margin-bottom: 0px"
                                                        Width="61px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="DGVPerfilMedioPago" runat="server" AutoGenerateColumns="False"
                                                    Width="470px">
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" />
                                                        <asp:TemplateField HeaderText="Medio de Pago">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:HiddenField ID="HddIdPerfilMP" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMedioPago") %>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblNombrePerfilMP" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"MedioPago") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Estado">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChboxEstado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                </asp:GridView>
                                                <br />
                                            </td>
                                        </tr>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblBuscarPor" runat="server" CssClass="Label" Text="Buscar Por:" Visible="False"
                            Width="79px"></asp:Label>
                        <asp:DropDownList ID="cmbBuscarPor" runat="server" Visible="False" Width="139px">
                            <asp:ListItem>C�digo</asp:ListItem>
                            <asp:ListItem Selected="True">Descripci�n</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lblTextoBusqueda" runat="server" CssClass="Label" Text="Texto de B�squeda:"
                            Visible="False" Width="131px"></asp:Label>
                        <asp:TextBox ID="txttextoBusqueda" runat="server" AutoPostBack="false" onkeypress="return(onKeyPressEnter(event));"
                            Visible="False" Width="200px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                            onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                            onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                            Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 22px">
                        <table>
                            <tr>
                                <td align="center" style="text-align: left">
                                    <asp:GridView ID="DGV1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                                        Height="16px" Width="897px" Style="text-align: left" AutoGenerateColumns="False"
                                        AllowPaging="True">
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView>
                                    <asp:Label ID="lblMsjB" runat="server" CssClass="LabelRojo" Text="Label"></asp:Label>
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 36px">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEstadoB" runat="server" CssClass="Label" Text="Estado:" Visible="False"
                                        Width="50px"></asp:Label>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rdbEstadosB" runat="server" AutoPostBack="True" CssClass="Label"
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem>Todos</asp:ListItem>
                                        <asp:ListItem Selected="True">Activo</asp:ListItem>
                                        <asp:ListItem>Inactivo</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 129px">
                        <asp:HiddenField ID="hddTabla" runat="server" Visible="False" />
                        <asp:HiddenField ID="hddModo" runat="server" Visible="False" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>

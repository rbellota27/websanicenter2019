﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmMantModulo.aspx.vb" Inherits="APPWEB.FrmMantModulo" MasterPageFile="~/Principal.Master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="ContentModulo" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="100%" >
       <tr>
            <td>
                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseout="this.src='/Imagenes/Nuevo_b.JPG';"
                    onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" />

                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                    onmouseout="this.src='/Imagenes/Guardar_B.JPG';" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                     />
              

                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                    onmouseout="this.src='/Imagenes/Arriba_B.JPG';" onmouseover="this.src='/Imagenes/Arriba_A.JPG';"
                    ToolTip="Regresar a Administración Sistema" />
            </td>
       </tr>
        <tr>
            <td class="TituloCelda">
                Modulo
            </td>
        </tr>


        <tr>
                <td>
                <asp:Panel ID="Panel_Modulo" runat="server" Width="100%">
                    <table cellpadding="0" cellspacing="3">
                        <tr>
                            <td class="Texto">
                                Codigo:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigoModulo" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                    Width="120px"></asp:TextBox>
                            </td>
                             
                        </tr>
                       <tr>
                      <td class="Texto">
                                Nombre:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNombre" runat="server" MaxLength="25" Width="121px" ></asp:TextBox>
                            </td>
                       </tr>
                        
                        <tr>
                                                      
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>

        <tr>
            <td>
                <asp:GridView ID="DGV_ModuloBusqueda" Width="27%" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="1" EmptyDataText="No existe información."
                    ForeColor="#333333" GridLines="None"  DatakeyNames="IdModulo">
                    <RowStyle BackColor="#F7F6F3" CssClass="GrillaRow" ForeColor="#333333" />
                    <Columns>
                    
                        <asp:BoundField DataField="IdModulo" HeaderText="Código" NullDisplayText="0" >
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" NullDisplayText="-----" />
                       
                        <asp:CommandField  SelectText="Editar" ShowSelectButton="True" />
                        
                        <asp:ButtonField CommandName="Delete" HeaderText="Eliminar" ShowHeader="True" 
                            Text="Eliminar" />
                       
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" CssClass="GrillaFooter" Font-Bold="True" ForeColor="White" />
                    
                    <SelectedRowStyle BackColor="#E2DED6" CssClass="GrillaSelectedRow" Font-Bold="True"
                        ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" CssClass="GrillaHeader" Font-Bold="True" ForeColor="White" />
                    
                    <AlternatingRowStyle BackColor="White" CssClass="GrillaRowAlternating" ForeColor="#284775" />
                </asp:GridView>
                </td>
        </tr>

        <tr>
            <td>
            
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>


</table>
  <script language="javascript" type="text/javascript">
        
</script>
</asp:Content>

<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantPais.aspx.vb" Inherits="APPWEB.FrmMantPais" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
        function valAddPerfil() {
            var cboPerfil = document.getElementById('<%=cmbProcedencia_Pais.ClientID %>');
            var grilla = document.getElementById('<%=DGV_PerfilUsuario.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(cboPerfil.value)) {
                        alert('El Perfil seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }


        function registrarP() {
            offCapa('capaBuscarP');
            onCapa('capaRegistrarP');
        }
        function validarRegPersona() {
            var caja = document.getElementById('<%=txtApPaterno_RegPersona.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('El campo <Ap. Paterno> es un campo requerido.');
                caja.focus();
                return false;
            }
            caja = document.getElementById('<%=txtNombres_RegPersona.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('El campo <Nombres> es un campo requerido.');
                caja.focus();
                return false;
            }
            return true;
        }
        function validarCajaBlanco() {
            var caja = document.getElementById('<%=txtUsuario.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('Ingrese un Login');
                caja.select();
                caja.focus();
                return false;
            } else {
                return true;
            }
        }
        function validarId() {
            var caja = document.getElementById('<%=txtCodigo.ClientID %>');
            if (caja.value.length == 0) {
                alert('Ingrese D.N.I.');
                caja.select();
                caja.focus();
                return false;
            }
            return true;
        }
        function validarSave() {
            var caja = document.getElementById('<%=txtPersona.ClientID %>');
            if (caja.value.length == 0) {
                alert('Debe ingresar nombre de pais.');
                var cajaCod = document.getElementById('<%=txtPersona.ClientID %>');
                cajaCod.select();
                cajaCod.focus();
                return false;
            }
            caja = document.getElementById('<%=cmbMotivoBaja.ClientID %>');
            if (caja.value.length == 0) {
                alert('Debe ingresar una procedencia.');
                caja.select();
                caja.focus();
                return false;
            }
            var rdbEstado = document.getElementById('<%=rdbEstado.ClientID %>');
            var hddModo = document.getElementById('<%=hddModo.ClientID %>');

            return (confirm('Desea Continuar con la operaci�n'));
        }

        function validar(e) { // 1
            tecla = (document.all) ? e.keyCode : e.which; // 2
            if (tecla == 8) return true; // 3
            patron = /[A-Za-z\s]/; // 4
            te = String.fromCharCode(tecla); // 5
            return patron.test(te); // 6
        }    
    </script>
    <script language="javascript" type="text/javascript">
            //////////////////////////////////////////
            var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
            /////////////////////////////////////////
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                            onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                        <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                            onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                            OnClientClick="return(validarSave());" CausesValidation="true" Style="height: 26px" />
                        <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                            onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                            OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Usuario" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda">
                                        PA�S
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCeldaLeft">
                                        <asp:Image ID="imgPerfil" runat="server" />&nbsp;Datos Pais
                                        <cc1:CollapsiblePanelExtender ID="cpePerfil" runat="server" TargetControlID="Panel_Perfil"
                                            ImageControlID="imgPerfil" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                            Collapsed="false" CollapseControlID="imgPerfil" ExpandControlID="imgPerfil">
                                        </cc1:CollapsiblePanelExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel_Perfil" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label28" runat="server" CssClass="Label" Text="Procedencia:" Visible="False"></asp:Label>
                                                        <asp:DropDownList ID="cmbProcedencia_Pais" runat="server" DataTextField="proc_Nombre"
                                                            DataValueField="IdProcedencia" Visible="False">
                                                        </asp:DropDownList>
                                                        <asp:ImageButton ID="btnAgregarTAP" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                            OnClientClick="return(valAddPerfil());" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                            onmouseover="this.src='/Imagenes/Agregar_A.JPG';" Visible="False" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="DGV_PerfilUsuario" runat="server" AutoGenerateColumns="False" Width="500px"
                                                            Visible="False">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Procedencia" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px">
                                                                    <ItemTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:HiddenField ID="hddIdPerfil" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProcedencia")%>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblPerfil" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Height="25px" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Activo" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbEstado_UsuarioPerfil" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                            <FooterStyle CssClass="GrillaFooter" />
                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                            <PagerStyle CssClass="GrillaPager" />
                                                            <RowStyle CssClass="GrillaRow" />
                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre Pa�s"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox CssClass="TextBoxReadOnly" ID="txtPersona" runat="server" Width="420px"
                                                        BackColor="White" onkeypress="return validar(event)"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodigoPersona" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                        Width="83px" Enabled="False"></asp:TextBox>
                                                    <asp:ImageButton ID="btnBuscarPersona" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                                                        onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                                        OnClientClick="return(onCapa('capaBuscarP'));" />
                                                    <asp:TextBox ID="txtCodigo" Width="150px" runat="server" MaxLength="20" Enabled="False"
                                                        ReadOnly="True" Visible="False"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label12016" runat="server" CssClass="Label" Text="C�d. Pa�s:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCodPais" runat="server" MaxLength="1" Width="84px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" CssClass="Label" Text="C�digo Procedencia:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbMotivoBaja" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkPaisBase" runat="server" CssClass="Label" Text="Pa�s Principal" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUsuario" Width="150px" runat="server" MaxLength="20" Enabled="False"
                                                        Visible="False"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblEstado" runat="server" CssClass="Label" Text="Estado:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rdbEstado" runat="server" CssClass="Label" RepeatDirection="Horizontal"
                                                        AutoPostBack="true">
                                                        <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        Filtro
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_Busqueda" runat="server">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label24" CssClass="Label" runat="server" Text="Pais:"></asp:Label>
                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtTextoABuscar" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                                                <asp:ImageButton ID="btnBuscarPersona0" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                                                    onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                                    Visible="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label25" runat="server" Text="Estado" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rdbestadoBusq" runat="server" CssClass="Label" RepeatDirection="Horizontal"
                                                                AutoPostBack="true">
                                                                <asp:ListItem Value="">Todos </asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="DGVUsuario" runat="server" AllowPaging="True" PageSize="30" AutoGenerateColumns="False"
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" />
                                                        <asp:TemplateField HeaderText="C�d. Pa�s">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hddIdPais" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPais") %>' />
                                                                <asp:Label ID="lblCodPais" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"pa_Codigo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="IdPais" HeaderText="Id" NullDisplayText="---" />--%>
                                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" NullDisplayText="---" />
                                                        <asp:TemplateField HeaderText="Pa�s Base">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkPaisBasedgv" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"pa_BaseBit") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="IdProcedencia" HeaderText="Procedencia" NullDisplayText="---" />
                                                        <asp:BoundField DataField="strEstado" HeaderText="Estado" NullDisplayText="---">
                                                            <ControlStyle Width="0px" />
                                                            <HeaderStyle Width="0px" />
                                                            <ItemStyle Width="0px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hddModo" runat="server" />
                                <asp:HiddenField ID="hddLogin" runat="server" Value="" />
                                <asp:HiddenField ID="hddClave" runat="server" Value="" />
                            </td>
                        </tr>
            </table>
             <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="capaBuscarP" style="border: 3px solid blue; padding: 10px; width: 820px;
        height: auto; position: absolute; top: 156px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanelBuscarP" runat="server">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrarClientes" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaBuscarP'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label23" runat="server" CssClass="Label" Text="Ingrese Ap. y Nombres:"></asp:Label>
                            <asp:TextBox ID="txtTextoBusqCliente" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" runat="server" Width="400px" MaxLength="100"></asp:TextBox>
                            <asp:ImageButton ID="btnBuscarPersonaCapa" runat="server" CausesValidation="false"
                                ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvBusquedaClientes" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CellPadding="4" GridLines="None" PageSize="30" Width="100%">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdPais" HeaderText="Id" NullDisplayText="---" />
                                    <asp:BoundField DataField="pa_Nombre" HeaderText="Nombre" NullDisplayText="---" />
                                    <asp:BoundField DataField="proc_Nombre" HeaderText="Procedencia" NullDisplayText="---" />
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="capaRegistrarP" style="border: 3px solid blue; padding: 10px; width: auto;
        height: auto; position: absolute; top: 229px; left: 77px; background-color: white;
        z-index: 3; display: none;">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:ImageButton ID="btnCerrar_RegPersona" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaRegistrarP'));" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" Text="Ap. Paterno:" CssClass="Label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtApPaterno_RegPersona" Width="200px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label4" runat="server" Text="Ap. Materno:" CssClass="Label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );"  ID="txtApMaterno_RegPersona" Width="200px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label8" runat="server" Text="Nombres:" CssClass="Label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtNombres_RegPersona" Width="300" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label9" runat="server" Text="D.N.I.:" CssClass="Label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDNI_RegPersona" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnGuardar_RegPersona" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                OnClientClick="return(validarRegPersona());" CausesValidation="true" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="capaRegistrarCliente" style="border: 3px solid blue; padding: 10px; width: auto;
        height: auto; position: absolute; top: 1393px; left: 100px; background-color: white;
        z-index: 3; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaRegistrarCliente'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label29" runat="server" Text="Tipo Persona:" CssClass="Label"></asp:Label>
                    <asp:DropDownList onchange="return(activarPanel());" ID="cmbTipoPersona" runat="server">
                        <asp:ListItem Value="N" Selected="True">Natural</asp:ListItem>
                        <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_PerNatural" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label30" runat="server" Text="Ap. Paterno:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtApPaterno_Cliente" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label31" runat="server" Text="Ap. Materno:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtApMaterno_Cliente" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="label12015" runat="server" Text="Nombres:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNombres_Cliente" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" Width="200px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label33" runat="server" Text="D.N.I.:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDNI_Cliente" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label34" runat="server" Text="R.U.C.:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRUC_ClienteNat" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel Enabled="false" ID="Panel_PerJuridica" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label35" runat="server" Text="Raz�n Social:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRazonSocial_Cliente" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" Width="300px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label36" runat="server" CssClass="Label" Text="R.U.C.:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRUC_ClienteJur" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label37" runat="server" Text="Tel�fono:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTelefono_Cliente" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnGuardarCliente" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        OnClientClick="return(validarSaveCliente());" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';" CausesValidation="true" />
                </td>
            </tr>
        </table>
    </div>   
    
</asp:Content>

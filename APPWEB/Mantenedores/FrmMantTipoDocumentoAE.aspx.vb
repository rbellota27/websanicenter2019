﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmMantTipoDocumentoAE
    Inherits System.Web.UI.Page

#Region "Variables"
    Private objscript As New ScriptManagerClass
    Private cbo As New Combo
    Private Enum operativo
        GuardarNuevo = 1
        Actualizar = 2
        Ninguno = 3
    End Enum
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
        End If
    End Sub

    Private Sub inicializarFrm()
        Try

            With cbo

                '************ CABECERA
                .LlenarCboPropietario(Me.cboEmpresa, False)
                .llenarCboAreaxEmpresa(Me.CboArea, CInt(Me.cboEmpresa.SelectedValue), False)
                .LlenarCboTipoDocumentoIndep(Me.CboTipoDocumento)
                .LlenarCboMoneda(Me.cboMoneda, True)

                '************* BUSQUEDA
                .LlenarCboPropietario(Me.cboBusquedaEmpresa, True)
                .LlenarCboArea(Me.CboBusquedaArea, True)

            End With

            GrillaTipoDocumentoAE()
            btnCancelar.Visible = False
            btnGuardar.Visible = False
            lbl_MensajeBusq.Visible = False
            PanelTipoDocumentoAE.Visible = False
            Panel_Busqueda.Visible = True
            DGV_TipoDocumentoAE.Visible = True
            btnFiltrar.Visible = True
            ViewState.Add("operativo", operativo.Ninguno)
            cboEmpresa.Enabled = True
            CboArea.Enabled = True
            CboTipoDocumento.Enabled = True

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'Cargar Combor Empresa
    Private Sub CargarCboPropietario(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        ' Dim nCombo As New Combo
        cbo.LlenarCboPropietario(combo, True)
    End Sub
    'Cargar el combo Área dependiendo de IdEmpresa
    Protected Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        cbo.llenarCboAreaxEmpresa(CboArea, CInt(cboEmpresa.SelectedValue))
    End Sub
    'Cargar Tipo de Documento
    Private Sub CargarCboTipoDocumento(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        cbo.LlenarComboTipoDocumento(combo)
    End Sub
    'Cargar el Combo Moneda
    Private Sub CargarCboMoneda(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        cbo.LlenarCboMoneda(combo)
    End Sub
    'Cargar Combo para la busqueda Empresa
    Private Sub CargarCboBusquedaPropietario(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        cbo.LlenarCboPropietario(combo, True)
    End Sub
    ''Cargar Combo para la busqueda Área enlazada con Busqueda de Empresa
    'Protected Sub cboBusquedaEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboBusquedaEmpresa.SelectedIndexChanged
    '    cbo.llenarCboAreaxEmpresa(CboBusquedaArea, CInt(cboBusquedaEmpresa.SelectedValue), True)
    'End Sub

    '**** Cargar a la Grilla
    Private Sub GrillaTipoDocumentoAE()
        Try
            DGV_TipoDocumentoAE.DataSource = (New Negocio.TipoDocumentoAE).BuscarTipoDocumentoxIdArea(CInt(CboBusquedaArea.SelectedValue), CInt(cboBusquedaEmpresa.SelectedValue))
            DGV_TipoDocumentoAE.DataBind()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub Clear()
        txtCantidadFilas.Text = ""
        txtMontoMaxAfectado.Text = ""
        txtMaxNoAfectado.Text = ""
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        Try
            Clear()
            ViewState.Add("operativo", operativo.GuardarNuevo)
            Me.btnNuevo.Visible = False
            btnGuardar.Visible = True
            btnCancelar.Visible = True
            PanelTipoDocumentoAE.Visible = True
            Panel_Busqueda.Enabled = False
            DGV_TipoDocumentoAE.Enabled = False
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        Clear()
        ViewState.Add("operativo", operativo.Ninguno)
        Me.btnNuevo.Visible = True
        btnGuardar.Visible = False
        btnCancelar.Visible = False
        PanelTipoDocumentoAE.Visible = False
        Panel_Busqueda.Visible = True
        DGV_TipoDocumentoAE.Visible = True
        Panel_Busqueda.Enabled = True
        DGV_TipoDocumentoAE.Enabled = True
        lbl_MensajeBusq.Visible = False
        GrillaTipoDocumentoAE()
        cboEmpresa.Enabled = True
        CboArea.Enabled = True
        CboTipoDocumento.Enabled = True
    End Sub
    '**** Boton Guardar  ---> INSERTAR TipoDocumentoAE,Empresa y Area
    '                    ---> ACTUALIZA TipoDocumentoAE,Empresa y Area
    Private Sub ValidarGuardar()
        btnGuardar.Visible = False
        btnCancelar.Visible = False
        btnNuevo.Visible = True
        Panel_Busqueda.Visible = True
        Panel_Busqueda.Enabled = True
        PanelTipoDocumentoAE.Visible = False
        PanelTipoDocumentoAE.Enabled = True
        DGV_TipoDocumentoAE.Visible = True
        DGV_TipoDocumentoAE.Enabled = True
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Try
            Select Case CInt(ViewState("operativo"))

                Case operativo.GuardarNuevo

                    Dim objTipoDocumentoAE As New Entidades.TipoDocumentoAE
                    Dim nTipoDocumentoAE As New Negocio.TipoDocumentoAE
                    '*** el combos de Empresa es referencial para llegar a Área
                    objTipoDocumentoAE.IdEmpresa = CInt(CStr(cboEmpresa.SelectedValue))
                    objTipoDocumentoAE.IdTipoDocumento = CInt(CStr(CboTipoDocumento.SelectedValue))
                    objTipoDocumentoAE.IdMoneda = CInt(CStr(cboMoneda.SelectedValue))
                    objTipoDocumentoAE.IdArea = CInt(CStr(CboArea.SelectedValue))
                    objTipoDocumentoAE.CantidadFilas = CInt(IIf(txtCantidadFilas.Text = "", 0, txtCantidadFilas.Text.Trim))
                    objTipoDocumentoAE.MontoMaximoAfecto = CDec(IIf(txtMontoMaxAfectado.Text = "", 0.0, txtMontoMaxAfectado.Text.Trim))
                    objTipoDocumentoAE.MontoMaximoNoAfecto = CDec(IIf(txtMaxNoAfectado.Text = "", 0.0, txtMaxNoAfectado.Text.Trim))
                    Dim msj As String = (New Negocio.TipoDocumentoAE).InsertaTipoDocumentoAE(objTipoDocumentoAE)
                    GrillaTipoDocumentoAE()
                    ViewState.Add("operativo", operativo.Ninguno)
                    ValidarGuardar()
                    objscript.mostrarMsjAlerta(Me, msj)
                Case operativo.Actualizar
                    Dim objTipoDocumentoAE As New Entidades.TipoDocumentoAE
                    '*** el combos de Empresa es referencial para llegar a Área
                    '*** Solo se Actualizara por Empresa, TipoDocumento y no por Área

                    objTipoDocumentoAE.IdEmpresa = CInt(CStr(cboEmpresa.SelectedValue))
                    objTipoDocumentoAE.IdArea = CInt(CStr(CboArea.SelectedValue))
                    objTipoDocumentoAE.IdTipoDocumento = CInt(CStr(CboTipoDocumento.SelectedValue))
                    objTipoDocumentoAE.IdMoneda = CInt(CStr(cboMoneda.SelectedValue))
                    objTipoDocumentoAE.CantidadFilas = CInt(IIf(txtCantidadFilas.Text = "", 0, txtCantidadFilas.Text.Trim))
                    objTipoDocumentoAE.MontoMaximoAfecto = CDec(IIf(txtMontoMaxAfectado.Text = "", 0.0, txtMontoMaxAfectado.Text.Trim))
                    objTipoDocumentoAE.MontoMaximoNoAfecto = CDec(IIf(txtMaxNoAfectado.Text = "", 0.0, txtMaxNoAfectado.Text.Trim))

                    objTipoDocumentoAE.IdEmpresa = CInt(ViewState("IdEmpresa"))
                    objTipoDocumentoAE.IdTipoDocumento = CInt(ViewState("IdTipoDocumento"))
                    objTipoDocumentoAE.IdArea = CInt(ViewState("IdArea"))

                    If (New Negocio.TipoDocumentoAE).ActualizaTipoDocumentoAE(objTipoDocumentoAE) = 0 Then
                        objscript.mostrarMsjAlerta(Me, "Problemas en la Actualización del Registro.")
                    Else
                        objscript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
                    End If
                    GrillaTipoDocumentoAE()
                    ViewState.Add("operativo", operativo.Ninguno)
                    ValidarGuardar()
            End Select
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        cboEmpresa.Enabled = True
        CboArea.Enabled = True
        CboTipoDocumento.Enabled = True
    End Sub
    '**** ELIMINAR ----> Recordar en el código del diseño (Eliminar): OnClick ="Click_Eliminar"
    Protected Sub Click_Eliminar(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lb As LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lb.NamingContainer, GridViewRow)

            HDD_IdEmpresa.Value = CType(fila.Cells(2).FindControl("HDD_TipoDocumentoAExIdEmpresa"), HiddenField).Value.Trim
            HDD_IdArea.Value = CType(fila.Cells(3).FindControl("HDD_TipoDocumentoAExIdArea"), HiddenField).Value.Trim
            HDD_IdTipoDocumento.Value = CType(fila.Cells(4).FindControl("HDD_TipoDocumentoAExIdTipoDocumento"), HiddenField).Value.Trim

            Dim msj% = CInt((New Negocio.TipoDocumentoAE).EliminaxEmpresaxAreaxTipoDocumento(CInt(HDD_IdEmpresa.Value) _
                                                                                            , CInt(HDD_IdArea.Value) _
                                                                                            , CInt(HDD_IdTipoDocumento.Value)))
            'Ver el PROCEDIMIENTO--> Cuando se Elimina por Empresa por Area por TipoDocumento y es =1 registro
            If msj = 1 Then
                objscript.mostrarMsjAlerta(Me, "La Eliminación se ejecuto con éxito.")
            Else
                objscript.mostrarMsjAlerta(Me, "Problemas en la Eliminación del Registro.")
            End If


            GrillaTipoDocumentoAE()
            ViewState.Add("operativo", operativo.Ninguno)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la ejecución.")
        End Try
    End Sub
    '**** EDITAR

    Protected Sub Click_Editar(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lb As LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lb.NamingContainer, GridViewRow)
            HDD_IdEmpresa.Value = CType(fila.Cells(2).FindControl("HDD_TipoDocumentoAExIdEmpresa"), HiddenField).Value.Trim
            HDD_IdArea.Value = CType(fila.Cells(3).FindControl("HDD_TipoDocumentoAExIdArea"), HiddenField).Value.Trim
            HDD_IdTipoDocumento.Value = CType(fila.Cells(4).FindControl("HDD_TipoDocumentoAExIdTipoDocumento"), HiddenField).Value.Trim
            ViewState("IdEmpresa") = HDD_IdEmpresa.Value
            ViewState("IdArea") = HDD_IdArea.Value
            ViewState("IdTipoDocumento") = HDD_IdTipoDocumento.Value

            'La declaracion "Fila" es igual q pongamos "dgvTipoDocumento(cbo)"
            'La declaracion "Fila" es igual q pongamos "dgvTipoDocumento.SelectedRow(Txt)"
            'Lo pongo de esta manera porq en mi grilla tengo "Eliminar" y "Editar"

            PanelTipoDocumentoAE.Visible = True
            Panel_Busqueda.Enabled = False
            DGV_TipoDocumentoAE.Enabled = False
            btnNuevo.Visible = False
            btnGuardar.Visible = True
            btnCancelar.Visible = True



            cboEmpresa.SelectedValue = Me.HDD_IdEmpresa.Value
            cboEmpresa_SelectedIndexChanged(sender, e)

            CboArea.SelectedValue = Me.HDD_IdArea.Value

            CboTipoDocumento.SelectedValue = Me.HDD_IdTipoDocumento.Value

            cboMoneda.SelectedValue = CType(fila.Cells(8).FindControl("hdd_IdMoneda"), HiddenField).Value
            txtCantidadFilas.Text = fila.Cells(5).Text.Trim
            txtMaxNoAfectado.Text = fila.Cells(7).Text.Trim
            txtMontoMaxAfectado.Text = fila.Cells(6).Text.Trim

            GrillaTipoDocumentoAE()

            ViewState.Add("operativo", operativo.Actualizar)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        cboEmpresa.Enabled = False
        CboArea.Enabled = False
        CboTipoDocumento.Enabled = False
    End Sub
    'Busqueda
    Protected Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFiltrar.Click

        GrillaTipoDocumentoAE()

    End Sub


    Private Sub DGV_TipoDocumentoAE_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_TipoDocumentoAE.PageIndexChanging

        DGV_TipoDocumentoAE.PageIndex = e.NewPageIndex
        GrillaTipoDocumentoAE()
    End Sub
End Class
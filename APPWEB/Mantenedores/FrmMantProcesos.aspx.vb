﻿Public Class FrmMantProcesos
    Inherits System.Web.UI.Page

#Region "variables"
    Dim listaProceso As List(Of Entidades.Proceso)
    Private objScript As New ScriptManagerClass
    Private cbo As New Combo



    Private Enum operativo
        Ninguno = 0
        InsertUpdate = 1
    End Enum
#End Region

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / NRO DIAS VIGENCIA / REGISTRAR CLIENTE / EDITAR CLIENTE
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {132, 133})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

    End Sub


#Region "VISTAS FORMULARIOS"

    Private Sub limpiarControles()
        txtId.Text = ""
        txtCodigo.Text = ""
        txtdescripcion.Text = ""
        TxtNombre.Text = ""
        chkActivo.Checked = False

    End Sub

#End Region

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Private Sub frm_State(ByVal modo As Integer)
        limpiarControles()

        Select Case modo
            Case operativo.Ninguno
                ddlModulo.AutoPostBack = False
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
                Panel_Procesos.Visible = False

            Case operativo.InsertUpdate
                'ddlModulo.Enabled = True
                ddlModulo.AutoPostBack = True
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                Panel_Procesos.Visible = True

        End Select
    End Sub

#Region "funciones"

    Private Sub setListaProceso(ByVal lista As List(Of Entidades.Proceso))
        Session.Remove("ProcesoConsulta")
        Session.Add("ProcesoConsulta", lista)
    End Sub
    Private Function getlistaProceso() As List(Of Entidades.Proceso)
        Return CType(Session.Item("ProcesoConsulta"), List(Of Entidades.Proceso))
    End Function

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ValidarPermisos()
            ConfigurarDatos()
            frm_State(operativo.Ninguno)
            frm_OnLoad()
        End If
    End Sub

    Private Sub frm_OnLoad()
        Try
            cbo.llenarModulo(Me.ddlModulo, True)
            getLineaByParameters()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Private Sub getLineaByParameters()
        Try
            Me.listaProceso = (New Negocio.Proceso).SelectProceso("l")
            setListaProceso(listaProceso)
            DGV_ProcesosBusqueda.DataSource = listaProceso
            DGV_ProcesosBusqueda.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        cbo.llenarModulo(Me.ddlModulo, True)
        frm_State(operativo.InsertUpdate)
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim ngcValTabla As New Negocio.TipoTabla
        If CStr(ViewState("Modo")) = "1" Then
            Dim objValTabla As Entidades.TipoTabla = ngcValTabla.ValidadNombres("Proceso", "NomProceso", Me.TxtNombre.Text.Trim)
            If objValTabla.Item >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe el nombre " + Me.TxtNombre.Text.Trim + " ingrese un nombre diferente" + "\n ó este inactivo.")
                TxtNombre.Focus()
                Exit Sub
            End If
        End If
        registrarProceso()

    End Sub

    Private Sub registrarProceso()
        Dim hecho As Boolean = False
        Try
            Dim objProceso As Entidades.Proceso = getProceso()
            Dim objNProceso As New Negocio.Proceso

            If txtId.Text = "" Then
                hecho = objNProceso.AgregarProceso(objProceso)
            Else
                hecho = objNProceso.ActualizaProceso(objProceso)
            End If
            If hecho Then
                frm_State(operativo.Ninguno)
                getLineaByParameters()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Function getProceso() As Entidades.Proceso
        Dim objProceso As New Entidades.Proceso
        With objProceso

            .id = CInt(IIf(IsNumeric(txtId.Text) = True, txtId.Text, 0))
            .codigo = txtCodigo.Text.Trim
            .Descripcion = txtdescripcion.Text.Trim
            .nombre = TxtNombre.Text.Trim
            If (chkActivo.Checked() = True) Then
                .estado = 1
            Else
                .estado = 0
            End If

            .modulo = CInt(ddlModulo.SelectedValue)

        End With
        Return objProceso
    End Function


    Private Sub cargarDatosProcesosEdicion(ByVal idproceso As Integer)

        Try
            frm_State(operativo.InsertUpdate)

            Dim objProceso As List(Of Entidades.Proceso) = (New Negocio.Proceso).SelectxIdProceso(idproceso)
            If objProceso.Count > 0 Then

                txtId.Text = CStr(objProceso.Item(0).id)
                txtCodigo.Text = objProceso.Item(0).codigo
                txtdescripcion.Text = objProceso.Item(0).Descripcion
                TxtNombre.Text = objProceso.Item(0).nombre
                If (objProceso.Item(0).estado = 1) Then
                    chkActivo.Checked = True
                Else
                    chkActivo.Checked = False
                End If

                ddlModulo.SelectedValue = CInt(objProceso.Item(0).modulo)


            End If

            Try
                txtCodigo.Enabled = True
                txtdescripcion.Enabled = True
                TxtNombre.Enabled = True
            Finally

            End Try
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub DGV_ProcesosBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_ProcesosBusqueda.SelectedIndexChanged
        cargarDatosProcesosEdicion(CInt(DGV_ProcesosBusqueda.SelectedRow.Cells(0).Text))
    End Sub

    Protected Sub DGV_ProcesosBusqueda_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles DGV_ProcesosBusqueda.RowDeleting
        Dim proceso As String = DGV_ProcesosBusqueda.DataKeys(e.RowIndex).Value.ToString
        cargarDatosProcesoEliminar(CInt(proceso))
    End Sub

    Private Sub cargarDatosProcesoEliminar(ByVal idProceso As Integer)

        Try
            frm_State(operativo.Ninguno)
            idProceso = (New Negocio.Proceso).EliminarProceso(idProceso)
            getLineaByParameters()
            objScript.mostrarMsjAlerta(Me, "Se elimino el registro")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub DGV_ProcesosBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_ProcesosBusqueda.PageIndexChanging
        DGV_ProcesosBusqueda.PageIndex = e.NewPageIndex
        DGV_ProcesosBusqueda.DataSource = getlistaProceso()
        DGV_ProcesosBusqueda.DataBind()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        frm_State(operativo.Ninguno)

    End Sub
End Class
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantPost.aspx.vb" Inherits="APPWEB.FrmMantPost" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">
        <table width="100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                    onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                    OnClientClick="return(validarSave());" CausesValidation="true" />
                                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                                    OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    P.O.S.
                </td>
            </tr>
            <tr>
                <td align="center">
                    <br />
                    <fieldset class="FieldSetPanel">
                        <legend>
                            <asp:Label ID="lblModo" runat="server" Text="B�squeda"></asp:Label></legend>
                        <table cellpadding="2" style="width: 100%;">
                            <tr>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblBanco" runat="server" Text="Banco:"></asp:Label>
                                </td>
                                <td style="text-align: left;">
                                    <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblCuentaBancaria" runat="server" Text="Cuenta Bancaria:"></asp:Label>
                                </td>
                                <td style="text-align: left;">
                                    <asp:UpdatePanel ID="upCuentaBancaria" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="cboCuentaBancaria" runat="server">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="cboBanco" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblEstado" runat="server" Text="Estado:"></asp:Label>
                                </td>
                                <td style="text-align: left;">
                                    <asp:RadioButtonList ID="rbtlEstado" runat="server" RepeatDirection="Horizontal "
                                        CssClass="Label_fsp">
                                        <asp:ListItem Text="Todos" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Activo" Selected="True" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label_fsp">
                                    <asp:Label ID="lbldescripcion" runat="server" Text="Descripci�n:"></asp:Label>
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );"  ID="txtDescripcion" runat="server" Width="100%"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" onmouseout="this.src='/Imagenes/Buscar_B.JPG';" />
                                </td>
                            </tr>
                            <tr>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblIdentificador" runat="server" Text="Identificador:"></asp:Label>
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtIdentificador" runat="server" Width="100%"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel ID="Panel_DatosAdicionales" runat="server">
                        <br />
                            <fieldset class="FieldSetPanel">
                                <legend>
                                    <asp:Label ID="lblDatosAdicionales" runat="server" Text="Datos Adicionales"></asp:Label></legend>
                                  <table style ="width:100%">
                                  <tr>
                                  <td style ="width:50%">
                                  <asp:UpdatePanel ID="upTipoTarjeta" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                  <fieldset class="FieldSetPanel">
                                <legend>
                                    <asp:Label ID="fslblTipoTarjeta" runat="server" Text="Tipos de Tarjeta"></asp:Label></legend>
                                <table cellpadding="2" style="width: 100%;">
                                    <tr>
                                        <td class="Label_fsp">
                                            <asp:Label ID="lblTipoTarjeta" runat="server" Text="Tipo de Tarjeta:"></asp:Label>
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:DropDownList ID="cboTipoTarjeta" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAgregarTipoTarjeta" runat="server" ImageUrl="~/Imagenes/Agregar_b.JPG" onmouseover="this.src='/Imagenes/Agregar_A.JPG';" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:UpdatePanel ID="upGrillaTipoTarjeta" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvTipoTarjeta" runat="server" AutoGenerateColumns="False" CellPadding="2" HorizontalAlign="Justify" EnableViewState="True" style="width:100%">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbtnQuitarTipoTarjeta" runat="server" OnClick="lbtnQuitarTipoTarjeta_Click">Quitar</asp:LinkButton>
                                                                    <asp:HiddenField ID="hddIdTipoTarjeta" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>'
                                                                        runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Nombre" HeaderText="Tipo de Tarjeta" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                </fieldset>
                                </ContentTemplate>                        
                    </asp:UpdatePanel>
                                  </td>
                                  <td>
                                  </td>
                                  </tr>
                                  </table>
                                   
                            </fieldset></asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <br />
                    <asp:UpdateProgress ID="upGrilla_Progress" runat="server" DisplayAfter="1000" AssociatedUpdatePanelID="upGrilla">
                        <ProgressTemplate>
                            <asp:Image ID="ImgProgress" runat="server" ImageUrl="~/Imagenes/ajax.gif" /></ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upGrilla" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="gvDatos" runat="server" AutoGenerateColumns="False" CellPadding="2"
                                HorizontalAlign="Justify" EnableViewState="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEditar" runat="server" OnClick="lbtnEditar_Click">Editar</asp:LinkButton>
                                            <asp:HiddenField ID="hddId" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdBanco" Value='<%# DataBinder.Eval(Container.DataItem,"IdBanco") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdCuentaBancaria" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdEstado" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Eliminar</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                    <asp:BoundField DataField="Banco" HeaderText="Banco" />
                                    <asp:BoundField DataField="CuentaBancaria" HeaderText="Cta. Bancaria" />
                                    <asp:BoundField DataField="Persona" HeaderText="Propietario Cta." />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n P.O.S." NullDisplayText="" />
                                    <asp:BoundField DataField="Identificador" HeaderText="Identificador" NullDisplayText="" />
                                    <asp:CheckBoxField DataField="Estado" HeaderText="Activo" />
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
<asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />

     <script language="javascript" type="text/javascript">
        function validarSave() {
            var Banco = document.getElementById('<%=cboBanco.ClientID%>');
            var idBanco = Banco.value;
            if (idBanco == 0) {
                alert('Seleccione un Banco.');

                return false;
            }
            var CuentaBancaria = document.getElementById('<%=cboCuentaBancaria.ClientID%>');
            var idCuentaBancaria = CuentaBancaria.value;
            if (idCuentaBancaria == 0) {
                alert('Seleccione una Cuenta Bancaria.');

                return false;
            }

            var descripcion = document.getElementById('<%=txtdescripcion.ClientID%>');
            if (descripcion.value.length == 0) {
                alert('Debe Ingresar una Descripci�n.');
                descripcion.select();
                descripcion.focus();
                return false;
            }

            var identificador = document.getElementById('<%=txtIdentificador.ClientID%>');
            if (identificador.value.length == 0) {
                alert('Debe Ingresar un Identificador.');
                identificador.select();
                identificador.focus();
                return false;
            }

            return (confirm('Desea Continuar Con la Operacion'));
        }
    </script>
    
    <script language="javascript" type="text/javascript">
            //////////////////////////////////////////
            var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
            /////////////////////////////////////////
    </script>


</asp:Content>

﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmPersona

    '''<summary>
    '''Control btnNuevo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnNuevo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnCancelar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCancelar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control rbPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rbPersona As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control pnlPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlPersona As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbcodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbcodigo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbfechaAlta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbfechaAlta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ckpropietario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ckpropietario As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''Control dlnacionalidad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlnacionalidad As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbfechaBaja.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbfechaBaja As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control cefechabaja.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cefechabaja As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control rbestado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rbestado As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control tbwebSite.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbwebSite As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control dltipoPV.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dltipoPV As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control dlMotivoBaja.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlMotivoBaja As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbnombreComercial.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbnombreComercial As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control dlTipoAgente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlTipoAgente As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control btn_AddTipoAgente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_AddTipoAgente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control gv_tipoAgente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gv_tipoAgente As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control gvRol.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvRol As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btnagregarrol.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnagregarrol As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control pnlJuridica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlJuridica As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbrazonSocial.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbrazonSocial As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbrucjuridco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbrucjuridco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control dlgiro.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlgiro As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbrepresentanteLegal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbrepresentanteLegal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarContacto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarContacto As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbnrodocRepresentanteLegal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbnrodocRepresentanteLegal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnbuscarRepresentante.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnbuscarRepresentante As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnlimpiarRepresentanteLegal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnlimpiarRepresentanteLegal As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control gvContacto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvContacto As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control pnlNatural.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlNatural As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbpaterno.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbpaterno As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbmaterno.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbmaterno As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbnombres.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbnombres As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control cboSexo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboSexo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control dlEstadoCivil.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlEstadoCivil As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control txtFechaNac.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaNac As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control CalendarExtender_txtFechaNac.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender_txtFechaNac As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control dlCargo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlCargo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control dlCondicionTrabajo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlCondicionTrabajo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbCentroLaboral.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbCentroLaboral As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbEmpresaContacto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbEmpresaContacto As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbCentroLaboralRuc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbCentroLaboralRuc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarCentroLaboral.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarCentroLaboral As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnlimpiarCentroLaboral.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnlimpiarCentroLaboral As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbRucEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbRucEmpresa As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnbuscarEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnbuscarEmpresa As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnlimpiarContactoEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnlimpiarContactoEmpresa As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tr_empleado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_empleado As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''Control ckEmpleado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ckEmpleado As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''Control dlEmpCategoria.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlEmpCategoria As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbEmpFechaAlta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbEmpFechaAlta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control rbEmpEstado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rbEmpEstado As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control dlEmpMotivoBaja.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlEmpMotivoBaja As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbEmpFechaBaja.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbEmpFechaBaja As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ceFechaEmp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ceFechaEmp As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control tr_chofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_chofer As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''Control ckChofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ckChofer As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''Control tbnroLicencia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbnroLicencia As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbcategoria.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbcategoria As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control gvTipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvTipoDocumento As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btnagregarTipoDoc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnagregarTipoDoc As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control pnlProfesion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlProfesion As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control btnagregarProfesion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnagregarProfesion As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnagregarOcupacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnagregarOcupacion As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control gvProfesion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvProfesion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control gvOcupacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvOcupacion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control pnlTelefono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlTelefono As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control btnagregarTelefono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnagregarTelefono As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnagregarCorreo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnagregarCorreo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control gvTelefono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvTelefono As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control gvCorreo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvCorreo As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control dlTipoDireccion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlTipoDireccion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control dlDepartamento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlDepartamento As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control dlProvincia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlProvincia As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control dlDistrito.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlDistrito As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control dlTipoVia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlTipoVia As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbVia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbVia As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbnroExterior.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbnroExterior As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbnroInterior.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbnroInterior As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control dlZonificacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlZonificacion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbzona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbzona As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbEtapa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbEtapa As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbManzana.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbManzana As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbLote.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbLote As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbDireccion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbDireccion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbreferencia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbreferencia As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnagregarDireccion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnagregarDireccion As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control gvDireccion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvDireccion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control pnlBusquedaPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlBusquedaPersona As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control Panel1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbRazonApe.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbRazonApe As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control rbbuscarEstado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rbbuscarEstado As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control Panel2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel2 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbbuscarDni.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarDni As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel3 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbbuscarRuc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarRuc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control dlRol.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlRol As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control ddlNacionalidadBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlNacionalidadBuscar As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control gvBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvBuscar As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btAnterior.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btAnterior As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btSiguiente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btSiguiente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndex As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btIr.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btIr As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndexGO.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndexGO As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control hddIdPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdPersona As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddidpersonaR.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidpersonaR As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddidCentroLaboral.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidCentroLaboral As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddidPersonaE.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidPersonaE As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddChoferReg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddChoferReg As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddEsempleado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddEsempleado As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddConfigurarDatos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddConfigurarDatos As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddoperativo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddoperativo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hdd_idrolurl.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdd_idrolurl As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdTipoPv.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdTipoPv As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control btcerrarPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btcerrarPersona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control cbbuscarpersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cbbuscarpersona As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbbuscarPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarPersona As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarPersona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control gvBuscarPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvBuscarPersona As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btAnterior_Persona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btAnterior_Persona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btSiguiente_Persona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btSiguiente_Persona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndex_Persona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndex_Persona As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btIr_Persona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btIr_Persona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndexGO_Persona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndexGO_Persona As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control hddtipoBusqueda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddtipoBusqueda As Global.System.Web.UI.WebControls.HiddenField
End Class

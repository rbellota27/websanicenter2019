<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantUsuario.aspx.vb" Inherits="APPWEB.FrmMantUsuario" Title="Mantenimiento de Usuarios"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
        function valAddPerfil() {
            var cboPerfil = document.getElementById('<%=cmbPerfil_Usuario.ClientID %>');
            var grilla = document.getElementById('<%=DGV_PerfilUsuario.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(cboPerfil.value)) {
                        alert('El Perfil seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valAddArea() {
            var cboArea = document.getElementById('<%=cmbArea_Usuario.ClientID %>');
            var grilla = document.getElementById('<%=DGV_Usuario_Area.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].cells[0].children[0].value) == parseInt(cboArea.value)) {
                        alert('El Area seleccionado ya ha sido ingresada.');
                        return false;
                    }
                }
            }
            return true;
        }

        function registrarP() {
            offCapa('capaBuscarP');
            onCapa('capaRegistrarP');
        }
        function validarRegPersona() {
            var caja = document.getElementById('<%=txtApPaterno_RegPersona.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('El campo <Ap. Paterno> es un campo requerido.');
                caja.focus();
                return false;
            }
            caja = document.getElementById('<%=txtNombres_RegPersona.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('El campo <Nombres> es un campo requerido.');
                caja.focus();
                return false;
            }
            return true;
        }
        function validarCajaBlanco() {
            var caja = document.getElementById('<%=txtUsuario.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('Ingrese un Login');
                caja.select();
                caja.focus();
                return false;
            } else {
                return true;
            }
        }
        function validarId() {
            var caja = document.getElementById('<%=txtCodigo.ClientID %>');
            if (caja.value.length == 0) {
                alert('Ingrese D.N.I.');
                caja.select();
                caja.focus();
                return false;
            }
            return true;
        }
        function validarSave() {
            var caja = document.getElementById('<%=txtCodigoPersona.ClientID %>');
            if (caja.value.length == 0) {
                alert('Debe seleccionar una Persona a la cual crear un Usuario.');
                var cajaCod = document.getElementById('<%=txtCodigoPersona.ClientID %>');
                cajaCod.select();
                cajaCod.focus();
                return false;
            }
            caja = document.getElementById('<%=txtUsuario.ClientID %>');
            if (caja.value.length == 0) {
                alert('Debe ingresar un Login.');
                caja.select();
                caja.focus();
                return false;
            }
            var rdbEstado = document.getElementById('<%=rdbEstado.ClientID %>');
            var hddModo = document.getElementById('<%=hddModo.ClientID %>');
            var chbUpdatePassword = document.getElementById('<%=chbUpdatePassword.ClientID %>');

            if (chbUpdatePassword.status) {
                //********** Validamos el Password como Campo Requerido
                caja = document.getElementById('<%=txtClave.ClientID %>');
                if (caja.value.length == 0) {
                    alert('Debe ingresar un Password.');
                    caja.select();
                    caja.focus();
                    return false;
                }
                caja = document.getElementById('<%=txtConfirmarPassword.ClientID %>');
                if (caja.value.length == 0) {
                    alert('Debe confirmar su Password.');
                    caja.select();
                    caja.focus();
                    return false;
                }
                var caja1 = document.getElementById('<%=txtClave.ClientID %>');
                if (caja.value != caja1.value) {
                    alert('El Password ingresado no coincide');
                    caja.select();
                    caja.focus();
                    return false;
                }
            }
            return (confirm('Desea Continuar con la operaci�n'));
        }
    
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='../Imagenes/Nuevo_A.JPG';"
                            onmouseout="this.src='../Imagenes/Nuevo_b.JPG';" />
                        <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                            onmouseover="this.src='../Imagenes/Guardar_A.JPG';" onmouseout="this.src='../Imagenes/Guardar_B.JPG';"
                            OnClientClick="return(validarSave());" CausesValidation="true" />
                        <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                            onmouseover="this.src='../Imagenes/Cancelar_A.JPG';" onmouseout="this.src='../Imagenes/Cancelar_B.JPG';"
                            OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Usuario" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td class="TituloCelda">
                                        USUARIO
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="Texto">
                                                    Fecha de Alta:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td class="Texto" style="width: 150px">
                                                    Fecha de Baja:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFechaBaja" runat="server" Text="" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCeldaLeft">
                                        <asp:Image ID="imgPerfil" runat="server" />&nbsp;Perfil
                                        <cc1:CollapsiblePanelExtender ID="cpePerfil" runat="server" TargetControlID="Panel_Perfil"
                                            ImageControlID="imgPerfil" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                            Collapsed="false" CollapseControlID="imgPerfil" ExpandControlID="imgPerfil">
                                        </cc1:CollapsiblePanelExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel_Perfil" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label28" runat="server" CssClass="Label" Text="Perfil:"></asp:Label>
                                                        <asp:DropDownList ID="cmbPerfil_Usuario" runat="server" DataTextField="NomPerfil"
                                                            DataValueField="IdPerfil">
                                                        </asp:DropDownList>
                                                        <asp:ImageButton ID="btnAgregarTAP" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                            OnClientClick="return(valAddPerfil());" onmouseout="this.src='../Imagenes/Agregar_B.JPG';"
                                                            onmouseover="this.src='../Imagenes/Agregar_A.JPG';" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="DGV_PerfilUsuario" runat="server" AutoGenerateColumns="false" Width="500px">
                                                            <Columns>
                                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ItemStyle-HorizontalAlign="Center"
                                                                    ShowSelectButton="true" />
                                                                <asp:TemplateField HeaderText="Perfil" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px">
                                                                    <ItemTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:HiddenField ID="hddIdPerfil" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPerfil")%>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblPerfil" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomPerfil")%>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Activo" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbEstado_UsuarioPerfil" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                            <FooterStyle CssClass="GrillaFooter" />
                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                            <PagerStyle CssClass="GrillaPager" />
                                                            <RowStyle CssClass="GrillaRow" />
                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="TituloCeldaLeft">
                                        <asp:Image ID="ImgArea" runat="server" />&nbsp;Area
                                        <cc1:CollapsiblePanelExtender ID="cpeArea" runat="server" TargetControlID="Panel_Area"
                                            ImageControlID="ImgArea" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                            Collapsed="false" CollapseControlID="ImgArea" ExpandControlID="ImgArea">
                                        </cc1:CollapsiblePanelExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel_Area" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label10" runat="server" CssClass="Label" Text="Area:"></asp:Label>
                                                        <asp:DropDownList ID="cmbArea_Usuario" runat="server" DataTextField="Nombre" DataValueField="Id">
                                                        </asp:DropDownList>
                                                        <asp:ImageButton ID="btnAgregarTAA" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                            OnClientClick="return(valAddArea());" onmouseout="this.src='../Imagenes/Agregar_B.JPG';"
                                                            onmouseover="this.src='../Imagenes/Agregar_A.JPG';" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="DGV_Usuario_Area" runat="server" AutoGenerateColumns="false" Width="500px">
                                                            <Columns>
                                                                <asp:CommandField ButtonType="Link" SelectText="Quitar" ItemStyle-HorizontalAlign="Center"
                                                                    ShowSelectButton="true" />
                                                                <asp:TemplateField HeaderText="Area" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px">
                                                                    <ItemTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:HiddenField ID="hddIdArea" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdArea")%>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblArea" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NombreArea")%>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Activo" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbEstado_UsuarioArea" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                            <FooterStyle CssClass="GrillaFooter" />
                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                            <PagerStyle CssClass="GrillaPager" />
                                                            <RowStyle CssClass="GrillaRow" />
                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Ap. y Nombres/Raz�n Social:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ReadOnly="true" CssClass="TextBoxReadOnly" ID="txtPersona" runat="server"
                                                        Width="420px"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodigoPersona" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                        Width="83px"></asp:TextBox>
                                                    <asp:ImageButton ID="btnBuscarPersona" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                                                        onmouseout="this.src='../Imagenes/Buscar_B.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';"
                                                        Visible="True" OnClientClick="return(onCapa('capaBuscarP'));" />
                                                    <asp:Label ID="Label2" Visible="true" runat="server" CssClass="Label" Text="D.N.I.:"></asp:Label>
                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarCosPersona">                                                    
                                                    <asp:TextBox ID="txtCodigo" runat="server" Width="100px" Visible="True" MaxLength="20"></asp:TextBox>
                                                    </asp:Panel>
                                                    <asp:ImageButton ID="btnBuscarCosPersona" runat="server" ToolTip="Buscar Persona por D.N.I."
                                                        OnClientClick="return(validarId());" ImageUrl="~/Imagenes/Ok_b.bmp" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblDNI" runat="server" CssClass="Label" Text="D.N.I.:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ReadOnly="true" CssClass="TextBoxReadOnly" ID="txtDNI" Width="170px"
                                                        runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Login:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUsuario" Width="150px" runat="server" MaxLength="20"></asp:TextBox>
                                                    <asp:ImageButton ID="btnValidarLogin" runat="server" ToolTip="Haga clic para validar Login"
                                                        OnClientClick="return(validarCajaBlanco());" ImageUrl="~/Imagenes/Ok_b.bmp" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label6" runat="server" CssClass="Label" Text="Password:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtClave" Width="150px" runat="server" MaxLength="15" TextMode="Password"></asp:TextBox>
                                                    <asp:CheckBox ID="chbUpdatePassword" CssClass="LabelRojo" runat="server" Text="Cambiar Password" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label7" runat="server" CssClass="Label" Text="Confirmar Password:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtConfirmarPassword" Width="150px" runat="server" TextMode="Password"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblPasswordActual_Text" runat="server" CssClass="LabelRojo" Text="Password Actual:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPasswordActual" runat="server" CssClass="LabelRojo" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblEstado" runat="server" CssClass="Label" Text="Estado:"></asp:Label>
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButtonList ID="rdbEstado" runat="server" CssClass="Label" RepeatDirection="Horizontal"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblMotivoBaja" runat="server" CssClass="Label" Text="Motivo de Baja:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbMotivoBaja" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        Filtro
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_Busqueda" runat="server">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label24" CssClass="Label" runat="server" Text="Ap. y Nombres:"></asp:Label>
                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersona0">                                                
                                                <asp:TextBox ID="txtTextoABuscar" Width="300px" MaxLength="100" runat="server"></asp:TextBox>
                                                </asp:Panel>
                                                <asp:ImageButton ID="btnBuscarPersona0" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                                                    onmouseout="this.src='../Imagenes/Buscar_B.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';"
                                                    Visible="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label25" runat="server" Text="Estado" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rdbestadoBusq" runat="server" CssClass="Label" RepeatDirection="Horizontal"
                                                                AutoPostBack="true">
                                                                <asp:ListItem Value="">Todos </asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="DGVUsuario" runat="server" AllowPaging="True" PageSize="30" AutoGenerateColumns="False"
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" />
                                                        <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="---" />
                                                        <asp:BoundField DataField="Nombre" HeaderText="Ap. y Nombres" NullDisplayText="---" />
                                                        <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" />
                                                        <asp:BoundField DataField="Login" HeaderText="Login" NullDisplayText="---" />
                                                        <asp:BoundField DataField="DescFechaAlta" HeaderText="Fecha Alta" NullDisplayText="---" />
                                                        <asp:BoundField DataField="DescFechaBaja" HeaderText="Fecha Baja" NullDisplayText="---" />
                                                        <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="---" />
                                                        <asp:BoundField ControlStyle-Width="0px" DataField="IdMotivoBaja" HeaderStyle-Width="0px"
                                                            HeaderText="" ItemStyle-Width="0px" NullDisplayText="---" Visible="true" />
                                                        <asp:BoundField ControlStyle-Width="0px" DataField="Estado" HeaderStyle-Width="0px"
                                                            HeaderText="" ItemStyle-Width="0px" NullDisplayText="---" Visible="true" />
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hddModo" runat="server" />
                                <asp:HiddenField ID="hddLogin" runat="server" Value="" />
                                <asp:HiddenField ID="hddClave" runat="server" Value="" />
                            </td>
                        </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="capaBuscarP" style="border: 3px solid blue; padding: 10px; width: 820px;
        height: auto; position: absolute; top: 156px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanelBuscarP" runat="server">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrarClientes" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaBuscarP'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label23" runat="server" CssClass="Label" Text="Ingrese Ap. y Nombres:"></asp:Label>
                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersonaCapa">                            
                            <asp:TextBox ID="txtTextoBusqCliente" runat="server" Width="400px" MaxLength="100"></asp:TextBox>
                            </asp:Panel>
                            <asp:ImageButton ID="btnBuscarPersonaCapa" runat="server" CausesValidation="false"
                                ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvBusquedaClientes" runat="server" AllowPaging="True" PageSize="30"
                                AutoGenerateColumns="False" CellPadding="4" GridLines="None" Width="100%">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="---" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" NullDisplayText="---" />
                                    <asp:BoundField DataField="DescFechaNac" HeaderText="Fecha Nacimiento" NullDisplayText="---" />
                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" />
                                    <asp:BoundField DataField="NombreCargo" HeaderText="Cargo" NullDisplayText="---" />
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="capaRegistrarP" style="border: 3px solid blue; padding: 10px; width: auto;
        height: auto; position: absolute; top: 229px; left: 77px; background-color: white;
        z-index: 3; display: none;">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:ImageButton ID="btnCerrar_RegPersona" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaRegistrarP'));" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" Text="Ap. Paterno:" CssClass="Label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtApPaterno_RegPersona" Width="200px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label4" runat="server" Text="Ap. Materno:" CssClass="Label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtApMaterno_RegPersona" Width="200px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label8" runat="server" Text="Nombres:" CssClass="Label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNombres_RegPersona" Width="300" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label9" runat="server" Text="D.N.I.:" CssClass="Label"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDNI_RegPersona" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnGuardar_RegPersona" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                onmouseover="this.src='../Imagenes/Guardar_A.JPG';" onmouseout="this.src='../Imagenes/Guardar_B.JPG';"
                                OnClientClick="return(validarRegPersona());" CausesValidation="true" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="capaRegistrarCliente" style="border: 3px solid blue; padding: 10px; width: auto;
        height: auto; position: absolute; top: 1393px; left: 100px; background-color: white;
        z-index: 3; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaRegistrarCliente'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label29" runat="server" Text="Tipo Persona:" CssClass="Label"></asp:Label>
                    <asp:DropDownList onchange="return(activarPanel());" ID="cmbTipoPersona" runat="server">
                        <asp:ListItem Value="N" Selected="True">Natural</asp:ListItem>
                        <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_PerNatural" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label30" runat="server" Text="Ap. Paterno:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtApPaterno_Cliente" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label31" runat="server" Text="Ap. Materno:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtApMaterno_Cliente" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="label12015" runat="server" Text="Nombres:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNombres_Cliente" Width="200px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label33" runat="server" Text="D.N.I.:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDNI_Cliente" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label34" runat="server" Text="R.U.C.:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRUC_ClienteNat" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel Enabled="false" ID="Panel_PerJuridica" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label35" runat="server" Text="Raz�n Social:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRazonSocial_Cliente" Width="300px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label36" runat="server" CssClass="Label" Text="R.U.C.:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRUC_ClienteJur" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label37" runat="server" Text="Tel�fono:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTelefono_Cliente" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnGuardarCliente" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        OnClientClick="return(validarSaveCliente());" onmouseover="this.src='../Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='../Imagenes/Guardar_B.JPG';" CausesValidation="true" />
                </td>
            </tr>
        </table>
    </div>
    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
    </script>
</asp:Content>

﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMantControlProc.aspx.vb" Inherits="APPWEB.frmMantControlProc"  MasterPageFile="~/Principal.Master"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="MantenimientoControl" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
       <tr>
            <td>
                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseout="this.src='/Imagenes/Nuevo_b.JPG';"
                    onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" />

                <asp:ImageButton ID="btnActualizar" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                    onmouseout="this.src='/Imagenes/Editar_B.JPG';" onmouseover="this.src='/Imagenes/Editar_B.JPG';"
                     />
              
               <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                    onmouseout="this.src='/Imagenes/Guardar_B.JPG';" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                     />
              

                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                    onmouseout="this.src='/Imagenes/Arriba_B.JPG';" onmouseover="this.src='/Imagenes/Arriba_A.JPG';"
                    ToolTip="Regresar a Administración Sistema" />
            </td>
       </tr>

       <tr>
            <td class="TituloCelda" >
                Control Procesos
            </td>
       </tr>      

       <tr>
       <td>
       <asp:Panel  ID="Panel_control_Procesos" runat="server" Width="100%">
       <table cellpadding="0" cellspacing="3" width="80%">
       <tr>
              <td class="Texto" style="width: 65px">Proceso:</td>
              <td><asp:DropDownList ID="ddlProceso" runat="server"></asp:DropDownList></td>
       </tr>

              <tr>
              <td class="Texto" style="width: 65px">Proceso 1:</td>
              <td><asp:DropDownList ID="ddlProceso1" runat="server"></asp:DropDownList></td>
       </tr>

              <tr>
              <td class="Texto" style="width: 65px">Proceso 2:</td>
              <td><asp:DropDownList ID="ddlProceso2" runat="server"></asp:DropDownList></td>
       </tr>
         <tr>
                         <td colspan=2></td>
                        </tr>
       </table>
       </asp:Panel>
        </td>
       </tr>
       <tr>
       <td>
       <asp:GridView ID="DGV_Control_Proceso" Width="27%" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="1" EmptyDataText="No existe información."
                    ForeColor="#333333" GridLines="None"  DatakeyNames="Proceso">
                     <RowStyle BackColor="#F7F6F3" CssClass="GrillaRow" ForeColor="#333333" />

                      <Columns>
                    
                        <asp:BoundField DataField="Proceso" HeaderText="Proceso" NullDisplayText="0" >
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Proceso1" HeaderText="Proceso1" NullDisplayText="-----" />                      
                        <asp:BoundField DataField="Proceso2" HeaderText="Proceso2" NullDisplayText="-----" />
                       
                        <asp:CommandField  SelectText="Editar" ShowSelectButton="True" />                                               
                        <asp:ButtonField CommandName="Delete" HeaderText="Eliminar" ShowHeader="True" Text="Eliminar" />
                       
                    </Columns>

                    <FooterStyle BackColor="#5D7B9D" CssClass="GrillaFooter" Font-Bold="True" ForeColor="White" />                    
                    <SelectedRowStyle BackColor="#E2DED6" CssClass="GrillaSelectedRow" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" CssClass="GrillaHeader" Font-Bold="True" ForeColor="White" />                    
                    <AlternatingRowStyle BackColor="White" CssClass="GrillaRowAlternating" ForeColor="#284775" />
       </asp:GridView>
        </td>
    </tr>

          <tr>
            <td style="width: 34px">
            
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>

</table>

</asp:Content>

﻿Imports System.Web
Imports System.Web.Services
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports System.Collections.Generic
Imports System.Linq
Imports DAO

Public Class Autocomplete
    Implements System.Web.IHttpHandler
    Dim objConexion As New Conexion

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim nombre As String = context.Request("nombre")
        Dim ListUsuarios As New List(Of String)
        Dim cn As New SqlConnection()
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_NOMBRE_PERSONA", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@nombre", nombre)
        cn.Open()
        Dim lector As SqlDataReader
        lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
        While (lector.Read())

            ListUsuarios.Add(lector("Nombre").ToString())

        End While

            Dim js As New JavaScriptSerializer()
            context.Response.Write(js.Serialize(ListUsuarios))
       



    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Option Strict Off
Partial Public Class FrmMantVolumenVenta
    Inherits System.Web.UI.Page

    Private objCbo As New Combo
    Private EntVolVenta As New Entidades.VolumenVenta
    Private EntVolVenta_Anterior As New Entidades.VolumenVenta
    Private NegVolVenta As New Negocio.VolumenVenta
    Private objScript As New ScriptManagerClass
    'Private ListVolVenta As New List(Of Entidades.Producto_VolVenta)
    Private IdLinea, IdSubLinea, IdTPV, IdTienda, IdTipoExistencia As Integer
    Private Const _IDEMPRESA As Integer = 1 'Sanicenter
    Private List_Producto_VolVenta As List(Of Entidades.Producto_VolVenta)

    'Private Property IdTienda() As Integer
    '    Get
    '        Return CInt(ViewState("IdTienda"))
    '    End Get
    '    Set(ByVal value As Integer)
    '        ViewState.Remove("IdTienda")
    '        ViewState.Add("IdTienda", value)
    '    End Set
    'End Property

    Private Property ListVolVenta() As List(Of Entidades.Producto_VolVenta)
        Get
            Return CType(Session("ListVolVenta"), List(Of Entidades.Producto_VolVenta))
        End Get
        Set(ByVal value As List(Of Entidades.Producto_VolVenta))
            Session.Remove("ListVolVenta")
            Session.Add("ListVolVenta", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFRM()
        End If
    End Sub

    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try
            cargarControles()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarControles()
        objCbo.llenarCboTipoExistencia(cmbTipoExistencia)
        objCbo.llenarCboLineaxTipoExistencia(Me.cmbLinea, CInt(cmbTipoExistencia.SelectedValue), True)
        objCbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), CInt(cmbTipoExistencia.SelectedValue), True)
        objCbo.LlenarCboTipoPV1(Me.cmbTipoPrecioVenta, False)
        objCbo.llenarCboTiendaxIdEmpresa(Me.cmbTienda, _IDEMPRESA, False)
    End Sub

    Private Sub actualizar_atributos()
        Try
            IdTipoExistencia = CInt(cmbTipoExistencia.SelectedValue)
            IdLinea = CInt(Me.cmbLinea.SelectedValue)
            If (Not Me.cmbSubLinea.SelectedValue Is Nothing) And CStr(Me.cmbSubLinea.SelectedValue) <> "" Then
                IdSubLinea = CInt(Me.cmbSubLinea.SelectedValue)
            Else
                IdSubLinea = 0
            End If
            IdTPV = CInt(Me.cmbTipoPrecioVenta.SelectedValue)
            IdTienda = CInt(Me.cmbTienda.SelectedValue)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub actualizarProductoVolVenta()
        List_Producto_VolVenta = ListVolVenta

        For i As Integer = 0 To List_Producto_VolVenta.Count - 1
            List_Producto_VolVenta(i).vven_CantidadMin_UMPr = CDec(CType(gvProductoVolVenta.Rows(i).FindControl("txtcantidad"), TextBox).Text)
        Next

        ListVolVenta = List_Producto_VolVenta
    End Sub

    Protected Sub cargar_listview()
        actualizar_atributos()
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        List_Producto_VolVenta = NegVolVenta.SelectxLineaxSubLineaxTipoPVxTienda(IdLinea, IdSubLinea, IdTPV, IdTienda, IdTipoExistencia, tableTipoTabla)
        ListVolVenta = List_Producto_VolVenta

        gvProductoVolVenta.DataSource = ListVolVenta
        gvProductoVolVenta.DataBind()
    End Sub

    Protected Function FiltrarListaxIDLineaxIdSubLineaxIdTPVxIdTienda(ByVal x As Entidades.Producto_VolVenta) As Boolean
        If (x.IdLinea = IdLinea Or IdLinea = 0) _
        And (x.IdSubLinea = IdSubLinea Or IdSubLinea = 0) _
        And (x.IdTipoPV = IdTPV Or IdTPV = 0) _
        And (x.IdTienda = IdTienda Or IdTienda = 0) Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub cmbTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTienda.SelectedIndexChanged
        If CInt(cmbTipoExistencia.SelectedValue) <> 0 And _
           CInt(cmbLinea.SelectedValue) <> 0 And _
           CInt(cmbSubLinea.SelectedValue) <> 0 Then
            cargar_listview()
        End If

    End Sub
    Protected Sub cmbTipoPrecioVenta_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTipoPrecioVenta.SelectedIndexChanged
        If CInt(cmbTipoExistencia.SelectedValue) <> 0 And _
         CInt(cmbLinea.SelectedValue) <> 0 And _
         CInt(cmbSubLinea.SelectedValue) <> 0 Then
            cargar_listview()
        End If
    End Sub
    Private Sub cmbTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTipoExistencia.SelectedIndexChanged
        objCbo.llenarCboLineaxTipoExistencia(Me.cmbLinea, CInt(cmbTipoExistencia.SelectedValue), True)
        objCbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), CInt(cmbTipoExistencia.SelectedValue), True)
    End Sub
    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        objCbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), CInt(cmbTipoExistencia.SelectedValue), True)
    End Sub
    Protected Sub cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea.SelectedIndexChanged
        objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea.SelectedValue), False)
        GV_FiltroTipoTabla.DataBind()
        cargar_listview()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        cargar_listview()
    End Sub

#Region "Filtro de busquedad avanza de productos"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0

            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea.SelectedValue), CInt(Me.cmbSubLinea.SelectedValue), CInt(cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1
            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)
        Next

        Return dt
    End Function

    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

#End Region

  
   
    Private Sub btn_Guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Guardar.Click
        Try

        
            actualizarProductoVolVenta()
            List_Producto_VolVenta = ListVolVenta

            Dim nVolumenVenta As New Negocio.VolumenVenta
            nVolumenVenta.InsertaVolumenVentaxParametros(List_Producto_VolVenta)
            ListVolVenta = List_Producto_VolVenta

            objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


End Class


﻿Public Class FrmMantModulo
    Inherits System.Web.UI.Page

#Region "variables"
    Dim listaModulo As List(Of Entidades.Modulo)
    Private Lista_Modulo_Area As List(Of Entidades.Linea_Area)
    Private objScript As New ScriptManagerClass
   

    Private Enum operativo
        Ninguno = 0
        InsertUpdate = 1
    End Enum
#End Region

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / NRO DIAS VIGENCIA / REGISTRAR CLIENTE / EDITAR CLIENTE
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {132, 133})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

    End Sub
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Private Sub frm_State(ByVal modo As Integer)
        limpiarControles()

        Select Case modo
            Case operativo.Ninguno
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
                Panel_Modulo.Visible = False

            Case operativo.InsertUpdate
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                Panel_Modulo.Visible = True

        End Select
    End Sub

#Region "VISTAS FORMULARIOS"

    Private Sub limpiarControles()
        txtCodigoModulo.Text = ""
        txtNombre.Text = ""

    End Sub

#End Region

    Private Sub FrmMantModulo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ValidarPermisos()
            ConfigurarDatos()
            frm_State(operativo.Ninguno)
            frm_OnLoad()
        End If
    End Sub

#Region "funciones"

    Private Sub setlistaModulo(ByVal lista As List(Of Entidades.Modulo))
        Session.Remove("ModuloConsulta")
        Session.Add("ModuloConsulta", lista)
    End Sub
    Private Function getlistaModulo() As List(Of Entidades.Modulo)
        Return CType(Session.Item("ModuloConsulta"), List(Of Entidades.Modulo))
    End Function

#End Region

    Private Sub frm_OnLoad()
        Try
            getLineaByParameters()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Private Sub getLineaByParameters()
        Try
            Me.listaModulo = (New Negocio.Modulo).SelectModulo("l")
            setlistaModulo(listaModulo)
            DGV_ModuloBusqueda.DataSource = listaModulo
            DGV_ModuloBusqueda.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        frm_State(operativo.InsertUpdate)

        Try
            Dim objModulo As New Negocio.Modulo
           
            txtNombre.Enabled = True

        Catch ex As Exception
            txtNombre.Enabled = False

        End Try
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        frm_State(operativo.Ninguno)
    End Sub

    Protected Sub DGV_ModuloBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_ModuloBusqueda.PageIndexChanging
        DGV_ModuloBusqueda.PageIndex = e.NewPageIndex
        DGV_ModuloBusqueda.DataSource = getlistaModulo()
        DGV_ModuloBusqueda.DataBind()
    End Sub

    Private Sub actualizarModulo()
        listaModulo = getlistaModulo()
        setlistaModulo(listaModulo)
    End Sub


    Private Function getModulo() As Entidades.Modulo
        Dim objModulo As New Entidades.Modulo
        With objModulo
            .nombre = txtNombre.Text.Trim
            If IsNumeric(txtCodigoModulo.Text) Then .IdModulo = CInt(txtCodigoModulo.Text)
        End With
        Return objModulo
    End Function

    Private Sub registrarModulo()
        Dim hecho As Boolean = False
        Try
            Dim objModulo As Entidades.Modulo = getModulo()
            Dim objNModulo As New Negocio.Modulo

            If txtCodigoModulo.Text = "" Then
                hecho = objNModulo.InsertaModulo(objModulo)
            Else
                hecho = objNModulo.ActualizaModulo(objModulo)
            End If
            If hecho Then
                frm_State(operativo.Ninguno)
                getLineaByParameters()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim ngcValTabla As New Negocio.TipoTabla
        If CStr(ViewState("Modo")) = "1" Then
            Dim objValTabla As Entidades.TipoTabla = ngcValTabla.ValidadNombres("Modulo", "Nombre", Me.txtNombre.Text.Trim)
            If objValTabla.Item >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe el nombre " + Me.txtNombre.Text.Trim + " ingrese un nombre diferente" + "\n ó este inactivo.")
                txtNombre.Focus()
                Exit Sub
            End If
        End If

        registrarModulo()
    End Sub

    Protected Sub DGV_ModuloBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_ModuloBusqueda.SelectedIndexChanged
        cargarDatosModuloEdicion(CInt(DGV_ModuloBusqueda.SelectedRow.Cells(0).Text))
    End Sub

    Private Sub cargarDatosModuloEdicion(ByVal idModulo As Integer)
  
        Try
            frm_State(operativo.InsertUpdate)

            Dim objModulo As List(Of Entidades.Modulo) = (New Negocio.Modulo).SelectxIdModulo(idModulo)
            If objModulo.Count > 0 Then
                txtCodigoModulo.Text = CStr(objModulo.Item(0).IdModulo)
                txtNombre.Text = objModulo.Item(0).nombre

            End If

            Try
                Dim objNModulo As New Negocio.Modulo
                txtNombre.Enabled = True
            Finally

            End Try
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

  
    Protected Sub DGV_ModuloBusqueda_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles DGV_ModuloBusqueda.RowDeleting

        Dim modulo As String = DGV_ModuloBusqueda.DataKeys(e.RowIndex).Value.ToString
        cargarDatosModuloEliminar(CInt(modulo))

    End Sub

    Private Sub cargarDatosModuloEliminar(ByVal idModulo As Integer)

        Try
            frm_State(operativo.Ninguno)
            Dim objModulo As List(Of Entidades.Modulo) = (New Negocio.Modulo).EliminarIdModulo(idModulo)
            getLineaByParameters()
            objScript.mostrarMsjAlerta(Me, "Se elimino el registro")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

End Class
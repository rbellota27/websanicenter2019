﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmMantUbigeoZona
    Inherits System.Web.UI.Page

    Private FuncCombo As New Combo
    Private EntUbigeo As New Entidades.Ubigeo
    Private NegUbigeo As New Negocio.Ubigeo
    Private objScript As New ScriptManagerClass
    Private dtUbigeo As New DataTable
    Private util As New Util

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFRM()
        Else
            actualizar_atributos()
        End If
    End Sub

    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try
            cargarControles()
            cargar_recargar_datos()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarControles()
        FuncCombo.LLenarCboDepartamento(Me.cboDpto)
        FuncCombo.LlenarZonaUbigeo(Me.cboZona)
    End Sub

    Private Sub actualizar_atributos()
        Try
            dtUbigeo = CType(Session.Item("dtUbigeo"), DataTable)
            EntUbigeo.CodDpto = CStr(Me.cboDpto.SelectedValue)
            EntUbigeo.CodProv = CStr(Me.cboProv.SelectedValue)
            EntUbigeo.CodDist = CStr(Me.cboDist.SelectedValue)
            EntUbigeo.IdZona = CInt(Me.cboZona.SelectedValue)
            lblMensaje.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cboDpto_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboDpto.SelectedIndexChanged
        FuncCombo.LLenarCboProvincia(Me.cboProv, Me.cboDpto.SelectedValue)
        cargar_listview()
    End Sub
    Protected Sub cboProv_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboProv.SelectedIndexChanged
        FuncCombo.LLenarCboDistrito(Me.cboDist, Me.cboDpto.SelectedValue, Me.cboProv.SelectedValue)
        cargar_listview()
    End Sub
    Protected Sub cboDist_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboDist.SelectedIndexChanged
        cargar_listview()
    End Sub
    Private Sub cargar_listview()
        actualizar_atributos()
        Dim strFilter As String
        Dim strSort As String
        strFilter = ""
        If CStr(cboDpto.SelectedValue) <> "00" Then
            strFilter = "Dpto='" + cboDpto.SelectedItem.Text + "'"
            If CStr(cboProv.SelectedValue) <> "00" Then
                strFilter = strFilter + " and Prov='" + cboProv.SelectedItem.Text + "'"
                If CStr(cboDist.SelectedValue) <> "00" Then
                    strFilter = strFilter + " and Dist='" + cboDist.SelectedItem.Text + "'"
                End If
            End If
        End If
        strSort = "Dpto,Prov,Dist"

        lvUbigeo.DataSource = util.SelectDataTable(dtUbigeo, strFilter, strSort)
        lvUbigeo.DataBind()
    End Sub
    Protected Sub cargar_recargar_datos()
        dtUbigeo = NegUbigeo.SelectAllDatos()
        Session.Remove("dtUbigeo")
        Session.Add("dtUbigeo", dtUbigeo)
    End Sub
    Protected Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar.Click

        If NegUbigeo.ActualizaUbigeoZona(EntUbigeo) Then
            lblMensaje.Text = "Datos Actualizados"
        Else
            lblMensaje.Text = "Hay un error en la actualización. Consulte con el Administrador del Sistema"
        End If
        cargar_recargar_datos()
        cargar_listview()

    End Sub

    Protected Sub lvUbigeo_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles lvUbigeo.PreRender
        'Esta funcion es necesaria para refrescar el Listview con el datapager
        cargar_listview()
    End Sub

End Class
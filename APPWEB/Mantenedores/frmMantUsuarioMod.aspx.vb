﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Collections

Public Class frmMantUsuarioMod
    Inherits System.Web.UI.Page

#Region "variables"
    Dim listaUsuario As List(Of Entidades.UsuarioModulo)
    Private objScript As New ScriptManagerClass
    Private cbo As New Combo
    Dim source As New AutoCompleteStringCollection()

    Private Enum operativo
        Ninguno = 0
        InsertUpdate = 1

    End Enum
#End Region


#Region "VISTAS FORMULARIOS"

    Private Sub limpiarControles()
        txtID.Text = ""
        txtNombre.Text = ""

    End Sub

#End Region


#Region "funciones"
    Private Sub setListaUsuarioModulo(ByVal lista As List(Of Entidades.UsuarioModulo))
        Session.Remove("UsuarioConsulta")
        Session.Add("UsuarioConsulta", lista)
    End Sub
    Private Function getListaUsuarioModulo() As List(Of Entidades.UsuarioModulo)
        Return CType(Session.Item("UsuarioConsulta"), List(Of Entidades.UsuarioModulo))
    End Function
#End Region

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub


    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / NRO DIAS VIGENCIA / REGISTRAR CLIENTE / EDITAR CLIENTE
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {132, 133})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

    End Sub



    Private Sub frm_State(ByVal modo As Integer)
        limpiarControles()

        Select Case modo
            Case operativo.Ninguno
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
                Panel_Usuarios_Modulo.Visible = False

            Case operativo.InsertUpdate
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                Panel_Usuarios_Modulo.Visible = True


        End Select
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ValidarPermisos()
            ConfigurarDatos()
            frm_State(operativo.Ninguno)
            frm_OnLoad()
        End If
    End Sub

    Private Sub frm_OnLoad()
        Try
            cbo.llenarModulo(Me.ddlModulo, True)
            getLineaByParameters()
            'Data()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    'Private Sub Data()
    '    Dim objPersona As List(Of Entidades.Trabajador) = (New Negocio.UsuarioModulo).Textbox()
    '    For i As Integer = 1 To objPersona.Count - 1
    '        source.Add(objPersona.Item(i).nombre)
    '    Next

    'End Sub

    Private Sub getLineaByParameters()
        Try
            Me.listaUsuario = (New Negocio.UsuarioModulo).SelectUsuarioModulo("l")
            setListaUsuarioModulo(listaUsuario)
            DGV_Usuario.DataSource = listaUsuario
            DGV_Usuario.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        cbo.llenarModulo(Me.ddlModulo, True)
        frm_State(operativo.InsertUpdate)
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim ngcValTabla As New Negocio.TipoTabla
        If CStr(ViewState("Modo")) = "1" Then
            Dim objValTabla As Entidades.TipoTabla = ngcValTabla.ValidadNombres("UsuarioModulo", "Idusuario", Me.txtNombre.Text.Trim)
            If objValTabla.Item >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe el Id " + Me.txtID.Text.Trim + " ingrese un Id diferente" + "\n ó este inactivo.")
                txtNombre.Focus()
                Exit Sub
            End If
        End If
        registrarProceso()

    End Sub


    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        frm_State(operativo.Ninguno)
    End Sub

    Private Sub registrarProceso()
        Dim hecho As Boolean = False
        Try
            Dim objUsuMod As Entidades.UsuarioModulo = getUsuario()
            Dim objNUsuMod As New Negocio.UsuarioModulo

            If txtID.Text = "" Then
                hecho = objNUsuMod.AgregarUsuario(objUsuMod)
            Else
                hecho = objNUsuMod.ActualizaUsuario(objUsuMod)
            End If
            If hecho Then
                frm_State(operativo.Ninguno)
                getLineaByParameters()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Function getUsuario() As Entidades.UsuarioModulo
        Dim objUsuario As New Entidades.UsuarioModulo
        Dim objNUsuMod As New Negocio.UsuarioModulo

        With objUsuario

            .idusuario = CInt(IIf(IsNumeric(txtID.Text) = True, txtID.Text, 0))
            .usuario = CInt(objNUsuMod.SelectxNombreUsuario(txtNombre.Text.Trim))
            .modulo = CInt(ddlModulo.SelectedValue)

        End With
        Return objUsuario
    End Function

    Protected Sub DGV_Usuario_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_Usuario.SelectedIndexChanged
        cargarDatosProcesosEdicion(CInt(DGV_Usuario.SelectedRow.Cells(0).Text))
    End Sub


    Private Sub cargarDatosProcesosEdicion(ByVal idUsuario As Integer)

        Try
            frm_State(operativo.InsertUpdate)

            Dim objUsuario As List(Of Entidades.UsuarioModulo) = (New Negocio.UsuarioModulo).SelectxIdUsuario(idUsuario)
            If objUsuario.Count > 0 Then

                txtID.Text = CStr(objUsuario.Item(0).idusuario)
                txtNombre.Text = CStr(objUsuario.Item(0).usu)
                ddlModulo.SelectedValue = CInt(objUsuario.Item(0).modulo)

            End If

            Try
                txtNombre.Enabled = True

            Finally

            End Try
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub DGV_Usuario_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles DGV_Usuario.RowDeleting
        Dim usuario As String = DGV_Usuario.DataKeys(e.RowIndex).Value.ToString
        cargarDatosProcesoEliminar(CInt(usuario))
    End Sub

    Private Sub cargarDatosProcesoEliminar(ByVal idUsuario As Integer)

        Try
            frm_State(operativo.Ninguno)
            idUsuario = (New Negocio.UsuarioModulo).EliminarUsuario(idUsuario)
            getLineaByParameters()
            objScript.mostrarMsjAlerta(Me, "Se elimino el registro")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub DGV_Usuario_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_Usuario.PageIndexChanging
        DGV_Usuario.PageIndex = e.NewPageIndex
        DGV_Usuario.DataSource = getListaUsuarioModulo()
        DGV_Usuario.DataBind()
    End Sub


End Class
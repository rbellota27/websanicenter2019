﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantCaja_Post.aspx.vb" Inherits="APPWEB.FrmMantCaja_Post" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">
        <table width="100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                    onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                    OnClientClick="return(validarSave());" CausesValidation="true" />
                                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                                    OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Relación entre Cajas y sus P.O.S.
                </td>
            </tr>
            <tr>
                <td align="center">
                    <br />
                    <fieldset class="FieldSetPanel">
                        <legend>
                            <asp:Label ID="lblModo" runat="server" Text="Búsqueda"></asp:Label></legend>
                    <table width="90%">
                        <tr>
                            <td class="Label_fsp">
                                <asp:Label ID="lblTienda" runat="server" Text="Tienda:"></asp:Label>
                            </td>
                            <td style="text-align: left;">
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label_fsp">
                                <asp:Label ID="lblCaja" runat="server" Text="Caja:"></asp:Label>
                            </td>
                            <td style="text-align: left;">
                                <asp:UpdatePanel ID="upCaja" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="cboCaja" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="cboTienda" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td class="Label_fsp">
                                <asp:Label ID="lblEstado" runat="server" Text="Estado:"></asp:Label>
                            </td>
                            <td style="text-align: left;">
                                <asp:RadioButtonList ID="rbtlEstado" runat="server" CssClass="Label_fsp" RepeatDirection="Horizontal ">
                                    <asp:ListItem Text="Todos" Value="2"></asp:ListItem>
                                    <asp:ListItem Selected="True" Text="Activo" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label_fsp">
                                <asp:Label ID="lblBanco" runat="server" Text="Banco:"></asp:Label>
                            </td>
                            <td style="text-align: left;">
                                <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label_fsp">
                                <asp:Label ID="lblCuentaBancaria" runat="server" Text="Cuenta Bancaria:"></asp:Label>
                            </td>
                            <td style="text-align: left;">
                                <asp:UpdatePanel ID="upCuentaBancaria" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="cboCuentaBancaria" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="cboBanco" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td class="Label_fsp">
                                <asp:Label ID="lblPost" runat="server" Text="P.O.S.:"></asp:Label>
                            </td>
                            <td style="text-align: left;">
                                <asp:UpdatePanel ID="upPost" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="cboPost" runat="server">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="cboCuentaBancaria" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="cboBanco" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                        <td colspan="5"></td>
                        <td>
                        <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseover="this.src='/Imagenes/Buscar_A.JPG';" onmouseout="this.src='/Imagenes/Buscar_B.JPG';" />
                        </td>
                        
                        </tr>
                        
                    </table>
                    </fieldset>
                    
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <br />
                    <asp:UpdateProgress ID="upGrilla_Progress" runat="server" DisplayAfter="1000" AssociatedUpdatePanelID="upGrilla">
                        <ProgressTemplate>
                        <asp:Image ID="ImgProgress" runat="server" ImageUrl="~/Imagenes/ajax.gif" />
                            </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upGrilla" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="dgvCaja_Post" runat="server" AutoGenerateColumns="False" CellPadding="2"
                                Width="100%" EnableViewState="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEditar" runat="server" OnClick="lbtnEditar_Click">Editar</asp:LinkButton>
                                            <asp:HiddenField ID="hddIdCaja" Value='<%# DataBinder.Eval(Container.DataItem,"IdCaja") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdPost" Value='<%# DataBinder.Eval(Container.DataItem,"IdPost") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdTienda" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdBanco" Value='<%# DataBinder.Eval(Container.DataItem,"IdBanco") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdCuentaBancaria" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdEstado" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Eliminar</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" />
                                    <asp:BoundField DataField="CajaNombre" HeaderText="Caja" />
                                    <asp:BoundField DataField="PostIdentificador" HeaderText="P.O.S." NullDisplayText="" />
                                    <asp:BoundField DataField="PostDescripcion" HeaderText="Descripción P.O.S." NullDisplayText="" />
                                    <asp:BoundField DataField="CuentaBancaria" HeaderText="Cta. Bancaria" NullDisplayText="" />
                                    <asp:BoundField DataField="Banco" HeaderText="Banco" NullDisplayText="" />
                                    <asp:checkboxfield DataField="Estado" HeaderText="Activo"/>
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <script language="javascript" type="text/javascript">
        function validarSave() {
            var Caja = document.getElementById('<%=cboCaja.ClientID%>');
            var idCaja = Caja.value;
            if (idCaja == 0) {
                alert('Seleccione una Caja.');

                return false;
            }

            var Post = document.getElementById('<%=cboPost.ClientID%>');
            var idPost = Post.value;
            if (idPost == 0) {
                alert('Seleccione un Post.');

                return false;
            }

            return (confirm('Desea Continuar Con la Operacion'));
        }
    
    </script>

</asp:Content>

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmMantGrupoProd
    Inherits System.Web.UI.Page
    Private objTP As New Negocio.TipoProducto
    Private objScript As New ScriptManagerClass
#Region "Propiedades"
    Public Property ListaTP() As List(Of Entidades.TipoProducto)
        Get
            Return CType(Session("ListaTipoProducto"), List(Of Entidades.TipoProducto))
        End Get
        Set(ByVal value As List(Of Entidades.TipoProducto))
            Session.Remove("listaTipoProducto")
            Session.Add("listaTipoProducto", value)
        End Set
    End Property
#End Region

    Public Enum modo
        inicio = 0
        nuevo = 1
        edicion = 2
        buscar = 3
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            FrmInicial()
        End If
    End Sub
    Private Sub FrmInicial()
        ocultaBtn("Load")
        LimpiaCtrl()
        ListaTP = objTP.selectTipoProductoAll("")
        Me.gvwGrupoProductos.DataSource = ListaTP
        Me.gvwGrupoProductos.DataBind()
    End Sub
    Private Sub btnBuscarProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarProducto.Click
        ocultaBtn("load")
        ListaTP = objTP.selectTipoProductoAll(Me.txtnombreBusGrupo.Text)
        Me.gvwGrupoProductos.DataSource = ListaTP
        Me.gvwGrupoProductos.DataBind()

    End Sub
    Private Function getDatos() As Entidades.TipoProducto
        Dim objtProd As New Entidades.TipoProducto
        objtProd.Id = CInt(IIf(Me.txtCodigo.Text = "", 0, txtCodigo.Text))
        objtProd.Nombre = Me.txtNombreGrupo.Text
        objtProd.Abv = Me.txtNombreAbrev.Text
        objtProd.Estado = CStr(rblEstado.SelectedValue)
        Return objtProd
    End Function
    Private Sub InsertUpdateTP(ByVal tipo As Integer)
        Select Case tipo
            Case modo.edicion
                If objTP.UpdateTipoproducto(getDatos) = True Then
                    objScript.mostrarMsjAlerta(Me, "Se Actualizo con exito")
                End If
            Case modo.nuevo
                If objTP.InsertaTipoProducto(getDatos) > 0 Then
                    objScript.mostrarMsjAlerta(Me, "Se inserto con exito")
                End If
        End Select
    End Sub
    Private Sub ocultaControles(ByVal estado As Boolean)
        'Me.lblId.Visible = estado
        'Me.lblNombreGrupo.Visible = estado
        'Me.lblAbrev.Visible = estado
        'Me.lblEstado.Visible = estado
        'Me.lblNombreGrupoBusqueda.Visible = estado
        'Me.txtNombreAbrev.Visible = estado
        Me.pnlCtrl.Visible = estado
        Me.pnlCtrlBus.Visible = Not estado
    End Sub
    Private Sub ocultaBtn(ByVal tipo As String)
        Select Case tipo
            Case "Load"
                Me.pnlCtrl.Visible = False
                Me.pnlCtrlBus.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnNuevo.Visible = True
                Me.btnCancelar.Visible = False
            Case "Nuevo"
                Me.pnlCtrl.Visible = True
                Me.pnlCtrlBus.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnCancelar.Visible = True
            Case "Editar"
                Me.pnlCtrl.Visible = True
                Me.pnlCtrlBus.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnCancelar.Visible = True
        End Select


    End Sub
    Private Sub LimpiaCtrl()
        Me.txtNombreAbrev.Text = ""
        Me.txtNombreGrupo.Text = ""
        Me.txtnombreBusGrupo.Text = ""
        Me.txtCodigo.Text = ""
    End Sub
    Private Sub gvwGrupoProductos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvwGrupoProductos.SelectedIndexChanged
        Try
            ViewState.Add("Tipo", modo.edicion)
            ocultaBtn("Editar")
            cargaData()
            Me.gvwGrupoProductos.DataSource = Nothing
            Me.gvwGrupoProductos.DataBind()

        Catch ex As Exception
            Throw New Exception
        End Try

    End Sub
    Private Sub cargaData()
        txtCodigo.Text = CStr(IIf(Me.gvwGrupoProductos.SelectedRow.Cells(1).Text = "", 0, gvwGrupoProductos.SelectedRow.Cells(1).Text))
        txtNombreGrupo.Text = Me.gvwGrupoProductos.SelectedRow.Cells(2).Text
        txtNombreAbrev.Text = Me.gvwGrupoProductos.SelectedRow.Cells(3).Text
        rblEstado.SelectedValue = CStr(IIf(gvwGrupoProductos.SelectedRow.Cells(4).Text = "Activo", 1, 0))

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click

        ocultaBtn("Load")
        InsertUpdateTP(CInt(ViewState("Tipo")))
        ViewState.Add("Tipo", modo.nuevo)
        ListaTP = objTP.selectTipoProductoAll("")
        Me.gvwGrupoProductos.DataSource = ListaTP
        Me.gvwGrupoProductos.DataBind()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        ViewState.Add("Tipo", modo.nuevo)
        LimpiaCtrl()
        ocultaBtn("Load")
        ListaTP = objTP.selectTipoProductoAll("")
        Me.gvwGrupoProductos.DataSource = ListaTP
        Me.gvwGrupoProductos.DataBind()

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        LimpiaCtrl()
        ocultaBtn("Nuevo")
        ViewState.Add("Tipo", modo.nuevo)
        gvwGrupoProductos.DataSource = Nothing
        gvwGrupoProductos.DataBind()
    End Sub
End Class
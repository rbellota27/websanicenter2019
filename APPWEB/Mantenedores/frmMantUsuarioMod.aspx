﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMantUsuarioMod.aspx.vb" Inherits="APPWEB.frmMantUsuarioMod" MasterPageFile="~/Principal.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="MantenimientoUsuarioModulo" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

    <!--link href="http://localhost:25213/JS/jquery-ui.css" rel="stylesheet" type="text/css" />-->

  
  <script src="../JS/external/jquery/jquery.js" type="text/javascript"></script>
    <script src="http://localhost:25213/JS/jquery-ui.js" type="text/javascript"></script>
 
  


     <script type="text/javascript">
         $(document).ready(function () {
             $("#<%=txtNombre.ClientID %>").autocomplete({
                 source: '<%=Page.ResolveUrl("~/Autocomplete.ashx") %>'
             });
         });
</script>
    
<table width="100%">
<tr>
            <td>
                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseout="this.src='/Imagenes/Nuevo_b.JPG';"
                    onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" />              
              
               <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                    onmouseout="this.src='/Imagenes/Guardar_B.JPG';" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                     />              

                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                    onmouseout="this.src='/Imagenes/Arriba_B.JPG';" onmouseover="this.src='/Imagenes/Arriba_A.JPG';"
                    ToolTip="Regresar a Administración Sistema" />
            </td>
   </tr>
   
       <tr>
            <td class="TituloCelda" >
                Usuarios Modulo
            </td>
       </tr>  
       
       
              <tr>
       <td>
       <asp:Panel  ID="Panel_Usuarios_Modulo" runat="server" Width="100%">
       <table cellpadding="0" cellspacing="3" width="80%">
       <tr>
              <td class="Texto" style="width: 65px">Id : </td>
              <td>  <asp:TextBox ID="txtID" runat="server" CssClass="TextBoxReadOnly" 
                      ReadOnly="True" Width="52px"></asp:TextBox></td>
         
       </tr>

              <tr>
              <td class="Texto" style="width: 65px">Usuario :</td>
              <td><asp:TextBox ID="txtNombre" runat="server" ></asp:TextBox></td>
       </tr>
           

              <tr>
              <td class="Texto" style="width: 65px">Modulo :</td>
              <td><asp:DropDownList ID="ddlModulo" runat="server"></asp:DropDownList></td>
       </tr>
         <tr>
                         <td colspan="2"></td>
                        </tr>
       </table>
       </asp:Panel>
        </td>
       </tr>  
       
       
         <tr>
       <td>
       <asp:GridView ID="DGV_Usuario" Width="50%" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="1" EmptyDataText="No existe información."
                    ForeColor="#333333" GridLines="None"  DatakeyNames="IdUsuario">
                     <RowStyle BackColor="#F7F6F3" CssClass="GrillaRow" ForeColor="#333333" />

                      <Columns>
                    
                        <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario" NullDisplayText="0" >
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="usu" HeaderText="Nombre" ItemStyle-Width="250px" NullDisplayText="-----" />                      
                        <asp:BoundField DataField="mod1" HeaderText="Modulo" ItemStyle-Width="100px" NullDisplayText="-----" />                       
                        <asp:CommandField  SelectText="Editar" ShowSelectButton="True" HeaderText="Editar" />                                               
                        <asp:ButtonField CommandName="Delete" HeaderText="Eliminar" ShowHeader="True" Text="Eliminar" />
                       
                    </Columns>

                    <FooterStyle BackColor="#5D7B9D" CssClass="GrillaFooter" Font-Bold="True" ForeColor="White" />                    
                    <SelectedRowStyle BackColor="#E2DED6" CssClass="GrillaSelectedRow" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" CssClass="GrillaHeader" Font-Bold="True" ForeColor="White" />                    
                    <AlternatingRowStyle BackColor="White" CssClass="GrillaRowAlternating" ForeColor="#284775" />
       </asp:GridView>
        </td>
    </tr> 

     <tr>
            <td style="width: 34px">
            
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>

</table>



</asp:Content>




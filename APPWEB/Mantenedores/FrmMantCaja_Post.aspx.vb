﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmMantCaja_Post
    Inherits System.Web.UI.Page
    Private FuncCombo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegCaja_PostView As New Negocio.Caja_PostView
    Private ListCaja_PostView As New List(Of Entidades.Caja_PostView)
    Private p As New Entidades.Caja_PostView
    Private modo As New modo_menu

    Enum modo_menu
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Insertar = 3
        Actualizar = 4
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            inicializarFRM()
            modo = modo_menu.Inicio
            HabilitarControles(modo)
        Else
            actualizar_atributos()
        End If
    End Sub

    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try
            cargar_recargar_datos()
            cargar_controles()
            cargar_dgv()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizar_atributos()
        Try
            ListCaja_PostView = CType(Session.Item("ListCaja_PostView"), List(Of Entidades.Caja_PostView))
            modo = CType(Session.Item("modo"), modo_menu)
            p.IdTienda = CInt(Me.cboTienda.SelectedValue)
            p.IdCaja = CInt(IIf(CStr(Me.cboCaja.SelectedValue) <> "", Me.cboCaja.SelectedValue, 0))
            'p.IdTipoTarjeta = CInt(Me.cboTipoTarjeta.SelectedValue)
            p.IdBanco = CInt(Me.cboBanco.SelectedValue)
            p.IdCuentaBancaria = CInt(IIf(CStr(Me.cboCuentaBancaria.SelectedValue) <> "", Me.cboCuentaBancaria.SelectedValue, 0))
            p.IdPost = CInt(IIf(CStr(Me.cboPost.SelectedValue) <> "", Me.cboPost.SelectedValue, 0))

            Select Case CInt(rbtlEstado.SelectedValue)
                Case 2
                    p.Estado = Nothing
                Case 1
                    p.Estado = True
                Case 0
                    p.Estado = False
            End Select
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub cargar_recargar_datos()
        ListCaja_PostView = objNegCaja_PostView.SelectAll()
        Session.Remove("ListCaja_PostView")
        Session.Add("ListCaja_PostView", ListCaja_PostView)
    End Sub
    Sub cargar_recargar_modo(ByVal x As modo_menu)
        modo = x
        Session.Remove("modo")
        Session.Add("modo", modo)
    End Sub

    Private Sub cargar_controles()
        FuncCombo.LLenarCboTienda(cboTienda, True)
        FuncCombo.LlenarCboCajaxIdTienda(cboCaja, CInt(cboTienda.SelectedValue), True)

        FuncCombo.LlenarCboBanco(cboBanco, True)
        FuncCombo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(cboBanco.SelectedValue), True)
        FuncCombo.LlenarCboPostxIdBancoxIdCuentaBancaria(cboPost, CInt(cboBanco.SelectedValue), _
                                                         CInt(cboCuentaBancaria.SelectedValue), True)
    End Sub

    Protected Sub cargar_dgv()
        actualizar_atributos()
        dgvCaja_Post.DataSource = objNegCaja_PostView.FiltrarLista(p, ListCaja_PostView)
        dgvCaja_Post.DataBind()
    End Sub

    Public Sub limpiarCtrl()

        Me.cboCaja.SelectedIndex = 0
        Me.cboPost.SelectedIndex = 0
        Me.rbtlEstado.SelectedValue = "1" 'Activos

    End Sub

    'Private Sub cargar_entidad_en_controles(ByVal x As Entidades.Caja_PostView)
    '    cboCaja.SelectedValue = CStr(x.IdCaja)
    '    cboPost.SelectedValue = CStr(x.IdPost)
    '    txtDescripcion.Text = x.Descripcion
    '    rbtlEstado.SelectedValue = x.Estado
    'End Sub

    Public Sub HabilitarControles(ByVal x As modo_menu)
        Select Case x
            Case modo_menu.Inicio

                Me.btnNuevo.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscar.Visible = True

                Me.cboTienda.Enabled = True
                Me.cboCaja.Enabled = True
                'Me.cboTipoTarjeta.Enabled = True
                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True

                Me.cboPost.Enabled = True


                Me.rbtlEstado.Items(0).Enabled = True

                Me.dgvCaja_Post.Enabled = True
                Me.lblModo.Text = "Búsqueda"

            Case modo_menu.Nuevo

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False

                Me.cboTienda.Enabled = True
                Me.cboCaja.Enabled = True
                'Me.cboTipoTarjeta.Enabled = True
                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True

                Me.cboPost.Enabled = True
                Me.rbtlEstado.Items(0).Enabled = False

                Me.dgvCaja_Post.Enabled = False
                Me.lblModo.Text = "Nuevo"
            Case modo_menu.Editar

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False

                Me.cboTienda.Enabled = False
                Me.cboCaja.Enabled = False
                'Me.cboTipoTarjeta.Enabled = False
                Me.cboBanco.Enabled = False
                Me.cboCuentaBancaria.Enabled = False

                Me.cboPost.Enabled = False

                Me.rbtlEstado.Items(0).Enabled = False

                Me.dgvCaja_Post.Enabled = False
                Me.lblModo.Text = "Edición"
            Case modo_menu.Actualizar
            Case modo_menu.Insertar
        End Select
    End Sub

    Public Function DecodeCadena(ByVal cadena As String) As String
        Dim cadenavalida As String = ""
        cadenavalida = HttpUtility.HtmlDecode(cadena)
        Return cadenavalida
    End Function


    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        cargar_dgv()
    End Sub

    Protected Sub lbtnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            cargar_recargar_modo(modo_menu.Editar)
            Dim lbtnEditar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)

            Dim fila As GridViewRow = CType(lbtnEditar.NamingContainer, GridViewRow)

            Me.cboTienda.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdTienda"), HiddenField).Value.Trim())
            FuncCombo.LlenarCboCajaxIdTienda(cboCaja, CInt(Me.cboTienda.SelectedValue), True)
            Me.cboCaja.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdCaja"), HiddenField).Value.Trim())

            Me.cboBanco.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdBanco"), HiddenField).Value.Trim())
            FuncCombo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), True)
            Me.cboCuentaBancaria.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdCuentaBancaria"), HiddenField).Value.Trim())

            'Me.cboTipoTarjeta.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdTipoTarjeta"), HiddenField).Value.Trim())
            'FuncCombo.LlenarCboPostxIdTipoTarjeta(cboPost, CInt(Me.cboTipoTarjeta.SelectedValue), True)
            FuncCombo.LlenarCboPostxIdBancoxIdCuentaBancaria(cboPost, CInt(cboBanco.SelectedValue), _
                                                         CInt(cboCuentaBancaria.SelectedValue), True)
            Me.cboPost.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdPost"), HiddenField).Value.Trim())

            Me.rbtlEstado.SelectedValue = CStr(IIf(CBool((CType(fila.Cells(0).FindControl("hddIdEstado"), HiddenField).Value)), 1, 0))

            HabilitarControles(modo)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        cargar_recargar_modo(modo_menu.Nuevo)
        HabilitarControles(modo)
        'limpiarCtrl()  'se deben dejar los datos escritos
        cargar_dgv()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click

        cargar_recargar_modo(modo_menu.Inicio)
        HabilitarControles(modo_menu.Inicio)
        limpiarCtrl()
        cargar_dgv()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click

        Dim mensaje As String
        Dim exito As Boolean
        exito = True
        mensaje = ""
        'opcional ini
        cargar_recargar_datos() 'Los datos disponibles se actualizan antes de realizar la validacion
        'opcional fin

        Select Case modo
            Case modo_menu.Nuevo

                If objNegCaja_PostView.ValidarEntidadxPostxTienda(p, ListCaja_PostView) Then

                    If objNegCaja_PostView.ValidarEntidad(p, ListCaja_PostView) Then
                        If objNegCaja_PostView.InsertT(p) Then
                            mensaje = "Se Guardó Correctamente"
                        End If
                    Else
                        exito = False
                        mensaje = "Ya existe un registro con esa Caja y ese Post"
                    End If

                Else
                    exito = False
                    mensaje = "Ese Post ya esta asignado a una Tienda, no puede ser asignado a una Caja de otra Tienda"
                End If

            Case modo_menu.Editar
                If objNegCaja_PostView.ValidarEntidadxPostxTienda(p, ListCaja_PostView) Then
                    If objNegCaja_PostView.UpdateT(p) Then
                        mensaje = "Se Actualizó Correctamente"
                    End If
                Else
                    exito = False
                    mensaje = "No se puede activar este registro, porque ese Post ya esta asignado a una Tienda, no puede ser asignado a una Caja de otra Tienda"
                End If
        End Select

        objScript.mostrarMsjAlerta(Me, mensaje)

        If exito Then
            cargar_recargar_datos()
            cargar_recargar_modo(modo_menu.Inicio)
            HabilitarControles(modo)
            cargar_dgv()    'Muestra el registro ingresado

            limpiarCtrl()   'Limpia los controles
        End If
    End Sub

    Protected Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTienda.SelectedIndexChanged
        FuncCombo.LlenarCboCajaxIdTienda(cboCaja, p.IdTienda, True)
    End Sub

    'Protected Sub cboTipoTarjeta_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTipoTarjeta.SelectedIndexChanged
    '    FuncCombo.LlenarCboPostxIdTipoTarjeta(cboPost, p.IdTipoTarjeta, True)
    'End Sub

    Protected Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboBanco.SelectedIndexChanged
        FuncCombo.LlenarCboCuentaBancaria(cboCuentaBancaria, p.IdBanco, True)
        'FuncCombo.LlenarCboPostxIdBanco(cboPost, p.IdBanco, True)
        p.IdCuentaBancaria = CInt(cboCuentaBancaria.SelectedValue)
        FuncCombo.LlenarCboPostxIdBancoxIdCuentaBancaria(cboPost, p.IdBanco, p.IdCuentaBancaria, True)
    End Sub

    Protected Sub cboCuentaBancaria_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCuentaBancaria.SelectedIndexChanged
        FuncCombo.LlenarCboPostxIdBancoxIdCuentaBancaria(cboPost, p.IdBanco, p.IdCuentaBancaria, True)
    End Sub

    Protected Sub dgvCaja_Post_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvCaja_Post.RowDataBound
        'LinkButton Editar esta en un UpdatePanel y para que este no atrape su postback, se registra en el ScriptManager de la página Maestra
        Try
            If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
            e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
                Dim sm As ScriptManager
                sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                'sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("lbtnEditar"), LinkButton))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
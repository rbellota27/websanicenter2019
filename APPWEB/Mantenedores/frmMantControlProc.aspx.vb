﻿Public Class frmMantControlProc
    Inherits System.Web.UI.Page

#Region "variables"
    Dim listaControl As List(Of Entidades.ControlProceso)
    Private objScript As New ScriptManagerClass
    Private cbo As New Combo

    Private Enum operativo
        Ninguno = 0
        Insert = 1
        Update = 2
    End Enum
#End Region


#Region "VISTAS FORMULARIOS"

    Private Sub limpiarControles()
       
    End Sub

#End Region


#Region "funciones"

    Private Sub setListaControlProc(ByVal lista As List(Of Entidades.ControlProceso))
        Session.Remove("ControlConsulta")
        Session.Add("ControlConsulta", lista)
    End Sub
    Private Function getListaControlProc() As List(Of Entidades.ControlProceso)
        Return CType(Session.Item("ControlConsulta"), List(Of Entidades.ControlProceso))
    End Function

#End Region

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub


    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / NRO DIAS VIGENCIA / REGISTRAR CLIENTE / EDITAR CLIENTE
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {132, 133})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

    End Sub



    Private Sub frm_State(ByVal modo As Integer)
        limpiarControles()

        Select Case modo
            Case operativo.Ninguno
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
                btnActualizar.Visible = False
                Panel_control_Procesos.Visible = False

            Case operativo.Insert
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                btnActualizar.Visible = False
                Panel_control_Procesos.Visible = True

            Case operativo.Update
                btnNuevo.Visible = False
                btnGuardar.Visible = False
                btnCancelar.Visible = True
                btnActualizar.Visible = True
                Panel_control_Procesos.Visible = True

        End Select
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ValidarPermisos()
            ConfigurarDatos()
            frm_State(operativo.Ninguno)
            frm_OnLoad()
        End If
    End Sub


    Private Sub frm_OnLoad()
        Try
            cbo.llenarProceso(Me.ddlProceso, True)
            cbo.llenarProceso(Me.ddlProceso1, True)
            cbo.llenarProceso(Me.ddlProceso2, True)

            getLineaByParameters()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub


    Private Sub getLineaByParameters()
        Try
            Me.listaControl = (New Negocio.ControlProceso).SelectControlProc("l")
            setListaControlProc(listaControl)
            DGV_Control_Proceso.DataSource = listaControl
            DGV_Control_Proceso.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        cbo.llenarProceso(Me.ddlProceso, True)
        cbo.llenarProceso(Me.ddlProceso1, True)
        cbo.llenarProceso(Me.ddlProceso2, True)
        frm_State(operativo.Insert)
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
       
        RegistrarControlProceso()
    End Sub

    Private Sub RegistrarControlProceso()
        Dim hecho As Boolean = False
        Try
            Dim objControlProceso As Entidades.ControlProceso = getControlProceso()
            Dim objNControlProceso As New Negocio.ControlProceso

            If ddlProceso.SelectedValue > 0 Then
                hecho = objNControlProceso.AgregarControlProc(objControlProceso)
            Else
                hecho = objNControlProceso.ActualizaControlProc(objControlProceso)
            End If
            If hecho Then
                frm_State(operativo.Ninguno)
                getLineaByParameters()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub ActualizarControlProceso()
        Dim hecho As Boolean = False
        Try
            Dim objControlProceso As Entidades.ControlProceso = getControlProceso()
            Dim objNControlProceso As New Negocio.ControlProceso

            If ddlProceso.SelectedValue > 0 Then
                hecho = objNControlProceso.ActualizaControlProc(objControlProceso)
            End If
            If hecho Then
                frm_State(operativo.Ninguno)
                getLineaByParameters()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Function getControlProceso() As Entidades.ControlProceso
        Dim objControl As New Entidades.ControlProceso
        With objControl

            .proc = CInt(ddlProceso.SelectedValue)
            .proc1 = CInt(ddlProceso1.SelectedValue)
            .proc2 = CInt(ddlProceso2.SelectedValue)

        End With
        Return objControl
    End Function

    Protected Sub DGV_Control_Proceso_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_Control_Proceso.SelectedIndexChanged
        Dim listaControl As List(Of Entidades.ControlProceso) = (New Negocio.ControlProceso).SelectxNombreControl(CStr(DGV_Control_Proceso.SelectedRow.Cells(0).Text))
        Dim celda As Integer = listaControl.Item(0).proc
        cargarDatosControlEdicion(celda)
    End Sub


    Private Sub cargarDatosControlEdicion(ByVal proceso As Integer)

        Try
            frm_State(operativo.Update)

            Dim objControlProceso As List(Of Entidades.ControlProceso) = (New Negocio.ControlProceso).SelectxIdControl(proceso)
            If objControlProceso.Count > 0 Then

                ddlProceso.SelectedValue = CInt(objControlProceso.Item(0).proc)
                ddlProceso1.SelectedValue = CInt(objControlProceso.Item(0).proc1)
                ddlProceso2.SelectedValue = CInt(objControlProceso.Item(0).proc2)


            End If

         
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnActualizar.Click
        ActualizarControlProceso()
    End Sub

    Protected Sub DGV_Control_Proceso_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles DGV_Control_Proceso.RowDeleting
        Dim listaControl As List(Of Entidades.ControlProceso) = (New Negocio.ControlProceso).SelectxNombreControl(CStr(DGV_Control_Proceso.DataKeys(e.RowIndex).Value.ToString))
        Dim celda As Integer = listaControl.Item(0).proc

        cargarDatosProcesoEliminar(CInt(celda))
    End Sub

    Private Sub cargarDatosProcesoEliminar(ByVal idProceso As Integer)

        Try
            frm_State(operativo.Ninguno)
            idProceso = (New Negocio.ControlProceso).EliminarControlProceso(idProceso)
            getLineaByParameters()
            objScript.mostrarMsjAlerta(Me, "Se elimino el registro")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub DGV_Control_Proceso_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_Control_Proceso.PageIndexChanging
        DGV_Control_Proceso.PageIndex = e.NewPageIndex
        DGV_Control_Proceso.DataSource = getControlProceso()
        DGV_Control_Proceso.DataBind()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        frm_State(operativo.Ninguno)
    End Sub
End Class
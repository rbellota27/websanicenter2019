﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmGestionRemesa
    Inherits System.Web.UI.Page

    Dim cbo As Combo
    Dim objScript As New ScriptManagerClass

    Dim lista_MovBanco As List(Of Entidades.MovBancoView)
    Dim Lista_CajaAperturaCierre As List(Of Entidades.Caja_AperturaCierre)
    Private Const IdTipoDocumento As Integer = 9


    Enum operativo

        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar = 3
        Buscar_Exito = 4
        Registrar_Exito = 5
        Anular_Exito = 6
    End Enum

    Private Sub Frm_Limpiar()

        Me.ddl_Banco.SelectedIndex = 0
        OnChange_Banco()

        Me.tb_NroOperacion.Text = ""
        Me.tb_Observacion.Text = ""
        Me.tb_monto.Text = "0"

        Me.GV_Remesas.DataBind()
        Me.GV_Cheque.DataBind()
        Me.GV_ChequeRef.DataBind()
        Me.GV_DocumentoRef.DataBind()
        Me.GV_DocumentosReferencia_Find.DataBind()



    End Sub

    Private Property P_lista_MovBancoView() As List(Of Entidades.MovBancoView)
        Get
            Return CType(Session("lista_MovBanco"), List(Of Entidades.MovBancoView))
        End Get
        Set(ByVal value As List(Of Entidades.MovBancoView))
            Session.Remove("lista_MovBanco")
            Session.Add("lista_MovBanco", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        onLoad_GestionRemesa()
    End Sub

    Private Sub onLoad_GestionRemesa()
        Try

            If Not Me.IsPostBack Then

                Me.tb_FechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
                Me.tb_FechaMov.Text = Me.tb_FechaEmision.Text.Trim
                Me.tb_FechaCaja.Text = Me.tb_FechaEmision.Text.Trim
                Me.txtFechaInicio_DocRef.Text = Me.tb_FechaEmision.Text.Trim
                Me.txtFechaFin_DocRef.Text = Me.tb_FechaEmision.Text.Trim
                Me.tb_ChequeFechaIni.Text = Me.tb_FechaEmision.Text.Trim
                Me.tb_ChequeFechaFin.Text = Me.tb_FechaEmision.Text.Trim
                Me.txtFechaI.Text = Me.tb_FechaEmision.Text
                Me.txtFechaF.Text = Me.tb_FechaEmision.Text


                cbo = New Combo
                With cbo

                    .LlenarCboEmpresaxIdUsuario(Me.ddl_Empresa, CInt(Session("IdUsuario")), False)
                    .LLenarCboTipoDocumentoRefxIdTipoDocumento(Me.ddl_TipoDocumentoRef, IdTipoDocumento, 2, False)
                    .LlenarCboTipoConceptoBanco(Me.ddl_TipoConceptoBanco)
                    .LlenarCbo_MedioPago_MovBanco(Me.ddl_MedioPago, CInt(Me.ddl_TipoConceptoBanco.SelectedValue))
                    .LlenarCbo_ConceptoMovBanco_MedioPago(Me.ddl_ConceptoMovBanco, CInt(Me.ddl_MedioPago.SelectedValue), CInt(Me.ddl_TipoConceptoBanco.SelectedValue))
                    .LLenarCboEstadoDocumento(Me.ddl_EstadoDoc)
                    .llenarCboTipoOperacionxIdTpoDocumento(Me.ddl_TipoOperacion, IdTipoDocumento)
                    .LlenarCboMoneda(Me.ddl_Moneda)
                End With

                OnChange_Empresa()

                P_lista_MovBancoView = New List(Of Entidades.MovBancoView)

                cargar_CajaAperturaCierre()
                cargar_MovBanco()

                verFrm(operativo.Nuevo, True, False)

                actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue), False)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargar_CajaAperturaCierre(Optional ByVal setMonto As Boolean = False)

        Me.Lista_CajaAperturaCierre = (New Negocio.Caja_AperturaCierre).Caja_AperturaCierre_Select(0, CInt(Me.ddl_Empresa.SelectedValue), CInt(Me.ddl_Tienda.SelectedValue), CInt(Me.ddl_caja.SelectedValue), 0, Nothing, Me.tb_FechaCaja.Text)
        Me.GV_CajaAperuraCierre.DataSource = Me.Lista_CajaAperturaCierre
        Me.GV_CajaAperuraCierre.DataBind()

    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal Frm_Inicializar As Boolean, ByVal Frm_Limpiar As Boolean)

        If Frm_Inicializar Then

            Me.Lista_DocumentoRef = New List(Of Entidades.Documento)
            Me.setLista_DocumentoRef(Me.Lista_DocumentoRef)

            Me.Lista_ChequeRef = New List(Of Entidades.Cheque)
            Me.setLista_ChequeRef(Me.Lista_ChequeRef)

            P_lista_MovBancoView = New List(Of Entidades.MovBancoView)

        End If

        If Frm_Limpiar Then
            Me.Frm_Limpiar()
        End If


        Me.hddFrmModo.Value = CStr(FrmModo)

        Actualizar_Botones()
    End Sub

    Private Sub Actualizar_Botones()

        Me.lkb_Avanzado.Enabled = False
        Me.tb_NroDocumento.ReadOnly = True
        Me.Panel_Datos.Enabled = True

        Me.ddl_Empresa.Enabled = True
        Me.ddl_Tienda.Enabled = True
        Me.ddl_Serie.Enabled = True



        Select Case CInt(Me.hddFrmModo.Value)

            Case operativo.Inicio ' Registrar - buscar

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True
                Me.btnCheque.Visible = True

                GenerarCodigoDocumento()

            Case operativo.Nuevo

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True
                Me.btnCheque.Visible = True

                GenerarCodigoDocumento()

            Case operativo.Editar

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True
                Me.btnCheque.Visible = True

                Me.ddl_Empresa.Enabled = False
                Me.ddl_Tienda.Enabled = False
                Me.ddl_Serie.Enabled = False

            Case operativo.Buscar

            Case operativo.Registrar_Exito

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False
                Me.btnCheque.Visible = False

                Me.ddl_Empresa.Enabled = False
                Me.ddl_Tienda.Enabled = False
                Me.ddl_Serie.Enabled = False

            Case operativo.Buscar_Exito

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnBuscarDocumentoRef.Visible = True
                Me.btnCheque.Visible = True
                Me.Panel_Datos.Enabled = False

                Me.ddl_Empresa.Enabled = False
                Me.ddl_Tienda.Enabled = False
                Me.ddl_Serie.Enabled = False


            Case operativo.Anular_Exito

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False
                Me.btnCheque.Visible = False
                Me.Panel_Datos.Enabled = False
                Me.ddl_EstadoDoc.SelectedValue = "2"

                Me.ddl_Empresa.Enabled = False
                Me.ddl_Tienda.Enabled = False
                Me.ddl_Serie.Enabled = False

        End Select

    End Sub

    Private Sub ddl_Empresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Empresa.SelectedIndexChanged
        OnChange_Empresa()
    End Sub
    Private Sub OnChange_Empresa()
        If IsNothing(cbo) Then cbo = New Combo
        With cbo

            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.ddl_Tienda, CInt(Me.ddl_Empresa.SelectedValue), CInt(Session("IdUsuario")), False)
            .LLenarCboSeriexIdsEmpTienTipoDoc(Me.ddl_Serie, CInt(Me.ddl_Empresa.SelectedValue), CInt(Me.ddl_Tienda.SelectedValue), IdTipoDocumento)
            GenerarCodigoDocumento()

            .LlenarCboCajaxIdTiendaxIdUsuarioNew(Me.ddl_caja, CInt(Me.ddl_Tienda.SelectedValue), CInt(Session("IdUsuario")), False)
            .LlenarCbo_Banco_CuentaBancaria(Me.ddl_Banco, CInt(Me.ddl_Empresa.SelectedValue), True, True)
            .LlenarCboCuentaBancaria(Me.ddl_CuentaBancaria, CInt(Me.ddl_Banco.SelectedValue), CInt(Me.ddl_Empresa.SelectedValue), True)


        End With

    End Sub

    Private Sub ddl_Tienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Tienda.SelectedIndexChanged
        If IsNothing(cbo) Then cbo = New Combo
        With cbo

            .LLenarCboSeriexIdsEmpTienTipoDoc(Me.ddl_Serie, CInt(Me.ddl_Empresa.SelectedValue), CInt(Me.ddl_Tienda.SelectedValue), IdTipoDocumento)
            .LlenarCboCajaxIdTiendaxIdUsuarioNew(Me.ddl_caja, CInt(Me.ddl_Tienda.SelectedValue), CInt(Session("IdUsuario")), False)

            GenerarCodigoDocumento()

        End With
    End Sub

    Private Sub ddl_MedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_MedioPago.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .LlenarCbo_ConceptoMovBanco_MedioPago(Me.ddl_ConceptoMovBanco, CInt(Me.ddl_MedioPago.SelectedValue), CInt(Me.ddl_TipoConceptoBanco.SelectedValue))
        End With
    End Sub

    Private Sub ddl_Banco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Banco.SelectedIndexChanged
        OnChange_Banco()
    End Sub

    Private Sub OnChange_Banco()
        cbo = New Combo
        With cbo
            .LlenarCboCuentaBancaria(Me.ddl_CuentaBancaria, CInt(Me.ddl_Banco.SelectedValue), CInt(Me.ddl_Empresa.SelectedValue), True)
        End With
    End Sub

    Protected Sub btnAgregar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar.Click
        Try
            Dim IdCheque As Integer = 0
            Dim IdDocumento As Integer = 0
            Dim Referencia As String = ""
            If Me.GV_ChequeRef.Rows.Count > 0 Then
                IdCheque = CInt(CType(GV_ChequeRef.Rows(0).FindControl("hddIdCheque"), HiddenField).Value)
                Referencia = "CHEQUE " + HttpUtility.HtmlDecode(CType(GV_ChequeRef.Rows(0).FindControl("lblBanco"), Label).Text).Trim + " " + HttpUtility.HtmlDecode(CType(GV_ChequeRef.Rows(0).FindControl("lblNroCheque"), Label).Text).Trim

            End If
            If Me.GV_DocumentoRef.Rows.Count > 0 Then
                IdDocumento = CInt(CType(GV_DocumentoRef.Rows(0).FindControl("hddIdDocumento"), HiddenField).Value)
                Referencia = "DOCUMENTO " + HttpUtility.HtmlDecode(CType(GV_DocumentoRef.Rows(0).FindControl("lblTipoDocumento"), Label).Text).Trim + " " + HttpUtility.HtmlDecode(CType(GV_DocumentoRef.Rows(0).FindControl("lblNroDocumento"), Label).Text).Trim
            End If

            OnCLick_Agregar(IdDocumento, IdCheque, Referencia)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub OnCLick_Agregar(ByVal IdDocumento As Integer, ByVal IdCheque As Integer, ByVal Referencia As String)

        Me.lista_MovBanco = P_lista_MovBancoView

        Dim obj As New Entidades.MovBancoView
        With obj
            .IdMedioPago = CInt(Me.ddl_MedioPago.SelectedValue)
            .MedioPago = Me.ddl_MedioPago.SelectedItem.Text
            .IdBanco = CInt(Me.ddl_Banco.SelectedValue)
            .Banco = Me.ddl_Banco.SelectedItem.Text
            .IdCuentaBancaria = CInt(Me.ddl_CuentaBancaria.SelectedValue)
            .CuentaBancaria = Me.ddl_CuentaBancaria.SelectedItem.Text.Trim
            .IdConceptoMovBanco = CInt(Me.ddl_ConceptoMovBanco.SelectedValue)
            .ConceptoMovBanco = Me.ddl_ConceptoMovBanco.SelectedItem.Text.Trim
            .Monto = validar_AgregarMovBanco()
            .IdMedioPago = CInt(Me.ddl_MedioPago.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdCaja = CInt(Me.ddl_caja.SelectedValue)
            .IdTienda = CInt(Me.ddl_Tienda.SelectedValue)
            .FechaMov = CDate(Me.tb_FechaMov.Text)
            .FechaAprobacion = CDate(Me.tb_FechaCaja.Text.Trim)
            .NroOperacion = CStr(Me.tb_NroOperacion.Text.Trim)
            .Factor = (New Negocio.TipoConceptoBanco).SelectxId(CInt(Me.ddl_TipoConceptoBanco.SelectedValue))(0).Factor
            .Observacion = Me.tb_Observacion.Text.Trim
            .EstadoMov = True
            .EstadoAprobacion = True
            .IdDocumentoRef2 = IdDocumento
            .IdCheque = IdCheque
            .Referencia = Referencia

        End With

        Me.lista_MovBanco.Add(obj)

        P_lista_MovBancoView = Me.lista_MovBanco

        Me.GV_Remesas.DataSource = Me.lista_MovBanco
        Me.GV_Remesas.DataBind()

        Me.GV_ChequeRef.DataBind()
        Me.GV_DocumentoRef.DataBind()

        Me.Lista_ChequeRef = New List(Of Entidades.Cheque)
        Me.setLista_ChequeRef(Me.Lista_ChequeRef)

        Me.Lista_DocumentoRef = New List(Of Entidades.Documento)
        Me.setLista_DocumentoRef(Me.Lista_DocumentoRef)

    End Sub

    Private Function validar_AgregarMovBanco() As Decimal
        Dim Valor As Decimal = 0
        Dim TipoCambio As Decimal = 0 : If IsNumeric(Me.tb_TipoCambio.Text) Then TipoCambio = CDec(Me.tb_TipoCambio.Text)
        Dim Monto As Decimal = 0 : If IsNumeric(Me.tb_monto.Text) Then Monto = CDec(Me.tb_monto.Text)
        Dim ConvertirMonedaBase As Boolean = False

        Dim objCuentaBancaria As Entidades.CuentaBancaria = (New Negocio.CuentaBancaria).SelectxIdBancoxIdCuentaBancaria(CInt(Me.ddl_Banco.SelectedValue), CInt(Me.ddl_CuentaBancaria.SelectedValue))
        If CInt(Me.ddl_Moneda.SelectedValue) <> objCuentaBancaria.IdMoneda Then
            Dim ListaMoneda As List(Of Entidades.Moneda) = (New Negocio.Moneda).SelectxId(CInt(objCuentaBancaria.IdMoneda))
            If ListaMoneda(0).Base Then ConvertirMonedaBase = True
        Else
            TipoCambio = 1
        End If

        If ConvertirMonedaBase Then
            Valor = Monto * TipoCambio
        Else
            Valor = Monto / TipoCambio
        End If

        Return Valor

    End Function

    Private Sub GV_Remesas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Remesas.SelectedIndexChanged
        Me.lista_MovBanco = Me.P_lista_MovBancoView

        Me.lista_MovBanco.RemoveAt(Me.GV_Remesas.SelectedRow.RowIndex)

        Me.P_lista_MovBancoView = Me.lista_MovBanco

        Me.GV_Remesas.DataSource = Me.lista_MovBanco
        Me.GV_Remesas.DataBind()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        OnClick_Guardar()
    End Sub
    Private Sub OnClick_Guardar()

        Dim objRemesas As New Negocio.Remesas

        Try

            Me.lista_MovBanco = Me.P_lista_MovBancoView

            Dim objDocumento As Entidades.Documento = Obtener_Documento()

            Select Case CInt(Me.hddFrmModo.Value)

                Case operativo.Nuevo

                    objRemesas.Insert(objDocumento, lista_MovBanco)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case operativo.Editar

                    objRemesas.Editar(objDocumento, lista_MovBanco)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")

            End Select




            objScript.mostrarMsjAlerta(Me, "Se Guardó Correctamente")

            verFrm(operativo.Registrar_Exito, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function Obtener_Documento() As Entidades.Documento

        Dim obj As New Entidades.Documento
        With obj

            Select Case CInt(Me.hddFrmModo.Value)
                Case operativo.Nuevo
                    .Id = Nothing
                Case operativo.Editar
                    .Id = CInt(hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.ddl_Empresa.SelectedValue)
            .IdTienda = CInt(Me.ddl_Tienda.SelectedValue)
            .IdCaja = CInt(Me.ddl_caja.SelectedValue)
            .IdTipoOperacion = CInt(Me.ddl_TipoOperacion.SelectedValue)
            .IdTipoDocumento = IdTipoDocumento
            .IdSerie = CInt(Me.ddl_Serie.SelectedValue)
            .Serie = CStr(Me.ddl_Serie.SelectedItem.Text)
            .IdEstadoDoc = 1 ' ************** ACTIVO
            .FechaEmision = CDate(Me.tb_FechaEmision.Text)
            .FechaAEntregar = CDate(Me.tb_FechaCaja.Text)
            .IdUsuario = CInt(Session("IdUsuario"))

        End With

        Return obj

    End Function

    Private Sub btnBuscarCajaCierre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarCajaCierre.Click
        cargar_CajaAperturaCierre()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        OnClick_Buscar()
    End Sub
    Private Sub OnClick_Buscar()
        verFrm(operativo.Buscar, False, False)
        cargar_MovBanco()
    End Sub

    Private Sub cargar_MovBanco()

        Dim filtros As New Entidades.MovBancoView

        With filtros
            .IdBanco = CInt(Me.ddl_Banco.SelectedValue)
            .IdCuentaBancaria = CInt(IIf(Me.ddl_CuentaBancaria.SelectedValue = "", 0, Me.ddl_CuentaBancaria.SelectedValue))
            .NroOperacion = Me.tb_NroOperacion.Text.Trim
            .FechaMov = CDate(Me.tb_FechaMov.Text)
        End With

        ' Me.dgvMovBanco.DataSource = (New Negocio.Remesas).SelectxParams(filtros)
        ' Me.dgvMovBanco.DataBind()

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        OnClick_Nuevo()
    End Sub
    Private Sub OnClick_Nuevo()
        verFrm(operativo.Nuevo, True, True)
    End Sub

    Private Sub GenerarCodigoDocumento()
        If (IsNumeric(Me.ddl_Serie.SelectedValue)) Then
            Me.tb_NroDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.ddl_Serie.SelectedValue))
        Else
            Me.tb_NroDocumento.Text = ""
        End If

    End Sub


#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Dim Lista_DocumentoRef As List(Of Entidades.Documento)

    Private Function getLista_DocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setLista_DocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdEmpresa As Integer = 0
            Dim IdPersona As Integer = 0

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select


            ''*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.ddl_Empresa.SelectedValue), CInt(Me.ddl_Tienda.SelectedValue), IdPersona, IdTipoDocumento, serie, codigo, CInt(Me.ddl_TipoDocumentoRef.SelectedValue), fechaInicio, fechafin, False, False, CInt(Me.ddl_TipoOperacion.SelectedValue))

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub
    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            Me.Lista_DocumentoRef = getLista_DocumentoRef()
            Me.Lista_DocumentoRef.Add(objDocumento)
            setLista_DocumentoRef(Me.Lista_DocumentoRef)

            Me.GV_DocumentoRef.DataSource = Me.Lista_DocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.ddl_Moneda.SelectedValue = CStr(objDocumento.IdMoneda)
            Me.tb_monto.Text = CStr(Math.Round(objDocumento.TotalAPagar, 3))

            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.Lista_DocumentoRef = getLista_DocumentoRef()

        For i As Integer = 0 To Me.Lista_DocumentoRef.Count - 1

            If (Me.Lista_DocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Me.lista_MovBanco = P_lista_MovBancoView

        For i As Integer = 0 To Me.lista_MovBanco.Count - 1

            If (Me.lista_MovBanco(i).IdDocumentoRef = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.Lista_DocumentoRef = getLista_DocumentoRef()

            Me.Lista_DocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setLista_DocumentoRef(Me.Lista_DocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.Lista_DocumentoRef
            Me.GV_DocumentoRef.DataBind()


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region


#Region "*********************** BUSQUEDA DE CHEQUES"

    Dim Lista_ChequeRef As List(Of Entidades.Cheque)

    Private Function getLista_ChequeRef() As List(Of Entidades.Cheque)
        Return CType(Session.Item("listaChequeRef"), List(Of Entidades.Cheque))
    End Function
    Private Sub setLista_ChequeRef(ByVal lista As List(Of Entidades.Cheque))
        Session.Remove("listaChequeRef")
        Session.Add("listaChequeRef", lista)
    End Sub

    Private Sub Img_BuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_BuscarDocumento.Click
        OnClick_BuscarDocumento(CInt(Me.ddl_Serie.SelectedValue), CInt(Me.hddNroDocumento.Value), 0)
    End Sub
    Private Sub OnClick_BuscarDocumento(ByVal IdSerie As Integer, ByVal NroDocumento As Integer, ByVal IdDocumento As Integer)
        Try
            Dim ObjDocumento As Entidades.Documento = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectCab(IdSerie, NroDocumento, 0)

            Dim ListaMovBanco As List(Of Entidades.MovBancoView) = (New Negocio.MovBancoView).SelectxIdDocumento(ObjDocumento.Id)

            Cargar_DocumentoRemesa_GUI(ObjDocumento, ListaMovBanco)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub Cargar_DocumentoRemesa_GUI(ByVal obj As Entidades.Documento, ByVal ListaMovBanco As List(Of Entidades.MovBancoView))

        verFrm(operativo.Inicio, True, True)

        With obj

            Me.hddIdDocumento.Value = CStr(.Id)
            Me.tb_NroDocumento.Text = CStr(.Codigo)
            Me.tb_FechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            Me.tb_FechaCaja.Text = Format(.FechaAEntregar, "dd/MM/yyyy")

            If Not IsNothing(Me.ddl_TipoOperacion.Items.FindByValue(CStr(.IdTipoOperacion))) Then
                Me.ddl_TipoOperacion.SelectedValue = CStr(.IdTipoOperacion)
            End If

            If Not IsNothing(Me.ddl_caja.Items.FindByValue(CStr(.IdCaja))) Then
                Me.ddl_caja.SelectedValue = CStr(.IdCaja)
            End If

            If Not IsNothing(Me.ddl_EstadoDoc.Items.FindByValue(CStr(.IdEstadoDoc))) Then
                Me.ddl_EstadoDoc.SelectedValue = CStr(.IdEstadoDoc)
            End If

            '************************* MOV BANCO

            For i As Integer = 0 To ListaMovBanco.Count - 1
                If ListaMovBanco(i).IdCheque > 0 Then
                    Dim objCheque As Entidades.Cheque = (New Negocio.Cheque).Cheque_SelectxIdCheque(ListaMovBanco(i).IdCheque)
                    ListaMovBanco(i).Referencia = "CHEQUE " + objCheque.Banco + " " + objCheque.Numero

                End If
                If ListaMovBanco(i).IdDocumentoRef2 > 0 Then

                    Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectActivoxIdDocumento(ListaMovBanco(i).IdDocumentoRef2)
                    ListaMovBanco(i).Referencia = "DOCUMENTO " + objDocumento.NomTipoDocumento + " " + objDocumento.Serie + "-" + objDocumento.Codigo

                End If

            Next

            Me.GV_Remesas.DataSource = ListaMovBanco
            Me.GV_Remesas.DataBind()

            Me.P_lista_MovBancoView = ListaMovBanco

            verFrm(operativo.Buscar_Exito, False, False)

        End With
    End Sub


    Private Sub btnBuscarCheque_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarCheque.Click
        OnClick_BuscarCheque()
    End Sub

    Private Sub OnClick_BuscarCheque()
        Try

            Me.GV_Cheque.DataSource = (New Negocio.Cheque).Cheque_SelectxParams_ValBanco(Nothing, Nothing, Nothing, Nothing, Nothing, CDate(Me.tb_ChequeFechaIni.Text), CDate(Me.tb_ChequeFechaFin.Text), Nothing, Nothing, CInt(Me.ddl_Empresa.SelectedValue), CInt(Me.ddl_Tienda.SelectedValue), True, Nothing)
            Me.GV_Cheque.DataBind()

            If (Me.GV_Cheque.Rows.Count <= 0) Then
                Throw New Exception("NO SE HALLARON REGISTROS")
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "  onCapa('capaCheques');  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Cheque_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Cheque.SelectedIndexChanged
        OnChange_Cheque(Me.GV_Cheque.SelectedRow.RowIndex)
    End Sub
    Private Sub OnChange_Cheque(ByVal Index As Integer)
        Try

            Dim IdCheque As Integer = CInt(CType(GV_Cheque.Rows(Index).FindControl("hddIdCheque"), HiddenField).Value)

            validar_AddChequeRef(IdCheque)

            Dim objCheque As Entidades.Cheque = (New Negocio.Cheque).Cheque_SelectxIdCheque(IdCheque)

            Me.Lista_ChequeRef = getLista_ChequeRef()
            Me.Lista_ChequeRef.Add(objCheque)
            setLista_ChequeRef(Me.Lista_ChequeRef)

            With objCheque

                If Not IsNothing(Me.ddl_Banco.Items.FindByValue(CStr(.IdBanco))) Then
                    Me.ddl_Banco.SelectedValue = CStr(.IdBanco)
                    OnChange_Banco()
                End If

                If Not IsNothing(Me.ddl_Moneda.Items.FindByValue(CStr(.IdMoneda))) Then
                    Me.ddl_Moneda.SelectedValue = CStr(.IdMoneda)
                End If

                Me.tb_monto.Text = CStr(Math.Round(.Monto, 3))

            End With

            Me.GV_ChequeRef.DataSource = Me.Lista_ChequeRef
            Me.GV_ChequeRef.DataBind()

            Me.GV_Cheque.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function validar_AddChequeRef(ByVal IdChequeRef As Integer) As Boolean

        Me.Lista_ChequeRef = getLista_ChequeRef()

        For i As Integer = 0 To Me.Lista_ChequeRef.Count - 1

            If (Me.Lista_ChequeRef(i).IdCheque = IdChequeRef) Then
                Throw New Exception("EL CHEQUE SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next


        Me.lista_MovBanco = P_lista_MovBancoView

        For i As Integer = 0 To Me.lista_MovBanco.Count - 1

            If (Me.lista_MovBanco(i).IdCheque = IdChequeRef) Then
                Throw New Exception("EL CHEQUE SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function

    Private Sub GV_ChequeRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_ChequeRef.SelectedIndexChanged

        OnChange_ChequeRef(Me.GV_ChequeRef.SelectedRow.RowIndex)

    End Sub

    Private Sub OnChange_ChequeRef(ByVal Index As Integer)
        Try

            Me.Lista_ChequeRef = getLista_ChequeRef()
            Me.Lista_ChequeRef.RemoveAt(Index)

            Me.setLista_ChequeRef(Me.Lista_ChequeRef)

            Me.GV_ChequeRef.DataSource = Me.Lista_ChequeRef
            Me.GV_ChequeRef.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        OnClick_Anular()
    End Sub
    Private Sub OnClick_Anular()
        Try

            If ((New Negocio.Remesas).Anular(CInt(Me.hddIdDocumento.Value))) Then

                verFrm(operativo.Anular_Exito, False, False)

                objScript.mostrarMsjAlerta(Me, "Documento anulado con éxito.")

            Else

                Throw New Exception("Problemas en la Operación.")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        OnClick_Editar()
    End Sub
    Private Sub OnClick_Editar()
        Try

            Me.lista_MovBanco = P_lista_MovBancoView

            Me.GV_Remesas.DataSource = P_lista_MovBancoView
            Me.GV_Remesas.DataBind()

            verFrm(operativo.Editar, False, False)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub
    Private Sub busquedaAvanzado()

        Try

            Dim IdSerie As Integer = CInt(Me.ddl_Serie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(IdTipoDocumento, IdSerie, 0, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try

            OnClick_BuscarDocumento(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

End Class
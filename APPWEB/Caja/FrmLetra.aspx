<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmLetra.aspx.vb" Inherits="APPWEB.FrmLetra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" OnClientClick="return(   valOnClickNuevo()  );" Width="80px"
                                runat="server" Text="Nuevo" ToolTip="Nuevo" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" OnClientClick="return(  valOnClickEditar()  );" Width="80px"
                                runat="server" Text="Editar" ToolTip="Editar" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" OnClientClick="return(  valOnClickAnular()   );" Width="80px"
                                runat="server" Text="Anular" ToolTip="Anular" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( valSaveDocumento()  );" Width="80px"
                                runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                            <asp:Button ID="btnRenegociar" OnClientClick="return(  mostrarCapaRenegociar()  );"
                                Width="90px" runat="server" Text="Renovar" ToolTip="Renovar Letra" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" OnClientClick="return(  valOnClick_btnImprimir_Principal()  );"
                                Width="80px" runat="server" Text="Imprimir" ToolTip="Imprimir Documento" />
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 100%" align="right">
                            <table>
                                <tr>
                                    <td class="Texto">
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                LETRA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cabecera" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" align="right">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" align="right">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" Width="100%" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel4" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">                                
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" ReadOnly="true"
                                    onKeypress="return( valOnKeyPressCodigoDoc(this,event)  );" Width="100px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Documento por [ Serie - N�mero ]." OnClientClick="return( valOnClickBuscarDocumentoxCodigo()  );"
                                    Width="16px" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="Texto" align="right">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList Font-Bold="true" Width="100%" ID="cboMoneda" onClick="return( valOnClickMoneda()  );"
                                    runat="server" CssClass="LabelRojo" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" align="right">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DATOS DEL CLIENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" Enabled="true" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" OnClientClick="return( mostrarCapaBuscarPersona('0')   );" CssClass="btnBuscar"
                                    runat="server" Text="Buscar" ToolTip="Buscar Cliente" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNI" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="TituloCelda">
            <td>
                DOCUMENTO DE REFERENCIA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DocReferencia" runat="server">
                    <table class="style1">
                        <tr>
                            <td>
                                <asp:Button ID="btnAddDocumentoReferencia" OnClientClick="return(   valOnClickAddDocRef_Show()   );"
                                    Width="200px" runat="server" Text="Agregar Documento Referencia" ToolTip="Agregar Documento de Venta al cual hacer referencia." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocReferencia" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="20px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnQuitarDocReferencia" OnClick="btnQuitarDocReferencia_Click"
                                                    ToolTip="Quitar Documento de Referencia" ImageUrl="~/Imagenes/Eliminar1.gif"
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdDocumentoReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                            <asp:HiddenField ID="hddIdMovCuentaReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCuenta")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="getFechaVctoText" NullDisplayText="---" DataFormatString="{0:dd/MM/yyyy}"
                                            HeaderText="Fec. Vcto." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                                            <asp:Label ID="lblMonedaMontoTotal" Font-Bold="true" ForeColor="Red" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="lblMontoTotal" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoTotal","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                                            <asp:Label ID="lblMonedaSaldo" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="lblSaldo" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Abono" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                                            <asp:Label ID="lblMonedaNuevoSaldo" Font-Bold="true" ForeColor="Red" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:TextBox ID="txtSaldo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NuevoSaldo","{0:F2}")%>'
                                                                onKeyUp=" return ( calcularTotalDocRef() ); " onKeypress="return(   validarNumeroPuntoPositivo(event)   );"
                                                                onblur="return( valBlur(event)  );" onFocus="return(  aceptarFoco(this) );"></asp:TextBox>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Total:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotalDocRef" runat="server" Text="" CssClass="LabelRojo"
                                                Font-Bold="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTotalDocRef" onFocus="return(  valOnFocus_TotalMonto_DocRef()   );"
                                                onKeypress="return( false );" Text="0" CssClass="TextBox_ReadOnly" runat="server"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                LETRAS
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Letras" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnAddLetra" OnClientClick="return(  valOnClickAddLetra()  );" runat="server"
                                                Text="Agregar Letra" ToolTip="Agregar Nueva Letra" />
                                        </td>
                                        <td class="Texto" >
                                                Nro de Letras:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNroLetra" runat="server" Font-Bold="true" onKeypress="return(   validarNumeroPuntoPositivo(event)   );" onblur="return( valBlur(event)  );">1</asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <asp:GridView ID="GV_Letras" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="20px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnQuitarLetra" OnClientClick="return(   valOnClickQuitarLetra()   );"
                                                    OnClick="btnQuitarLetra_Click" ToolTip="Quitar Letra" ImageUrl="~/Imagenes/Eliminar1.gif"
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:TextBox ID="txtMonto" TabIndex="200" onKeyup="return(   calcularMontoTotal()   );"
                                                                onKeypress="return(   validarNumeroPuntoPositivo(event)   );" onblur="return( valBlur(event)  );"
                                                                onFocus="return(  aceptarFoco(this) );" Width="90px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:TextBox>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Vcto." HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                                            <asp:TextBox ID="txtFechaVcto" TabIndex="220" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaVenc","{0:dd/MM/yyyy}")%>'
                                                                onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>

                                                            <cc1:MaskedEditExtender ID="txtFechaVcto_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                                            </cc1:MaskedEditExtender>
                                                            <cc1:CalendarExtender ID="txtFechaVcto_CalendarExtender" runat="server" Enabled="True"
                                                                Format="dd/MM/yyyy" TargetControlID="txtFechaVcto">
                                                            </cc1:CalendarExtender>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Observaciones" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                                            <asp:Label ID="lblNroDocumento" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                            <asp:ImageButton ID="btnBuscarDoc" ToolTip="Buscar Documento Salteado" OnClientClick="return(  onClick_DocumentoSalteado(this)  );"
                                                                runat="server" ImageUrl="~/Caja/iconos/quick-link1.jpg" Width="25px" Height="25px" />

                                                            <asp:TextBox ID="txtObservaciones" TabIndex="240" Width="600px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Observaciones")%>'></asp:TextBox>

                                                            <asp:HiddenField ID="hddIdDocumentoLetra" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Total:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaMontoTotal" runat="server" Text="" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMontoTotal" onFocus="return(  valOnFocus_TotalMonto_Letras()   );"
                                                onKeypress="return( false );" Text="0" CssClass="TextBox_ReadOnly" runat="server"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                AGENCIA BANCARIA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Banco" runat="server">
                    <table>
                        <tr>
                            <td class="Texto">
                                Banco:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Oficina:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboOficina" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Nro. Cuenta:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboNroCuenta" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            En Cartera:
                                        </td>
                                        <td style="width: 60px">
                                            <asp:CheckBox ID="chbEnCartera" runat="server" Enabled="false" />
                                        </td>
                                        <td class="Texto">
                                            En Cobranza:
                                        </td>
                                        <td style="width: 60px">
                                            <asp:CheckBox ID="chbEnCobranza" runat="server" Enabled="false" />
                                        </td>
                                        <td class="Texto">
                                            Abono en Cuenta
                                        </td>
                                        <td style="width: 60px">
                                            <asp:CheckBox ID="chbDC" runat="server" Enabled="false" />
                                        </td>
                                        <td class="Texto">
                                            Renovado:
                                        </td>
                                        <td style="width: 60px">
                                            <asp:CheckBox ID="chbRenegociado" runat="server" Enabled="false" />
                                        </td>
                                        <td class="Texto">
                                            Protestado:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chbProtestado" runat="server" Enabled="false" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DATOS DEL AVAL
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Aval" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionAval" ReadOnly="true" Enabled="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarAval" OnClientClick="return( mostrarCapaBuscarPersona('1')  );"
                                    runat="server" Text="Buscar" ToolTip="Buscar Cliente" CssClass="btnBuscar" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNIAval" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUCAval" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdAval" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdTipoDocumento" Value="20" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddContAmortizaciones" runat="server" />
                <asp:HiddenField ID="hddOpcionBuscarPersona" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddIndexLetra" runat="server" />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btBuscarPersonaGrilla">                                                                        
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboRol" runat="server" Width="100%">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 261px; left: 42px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Button ID="btnAddDocumentoReferencia_Find" OnClientClick="return( valOnClickAddDocRef()  );"
                        runat="server" Text="Adicionar" ToolTip="Adicionar Documento de Referencia" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chbAddDocumentoRef_Find" runat="server" onClick="return(  valOnClickSelectDocRef(this)  );" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"getNroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                <asp:HiddenField ID="hddIdMovCuentaReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCuenta")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="getFechaEmisionText" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderText="Fec. Emisi�n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="getFechaVctoText" NullDisplayText="---" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderText="Fec. Vcto." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMonedaMontoTotal_Find" Font-Bold="true" ForeColor="Red" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblMontoTotal_Find" Font-Bold="true" ForeColor="Red" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"MontoTotal","{0:F2}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMonedaSaldo_Find" Font-Bold="true" ForeColor="Red" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblSaldo_Find" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F2}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="DivDocRef" style="border: 3px solid blue; padding: 2px; width: 950px; height: auto;
        position: absolute; background-color: white; z-index: 2; display: none; top: 100px;
        left: 15px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('DivDocRef')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoDocumentoRef" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocRef" runat="server" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Vcto."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblImporteMoneda" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblImporte" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaImprimir" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 261px; left: 42px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaImprimir')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Imprimir" runat="server" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_Print" ForeColor="Red" Font-Bold="true" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"getNroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_Print" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="getFechaEmisionText" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderText="Fec. Emisi�n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="getFechaVctoText" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Vcto"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Monto Total" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaMontoTotal_Print" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMontoTotal_Print" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="btnImprimir" OnClientClick="return(  valOnClick_btnImprimir(this)   );"
                                                    runat="server" ImageUrl="~/Imagenes/Print.ico" ToolTip="Imprimir Documento." />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaRenegociar" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 262px; left: 304px;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaRenegociar')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                N� de Letras por la cual Renovar:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCantidadLetrasxRenegociar" Width="70px" CssClass="TextBox_ReadOnlyLeft"
                                    onblur="return(  valBlurClear('0',event)  );" onKeypress="return( onKeyPressEsNumero('event')  );"
                                    runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnAceptarRenegociar" OnClientClick="return( valOnClickAceptarRenegociar()  );"
                                                Width="80px" runat="server" Text="Aceptar" ToolTip="Aceptar el n�mero de Letras por la cual Renovar." />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancelarRenegociar" OnClientClick="return(  offCapa('capaRenegociar')  );"
                                                Width="80px" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"  CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%" AllowPaging="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Destinatario" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al destinatario seleccionado.
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        //************************** BUSQUEDA PERSONA
        //***************************************************** BUSQUEDA PERSONA
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
		

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function valOnFocus_Cliente() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }
        //*********************************************************************************** FIN BUSCAR CLIENTE

        function calcularMontoTotal() {
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            var grilla = document.getElementById('<%=GV_Letras.ClientID %>');
            var montoTotal = 0;
            if (grilla != null) {
                var init = 1;
                if (parseInt(hddFrmModo.value) == 7) {  // RENEGOCIAR
                    init = getIniGvLetra(); // NO consideramos el monto del doc. letra x renegociar
                }
                for (var i = init; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    
                    var txtMonto = rowElem.cells[1].children[0];
                    var monto = 0;
                    if (!isNaN(txtMonto.value) && parseFloat(txtMonto.value) > 0) {
                        monto = parseFloat(txtMonto.value);
                    }
                    montoTotal = montoTotal + monto;
                }
            }
            //******** Imprimo los valores
            document.getElementById('<%=txtMontoTotal.ClientID %>').value = Format(montoTotal, 2);
            return false;
        }

        function getIniGvLetra() {
            var iniciar = 1;
            var IdDocumento = 0;
            var grilla = document.getElementById('<%=GV_Letras.ClientID %>');
            var montoTotal = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    IdDocumento = parseInt(rowElem.cells[3].children[4].value);
                    if (isNaN(IdDocumento)) { IdDocumento = 0; }
                    if (IdDocumento <= 0) {
                        iniciar = i;
                        break;
                    }
                }
            }            
            return iniciar;
        }
        

        function calcularTotalDocRef() {
            var grilla = document.getElementById('<%=GV_DocReferencia.ClientID%>');
            var totalDocRef = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    totalDocRef = totalDocRef + parseFloat(rowElem.cells[7].children[1].value);
                }
            }
            document.getElementById('<%=txtTotalDocRef.ClientID%>').value = Format(totalDocRef, 2);
            return false;
        }

        function valOnFocus_Aval() {
            document.getElementById('<%=btnBuscarAval.ClientID%>').focus();
            return false;
        }
        function valOnFocus_Cliente() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }
        function valOnFocus_TotalMonto_Letras() {
            document.getElementById('<%=btnAddLetra.ClientID%>').focus();
            return false;
        }
        function valOnFocus_TotalMonto_DocRef() {
            document.getElementById('<%=btnAddDocumentoReferencia.ClientID%>').focus();
            return false;
        }

        function valOnClickAddDocRef_Show() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('DEBE SELECCIONAR UN CLIENTE. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            return true;
        }
        function valSaveDocumento() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0 || parseInt(cboEmpresa.value) <= 0) {
                alert('Debe seleccionar una Empresa.');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0 || parseInt(cboTienda.value) <= 0) {
                alert('Debe seleccionar una Tienda.');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0 || parseInt(cboMoneda.value) <= 0) {
                alert('Debe seleccionar una Moneda de Emisi�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstado.value)) || cboEstado.value.length <= 0 || parseInt(cboEstado.value) <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }

            var grillaDocRef = document.getElementById('<%=GV_DocReferencia.ClientID%>');
            if (grillaDocRef == null) {
                alert('Debe Ingresar Documentos a los cuales hacer referencia.');
                return false;
            }

            /*var cboBanco = document.getElementById('<%=cboBanco.ClientID%>');
            if (isNaN(parseInt(cboBanco.value)) || cboBanco.value.length <= 0 || parseInt(cboBanco.value) <= 0) {
            alert('Debe seleccionar un Banco.');
            return false;
            }*/

            //************ Validamos las LETRAS
            var grillaLetras = document.getElementById('<%=GV_Letras.ClientID%>');
            if (grillaLetras == null) {
                alert('Debe ingresar al menos una Letra.');
                return false;
            }



            var grilla = document.getElementById('<%=GV_Letras.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var txtMonto = rowElem.cells[1].children[1];
                    if (isNaN(parseFloat(txtMonto.value)) || txtMonto.value.length <= 0 || parseFloat(txtMonto.value) <= 0) {
                        alert('El Monto debe ser un valor mayor a cero.');
                        txtMonto.select();
                        txtMonto.focus();
                        return false;
                    }

                }


                //*********** VALIDAMOS QUE LAS FECHAS DE VCTO NO SE REPITAN
                for (var i = 1; i < grilla.rows.length - 1; i++) {//comienzo en 1 xq 0 es la cabecera
                    for (var j = i + 1; j < grilla.rows.length; j++) {//comienzo en 1 xq 0 es la cabecera
                        if (grilla.rows[i].cells[2].children[0].value == grilla.rows[j].cells[2].children[0].value) {
                            alert('La Fecha de Vcto [ ' + grilla.rows[i].cells[2].children[0].value + ' ] se repite. Las Fechas de Vencimiento deben tener diferentes valores.');
                            return false;
                        }
                    }
                }
            }

            var txtTotalLetras = document.getElementById('<%=txtMontoTotal.ClientID%>');
            if (isNaN(parseFloat(txtTotalLetras.value)) || txtTotalLetras.value.length <= 0 || parseFloat(txtTotalLetras.value) <= 0) {
                alert('El Monto Total de las Letras debe ser un valor Mayor a CERO.');
                return false;
            }


            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 1) {  //**** Solo cuando es NUEVO
                var txtTotalDocRef = document.getElementById('<%=txtTotalDocRef.ClientID%>');
                if (parseFloat(txtTotalLetras.value) < parseFloat(txtTotalDocRef.value)) {
                    return confirm('EL SALDO TOTAL DE LOS DOCUMENTOS DE REFERENCIA ES MAYOR AL MONTO TOTAL DE LAS LETRAS. Desea continuar con la Operaci�n ?');
                }
            }
            if (parseInt(hddFrmModo.value) == 7) {  //***** RENEGOCIAR
                var grilla = document.getElementById('<%=GV_Letras.ClientID%>');
                if (grilla != null) {
                    var txtMontoLetraxRenegociar = grilla.rows[1].cells[1].children[1];
                    var montoLetrasxCanjear = 0;
                    for (var i = 2; i < grilla.rows.length; i++) {//comienzo en 2 xq sumamos solo los doc. letras canjeadas
                        var rowElem = grilla.rows[i];
                        var txtMonto = rowElem.cells[1].children[1];
                        montoLetrasxCanjear = montoLetrasxCanjear + parseFloat(txtMonto.value);
                    }
                    if (parseFloat(txtMontoLetraxRenegociar.value) > montoLetrasxCanjear) {
                        return confirm('El Monto de la Letra a Renovar [ ' + txtMontoLetraxRenegociar.value + ' ] es MAYOR al monto total de las nuevas letras [ ' + montoLetrasxCanjear + ' ]. Desea continuar de todas formas con la Operaci�n ?');
                    }
                }
            }

            return confirm('Desea continuar con la Operaci�n ?');
        }


        function valOnClickNuevo() {

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 0 || parseInt(hddFrmModo.value) == 4 || parseInt(hddFrmModo.value) == 5 || parseInt(hddFrmModo.value) == 6) {
                return true;
            }

            var grillaDocRef = document.getElementById('<%=GV_DocReferencia.ClientID%>');
            if (grillaDocRef == null) {
                return true;
            }

            return (confirm('Est� seguro que desea generar un NUEVO Documento ?'));
        }


        function valOnClickBuscar() {

            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');

            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }

        function valOnClickSelectDocRef(chbSelectDocRef) {
            //********* VALIDAMOS LA SELECCION DEL DOC REFERENCIA NO ESTE REPETIDO
            var grilla = document.getElementById('<%=GV_DocumentosReferencia_Find.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var chb = rowElem.cells[0].children[0];
                    if (chb.id == chbSelectDocRef.id) {

                        //**** Obtenemos el IdDocumento a agregar
                        var hddIdDocumentoRef_Find = rowElem.cells[2].children[1];

                        var grilla_DocRef = document.getElementById('<%=GV_DocReferencia.ClientID%>');
                        if (grilla_DocRef != null) {
                            for (var j = 1; j < grilla_DocRef.rows.length; j++) {//comienzo en 1 xq 0 es la cabecera
                                var rowElem_DocRef = grilla_DocRef.rows[j];

                                //**** Obtenemos el IdDocumento a�adido
                                var hddIdDocumentoRef = rowElem_DocRef.cells[2].children[1];
                                if (parseInt(hddIdDocumentoRef_Find.value) == parseInt(hddIdDocumentoRef.value)) {
                                    alert('Este Documento ya ha sido agregado como Documento de Referencia.');
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                }
            }
            return true;
        }


        function valOnClickAddDocRef() {

            var grilla = document.getElementById('<%=GV_DocumentosReferencia_Find.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var chb = rowElem.cells[0].children[0];
                    if (chb.status == true) {
                        return true;
                    }
                }
            }
            offCapa('capaDocumentosReferencia');
            return false;
        }

        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }

            var chbRenegociado = document.getElementById('<%=chbRenegociado.ClientID%>');
            if (chbRenegociado.status == true) {
                alert('El Documento Letra ha sido RENOVADO. No se permite su edici�n.');
                return false;
            }

            var hddContAmortizaciones = document.getElementById('<%=hddContAmortizaciones.ClientID%>');
            if (parseInt(hddContAmortizaciones.value) > 0) {  //***** CANCELADO
                alert('El Documento posee AMORTIZACIONES [ ABONOS ]. No se permite su edici�n.');
                return false;
            }
            return true;
        }


        function valOnClickAddLetra() {
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 7) {  //**** RENEGOCIAR
                alert('No se permite a�adir una Letra en el Proceso de RENOVACI�N.');
                return false;
            }
            if (parseInt(hddFrmModo.value) == 2) {  //**** EDITAR
                alert('No se permite a�adir una Letra en el Proceso de EDICI�N.');
                return false;
            }
            return true;
        }

        function valOnClickQuitarLetra() {
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 7) {  //**** RENEGOCIAR
                alert('No se permite quitar una Letra en el Proceso de RENOVACI�N.');
                return false;
            }
            if (parseInt(hddFrmModo.value) == 2) {  //**** EDITAR
                alert('No se permite quitar la Letra en el Proceso de EDICI�N.');
                return false;
            }
            return true;
        }
        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            var chbRenegociado = document.getElementById('<%=chbRenegociado.ClientID%>');
            if (chbRenegociado.status) {
                alert('El Documento Letra tiene un estado [ RENOVADO ]. No se permite su Anulaci�n.');
                return false;
            }
            var hddContAmortizaciones = document.getElementById('<%=hddContAmortizaciones.ClientID%>');
            if (parseInt(hddContAmortizaciones.value) > 0) {  //***** CANCELADO
                alert('El Documento posee AMORTIZACIONES [ ABONOS ]. No se permite su Anulaci�n.');
                return false;
            }

            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }

        function valOnClickAceptarRenegociar() {
            var txtCantidadLetrasxRenegociar = document.getElementById('<%=txtCantidadLetrasxRenegociar.ClientID%>');
            if (isNaN(parseInt(txtCantidadLetrasxRenegociar.value)) || txtCantidadLetrasxRenegociar.value.length <= 0 || parseInt(txtCantidadLetrasxRenegociar.value) <= 0) {
                alert('Ingrese un valor v�lido mayor a cero.');
                txtCantidadLetrasxRenegociar.select();
                txtCantidadLetrasxRenegociar.focus();
                return false;
            }
            return true;
        }
        function mostrarCapaRenegociar() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Renovaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su Renovaci�n.');
                return false;
            }
            var chbRenegociado = document.getElementById('<%=chbRenegociado.ClientID%>');
            if (chbRenegociado.status) {
                alert('El Documento Letra ya tiene un estado [ RENOVADO ]. No se permite su Renovaci�n.');
                return false;
            }
            var hddContAmortizaciones = document.getElementById('<%=hddContAmortizaciones.ClientID%>');
            if (parseInt(hddContAmortizaciones.value) > 0) {  //***** CANCELADO
                alert('El Documento posee AMORTIZACIONES [ ABONOS ]. No se permite su Renovaci�n.');
                return false;
            }

            onCapa('capaRenegociar');
            var txtCantidadLetrasxRenegociar = document.getElementById('<%=txtCantidadLetrasxRenegociar.ClientID%>');
            txtCantidadLetrasxRenegociar.value = '0';
            txtCantidadLetrasxRenegociar.select();
            txtCantidadLetrasxRenegociar.focus();
            return false;
        }

        function calcularTotales() {
            calcularMontoTotal();
            calcularTotalDocRef();

            return false;
        }
        function valOnClickMoneda() {
            var grillaDocRef = document.getElementById('<%=GV_DocReferencia.ClientID%>');
            if (grillaDocRef != null) {
                alert('Para cambiar la Moneda de Emisi�n debe quitar todos los Documentos de Referencia.');
                return false;
            }
            //            var grillaLetras = document.getElementById('<%=GV_Letras.ClientID%>');
            //            if (grillaLetras != null) {
            //                alert('Para cambiar la Moneda de Emisi�n debe quitar todas las Letras.');
            //                return false;
            //            }
            return true;
        }

        function mostrarCapaImpresion() {
            onCapa('capaImprimir');
            return false;
        }

        function valOnClick_btnImprimir(boton) {
            var grilla = document.getElementById('<%=GV_Imprimir.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var hddIdDocumento = rowElem.cells[1].children[1];
                    var btnImprimir = rowElem.cells[5].children[0];
                    if (btnImprimir.id == boton.id) {
                        window.open('../../Caja/Reportes/Visor1.aspx?iReporte=6&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                        return false;
                    }



                }
            }
            alert('NO SE PUDO UBICAR EL DOCUMENTO DE IMPRESI�N.');
            return false;

        }
        function valOnClick_btnImprimir_Principal() {
            var grilla = document.getElementById('<%=GV_Imprimir.ClientID%>');
            if (grilla != null) {
                mostrarCapaImpresion();
            } else {
                var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
                window.open('../../Caja/Reportes/Visor1.aspx?iReporte=6&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            }
            return false;
        }
        function mostrarCapaBuscarPersona(opcion) {
            var hddOpcionBuscarPersona = document.getElementById('<%=hddOpcionBuscarPersona.ClientID%>');
            hddOpcionBuscarPersona.value = opcion;
            mostrarCapaPersona();
            return false;
        }
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cabecera.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');

        }

        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////


        function onClick_DocumentoSalteado(boton) {
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');

            if (parseInt(hddFrmModo.value) == 2 || parseInt(hddFrmModo.value) == 7) {
                alert('NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            
            var grilla = document.getElementById('<%=GV_Letras.ClientID%>');
            var index = -1;
            var glosa = '';
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[1].id == boton.id) {
                        index = (i - 1);
                        break;
                    }
                }
            }
            if (index < 0) {
                alert('No se hall� el �ndice del Producto seleccionado.');
                return false;
            }
            onCapa('DivDocRef');
            document.getElementById('<%=hddIndexLetra.ClientID%>').value = index;
            return false;
        }
        
    </script>

</asp:Content>

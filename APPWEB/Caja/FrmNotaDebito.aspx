<%@ Page EnableEventValidation="false" Title="" Language="vb" AutoEventWireup="false"
    MasterPageFile="~/Principal.Master" CodeBehind="FrmNotaDebito.aspx.vb" Inherits="APPWEB.FrmNotaDebito" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr style="background-color: Yellow">
            <td style="width: 350px">
                <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                    runat="server">Config. Formulario</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" OnClientClick="return(   valOnClickNuevo()  );"
                                Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valSaveDocumento()  );"
                                Text="Guardar" ToolTip="Guardar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimirNDElectronica" runat="server" OnClientClick="return(  valOnClick_btnImprimirNDElectronica()  );"
                                Text="Imprimir ND Electr�nica" ToolTip="Imprimir ND Electr�nica" Width="150px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocRef" runat="server" OnClientClick="return(     valOnClick_btnBuscarDocRef()         );"
                                Text="Documento Ref." ToolTip="Buscar Documento de Referencia" Width="120px" />
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                NOTA DE D�BITO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                           <tr>
                            <td align="right" class="LabelRojo" style="font-weight: bold">
                                Tipo Documento:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="ddlTipoDocumento" Width="100%" runat="server" CssClass="LabelRojo"
                                    Font-Bold="true" AutoPostBack="true">
                                    <asp:ListItem Text="Nota de D�bito Electr�nica" Value = "2102866601"></asp:ListItem>
                                    <asp:ListItem Text="Nota de D�bito" Value = "5"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                               <td>
                               &nbsp;
                            </td>
                            <td>
                   
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Documento por [ Serie - N�mero ]." OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMoneda" Width="100%" runat="server" CssClass="LabelRojo"
                                    Font-Bold="true" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Vcto.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaVcto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaVcto" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaVcto" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaVcto">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="right" class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tipo Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" Width="100%" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Motivo:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMotivo" runat="server"  Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Caja:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboCaja" runat="server" AutoPostBack="false" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Condici�n:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboCondicionPago" runat="server" AutoPostBack="false" Width="100%">
                                    <asp:ListItem Value="2">Cr�dito</asp:ListItem>
                                    <asp:ListItem Value="3">Contado</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Datos del Cliente
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" runat="server" CssClass="TextBox_ReadOnlyLeft"
                                    Enabled="true" ReadOnly="true" Width="400px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" runat="server" OnClientClick="return( valOnClick_btnBuscarPersona()  );"
                                    Text="Buscar" ToolTip="Buscar" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNI" runat="server" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC" runat="server" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto" colspan="6">
                                <asp:GridView ID="GV_LineaCredito" runat="server" AutoGenerateColumns="false" Width="500px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Otorgado" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdCuenta" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMonedaOtorgado" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblOtorgado" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CargoMaximo","{0:F2}")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Disponible" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMonedaDisponible" ForeColor="Red" Font-Bold="true" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblDisponible" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F2}")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="imgDocumentoRef" runat="server" />&nbsp;DOCUMENTO DE REFERENCIA
                            <cc1:CollapsiblePanelExtender ID="cpeDocumentoRef" runat="server" TargetControlID="Panel_DocumentoRef"
                                ImageControlID="imgDocumentoRef" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="imgDocumentoRef" ExpandControlID="imgDocumentoRef">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                                        HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMoneda_Total" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblTotal" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F2}")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Percepci�n" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMoneda_Percepcion" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblPercepcion" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Percepcion","{0:F2}")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total General" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMoneda_General" runat="server" ForeColor="Red" Font-Bold="true"
                                                                            Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblGeneral" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                CONCEPTOS
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_ListaProducto" runat="server" Width="100%">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Detalle Productos:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboNotaDebitoModo" runat="server" AutoPostBack="true" onClick="   return( valOnClickCboModoNotaDebito()  );   "
                                                    Enabled="False">
                                                    <asp:ListItem Value="0" Selected="True">Listar Art�culos contenidos en el Documento de Referencia</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCargarArticuloAll_DocRef" Width="370px" runat="server" Text="[ Cargar Todos los Art�culos del Documento de Referencia ]"
                                                    OnClientClick="return(   valOnClick_btnCargarArticuloAll_DocRef()    );" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GVProducto" runat="server" AutoGenerateColumns="false" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnQuitarProducto" OnClick="btnQuitarDetalle_Click" ToolTip="Quitar Producto del detalle."
                                                                    ImageUrl="~/Imagenes/Eliminar1.gif" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="C�d">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdDetalleDocumento_Detalle" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdDetalleAfecto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleAfecto")%>' />
                                                                        </td>
                                                                        <tr>
                                                                            <asp:Label ID="lblCodigoProducto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodigoProducto")%>'></asp:Label>
                                                                        </tr>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Cantidad Ref." ItemStyle-Font-Bold="true" DataFormatString="{0:F2}"
                                                            HeaderStyle-HorizontalAlign="Center" DataField="CantxAtender" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="Cantidad">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCantidadDetalle" TabIndex="100" onFocus="return( aceptarFoco(this)    );"
                                                                                onKeyup="return(   calcularMontos1('1')   );" onblur="return(   valBlur(event)   );"
                                                                                onKeypress="return(  validarNumeroPuntoPositivo('event')   );" runat="server" Width="70px"
                                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblUM_Detalle" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"UMedida")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdUnidadMedidaDetalle" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Descripci�n" HeaderStyle-HorizontalAlign="Center" DataField="NomProducto" />
                                                        <asp:BoundField HeaderText="P. Unitario" ItemStyle-Font-Bold="true" DataFormatString="{0:F2}"
                                                            HeaderStyle-HorizontalAlign="Center" DataField="PrecioSD" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="P. Unitario">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtPUnit_Detalle" TabIndex="200" onFocus="return( aceptarFoco(this)    );"
                                                                                onKeypress="return(  validarNumeroPuntoPositivo('event')   );" onKeyup="return(   calcularMontos1('2')   );"
                                                                                onblur="return(   valBlur(event)   );" runat="server" Width="70px" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="Valor de Venta">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtImporte_Detalle" CssClass="TextBoxReadOnly" onFocus="return(  valOnFocusImporte1()  );"
                                                                                onKeypress="return(  false  );" runat="server" Width="70px" Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                                <asp:GridView ID="GV_Concepto" runat="server" AutoGenerateColumns="false" Width="56%"
                                                    Height="32px">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnQuitarConcepto" OnClick="btnQuitarDetalleConcepto_Click"
                                                                    ToolTip="Quitar Concepto al Detalle" ImageUrl="~/Imagenes/Eliminar1.gif" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" Width="50px" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Descripci�n del Concepto" HeaderStyle-HorizontalAlign="Center"
                                                            DataField="Descripcion">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="Monto">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtConcepto" TabIndex="200" onFocus="return( aceptarFoco(this)    );"
                                                                                onKeypress="return(  validarNumeroPuntoPositivo('event')   );" onKeyup="return(   calcularMontos('2')   );"
                                                                                onblur="return(   valBlur(event)   );" Enabled="false" runat="server" Width="70px" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Sub Total:
                                        </td>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtSubTotalproducto" onFocus="return(   valOnFocus_Totales()   );"
                                                onKeypress="return(  false );" Width="90px" CssClass="TextBox_ReadOnly" Text="0"
                                                runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            I.G.V.
                                        </td>
                                        <td>
                                            <asp:Label ID="lblIGV_Tasaproducto" runat="server" CssClass="Label" Text="-"></asp:Label>
                                        </td>
                                        <td class="Texto">
                                            (%):
                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtIGVproducto" onFocus="return(   valOnFocus_Totales()   );" Width="90px"
                                                CssClass="TextBox_ReadOnly" Text="0" onKeypress="return(  false );" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Total de Productos:
                                        </td>
                                        <td>
                                            <asp:Label ID="Label4" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtTotalproducto" onFocus="return(   valOnFocus_Totales()   );"
                                                onKeypress="return(  false );" Width="90px" CssClass="TextBox_ReadOnly" Text="0"
                                                runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnAddDetalle" Text="Nuevo Concepto" runat="server" Width="120px"
                                    ToolTip="Agregar nuevo Concepto" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ItemStyle-Width="65px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                        <asp:TemplateField HeaderText="Concepto" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboConcepto" runat="server" DataTextField="Nombre" DataValueField="Id"
                                                                DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaConcepto")%>'>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDescripcionConcepto" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" Width="400px"
                                                                ToolTip="Ingrese una Descripci�n Adicional" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Concepto")%>'
                                                                TabIndex="200"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMoneda" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAbono" onKeypress="return(   validarNumeroPuntoPositivo('event')   );"
                                                                onblur="return( valBlur(event)  );" onKeyup="return( calcularMontos('0')  );" onFocus="return(  aceptarFoco(this) );"
                                                                Width="90px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F2}")%>'
                                                                TabIndex="210"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Sub Total:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaSubTotal" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtSubTotal" onFocus="return(   valOnFocus_Totales()   );" onKeypress="return(  false );"
                                                Width="90px" CssClass="TextBox_ReadOnly" Text="0" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            I.G.V.
                                        </td>
                                        <td>
                                            <asp:Label ID="lblIGV_Tasa" runat="server" CssClass="Label" Text="-"></asp:Label>
                                        </td>
                                        <td class="Texto">
                                            (%):
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaIGV" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtIGV" onFocus="return(   valOnFocus_Totales()   );" Width="90px"
                                                CssClass="TextBox_ReadOnly" Text="0" onKeypress="return(  false );" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Total:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotal" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtTotal" onFocus="return(   valOnFocus_Totales()   );" onKeypress="return(  false );"
                                                Width="90px" CssClass="TextBox_ReadOnly" Text="0" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                Observaciones
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" Width="100%" Height="120px" runat="server" MaxLength="500"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hddIdPersona" runat="server" />
                            <asp:HiddenField ID="hddIdDocumento" runat="server" />
                            <asp:HiddenField ID="hddIdDocumentoRef" runat="server" />
                            <asp:HiddenField ID="hddIdMonedaDocRef" runat="server" />
                            <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                            <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />                            
                            <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda());" runat="server"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboRol" runat="server" Width="100%">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 261px; left: 42px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="cbotipodocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Tienda:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTienda_DocRef" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdMonedaDocRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>    
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConfiguracionFrm" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 120px; left: 50px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton9" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaConfiguracionFrm'));" />
                </td>
            </tr>
            <tr align="left">
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chb_CuentaxCobrar" Checked="true" CssClass="Texto" Font-Bold="true"
                                    runat="server" Text="Generar una Cuenta por Cobrar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicio_BA" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                    CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicio_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin_BA" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al cliente seleccionado.
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        //**********************  BUSCAR PERSONA
        function valOnClick_btnBuscarPersona() {
            mostrarCapaPersona();
            return false;
        }


        function valOnClick_btnImprimirNDElectronica() {
            var comboSerie = document.getElementById('<%= cboSerie.ClientID %>');
            var txtCodigo = document.getElementById('<%= txtCodigoDocumento.ClientID %>');
            var comboTipoDocumento = document.getElementById('<%= ddlTipoDocumento.ClientID %>');

            var idSerie = comboSerie.value;
            var idTipoDocumento = comboTipoDocumento.value;
            var codigo = txtCodigo.value;

            window.open('../../caja/reportes/frmImprimirFacElectronica.aspx?idSerie=' + idSerie + '&codigo=' + codigo + '&idtipoDocumento=' + idTipoDocumento + '', 'popup', 'width=800,height=500');
            return false;
        }
        //************** FIN BUSCAR PERSONA

        function valOnFocus_Totales() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }

        function mostrarCapaBuscarDocRef() {
            onCapa('capaDocumentosReferencia');
            return false;
        }
        function valOnFocusImporte() {
            document.getElementById('<%=btnBuscarDocRef.ClientID%>').focus();
            return false;
        }
        function valOnFocusImporte1() {
            document.getElementById('<%=btnBuscarDocRef.ClientID%>').focus();
            return false;
        }
        function valOnClickBuscarDocRef() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                mostrarCapaBuscarPersona();
                return false;
            }
            return true;
        }





        function calcularMontos(opcion) {

            //******* opcion
            //** 0: Todo                        

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var total = 0;
            var importe = 0;

            switch (parseInt(opcion)) {
                case 0:  //*********** Lista Productos
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            importe = parseFloat(rowElem.cells[2].children[0].cells[1].children[0].value);
                            if (isNaN(importe)) { importe = 0; }
                            total = total + redondear(importe, 2);
                        }
                    }
                    break;
            }
            var tasaIGV = parseFloat(document.getElementById('<%=lblIGV_Tasa.ClientID%>').innerHTML);
            var subTotal = (total / (1 + tasaIGV / 100));
            document.getElementById('<%=txtSubTotal.ClientID%>').value = Format(subTotal, 2);  //**** SUB TOTAL
            document.getElementById('<%=txtIGV.ClientID%>').value = Format(subTotal * (tasaIGV / 100), 2);  //**** IGV
            document.getElementById('<%=txtTotal.ClientID%>').value = Format(total, 2);  //**** TOTAL                                   

            return false;
        }

        function valSaveDocumento() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0) {
                alert('Debe seleccionar una Moneda.');
                return false;
            }
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }
            var cboCaja = document.getElementById('<%=cboCaja.ClientID%>');
            if (isNaN(parseInt(cboCaja.value)) || cboCaja.value.length <= 0 || parseInt(cboCaja.value) <= 0) {
                alert('Debe seleccionar una Caja.');
                return false;
            }
            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }

            var txtTotalAPagar = document.getElementById('<%=txtTotal.ClientID%>');
            if (isNaN(parseFloat(txtTotalAPagar.value)) || txtTotalAPagar.value.length <= 0 || parseFloat(txtTotalAPagar.value) <= 0) {
                alert('El Total debe ser un valor mayor a cero.');
                return false;
            }

            var monedaEmision = getCampoxValorCombo(cboMoneda, cboMoneda.value);

            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                var importe = 0;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    importe = parseFloat(rowElem.cells[2].children[0].cells[1].children[0].value);
                    if (isNaN(importe)) { importe = 0; }
                    if (importe <= 0) {
                        alert('Ingrese una valor v�lido.');
                        rowElem.cells[2].children[0].cells[1].children[0].select();
                        rowElem.cells[2].children[0].cells[1].children[0].focus();
                        return false;
                    }

                }
            }
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var mensaje = '';
            switch (parseInt(hddFrmModo.value)) {
                case 1: //******** NUEVO
                    mensaje = 'Usted est� registrando un Documento [ Nota de D�bito ] por un valor de ' + monedaEmision + ' ' + txtTotalAPagar.value + '. Desea continuar con la Operaci�n ?';
                    break;
                case 2: //******** EDITAR
                    mensaje = 'Usted est� editando un Documento [ Nota de D�bito ] por un valor de ' + monedaEmision + ' ' + txtTotalAPagar.value + '. Desea continuar con la Operaci�n ?';
                    break;
            }
            return confirm(mensaje);
        }


        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }

            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }

        function valOnKeyPressCodigoDoc(txtCodigoDocumento) {
            var key = event.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }
        function valOnClickNuevo() {

            return true;

        }

        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }

            return true;
        }

        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }

        function valOnClick_btnGenerarIngresoAlmacen() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para la Generaci�n de una Gu�a de Recepci�n de Almac�n.');
                return false;
            }

            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite la Operaci�n.');
                return false;
            }
            window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=25&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        function valOnClick_btnImprimir() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Impresi�n.');
                return false;
            }
            //window.open('../../Caja/Reportes/Visor1.aspx?iReporte=5&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=5&IdDocumento=' + hddIdDocumento.value, 'Cotizacion', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }

        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }

        //************************** BUSQUEDA PERSONA
        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function valOnFocus_Cliente() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }
        //*********************************************************************************** FIN BUSCAR CLIENTE
        function valOnClickCboModoNotaDebito() {
            var hddIdDocumentoRef = document.getElementById('<%=hddIdDocumentoRef.ClientID%>');
         
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            var hddIdDocumentoRef = document.getElementById('<%=hddIdDocumentoRef.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoRef.value)) || hddIdDocumentoRef.value.length <= 0) {
                alert('Debe seleccionar un Documento de Referencia.');
                return false;
            }
            return true;
        }

        function valOnClick_btnBuscarDocRef() {
            
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('DEBE SELECCIONAR UN PERSONA. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var grilla = document.getElementById('<%=GV_DocumentoRef.ClientID%>');
            if (grilla != null) {

                alert('Solo se permite seleccionar una factura');
                return false;
            }

                
           
        }
        function calcularMontos1(opcion) {

            //******* opcion
            //** 0: Todo
            //** 1: Cantidad
            //** 2: Precio Unit

            var grilla = document.getElementById('<%=GVProducto.ClientID%>');
            var grilla1 = document.getElementById('<%=GV_Concepto.ClientID%>');
            var total1 = 0;
            var cantidad = 0;
            var precioUnit = 0;
            var monto = 0;
            var montototal = 0;
            var importe1 = 0;

            var cboModo = document.getElementById('<%=cboNotaDebitoModo.ClientID%>');

            switch (parseInt(cboModo.value)) {
                case 0:  //*********** Lista Productos
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            cantidad = parseFloat(rowElem.cells[3].children[0].cells[0].children[0].value);
                            if (isNaN(cantidad)) { cantidad = 0; }
                            precioUnit = parseFloat(rowElem.cells[6].children[0].cells[0].children[0].value);
                            if (isNaN(precioUnit)) { precioUnit = 0; }
                            importe1 = cantidad * precioUnit;
                            rowElem.cells[7].children[0].cells[0].children[0].value = redondear(importe1, 2);
                            total1 = total1 + redondear(importe1, 2);
                        }
                    }


                    break;

               
            }

            var tasaIGV1 = parseFloat(document.getElementById('<%=lblIGV_Tasaproducto.ClientID%>').innerHTML);
            var subTotal1 = (total1 / (1 + tasaIGV1 / 100));
            document.getElementById('<%=txtSubTotalproducto.ClientID%>').value = redondear(subTotal1, 2);  //**** SUB TOTAL
            document.getElementById('<%=txtIGVproducto.ClientID%>').value = redondear(subTotal1 * (tasaIGV1 / 100), 2);  //**** IGV


            if (grilla1 != null) {
                for (var p = 1; p < grilla1.rows.length; p++) {
                    var rowElemx = grilla1.rows[p];
                    monto = parseFloat(rowElemx.cells[2].children[0].cells[0].children[0].value);
                    if (isNaN(monto)) { monto = 0; }
                    montototal = monto;
                    total1 = total1 + montototal;
                }
            }

            document.getElementById('<%=txtTotalproducto.ClientID%>').value = Format(total1, 2);  //**** TOTAL                                   

            return false;
        }
        
        function valOnClick_btnCargarArticuloAll_DocRef() {
            var hddIdDocumentoRef = document.getElementById('<%=hddIdDocumentoRef.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoRef.value)) || hddIdDocumentoRef.value.length <= 0) {
                alert('Debe seleccionar un Documento de Referencia.');
                return false;
            }
            return true;
        }
        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;
        }
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>

﻿Imports CrystalDecisions.CrystalReports.Engine
Public Class Visor3
    Inherits System.Web.UI.Page
    Dim reporte As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            If iReporte Is Nothing Then
                iReporte = ""
            End If
            ViewState.Add("iReporte", iReporte)
            Select Case iReporte
                Case "1" '*** Recibo Ingreso
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "2"
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("Resumen", Request.QueryString("Resumen"))

            End Select
        End If
        mostrarReporte()
    End Sub

    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString
            Case "1"
                ReporteDocReciboIngreso()
            Case "2"
                getMetdos()
        End Select
    End Sub
    Private Sub getMetdos()
        Dim soloResumen As Integer = CInt(ViewState.Item("Resumen"))
        If soloResumen = 1 Then
            reporteIngresoxTienda()
        Else
            ReporteResumenDiarioVentas()
        End If
    End Sub

    Private Sub ReporteDocReciboIngreso()
        Try
            reporte = New CR_ReciboIngreso
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetReciboIngreso(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub ReporteResumenDiarioVentas()
        Try
            'Dim Reporte As New CR_ReporteResumenDiarioVentas
            reporte = New CR_RptResumenDiarioVentas
            Dim objReporte As New Negocio.Reportes
            Dim dttable As DataTable

            dttable = objReporte.getResumendiarioVentas(CInt(ViewState.Item("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin"))).Tables("DT_ResumenDiarioVentas")

            'If dttable.Rows.Count > 0 Then
            'Dim fila As String = ""
            'For Each row As DataRow In dttable.Rows
            '    fila = row("Doc_FechaEmision").ToString
            'Next
            'Dim coll As DataColumn = Nothing
            'For Each col As DataColumn In dttable.Columns
            '    coll.ColumnName(0) = col.ColumnName(0)
            'Next
            'Reporte.SetDataSource(objReporte.getResumendiarioVentas(CInt(ViewState.Item("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin"))))

            Reporte.SetDataSource(dttable)
            Reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            Reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            CRViewer.ReportSource = Reporte
            CRViewer.DataBind()
            'Else
            'Response.Write("<script>alert('no vienen  datos.');</script>")
            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub reporteIngresoxTienda()
        Try
            Dim vidempresa As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente
            ds = objReporte.ReporteIngresosxTienda(CInt(ViewState("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")))
            reporte = New CR_IngresoxTienda
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub Visor3_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class
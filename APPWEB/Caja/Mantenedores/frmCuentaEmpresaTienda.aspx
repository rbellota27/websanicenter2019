﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmCuentaEmpresaTienda.aspx.vb" Inherits="APPWEB.frmCuentaEmpresaTienda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td colspan="2">
                <asp:Button Width="90px" ID="btGuardar" runat="server" Text="Guardar" OnClientClick="return(validarSave())" />
                <asp:Button Width="90px" ID="btCancelar" runat="server" Text="Cancelar" />
            </td>
        </tr>
        <tr>
            <td colspan="2" class="TituloCelda">
                Cuenta Empresa Tienda
            </td>
        </tr>
        <tr>
            <td class="LabelTab">
                &nbsp;Empresa:
            </td>
            <td>
                <asp:DropDownList ID="dlempresa" Width="400px" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="LabelTab">
                &nbsp;Tienda:
            </td>
            <td>
                <asp:DropDownList ID="dltienda" Width="400px" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="LabelTab">
                &nbsp;Moneda:
            </td>
            <td>
                <asp:DropDownList ID="dlmoneda" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                &nbsp;&nbsp;
                <asp:Button Width="90px" ID="btagregar" runat="server" Text="Agregar" OnClientClick="return(validarAgregar());" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:GridView ID="gvCuenta" runat="server" AutoGenerateColumns="False" GridLines="None"
                    Width="90%">
                    <RowStyle CssClass="GrillaRow" />
                    <Columns>
                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hddidempresa" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>' />
                                <asp:HiddenField ID="hddidtienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>' />
                                <asp:HiddenField ID="hddidmoneda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>' />
                                <asp:HiddenField ID="hdd_tipo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Tipo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="cstrEmpresa" HeaderText="Empresa" />
                        <asp:BoundField DataField="cstrTienda" HeaderText="Tienda" />
                        <asp:BoundField DataField="cstrMoneda" HeaderText="Moneda" />
                        <asp:TemplateField HeaderText="Monto Maximo">
                            <ItemTemplate>
                                <asp:TextBox ID="gtbmontomax" runat="server" onfocus="return(aceptarFoco(this));"
                                    MaxLength="12" onKeyUp="return(copiar(this));" onKeyPress="return(validarNumeroPunto(event));"
                                    Text='<%# DataBinder.Eval(Container.DataItem,"cet_MontoMax","{0:F2}") %>'></asp:TextBox>
                                <asp:HiddenField ID="hddmontoAnterior" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"montoAnt","{0:F2}") %>' />
                                <asp:HiddenField ID="hddsaldoAnterior" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"saldoAnt","{0:F2}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="cet_Saldo" DataFormatString="{0:F2}" HeaderText="Saldo" />
                        <asp:TemplateField HeaderText="N° Max. Cuentas">
                            <ItemTemplate>
                                <asp:TextBox ID="gtbnromaxcta" runat="server" onfocus="return(aceptarFoco(this));"
                                    MaxLength="3" onKeyPress="return(onKeyPressEsNumero('event'));" Text='<%# DataBinder.Eval(Container.DataItem,"cet_NroMaxCuentas") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estado">
                            <ItemTemplate>
                                <asp:CheckBox ID="ckEstado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"cet_Estado") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <HeaderStyle CssClass="GrillaCabecera" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2" class="TituloCeldaLeft">
                &nbsp;Registros
            </td>
        </tr>
        <tr>
            <td class="LabelTab">
                &nbsp;Estado:
            </td>
            <td>
                <asp:RadioButtonList ID="rb_estado" runat="server" RepeatDirection="Horizontal" CssClass="Texto"
                    AutoPostBack="true">
                    <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                    <asp:ListItem Value="2">Todos</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:GridView ID="gvregistros" runat="server" AutoGenerateColumns="False" GridLines="None"
                    Width="100%">
                    <RowStyle CssClass="GrillaRow" />
                    <Columns>
                        <asp:CommandField SelectText="Editar" ShowSelectButton="True" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hddidempresa" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>' />
                                <asp:HiddenField ID="hddidtienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>' />
                                <asp:HiddenField ID="hddidmoneda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>' />
                                <asp:HiddenField ID="hddtipo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Tipo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="cstrEmpresa" HeaderText="Empresa" />
                        <asp:BoundField DataField="cstrTienda" HeaderText="Tienda" />
                        <asp:BoundField DataField="cstrMoneda" HeaderText="Moneda" />
                        <asp:BoundField DataField="cet_MontoMax" HeaderText="Monto Maximo" DataFormatString="{0:F2}" />
                        <asp:BoundField DataField="cet_Saldo" HeaderText="Saldo" DataFormatString="{0:F2}" />
                        <asp:BoundField DataField="cet_NroMaxCuentas" HeaderText="N° Max. Cuentas" DataFormatString="{0:F2}" />
                        <asp:BoundField DataField="CuentasActivas" HeaderText="Nº Ctas Activas" DataFormatString="{0:F2}" />
                        <asp:BoundField DataField="CargoMaxCtasxTienda" DataFormatString="{0:F2}" HeaderText="Cargo Max. Ctas." />
                        <asp:TemplateField HeaderText="Estado">
                            <ItemTemplate>
                                <asp:CheckBox ID="ckEstado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"cet_Estado") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <HeaderStyle CssClass="GrillaCabecera" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HiddenField ID="hdd_operativo" runat="server" />
                <asp:HiddenField ID="hdd_idusuario" Value="0" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        function validarAgregar() {
            var idempresa = parseInt(document.getElementById('<%=dlEmpresa.ClientID %>').value);
            var idtienda = parseInt(document.getElementById('<%=dlTienda.ClientID %>').value);
            var idmoneda = parseInt(document.getElementById('<%=dlMoneda.ClientID %>').value);
            var grilla = document.getElementById('<%=gvCuenta.ClientID %>');

            if (isNaN(idtienda)) { idtienda = 0; }
            if (idtienda == 0) {
                alert('Debe seleccionar una tienda');
                return false;
            }

            if (isNaN(idmoneda)) { idmoneda = 0; }
            if (idmoneda == 0) {
                alert('Debe seleccionar una moneda');
                return false;
            }

            if (grilla != null) { //
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[0].value == idempresa.value && rowElem.cells[1].children[1].value == idtienda.value && rowElem.cells[1].children[2].value == idmoneda.value) {
                        alert('La cuadricula ya contiene la [Empresa], [Tienda] y [Moneda] que desea agregar');
                        return false;
                    }
                } // next
            } //end if


            return true;
        }
        //+++++++++
        function copiar(obj) {
            var grilla = document.getElementById('<%=gvCuenta.ClientID %>');
            var dif = 0;
            var saldonuevo = 0;

            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];

                var montonuevo = parseFloat(rowElem.cells[5].children[0].value);
                if (isNaN(montonuevo)) { montonuevo = 0; }

                var montoant = parseFloat(rowElem.cells[5].children[1].value);
                if (isNaN(montoant)) { montoant = 0; }

                var saldoant = parseFloat(rowElem.cells[5].children[2].value);

                if (rowElem.cells[5].children[0].id == obj.id && rowElem.cells[1].children[3].value == '1') {
                    rowElem.cells[6].innerText = redondear(montonuevo, 2);
                }
                if (rowElem.cells[5].children[0].id == obj.id && rowElem.cells[1].children[3].value == '2') {
                    if (montoant < montonuevo) {
                        dif = (montonuevo - montoant);
                        saldonuevo = saldoant + dif;
                        rowElem.cells[6].innerText = redondear(saldonuevo, 2);
                        return false;
                    }
                    if (montoant >= montonuevo) {
                        dif = (montoant - montonuevo);
                        saldonuevo = saldoant - dif;
                        rowElem.cells[6].innerText = redondear(saldonuevo, 2);
                        return false;
                    }
                }
            }
        }
        //++++++++++++++++++++++++++++
        function validarSave() {
            var grilla = document.getElementById('<%=gvCuenta.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var montomx = rowElem.cells[5].children[0];
                    if (isNaN(parseFloat(montomx.value)) || montomx.value.length <= 0) {
                        alert('ingrese el Monto Maximo');
                        rowElem.cells[5].children[0].focus();
                        return false;
                    }
                    var cuenta = rowElem.cells[7].children[0];
                    if (isNaN(parseFloat(cuenta.value)) || cuenta.value.length <= 0 || parseInt(cuenta.value) == 0) {
                        alert('ingrese el N° Max. Cuentas');
                        rowElem.cells[7].children[0].focus();
                        return false;
                    }
                }
            } else {
                alert('La cudricula esta vacia');
                return false;
            }
            return confirm('Desea continuar con la operacion');
        } 
        
        
    </script>

</asp:Content>

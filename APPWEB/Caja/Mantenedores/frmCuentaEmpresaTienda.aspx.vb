﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmCuentaEmpresaTienda
    Inherits System.Web.UI.Page


#Region "variables"
    Private objscript As New ScriptManagerClass
    Private list_CtaEmpresaTienda As List(Of Entidades.CuentaEmpresaTienda)
    Private ListaBusqueda As List(Of Entidades.CuentaEmpresaTienda)
    Private obj_CtaEmpresaTienda As Entidades.CuentaEmpresaTienda
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
    End Enum
    Private drop As Combo
#End Region

#Region "Propiedades"

    Private Property SessionBusqueda() As List(Of Entidades.CuentaEmpresaTienda)
        Get
            Return CType(Session("EmpresaTienda"), List(Of Entidades.CuentaEmpresaTienda))
        End Get
        Set(ByVal value As List(Of Entidades.CuentaEmpresaTienda))
            Session.Remove("EmpresaTienda")
            Session.Add("EmpresaTienda", value)
        End Set
    End Property

    Private Property SessionCuentaEmpresaTienda() As List(Of Entidades.CuentaEmpresaTienda)
        Get
            Return CType(Session("CuentaEmpresaTienda"), List(Of Entidades.CuentaEmpresaTienda))
        End Get
        Set(ByVal value As List(Of Entidades.CuentaEmpresaTienda))
            Session.Remove("CuentaEmpresaTienda")
            Session.Add("CuentaEmpresaTienda", value)
        End Set
    End Property

#End Region

#Region "Procedimientos"

    Private Sub Mantenimiento()
        Dim mantenimiento As Boolean = False

        actualizarCuentaEmpresaTienda()
        list_CtaEmpresaTienda = SessionCuentaEmpresaTienda

        For i As Integer = 0 To list_CtaEmpresaTienda.Count - 1

            If list_CtaEmpresaTienda(i).Tipo = operativo.GuardarNuevo Then
                list_CtaEmpresaTienda(i).cet_Saldo = list_CtaEmpresaTienda(i).cet_MontoMax
            End If

        Next

        mantenimiento = (New Negocio.CuentaEmpresaTienda).UpdateCuentaEmpresaTienda(list_CtaEmpresaTienda, CInt(hdd_idusuario.Value))

        If mantenimiento = True Then
            LlenarGrillaxIdusuario(CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), CInt(Me.dlmoneda.SelectedValue))
            SessionCuentaEmpresaTienda = Nothing
            gvCuenta.DataBind()
            objscript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")
        End If

    End Sub

    Private Sub actualizarCuentaEmpresaTienda()

        Me.list_CtaEmpresaTienda = Me.SessionCuentaEmpresaTienda

        If Me.list_CtaEmpresaTienda Is Nothing Then list_CtaEmpresaTienda = New List(Of Entidades.CuentaEmpresaTienda)

        For i As Integer = 0 To list_CtaEmpresaTienda.Count - 1

            list_CtaEmpresaTienda(i).cet_MontoMax = CDec(CType(gvCuenta.Rows(i).FindControl("gtbmontomax"), TextBox).Text)
            list_CtaEmpresaTienda(i).cet_NroMaxCuentas = CInt(CType(gvCuenta.Rows(i).FindControl("gtbnromaxcta"), TextBox).Text)
            list_CtaEmpresaTienda(i).cet_Estado = CBool(CType(gvCuenta.Rows(i).FindControl("ckEstado"), CheckBox).Checked)

        Next

        Me.SessionCuentaEmpresaTienda = list_CtaEmpresaTienda

    End Sub

    Private Sub cargarALIniciar()
        drop = New Combo
        drop.LlenarCboEmpresaxIdUsuario(Me.dlempresa, CInt(hdd_idusuario.Value), False)
        drop.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.dltienda, CInt(dlempresa.SelectedValue), CInt(hdd_idusuario.Value), False)
        drop.LlenarCboMoneda(Me.dlmoneda)
        LlenarGrillaxIdusuario(CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), CInt(Me.dlmoneda.SelectedValue))
        SessionCuentaEmpresaTienda = Nothing
    End Sub

    Private Sub LlenarGrillaxIdusuario(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer)

        Me.ListaBusqueda = (New Negocio.CuentaEmpresaTienda).SelectCuentaEmpresaTienda(IdEmpresa, IdTienda, IdMoneda, CInt(rb_estado.SelectedValue))

        Me.gvregistros.DataSource = Me.ListaBusqueda
        Me.gvregistros.DataBind()

        Me.SessionBusqueda = Me.ListaBusqueda

    End Sub

#End Region

#Region "Eventos Principales"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hdd_idusuario.Value = CStr(Session("IdUsuario"))
            cargarALIniciar()
        End If
    End Sub

    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Try
            Mantenimiento()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        LlenarGrillaxIdusuario(CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), CInt(Me.dlmoneda.SelectedValue))
        gvCuenta.DataBind()
        SessionCuentaEmpresaTienda = Nothing
    End Sub

    Private Sub gvregistros_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvregistros.SelectedIndexChanged
        Try

            Dim index As Integer = gvregistros.SelectedRow.RowIndex

            actualizarCuentaEmpresaTienda()
            Me.list_CtaEmpresaTienda = Me.SessionCuentaEmpresaTienda

            Me.ListaBusqueda = Me.SessionBusqueda


            If Not Me.list_CtaEmpresaTienda.Find(Function(cet As Entidades.CuentaEmpresaTienda) cet.IdEmpresa = ListaBusqueda(index).IdEmpresa And cet.IdTienda = ListaBusqueda(index).IdTienda And cet.IdMoneda = ListaBusqueda(index).IdMoneda) Is Nothing Then
                Throw New Exception("La cuadricula ya contiene un registro igual al que desea agregar")
            End If


            Me.list_CtaEmpresaTienda.Add(ListaBusqueda(index))

            gvCuenta.DataSource = list_CtaEmpresaTienda
            gvCuenta.DataBind()

            Me.SessionCuentaEmpresaTienda = Me.list_CtaEmpresaTienda
            Me.SessionBusqueda = Me.ListaBusqueda

        Catch ex As Exception

            objscript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region

#Region "Grilla Cuenta Empresa Tienda"

    Private Sub LlenarGrillaCuenta()

        actualizarCuentaEmpresaTienda()
        Me.list_CtaEmpresaTienda = SessionCuentaEmpresaTienda

        Dim lista As List(Of Entidades.CuentaEmpresaTienda) = (New Negocio.CuentaEmpresaTienda).SelectCuentaEmpresaTienda(CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), CInt(dlmoneda.SelectedValue), 1)

        If lista.Count > 0 Then
            Me.list_CtaEmpresaTienda.Add(lista(0))
        Else



            obj_CtaEmpresaTienda = New Entidades.CuentaEmpresaTienda
            With obj_CtaEmpresaTienda
                .IdEmpresa = CInt(dlempresa.SelectedValue)
                .cstrEmpresa = HttpUtility.HtmlDecode(dlempresa.SelectedItem.Text.Trim)
                .IdTienda = CInt(dltienda.SelectedValue)
                .cstrTienda = HttpUtility.HtmlDecode(dltienda.SelectedItem.Text.Trim)
                .IdMoneda = CInt(dlmoneda.SelectedValue)
                .cstrMoneda = HttpUtility.HtmlDecode(dlmoneda.SelectedItem.Text.Trim)
                .cet_MontoMax = 0
                .cet_Saldo = 0
                .cet_NroMaxCuentas = 0
                .cet_Estado = True
                .montoAnt = 0
                .saldoAnt = 0
                .Tipo = operativo.GuardarNuevo
            End With

            Me.list_CtaEmpresaTienda.Add(obj_CtaEmpresaTienda)

        End If

        gvCuenta.DataSource = list_CtaEmpresaTienda
        gvCuenta.DataBind()

        Me.SessionCuentaEmpresaTienda = Me.list_CtaEmpresaTienda
    End Sub

    Protected Sub btagregar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btagregar.Click
        Try

            LlenarGrillaCuenta()

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvCuenta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCuenta.SelectedIndexChanged
        Dim index As Integer = gvCuenta.SelectedRow.RowIndex

        actualizarCuentaEmpresaTienda()
        Me.list_CtaEmpresaTienda = Me.SessionCuentaEmpresaTienda

        Me.list_CtaEmpresaTienda.RemoveAt(index)

        gvCuenta.DataSource = Me.list_CtaEmpresaTienda
        gvCuenta.DataBind()

        SessionCuentaEmpresaTienda = Me.list_CtaEmpresaTienda

    End Sub

#End Region


    Private Sub dlempresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlempresa.SelectedIndexChanged
        drop = New Combo
        drop.LlenarCboTiendaxIdEmpresaxIdUsuario(dltienda, CInt(dlempresa.SelectedValue), CInt(hdd_idusuario.Value), False)
        LlenarGrillaxIdusuario(CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), CInt(Me.dlmoneda.SelectedValue))
    End Sub

    Private Sub dltienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dltienda.SelectedIndexChanged
        LlenarGrillaxIdusuario(CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), CInt(Me.dlmoneda.SelectedValue))
    End Sub

    Private Sub dlmoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlmoneda.SelectedIndexChanged
        LlenarGrillaxIdusuario(CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), CInt(Me.dlmoneda.SelectedValue))
    End Sub

    Private Sub rb_estado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rb_estado.SelectedIndexChanged
        LlenarGrillaxIdusuario(CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), CInt(Me.dlmoneda.SelectedValue))
    End Sub


End Class
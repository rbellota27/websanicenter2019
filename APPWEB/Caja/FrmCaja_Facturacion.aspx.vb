﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports System.Net

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmCaja_Facturacion

    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaCatalogo As List(Of Entidades.Catalogo)
    Private listaDetalleConcepto As List(Of Entidades.DetalleConcepto)
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private listaCancelacion As List(Of Entidades.PagoCaja)

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Private ValidoRep As New Negocio.Util

    Private Const _PrecioBaseDcto_Lista As String = "PL"
    Private Const _PrecioBaseDcto_Comercial As String = "PC"
    Private Const _IdCondicionPago_Contado As Integer = 1
    Private Const _MedioPago_Interfaz_Cheque As Integer = 3
    Private Const _EstadoMov_Cheque_Aprobado As String = "AP"
    Private Const _EstadoMov_Cheque_PorAprobar As String = "PA"
    Private Const _IdTipoDocumento_FacturaTR As Integer = 59
    Private objCombo As Combo
#Region "************************ MANEJO DE SESSION"

    Private Function getListaCatalogo() As List(Of Entidades.Catalogo)
        Return CType(Session.Item(CStr(ViewState("listaCatalogo"))), List(Of Entidades.Catalogo))
    End Function
    Private Sub setListaCatalogo(ByVal lista As List(Of Entidades.Catalogo))
        Session.Remove(CStr(ViewState("listaCatalogo")))
        Session.Add(CStr(ViewState("listaCatalogo")), lista)
    End Sub
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item(CStr(ViewState("DetalleDocumento"))), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove(CStr(ViewState("DetalleDocumento")))
        Session.Add(CStr(ViewState("DetalleDocumento")), lista)
    End Sub
    Private Function getListaCancelacion() As List(Of Entidades.PagoCaja)
        Return CType(Session.Item(CStr(ViewState("ListaCancelacion"))), List(Of Entidades.PagoCaja))
    End Function
    Private Sub setListaCancelacion(ByVal lista As List(Of Entidades.PagoCaja))
        Session.Remove(CStr(ViewState("ListaCancelacion")))
        Session.Add(CStr(ViewState("ListaCancelacion")), lista)
    End Sub

    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("DetalleDocumento", "DetalleDocumento" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("ListaCancelacion", "ListaCancelacion" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaCatalogo", "listaCatalogo" + Aleatorio.Next(0, 100000).ToString)

    End Sub

#End Region
    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
        Documento_EnProceso = 7
    End Enum
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))

        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.btnBuscarDocumentoxCodigo)
        scriptmanager.RegisterPostBackControl(Me.btnImprimirFacElectronica)
        If (Not Me.IsPostBack) Then

            onLoad_Session()
            Dim objCbo As New Combo

            objCbo.llenarCboTipoDocumentoxIdTipoDocumentos(cboTipoDocumento, "1,3,59,1101353001,1101353002", 2)

            verSeccionesFrm()

            ConfigurarDatos()

            ValidarPermisos()

            inicializarFrm()
            Trace.Warn("termina la inicializacion de formulario")

            Trace.Warn("muestra cliente po defecto")
            clienteXdefecto()
            Trace.Warn("termina de mostrar cliente por defecto")

            Trace.Warn("valida apertura")
            Validar_AperturaCierreCaja()
            Trace.Warn("termina de validar apertura")
        End If

        'ocultar boton imprimir
        btnImprimir.Visible = False
        btnImprimirGrupo.Visible = False
        btnImprimirDetallado.Visible = False
        visualizarMoneda()

        Me.GV_ComponenteKit.DataSource = Nothing
        Me.GV_ComponenteKit.DataBind()
        Me.GV_ComponenteKit_Find.DataSource = Nothing
        Me.GV_ComponenteKit_Find.DataBind()
        'exportar()
    End Sub
    Private Sub verSeccionesFrm()
        Dim cad() As String = Nothing
        cad = Split(ValidoRep.fx_getValorParametroGeneralxIdparam("18,19,20,21,22,23"), ";", -1)

        ViewState.Add("ClientexDefecto", CInt(cad(0).ToString))

        verPanelMaestroObra(CInt(cad(1).ToString)) 'para mostrar seccion maestro obra
        TipoDocumentoXDefecto(CInt(cad(2).ToString)) 'mostrar documento por defecto
        verPanelPuntoPartidaPuntoLlegada(CInt(cad(3).ToString)) 'para mostrar seccion punto partida y punto llegada
        verpanelConceptos(CInt(cad(4).ToString)) 'para mostrar seccion conceptos

        ViewState.Add("secCodBarras", CInt(cad(5).ToString))
        If (CInt(cad(5).ToString) > 0) Then
            Me.tdSecCodBarras.Visible = True
            Me.txtCodBarrasPrincipal.Focus()
        Else
            Me.tdSecCodBarras.Visible = False
        End If
    End Sub
    Private Sub clienteXdefecto()
        If (Int(ViewState("ClientexDefecto")) > 0) Then 'para mostrar cliente por defecto
            cargarPersona(0)
        End If
    End Sub
    Private Sub TipoDocumentoXDefecto(ByVal documento As Integer)
        Select Case documento
            Case 1
                Me.cboTipoDocumento.SelectedValue = "1"
            Case 3
                Me.cboTipoDocumento.SelectedValue = "3"
            Case 59
                Me.cboTipoDocumento.SelectedValue = "59"
            Case 1101353001
                Me.cboTipoDocumento.SelectedValue = "1101353001"
            Case 1101353002
                Me.cboTipoDocumento.SelectedValue = "1101353002"
        End Select
    End Sub
    Private Sub cursorCodigoBarras()
        If (CInt(ViewState("secCodBarras")) > 0) Then
            Me.txtCodBarrasPrincipal.Focus()
        End If
    End Sub
    Private Sub verPanelMaestroObra(ByVal valor As Integer)
        If valor = 1 Then
            pn_MaestroObra.Visible = True
        Else
            pn_MaestroObra.Visible = False
        End If

    End Sub
    Private Sub verPanelPuntoPartidaPuntoLlegada(ByVal valor As Integer)
        If valor = 1 Then
            trTituloPuntoPartida.Visible = True
            trTituloPuntoLlegada.Visible = True
            trDetallePuntoLlegada.Visible = True
            trDetallePuntoPartida.Visible = True

        Else
            trTituloPuntoPartida.Visible = False
            trTituloPuntoLlegada.Visible = False
            trDetallePuntoLlegada.Visible = False
            trDetallePuntoPartida.Visible = False

        End If
    End Sub
    Private Sub verpanelConceptos(ByVal valor As Integer)
        If valor = 1 Then
            trtituloConceptos.Visible = True
            trDetalleConcepto.Visible = True
        Else
            trtituloConceptos.Visible = False
            trDetalleConcepto.Visible = False
        End If
    End Sub
    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / REGISTRAR CLIENTE / EDITAR CLIENTE / BUSCAR DOC REFERENCIA / BUSCAR MAESTRO DE OBRA  / SOLO PANEL DE CANCELACION
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 154, 155, 156, 157, 158, 159, 160, 196, 218, 219, 220, 241})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* FECHA VCTO
            Me.txtFechaVcto.Enabled = True
        Else
            Me.txtFechaVcto.Enabled = False
        End If

        If listaPermisos(6) > 0 Then  '********* REGISTRAR CLIENTE
            Me.btnNuevoCliente.Enabled = True
            Me.btnGuardarCliente.Enabled = True
        Else
            Me.btnNuevoCliente.Enabled = False
            Me.btnGuardarCliente.Enabled = False
        End If

        If listaPermisos(7) > 0 Then  '********* EDITAR CLIENTE
            Me.btnEditar_Cliente.Enabled = True
            Me.btncheckadd.Enabled = True
            Me.btncheckdel.Enabled = True
            Me.btnCambvende.Enabled = True
            Me.btnchangepl.Enabled = True
            Me.btnGuardar_EditarCliente.Enabled = True
        Else
            Me.btncheckadd.Enabled = False
            Me.btncheckdel.Enabled = False
            Me.btnEditar_Cliente.Enabled = False
            Me.btnCambvende.Enabled = False
            btnchangepl.Enabled = False
            Me.btnGuardar_EditarCliente.Enabled = False
            Me.btnGuardar_EditarCliente.Enabled = False
        End If

        If listaPermisos(8) > 0 Then  '********* BUSCAR DOC REFERENCIA
            Me.btnBuscarDocumentoRef.Enabled = True
        Else
            Me.btnBuscarDocumentoRef.Enabled = False
        End If

        If listaPermisos(9) > 0 Then  '********* BUSCAR MAESTRO DE OBRA
            Me.btnBuscarMaestroObra.Enabled = True
        Else
            Me.btnBuscarMaestroObra.Enabled = False
        End If

        Me.hddPermiso_OnlyPanel_Cancelacion.Value = CStr(listaPermisos(10))

        If listaPermisos(11) > 0 Then  '********* MOVER STOCK FISICO
            Me.chb_MoverAlmacen.Enabled = True
        Else
            Me.chb_MoverAlmacen.Enabled = False
        End If

        If listaPermisos(12) > 0 Then  '********* COMPROMETER STOCK
            Me.chb_ComprometerStock.Enabled = True
        Else
            Me.chb_ComprometerStock.Enabled = False
        End If

        If listaPermisos(13) > 0 Then  '********* SEPARAR AFECTOS / NO AFECTOS
            Me.chb_SepararAfecto_NoAfecto.Enabled = True
        Else
            Me.chb_SepararAfecto_NoAfecto.Enabled = False
        End If

        If listaPermisos(14) > 0 Then  '********* SALTAR CORRELATIVO
            Me.chb_SaltarCorrelativo.Enabled = True
        Else
            Me.chb_SaltarCorrelativo.Enabled = False
        End If

        If listaPermisos(15) > 0 Then  '********* SEPARAR X MONTO
            Me.chb_SepararxMontos.Enabled = True
        Else
            Me.chb_SepararxMontos.Enabled = False
        End If

        If listaPermisos(16) > 0 Then  '********* COMPROMETER PERCEPCION
            Me.chb_CompPercepcion.Enabled = True
        Else
            Me.chb_CompPercepcion.Enabled = False
        End If

        If listaPermisos(17) > 0 Then  '********* USAR ALIAS
            Me.chb_UseAlias.Enabled = True
        Else
            Me.chb_UseAlias.Enabled = False
        End If

        If listaPermisos(18) > 0 Then  '********* REDONDEO
            Me.rbl_UtilizarRedondeo.Enabled = True
        Else
            Me.rbl_UtilizarRedondeo.Enabled = False
        End If

        If listaPermisos(19) > 0 Then  '********* USUARIO VENDEDOR
            Me.cboUsuarioComision.Enabled = True
        Else
            Me.cboUsuarioComision.Enabled = False
        End If

        If listaPermisos(20) > 0 Then  '********* CONDICIÓN PAGO
            Me.cboCondicionPago.Enabled = True
        Else
            Me.cboCondicionPago.Enabled = False
        End If

        If listaPermisos(21) > 0 Then  '********* MEDIO PAGO
            Me.cboMedioPago.Enabled = True
        Else
            Me.cboMedioPago.Enabled = False
        End If

        If listaPermisos(22) > 0 Then  '********* MEDIO PAGO
            Me.chb_ValidarCanjeUnico_Cotizacion.Enabled = True
        Else
            Me.chb_ValidarCanjeUnico_Cotizacion.Enabled = False
        End If

    End Sub
    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
                '***GenerarCodigoDocumento()  ** EL CODIGO SE GENERA EN ACTUALIZAR BOTONES DE CONTROL
                .LlenarCboMoneda(Me.cboMoneda, False)
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.cboTipoDocumento.SelectedValue), False)
                .LlenarCboCondicionPago(Me.cboCondicionPago)
                '***************** CARGAMOS MEDIO DE PAGO
                '**************** .LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago, CInt(Me.cboTipoDocumento.SelectedValue), False)
                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, CInt(Me.cboTipoDocumento.SelectedValue), 1, False) '***** CONTADO
                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago_Credito, CInt(Me.cboTipoDocumento.SelectedValue), 2, True) '***** CREDITO
                .LlenarCboTipoPV(Me.cboTipoPrecioV)
                .LlenarCboTipoPV(Me.cboTipoPrecioPV_CambiarDetalle)
                .LlenarCboTipoAgente(Me.cboTipoAgente, True)
                .LlenarCboTipoAgente(Me.cmbTipoAgente_RegistrarCliente, True)
                .LlenarCboTipoAgente(Me.cboTipoAgente_EditarCliente, True)
                '************** PUNTO DE LLEGADA
                .LLenarCboDepartamento(Me.cboDepartamento)
                .LLenarCboProvincia(Me.cboProvincia, (Me.cboDepartamento.SelectedValue))
                .LLenarCboDistrito(Me.cboDistrito, (Me.cboDepartamento.SelectedValue), (Me.cboProvincia.SelectedValue))
                '**************** BUSQUEDA DE PRODUCTOS
                .llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)
                .llenarCboTipoExistencia(cmbTipoExistencia)
                .llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(cmbTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cmbTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
                '**************** DATOS DE CANCELACION
                .LlenarCboBanco(Me.cboBanco, False)
                .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
                .LlenarCboMoneda(Me.cboMoneda_DatoCancelacion, False)
                '' cboPost_DC se llena el tipo de tarjeta no el POS fue cambiado el 19/05/2011
                .LlenarCboTipoTarjetaxIdCaja(Me.cboPost_DC, CInt(Me.cboCaja.SelectedValue), False)
                '****************** LLENAR USUARIO VENDEDOR
                .LLenarCboUsuario(Me.cboUsuarioComision, False)
                .LlenarCboRol(cmbRol, True)
                .LLenarCboUsuario(Me.cboNuevoUsuario, False)

                If (Me.cboUsuarioComision.Items.FindByValue(CStr(Session("IdUsuario"))) IsNot Nothing) Then
                    Me.cboUsuarioComision.SelectedValue = CStr(Session("IdUsuario"))
                End If
                .LlenarCboMoneda(Me.cboMoneda_ValorRef, False)
                .llenarCboValorReferencia_Transporte(Me.cboValorReferencial, False)
                valOnChange_cboValorReferencial()
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(cboTipoDocumentoRef, CInt(cboTipoDocumento.SelectedValue), 2, False)
            End With

            Me.lblPorcentIgv.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.txtFechaAEntregar.Text = Me.txtFechaEmision.Text
            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

            Me.hddIdTipoPVDefault.Value = CStr((New Negocio.TipoPrecioV).SelectIdTipoPrecioVDefault())

            '************** MEDIO DE PAGO PRINCIPAL
            Me.hddIdMedioPagoPrincipal.Value = CStr((New Negocio.MedioPago).SelectIdMedioPagoPrincipal)

            actualizarOpcionesBusquedaDocRef(0, False)

            actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago.SelectedValue))

            actualizarMontoMaxAfecto_TipoDocumentoAE()

            verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub actualizarMontoMaxAfecto_TipoDocumentoAE()

        '***************** OBTENEMOS EL MONTO MINIMO AFECTO PERCEPCION
        Dim objTipoDocumentoAE As Entidades.TipoDocumentoAE = (New Negocio.TipoDocumentoAE).SelectValoresxParams(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue), CInt(Me.cboMoneda.SelectedValue))
        If (objTipoDocumentoAE IsNot Nothing) Then
            Me.hddMontoMaxAfectoPercepcion.Value = CStr(objTipoDocumentoAE.MontoMaximoAfecto)
        Else
            Me.hddMontoMaxAfectoPercepcion.Value = "0"
        End If
    End Sub

    Private Sub actualizarPuntoPartida(ByVal IdTienda As Integer, ByVal updateDepto As Boolean, ByVal updateProv As Boolean, ByVal updateDist As Boolean)

        Dim ubigeoTienda As String = (New Negocio.Tienda).SelectxId(IdTienda)(0).Ubigeo
        Dim direccion_Tienda As String = (New Negocio.Tienda).SelectxId(IdTienda)(0).Direccion

        '***************** PUNTO DE PARTIDA
        Dim objCbo As New Combo

        If (updateDepto) Then
            objCbo.LLenarCboDepartamento(Me.cboDepto_Partida)
            Me.cboDepto_Partida.SelectedValue = ubigeoTienda.Substring(0, 2)
        End If
        If (updateProv) Then
            objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
            Me.cboProvincia_Partida.SelectedValue = ubigeoTienda.Substring(2, 2)
        End If
        If (updateDist) Then
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, (Me.cboProvincia_Partida.SelectedValue))
            Me.cboDistrito_Partida.SelectedValue = ubigeoTienda.Substring(4, 2)
        End If

        Me.txtDireccion_Partida.Text = direccion_Tienda

    End Sub
    Private Sub visualizarMoneda()

        Me.lblMonedaTotal.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalGasto.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalPagar.Text = Me.cboMoneda.SelectedItem.ToString

        Me.lblMonedaPercepcionRef.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalAPagarRef.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalRef.Text = Me.cboMoneda.SelectedItem.ToString

        Me.lblMonedaFaltante.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalRecibido.Text = Me.cboMoneda.SelectedItem.ToString

        Me.lblMoneda_Percepcion_VistaPrevia.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMoneda_Total_VistaPrevia.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMoneda_TotalAPagar_VistaPrevia.Text = Me.cboMoneda.SelectedItem.ToString

        Me.lblMonedaPercepcion_DocSAVED.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalAPagar_DocSAVED.Text = Me.cboMoneda.SelectedItem.ToString

        Me.lblMoneda_MontoAdelanto_Nuevo.Text = Me.cboMoneda.SelectedItem.ToString

        Me.lblMoneda_Retencion.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMoneda_Detraccion.Text = Me.cboMoneda.SelectedItem.ToString

    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub
    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)
            actualizarPuntoPartida(CInt(Me.cboTienda.SelectedValue), True, True, True)

            '************* Línea de Crédito
            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                Me.GV_LineaCredito.DataBind()
            End If

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)
            actualizarPuntoPartida(CInt(Me.cboTienda.SelectedValue), True, True, True)

            '************* Línea de Crédito
            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                Me.GV_LineaCredito.DataBind()
            End If

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartamento.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia, (Me.cboDepartamento.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito, (Me.cboDepartamento.SelectedValue), (Me.cboProvincia.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboProvincia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito, (Me.cboDepartamento.SelectedValue), (Me.cboProvincia.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, ByVal viewMoneda As Boolean, Optional ByVal actualizarPuntoPartida_Tienda As Boolean = False)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then

            Session.Remove(CStr(ViewState("DetalleDocumento")))
            Session.Remove(CStr(ViewState("ListaCancelacion")))
            Session.Remove(CStr(ViewState("listaCatalogo")))

        End If

        If (initSession) Then
            Me.listaCatalogo = New List(Of Entidades.Catalogo)
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            Me.listaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCatalogo(Me.listaCatalogo)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            setListaCancelacion(Me.listaCancelacion)
        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {8, 15, 16, 24, 26})

            'Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, listaParametros(0), CDate(Me.txtFechaEmision.Text)), "dd/MM/yyyy")            
            Me.hddIdUnidadMedida_RetazoLongitud.Value = CStr(CInt(listaParametros(1)))
            Me.hddIdUnidadMedida_RetazoArea.Value = CStr(CInt(listaParametros(2)))
            Me.hddMontoAfectoRetencion.Value = CStr(CInt(listaParametros(4)))


            If (CInt(Me.hddIdUnidadMedida_RetazoLongitud.Value) > 0) Then
                Dim listaUnidadMedida As List(Of Entidades.UnidadMedida) = (New Negocio.UnidadMedida).SelectxId(CInt(Me.hddIdUnidadMedida_RetazoLongitud.Value))
                If (listaUnidadMedida.Count > 0) Then
                    Me.lblUnidadMedida_RetazoLongitud.Text = listaUnidadMedida(0).Descripcion
                End If
            End If
            Try : hddValidarBoleta.Value = CStr(CDec(listaParametros(3))) : Catch ex As Exception : End Try
        End If

        If (viewMoneda) Then
            visualizarMoneda()
        End If

        If (actualizarPuntoPartida_Tienda) Then
            actualizarPuntoPartida(CInt(Me.cboTienda.SelectedValue), True, True, True)
        End If

        Me.hddFrmModo.Value = CStr(FrmModo)

        ActualizarBotonesControl()

    End Sub
    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaVcto.Text = ""
        Me.txtnrodias.Text = ""
        Me.txtFechaAEntregar.Text = Me.txtFechaEmision.Text
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto
        Me.cboTipoOperacion.SelectedIndex = 0  '******* SIN TIPO DE OPERACION POR DEFECTO / POR DEFECTO

        If (Me.cboUsuarioComision.Items.FindByValue(CStr(Session("IdUsuario"))) IsNot Nothing) Then
            Me.cboUsuarioComision.SelectedValue = CStr(Session("IdUsuario"))
        End If

        Me.chb_ComprometerStock.Checked = True
        Me.chb_MoverAlmacen.Checked = False

        '************** DOCUMENTO REFERENCIA

        Me.lblTipoDocumentoRef.Text = ""
        Me.lblNroDocumentoRef.Text = ""
        Me.lblFechaEmisionRef.Text = ""
        Me.lblFechaVctoRef.Text = ""
        Me.lblDescripcionClienteRef.Text = ""
        Me.lblRucRef.Text = ""
        Me.lblDniRef.Text = ""
        Me.lblCondicionPagoRef.Text = ""
        Me.lblMedioPagoRef.Text = ""
        Me.lblNroDiasVigenciaRef.Text = ""
        Me.lblTotalRef.Text = "0"
        Me.lblPercepcionRef.Text = "0"
        Me.lblTotalAPagarRef.Text = "0"
        Me.lblTipoOperacionRef.Text = ""
        Me.txtObservaciones_DocRef.Text = ""

        '***************** MAESTRO DE OBRA
        Me.txtMaestro.Text = ""
        Me.txtDni_Maestro.Text = ""
        Me.txtRuc_Maestro.Text = ""


        '******************* PERSONA
        Me.cboTipoPersona_PanelCliente.SelectedIndex = 0
        Me.txtApPaterno_RazonSocial.Text = ""
        Me.txtApMaterno.Text = ""
        Me.txtNombres.Text = ""
        Me.txtCodigoCliente.Text = ""
        Me.txtIdCliente_BuscarxId.Text = ""
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""
        Me.cboTipoPrecioV.SelectedIndex = 0
        Me.txtDireccionCliente.Text = ""
        Me.cboTipoAgente.SelectedIndex = 0
        Me.txtTasaAgente.Text = "0"

        '********** LINEA DE CREDITO
        Me.GV_LineaCredito.DataSource = Nothing
        Me.GV_LineaCredito.DataBind()


        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

        Me.txtDescuento.Text = "0"
        Me.txtSubTotal.Text = "0"
        Me.txtTotal.Text = "0"
        Me.txtPercepcion.Text = "0"
        Me.txtTotalAPagar.Text = "0"
        Me.txtTotalConcepto_PanelDetalle.Text = "0"
        Me.txtIGV.Text = "0"
        Me.txtImporteTotal.Text = "0"
        Me.txtDescuentoGlobal.Text = "0"

        '********************** TRANSPORTE
        Me.txtCantidad_Transporte.Text = "0"
        Me.txtValorReferencial_Total.Text = "0"

        '*********** PUNTO PARTIDA
        Me.cboDepto_Partida.SelectedIndex = 0
        Me.cboProvincia_Partida.SelectedIndex = 0
        Me.cboDistrito_Partida.SelectedIndex = 0
        Me.txtDireccion_Partida.Text = ""

        '*********** PUNTO LLEGADA
        Me.cboDepartamento.SelectedIndex = 0
        Me.cboProvincia.SelectedIndex = 0
        Me.cboDistrito.SelectedIndex = 0
        Me.txtDireccion_Llegada.Text = ""

        '************* CONCEPTO
        Me.GV_Concepto.DataSource = Nothing
        Me.GV_Concepto.DataBind()
        Me.txtTotalConcepto.Text = "0"

        '*************** DATOS DE CANCELACION
        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()

        Me.txtTotalRecibido.Text = "0"
        Me.txtFaltante.Text = "0"
        Me.txtVuelto.Text = "0"

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '*********** VEHICULO   
        Me.txtModelo_Vehiculo.Text = ""
        Me.txtPlaca_Vehiculo.Text = ""
        Me.txtCertificado_Vehiculo.Text = ""



        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIdPersona.Value = ""
        Me.hddIndex_GV_Detalle.Value = ""
        Me.hddIndexCambiarTipoPV_GV_Detalle.Value = ""
        Me.hdddoc_CompPercepcion.Value = ""
        Me.hddIndexGlosa_Gv_Detalle.Value = ""
        Me.hddIdDocumentoRef.Value = ""
        Me.hddIdMaestroObra.Value = ""
        Me.hddBusquedaMaestro.Value = "0"
        Me.hddPermiso_AddMedioPago.Value = "0"
        Me.hddSujetoRetencion.Value = "0"
        Me.hddIdVehiculo.Value = ""
        Me.hddIdTipoDocumentoRef.Value = "0"
        Me.hddIdTipoOperacionRef.Value = "0"

        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
        Me.GV_ComponenteKit.DataSource = Nothing
        Me.GV_ComponenteKit.DataBind()
        Me.GV_ComponenteKit_Find.DataSource = Nothing
        Me.GV_ComponenteKit_Find.DataBind()

        Me.GV_ImpresionDoc_Cab.DataSource = Nothing
        Me.GV_ImpresionDoc_Cab.DataBind()
        Me.GV_ImpresionDoc_Det.DataSource = Nothing
        Me.GV_ImpresionDoc_Det.DataBind()

        '***************** REGIMEN OPERACION
        Me.txtRetencion.Text = "0"
        Me.txtDetraccion.Text = "0"
        'Me.txtNroOperacion_Detraccion.Text = ""
        'Me.txtNroOperacion_Retencion.Text = ""

        '************* CONTROLES DEL PROCESO DE ADELANTO
        limpiarControles_Adelanto()

        '************* DESCUENTO GLOBAL
        Me.txtMonto_DescuentoGlobal.Text = "0"

        actualizarControlesMedioPago(1, CInt(Me.hddIdMedioPagoPrincipal.Value))


    End Sub

    Private Sub ActualizarBotonesControl()

        '*********** SE DESACTIVAN LOS PANELES SEGÚN PERMISOS
        Me.Panel_DocumentoRef.Enabled = False
        Me.Panel_MaestroObra.Enabled = False
        Me.Panel_Cliente.Enabled = False

        Me.Panel_PuntoPartida.Enabled = False
        Me.Panel_PuntoLlegada.Enabled = False
        Me.Panel_RegimenOperacion.Enabled = False
        Me.Panel_ValorRef_TRANSPORTE.Visible = False
        Me.Panel_ValorRef_TRANSPORTE.Enabled = False

        If (CInt(Me.cboTipoDocumento.SelectedValue) = _IdTipoDocumento_FacturaTR) Then
            Me.Panel_ValorRef_TRANSPORTE.Visible = True
        End If

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btncheckadd.Visible = False
                Me.btncheckdel.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnCambvende.Visible = False
                Me.btnchangepl.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                Me.btnImprimirGrupo.Visible = False
                Me.btnGuardarVende.Visible = False
                Me.btnaparecer.Visible = False
                Me.btnEmitirGuiaRemision.Visible = False
                Me.btnEmitirPercepcion.Visible = False
                Me.cboNuevoUsuario.Visible = False
                Me.btnRecalcular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False
                Me.lblnuevovendedor.Visible = False
                Me.btnEmitirGuiaRemision.Visible = False

                ''Agregado
                Me.Panel_Cab.Enabled = True
                Me.ckProductoComprometidos.Enabled = True
                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.cboEstado.Enabled = True
                Me.cboCaja.Enabled = True
                Me.cboUsuarioComision.Enabled = True
                Me.txtFechaEmision.Enabled = True
                Me.txtFechaAEntregar.Enabled = True
                Me.cboTipoDocumento.Enabled = True
                Me.cboAlmacenReferencia.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.txtCodigoDocumento.Enabled = True
                Me.btnBuscarDocumentoxCodigo.Enabled = True
                ''

                Me.Panel_Cab.Enabled = True
                Me.Panel_DatosCancelacion.Enabled = False

                Me.Panel_Obs.Enabled = False

                If (CInt(Me.hddPermiso_OnlyPanel_Cancelacion.Value) > 0) Then

                    Me.Panel_Cliente.Enabled = False


                    Me.Panel_PuntoPartida.Enabled = False
                    Me.Panel_PuntoLlegada.Enabled = False

                    Me.Panel_MaestroObra.Enabled = False
                    Me.Panel_DocumentoRef.Enabled = False

                End If

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboMoneda.Enabled = True
                Me.cboEstado.SelectedValue = "1"

                Me.ckProductoComprometidos.Visible = False

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btncheckadd.Visible = False
                Me.btncheckdel.Visible = False
                Me.btnCambvende.Visible = False
                Me.btnchangepl.Visible = False
                Me.btnaparecer.Visible = False
                Me.btnGuardarVende.Visible = False
                Me.cboNuevoUsuario.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                Me.btnImprimirGrupo.Visible = False
                Me.lblnuevovendedor.Visible = False
                Me.btnEmitirGuiaRemision.Visible = False
                Me.btnEmitirPercepcion.Visible = False

                '' agregado
                Me.Panel_Cab.Enabled = True
                Me.ckProductoComprometidos.Enabled = True
                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.cboEstado.Enabled = True
                Me.cboCaja.Enabled = True
                Me.cboUsuarioComision.Enabled = True
                Me.txtFechaEmision.Enabled = True
                Me.txtFechaAEntregar.Enabled = True
                Me.cboTipoDocumento.Enabled = True
                Me.cboAlmacenReferencia.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.txtCodigoDocumento.Enabled = True
                Me.btnBuscarDocumentoxCodigo.Enabled = True

                ''

                Me.btnRecalcular.Visible = True
                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DatosCancelacion.Enabled = True

                Me.Panel_Obs.Enabled = True
                Me.Panel_RegimenOperacion.Enabled = True
                Me.Panel_ValorRef_TRANSPORTE.Enabled = True
                Me.Panel_Vehiculo.Enabled = True
                If (CInt(Me.hddPermiso_OnlyPanel_Cancelacion.Value) > 0) Then

                    Me.Panel_Cliente.Enabled = True
                    Me.Panel_PuntoPartida.Enabled = True
                    Me.Panel_PuntoLlegada.Enabled = True
                    Me.Panel_MaestroObra.Enabled = True
                    Me.Panel_DocumentoRef.Enabled = True

                End If

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.cboEstado.SelectedValue = "1"

                Me.ckProductoComprometidos.Visible = False

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()


            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btncheckadd.Visible = False
                Me.btncheckdel.Visible = False
                Me.btnCambvende.Visible = False
                Me.btnchangepl.Visible = False
                Me.btnaparecer.Visible = False
                Me.btnGuardarVende.Visible = False
                Me.cboNuevoUsuario.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                Me.btnImprimirGrupo.Visible = False
                Me.lblnuevovendedor.Visible = False
                Me.btnEmitirGuiaRemision.Visible = False
                Me.btnEmitirPercepcion.Visible = False

                Me.btnRecalcular.Visible = True
                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_DatosCancelacion.Enabled = True
                Me.Panel_RegimenOperacion.Enabled = True
                Me.Panel_ValorRef_TRANSPORTE.Enabled = True
                If (CInt(Me.hddPermiso_OnlyPanel_Cancelacion.Value) > 0) Then
                    Me.Panel_Cliente.Enabled = True
                    Me.Panel_PuntoPartida.Enabled = True
                    Me.Panel_PuntoLlegada.Enabled = True
                    Me.Panel_MaestroObra.Enabled = True
                    Me.Panel_DocumentoRef.Enabled = True
                End If

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = True

                Me.ckProductoComprometidos.Visible = True

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btncheckdel.Visible = False
                Me.btncheckadd.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                Me.btnImprimirGrupo.Visible = False
                Me.btnCambvende.Visible = False
                Me.btnchangepl.Visible = False
                Me.btnaparecer.Visible = False
                Me.btnGuardarVende.Visible = False
                Me.btnEmitirGuiaRemision.Visible = False
                Me.btnEmitirPercepcion.Visible = False
                Me.cboNuevoUsuario.Visible = False
                Me.btnRecalcular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False
                Me.lblnuevovendedor.Visible = False


                Me.Panel_Cab.Enabled = True
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False

                If (CInt(Me.hddPermiso_OnlyPanel_Cancelacion.Value) > 0) Then
                    Me.Panel_Cliente.Enabled = False
                    Me.Panel_PuntoPartida.Enabled = False
                    Me.Panel_PuntoLlegada.Enabled = False

                    Me.Panel_MaestroObra.Enabled = False
                    Me.Panel_DocumentoRef.Enabled = False

                End If

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True

                Me.ckProductoComprometidos.Visible = False

            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btncheckadd.Visible = True
                Me.btncheckdel.Visible = True
                Me.btnCambvende.Visible = True
                Me.btnchangepl.Visible = True
                'Me.btnImprimir.Visible = True
                Me.btnImprimir.Visible = True

                'Me.btnImprimirDetallado.Visible = True
                Me.btnImprimirDetallado.Visible = False
                'Me.btnImprimirGrupo.Visible = True
                Me.btnImprimirGrupo.Visible = False
                Me.btnaparecer.Visible = False
                Me.btnGuardarVende.Visible = False

                Me.btnEmitirPercepcion.Visible = True
                Me.cboNuevoUsuario.Visible = False
                Me.btnRecalcular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False
                Me.lblnuevovendedor.Visible = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.btnEmitirGuiaRemision.Visible = False

                If (CInt(Me.hddPermiso_OnlyPanel_Cancelacion.Value) > 0) Then
                    Me.Panel_Cliente.Enabled = False


                    Me.Panel_PuntoPartida.Enabled = False
                    Me.Panel_PuntoLlegada.Enabled = False

                    Me.Panel_MaestroObra.Enabled = False
                    Me.Panel_DocumentoRef.Enabled = False
                End If
                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False

                Me.ckProductoComprometidos.Visible = True

            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btncheckadd.Visible = False
                Me.btncheckdel.Visible = False
                Me.btnRecalcular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False
                Me.btnCambvende.Visible = False
                Me.btnchangepl.Visible = False
                Me.btnaparecer.Visible = False
                Me.btnGuardarVende.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                Me.btnImprimirGrupo.Visible = False
                Me.cboNuevoUsuario.Visible = False
                Me.btnEmitirGuiaRemision.Visible = False
                Me.btnEmitirPercepcion.Visible = True
                Me.lblnuevovendedor.Visible = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False

                If (CInt(Me.hddPermiso_OnlyPanel_Cancelacion.Value) > 0) Then
                    Me.Panel_Cliente.Enabled = False


                    Me.Panel_PuntoPartida.Enabled = False
                    Me.Panel_PuntoLlegada.Enabled = False

                    Me.Panel_MaestroObra.Enabled = False
                    Me.Panel_DocumentoRef.Enabled = False
                    Me.Panel_Vehiculo.Enabled = False
                End If

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False

                Me.ckProductoComprometidos.Visible = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btncheckadd.Visible = False
                Me.btncheckdel.Visible = False
                Me.btnCambvende.Visible = False
                Me.btnchangepl.Visible = False
                Me.btnaparecer.Visible = False
                Me.btnGuardarVende.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                Me.btnImprimirGrupo.Visible = False
                Me.cboNuevoUsuario.Visible = False
                Me.btnEmitirGuiaRemision.Visible = False
                Me.btnEmitirPercepcion.Visible = False
                Me.lblnuevovendedor.Visible = False
                Me.btnRecalcular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False


                If (CInt(Me.hddPermiso_OnlyPanel_Cancelacion.Value) > 0) Then
                    Me.Panel_Cliente.Enabled = False


                    Me.Panel_PuntoPartida.Enabled = False
                    Me.Panel_PuntoLlegada.Enabled = False

                    Me.Panel_MaestroObra.Enabled = False
                    Me.Panel_DocumentoRef.Enabled = False
                End If

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False
                Me.cboEstado.SelectedValue = "2"

                Me.ckProductoComprometidos.Visible = False

        End Select
    End Sub


#Region "************************** BUSQUEDA PRODUCTO"
    'Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
    '    BuscarProductoCatalogo(2)
    'End Sub



    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                        If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                            IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                        End If
                    End If

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)
                    ViewState.Add("IdMoneda_BuscarProducto", Me.cboMoneda.SelectedValue)
                    ViewState.Add("IdTipoPV_BuscarProducto", IdTipoPV)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("CodBarras", Me.txtCodBarrasCapa.Text)
                    ViewState.Add("IdTipoExistencia", CInt(cmbTipoExistencia.SelectedValue))

                Case 1
                    '******************* Cuando se cambia el almacen de busqueda
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)

                Case 2
                    '******************** cuando se cambia la tienda de precios
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), _
            CStr(ViewState("CodBarras")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, _
                                          ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, _
                                          ByVal nomProducto As String, ByVal IdAlmacen As Integer, _
                                          ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, _
                                          ByVal IdMoneda As Integer, ByVal tipoMov As Integer, _
                                          ByVal IdEmpresa As Integer, ByVal codBarras As String, _
                                          ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        'Aqui se comento a pedido de cristhian panta 
        Dim IdMedioPago As Integer = 0
        'If (CInt(Me.cboCondicionPago.SelectedValue) = 1) Then  '************ CONTADO
        IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
        'End If

        Dim IdCliente As Integer = 0
        If (IsNumeric(Me.hddIdPersona.Value) And (Me.hddIdPersona.Value.Trim.Length) > 0) Then
            IdCliente = CInt(Me.hddIdPersona.Value)
        End If

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        'Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))

        Dim filtroProductoCampania As Boolean = Me.chb_FiltroProductoCampania.Checked

        'Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania)
        Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarras(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, codBarras, filtroProductoCampania, IdTipoExistencia)

        If Me.listaCatalogo.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            setListaCatalogo(Me.listaCatalogo)
            grilla.DataSource = Me.listaCatalogo
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), _
            CStr(ViewState("CodBarras")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), _
            CStr(ViewState("CodBarras")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), _
            CStr(ViewState("CodBarras")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
                Dim cboUM As DropDownList = CType(gvrow.FindControl("cmbUnidadMedida_AddProd"), DropDownList)
                Me.listaCatalogo(e.Row.RowIndex).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(Me.listaCatalogo(e.Row.RowIndex).IdProducto, CInt(Me.cboTienda.SelectedValue), Me.listaCatalogo(e.Row.RowIndex).IdTipoPV)
                cboUM.DataSource = Me.listaCatalogo(e.Row.RowIndex).ListaUM_Venta
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaCatalogo(e.Row.RowIndex).IdUnidadMedida.ToString
                setListaCatalogo(Me.listaCatalogo)

                If (Me.listaCatalogo(e.Row.RowIndex).Kit) Then  '********* KIT

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = False

                End If


                Dim btnVerCampania As ImageButton = CType(e.Row.FindControl("btnViewCampania"), ImageButton)
                If (Me.listaCatalogo(e.Row.RowIndex).ExisteCampania_Producto) Then
                    btnVerCampania.Visible = True
                Else
                    btnVerCampania.Visible = False
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            ActualizarListaCatalogo()

            Me.listaCatalogo = Me.getListaCatalogo
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex
            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaCatalogo(i).IdUnidadMedida, Me.listaCatalogo(i).IdProducto, _
             IdTipoPV, Me.listaCatalogo(i).IdTienda, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text))

            If objCatalogo IsNot Nothing Then

                Me.listaCatalogo(i).PrecioSD = objCatalogo.PrecioSD
                Me.listaCatalogo(i).PrecioLista = objCatalogo.PrecioLista
                Me.listaCatalogo(i).StockDisponibleN = objCatalogo.StockDisponibleN
                Me.listaCatalogo(i).pvComercial = objCatalogo.pvComercial


            End If

            setListaCatalogo(Me.listaCatalogo)

            DGV_AddProd.DataSource = Me.listaCatalogo
            DGV_AddProd.DataBind()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub ActualizarListaCatalogo()

        Me.listaCatalogo = getListaCatalogo()

        For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

            If (CType(DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text.Trim.Length > 0) Then
                Me.listaCatalogo(i).IdUnidadMedida = CInt(CType(DGV_AddProd.Rows(i).Cells(3).FindControl("cmbUnidadMedida_AddProd"), DropDownList).SelectedValue)
                Me.listaCatalogo(i).Cantidad = CDec(CType(DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)
            End If

        Next

        setListaCatalogo(Me.listaCatalogo)

    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            Dim IdTipoPv As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            If (IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0) Then
                If (CInt(Me.cboTipoPrecioV.SelectedValue) > 0) Then
                    IdTipoPv = CInt(Me.cboTipoPrecioV.SelectedValue)
                End If
            End If

            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, IdTipoPv, CInt(Me.cboMoneda.SelectedValue), (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd'); CalcularConsultarStockPrecioxProd();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub mostrarCapaConsultarTipoPrecio(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProducto_ConsultarTipoPrecio.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Me.lblTienda_ConsultarTipoPrecio.Text = CStr(Me.cboTienda.SelectedItem.ToString)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            Dim IdTienda As Integer = CInt(Me.cboTienda.SelectedValue)

            Me.GV_ConsultarTipoPrecio.DataSource = (New Negocio.DocumentoCotizacion).DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, CInt(Me.cboMoneda.SelectedValue), CDate(Me.txtFechaEmision.Text))
            Me.GV_ConsultarTipoPrecio.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarTipoPrecio');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub mostrarCapaConsultarTipoPrecio_CapaCambiarTipoPV(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim indexGrilla As Integer = CInt(Me.hddIndexCambiarTipoPV_GV_Detalle.Value)
            Me.lblProducto_ConsultarTipoPrecio.Text = HttpUtility.HtmlDecode(CType(Me.GV_Detalle.Rows(indexGrilla).FindControl("lblDescripcion"), Label).Text)
            Me.lblTienda_ConsultarTipoPrecio.Text = CStr(Me.cboTienda.SelectedItem.ToString)
            Dim IdProducto As Integer = CInt(CType(Me.GV_Detalle.Rows(indexGrilla).FindControl("hddIdProducto"), HiddenField).Value)
            Dim IdTienda As Integer = CInt(Me.cboTienda.SelectedValue)

            Me.GV_ConsultarTipoPrecio.DataSource = (New Negocio.DocumentoCotizacion).DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, CInt(Me.cboMoneda.SelectedValue), CDate(Me.txtFechaEmision.Text))
            Me.GV_ConsultarTipoPrecio.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarTipoPrecio');   onCapa('capaCambiarTipoPrecioPV');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#End Region
    Private Sub cmbTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTipoExistencia.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(cmbTipoExistencia.SelectedValue), True)
            objCbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cmbTipoExistencia.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Dim objCbo As New Combo
        objCbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cmbTipoExistencia.SelectedValue), True)
        objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

        objScript.onCapa(Me, "capaBuscarProducto_AddProd")
    End Sub
    Protected Sub cmbSubLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#Region "****************Buscar Personas Mantenimiento"
    Private Sub btnGuardar_EditarCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar_EditarCliente.Click
        editarCliente()
    End Sub
    Private Sub editarCliente()
        Try
            Dim objPersonaView As Entidades.PersonaView = obtenerPersonaView()
            If ((New Negocio.Cliente).PersonaUpdate_Ventas(objPersonaView)) Then
                cargarPersona(objPersonaView.IdPersona, False)
            Else
                Throw New Exception("Problemas en la Edición del Cliente.")
            End If

            'If (Me.GV_Detalle.Rows.Count > 0) Then
            '    Throw New Exception("DEBE QUITAR EL DETALLE DEL DOCUMENTO. NO SE PERMITE LA OPERACIÓN.")
            'End If
            'If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Juridica", "jur_Rsocial", objPersonaView.RazonSocial, "IdPersona", objPersonaView.IdPersona) <= 0 Then
            'Else
            'objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
            'End If
            'Try

            '    If (Me.GV_Detalle.Rows.Count > 0) Then
            '        Throw New Exception("DEBE QUITAR EL DETALLE DEL DOCUMENTO. NO SE PERMITE LA OPERACIÓN.")
            '    End If
            '    Dim objPersonaView As Entidades.PersonaView = obtenerPersonaView()
            '    If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Juridica", "jur_Rsocial", objPersonaView.RazonSocial, "IdPersona", objPersonaView.IdPersona) <= 0 Then
            '        If ((New Negocio.Cliente).PersonaUpdate_Ventas(objPersonaView)) Then
            '            cargarPersona(objPersonaView.IdPersona)
            '        Else
            '            Throw New Exception("Problemas en la Edición del Cliente.")
            '        End If
            '    Else
            '        objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
            '    End If
            'Catch ex As Exception
            '    objScript.mostrarMsjAlerta(Me, ex.Message)
            'End Try
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerPersonaView() As Entidades.PersonaView

        Dim objPersonaView As New Entidades.PersonaView
        With objPersonaView

            .IdPersona = CInt(Me.txtCodigoCliente.Text.Trim)
            .RazonSocial = Me.txtRazonSocial_EditarCliente.Text.Trim
            .ApPaterno = Me.txtApPaterno_EditarCliente.Text.Trim
            .ApMaterno = Me.txtApMaterno_EditarCliente.Text.Trim
            .Nombres = Me.txtNombres_EditarCliente.Text.Trim
            .Ruc = Me.txtRUC_EditarCliente.Text.Trim
            .Direccion = Me.txtDireccion_EditarCliente.Text.Trim()
            .Dni = Me.txtDNI_EditarCliente.Text.Trim
            .IdTipoAgente = CInt(Me.cboTipoAgente_EditarCliente.SelectedValue)

            .TipoPersona = CInt(Me.cboTipoPersona_PanelCliente.SelectedValue)
            '***** 0: --------
            '***** 1: Natural
            '***** 2: Jurídica

        End With
        Return objPersonaView
    End Function


    Private Sub btnBuscarClientexIdCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarClientexIdCliente.Click

        Try
            Me.DGV_AddProd.DataSource = Nothing
            Me.DGV_AddProd.DataBind()
            cargarPersona(CInt(txtIdCliente_BuscarxId.Text))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count = 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
        End If
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try



            '************ Limpiamos la grilla de productos
            Me.DGV_AddProd.DataSource = Nothing
            Me.DGV_AddProd.DataBind()

            If (CInt(Me.hddBusquedaMaestro.Value) = 1) Then  '***************** Búsqueda de Maestros

                Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
                Me.txtMaestro.Text = objPersona.Descripcion
                Me.txtRuc_Maestro.Text = objPersona.Ruc
                Me.txtDni_Maestro.Text = objPersona.Dni
                Me.hddIdMaestroObra.Value = CStr(objPersona.IdPersona)

                Me.hddBusquedaMaestro.Value = "0"

            Else
                cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    '********************* CARGAMOS DATOS AL FRM DE COTIZACION SEGUN PERSONA SELECCIONADA

    Protected Sub btnGuardarCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardarCliente.Click
        RegistrarCliente()
    End Sub
    Private Sub RegistrarCliente()
        Try

            Dim objPersonaView As New Entidades.PersonaView
            With objPersonaView

                Select Case Me.cboTipoPersona_RegistrarCliente.SelectedValue
                    Case "N"
                        .TipoPersona = 0
                    Case "J"
                        .TipoPersona = 1
                End Select

                .IdTipoAgente = CInt(Me.cmbTipoAgente_RegistrarCliente.SelectedValue)
                .RazonSocial = Me.txtRazonSocial_Cliente.Text.Trim
                .ApPaterno = Me.txtApPaterno_Cliente.Text.Trim
                .ApMaterno = Me.txtApMaterno_Cliente.Text.Trim
                .Nombres = Me.txtNombres_Cliente.Text.Trim
                .Dni = Me.txtDNI_Cliente.Text.Trim
                .Ruc = Me.txtRUC_Cliente.Text.Trim
                .Direccion = Me.txtDireccion_Cliente.Text.Trim
                .Telefeono = Me.txtTelefono_Cliente.Text.Trim
                .EMail = Me.txtMail_Cliente.Text

                Select Case Me.cboTipoPersona_RegistrarCliente.SelectedValue
                    Case "N"
                        validar_RegistrarCliente(objPersonaView)
                        .IdPersona = (New Negocio.Persona).RegistrarCliente_Ventas(objPersonaView, Me.cboTipoPersona_RegistrarCliente.SelectedValue, CInt(Me.cboEmpresa.SelectedValue))
                    Case "J"
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Juridica", "jur_Rsocial", CStr(IIf(objPersonaView.RazonSocial Is Nothing, "xzxzxz", objPersonaView.RazonSocial))) <= 0 Then
                            validar_RegistrarCliente(objPersonaView)
                            .IdPersona = (New Negocio.Persona).RegistrarCliente_Ventas(objPersonaView, Me.cboTipoPersona_RegistrarCliente.SelectedValue, CInt(Me.cboEmpresa.SelectedValue))
                        Else
                            objScript.mostrarMsjAlerta(Me, "La Razon social ya existe")
                            objScript.offCapa(Me, "capaRegistrarCliente")
                        End If
                End Select

            End With

            If objPersonaView.IdPersona <> Nothing Then
                cargarPersona(objPersonaView.IdPersona)
                objScript.offCapa(Me, "capaRegistrarCliente")
                'Else
                '    Throw New Exception("Problemas en la Operación.")
            End If


        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('" + ex.Message.Replace(vbCrLf, " ").Replace(vbCr, " ") + "'); onCapa('capaRegistrarCliente'); activarPanel_V1(); ", True)
        End Try
    End Sub

    Private Sub validar_RegistrarCliente(ByVal objPersonaView As Entidades.PersonaView)

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), (New Integer() {242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254}))

        Select Case objPersonaView.TipoPersona
            Case 0  '**************  NATURAL

                If (listaPermisos(5) > 0 And objPersonaView.ApPaterno.Trim.Length <= 0) Then
                    Throw New Exception("< Ap. Paterno > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(6) > 0 And objPersonaView.ApMaterno.Trim.Length <= 0) Then
                    Throw New Exception("< Ap. Materno > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(7) > 0 And objPersonaView.Nombres.Trim.Length <= 0) Then
                    Throw New Exception("< Nombres > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(8) > 0 And objPersonaView.Dni.Trim.Length <= 0) Then
                    Throw New Exception("< D.N.I. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(9) > 0 And objPersonaView.Ruc.Trim.Length <= 0) Then
                    Throw New Exception("< R.U.C. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(10) > 0 And objPersonaView.Direccion.Trim.Length <= 0) Then
                    Throw New Exception("< Dirección > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(11) > 0 And objPersonaView.Telefeono.Trim.Length <= 0) Then
                    Throw New Exception("< Teléfono > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(12) > 0 And objPersonaView.EMail.Trim.Length <= 0) Then
                    Throw New Exception("< E - Mail > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

            Case 1  '**************  JURIDICA

                If (listaPermisos(0) > 0 And objPersonaView.RazonSocial.Trim.Length <= 0) Then
                    Throw New Exception("< Razón Social > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(1) > 0 And objPersonaView.Telefeono.Trim.Length <= 0) Then
                    Throw New Exception("< Teléfono > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(2) > 0 And objPersonaView.Ruc.Trim.Length <= 0) Then
                    Throw New Exception("< R.U.C. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(3) > 0 And objPersonaView.Direccion.Trim.Length <= 0) Then
                    Throw New Exception("< Dirección > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(4) > 0 And objPersonaView.EMail.Trim.Length <= 0) Then
                    Throw New Exception("< E - Mail > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

        End Select

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer, Optional ByVal valGv_Detalle As Boolean = True)



        If (Me.GV_Detalle.Rows.Count > 0) And valGv_Detalle Then
            Throw New Exception("DEBE QUITAR EL DETALLE DEL DOCUMENTO. NO SE PERMITE LA OPERACIÓN.")
        End If


        Dim objPersona As Entidades.PersonaView = (New Negocio.Cliente).SelectxId_Ventas(IdPersona, CInt(Me.cboEmpresa.SelectedValue))

        '***************** CARGAMOS DATOS CLIENTE

        If (cboTipoDocumento.SelectedValue = 1 And objPersona.Ruc = "") Then

            Throw New Exception("DEBE SEÑALAR UN CLIENTE CON RUC")
            Return
        End If
        If (cboTipoDocumento.SelectedValue = 1101353001 And objPersona.Ruc = "") Then

            Throw New Exception("DEBE SEÑALAR UN CLIENTE CON RUC")
            Return
        End If

        Me.txtApPaterno_RazonSocial.Text = objPersona.RazonSocial + objPersona.ApPaterno
        Me.txtApMaterno.Text = objPersona.ApMaterno
        Me.txtNombres.Text = objPersona.Nombres
        Me.txtCodigoCliente.Text = CStr(objPersona.IdPersona)
        Me.txtIdCliente_BuscarxId.Text = CStr(objPersona.IdPersona)
        Me.txtDni.Text = objPersona.Dni
        Me.txtRuc.Text = objPersona.Ruc
        Me.cboTipoPrecioV.SelectedValue = CStr(objPersona.IdTipoPV)
        Me.txtDireccionCliente.Text = objPersona.Direccion
        Me.cboTipoAgente.SelectedValue = CStr(objPersona.IdTipoAgente)
        Me.txtTasaAgente.Text = CStr(Math.Round(objPersona.PorcentTipoAgente, 3))
        Me.cboTipoPersona_PanelCliente.SelectedValue = CStr(objPersona.TipoPersona)

        '*************** HIDDEN
        Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

        '************** LINEA DE CREDITO
        Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
        Me.GV_LineaCredito.DataBind()

        '**************** DATOS DE ENTREGAR POR DEFECTO
        Dim ubigeo As String = objPersona.Ubigeo
        If (ubigeo.Trim.Length <= 0) Then
            ubigeo = "000000"
        End If
        Dim codDepto As String = ubigeo.Substring(0, 2)
        Dim codProv As String = ubigeo.Substring(2, 2)
        Dim codDist As String = ubigeo.Substring(4, 2)
        Dim objCbo As New Combo
        Me.cboDepartamento.SelectedValue = codDepto
        objCbo.LLenarCboProvincia(Me.cboProvincia, codDepto)
        Me.cboProvincia.SelectedValue = codProv
        objCbo.LLenarCboDistrito(Me.cboDistrito, codDepto, codProv)
        Me.cboDistrito.SelectedValue = codDist
        Me.txtDireccion_Llegada.Text = objPersona.Direccion


        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()

        Me.listaCancelacion = New List(Of Entidades.PagoCaja)
        setListaCancelacion(Me.listaCancelacion)

        '**************** SUJETO A RETENCION
        If (objPersona.SujetoARetencion) Then
            Me.hddSujetoRetencion.Value = "1"
        Else
            Me.hddSujetoRetencion.Value = "0"
        End If

        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region



    Protected Sub btnAddDetalleCobro_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDetalleCobro.Click
        addDetalleConcepto()
    End Sub
    Private Sub addDetalleConcepto()
        Try

            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(1)

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularTotalConcepto(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaDetalleConcepto(ByVal cantAdd As Integer) As List(Of Entidades.DetalleConcepto)
        Dim lista As New List(Of Entidades.DetalleConcepto)

        Dim listaConcepto As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.cboTipoDocumento.SelectedValue))
        listaConcepto.Insert(0, (New Entidades.Concepto(0, "-----")))

        For i As Integer = 0 To Me.GV_Concepto.Rows.Count - 1
            Dim obj As New Entidades.DetalleConcepto
            With obj
                .IdConcepto = CInt(CType(Me.GV_Concepto.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                .Concepto = CStr(CType(Me.GV_Concepto.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text)
                .Monto = CDec(CType(Me.GV_Concepto.Rows(i).FindControl("txtMonto"), TextBox).Text)

                .PorcentDetraccion = CDec(CType(Me.GV_Concepto.Rows(i).FindControl("hddPorcentDetraccion"), HiddenField).Value)
                .MontoMinDetraccion = CDec(CType(Me.GV_Concepto.Rows(i).FindControl("hddMontoMinimoDetraccion"), HiddenField).Value)

                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)

                .ConceptoAdelanto = CBool(CInt(CType(Me.GV_Concepto.Rows(i).FindControl("hddConceptoAdelanto"), HiddenField).Value))

                .NoAfectoIGV = CBool(CInt(CType(Me.GV_Concepto.Rows(i).FindControl("hddNoAfectoIgv"), HiddenField).Value))
                If .IdConcepto <= 0 Then .NoAfectoIGV = False

                .PorcentDsctoGlobal = CDec(CType(Me.GV_Concepto.Rows(i).FindControl("hddPorcentDsctoGlobal"), HiddenField).Value)

                Try : .IdDocumentoRef = CInt(CType(Me.GV_Concepto.Rows(i).FindControl("hddIdDocumentoRef"), HiddenField).Value) : Catch ex As Exception : .IdDocumentoRef = Nothing : End Try

                .ListaConcepto = listaConcepto


            End With
            lista.Add(obj)
        Next
        For i As Integer = 0 To cantAdd - 1
            Dim obj As New Entidades.DetalleConcepto
            With obj

                .IdConcepto = 0
                .Descripcion = ""
                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .Monto = 0
                .ConceptoAdelanto = False
                .ListaConcepto = listaConcepto
                .NoAfectoIGV = 0
            End With
            lista.Add(obj)
        Next

        Return lista
    End Function

    Private Sub GV_Concepto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Concepto.RowDataBound
        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim cboConcepto As DropDownList = CType(e.Row.FindControl("cboConcepto"), DropDownList)
                cboConcepto.DataSource = Me.listaDetalleConcepto(e.Row.RowIndex).ListaConcepto
                cboConcepto.DataBind()
                If (Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto <> 0) Then
                    cboConcepto.SelectedValue = CStr(Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto)
                End If

                Dim desactivarControles As Boolean = False

                If Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto = 10 Then desactivarControles = True


                If (CInt(CType(e.Row.FindControl("hddConceptoAdelanto"), HiddenField).Value) = 1) Then  '******* CONCEPTO ANTICIPO

                    desactivarControles = True
                    CType(e.Row.FindControl("btnAdelanto"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnAdelanto"), ImageButton).Visible = False

                End If

                If desactivarControles Then

                    '************ Desactivamos los controles
                    cboConcepto.Enabled = False
                    CType(e.Row.FindControl("txtDescripcionConcepto"), TextBox).Enabled = False
                    CType(e.Row.FindControl("txtMonto"), TextBox).Enabled = False

                End If


            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Concepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Concepto.SelectedIndexChanged
        quitarDetalleConcepto(Me.GV_Concepto.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDetalleConcepto(ByVal Index As Integer)
        Try

            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)

            If Me.listaDetalleConcepto(Index).IdConcepto = 10 Then Me.txtDescuentoGlobal.Text = "0"
            Me.listaDetalleConcepto.RemoveAt(Index)

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularTotalConcepto(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub cboCondicionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCondicionPago.SelectedIndexChanged
        Try

            If (Me.GV_Cancelacion.Rows.Count > 0) Then
                Throw New Exception("Se han ingresado datos de Cancelación. No se permite la Operación.")
            End If

            actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago.SelectedValue))
            actualizarPorcentDctoMax_xArticulo_GV_Detalle()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub ActualizarListaDetalleDocumento(Optional ByVal index As Integer = -1)
        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            If index = i Then Me.listaDetalleDocumento(i).IdUMold = Me.listaDetalleDocumento(i).IdUnidadMedida

            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUM"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUM"), DropDownList).SelectedItem.ToString)
            Me.listaDetalleDocumento(i).PrecioSD = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtPrecioVenta"), TextBox).Text)
            Me.listaDetalleDocumento(i).Descuento = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtDescuento_Valor"), TextBox).Text)
            Me.listaDetalleDocumento(i).PorcentDcto = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtDescuento_Porcent"), TextBox).Text)
            Me.listaDetalleDocumento(i).PrecioCD = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtPrecioVentaFinal"), TextBox).Text)
            Me.listaDetalleDocumento(i).Importe = Me.listaDetalleDocumento(i).Cantidad * Math.Round(Me.listaDetalleDocumento(i).PrecioCD, 2)
            Me.listaDetalleDocumento(i).DctoValor_MaximoxPVenta = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtDctoValor_MaximoxPVenta"), TextBox).Text)
            Me.listaDetalleDocumento(i).DctoPorcent_MaximoxPVenta = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtDctoPorcent_MaximoxPVenta"), TextBox).Text)
            Me.listaDetalleDocumento(i).CantxAtender = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaDetalleDocumento(i).prod_CodigoBarras = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("hddcodBarraProductoDetDoc"), HiddenField).Value)

        Next


        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub
    Dim valCant As Boolean
    Private Sub addProducto_DetalleDocumento()
        Try

            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaCatalogo()
            ActualizarListaDetalleDocumento()

            Me.listaCatalogo = getListaCatalogo()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            For i As Integer = 0 To Me.listaCatalogo.Count - 1

                If (Me.listaCatalogo(i).Cantidad > 0) Then

                    valCant = False
                    For k As Integer = 0 To listaDetalleDocumento.Count - 1

                        If listaDetalleDocumento(k).IdProducto = listaCatalogo(i).IdProducto And listaDetalleDocumento(k).IdUnidadMedida = listaCatalogo(i).IdUnidadMedida Then
                            listaDetalleDocumento(k).Cantidad = Me.listaCatalogo(i).Cantidad

                            valCant = True
                            Exit For

                        End If

                    Next

                    If valCant = False Then
                        Dim objDetalle As New Entidades.DetalleDocumento
                        With objDetalle

                            .IdProducto = Me.listaCatalogo(i).IdProducto
                            .NomProducto = Me.listaCatalogo(i).Descripcion
                            .Cantidad = Me.listaCatalogo(i).Cantidad
                            .ListaUM_Venta = Me.listaCatalogo(i).ListaUM_Venta
                            .IdUnidadMedida = Me.listaCatalogo(i).IdUnidadMedida
                            .StockDisponibleN = Me.listaCatalogo(i).StockDisponibleN
                            .PrecioLista = Me.listaCatalogo(i).PrecioLista
                            .PrecioSD = Me.listaCatalogo(i).PrecioSD
                            .Descuento = 0
                            .PorcentDcto = 0
                            .PrecioCD = Me.listaCatalogo(i).PrecioSD
                            .Importe = .Cantidad * .PrecioCD
                            .TasaPercepcion = Me.listaCatalogo(i).Percepcion
                            .PorcentDctoMaximo = Me.listaCatalogo(i).PorcentDctoMaximo
                            .PrecioBaseDcto = Me.listaCatalogo(i).PrecioBaseDcto
                            .IdUsuarioSupervisor = 0
                            .IdTienda = Me.listaCatalogo(i).IdTienda
                            .IdTipoPV = Me.listaCatalogo(i).IdTipoPV
                            .pvComercial = Me.listaCatalogo(i).pvComercial
                            .CodigoProducto = Me.listaCatalogo(i).CodigoProducto

                            .Kit = Me.listaCatalogo(i).Kit
                            .ExisteCampania_Producto = Me.listaCatalogo(i).ExisteCampania_Producto
                            .prod_CodigoBarras = listaCatalogo(i).Prod_CodigoBarras

                        End With
                        Me.listaDetalleDocumento.Add(objDetalle)
                    End If

                End If
            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);   onCapa('capaBuscarProducto_AddProd');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then

                'Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUM"), DropDownList)
                'cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta
                'cboUM.DataBind()

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUM"), DropDownList)
                If Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta Is Nothing Then
                    Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(Me.listaDetalleDocumento(e.Row.RowIndex).IdProducto, CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoPrecioV.SelectedValue))
                    cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta
                    cboUM.DataBind()
                    If (cboUM.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                        cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString
                    End If
                Else
                    cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta
                    cboUM.DataBind()
                    If (cboUM.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                        cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString
                    End If
                End If


                If (cboUM.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                    cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString
                End If

                If (Me.listaDetalleDocumento(e.Row.RowIndex).VolumenVentaMin > 0) Then
                    cboUM.Enabled = False
                End If

                If (Me.listaDetalleDocumento(e.Row.RowIndex).Kit) Then  '********* KIT

                    CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton).Visible = False

                End If

                Dim btnVerCampania As ImageButton = CType(e.Row.FindControl("btnViewCampania"), ImageButton)
                If (Me.listaDetalleDocumento(e.Row.RowIndex).ExisteCampania_Producto) And Me.listaDetalleDocumento(e.Row.RowIndex).PorcentDctoMaximo < 100 Then
                    btnVerCampania.Visible = True
                Else
                    btnVerCampania.Visible = False
                End If

                '***************** VALIDAMOS BOTONES DE RETAZO
                Dim btnRetazo_Longitud As ImageButton = CType(e.Row.FindControl("btnLongitud"), ImageButton)
                Dim btnRetazo_Area As ImageButton = CType(e.Row.FindControl("btnArea"), ImageButton)
                btnRetazo_Longitud.Visible = False
                btnRetazo_Area.Visible = False

                If (cboUM.Items.FindByValue(CStr(Me.hddIdUnidadMedida_RetazoLongitud.Value)) IsNot Nothing) Then
                    btnRetazo_Longitud.Visible = True
                End If
                If (cboUM.Items.FindByValue(CStr(Me.hddIdUnidadMedida_RetazoArea.Value)) IsNot Nothing) Then
                    btnRetazo_Area.Visible = True
                End If



            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub




    Private Sub actualizarPorcentDctoMax_xArticulo_GV_Detalle()

        Dim IdUsuario As Integer = CInt(Session("IdUsuario"))
        Dim IdCondicionPago As Integer = CInt(Me.cboCondicionPago.SelectedValue)
        Dim IdMedioPago As Integer = 0

        Select Case IdMedioPago
            Case 1 '***************************************************************** CONTADO
                IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            Case 2 '***************************************************************** CREDITO
                IdMedioPago = CInt(Me.cboMedioPago_Credito.SelectedValue)
        End Select

        Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
        If IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
            If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
            End If
        End If

        ActualizarListaDetalleDocumento()
        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            If Me.listaDetalleDocumento(i).PorcentDctoMaximo < 100 Then


                If (Me.listaDetalleDocumento(i).IdUsuarioSupervisor <> Nothing) Then
                    Me.listaDetalleDocumento(i).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuento).getPorcentDctoMax(Me.listaDetalleDocumento(i).IdUsuarioSupervisor, IdTipoPV, IdCondicionPago, IdMedioPago)
                    Me.listaDetalleDocumento(i).PrecioBaseDcto = (New Negocio.ConfiguracionDescuento).getPrecioBaseDcto(Me.listaDetalleDocumento(i).IdUsuarioSupervisor, IdTipoPV, IdCondicionPago, IdMedioPago)
                Else
                    Me.listaDetalleDocumento(i).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuento).getPorcentDctoMax(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
                    Me.listaDetalleDocumento(i).PrecioBaseDcto = (New Negocio.ConfiguracionDescuento).getPrecioBaseDcto(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
                End If

            End If

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

    End Sub





    'Private Function obtener_Cad_IdProducto(ByVal lista As List(Of Entidades.DetalleDocumento)) As String

    '    Dim cad As String = ""
    '    For i As Integer = 0 To lista.Count - 1
    '        If (i = lista.Count - 1) Then
    '            cad = cad + CStr(lista(i).IdProducto)
    '        Else
    '            cad = cad + CStr(lista(i).IdProducto) + "/"
    '        End If
    '    Next
    '    Return cad
    'End Function

    'Private Function obtener_Cad_IdUnidadMedida(ByVal lista As List(Of Entidades.DetalleDocumento)) As String

    '    Dim cad As String = ""
    '    For i As Integer = 0 To lista.Count - 1
    '        If (i = lista.Count - 1) Then
    '            cad = cad + CStr(lista(i).IdUnidadMedida)
    '        Else
    '            cad = cad + CStr(lista(i).IdUnidadMedida) + "/"
    '        End If
    '    Next
    '    Return cad
    'End Function
    'Private Function obtener_Cad_Cantidad(ByVal lista As List(Of Entidades.DetalleDocumento)) As String

    '    Dim cad As String = ""
    '    For i As Integer = 0 To lista.Count - 1
    '        If (i = lista.Count - 1) Then
    '            cad = cad + CStr(lista(i).Cantidad)
    '        Else
    '            cad = cad + CStr(lista(i).Cantidad) + "/"
    '        End If
    '    Next
    '    Return cad
    'End Function
    Private Sub btnAceptar_AddDctoAdicional_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_AddDctoAdicional.Click
        addDctoAdicionalxArticulo_GV_Detalle()
    End Sub
    Private Sub addDctoAdicionalxArticulo_GV_Detalle()
        Try

            Dim IdUsuario As Integer = (New Negocio.Usuario).ValidarIdentidad(Me.txtLogin_AddDctoAdicional.Text.Trim, Me.txtClave_AddDctoAdicional.Text.Trim)

            If (IdUsuario = Nothing) Then
                Throw New Exception("USUARIO Y/O CLAVE INCORRECTOS. NO SE PERMITE LA OPERACIÓN.")
            End If

            Dim IdCondicionPago As Integer = CInt(Me.cboCondicionPago.SelectedValue)
            Dim IdMedioPago As Integer = 0
            If (IdCondicionPago = 1) Then  '************* CONTADO
                IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            End If

            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            If IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento(index).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuento).getPorcentDctoMax(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
            Me.listaDetalleDocumento(index).PrecioBaseDcto = (New Negocio.ConfiguracionDescuento).getPrecioBaseDcto(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
            Me.listaDetalleDocumento(index).IdUsuarioSupervisor = IdUsuario

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAddGlosa_CapaGlosa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddGlosa_CapaGlosa.Click

        addGlosaDetalle()

    End Sub
    Private Sub addGlosaDetalle()
        Try

            Dim glosa As String = Me.txtGlosa_CapaGlosa.Text.Trim

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim index As Integer = CInt(Me.hddIndexGlosa_Gv_Detalle.Value)
            Me.listaDetalleDocumento(index).DetalleGlosa = Me.txtGlosa_CapaGlosa.Text.Trim

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cboConcepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        addConcepto_Procesar(CType(sender, DropDownList))

    End Sub

    Private Sub addConcepto_Procesar(ByVal cboConcepto As DropDownList)

        Dim index As Integer = CType(cboConcepto.NamingContainer, GridViewRow).RowIndex
        Dim ExisteConceptoAfectoIGV As Boolean = False
        Dim ExisteConceptoNoAfectoIGV As Boolean = False

        Try

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()



            Dim ubigeoOrigen As String = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue
            Dim ubigeoDestino As String = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue
            Dim pesoTotal As Decimal = obtenerPesoTotal(Me.listaDetalleDocumento)
            Dim importeTotal As Decimal = CDec(Me.txtTotal.Text.Trim)
            Dim valor As Decimal = (New Negocio.DocumentoCotizacion).SelectValorEstimadoConcepto(CInt(cboConcepto.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue), ubigeoOrigen, ubigeoDestino, CInt(Me.cboMoneda.SelectedValue), pesoTotal, importeTotal, CDate(Me.txtFechaEmision.Text))


            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)

            Dim objConcepto As Entidades.Concepto = (New Negocio.Concepto).SelectxId(Me.listaDetalleConcepto(index).IdConcepto)(0)


            For p As Integer = 0 To Me.listaDetalleConcepto.Count - 1

                If Me.listaDetalleConcepto(p).IdConcepto > 0 And p <> index Then

                    If Me.listaDetalleConcepto(p).NoAfectoIGV = False Then ExisteConceptoAfectoIGV = True
                    If Me.listaDetalleConcepto(p).NoAfectoIGV Then ExisteConceptoNoAfectoIGV = True

                End If

            Next

            If objConcepto.con_NoAfectoIGV And ExisteConceptoAfectoIGV Then
                Throw New Exception("La lista de conceptos contiene registros afectos a I.G.V. \n No se permite la operación.")
            End If

            If objConcepto.con_NoAfectoIGV = False And ExisteConceptoNoAfectoIGV Then
                Throw New Exception("La lista de conceptos contiene registros no afectos a I.G.V. \n No se permite la operación.")
            End If

            If objConcepto.con_NoAfectoIGV And Me.listaDetalleDocumento.Count > 0 Then
                Throw New Exception("El concepto [" + HttpUtility.HtmlDecode(objConcepto.Nombre).Trim + "] no esta afecto a I.G.V. \n El Documento posee detalles de productos. \n No se permite la operación.")
            End If

            Me.listaDetalleConcepto(index).Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
            Me.listaDetalleConcepto(index).Monto = CDec(valor)
            Me.listaDetalleConcepto(index).IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            Me.listaDetalleConcepto(index).ConceptoAdelanto = objConcepto.ConceptoAdelanto
            Me.listaDetalleConcepto(index).PorcentDetraccion = objConcepto.PorcentDetraccion
            Me.listaDetalleConcepto(index).MontoMinDetraccion = objConcepto.MontoMinimoDetraccion

            Me.listaDetalleConcepto(index).NoAfectoIGV = objConcepto.con_NoAfectoIGV

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularTotalConcepto(); ", True)

        Catch ex As Exception

            cboConcepto.SelectedValue = "0"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularTotalConcepto();     alert('" + ex.Message + "');    ", True)

        End Try
    End Sub

    Private Function obtenerPesoTotal(ByVal listaDetalle As List(Of Entidades.DetalleDocumento)) As Decimal

        Dim pesoTotal As Decimal = 0

        For i As Integer = 0 To listaDetalle.Count - 1

            pesoTotal = pesoTotal + (New Negocio.ProductoMedida).getMagnitudProducto(listaDetalle(i).IdProducto, listaDetalle(i).IdUnidadMedida, listaDetalle(i).Cantidad, 1, 0)

        Next

        Return pesoTotal

    End Function

    Private Sub cboDepto_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepto_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboProvincia_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnConsultarVolumenVentaMin_Detalle_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)

        verVolumenVentaMinimo()

    End Sub

    Private Sub verVolumenVentaMinimo()

        Try

            Dim index As Integer = CInt(Me.hddIndexCambiarTipoPV_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim IdProducto As Integer = Me.listaDetalleDocumento(index).IdProducto
            Me.lblProducto_CambiarTipoPV_GV_Detalle.Text = Me.listaDetalleDocumento(index).NomProducto

            Me.GV_VolumenVentaMin.DataSource = (New Negocio.VolumenVenta).SelectxIdProductoxIdTipoPVxIdTienda(IdProducto, 0, CInt(Me.cboTienda.SelectedValue))
            Me.GV_VolumenVentaMin.DataBind()

            objScript.onCapa(Me, "capaCambiarTipoPrecioPV")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub valChange_TipoPrecioPV()
        Try
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim Cad_IdProducto, cad_IdUnidadMedida, cad_Cantidad As String
            Dim index As Integer = CInt(Me.hddIndexCambiarTipoPV_GV_Detalle.Value)
            Cad_IdProducto = CStr(Me.listaDetalleDocumento(index).IdProducto)
            cad_IdUnidadMedida = CStr(Me.listaDetalleDocumento(index).IdUnidadMedida)
            cad_Cantidad = CStr(Me.listaDetalleDocumento(index).Cantidad)

            Dim IdUsuario As Integer = CInt(Session("IdUsuario"))
            If (Me.listaDetalleDocumento(index).IdUsuarioSupervisor <> Nothing) Then
                IdUsuario = Me.listaDetalleDocumento(index).IdUsuarioSupervisor
            End If
            Dim IdMedioPago As Integer = 0
            If (CInt(Me.cboCondicionPago.SelectedValue) = 1) Then  '********** CONTADO
                IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            End If

            Dim IdTipoOperacion As Integer = 0
            IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)

            Dim objCatalogo As Entidades.Catalogo = ((New Negocio.DocumentoCotizacion).DocumentoCotizacion_ValSelectTipoPrecioPV(IdUsuario, CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, CDate(Me.txtFechaEmision.Text), CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdPersona.Value), CInt(Me.cboTipoPrecioPV_CambiarDetalle.SelectedValue), Cad_IdProducto, cad_IdUnidadMedida, cad_Cantidad, IdTipoOperacion))


            '********* ALMACENAMOS LOS VALORES
            Me.listaDetalleDocumento(index).PrecioLista = objCatalogo.PrecioLista
            Me.listaDetalleDocumento(index).pvComercial = objCatalogo.pvComercial
            Me.listaDetalleDocumento(index).PrecioCD = objCatalogo.PrecioLista
            Me.listaDetalleDocumento(index).PrecioSD = objCatalogo.PrecioLista
            Me.listaDetalleDocumento(index).PorcentDctoMaximo = objCatalogo.PorcentDctoMaximo
            Me.listaDetalleDocumento(index).PrecioBaseDcto = objCatalogo.PrecioBaseDcto
            Me.listaDetalleDocumento(index).VolumenVentaMin = objCatalogo.VolumenVenta
            Me.listaDetalleDocumento(index).IdTipoPV = CInt(Me.cboTipoPrecioPV_CambiarDetalle.SelectedValue)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnCambiarTipoPrecioVenta_Detalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCambiarTipoPrecioVenta_Detalle.Click
        valChange_TipoPrecioPV()
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        If Validar_AperturaCierreCaja() Then
            'Dim TipoDocumentos As Integer
            'TipoDocumentos = CCcboSerie.SelectedValue;
            Dim registra As Boolean = True
            If (Me.hddIdDocumentoRef.Value.Trim.Length > 0 And IsNumeric(Me.hddIdDocumentoRef.Value.Trim)) Then

                Dim _objAnexo_Documento As Entidades.Anexo_Documento = (New Negocio.Anexo_Documento).Anexo_DocumentoSelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value.Trim))

                If _objAnexo_Documento IsNot Nothing Then

                    If _objAnexo_Documento.IdMaestroObra > 0 Then
                        'Dim _objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(_objAnexo_Documento.IdMaestroObra)

                        Me.lblPregunta.Text = "¿VIENE CON UN MAESTRO DE OBRA?" ' + _objPersona.Descripcion + "."
                        Me.CollapsiblePanelExtender_Panel_MaestroObra.Collapsed = False
                        Me.CollapsiblePanelExtender1.Collapsed = True
                        Me.cpeDocumentoRef.Collapsed = False
                        Me.ModalPopup_Pregunta.Show()

                        Return
                        registra = False
                    End If
                End If
            End If

            If registra Then
                registrarDocumentoVenta()
            End If
        End If
    End Sub

    Private Sub validarFrm()

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {151, 152, 153, 221})

        If (listaPermisos(0) <= 0) Then '************** DESCUENTO SOBRE MAESTRO DE OBRA

            '************* VALIDAMOS
            If (IsNumeric(Me.hddIdMaestroObra.Value) And Me.hddIdMaestroObra.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdMaestroObra.Value) > 0) Then

                    Me.listaDetalleDocumento = getListaDetalleDocumento()

                    For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                        If (Me.listaDetalleDocumento(i).PrecioCD < Me.listaDetalleDocumento(i).PrecioLista) Then
                            Throw New Exception("EL PRODUCTO < " + Me.listaDetalleDocumento(i).NomProducto + " > TIENE UN DESCUENTO POR DEBAJO DEL PRECIO DE LISTA. NO TIENE LOS PERMISOS NECESARIOS PARA DAR ESTE TIPO DE DESCUENTO. NO SE PERMITE LA OPERACIÓN < RESTRICCIÓN : MAESTROS >")
                        End If
                    Next
                End If
            End If
        End If

        If (listaPermisos(1) <= 0) Then  '*****************  VERIFICAR RUC FACTURA
            If (Me.txtRuc.Text.Trim.Length <= 0 And CInt(Me.cboTipoDocumento.SelectedValue) = 1) Then
                Throw New Exception("EL CAMPO R.U.C. ES UN CAMPO REQUERIDO PARA EL TIPO DE DOCUMENTO < " + Me.cboTipoDocumento.SelectedItem.ToString + " >. NO SE PERMITE LA OPERACIÓN.")
            End If
        End If

        If (listaPermisos(2) <= 0) Then  '*****************  VERIFICAR RUC BOLETA

            If (Me.txtRuc.Text.Trim.Length <= 0 And CInt(Me.cboTipoDocumento.SelectedValue) = 3) Then
                Throw New Exception("EL CAMPO R.U.C. ES UN CAMPO REQUERIDO PARA EL TIPO DE DOCUMENTO < " + Me.cboTipoDocumento.SelectedItem.ToString + " >. NO SE PERMITE LA OPERACIÓN.")
            End If
        End If

        '**********************  VALIDAMOS EL MEDIO DE PAGO DE REFERENCIA
        If (listaPermisos(3) > 0) Then

            Dim IdDocumentoRef As Integer = 0
            If (IsNumeric(Me.hddIdDocumentoRef.Value) And Me.hddIdDocumentoRef.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdDocumentoRef.Value) <> 0) Then

                    IdDocumentoRef = CInt(Me.hddIdDocumentoRef.Value)

                End If
            End If

            If (IdDocumentoRef <> 0) Then  '************ POSEE DOCUMENTO REFERENCIA

                Me.listaCancelacion = getListaCancelacion()

                If listaCancelacion.Count > 0 Then ''*****************se agrego para la facturacion rapida

                    Dim objAnexoDocumento As Entidades.Anexo_Documento = (New Negocio.Anexo_Documento).Anexo_DocumentoSelectxIdDocumento(IdDocumentoRef)

                    If (objAnexoDocumento IsNot Nothing And CInt(Me.cboCondicionPago.SelectedValue) = _IdCondicionPago_Contado) Then

                        Dim flag As Boolean = False
                        For i As Integer = 0 To Me.listaCancelacion.Count - 1

                            If (Me.listaCancelacion(i).IdMedioPago = objAnexoDocumento.IdMedioPago) Then

                                flag = True
                                Exit For

                            End If

                        Next

                        If (flag = False) Then

                            Throw New Exception("SE HAN INGRESADO MEDIOS DE PAGO QUE NO COINCIDEN CON EL MEDIO DE PAGO DEL DOCUMENTO DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")

                        End If

                    End If

                End If

            End If



        End If

    End Sub

    Private Sub registrarDocumentoVenta(Optional ByVal vistaPrevia As Boolean = False, Optional ByVal RespuestaMaestro As Integer = -1)

        Try

            '**************** VALIDAMOS LOS CONCEPTOS
            If (Me.GV_Concepto.Rows.Count > 0) Then

                validarListaConceptos()

            End If

            ActualizarListaDetalleDocumento()

            '***************** VALIDAR FRM
            validarFrm()

            '********************** ACTUALIZAMOS EL DETALLE DOCUMENTO - ADD COMPONENTES AL DETALLE - KIT
            '******** SE ACTUALIZA EN EL PROCESO DE REGISTRO
            '*********************************   ActualizarListaDetalleDocumento_KIT()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab(RespuestaMaestro)
            Dim objAnexoDocumento As Entidades.Anexo_Documento = obtenerAnexoDocumento(RespuestaMaestro)
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)


            If GV_Cancelacion.Rows.Count <= 0 Then
                Me.listaCancelacion = getListaCancelacionXDefecto()
                setListaCancelacion(Me.listaCancelacion)
            Else
                Me.listaCancelacion = getListaCancelacion()
            End If



            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

            ' Dim objVehiculo As Entidades.Vehiculo=obtene



            Dim objMontoRegimenPercepcion As Entidades.MontoRegimen = obtenerMontoRegimen_Percepcion()
            Dim objMontoRegimenRetencion As Entidades.MontoRegimen = obtenerMontoRegimen_Retencion()
            Dim objMontoRegimenDetraccion As Entidades.MontoRegimen = obtenerMontoRegimen_Detraccion()

            Dim objPuntoPartida As Entidades.PuntoPartida = obtenerPuntoPartida()
            Dim objPuntoLlegada As Entidades.PuntoLlegada = obtenerPuntoLlegada()
            Dim objMovCaja As Entidades.MovCaja = obtenerMovCaja()
            Dim objMovCuenta As Entidades.MovCuenta = obtenerMovCuenta()
            Dim listaDocumento As New List(Of Entidades.Documento)
            Dim listaMovBanco As List(Of Entidades.MovBancoView) = obtenerListaMovBanco(Me.listaCancelacion, objDocumento) '********* Obtiene una Lista de los Mov de Caja
            Dim objRelacionDocumento As Entidades.RelacionDocumento = obtenerRelacionDocumento()
            Dim objAnexo_FacturaTransporte As Entidades.Anexo_FacturaTransporte = obtenerAnexo_FacturaTransporte()

            Dim listaCheque As List(Of Entidades.Cheque) = obtenerListaCheque_Save()

            Dim separarAfectos As Boolean = Me.chb_SepararAfecto_NoAfecto.Checked
            Dim separarxMonto As Boolean = Me.chb_SepararxMontos.Checked
            Dim saltarCorrelativo As Boolean = Me.chb_SaltarCorrelativo.Checked

            Dim moverAlmacen As Boolean = Me.chb_MoverAlmacen.Checked
            Dim comprometerStock As Boolean = Me.chb_ComprometerStock.Checked

            If (comprometerStock) Then
                moverAlmacen = True  '********** SE MUEVE ALMACÉN FÍSICO DE FORMA COMPROMETIDA
            End If

            If ((CInt(Me.hddFrmModo.Value) = FrmModo.Editar) Or (existConceptoAnticipo_Aplicacion()) Or (existConceptoAnticipo_Ingreso()) Or (existDetalleDocumentoKit()) Or (CDec(Me.txtRetencion.Text) > 0) Or (CDec(Me.txtDetraccion.Text) > 0) Or CInt(Me.cboTipoDocumento.SelectedValue) = _IdTipoDocumento_FacturaTR) Then
                separarAfectos = False
                separarxMonto = False
                saltarCorrelativo = False
            End If

            If (CInt(Me.cboTipoDocumento.SelectedValue) = _IdTipoDocumento_FacturaTR) Then
                moverAlmacen = False
                comprometerStock = False
            End If

            If IsNumeric(hddIdTipoDocumentoRef.Value) And IsNumeric(hddIdTipoOperacionRef.Value) Then
                'Consignacion a Cliente
                If CInt(hddIdTipoDocumentoRef.Value) = 14 And CInt(hddIdTipoOperacionRef.Value) = 4 Then
                    moverAlmacen = False
                    comprometerStock = False
                End If
            End If

            If (vistaPrevia) Then

                listaDocumento = (New Negocio.DocumentoFacturacion).registrarDocumentoVenta(objDocumento, objAnexoDocumento, Me.listaDetalleConcepto, objPuntoPartida, objPuntoLlegada, objObservaciones, Me.listaDetalleDocumento, objMovCaja, objMovCuenta, Me.listaCancelacion, New List(Of Entidades.PagoCaja), moverAlmacen, comprometerStock, separarAfectos, saltarCorrelativo, separarxMonto, True, Me.chb_CompPercepcion.Checked, listaMovBanco, Me.chb_UseAlias.Checked, objRelacionDocumento, CInt(Me.hddFrmModo.Value))

                recalcular_VistaPrevia(listaDocumento)

                setListaDetalleDocumento(Me.listaDetalleDocumento)

                GV_Detalle.DataSource = Me.listaDetalleDocumento
                GV_Detalle.DataBind()

                If GV_Cancelacion.Rows.Count <= 0 Then
                    Me.listaCancelacion = New List(Of Entidades.PagoCaja)
                    setListaCancelacion(Me.listaCancelacion)
                End If

                Return

            End If


            listaDocumento = (New Negocio.DocumentoFacturacion).registrarDocumentoVenta(objDocumento, objAnexoDocumento, Me.listaDetalleConcepto, objPuntoPartida, objPuntoLlegada, objObservaciones, Me.listaDetalleDocumento, objMovCaja, objMovCuenta, Me.listaCancelacion, New List(Of Entidades.PagoCaja), moverAlmacen, comprometerStock, separarAfectos, saltarCorrelativo, separarxMonto, False, Me.chb_CompPercepcion.Checked, listaMovBanco, Me.chb_UseAlias.Checked, objRelacionDocumento, CInt(Me.hddFrmModo.Value), objMontoRegimenRetencion, objMontoRegimenDetraccion, listaCheque, objAnexo_FacturaTransporte)

            If (listaDocumento.Count > 0) Then

                Me.GV_ImpresionDoc_Cab.DataSource = obtenerListaDocumento_Print(listaDocumento)
                Me.GV_ImpresionDoc_Cab.DataBind()

                verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)

                If (listaDocumento.Count = 1) Then
                    validarGenerarComprobantePercepcion(listaDocumento(0).Id, listaDocumento(0).IdPersona)
                End If


                objScript.onCapa(Me, "capaImpresionDocumento")

            Else
                Throw New Exception("Problemas en la Operación.")
            End If




        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function existDetalleDocumentoKit() As Boolean

        Dim exist As Boolean = False

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            If (Me.listaDetalleDocumento(i).Kit) Then
                Return True
            End If

        Next

        Return exist

    End Function


    Private Function obtenerRelacionDocumento() As Entidades.RelacionDocumento

        Dim objRelacionDocumento As Entidades.RelacionDocumento = Nothing

        If (Me.hddIdDocumentoRef.Value.Trim.Length > 0 And IsNumeric(Me.hddIdDocumentoRef.Value.Trim)) Then
            If CInt(Me.hddIdDocumentoRef.Value.Trim) > 0 Then
                objRelacionDocumento = New Entidades.RelacionDocumento
                With objRelacionDocumento
                    Select Case CInt(Me.hddFrmModo.Value)
                        Case FrmModo.Nuevo
                            .IdDocumento2 = Nothing
                        Case FrmModo.Editar
                            .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                    End Select
                    .IdDocumento1 = CInt(Me.hddIdDocumentoRef.Value)
                End With
            End If
        End If

        Return objRelacionDocumento

    End Function

    Private Function obtenerListaMovBanco(ByVal lista As List(Of Entidades.PagoCaja), ByVal objDocumento As Entidades.Documento) As List(Of Entidades.MovBancoView)

        Dim listaMovBanco As New List(Of Entidades.MovBancoView)
        For i As Integer = 0 To lista.Count - 1

            Dim obj As New Entidades.MovBancoView
            With obj
                .IdBanco = lista(i).IdBanco
                .IdCuentaBancaria = lista(i).IdCuentaBancaria
                .Monto = lista(i).Efectivo
                .IdMoneda = lista(i).IdMoneda
                .IdMedioPago = lista(i).IdMedioPago
                .IdUsuario = objDocumento.IdUsuario
                .IdPersonaRef = objDocumento.IdPersona
                .IdCaja = objDocumento.IdCaja
                .IdTienda = objDocumento.IdTienda
                .FechaMov = objDocumento.FechaEmision

                Select Case (lista(i).IdMedioPagoInterfaz)
                    Case 3  '*********** CHEQUE
                        .NroOperacion = lista(i).NumeroCheque
                    Case Else
                        .NroOperacion = lista(i).NumeroOp
                End Select

                .IdTarjeta = lista(i).IdTarjeta
                .IdPost = lista(i).IdPost

            End With

            listaMovBanco.Add(obj)

        Next

        Return listaMovBanco

    End Function

    Private Function obtenerListaDocumento_Print(ByVal listaDocumento As List(Of Entidades.Documento)) As List(Of Entidades.Documento)

        Dim IdDocumentoInicio As Integer = listaDocumento(0).Id
        Dim IdDocumentoFin As Integer = listaDocumento(listaDocumento.Count - 1).Id
        Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
        Dim listaRetorno As List(Of Entidades.Documento) = (New Negocio.DocumentoFacturacion).DocumentoFacturacionSelectPrintxIds(IdDocumentoInicio, IdDocumentoFin, IdSerie)
        Dim percepionTotal As Decimal = 0
        Dim totalAPagar As Decimal = 0

        For i As Integer = 0 To listaRetorno.Count - 1

            percepionTotal = percepionTotal + listaRetorno(i).Percepcion
            totalAPagar = totalAPagar + listaRetorno(i).TotalAPagar

        Next

        Me.txtPercepcion_DocSAVED.Text = CStr(Math.Round(percepionTotal, 2, MidpointRounding.AwayFromZero))
        Me.txtTotalAPagar_DocSAVED.Text = CStr(Math.Round(totalAPagar, 2, MidpointRounding.AwayFromZero))

        Return listaRetorno

    End Function

    Private Function obtenerMovCuenta() As Entidades.MovCuenta

        Dim objMovCuenta As Entidades.MovCuenta = Nothing

        Select Case CInt(Me.cboCondicionPago.SelectedValue)

            Case 1  '*********** CONTADO

                objMovCuenta = Nothing

            Case 2  '************* CREDITO

                objMovCuenta = New Entidades.MovCuenta

                With objMovCuenta

                    .IdCuentaPersona = obtenerIdCuentaPersona()
                    If (.IdCuentaPersona = 0) Then
                        Throw New Exception("EL CLIENTE NO POSEE LÍNEA DE CRÉDITO. NO SE PERMITE LA OPERACIÓN.")
                    End If
                    .IdPersona = CInt(Me.hddIdPersona.Value)
                    .Monto = CDec(Me.txtTotalAPagar.Text)
                    .Factor = 1  '********* AUMENTA LA CUENTA CORRIENTE DE LA PERSONA

                    Select Case CInt(Me.hddFrmModo.Value)
                        Case FrmModo.Nuevo
                            .IdDocumento = Nothing
                        Case FrmModo.Editar
                            .IdDocumento = CInt(Me.hddIdDocumento.Value)
                    End Select

                    .IdMovCuentaTipo = 1
                    .Saldo = CDec(Me.txtTotalAPagar.Text)

                End With

        End Select

        Return objMovCuenta

    End Function

    Private Function obtenerIdCuentaPersona() As Integer
        Dim IdCuentaPersona As Integer = 0
        Dim total As Decimal = CDec(Me.txtTotalAPagar.Text)
        Dim saldoDisponible As Decimal = 0

        For i As Integer = 0 To Me.GV_LineaCredito.Rows.Count - 1

            If ((CInt(CType(Me.GV_LineaCredito.Rows(i).FindControl("hddIdMoneda"), HiddenField).Value) = CInt(Me.cboMoneda.SelectedValue)) And (CInt(CType(Me.GV_LineaCredito.Rows(i).FindControl("hddIdEmpresa"), HiddenField).Value) = CInt(Me.cboEmpresa.SelectedValue))) Then

                saldoDisponible = CDec(CType(Me.GV_LineaCredito.Rows(i).FindControl("lblDisponible"), Label).Text)

                If (total > saldoDisponible And CInt(Me.hddFrmModo.Value) = FrmModo.Nuevo) Then
                    Throw New Exception("EL CLIENTE NO POSEE SALDO DISPONIBLE.")
                Else
                    IdCuentaPersona = CInt(CType(Me.GV_LineaCredito.Rows(i).FindControl("hddIdCuenta"), HiddenField).Value)
                    Return IdCuentaPersona
                End If


            End If


        Next

        Return IdCuentaPersona

    End Function

    Private Function obtenerMovCaja() As Entidades.MovCaja

        Dim objMovCaja As Entidades.MovCaja = Nothing

        Select Case CInt(Me.cboCondicionPago.SelectedValue)

            Case 1 '*********** CONTADO
                objMovCaja = New Entidades.MovCaja
                With objMovCaja

                    Select Case CInt(Me.hddFrmModo.Value)

                        Case FrmModo.Nuevo

                            .IdDocumento = Nothing

                        Case FrmModo.Editar

                            .IdDocumento = CInt(Me.hddIdDocumento.Value)

                    End Select

                    .IdCaja = CInt(Me.cboCaja.SelectedValue)
                    .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                    .IdTienda = CInt(Me.cboTienda.SelectedValue)
                    .IdPersona = CInt(Session("IdUsuario"))
                    .IdTipoMovimiento = 1  '************** INGRESO

                    'If()

                End With

            Case 2 '*********** CREDITO
                objMovCaja = Nothing
        End Select

        Return objMovCaja

    End Function

    Private Sub validarListaConceptos()

        '************** EL ERROR ES GENERADO DESDE LA MISMA BASE DE DATOS

        Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)

        ActualizarListaDetalleDocumento()
        Me.listaDetalleDocumento = getListaDetalleDocumento()

        Dim ubigeoOrigen As String = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue
        Dim ubigeoDestino As String = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue
        Dim pesoTotal As Decimal = obtenerPesoTotal(Me.listaDetalleDocumento)
        Dim importeTotal As Decimal = CDec(Me.txtTotal.Text.Trim)

        Dim objDocumentoCotizacion As New Negocio.DocumentoCotizacion

        For i As Integer = 0 To Me.listaDetalleConcepto.Count - 1

            objDocumentoCotizacion.SelectValorEstimadoConcepto(Me.listaDetalleConcepto(i).IdConcepto, CInt(Me.cboTipoDocumento.SelectedValue), ubigeoOrigen, ubigeoDestino, CInt(Me.cboMoneda.SelectedValue), pesoTotal, importeTotal, CDate(Me.txtFechaEmision.Text))

            If (Me.listaDetalleConcepto(i).Monto = 0) Then
                Throw New Exception("NO SE PERMITEN MONTOS NULOS EN LOS CONCEPTOS. NO SE PERMITE LA OPERACIÓN.")
            End If

            If (Me.listaDetalleConcepto(i).ConceptoAdelanto) Then

                If (Me.listaDetalleConcepto(i).Monto > 0) Then  '***** INGRESO DE ANTICIPO

                    If (Me.GV_Detalle.Rows.Count > 0) Then
                        Throw New Exception("SE HAN INGRESADO CONCEPTOS POR ANTICIPO. DEBE QUITAR EL DETALLE DEL DOCUMENTO. NO SE PERMITE LA OPERACIÓN.")
                    End If

                    If (Me.GV_Concepto.Rows.Count > 1) Then
                        Throw New Exception("SE HAN INGRESADO CONCEPTOS POR ANTICIPO. DEBE QUITAR LOS OTROS CONCEPTOS. NO SE PERMITE LA OPERACIÓN.")
                    End If

                    If (CInt(Me.cboCondicionPago.SelectedValue) = 2) Then '**** CREDITO
                        Throw New Exception("SE HAN INGRESADO CONCEPTOS POR ANTICIPO. CONDICIÓN DE PAGO NO PERMITIDO. NO SE PERMITE LA OPERACIÓN.")
                    End If

                End If

            End If

        Next

    End Sub

    Private Function obtenerListaAnexoDetalleDocumento() As List(Of Entidades.Anexo_DetalleDocumento)
        Dim lista As New List(Of Entidades.Anexo_DetalleDocumento)

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            Dim obj As New Entidades.Anexo_DetalleDocumento
            With obj
                .IdDocumento = Me.listaDetalleDocumento(i).IdDocumento
                .IdDetalleDocumento = Me.listaDetalleDocumento(i).IdDetalleDocumento
                .IdTipoPV = Me.listaDetalleDocumento(i).IdTipoPV
            End With
            lista.Add(obj)

        Next

        Return lista

    End Function

    Private Function obtenerAnexoDocumento(ByVal RespuestaMaestro As Integer) As Entidades.Anexo_Documento

        Dim objAnexoDocumento As Entidades.Anexo_Documento = Nothing

        '************* EN ANEXO DOCUMENTO SE ALMACENA EL TOTAL CONCEPTO
        If ((Me.GV_Concepto.Rows.Count > 0) Or (Me.chb_UseAlias.Checked)) Or ((Me.hddIdMaestroObra.Value.Trim.Length > 0) And (IsNumeric(Me.hddIdMaestroObra.Value.Trim))) Then
            objAnexoDocumento = New Entidades.Anexo_Documento

            If (Me.GV_Concepto.Rows.Count > 0) Then

                Dim TotalConceptoAbs As Decimal = 0
                For i As Integer = 0 To Me.GV_Concepto.Rows.Count - 1
                    Dim monto As Decimal = CDec(CType(Me.GV_Concepto.Rows(i).FindControl("txtMonto"), TextBox).Text)
                    If monto > 0 Or CInt(CType(Me.GV_Concepto.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue) = 10 Then
                        TotalConceptoAbs = TotalConceptoAbs + monto
                    End If
                Next

                objAnexoDocumento.TotalConcepto = CDec(Me.txtTotalConcepto.Text)
                objAnexoDocumento.TotalConceptoAbs = TotalConceptoAbs
                objAnexoDocumento.anex_DescuentoGlobal = CDec(Me.txtDescuentoGlobal.Text)
            End If

            If ((Me.hddIdMaestroObra.Value.Trim.Length > 0) And (IsNumeric(Me.hddIdMaestroObra.Value.Trim))) Then

                If (CInt(Me.hddIdMaestroObra.Value.Trim) > 0) Then

                    objAnexoDocumento.IdMaestroObra = CInt(Me.hddIdMaestroObra.Value)

                    If RespuestaMaestro = 0 Then
                        objAnexoDocumento.IdMaestroObra = Nothing
                    End If

                End If

            End If

        End If

        Return objAnexoDocumento
    End Function

    Private Function obtenerAnexo_FacturaTransporte() As Entidades.Anexo_FacturaTransporte

        Dim objAnexo_FacturaTransporte As Entidades.Anexo_FacturaTransporte = Nothing

        If (CInt(Me.cboTipoDocumento.SelectedValue) = _IdTipoDocumento_FacturaTR) Then

            objAnexo_FacturaTransporte = New Entidades.Anexo_FacturaTransporte
            With objAnexo_FacturaTransporte

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .IdMoneda = CInt(Me.cboMoneda_ValorRef.SelectedValue)
                .IdValorReferencial = CInt(Me.cboValorReferencial.SelectedValue)
                .Cantidad = CDec(Me.txtCantidad_Transporte.Text)
                .ValorReferencial_Unit = CDec(Me.txtValorReferencial.Text)
                .ValorReferencia_Total = CDec(Me.txtValorReferencial_Total.Text)

            End With

        End If

        Return objAnexo_FacturaTransporte

    End Function

    Private Function obtenerPuntoLlegada() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing

        objPuntoLlegada = New Entidades.PuntoLlegada
        With objPuntoLlegada

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .pll_Direccion = Me.txtDireccion_Llegada.Text.Trim
            .IdAlmacen = Nothing
            .pll_Ubigeo = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue

        End With


        Return objPuntoLlegada

    End Function

    Private Function obtenerPuntoPartida() As Entidades.PuntoPartida

        Dim objPuntoPartida As Entidades.PuntoPartida = Nothing

        objPuntoPartida = New Entidades.PuntoPartida

        With objPuntoPartida

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .Direccion = Me.txtDireccion_Partida.Text.Trim
            .IdAlmacen = Nothing
            .Ubigeo = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue

        End With

        Return objPuntoPartida

    End Function

    Private Function obtenerMontoRegimen_Percepcion() As Entidades.MontoRegimen

        Dim objMontoRegimen As Entidades.MontoRegimen = Nothing

        If (CDec(Me.txtPercepcion.Text.Trim) > 0) Then

            objMontoRegimen = New Entidades.MontoRegimen
            With objMontoRegimen

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .Monto = CDec(Me.txtPercepcion.Text.Trim)
                .ro_Id = 1 '***************** PERCEPCION

            End With

        End If

        Return objMontoRegimen

    End Function

    Private Function obtenerMontoRegimen_Retencion() As Entidades.MontoRegimen

        Dim objMontoRegimen As Entidades.MontoRegimen = Nothing

        If (CDec(Me.txtRetencion.Text.Trim) > 0) Then

            objMontoRegimen = New Entidades.MontoRegimen
            With objMontoRegimen

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .Monto = CDec(Me.txtRetencion.Text.Trim)
                .ro_Id = 3 '***************** RETENCION

            End With

        End If

        Return objMontoRegimen

    End Function

    Private Function obtenerMontoRegimen_Detraccion() As Entidades.MontoRegimen

        Dim objMontoRegimen As Entidades.MontoRegimen = Nothing

        If (CDec(Me.txtDetraccion.Text.Trim) > 0) Then

            objMontoRegimen = New Entidades.MontoRegimen
            With objMontoRegimen

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .Monto = CDec(Me.txtDetraccion.Text.Trim)
                .ro_Id = 2 '***************** DETRACCIÓN

            End With

        End If

        Return objMontoRegimen

    End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function

    Private Function existConceptoAnticipo_Ingreso() As Boolean

        Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)
        For i As Integer = 0 To Me.listaDetalleConcepto.Count - 1

            If (Me.listaDetalleConcepto(i).ConceptoAdelanto And Me.listaDetalleConcepto(i).Monto > 0) Then
                Return True
            End If

        Next
        Return False
    End Function

    Private Function existConceptoAnticipo_Aplicacion() As Boolean

        Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)
        For i As Integer = 0 To Me.listaDetalleConcepto.Count - 1

            If (Me.listaDetalleConcepto(i).ConceptoAdelanto And Me.listaDetalleConcepto(i).Monto < 0) Then
                Return True
            End If

        Next
        Return False
    End Function

    Private Function obtenerDocumentoCab(ByVal RespuestaMaestro As Integer) As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdCondicionPago = CInt(Me.cboCondicionPago.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacenReferencia.SelectedValue)

            '************************ USUARIO VENDEDOR
            .IdUsuarioComision = CInt(Me.cboUsuarioComision.SelectedValue)

            .IdUsuario = CInt(Session("IdUsuario"))
            .CompPercepcion = CBool(Me.chb_CompPercepcion.Checked)

            If RespuestaMaestro = 0 Then
                .DeleteIdMaestroObra = True
            End If

            Select Case CInt(Me.cboCondicionPago.SelectedValue)

                Case 1 '*********** CONTADO

                    If (existConceptoAnticipo_Ingreso()) Then
                        .IdEstadoCancelacion = 1 '*************** POR PAGAR ( CASO DE CONCEPTOS ANTICIPO INGRESO )
                    Else
                        .IdEstadoCancelacion = 2 '*************** CANCELADO
                    End If


                    .FechaCancelacion = CDate(Me.txtFechaEmision.Text)
                    .IdMedioPagoCredito = Nothing

                Case 2 '********** CREDITO
                    .IdEstadoCancelacion = 1  '**************** POR PAGAR
                    .IdMedioPagoCredito = CInt(Me.cboMedioPago_Credito.SelectedValue)
                    Try
                        .FechaVenc = CDate(Me.txtFechaVcto.Text)
                    Catch ex As Exception
                        .FechaVenc = Nothing
                    End Try
            End Select

            Try
                .FechaAEntregar = CDate(Me.txtFechaAEntregar.Text)
            Catch ex As Exception
                .FechaAEntregar = Nothing
            End Try

            If (Me.GV_Detalle.Rows.Count > 0) Then
                .IdEstadoEntrega = 1 '************* POR ENTREGAR
            Else
                .IdEstadoEntrega = 2 '************* ENTREGADO
            End If
            If (IsNumeric(Me.hddIdVehiculo.Value) And Me.hddIdVehiculo.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdVehiculo.Value) > 0) Then
                    .IdVehiculo = CInt(Me.hddIdVehiculo.Value)
                End If
            End If
            .IdRemitente = CInt(Me.cboEmpresa.SelectedValue)
            .IdPersona = CInt(Me.hddIdPersona.Value)
            .IdDestinatario = CInt(Me.hddIdPersona.Value)
            .IdTipoPV = CInt(Me.cboTipoPrecioV.SelectedValue)
            .Descuento = CDec(Me.txtDescuento.Text)
            .Total = CDec(Me.txtTotal.Text)
            .SubTotal = CDec(Me.txtSubTotal.Text)
            .IGV = CDec(Me.txtIGV.Text)
            .ImporteTotal = CDec(Me.txtImporteTotal.Text)
            .TotalAPagar = CDec(Me.txtTotalAPagar.Text)
            .IdTipoDocumento = CInt(Me.cboTipoDocumento.SelectedValue)
            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .TotalLetras = (New Negocio.ALetras).Letras(CStr(Math.Round(CDec(Me.txtTotal.Text) + CDec(Me.txtTotalConcepto_PanelDetalle.Text), 2)))

            Select Case .IdMoneda
                Case 1
                    .TotalLetras = (.TotalLetras + "/100 SOLES").ToUpper
                Case 2
                    .TotalLetras = (.TotalLetras + "/100 DÓLARES AMERICANOS").ToUpper
            End Select


            '************* VALORES ADICIONALES
            .FactorMov = -1 '************** Mov de Almacén
            .Vuelto = CDec(Me.txtVuelto.Text)

        End With

        Return objDocumento

    End Function

    Protected Sub cboUnidadMedida_GV_Detalle(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

            ActualizarListaDetalleDocumento(i)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If



            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaDetalleDocumento(i).IdUnidadMedida, Me.listaDetalleDocumento(i).IdProducto, _
              IdTipoPV, CInt(IIf(Me.listaDetalleDocumento(i).IdTienda = 0, CInt(cboTienda.SelectedValue), Me.listaDetalleDocumento(i).IdTienda)), CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text), _
              Me.listaDetalleDocumento(i).Cantidad, Me.listaDetalleDocumento(i).IdUMold)


            If objCatalogo IsNot Nothing Then
                If objCatalogo.Cantidad > 0 Then
                    Me.listaDetalleDocumento(i).Cantidad = objCatalogo.Cantidad
                    listaDetalleDocumento(i).IdUMold = 0
                End If
                Me.listaDetalleDocumento(i).PrecioSD = objCatalogo.PrecioSD
                Me.listaDetalleDocumento(i).PrecioLista = objCatalogo.PrecioLista
                Me.listaDetalleDocumento(i).pvComercial = objCatalogo.pvComercial
                Me.listaDetalleDocumento(i).PrecioCD = objCatalogo.PrecioLista
                Me.listaDetalleDocumento(i).StockDisponibleN = objCatalogo.StockDisponibleN
                Me.listaDetalleDocumento(i).Descuento = 0
                Me.listaDetalleDocumento(i).PorcentDcto = 0
            End If

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            GV_Detalle.DataSource = Me.listaDetalleDocumento
            GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        cargarDocumentoFacturacion(CInt(Me.cboSerie.SelectedValue), CStr(Me.hddCodigoDocumento.Value.Trim), 0)
    End Sub

    Private Function obtenerListaDetalleDocumento_Load(ByVal IdDocumento As Integer, ByVal IdTienda As Integer, Optional ByVal Consignacion As Boolean = False) As List(Of Entidades.DetalleDocumento)
        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectDet(IdDocumento, Consignacion)

        '************* QUITAMOS EL DETALLE DE LOS COMPONENTES
        For i As Integer = (lista.Count - 1) To 0 Step -1

            If (lista(i).ComponenteKit) Then

                lista.RemoveAt(i)

            End If

        Next

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(lista(i).IdProducto, IdTienda, lista(i).IdTipoPV)


            If lista(i).IdProductoAux > 0 Or lista(i).ListaUM_Venta.Count <= 0 Then

                If lista(i).ListaUM_Venta.Count <= 0 Then
                    Dim obj As New Entidades.UnidadMedida

                    obj.Id = lista(i).IdUnidadMedida
                    obj.DescripcionCorto = (New Negocio.UnidadMedida).SelectNombreCortoxIdUnidadMedida(lista(i).IdUnidadMedida)

                    lista(i).ListaUM_Venta.Add(obj)

                End If

            End If

            If lista(i).IdCampania > 0 And lista(i).ExisteCampania_Producto And lista(i).IdProductoAux <= 0 And lista(i).PorcentDctoMaximo < 100 Then
                ' PRODUCTO EN CAMPAÑA
                Dim dcto, porcentDcto As Decimal
                Dim IdCampania As Integer = lista(i).IdCampania
                Dim IdProducto As Integer = lista(i).IdProducto
                Dim IdUnidadMedida As Integer = lista(i).IdUnidadMedida

                Dim objCampania_Detalle As Entidades.Campania_Detalle = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente(IdCampania, IdProducto, IdUnidadMedida)

                With lista(i)
                    dcto = (lista(i).PrecioLista - objCampania_Detalle.Precio)

                    If (dcto < 0) Then
                        dcto = 0
                    Else
                        porcentDcto = Math.Round(((dcto) / lista(i).PrecioLista) * 100, 1)
                    End If
                    .VolumenVentaMin = objCampania_Detalle.CantidadMin
                    .PorcentDctoMaximo = porcentDcto

                End With

            End If

            If lista(i).IdCampania > 0 And lista(i).IdProductoAux > 0 Then
                ' PRODUCTO EN TRANSFERENCIA X CAMPAÑA
                Dim dcto, porcentDcto As Decimal

                With lista(i)
                    dcto = (lista(i).PrecioSD - lista(i).PrecioCD)

                    If (dcto < 0) Then
                        dcto = 0
                    Else
                        porcentDcto = Math.Round(((dcto) / lista(i).PrecioSD) * 100, 1)
                    End If

                    .PorcentDctoMaximo = porcentDcto

                End With

            End If

        Next

        Return lista

    End Function

    Private Function obtenerListaDetalleConcepto_Load(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As List(Of Entidades.DetalleConcepto) = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectDetalleConcepto(IdDocumento)

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaConcepto = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.cboTipoDocumento.SelectedValue))
            lista(i).ListaConcepto.Insert(0, (New Entidades.Concepto(0, "-----")))

        Next

        Return lista

    End Function

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try

            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100011}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.cboTipoDocumento.SelectedValue)
            Select Case listaPermisos(0)
                Case 0
                    objScript.mostrarMsjAlerta(Me, "No está permitido editar el documento con fecha anterior a la de hoy")
                Case 1
                    Me.listaDetalleDocumento = getListaDetalleDocumento()
                    Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                    Me.GV_Detalle.DataBind()

                    '********** Inicializamos la Lista de Cancelación
                    Me.listaCancelacion = New List(Of Entidades.PagoCaja)
                    setListaCancelacion(Me.listaCancelacion)
                    Me.GV_Cancelacion.DataSource = Nothing
                    Me.GV_Cancelacion.DataBind()

                    '*********** Detalle Concepto
                    Me.listaDetalleConcepto = obtenerListaDetalleConcepto_Load(CInt(Me.hddIdDocumento.Value))
                    Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
                    Me.GV_Concepto.DataBind()

                    '**************** LINEA DE CREDITO
                    If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                        Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                        Me.GV_LineaCredito.DataBind()
                    End If

                    verFrm(FrmModo.Editar, False, False, False, False, True)

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumentoFacturacion()
    End Sub
    Private Sub anularDocumentoFacturacion()

        Try
            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100008}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.cboTipoDocumento.SelectedValue)
            Select Case listaPermisos(0)
                Case 1
                    If ((New Negocio.DocumentoFacturacion).AnularDocumentoFacturacion(CInt(Me.hddIdDocumento.Value))) Then
                        verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, True)
                        objScript.mostrarMsjAlerta(Me, "Documento anulado con éxito.")
                    Else
                        Throw New Exception("Problemas en la Operación.")
                    End If
                Case 0
                    objScript.mostrarMsjAlerta(Me, "No procede [Anular] un documento de un mes anterior al actual.")

            End Select
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
        If (CInt(ViewState("ClientexDefecto")) > 0) Then
            cargarPersona(0)
        End If
        cursorCodigoBarras()
        Me.lblPorcentIgv.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))
    End Sub

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub
    Private Sub busquedaAvanzado()

        Try

            Dim IdCliente As Integer = 0
            Try
                IdCliente = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdCliente = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.cboTipoDocumento.SelectedValue), IdSerie, IdCliente, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoFacturacion(0, "0", CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoDocumento.SelectedIndexChanged
        valOnChange_cboTipoDocumento()
    End Sub

    Private Sub valOnChange_cboTipoDocumento()

        Try
            If (txtCodigoCliente.Text <> "") Then
                If (cboTipoDocumento.SelectedValue = 1 And txtRuc.Text = "") Then
                    cboTipoDocumento.SelectedValue = 1101353002
                    cambiodocumento()
                    Throw New Exception("DEBE SEÑALAR UN CLIENTE CON RUC")

                    Return
                End If
                If (cboTipoDocumento.SelectedValue = 1101353001 And txtRuc.Text = "") Then
                    cboTipoDocumento.SelectedValue = 1101353002
                    cambiodocumento()
                    Throw New Exception("DEBE SEÑALAR UN CLIENTE CON RUC")

                    Return
                End If

                If (cboTipoDocumento.SelectedValue = 3 And txtDni.Text = "") Then
                    cboTipoDocumento.SelectedValue = 1101353001
                    cambiodocumento()
                    Throw New Exception("DEBE SEÑALAR UN CLIENTE CON DNI")

                    Return
                End If
                If (cboTipoDocumento.SelectedValue = 1101353002 And txtDni.Text = "") Then
                    cboTipoDocumento.SelectedValue = 1101353001
                    cambiodocumento()
                    Throw New Exception("DEBE SEÑALAR UN CLIENTE CON DNI")
                    Return
                End If

            End If
           
            Dim objCbo As New Combo

            objCbo.LLenarCboTipoDocumentoRefxIdTipoDocumento(cboTipoDocumentoRef, CInt(cboTipoDocumento.SelectedValue), 2, False)
            '*******************  objCbo.LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago, CInt(Me.cboTipoDocumento.SelectedValue), False)
            objCbo.LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, CInt(Me.cboTipoDocumento.SelectedValue), 1, False) '***** CONTADO
            objCbo.LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago_Credito, CInt(Me.cboTipoDocumento.SelectedValue), 2, True) '***** CREDITO

            objCbo.llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.cboTipoDocumento.SelectedValue), False)

            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            GenerarCodigoDocumento()

            actualizarMontoMaxAfecto_TipoDocumentoAE()
            Select Case CInt(Me.cboCondicionPago.SelectedValue)
                Case 1  '***************     CONTADO
                    actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago.SelectedValue))
                Case 2  '***************     CREDITO
                    actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago_Credito.SelectedValue))
            End Select

            If (CInt(Me.cboTipoDocumento.SelectedValue) = _IdTipoDocumento_FacturaTR) Then
                Me.Panel_ValorRef_TRANSPORTE.Visible = True
            Else
                Me.Panel_ValorRef_TRANSPORTE.Visible = False
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Public Sub cambiodocumento()
        Dim objCbo As New Combo

        objCbo.LLenarCboTipoDocumentoRefxIdTipoDocumento(cboTipoDocumentoRef, CInt(cboTipoDocumento.SelectedValue), 2, False)
        '*******************  objCbo.LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago, CInt(Me.cboTipoDocumento.SelectedValue), False)
        objCbo.LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, CInt(Me.cboTipoDocumento.SelectedValue), 1, False) '***** CONTADO
        objCbo.LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago_Credito, CInt(Me.cboTipoDocumento.SelectedValue), 2, True) '***** CREDITO

        objCbo.llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.cboTipoDocumento.SelectedValue), False)

        objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
        GenerarCodigoDocumento()

        actualizarMontoMaxAfecto_TipoDocumentoAE()
        Select Case CInt(Me.cboCondicionPago.SelectedValue)
            Case 1  '***************     CONTADO
                actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago.SelectedValue))
            Case 2  '***************     CREDITO
                actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago_Credito.SelectedValue))
        End Select

        If (CInt(Me.cboTipoDocumento.SelectedValue) = _IdTipoDocumento_FacturaTR) Then
            Me.Panel_ValorRef_TRANSPORTE.Visible = True
        Else
            Me.Panel_ValorRef_TRANSPORTE.Visible = False
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As String = ""
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CStr(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef2(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, CInt(Me.cboTipoDocumentoRef.SelectedValue), fechaInicio, fechafin, True, Me.chb_ValidarCanjeUnico_Cotizacion.Checked)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")
            'GV_Detalle.
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub
    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If


    End Sub

    Private Sub GV_DocumentosReferencia_Find_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_DocumentosReferencia_Find.PageIndexChanging
        Me.GV_DocumentosReferencia_Find.PageIndex = e.NewPageIndex
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged
        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))
    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer, Optional ByVal onlyCab As Boolean = False)

        Try
            If CInt(hddFrmModo.Value) = FrmModo.Editar Then
                verFrm(FrmModo.Editar, False, True, True, True, True)
            Else
                verFrm(FrmModo.Inicio, True, True, True, True, True)
            End If


            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumentoCotizacion As Entidades.DocumentoCotizacion = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectCab(0, 0, IdDocumentoRef)

            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumentoCotizacion.Id)




            ' Detalle de Documento 
            If objDocumentoCotizacion.IdTipoDocumento = 14 And objDocumentoCotizacion.IdTipoOperacion = 4 Then
                ' Consignacion a Cliente
                Me.listaDetalleDocumento = obtenerListaDetalleDocumento_Load(objDocumentoCotizacion.Id, objDocumentoCotizacion.IdTienda, True)

                For i As Integer = 0 To listaDetalleDocumento.Count - 1
                    Me.listaDetalleDocumento(i).IdDetalleAfecto = Me.listaDetalleDocumento(i).IdDetalleDocumento
                Next
            Else

                Me.listaDetalleDocumento = obtenerListaDetalleDocumento_Load(objDocumentoCotizacion.Id, objDocumentoCotizacion.IdTienda)

            End If
            ' Fin Detalle de Documento 


            Me.listaDetalleConcepto = obtenerListaDetalleConcepto_Load(objDocumentoCotizacion.Id)

            Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumentoCotizacion.Id)
            Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumentoCotizacion.Id)

            '*************** GUARDAMOS EN SESSION
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            cargarDocumentoRef_GUI(objDocumentoCotizacion, Me.listaDetalleDocumento, Me.listaDetalleConcepto, objPuntoPartida, objPuntoLlegada, objObservaciones)

            If CInt(hddFrmModo.Value) = FrmModo.Editar Then
                verFrm(FrmModo.Editar, False, False, False, False, True, False)
            Else
                verFrm(FrmModo.Nuevo, False, False, False, False, True, False)
            End If


            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Sub cargarDocumentoRef_GUI(ByVal objDocumento As Entidades.DocumentoCotizacion, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservaciones As Entidades.Observacion)


        With objDocumento

            If (cboTipoDocumento.SelectedValue = 1 And .getObjPersonaView.Ruc = "") Then

                Throw New Exception("DEBE SEÑALAR UN CLIENTE CON RUC")
                Return
            End If
            If (cboTipoDocumento.SelectedValue = 1101353001 And .getObjPersonaView.Ruc = "") Then

                Throw New Exception("DEBE SEÑALAR UN CLIENTE CON RUC")
                Return
            End If

            If ((cboTipoDocumento.SelectedValue = 3 And .getObjPersonaView.Dni = "")) Then

                Throw New Exception("DEBE SEÑALAR UN CLIENTE CON DNI")
                Return
            End If

            If ((cboTipoDocumento.SelectedValue = 1101353002 And .getObjPersonaView.Dni = "")) Then
                Throw New Exception("DEBE SEÑALAR UN CLIENTE CON DNI")
                Return
            End If


            '******************* CARGAMOS LA CABECERA            
            If (Me.cboMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(.IdMoneda)
            End If

            If (Me.cboCondicionPago.Items.FindByValue(CStr(.IdCondicionPago)) IsNot Nothing) Then
                Me.cboCondicionPago.SelectedValue = CStr(.IdCondicionPago)
            End If

            Dim val As String = ""
            val = Me.cboMedioPago.SelectedValue
            If (Me.cboMedioPago.Items.FindByValue(CStr(.getObjAnexoDocumento.IdMedioPago)) IsNot Nothing) Then
                Me.cboMedioPago.SelectedValue = CStr(.getObjAnexoDocumento.IdMedioPago)
            End If

            If (Me.cboMedioPago.SelectedValue = "0") Then
                Me.cboMedioPago.SelectedValue = val
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(.IdTipoOperacion)
            End If

            '*****Me.txtNroDiasVigenciaCredito.Text = CStr(Math.Round(.getObjAnexoDocumento.NroDiasVigencia, 0))

            If (.getObjAnexoDocumento.NroDiasVigencia > 0) Then

                Dim fechaVcto As Date = DateAdd(DateInterval.Day, .getObjAnexoDocumento.NroDiasVigencia, CDate(Me.txtFechaEmision.Text))

            End If

            If (Me.cboAlmacenReferencia.Items.FindByValue(CStr(.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacenReferencia.SelectedValue = CStr(.IdAlmacen)
            End If

            If (Me.cboUsuarioComision.Items.FindByValue(CStr(.IdUsuarioComision)) IsNot Nothing) Then
                Me.cboUsuarioComision.SelectedValue = CStr(.IdUsuarioComision)
            End If

            Me.hddIdDocumentoRef.Value = CStr(.Id)

            '******************************* TOTALES
            Me.txtDescuento.Text = CStr(Math.Round(.Descuento, 2, MidpointRounding.AwayFromZero))
            Me.txtSubTotal.Text = CStr(Math.Round(.SubTotal, 2, MidpointRounding.AwayFromZero))
            Me.txtIGV.Text = CStr(Math.Round(.IGV, 2, MidpointRounding.AwayFromZero))
            Me.txtTotal.Text = CStr(Math.Round(.Total, 2, MidpointRounding.AwayFromZero))

            Me.txtPercepcion.Text = CStr(Math.Round(.Percepcion, 2, MidpointRounding.AwayFromZero))
            Me.txtRetencion.Text = CStr(Math.Round(.Retencion, 2, MidpointRounding.AwayFromZero))
            Me.txtDetraccion.Text = CStr(Math.Round(.Detraccion, 2, MidpointRounding.AwayFromZero))



            Me.txtTotalConcepto.Text = CStr(Math.Round(.getObjAnexoDocumento.TotalConcepto, 2, MidpointRounding.AwayFromZero))
            Me.txtTotalConcepto_PanelDetalle.Text = CStr(Math.Round(.getObjAnexoDocumento.TotalConcepto, 2, MidpointRounding.AwayFromZero))
            Me.txtTotalAPagar.Text = CStr(Math.Round(.TotalAPagar, 2, MidpointRounding.AwayFromZero))
            Me.txtImporteTotal.Text = CStr(Math.Round(.ImporteTotal, 2, MidpointRounding.AwayFromZero))

            '******************************** DATOS PROPIOS DEL DOCUMENTO DE REFERENCIA
            Me.hddIdTipoDocumentoRef.Value = CStr(.IdTipoDocumento)
            Me.hddIdTipoOperacionRef.Value = CStr(.IdTipoOperacion)

            Me.lblTipoOperacionRef.Text = CStr(.NomTipoOperacion)
            Me.lblTipoDocumentoRef.Text = CStr(.NomTipoDocumento)

            Me.lblNroDocumentoRef.Text = CStr(.Serie + " - " + .Codigo)
            Me.lblFechaEmisionRef.Text = Format(.FechaEmision, "dd/MM/yyyy")

            If (.FechaEmision <> Nothing) Then
                Me.lblFechaEmisionRef.Text = Format(.FechaEmision, "dd/MM/yyyy")
            End If
            If (.FechaVenc <> Nothing) Then
                Me.lblFechaVctoRef.Text = Format(.FechaVenc, "dd/MM/yyyy")
            End If

            Me.lblDescripcionClienteRef.Text = CStr(.DescripcionPersona)
            Me.lblRucRef.Text = CStr(.getObjPersonaView.Ruc)
            Me.lblDniRef.Text = CStr(.getObjPersonaView.Dni)
            Me.lblCondicionPagoRef.Text = CStr(.NomCondicionPago)
            Me.lblMedioPagoRef.Text = CStr(.MedioPago)

            If (.getObjAnexoDocumento.NroDiasVigencia > 0) Then
                Me.lblNroDiasVigenciaRef.Text = CStr(Math.Round(.getObjAnexoDocumento.NroDiasVigencia, 0))
            End If

            Me.lblTotalRef.Text = CStr(Math.Round(.Total, 2, MidpointRounding.AwayFromZero))
            Me.lblPercepcionRef.Text = CStr(Math.Round(.Percepcion, 2, MidpointRounding.AwayFromZero))
            Me.lblTotalAPagarRef.Text = CStr(Math.Round(.TotalAPagar, 2, MidpointRounding.AwayFromZero))


            '******************************** PERSONA





            Me.cboTipoPersona_PanelCliente.SelectedValue = CStr(.getObjPersonaView.TipoPersona)





            If (.getObjPersonaView.ApPaterno <> Nothing) Then
                Me.txtApPaterno_RazonSocial.Text = .getObjPersonaView.ApPaterno
            Else
                Me.txtApPaterno_RazonSocial.Text = .getObjPersonaView.RazonSocial
            End If





            Me.txtApMaterno.Text = .getObjPersonaView.ApMaterno
            Me.txtNombres.Text = .getObjPersonaView.Nombres
            Me.txtDni.Text = .getObjPersonaView.Dni
            Me.txtRuc.Text = .getObjPersonaView.Ruc
            Me.txtDireccionCliente.Text = .getObjPersonaView.Direccion
            Me.txtCodigoCliente.Text = CStr(.IdPersona)
            Me.hddIdPersona.Value = CStr(.IdPersona)

            '********************* TIPO AGENTE

            If (Me.cboTipoAgente.Items.FindByValue(CStr(.getObjTipoAgente.IdAgente)) IsNot Nothing) Then
                Me.cboTipoAgente.SelectedValue = CStr(.getObjTipoAgente.IdAgente)
            End If


            Me.txtTasaAgente.Text = CStr(Math.Round(.getObjTipoAgente.Tasa, 2))

            '************** TIPO PV
            If (Me.cboTipoPrecioV.Items.FindByValue(CStr(.IdTipoPV)) IsNot Nothing) Then
                Me.cboTipoPrecioV.SelectedValue = CStr(.IdTipoPV)
            End If


            '*************** LINEA DE CREDITO
            Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(.IdPersona), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
            Me.GV_LineaCredito.DataBind()


            '************ MAESTRO DE OBRA
            Me.txtMaestro.Text = .getObjMaestroObra.Descripcion
            Me.txtDni_Maestro.Text = .getObjMaestroObra.Dni
            Me.txtRuc_Maestro.Text = .getObjMaestroObra.Ruc
            Me.hddIdMaestroObra.Value = CStr(.getObjMaestroObra.IdPersona)

        End With

        Dim objCbo As New Combo

        If (objPuntoPartida IsNot Nothing) Then

            If (Me.cboDepto_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepto_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(0, 2)
                objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, Me.cboDepto_Partida.SelectedValue)
            End If

            If (Me.cboProvincia_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(2, 2)
                objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, Me.cboProvincia_Partida.SelectedValue)
            End If

            If (Me.cboDistrito_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Partida.Text = objPuntoPartida.Direccion

        End If

        If (objPuntoLlegada IsNot Nothing) Then

            If (Me.cboDepartamento.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepartamento.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(0, 2)
                objCbo.LLenarCboProvincia(Me.cboProvincia, Me.cboDepartamento.SelectedValue)
            End If

            If (Me.cboProvincia.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(2, 2)
                objCbo.LLenarCboDistrito(Me.cboDistrito, Me.cboDepartamento.SelectedValue, Me.cboProvincia.SelectedValue)
            End If

            If (Me.cboDistrito.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Llegada.Text = objPuntoLlegada.pll_Direccion

        End If

        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()

        For Each gvRow As GridViewRow In GV_Detalle.Rows

            Dim btnbtnPermisoDcto As ImageButton = DirectCast(gvRow.FindControl("btnPermisoDcto_GV_Detalle"), ImageButton)
            btnbtnPermisoDcto.Enabled = False

        Next

        Me.GV_Concepto.DataSource = listaDetalleConcepto
        Me.GV_Concepto.DataBind()

        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones_DocRef.Text = objObservaciones.Observacion
        End If

        '**************** ACTUALIZAMOS LOS CONTROLES DE CONDICION PAGO
        actualizarControlesMedioPago(objDocumento.IdCondicionPago, objDocumento.getObjAnexoDocumento.IdMedioPago)

    End Sub


    Protected Sub btnRecalcular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRecalcular.Click
        If Validar_AperturaCierreCaja() Then

            registrarDocumentoVenta(True)

        End If
    End Sub
    Private Sub recalcular_VistaPrevia(ByVal listaDocumento As List(Of Entidades.Documento))

        Try

            For i As Integer = 0 To listaDocumento.Count - 1

                listaDocumento(i).NomMoneda = Me.cboMoneda.SelectedItem.ToString

            Next

            Me.GV_VistaPreviaDoc.DataSource = listaDocumento
            Me.GV_VistaPreviaDoc.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapaVistaPrevia();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnGuardar_VistaPrevia_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar_VistaPrevia.Click
        registrarDocumentoVenta(False)
    End Sub



    Private Sub GV_ImpresionDoc_Cab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_ImpresionDoc_Cab.SelectedIndexChanged
        verDetalleDocumento_Print()
    End Sub
    Private Sub verDetalleDocumento_Print()

        Try
            Dim IdDocumento As Integer = CInt(CType(Me.GV_ImpresionDoc_Cab.SelectedRow.FindControl("hddIdDocumentoPrint"), HiddenField).Value)
            Me.GV_ImpresionDoc_Det.DataSource = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
            Me.GV_ImpresionDoc_Det.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnBuscarMaestroObra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarMaestroObra.Click

        buscarMaestroObra()

    End Sub

    Private Sub buscarMaestroObra()

        Try

            Me.gvBuscar.DataSource = Nothing
            Me.gvBuscar.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  mostrarCapaPersona();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoFacturacion(ByVal IdSerie As Integer, ByVal nroDocumento As String, ByVal IdDocumento As Integer)

        Try

            verFrm(FrmModo.Inicio, True, True, True, True, True)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.DocumentoCotizacion = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectCab22Factura(IdSerie, nroDocumento, IdDocumento)
            Dim objDocumentoRef As Entidades.DocumentoCotizacion = New Entidades.DocumentoCotizacion

            If (objDocumento.IdDocumentoRef <> 0) Then
                objDocumentoRef = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectCab22Factura(0, "0", objDocumento.IdDocumentoRef)
            Else
                objDocumentoRef = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectCab22Factura(0, "0", objDocumento.Id)
            End If

            Dim objAnexo_FacturaTransporte As Entidades.Anexo_FacturaTransporte = (New Negocio.Anexo_FacturaTransporte).SelectxIdDocumento(objDocumento.Id)

            'Dim valor As Integer
            'valor = objDocumento.Id

            'lblvalor.Text = CStr(valor)
            'lblvalor.Visible = True

            Me.listaDetalleDocumento = obtenerListaDetalleDocumento_Load(objDocumento.Id, objDocumento.IdTienda)
            Me.listaDetalleConcepto = obtenerListaDetalleConcepto_Load(objDocumento.Id)

            Me.listaCancelacion = obtenerListaCancelacion_Load(objDocumento.Id)

            Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
            Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)
            Dim objObservacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

            '*************** GUARDAMOS EN SESSION
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            setListaCancelacion(Me.listaCancelacion)

            If (Me.cboCaja.Items.FindByValue(CStr(objDocumento.IdCaja)) Is Nothing) And objDocumento.IdCaja > 0 Then : objCombo = New Combo
                Dim objCaja As Entidades.Caja = (New Negocio.Caja).SelectCajaxId(objDocumento.IdCaja)
                cboCaja.Items.Insert(0, New ListItem(objCaja.Nombre, CStr(objCaja.IdCaja)))
            End If

            cargarDocumentoFacturacion_GUI(objDocumento, objDocumentoRef, Me.listaDetalleDocumento, Me.listaDetalleConcepto, Me.listaCancelacion, objPuntoPartida, objPuntoLlegada, objObservacion, objAnexo_FacturaTransporte)

            verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, True, False)

            validarGenerarComprobantePercepcion(objDocumento.Id, objDocumento.IdPersona)

            'se movió a última linea validar caja

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularTotales_GV_Detalle(0); ", True)

            If (New Negocio.Util).valDocComprometerStock(objDocumento.Id) <= 0 Then
                Me.ckProductoComprometidos.Text = "Productos no comprometidos"
                Me.ckProductoComprometidos.Checked = False
            Else
                Me.ckProductoComprometidos.Text = "Productos comprometidos"
                Me.ckProductoComprometidos.Checked = True
            End If
            'última linea
            Validar_AperturaCierreCaja(False)
        Catch ex As Exception

            'objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Sub validarGenerarComprobantePercepcion(ByVal IdDocumento As Integer, ByVal IdPersona As Integer)
        Dim Lista As List(Of Entidades.DocumentoCompPercepcion) = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcionSelectDocReferencia_Find(IdPersona, 1)
        If Not Lista Is Nothing Then
            If Lista.Count > 0 Then
                If Not Lista.Find(Function(k As Entidades.DocumentoCompPercepcion) k.Id = IdDocumento) Is Nothing Then
                    Me.btnEmitirPercepcion.Visible = True
                    Return
                End If
            End If
        End If
        Me.btnEmitirPercepcion.Visible = False
    End Sub

    Private Sub cargarDocumentoFacturacion_GUI(ByVal objDocumento As Entidades.DocumentoCotizacion, ByVal objDocumentoRef As Entidades.DocumentoCotizacion, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservacion As Entidades.Observacion, ByVal objAnexo_FacturaTR As Entidades.Anexo_FacturaTransporte)

        With objDocumento

            '******************* CARGAMOS LA CABECERA
            Me.lblPorcentIgv.Text = CStr(Math.Round(.getObjImpuesto.Tasa, 0))
            If CInt(Me.lblPorcentIgv.Text) <= 0 Then
                Me.lblPorcentIgv.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))
            End If

            Me.txtCodigoDocumento.Text = CStr(.Codigo)
            Me.hddIdDocumento.Value = CStr(.Id)
            Me.hddCodigoDocumento.Value = CStr(.Codigo)
            Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
            Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            'calcula fechas
            Me.txtFechaVcto.Text = Format(.FechaVenc, "dd/MM/yyyy")
            Dim nuevafecha As TimeSpan = Convert.ToDateTime(.FechaVenc).Subtract(Convert.ToDateTime(.FechaEmision))
            Me.txtnrodias.Text = nuevafecha.Days.ToString()

            If (.IdEstadoDoc = 3) Then  '************** ESTADO SALTEADO
                Return
            End If



            If (Me.cboMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(.IdMoneda)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(.IdTipoOperacion)
            End If

            If (Me.cboCondicionPago.Items.FindByValue(CStr(.IdCondicionPago)) IsNot Nothing) Then
                Me.cboCondicionPago.SelectedValue = CStr(.IdCondicionPago)
            End If

            If (Me.cboMedioPago_Credito.Items.FindByValue(CStr(.IdMedioPagoCredito)) IsNot Nothing) Then
                Me.cboMedioPago_Credito.SelectedValue = CStr(.IdMedioPagoCredito)
            End If

            If (Me.cboCaja.Items.FindByValue(CStr(.IdCaja)) IsNot Nothing) Then
                Me.cboCaja.SelectedValue = CStr(.IdCaja)
            End If

            If (.FechaAEntregar <> Nothing) Then
                Me.txtFechaAEntregar.Text = Format(.FechaAEntregar, "dd/MM/yyyy")
            End If

            If (Me.cboAlmacenReferencia.Items.FindByValue(CStr(.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacenReferencia.SelectedValue = CStr(.IdAlmacen)
            End If

            '*********************** USUARIO VENDEDOR
            If (Me.cboUsuarioComision.Items.FindByValue(CStr(.IdUsuarioComision)) IsNot Nothing) Then
                Me.cboUsuarioComision.SelectedValue = CStr(.IdUsuarioComision)
            End If


            '******************************* TOTALES
            Me.txtDescuento.Text = CStr(Math.Round(.Descuento, 2, MidpointRounding.AwayFromZero))
            Me.txtSubTotal.Text = CStr(Math.Round(.SubTotal, 2, MidpointRounding.AwayFromZero))
            Me.txtIGV.Text = CStr(Math.Round(.IGV, 2, MidpointRounding.AwayFromZero))
            Me.txtTotal.Text = CStr(Math.Round(.Total, 2, MidpointRounding.AwayFromZero))

            '***************************** REGIMEN
            Me.txtPercepcion.Text = CStr(Math.Round(.Percepcion, 2, MidpointRounding.AwayFromZero))
            Me.hdddoc_CompPercepcion.Value = CStr(IIf(.CompPercepcion = True, "1", ""))

            Me.txtDetraccion.Text = CStr(Math.Round(.Detraccion, 3))
            Me.txtRetencion.Text = CStr(Math.Round(.Retencion, 3))

            Me.txtTotalConcepto.Text = CStr(Math.Round(.getObjAnexoDocumento.TotalConcepto, 2, MidpointRounding.AwayFromZero))
            Me.txtTotalConcepto_PanelDetalle.Text = CStr(Math.Round(.getObjAnexoDocumento.TotalConcepto, 2, MidpointRounding.AwayFromZero))
            Me.txtTotalAPagar.Text = CStr(Math.Round(.TotalAPagar, 2, MidpointRounding.AwayFromZero))
            Me.txtImporteTotal.Text = CStr(Math.Round(.ImporteTotal, 2, MidpointRounding.AwayFromZero))
            Me.txtDescuentoGlobal.Text = CStr(Math.Round(.getObjAnexoDocumento.anex_DescuentoGlobal, 2, MidpointRounding.AwayFromZero))


            '******************************** DATOS PROPIOS DEL DOCUMENTO DE REFERENCIA
            Me.hddIdDocumentoRef.Value = CStr(objDocumentoRef.Id)
            Me.hddIdTipoDocumentoRef.Value = CStr(objDocumentoRef.IdTipoDocumento)
            Me.hddIdTipoOperacionRef.Value = CStr(objDocumentoRef.IdTipoOperacion)

            Me.lblTipoDocumentoRef.Text = CStr(objDocumentoRef.NomTipoDocumento)
            Me.lblTipoOperacionRef.Text = CStr(objDocumentoRef.NomTipoOperacion)
            Me.lblNroDocumentoRef.Text = CStr(objDocumentoRef.Serie + " - " + objDocumentoRef.Codigo)

            If (objDocumentoRef.FechaEmision <> Nothing) Then
                Me.lblFechaEmisionRef.Text = Format(objDocumentoRef.FechaEmision, "dd/MM/yyyy")
            End If
            If (objDocumentoRef.FechaVenc <> Nothing) Then
                Me.lblFechaVctoRef.Text = Format(objDocumentoRef.FechaVenc, "dd/MM/yyyy")
            End If

            Me.lblDescripcionClienteRef.Text = CStr(objDocumentoRef.DescripcionPersona)
            Me.lblRucRef.Text = CStr(objDocumentoRef.getObjPersonaView.Ruc)
            Me.lblDniRef.Text = CStr(objDocumentoRef.getObjPersonaView.Dni)
            Me.lblCondicionPagoRef.Text = CStr(objDocumentoRef.NomCondicionPago)
            Me.lblMedioPagoRef.Text = CStr(objDocumentoRef.MedioPago)

            If (objDocumentoRef.getObjAnexoDocumento.NroDiasVigencia > 0) Then
                Me.lblNroDiasVigenciaRef.Text = CStr(Math.Round(objDocumentoRef.getObjAnexoDocumento.NroDiasVigencia, 0))
            End If

            Me.lblTotalRef.Text = CStr(Math.Round(objDocumentoRef.Total, 2, MidpointRounding.AwayFromZero))
            Me.lblPercepcionRef.Text = CStr(Math.Round(objDocumentoRef.Percepcion, 2, MidpointRounding.AwayFromZero))
            Me.lblTotalAPagarRef.Text = CStr(Math.Round(objDocumentoRef.TotalAPagar, 2, MidpointRounding.AwayFromZero))

            If (.getObjPersonaView.SujetoARetencion) Then
                Me.hddSujetoRetencion.Value = "1"
            Else
                Me.hddSujetoRetencion.Value = "0"
            End If

            '******************************** PERSONA
            If (.getObjPersonaView.ApPaterno <> Nothing) Then
                Me.txtApPaterno_RazonSocial.Text = .getObjPersonaView.ApPaterno
            Else
                Me.txtApPaterno_RazonSocial.Text = .getObjPersonaView.RazonSocial
            End If
            Me.cboTipoPersona_PanelCliente.SelectedValue = CStr(.getObjPersonaView.TipoPersona)
            Me.txtApMaterno.Text = .getObjPersonaView.ApMaterno
            Me.txtNombres.Text = .getObjPersonaView.Nombres
            Me.txtDni.Text = .getObjPersonaView.Dni
            Me.txtRuc.Text = .getObjPersonaView.Ruc
            Me.txtDireccionCliente.Text = .getObjPersonaView.Direccion
            Me.txtCodigoCliente.Text = CStr(.IdPersona)
            Me.hddIdPersona.Value = CStr(.IdPersona)

            '********************* TIPO AGENTE
            If (Me.cboTipoAgente.Items.FindByValue(CStr(.getObjTipoAgente.IdAgente)) IsNot Nothing) Then
                Me.cboTipoAgente.SelectedValue = CStr(.getObjTipoAgente.IdAgente)
            End If


            Me.txtTasaAgente.Text = CStr(Math.Round(.getObjTipoAgente.Tasa, 2))

            '************** TIPO PV
            If (Me.cboTipoPrecioV.Items.FindByValue(CStr(.IdTipoPV)) IsNot Nothing) Then
                Me.cboTipoPrecioV.SelectedValue = CStr(.IdTipoPV)
            End If

            '*************** LINEA DE CREDITO
            Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(.IdPersona), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
            Me.GV_LineaCredito.DataBind()

            '************ MAESTRO DE OBRA
            Me.txtMaestro.Text = .getObjMaestroObra.Descripcion
            Me.txtDni_Maestro.Text = .getObjMaestroObra.Dni
            Me.txtRuc_Maestro.Text = .getObjMaestroObra.Ruc
            Me.hddIdMaestroObra.Value = CStr(.getObjMaestroObra.IdPersona)

        End With

        Dim objCbo As New Combo

        If (objPuntoPartida IsNot Nothing) Then

            If (Me.cboDepto_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepto_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(0, 2)
                objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, Me.cboDepto_Partida.SelectedValue)
            End If

            If (Me.cboProvincia_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(2, 2)
                objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, Me.cboProvincia_Partida.SelectedValue)
            End If

            If (Me.cboDistrito_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Partida.Text = objPuntoPartida.Direccion

        End If

        If (objPuntoLlegada IsNot Nothing) Then

            If (Me.cboDepartamento.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepartamento.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(0, 2)
                objCbo.LLenarCboProvincia(Me.cboProvincia, Me.cboDepartamento.SelectedValue)
            End If

            If (Me.cboProvincia.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(2, 2)
                objCbo.LLenarCboDistrito(Me.cboDistrito, Me.cboDepartamento.SelectedValue, Me.cboProvincia.SelectedValue)
            End If

            If (Me.cboDistrito.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Llegada.Text = objPuntoLlegada.pll_Direccion

        End If

        If (objObservacion IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservacion.Observacion
        End If

        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()

        Me.GV_Concepto.DataSource = listaDetalleConcepto
        Me.GV_Concepto.DataBind()

        Me.GV_Cancelacion.DataSource = listaCancelacion
        Me.GV_Cancelacion.DataBind()


        '**************** ANEXO DOCUMENTO
        If (objAnexo_FacturaTR IsNot Nothing) Then

            If (Me.cboValorReferencial.Items.FindByValue(CStr(objAnexo_FacturaTR.IdValorReferencial)) IsNot Nothing) Then
                Me.cboValorReferencial.SelectedValue = CStr(objAnexo_FacturaTR.IdValorReferencial)
            End If
            If (Me.cboMoneda_ValorRef.Items.FindByValue(CStr(objAnexo_FacturaTR.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda_ValorRef.SelectedValue = CStr(objAnexo_FacturaTR.IdMoneda)
            End If

            Me.txtCantidad_Transporte.Text = Format(objAnexo_FacturaTR.Cantidad, "F3")
            Me.txtValorReferencial.Text = Format(objAnexo_FacturaTR.ValorReferencial_Unit, "F3")
            Me.txtValorReferencial_Total.Text = Format(objAnexo_FacturaTR.ValorReferencia_Total, "F3")

        End If

        '**************** ACTUALIZAMOS LOS CONTROLES DE CONDICION PAGO
        actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago.SelectedValue))

    End Sub



    Protected Sub btnAdelanto_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        mostrarCapaAdelanto(CType(sender, ImageButton))
    End Sub

    Private Sub mostrarCapaAdelanto(ByVal btnAdelanto As ImageButton)
        Try

            Dim gvRow As GridViewRow = CType(btnAdelanto.NamingContainer, GridViewRow)
            Me.hddIndexGV_Concepto.Value = CStr(gvRow.RowIndex)

            actualizarControles_Adelanto(0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControles_Adelanto(ByVal motivoAdelanto As Integer)

        '*****  motivoAdelanto       0: Seleccionar    1: Ingresar Nuevo     2: Aplicación

        Try
            Me.cboTipoAdelanto.SelectedValue = CStr(motivoAdelanto)
            Me.Panel_Adelanto_Nuevo.Visible = False
            Me.Panel_Adelanto_Aplicacion.Visible = False
            limpiarControles_Adelanto()

            Select Case motivoAdelanto

                Case 0 '***** 0: Seleccionar

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaAnticipo();  ", True)

                Case 1 '***** 1: Ingresar Nuevo     
                    Me.Panel_Adelanto_Nuevo.Visible = True

                    objScript.onCapa(Me, "capaAdelanto")
                Case 2 '***** 2: Aplicación
                    Me.Panel_Adelanto_Aplicacion.Visible = True

                    Me.GV_BuscarDocRef_AdelantoApp.DataSource = (New Negocio.DocumentoFacturacion).DocumentoSelectDocumentoAnticipo(CInt(Me.hddIdPersona.Value), CInt(Me.cboMoneda.SelectedValue))
                    Me.GV_BuscarDocRef_AdelantoApp.DataBind()

                    If (Me.GV_BuscarDocRef_AdelantoApp.Rows.Count > 0) Then
                        objScript.onCapa(Me, "capaAdelanto")
                    Else
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaAdelanto');      alert('No se hallaron registros.');  ", True)
                    End If

            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub limpiarControles_Adelanto()

        Me.txtMonto_Adelanto_Nuevo.Text = "0"

        Me.GV_BuscarDocRef_AdelantoApp.DataSource = Nothing
        Me.GV_BuscarDocRef_AdelantoApp.DataBind()

    End Sub

    Protected Sub cboTipoAdelanto_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTipoAdelanto.SelectedIndexChanged
        actualizarControles_Adelanto(CInt(Me.cboTipoAdelanto.SelectedValue))
    End Sub

    Protected Sub btnAceptar_MontoAdelanto_Nuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar_MontoAdelanto_Nuevo.Click
        addConceptoAnticipo_Nuevo()
    End Sub

    Private Sub addConceptoAnticipo_Nuevo()

        Try

            Dim montoAnticipo As Decimal = CDec(Me.txtMonto_Adelanto_Nuevo.Text)
            Dim montoTotalRef As Decimal = CDec(Me.lblTotalAPagarRef.Text)
            Dim porcentAnticipo As Decimal = 0
            If (montoTotalRef > 0) Then
                porcentAnticipo = (montoAnticipo / montoTotalRef) * 100
            End If

            Dim listaParametro() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {11})

            If (porcentAnticipo < listaParametro(0)) Then
                Throw New Exception("EL MONTO ESTABLECIDO POR ANTICIPO ES DEL < " + CStr(Math.Round(listaParametro(0), 2)) + " % > DEL MONTO TOTAL. NO SE PERMITE LA OPERACIÓN.")
            End If


            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)
            Me.listaDetalleConcepto(CInt(Me.hddIndexGV_Concepto.Value)).Monto = montoAnticipo
            Me.listaDetalleConcepto(CInt(Me.hddIndexGV_Concepto.Value)).IdDocumentoRef = Nothing
            Me.listaDetalleConcepto(CInt(Me.hddIndexGV_Concepto.Value)).Concepto = Nothing

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            ' **************************** LIMPIO LOS DETALLE DE LOS PRODUCTO
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularTotales_GV_Detalle(0);  calcularTotalConcepto();  offCapa('capaAdelanto'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Protected Sub GV_BuscarDocRef_AdelantoApp_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BuscarDocRef_AdelantoApp.SelectedIndexChanged

        Try

            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)

            Dim monto As Decimal = CDec(CType(Me.GV_BuscarDocRef_AdelantoApp.SelectedRow.FindControl("lblMontoDocRef_AdelantoApp"), Label).Text)
            Dim IdDocumentoRef As Integer = CInt(CType(Me.GV_BuscarDocRef_AdelantoApp.SelectedRow.FindControl("hddIdDocumentoRef_Find"), HiddenField).Value)

            For i As Integer = 0 To Me.listaDetalleConcepto.Count - 1

                If (Me.listaDetalleConcepto(i).IdDocumentoRef = IdDocumentoRef) Then
                    Throw New Exception("EL DOCUMENTO POR ANTICIPO YA HA SIDO AGREGADO. NO SE PERMITE LA OPERACIÓN.")
                End If

            Next

            Dim descripcionConcepto As String
            descripcionConcepto = "Doc. Ref.: " + Me.GV_BuscarDocRef_AdelantoApp.SelectedRow.Cells(1).Text + " Nro. " + CType(Me.GV_BuscarDocRef_AdelantoApp.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text

            Me.listaDetalleConcepto(CInt(Me.hddIndexGV_Concepto.Value)).Monto = -1 * monto  '********** RESTA EL TOTAL DEL DOCUMENTO
            Me.listaDetalleConcepto(CInt(Me.hddIndexGV_Concepto.Value)).IdDocumentoRef = IdDocumentoRef
            Me.listaDetalleConcepto(CInt(Me.hddIndexGV_Concepto.Value)).Concepto = descripcionConcepto

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularTotalConcepto();   offCapa('capaAdelanto');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        limpiarDetalleDocumento()
    End Sub
    Private Sub limpiarDetalleDocumento()
        Try

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "**************************** DATOS DE CANCELACION"

    Private Function obtenerListaCancelacion_Load(ByVal IdDocumento As Integer) As List(Of Entidades.PagoCaja)
        Dim lista As List(Of Entidades.PagoCaja) = (New Negocio.PagoCaja).SelectListaxIdDocumento(IdDocumento)

        For i As Integer = 0 To lista.Count - 1

            Select Case lista(i).IdMedioPagoInterfaz


                Case 1  '*********** EFECTIVO

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ]"

                Case 2  '*********** BANCO  (NRO OPERACION)

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Cuenta: [ " + lista(i).NumeroCuenta + " ] Nro Op: [ " + lista(i).NumeroOp + "]"


                Case 3  '************** BANCO CHEQUE

                    Dim fecha As String = ""

                    If (lista(i).FechaACobrar <> Nothing) Then
                        fecha = Format(lista(i).FechaACobrar, "dd/MM/yyyy")
                    Else
                        fecha = "---"
                    End If

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Fecha a Cobrar: [ " + fecha + " ]"


                Case 4 '************* TARJETA
                    Try : lista(i).NumeroCuenta = (New Negocio.CuentaBancaria).SelectxIdBancoxIdCuentaBancaria(lista(i).IdBanco, lista(i).IdCuentaBancaria).NroCuentaBancaria
                    Catch ex As Exception : End Try

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Cuenta: [ " + lista(i).NumeroCuenta + " ] Operacion: [ " + lista(i).NumeroOp + " ]"

                Case 5   '*********** NOTA DE CREDITO

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Nro. Documento: [ " + lista(i).NumeroOp + " ] "

                Case 6  '************* COMP RETENCION

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Nro. Documento: [ " + lista(i).NumeroOp + " ] "

            End Select


        Next

        Return lista
    End Function

    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged
        Try
            ' CAMBIA LOS PORCENTAJES DE DESCUENTO X ARTICULOS
            actualizarPorcentDctoMax_xArticulo_GV_Detalle()
            actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControlesMedioPago(ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer)

        '************ Limpiar Datos de Cancelación
        Me.txtNro_DC.Text = ""
        Me.txtMonto_DatoCancelacion.Text = "0"
        Me.txtFechaACobrar.Text = ""
        Me.hddPermiso_AddMedioPago.Value = "0"

        Me.btnEnviarSolicitudAprobacíon.Visible = False
        Me.btnConsultarAprobacion.Visible = False

        If (Me.cboCondicionPago.Items.FindByValue(CStr(IdCondicionPago)) IsNot Nothing) Then
            Me.cboCondicionPago.SelectedValue = CStr(IdCondicionPago)
        End If

        Select Case IdCondicionPago
            Case 1  '**************** CONTADO
                Me.Panel_CP_Contado.Visible = True
                Me.Panel_CP_Credito.Visible = False

                '*************** MEDIO DE PAGO
                Me.lblMonto_DC.Visible = False
                Me.txtMonto_DatoCancelacion.Visible = False
                Me.cboMoneda_DatoCancelacion.Visible = False
                Me.btnAddDatoCancelacion.Visible = False

                Me.lblPost_DC.Visible = False
                Me.cboPost_DC.Visible = False
                Me.lblTarjeta_DC.Visible = False
                Me.cboTarjeta_DC.Visible = False
                Me.lblBanco_DC.Visible = False
                Me.cboBanco.Visible = False
                Me.lblCuentaBancaria_DC.Visible = False
                Me.cboCuentaBancaria.Visible = False
                Me.lblNro_DC.Visible = False
                Me.txtNro_DC.Visible = False
                Me.lblFechaACobrar_DC.Visible = False
                Me.txtFechaACobrar.Visible = False

                Me.btnVer_NotaCredito.Visible = False

                Dim objMedioPago As Entidades.MedioPago = (New Negocio.MedioPago).SelectxId(IdMedioPago)(0)

                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True

                Select Case objMedioPago.IdMedioPagoInterfaz

                    Case 1  '*************** EFECTIVO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True


                        '**********


                    Case 2  '*************** BANCO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblFechaACobrar_DC.Text = "Fecha Emision:"
                        Me.txtFechaACobrar.Visible = True


                        actualizarControles_BANCOS()

                    Case 3  '**************** BANCO CHEQUE
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblFechaACobrar_DC.Visible = True
                        Me.lblFechaACobrar_DC.Text = "Fecha a Cobrar:"
                        Me.txtFechaACobrar.Visible = True

                        '*******************   Me.btnEnviarSolicitudAprobacíon.Visible = True
                        '*******************   Me.btnConsultarAprobacion.Visible = True

                        actualizarControles_BANCOS()

                    Case 4  '*********** TARJETA

                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblPost_DC.Visible = True
                        Me.cboPost_DC.Visible = True
                        Me.lblTarjeta_DC.Visible = True
                        Me.cboTarjeta_DC.Visible = True

                        '************ Configuramos los cambios por POST
                        actualizarControles_POST()
                        Me.cboBanco.Enabled = False
                        Me.cboCuentaBancaria.Enabled = False

                    Case 5  '************* NOTA CREDITO

                        Me.btnVer_NotaCredito.Visible = True

                    Case 6  '************** COMPROBANTE DE RETENCION

                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                End Select

                Me.hddIdMedioPagoInterfaz.Value = CStr(objMedioPago.IdMedioPagoInterfaz)

            Case 2  '**************** CREDITO

                If (Me.cboMedioPago_Credito.Items.FindByValue(CStr(IdMedioPago)) IsNot Nothing) Then
                    Me.cboMedioPago_Credito.SelectedValue = CStr(IdMedioPago)
                End If

                Me.Panel_CP_Contado.Visible = False
                Me.Panel_CP_Credito.Visible = True

        End Select

    End Sub

    Protected Sub btnAddDatoCancelacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDatoCancelacion.Click

        addDatoCancelacion()

    End Sub

    Private Sub addDatoCancelacion()
        Try



            If (CInt(Me.hddPermiso_AddMedioPago.Value) <= 0) Then
                If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(CInt(Session("IdUsuario")), CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then


                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');     verCapaPermiso_AddMedioPago();  ", True)

                    If (CInt(Me.hddIdMedioPagoInterfaz.Value) = _MedioPago_Interfaz_Cheque) Then
                        Me.btnEnviarSolicitudAprobacíon.Visible = True
                        Me.btnConsultarAprobacion.Visible = True
                    End If

                    Return
                End If
            End If


            'For Each objPagCaj As Entidades.PagoCaja In getListaCancelacion()
            '    If objPagCaj.IdMedioPago = 1 And objPagCaj.Efectivo = 0 Then
            '        objPagCaj.MontoEquivalenteDestino = CDec(Me.txtTotalAPagar.Text)
            '        objPagCaj.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(CDec(Me.txtTotalAPagar.Text), 2)) + " ]"
            '    End If
            'Next


            Me.listaCancelacion = getListaCancelacion()


            Dim objPagoCaja As New Entidades.PagoCaja

            objPagoCaja.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objPagoCaja.Factor = 1
            objPagoCaja.IdTipoMovimiento = 1 '*************** INGRESO
            objPagoCaja.IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

            Select Case CInt(Me.hddIdMedioPagoInterfaz.Value)

                Case 1  '*********** EFECTIVO

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ]"
                    'objPagoCaja.Vueltox = CDec(txtVuelto.Text)
                    'objPagoCaja.DifTotalxPago = CDec(objPagoCaja.Efectivo - objPagoCaja.Vueltox)

                Case 2  '*********** BANCO  (NRO OPERACION)

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)

                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)

                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedItem.ToString + " ]"
                    objPagoCaja.NumeroOp = Me.txtNro_DC.Text
                    objPagoCaja.FechaACobrar = CDate(Me.txtFechaACobrar.Text)

                Case 3  '************** BANCO CHEQUE

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)

                    Dim fecha As String = ""
                    Try
                        objPagoCaja.FechaACobrar = CDate(Me.txtFechaACobrar.Text)
                        fecha = Me.txtFechaACobrar.Text
                    Catch ex As Exception
                        objPagoCaja.FechaACobrar = Nothing
                        fecha = "---"
                    End Try

                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Fecha a Cobrar: [ " + fecha + " ]"
                    objPagoCaja.NumeroCheque = Me.txtNro_DC.Text

                Case 4 '************* TARJETA

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.NumeroOp = CStr(Me.txtNro_DC.Text.Trim)

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    '' Aqui puede haber errores ya que verificar despues el modelo de base de datos 
                    objPagoCaja.IdPost = (New Negocio.PostView).SelectIdPosxTipoTarjetaCaja(CInt(Me.cboTarjeta_DC.SelectedValue), CInt(Me.cboPost_DC.SelectedValue), CInt(Me.cboCaja.SelectedValue))
                    objPagoCaja.IdTarjeta = CInt(Me.cboTarjeta_DC.SelectedValue)
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedItem.ToString + " ] Operacion: [ " + CStr(Me.txtNro_DC.Text.Trim) + " ]"

                Case 5   '*********** NOTA DE CREDITO

                    '*************** NO SE UTILIZA

                Case 6   '************** COMP RETENCION


                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.NumeroOp = CStr(Me.txtNro_DC.Text.Trim)
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] "

            End Select

            Me.listaCancelacion.Add(objPagoCaja)
            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            Me.txtMonto_DatoCancelacion.Text = "0"
            Me.txtNro_DC.Text = ""
            Me.txtFechaACobrar.Text = ""

            If CInt(Me.hddIdMedioPagoInterfaz.Value) = 1 Then 'Efectivo He puesto esto por que si el usuario no puede manipular los detalle los calculos se pierden Verificar !!!!!

                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularTotales_GV_Detalle(0); ", True)

            Else

                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularTotales_GV_Detalle(0);  calcularDatosCancelacion(); ", True)

            End If



        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub
    Private Function getListaCancelacionXDefecto() As List(Of Entidades.PagoCaja)
        Dim listCancelacion As New List(Of Entidades.PagoCaja)
        Dim objPagoCaja As New Entidades.PagoCaja

        Try

            If (CInt(Me.hddPermiso_AddMedioPago.Value) <= 0) Then
                If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(CInt(Session("IdUsuario")), CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then


                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');     verCapaPermiso_AddMedioPago();  ", True)

                    If (CInt(Me.hddIdMedioPagoInterfaz.Value) = _MedioPago_Interfaz_Cheque) Then
                        Me.btnEnviarSolicitudAprobacíon.Visible = True
                        Me.btnConsultarAprobacion.Visible = True
                    End If

                    'Return

                End If
            End If

            objPagoCaja.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objPagoCaja.Factor = 1
            objPagoCaja.IdTipoMovimiento = 1 '*************** INGRESO
            objPagoCaja.IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

            objPagoCaja.EfectivoInicial = CDec(Me.txtTotalAPagar.Text)
            objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
            objPagoCaja.Efectivo = CDec(Me.txtTotalAPagar.Text)
            objPagoCaja.EfectivoInicial = CDec(Me.txtTotalAPagar.Text)
            objPagoCaja.IdMoneda = CInt(Me.cboMoneda.SelectedValue) 'CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
            objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
            objPagoCaja.MontoEquivalenteDestino = CDec(Me.txtTotalAPagar.Text) '(New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
            objPagoCaja.descripcionPagoCaja = " Medio Pago: [  EFECTIVO  ] Monto: [ " + Me.cboMoneda.SelectedItem.ToString + " " + CStr(Math.Round(CDec(txtTotalAPagar.Text), 2)) + " ]"
            objPagoCaja.Vueltox = CDec(txtVuelto.Text)
            objPagoCaja.DifTotalxPago = CDec(objPagoCaja.Efectivo - objPagoCaja.Vueltox)
            listCancelacion.Add(objPagoCaja)



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

        Return listCancelacion
    End Function

    Private Sub addDatoCancelacionxDefecto()
        Try

            If (CInt(Me.hddPermiso_AddMedioPago.Value) <= 0) Then
                If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(CInt(Session("IdUsuario")), CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then


                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');     verCapaPermiso_AddMedioPago();  ", True)

                    If (CInt(Me.hddIdMedioPagoInterfaz.Value) = _MedioPago_Interfaz_Cheque) Then
                        Me.btnEnviarSolicitudAprobacíon.Visible = True
                        Me.btnConsultarAprobacion.Visible = True
                    End If

                    Return

                End If
            End If

            Me.listaCancelacion = getListaCancelacion()

            Dim objPagoCaja As New Entidades.PagoCaja

            objPagoCaja.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objPagoCaja.Factor = 1
            objPagoCaja.IdTipoMovimiento = 1 '*************** INGRESO
            objPagoCaja.IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

            Select Case CInt(Me.hddIdMedioPagoInterfaz.Value)

                Case 1  '*********** EFECTIVO

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ]"

                Case 2  '*********** BANCO  (NRO OPERACION)

                Case 3  '************** BANCO CHEQUE

                Case 4 '************* TARJETA

                Case 5   '*********** NOTA DE CREDITO

                    '*************** NO SE UTILIZA

                Case 6   '************** COMP RETENCION

            End Select

            Me.listaCancelacion.Add(objPagoCaja)
            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            Me.txtMonto_DatoCancelacion.Text = "0"
            Me.txtNro_DC.Text = ""
            Me.txtFechaACobrar.Text = ""

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacionXDefecto(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub



    Protected Sub GV_Cancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Cancelacion.SelectedIndexChanged
        quitarDatoCancelacion()
    End Sub
    Private Sub quitarDatoCancelacion()

        Try

            Me.listaCancelacion = getListaCancelacion()
            Me.listaCancelacion.RemoveAt(Me.GV_Cancelacion.SelectedRow.RowIndex)

            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub GV_NotaCredito_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_NotaCredito.SelectedIndexChanged
        agregarDatoCancelacion_NC()
    End Sub

    Private Sub agregarDatoCancelacion_NC()
        Try

            '************** VALIDAMOS SALDO
            Dim saldo As Decimal = CDec(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblSaldo_Find"), Label).Text)
            Dim monto As Decimal = CDec(CType(Me.GV_NotaCredito.SelectedRow.FindControl("txtMontoRecibir_Find"), TextBox).Text)
            Dim moneda As String = CStr(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblMonedaMontoRecibir_Find"), Label).Text)
            Dim IdDocumento As Integer = CInt(CType(Me.GV_NotaCredito.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value)
            Dim IdMonedaNC As Integer = CInt(CType(Me.GV_NotaCredito.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value)
            Dim nroDocumento As String = CStr(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text)

            If (monto > saldo) Then
                Throw New Exception("El Monto Ingresado excede al saldo disponible [ " + moneda + " " + CStr(Math.Round(saldo, 2)) + " ].")
            Else
                '**************** Validamos el dato en Grilla de cancelación
                Me.listaCancelacion = getListaCancelacion()
                For i As Integer = 0 To Me.listaCancelacion.Count - 1
                    If (CInt(Me.listaCancelacion(i).NumeroCheque) = IdDocumento) Then
                        Throw New Exception("La Nota de Crédito seleccionado ya se encuentra en [ Datos de Cancelación ].")
                    End If
                Next

                '**************** Agregamos el dato de cancelación
                Dim objPagoCaja As New Entidades.PagoCaja
                With objPagoCaja

                    .Efectivo = monto
                    .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                    .IdMoneda = CInt(IdMonedaNC)
                    .IdTipoMovimiento = 1   '******** Ingreso
                    .Factor = 1

                    .NumeroCheque = CStr(IdDocumento)

                    .NomMonedaDestino = Me.cboMoneda.SelectedItem.ToString
                    .IdMonedaDestino = CInt(cboMoneda.SelectedValue)
                    .MontoEquivalenteDestino = (New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(.IdMoneda, .IdMonedaDestino, .Efectivo, "E")
                    .descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Nro. Documento: [ " + nroDocumento + " ]"

                    .IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

                End With
                Me.listaCancelacion.Add(objPagoCaja)
                setListaCancelacion(Me.listaCancelacion)

                '************* Mostramos la grilla
                Me.GV_Cancelacion.DataSource = Me.listaCancelacion
                Me.GV_Cancelacion.DataBind()

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " offCapa('capaDocumento_NotaCredito');  calcularDatosCancelacion();", True)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub mostrarDocumentosNotaCredito()
        Try

            If (CInt(Me.hddPermiso_AddMedioPago.Value) <= 0) Then
                If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(CInt(Session("IdUsuario")), CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');     verCapaPermiso_AddMedioPago();   ", True)
                    Return

                End If
            End If

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoSelectCabFind_Aplicacion(CInt(Me.hddIdPersona.Value), 0, 0)

            If (lista.Count > 0) Then

                Me.GV_NotaCredito.DataSource = lista
                Me.GV_NotaCredito.DataBind()
                objScript.onCapa(Me, "capaDocumento_NotaCredito")

            Else

                Throw New Exception("No se hallaron registros.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnVer_NotaCredito_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVer_NotaCredito.Click
        mostrarDocumentosNotaCredito()
    End Sub


    Private Sub cboPost_DC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPost_DC.SelectedIndexChanged
        Try
            actualizarControles_POST()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControles_POST()

        Dim objcbo As New Combo

        With objcbo
          
            .LlenarCboTarjetaxTipoTarjetaCaja(Me.cboTarjeta_DC, CInt(Me.cboPost_DC.SelectedValue), CInt(Me.cboCaja.SelectedValue))

            Dim objPost As Entidades.Post = (New Negocio.PostView).SelectxIdPost(CInt(Me.cboPost_DC.SelectedValue))

            '************* Banco Cuenta bancaria
            Me.cboBanco.SelectedValue = CStr(objPost.IdBanco)

            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
            Me.cboCuentaBancaria.SelectedValue = CStr(objPost.IdCuentaBancaria)

        End With

    End Sub

    Private Sub actualizarControles_BANCOS()

        Dim objCbo As New Combo
        With objCbo
            .LlenarCboBanco(Me.cboBanco, False)
            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
        End With

    End Sub

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

    Private Sub btnAceptar_AddMedioPago_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_AddMedioPago.Click
        validarLogeo_PermisoAddMedioPago()
    End Sub
    Private Sub validarLogeo_PermisoAddMedioPago()

        Try

            If (Me.txtClave_AddMedioPago.Text.Trim <> Me.txtClave2_AddMedioPago.Text.Trim) Then
                Throw New Exception("LAS CLAVES INGRESADAS NO COINCIDEN. NO SE PERMITE LA OPERACIÓN.")
            End If

            Dim IdUsuario As Integer = (New Negocio.Usuario).ValidarIdentidad(Me.txtUsuario_AddMedioPago.Text.Trim, Me.txtClave_AddMedioPago.Text.Trim)

            If (IdUsuario = Nothing) Then
                Throw New Exception("USUARIO Y/O CLAVE INCORRECTOS. NO SE PERMITE LA OPERACIÓN.")
            End If

            If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(IdUsuario, CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');   verCapaPermiso_AddMedioPago();   ", True)
                Return

            End If

            Me.hddPermiso_AddMedioPago.Value = "1"
            objScript.mostrarMsjAlerta(Me, ("Se ha concedido la autorización para agregar el Medio de Pago < " + Me.cboMedioPago.SelectedItem.ToString + " >"))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try






    End Sub
#Region "**************************    CALCULAR EQUIVALENCIAS"
    ''DESHABILITADO....
    'Protected Sub btnEquivalencia_PR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    '    Try

    '        ActualizarListaDetalleDocumento()
    '        Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)

    '        Me.listaDetalleDocumento = getListaDetalleDocumento()


    '        '******************** CARGAMOS LOS CONTROLES
    '        Me.cboUnidadMedida_Ingreso.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
    '        Me.cboUnidadMedida_Ingreso.DataBind()

    '        Me.cboUnidadMedida_Salida.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
    '        Me.cboUnidadMedida_Salida.DataBind()

    '        Me.GV_CalcularEquivalencia.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
    '        Me.GV_CalcularEquivalencia.DataBind()

    '        Me.GV_ResuldoEQ.DataSource = Nothing
    '        Me.GV_ResuldoEQ.DataBind()

    '        Me.txtCantidad_Ingreso.Text = CStr(Me.listaDetalleDocumento(index).Cantidad)
    '        Me.txtCantidad_Salida.Text = ""

    '        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaEquivalencia_PR();  ", True)

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try

    'End Sub


    Protected Sub btnCalcular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCalcular.Click
        calcularEquivalencia()
    End Sub
    Private Sub calcularEquivalencia()
        Try

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()


            Dim tableUM As DataTable = obtenerTablaUM(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_ResuldoEQ.DataSource = (New Negocio.Producto).Producto_ConsultaEquivalenciaProductoxParams(Me.listaDetalleDocumento(index).IdProducto, CInt(Me.cboUnidadMedida_Ingreso.SelectedValue), CInt(Me.cboUnidadMedida_Salida.SelectedValue), CDec(Me.txtCantidad_Ingreso.Text), tableUM, CInt(Me.rbl_UtilizarRedondeo.SelectedValue))
            Me.GV_ResuldoEQ.DataBind()

            Me.GV_ResuldoEQ.Rows(Me.GV_ResuldoEQ.Rows.Count - 1).BackColor = Drawing.Color.Yellow

            Dim cantidadEq As Decimal = CDec(Me.GV_ResuldoEQ.Rows(Me.GV_ResuldoEQ.Rows.Count - 1).Cells(2).Text)
            Me.txtCantidad_Salida.Text = CStr(cantidadEq)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaEquivalencia_PR();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerTablaUM(ByVal IdProducto As Integer) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        For i As Integer = 0 To Me.GV_CalcularEquivalencia.Rows.Count - 1

            If (CType(Me.GV_CalcularEquivalencia.Rows(i).FindControl("chb_UnidadMedida"), CheckBox).Checked) Then
                Dim IdUnidadMedida As Integer = CInt(CType(Me.GV_CalcularEquivalencia.Rows(i).FindControl("hddIdUnidadMedida"), HiddenField).Value)
                dt.Rows.Add(IdProducto, IdUnidadMedida)
            End If

        Next

        Return dt


    End Function
    Private Sub aceptarEquivalencia_PR()
        Try

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim cantidadNew As Decimal = CDec(Me.txtCantidad_Salida.Text)
            '' Add 
            If listaDetalleDocumento(index).ListaUM_Venta.Find(Function(U As Entidades.UnidadMedida) U.Id = CInt(Me.cboUnidadMedida_Salida.SelectedValue)) IsNot Nothing Then
                Me.listaDetalleDocumento(index).Cantidad = cantidadNew
                Me.listaDetalleDocumento(index).IdUnidadMedida = CInt(Me.cboUnidadMedida_Salida.SelectedValue)
                Me.listaDetalleDocumento(index).UMedida = CStr(Me.cboUnidadMedida_Salida.SelectedItem.ToString)
            End If


            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);   offCapa('capaEquivalenciaProducto');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub btnAceptar_EquivalenciaPR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_EquivalenciaPR.Click

        aceptarEquivalencia_PR()

    End Sub
#End Region
    Protected Sub btnMostrarComponenteKit_Find_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Me.listaCatalogo = getListaCatalogo()

            '****************** MOSTRAMOS LOS COMPONENTES DEL KIT
            Me.GV_ComponenteKit_Find.DataSource = (New Negocio.Kit).SelectComponentexIdKit(Me.listaCatalogo(index).IdProducto)
            Me.GV_ComponenteKit_Find.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub btnMostrarComponenteKit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            ActualizarListaDetalleDocumento()
            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            'Select Case CInt(Me.hddFrmModo.Value)
            '    Case FrmModo.Nuevo

            '****************** MOSTRAMOS LOS COMPONENTES DEL KIT
            Me.GV_ComponenteKit.DataSource = (New Negocio.Kit).SelectComponentexIdKit(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_ComponenteKit.DataBind()

            '    Case FrmModo.Editar

            ''************* MOSTRAMOS LOS COMPONENTES REGISTRADOS EN EL DOCUMENTO
            'Me.GV_ComponenteKit.DataSource = (New Negocio.DetalleDocumento).SelectComponenteKitxIdKitxIdDocumento(Me.listaDetalleDocumento(index).IdProducto, CInt(Me.hddIdDocumento.Value))
            'Me.GV_ComponenteKit.DataBind()

            'End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub



    Private Sub calcularRetazo(ByVal opcion As String)
        Try



            ActualizarListaDetalleDocumento()

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            Me.listaDetalleDocumento = getListaDetalleDocumento()





        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectedIndexChanged
        valOnChange_cboMoneda()
    End Sub
    Private Sub valOnChange_cboMoneda()

        Try

            actualizarMontoMaxAfecto_TipoDocumentoAE()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub valOnClick_btnViewCampania(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        visualizarCampania_Producto_Find(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex)

    End Sub
    Private Sub visualizarCampania_Producto_Find(ByVal index As Integer)
        Try

            Me.listaCatalogo = getListaCatalogo()

            Dim IdProducto As Integer = Me.listaCatalogo(index).IdProducto

            Me.lblProducto_capaCampania_Producto.Text = Me.listaCatalogo(index).Descripcion
            Me.lblCodigoProducto_capaCampania_Producto.Text = Me.listaCatalogo(index).CodigoProducto

            Me.GV_Campania_Producto.DataSource = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente_SelectxParams_DT(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdProducto, Nothing, CDate(Me.txtFechaEmision.Text))
            Me.GV_Campania_Producto.DataBind()

            Me.hddIndex_Campania_Producto.Value = CStr(index)
            Me.hddIndex_GV_Detalle.Value = CStr(-1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "      onCapa('capaBuscarProducto_AddProd');   onCapa2('capaCampania_Producto');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Campania_Producto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Campania_Producto.SelectedIndexChanged
        valOnChange_GV_Campania_Producto(Me.GV_Campania_Producto.SelectedIndex)
    End Sub
    Private Sub valOnChange_GV_Campania_Producto(ByVal index As Integer)

        Try

            Dim IdCampania As Integer = CInt(CType(Me.GV_Campania_Producto.Rows(index).FindControl("hddIdCampania"), HiddenField).Value)
            Dim IdProducto As Integer = CInt(CType(Me.GV_Campania_Producto.Rows(index).FindControl("hddIdProducto"), HiddenField).Value)
            Dim IdUnidadMedida As Integer = CInt(CType(Me.GV_Campania_Producto.Rows(index).FindControl("hddIdUnidadMedida"), HiddenField).Value)

            Dim objCampania_Detalle As Entidades.Campania_Detalle = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente(IdCampania, IdProducto, IdUnidadMedida)

            If (objCampania_Detalle IsNot Nothing) Then

                Dim listaCampaniaTG As List(Of Entidades.Campania_DetalleTG) = (New Negocio.Campania_DetalleTG).Campania_DetalleTGxIdCampaniaDetalle(objCampania_Detalle.IdCampaniaDetalle)

                Dim opcionLista As Integer = 0
                Dim indexLista As Integer = 0
                '*********** LISTA CATALOGO = 0       LISTA DETALLE DOCUMENTO = 1

                If (CInt(Me.hddIndex_Campania_Producto.Value) < 0) Then
                    opcionLista = 1  '************** TRABAJA CON LISTA DETALLE
                    indexLista = CInt(Me.hddIndex_GV_Detalle.Value)
                Else
                    opcionLista = 0  '************** TRABAJA CON LISTA CATALOGO
                    indexLista = CInt(Me.hddIndex_Campania_Producto.Value)
                End If

                ActualizarListaDetalleDocumento()
                Me.listaDetalleDocumento = getListaDetalleDocumento()
                Dim dcto, porcentDcto As Decimal

                Select Case opcionLista

                    Case 0

                        Dim objDetalleDocumento As New Entidades.DetalleDocumento
                        Me.listaCatalogo = getListaCatalogo()
                        dcto = (Me.listaCatalogo(indexLista).PrecioLista - objCampania_Detalle.Precio)

                        If (dcto < 0) Then
                            dcto = 0
                        Else

                            porcentDcto = Math.Round(((dcto) / Me.listaCatalogo(indexLista).PrecioLista) * 100, 1)

                        End If

                        With objDetalleDocumento

                            .IdProducto = objCampania_Detalle.IdProducto
                            .NomProducto = objCampania_Detalle.Producto
                            .Cantidad = objCampania_Detalle.CantidadMin
                            .IdUnidadMedida = objCampania_Detalle.IdUnidadMedida
                            .PrecioCD = objCampania_Detalle.Precio
                            .Importe = .Cantidad * .PrecioCD
                            .PrecioBaseDcto = _PrecioBaseDcto_Lista
                            .VolumenVentaMin = objCampania_Detalle.CantidadMin
                            .IdCampania = objCampania_Detalle.IdCampania
                            .IdCampaniaDetalle = objCampania_Detalle.IdCampaniaDetalle
                            .ExisteCampania_Producto = True

                            .ListaUM_Venta = Me.listaCatalogo(indexLista).ListaUM_Venta
                            .StockDisponibleN = Me.listaCatalogo(indexLista).StockDisponibleN
                            .PrecioLista = Me.listaCatalogo(indexLista).PrecioLista
                            .PrecioSD = Me.listaCatalogo(indexLista).PrecioLista
                            .Descuento = dcto
                            .PorcentDcto = porcentDcto
                            .TasaPercepcion = Me.listaCatalogo(indexLista).Percepcion
                            .PorcentDctoMaximo = porcentDcto
                            .IdUsuarioSupervisor = 0
                            .IdTienda = Me.listaCatalogo(indexLista).IdTienda
                            .IdTipoPV = Me.listaCatalogo(indexLista).IdTipoPV
                            .pvComercial = Me.listaCatalogo(indexLista).pvComercial
                            .CodigoProducto = Me.listaCatalogo(indexLista).CodigoProducto
                            .Kit = Me.listaCatalogo(indexLista).Kit

                            Me.listaDetalleDocumento.Add(objDetalleDocumento)

                        End With

                        Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumentoTG(listaCampaniaTG, objCampania_Detalle.IdProducto, objCampania_Detalle.CantidadMin))

                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);   onCapa('capaBuscarProducto_AddProd');    ", True)

                    Case 1

                        dcto = (Me.listaDetalleDocumento(indexLista).PrecioLista - objCampania_Detalle.Precio)

                        If (dcto < 0) Then
                            dcto = 0
                        Else

                            porcentDcto = Math.Round(((dcto) / Me.listaDetalleDocumento(indexLista).PrecioLista) * 100, 1)

                        End If

                        With Me.listaDetalleDocumento(indexLista)

                            If (.Cantidad < objCampania_Detalle.CantidadMin) Then
                                .Cantidad = objCampania_Detalle.CantidadMin
                            End If

                            .IdUnidadMedida = objCampania_Detalle.IdUnidadMedida
                            .PrecioCD = objCampania_Detalle.Precio
                            .Importe = .Cantidad * .PrecioCD
                            .PrecioBaseDcto = _PrecioBaseDcto_Lista
                            .VolumenVentaMin = objCampania_Detalle.CantidadMin
                            .IdCampania = objCampania_Detalle.IdCampania
                            .IdCampaniaDetalle = objCampania_Detalle.IdCampaniaDetalle
                            .ExisteCampania_Producto = True

                            .Descuento = dcto
                            .PorcentDcto = porcentDcto
                            .PorcentDctoMaximo = porcentDcto

                        End With

                        Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumentoTG(listaCampaniaTG, Me.listaDetalleDocumento(indexLista).IdProducto, objCampania_Detalle.CantidadMin))

                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);  ", True)

                End Select

                setListaDetalleDocumento(Me.listaDetalleDocumento)

                Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                Me.GV_Detalle.DataBind()

            Else
                Throw New Exception("NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaDetalleDocumentoTG(ByVal listaDetalleCampaniaTG As List(Of Entidades.Campania_DetalleTG), ByVal IdProductoCampania As Integer, ByVal CantMin As Decimal) As List(Of Entidades.DetalleDocumento)
        Dim objDetalleDocumentoTG As Entidades.DetalleDocumento
        Dim listaDetalleDocumentoTG As New List(Of Entidades.DetalleDocumento)

        For Each objCampaniaDetalleTG As Entidades.Campania_DetalleTG In listaDetalleCampaniaTG
            objDetalleDocumentoTG = New Entidades.DetalleDocumento
            With objDetalleDocumentoTG

                .IdCampania = objCampaniaDetalleTG.IdCampania
                .ExisteCampania_Producto = False
                .IdProducto = objCampaniaDetalleTG.IdProducto
                .NomProducto = objCampaniaDetalleTG.Producto
                .Cantidad = objCampaniaDetalleTG.cdtg_Cantidad
                .IdUnidadMedida = objCampaniaDetalleTG.IdUnidadMedida
                .PrecioBaseDcto = _PrecioBaseDcto_Lista
                .VolumenVentaMin = objCampaniaDetalleTG.cdtg_Cantidad

                .PorcentDcto = 100
                .PorcentDctoMaximo = 100

                .PrecioCD = objCampaniaDetalleTG.cdtg_Precio * (1 - .PorcentDcto / 100)
                .Importe = .Cantidad * .PrecioCD

                .ListaUM_Venta = objCampaniaDetalleTG.getListaUnidadMedida
                .StockDisponibleN = (New Negocio.Util).fx_StockDisponible(CInt(cboEmpresa.SelectedValue), CInt(cboAlmacenReferencia.SelectedValue), objCampaniaDetalleTG.IdProducto)
                .PrecioLista = (New Negocio.ProductoTipoPV).SelectValorCalculadoxParams(objCampaniaDetalleTG.IdProducto, CInt(cboTienda.SelectedValue), CInt(cboTipoPrecioV.SelectedValue), objCampaniaDetalleTG.IdUnidadMedida, CInt(cboMoneda.SelectedValue))
                .PrecioSD = .PrecioLista
                .CodigoProducto = objCampaniaDetalleTG.CodigoProducto
                .Kit = False
                .IdCampania = objCampaniaDetalleTG.IdCampania
                .IdCampaniaDetalle = objCampaniaDetalleTG.IdCampaniaDetalle
                .IdProductoAux = IdProductoCampania
                ' verifica si se incrementa la transferencia deacuerdo a la cant de la campania
                If objCampaniaDetalleTG.cdtg_cantCampania Then
                    .CantidadDetalleAfecto = CantMin ' volumen de venta minima para la campania
                End If


                listaDetalleDocumentoTG.Add(objDetalleDocumentoTG)

            End With

        Next

        Return listaDetalleDocumentoTG
    End Function
    ''DESHABILITADO.....
    Protected Sub valOnClick_btnViewCampania_GV_Detalle(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        visualizarCampania_Producto_GV_Detalle(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex)

    End Sub
    Private Sub visualizarCampania_Producto_GV_Detalle(ByVal index As Integer)
        Try

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim IdProducto As Integer = Me.listaDetalleDocumento(index).IdProducto

            Me.lblProducto_capaCampania_Producto.Text = Me.listaDetalleDocumento(index).NomProducto
            Me.lblCodigoProducto_capaCampania_Producto.Text = Me.listaDetalleDocumento(index).CodigoProducto

            Me.GV_Campania_Producto.DataSource = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente_SelectxParams_DT(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdProducto, Nothing, CDate(Me.txtFechaEmision.Text))
            Me.GV_Campania_Producto.DataBind()

            Me.hddIndex_GV_Detalle.Value = CStr(index)
            Me.hddIndex_Campania_Producto.Value = CStr(-1)


            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa2('capaCampania_Producto');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnVerCampania_Consulta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerCampania_Consulta.Click
        valOnClick_btnVerCampania_Consulta()
    End Sub
    Private Sub valOnClick_btnVerCampania_Consulta()
        Try

            Dim fechaActual As Date = (New Negocio.FechaActual).SelectFechaActual

            Me.GV_Campania_Cab.DataSource = (New Negocio.Campania).Campania_SelectxParams(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), Nothing, fechaActual, fechaActual, True)
            Me.GV_Campania_Cab.DataBind()

            Me.GV_Campania_Det.DataSource = Nothing
            Me.GV_Campania_Det.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "      onCapa('capaBuscarProducto_AddProd');   onCapa2('capaConsultar_Campania');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Campania_Cab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Campania_Cab.SelectedIndexChanged
        valOnChange_GV_Campania_Cab(Me.GV_Campania_Cab.SelectedIndex)
    End Sub
    Private Sub valOnChange_GV_Campania_Cab(ByVal index As Integer)
        Try

            Dim IdCampania As Integer = CInt(CType(Me.GV_Campania_Cab.Rows(index).FindControl("hddIdCampania"), HiddenField).Value)

            Me.GV_Campania_Det.DataSource = (New Negocio.Campania_Detalle).Campania_Detalle_SelectxIdCampania(IdCampania)
            Me.GV_Campania_Det.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "      onCapa('capaBuscarProducto_AddProd');   onCapa2('capaConsultar_Campania');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Campania_Det_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_Campania_Det.PageIndexChanging

        Me.GV_Campania_Det.PageIndex = e.NewPageIndex
        valOnChange_GV_Campania_Cab(Me.GV_Campania_Cab.SelectedIndex)

    End Sub



    Protected Sub btnEnviarSolicitudAprobacíon_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEnviarSolicitudAprobacíon.Click

        enviarSolicitudAprobacion_Cheque()

    End Sub
    Private Sub enviarSolicitudAprobacion_Cheque()

        Try

            Dim objCheque As Entidades.Cheque = obtenerCheque()
            Dim listaCheque As New List(Of Entidades.Cheque)
            listaCheque.Add(objCheque)

            If ((New Negocio.Cheque).Registrar(listaCheque, New List(Of Entidades.Cheque_DocumentoRef))) Then

                Me.btnEnviarSolicitudAprobacíon.Visible = False
                Me.txtMonto_DatoCancelacion.Text = "0"
                Me.txtNro_DC.Text = ""
                Me.txtFechaACobrar.Text = ""

                objScript.mostrarMsjAlerta(Me, "La solicitud de aprobación del Medio de Pago resultó con éxito. Consulte el estado de aprobación haciendo click en el botón < " + Me.btnConsultarAprobacion.Text + " >")

            Else

                Me.btnEnviarSolicitudAprobacíon.Visible = True
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerCheque() As Entidades.Cheque

        Dim objCheque As New Entidades.Cheque
        With objCheque

            .IdCheque = Nothing
            .IdBanco = CInt(Me.cboBanco.SelectedValue)
            .IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
            .Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
            .Numero = Me.txtNro_DC.Text
            .FechaMov = CDate(Me.txtFechaEmision.Text)
            .IdTipoMovimiento = 1 '****************** INGRESO
            .IdCliente = CInt(Me.hddIdPersona.Value)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            .EstadoMov = _EstadoMov_Cheque_PorAprobar
            .Estado = True
            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            If (IsDate(Me.txtFechaACobrar.Text) And Me.txtFechaACobrar.Text.Trim.Length > 0) Then
                .FechaCobrar = CDate(Me.txtFechaACobrar.Text)
            End If

        End With
        Return objCheque

    End Function

    Protected Sub btnConsultarAprobacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnConsultarAprobacion.Click
        visualizar_ConsultaCheque()
    End Sub
    Private Sub visualizar_ConsultaCheque()

        Try

            Dim IdPersona As Integer = Nothing
            Dim fecha As Date = CDate(Me.txtFechaEmision.Text)
            Dim val_DocumentoRelacionado As Boolean = True

            If (IsNumeric(Me.hddIdPersona.Value) And Me.hddIdPersona.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            If (IdPersona = Nothing) Then
                Throw New Exception("DEBE SELECCIONAR UN CLIENTE. NO SE PERMITE LA OPERACIÓN.")
            End If

            Me.GV_Cheque_AddDC.DataSource = (New Negocio.Cheque).Cheque_ValSelect_AddDatoCancelacion_DT(IdPersona, fecha, val_DocumentoRelacionado)
            Me.GV_Cheque_AddDC.DataBind()

            If (Me.GV_Cheque_AddDC.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            Else
                objScript.onCapa(Me, "capaCheque_AddDatoCancelacion")
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub GV_Cheque_AddDC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Cheque_AddDC.SelectedIndexChanged

        add_Cheque_DC(Me.GV_Cheque_AddDC.SelectedIndex)

    End Sub
    Private Sub add_Cheque_DC(ByVal index As Integer)
        Try

            Dim IdCheque As Integer = CInt(CType(Me.GV_Cheque_AddDC.Rows(index).FindControl("hddIdCheque"), HiddenField).Value)
            Me.listaCancelacion = getListaCancelacion()

            For i As Integer = 0 To Me.listaCancelacion.Count - 1
                If (Me.listaCancelacion(i).IdCheque = IdCheque) Then
                    Throw New Exception("EL CHEQUE SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                End If
            Next

            Dim objPagoCaja As Entidades.PagoCaja = obtenerPagoCaja_AddCheque_DC(IdCheque)

            Me.listaCancelacion.Add(objPagoCaja)

            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerPagoCaja_AddCheque_DC(ByVal IdCheque As Integer) As Entidades.PagoCaja

        Dim objPagoCaja As Entidades.PagoCaja = Nothing

        Dim objCheque As Entidades.Cheque = (New Negocio.Cheque).Cheque_SelectxIdCheque(IdCheque)
        If (objCheque IsNot Nothing) Then

            If (objCheque.EstadoMov <> _EstadoMov_Cheque_Aprobado) Then
                Throw New Exception("EL CHEQUE SELECCIONADO NO HA SIDO APROBADO. NO SE PERMITE LA OPERACIÓN.")
            End If

            objPagoCaja = New Entidades.PagoCaja
            With objPagoCaja

                .IdMedioPago = objCheque.IdMedioPago
                .Factor = 1
                .IdTipoMovimiento = 1 '**************** INGRESO
                .IdMedioPagoInterfaz = objCheque.IdMedioPagoInterfaz
                .EfectivoInicial = objCheque.Monto
                .Efectivo = objCheque.Monto
                .IdMoneda = objCheque.IdMoneda
                .IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                .NomMonedaDestino = Me.cboMoneda.SelectedItem.ToString
                .MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objCheque.Monto, objCheque.IdMoneda, CInt(Me.cboMoneda.SelectedValue), "E", CDate(Me.txtFechaEmision.Text))
                .IdBanco = objCheque.IdBanco

                Dim fecha As String = "---"
                If (objCheque.FechaCobrar <> Nothing) Then
                    .FechaACobrar = objCheque.FechaCobrar
                    fecha = Format(objCheque.FechaCobrar, "dd/MM/yyyy")
                End If

                .descripcionPagoCaja = " Medio Pago: [ " + objCheque.MedioPago + " ] Monto: [ " + objCheque.Moneda + " " + CStr(Math.Round(objCheque.Monto, 2)) + " ] Banco: [ " + objCheque.Banco + " ] Fecha a Cobrar: [ " + fecha + " ]"

                .NumeroCheque = objCheque.Numero
                .IdCheque = objCheque.IdCheque

            End With

        Else

            Throw New Exception("NO SE HALLARON REGISTROS")

        End If

        Return objPagoCaja

    End Function

    Private Function obtenerListaCheque_Save() As List(Of Entidades.Cheque)

        Dim listaCheque As New List(Of Entidades.Cheque)

        Me.listaCancelacion = getListaCancelacion()

        For i As Integer = 0 To Me.listaCancelacion.Count - 1

            If (Me.listaCancelacion(i).IdMedioPagoInterfaz = _MedioPago_Interfaz_Cheque) Then

                Dim objCheque As New Entidades.Cheque
                With objCheque

                    .IdCheque = Me.listaCancelacion(i).IdCheque
                    .IdMedioPago = Me.listaCancelacion(i).IdMedioPago
                    .IdMoneda = Me.listaCancelacion(i).IdMoneda
                    .Monto = Me.listaCancelacion(i).getEfectivo
                    .IdBanco = Me.listaCancelacion(i).IdBanco
                    .Numero = Me.listaCancelacion(i).NumeroCheque
                    .FechaCobrar = Me.listaCancelacion(i).FechaACobrar
                    .EstadoMov = _EstadoMov_Cheque_Aprobado
                    .Estado = True

                    .IdTipoMovimiento = 1
                    .IdCliente = CInt(Me.hddIdPersona.Value)
                    .IdUsuario = CInt(Me.cboUsuarioComision.SelectedValue)
                    .FechaMov = CDate(Me.txtFechaEmision.Text)

                    .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                    .IdTienda = CInt(Me.cboTienda.SelectedValue)

                End With
                listaCheque.Add(objCheque)

            End If

        Next

        Return listaCheque

    End Function

    Private Sub cboValorReferencial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboValorReferencial.SelectedIndexChanged
        Try
            valOnChange_cboValorReferencial()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub valOnChange_cboValorReferencial()

        If (Me.cboValorReferencial.Items.Count > 0) Then

            Dim IdValorReferencial As Integer = CInt(Me.cboValorReferencial.SelectedValue)

            Dim objValorReferencial As Entidades.ValorReferencia_Transporte = (New Negocio.ValorReferencia_Transporte).SelectxIdValorReferenciaT(IdValorReferencial)
            If (objValorReferencial IsNot Nothing) Then

                If (Me.cboMoneda_ValorRef.Items.FindByValue(CStr(objValorReferencial.IdMoneda)) IsNot Nothing) Then
                    Me.cboMoneda_ValorRef.SelectedValue = CStr(objValorReferencial.IdMoneda)
                End If

                Me.txtValorReferencial.Text = Format(objValorReferencial.Monto, "F3")
                Me.txtValorReferencial_Total.Text = Format((objValorReferencial.Monto * CDec(Me.txtCantidad_Transporte.Text)), "F3")

            End If

        End If

    End Sub

    Private Sub btnRetazoLongitud_Aceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRetazoLongitud_Aceptar.Click

        valOnClick_btnRetazoLongitud_Aceptar()

    End Sub
    Private Function obtenerUnidadMedida_Retazo(ByVal lista As List(Of Entidades.UnidadMedida), ByVal IdUnidadMedida_Retazo As Integer) As String

        Dim unidadMedida As String = ""
        For i As Integer = 0 To lista.Count - 1
            If (lista(i).Id = IdUnidadMedida_Retazo) Then
                Return (lista(i).DescripcionCorto)
            End If
        Next
        Return unidadMedida
    End Function
    Private Sub valOnClick_btnRetazoLongitud_Aceptar()
        Try

            ActualizarListaDetalleDocumento()

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)

            Dim cantidad As Decimal = CDec(Me.txtRetazoLongitud_Largo.Text) * CDec(Me.txtRetazoLongitud_Cantidad.Text)
            Dim unidadMedida_Retazo As String = obtenerUnidadMedida_Retazo(Me.listaDetalleDocumento(index).ListaUM_Venta, CInt(Me.hddIdUnidadMedida_RetazoLongitud.Value))

            Me.listaDetalleDocumento(index).Cantidad = (New Negocio.Util).fx_getValorEquivalenteProducto(Me.listaDetalleDocumento(index).IdProducto, CInt(Me.hddIdUnidadMedida_RetazoLongitud.Value), Me.listaDetalleDocumento(index).IdUnidadMedida, cantidad)
            Me.listaDetalleDocumento(index).DetalleGlosa = "(RETAZO) " + CStr(CDec(Me.txtRetazoLongitud_Cantidad.Text)) + " de " + CStr(CDec(Me.txtRetazoLongitud_Largo.Text)) + " " + unidadMedida_Retazo

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnRetazoArea_Aceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRetazoArea_Aceptar.Click

        valOnClick_btnRetazoArea_Aceptar()

    End Sub
    Private Sub valOnClick_btnRetazoArea_Aceptar()
        Try

            ActualizarListaDetalleDocumento()

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)

            Dim cantidad As Decimal = CDec(Me.txtRetazoArea_Ancho.Text) * CDec(Me.txtRetazoArea_Cantidad.Text) * CDec(Me.txtRetazoArea_Largo.Text)
            Dim unidadMedida_Retazo As String = obtenerUnidadMedida_Retazo(Me.listaDetalleDocumento(index).ListaUM_Venta, CInt(Me.hddIdUnidadMedida_RetazoArea.Value))



            Me.listaDetalleDocumento(index).Cantidad = (New Negocio.Util).fx_getValorEquivalenteProducto(Me.listaDetalleDocumento(index).IdProducto, CInt(Me.hddIdUnidadMedida_RetazoArea.Value), Me.listaDetalleDocumento(index).IdUnidadMedida, cantidad)
            Me.listaDetalleDocumento(index).DetalleGlosa = "(RETAZO) " + CStr(CDec(Me.txtRetazoArea_Cantidad.Text)) + " de " + CStr(CDec(Me.txtRetazoArea_Largo.Text)) + " x " + CStr(CDec(Me.txtRetazoArea_Ancho.Text)) + " " + unidadMedida_Retazo

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnAceptarCodBarras_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarCodBarras.Click

        If getListaDetalleDocumento() IsNot Nothing Then
            If BuscaProductoEnLista(Me.txtCodBarrasPrincipal.Text.Trim) = False Then
                BuscarProductoCatalogoCodBarras(0)
            End If
        Else
            BuscarProductoCatalogoCodBarras(0)
        End If

        Me.txtCodBarrasPrincipal.Text = ""
        Me.txtCodBarrasPrincipal.Focus()

    End Sub
    Private Function BuscaProductoEnLista(ByVal codBarraproducto As String) As Boolean
        Dim val As Boolean = False
        ActualizarListaCatalogo()
        ActualizarListaDetalleDocumento()

        Dim listaDet As New List(Of Entidades.DetalleDocumento)
        listaDet = getListaDetalleDocumento()

        For Each prodCodBarra As Entidades.DetalleDocumento In listaDet
            If prodCodBarra.prod_CodigoBarras = codBarraproducto Then
                prodCodBarra.Cantidad = prodCodBarra.Cantidad + 1

                setListaDetalleDocumento(listaDet)

                Me.GV_Detalle.DataSource = getListaDetalleDocumento()
                Me.GV_Detalle.DataBind()

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0); CursorCodBarras();  ", True)
                val = True
            End If
        Next

        Return val

    End Function

    Protected Sub BuscarProductoCatalogoCodBarras(ByVal opcion As Integer)
        Try
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                        If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                            IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                        End If
                    End If

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)
                    ViewState.Add("IdMoneda_BuscarProducto", Me.cboMoneda.SelectedValue)
                    ViewState.Add("IdTipoPV_BuscarProducto", IdTipoPV)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("CodigoBarras", Me.txtCodBarrasPrincipal.Text)

                Case 1
                    '******************* Cuando se cambia el almacen de busqueda
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)

                Case 2
                    '******************** cuando se cambia la tienda de precios
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)

            End Select

            cargarDatosProductoGrillaCodBarras(0, 0, "", "", _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarDatosProductoGrillaCodBarras(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMoneda As Integer, ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal codbarras As String)

        Dim IdMedioPago As Integer = 0
        If (CInt(Me.cboCondicionPago.SelectedValue) = 1) Then  '************ CONTADO
            IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
        End If

        Dim IdCliente As Integer = 0
        If (IsNumeric(Me.hddIdPersona.Value) And (Me.hddIdPersona.Value.Trim.Length) > 0) Then
            IdCliente = CInt(Me.hddIdPersona.Value)
        End If

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        Dim filtroProductoCampania As Boolean = Me.chb_FiltroProductoCampania.Checked
        Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Cotizacion_V2CodBarras(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, 1, 1, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, codbarras, filtroProductoCampania)
        If Me.listaCatalogo.Count = 0 Then
            objScript.mostrarMsjAlerta(Me, "No se Encontro el producto")
        Else
            'guardamos el producto que se busco por codigo de barras
            ListProdCodBarras = listaCatalogo
            addProducto_DetalleDocumentoCodBarras()
        End If
    End Sub

    Private Sub addProducto_DetalleDocumentoCodBarras()
        Try
            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaCatalogo()
            ActualizarListaDetalleDocumento()
            Me.listaCatalogo = ListProdCodBarras
            Me.listaDetalleDocumento = getListaDetalleDocumento()


            For i As Integer = 0 To Me.listaCatalogo.Count - 1
                'If (Me.listaCatalogo(i).Cantidad > 0) Then
                valCant = False
                For k As Integer = 0 To listaDetalleDocumento.Count - 1
                    If listaDetalleDocumento(k).IdProducto = listaCatalogo(i).IdProducto And listaDetalleDocumento(k).IdUnidadMedida = listaCatalogo(i).IdUnidadMedida Then
                        listaDetalleDocumento(k).Cantidad = listaDetalleDocumento(k).Cantidad + 1 'Me.listaCatalogo(i).Cantidad
                        valCant = True
                        Exit For
                    End If
                Next

                If valCant = False Then
                    Dim objDetalle As New Entidades.DetalleDocumento
                    With objDetalle

                        .IdProducto = Me.listaCatalogo(i).IdProducto
                        .NomProducto = Me.listaCatalogo(i).Descripcion
                        .Cantidad = 1 'Me.listaCatalogo(i).Cantidad '1
                        .ListaUM_Venta = Me.listaCatalogo(i).ListaUM_Venta
                        .IdUnidadMedida = Me.listaCatalogo(i).IdUnidadMedida
                        .StockDisponibleN = Me.listaCatalogo(i).StockDisponibleN
                        .PrecioLista = Me.listaCatalogo(i).PrecioLista
                        .PrecioSD = Me.listaCatalogo(i).PrecioSD
                        .Descuento = 0
                        .PorcentDcto = 0
                        .PrecioCD = Me.listaCatalogo(i).PrecioSD
                        .Importe = .Cantidad * .PrecioCD
                        .TasaPercepcion = Me.listaCatalogo(i).Percepcion
                        .PorcentDctoMaximo = Me.listaCatalogo(i).PorcentDctoMaximo
                        .PrecioBaseDcto = Me.listaCatalogo(i).PrecioBaseDcto
                        .IdUsuarioSupervisor = 0
                        .IdTienda = Me.listaCatalogo(i).IdTienda
                        .IdTipoPV = Me.listaCatalogo(i).IdTipoPV
                        .pvComercial = Me.listaCatalogo(i).pvComercial
                        .CodigoProducto = Me.listaCatalogo(i).CodigoProducto
                        .Kit = Me.listaCatalogo(i).Kit
                        .ExisteCampania_Producto = Me.listaCatalogo(i).ExisteCampania_Producto
                        .prod_CodigoBarras = listaCatalogo(i).Prod_CodigoBarras

                    End With
                    Me.listaDetalleDocumento.Add(objDetalle)

                End If
                'End If

            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0); CursorCodBarras();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Public Property ListProdCodBarras() As List(Of Entidades.Catalogo)
        Get
            Return CType(Session.Item("ProdCodBarras"), List(Of Entidades.Catalogo))
        End Get
        Set(ByVal value As List(Of Entidades.Catalogo))
            Session.Remove("ProdCodBarras")
            Session.Add("ProdCodBarras", value)
        End Set
    End Property


    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        'Dim listaDocumento As New List(Of Entidades.Documento)
        'listaDocumento = (New Negocio.DocumentoFacturacion).registrarDocumentoVenta(objDocumento, objAnexoDocumento, Me.listaDetalleConcepto, objPuntoPartida, objPuntoLlegada, objObservaciones, Me.listaDetalleDocumento, objMovCaja, objMovCuenta, Me.listaCancelacion, New List(Of Entidades.PagoCaja), moverAlmacen, comprometerStock, separarAfectos, saltarCorrelativo, separarxMonto, False, Me.chb_CompPercepcion.Checked, listaMovBanco, Me.chb_UseAlias.Checked, objRelacionDocumento, CInt(Me.hddFrmModo.Value), objMontoRegimenRetencion, objMontoRegimenDetraccion, listaCheque, objAnexo_FacturaTransporte)
        'If (listaDocumento.Count > 0) Then
        '    Me.GV_ImpresionDoc_Cab.DataSource = obtenerListaDocumento_Print(listaDocumento)
        '    Me.GV_ImpresionDoc_Cab.DataBind()
        '    verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)
        '    objScript.onCapa(Me, "capaImpresionDocumento")
        'End If

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        CollapsiblePanelExtender1.Collapsed = False
        CollapsiblePanelExtender1.ClientState = "False"
        txtCodigoDocumento.Focus()

    End Sub

    Private Sub buscarVehiculo()
        Try

            Dim lista As List(Of Entidades.Vehiculo) = (New Negocio.Vehiculo).SelectActivoxNroPlaca(Me.txtNroPlaca_BuscarVehiculo.Text)
            If (lista.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaVehiculo');   alert('No se hallaron registros.');     ", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaVehiculo');     ", True)
            End If
            Me.GV_Vehiculo.DataSource = lista
            Me.GV_Vehiculo.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Vehiculo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Vehiculo.SelectedIndexChanged
        Try
            cargarVehiculo(CInt(CType(Me.GV_Vehiculo.SelectedRow.FindControl("hddIdVehiculo"), HiddenField).Value))
            objScript.offCapa(Me, "capaVehiculo")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarVehiculo(ByVal IdVehiculo As Integer)

        Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(IdVehiculo)
        With objVehiculo

            Me.txtModelo_Vehiculo.Text = .Modelo
            Me.txtPlaca_Vehiculo.Text = .Placa
            Me.txtCertificado_Vehiculo.Text = .ConstanciaIns
            Me.hddIdVehiculo.Value = CStr(.IdProducto)

        End With

    End Sub

    Protected Sub btnBuscarVehiculo_Grilla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarVehiculo_Grilla.Click
        buscarVehiculo()
    End Sub

    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalleDocumento(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub


    Private Sub quitarDetalleDocumento(ByVal Index As Integer)

        Try

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim Idcampania As Integer = listaDetalleDocumento(Index).IdCampania
            Dim IdCampaniaDetalle As Integer = listaDetalleDocumento(Index).IdCampaniaDetalle
            Dim IdProducto As Integer = listaDetalleDocumento(Index).IdProducto
            Me.listaDetalleDocumento.RemoveAt(Index)


            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    Me.listaDetalleDocumento.RemoveAll(Function(e As Entidades.DetalleDocumento) e.IdCampania = Idcampania And e.IdProductoAux = IdProducto And e.IdCampaniaDetalle = IdCampaniaDetalle)

                Case FrmModo.Editar

                    Me.listaDetalleDocumento.RemoveAll(Function(e As Entidades.DetalleDocumento) e.IdCampania = Idcampania And e.IdProductoAux = IdProducto)

            End Select



            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function Validar_AperturaCierreCaja(Optional ByVal showMessage As Boolean = True) As Boolean

        objCombo = New Combo
        objCombo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
        If Me.cboCaja.Items.Count > 0 Then lblCaja.Text = (New Negocio.Caja_AperturaCierre).Caja_AperturaCierre_SelectEstado(CInt(Me.cboCaja.SelectedValue), Me.txtFechaEmision.Text.Trim)

        If Me.lblCaja.Text <> "Caja Aperturada" And showMessage Then
            objScript.mostrarMsjAlerta(Me, "La caja no esta apertutada.\nNo se permite la operación.")
            Return False
        End If

        Return True

    End Function

    Private Sub btnAceptar_DescuentoGlobal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_DescuentoGlobal.Click
        OnClick_Aceptar_DescuentoGlobal()
    End Sub
    Private Sub OnClick_Aceptar_DescuentoGlobal()
        Try

            Dim DescuentoGlobal As Decimal = Math.Round(CDec(Me.txtMonto_DescuentoGlobal.Text), 2, MidpointRounding.AwayFromZero)
            Me.txtDescuentoGlobal.Text = CStr(DescuentoGlobal)


            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)

            Dim ImporteTotal As Decimal = CDec(Me.txtImporteTotal.Text)
            Dim TotalConcepto As Decimal = get_TotalConcepto_SinDescuentoGlobal(Me.listaDetalleConcepto)

            Dim TotalDocumento As Decimal = ImporteTotal + TotalConcepto

            '*********      VALIDAR DESCUENTO GLOBAL
            Me.listaDetalleConcepto.RemoveAll(Function(concepto As Entidades.DetalleConcepto) concepto.IdConcepto = 10)


            Dim objDetalleConcepto As New Entidades.DetalleConcepto
            With objDetalleConcepto
                .IdConcepto = 10
                .ListaConcepto = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.cboTipoDocumento.SelectedValue))
                .Concepto = CStr(DescuentoGlobal) + " (%)"
                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .Monto = CDec(Math.Round((TotalDocumento - (TotalDocumento * (1.0 - DescuentoGlobal / 100.0))) * -1, 2))
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            End With

            Me.listaDetalleConcepto.Add(objDetalleConcepto)

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            Me.txtMonto_DescuentoGlobal.Text = "0"

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularTotalConcepto(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Function get_TotalConcepto_SinDescuentoGlobal(ByVal lista As List(Of Entidades.DetalleConcepto)) As Decimal

        Dim TotalConcepto_SinDescuentoGlobal As Decimal = 0
        If Not IsNothing(lista) Then

            For i As Integer = 0 To lista.Count - 1

                If lista(i).IdConcepto <> 10 And lista(i).Monto > 0 Then

                    TotalConcepto_SinDescuentoGlobal = TotalConcepto_SinDescuentoGlobal + lista(i).Monto

                End If

            Next

        End If

        Return TotalConcepto_SinDescuentoGlobal
    End Function

    Private Sub btnAceptar_MaestroObra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_MaestroObra.Click

        registrarDocumentoVenta(False, CInt(Me.rbPregunta.SelectedValue))

    End Sub
    ''AGREGAR CHECK AL DOCUMENTO

    Protected Sub btncheckadd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncheckadd.Click

        Dim Iddocumentox As Integer = 0

        Iddocumentox = CInt(Me.hddIdDocumento.Value)

        Dim obj As Boolean = (New Negocio.Documento).AgregarCheckDocumentoxIdDocumento(Iddocumentox)

        If (obj = True) Then
            objScript.mostrarMsjAlerta(Me, "Modificación Éxitosa.")
        Else
            objScript.mostrarMsjAlerta(Me, "No se pudo agregar el check ya que ya hay stock comprometido, o posee OP-VP.")
        End If


    End Sub

    ''QUITAR CHECK AL DOCUMENTO
    Protected Sub btncheckdel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncheckdel.Click

        Dim Iddocumentox As Integer = 0

        Iddocumentox = CInt(hddIdDocumento.Value)

        Dim obj As Boolean = (New Negocio.Documento).QuitarCheckDocumento(Iddocumentox)

        If (obj = True) Then
            objScript.mostrarMsjAlerta(Me, "Modificación Éxitosa.")
        Else
            objScript.mostrarMsjAlerta(Me, "No se pudo quitar el check ya que no hay stock comprometido o ya se despacho parte de la venta.")
        End If
        ''whitesmoke

    End Sub

    Protected Sub btnchangepl_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnchangepl.Click

        Me.Panel_PuntoLlegada.Enabled = True
        Me.btnchangepl.Visible = False
        Me.btnaparecer.Visible = True
        Me.btncheckadd.Visible = False
        Me.btncheckdel.Visible = False
        Me.btnCambvende.Visible = False
        Me.btnGuardarVende.Visible = False
        Me.btnCambvende.Visible = False
        Me.lblnuevovendedor.Visible = False
        Me.cboNuevoUsuario.Visible = False

    End Sub

    'EliminarPuntoLlegada_DeshacerMov
    Private Function obPuntoLlegada() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing

        objPuntoLlegada = New Entidades.PuntoLlegada
        With objPuntoLlegada
            .IdDocumento = CInt(Me.hddIdDocumento.Value)
            .pll_Direccion = Me.txtDireccion_Llegada.Text.Trim
            .IdAlmacen = Nothing
            .pll_Ubigeo = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue

        End With


        Return objPuntoLlegada

    End Function

    Private Function obPuntoLlegadaCoti() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing

        objPuntoLlegada = New Entidades.PuntoLlegada
        With objPuntoLlegada
            .IdDocumento = CInt(Me.idcoti.Text)
            .pll_Direccion = Me.txtDireccion_Llegada.Text.Trim
            .IdAlmacen = Nothing
            .pll_Ubigeo = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue

        End With


        Return objPuntoLlegada

    End Function

    Protected Sub btnaparecer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnaparecer.Click
        Dim valorcoti As Boolean = True
        Dim Iddocumentox As Integer = 0
        Iddocumentox = CInt(hddIdDocumento.Value)

        Dim selects As Integer = (New Negocio.Documento).SelectIdCotizacion(Iddocumentox)

        Dim delete As Boolean = (New Negocio.Documento).EliminarPuntoLlegada(Iddocumentox)

        idcoti.Text = CStr(selects)
        idcoti.Visible = True
        ''Obtenemos los datos ingresados
        Dim inserts As Entidades.PuntoLlegada = obPuntoLlegada()
        Dim insertsCoti As Entidades.PuntoLlegada = obPuntoLlegadaCoti()

        ''Insertamos el punto de llegada del documento de venta
        Dim valordocu As Boolean = (New Negocio.Documento).InsertarPuntoLlegadax(inserts)
        ''Insertamos el punto de llegada de la cotizacion siempre y cuando haya cotización
        If (insertsCoti.IdDocumento <> 0) Then
            valorcoti = (New Negocio.Documento).InsertarPuntoLlegadax(insertsCoti)
        End If

        idcoti.Visible = False

        If (valordocu = True And valorcoti = True) Then
            objScript.mostrarMsjAlerta(Me, "Modificación Éxitosa.")
        Else
            objScript.mostrarMsjAlerta(Me, "Error")
        End If

        btnchangepl.Visible = True
        btnaparecer.Visible = False
        Panel_PuntoLlegada.Enabled = False
        btnCambvende.Visible = True
        btncheckdel.Visible = True
        btncheckadd.Visible = True
        btnGuardarVende.Visible = False

    End Sub

    Protected Sub btnGuardarVende_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarVende.Click

        Try

            Dim IdDocumentoCotizacion As Integer = 0
            Dim upvendecoti As Boolean = True

            Dim Iddocumentox As Integer = CInt(hddIdDocumento.Value)
            Dim IdDocumentoCotiBusca As Integer = (New Negocio.Documento).SelectIdCotizacion(Iddocumentox)
            'idcoti.Text = CStr(selects.Id)
            'idcoti.Visible = True
            IdDocumentoCotizacion = IdDocumentoCotiBusca

            If (IdDocumentoCotizacion <> 0) Then
                ''Cotizacion
                Dim objDocumento As New Entidades.Documento
                With objDocumento
                    .IdUsuarioComision = CInt(Me.cboNuevoUsuario.SelectedValue)
                    .IdUsuario = CInt(Me.cboNuevoUsuario.SelectedValue)
                    .IdDocumento = IdDocumentoCotizacion
                End With
                upvendecoti = (New Negocio.Documento).UpdateVendedorCotizacion(objDocumento)
            End If

            ''Boleta / Factura
            Dim objDocumentobolfact As New Entidades.Documento
            With objDocumentobolfact
                .IdUsuarioComision = CInt(Me.cboNuevoUsuario.SelectedValue)
                .IdDocumento = CInt(hddIdDocumento.Value)
            End With

            Dim upvendebolfact As Boolean = (New Negocio.Documento).UpdateVendedorBolFact(objDocumentobolfact)


            idcoti.Visible = False

            If (upvendecoti = True And upvendebolfact = True) Then
                objScript.mostrarMsjAlerta(Me, "Modificación Éxitosa.")
            Else
                objScript.mostrarMsjAlerta(Me, "Error")
            End If

            Me.Panel_Cab.Enabled = False
            Me.btnGuardarVende.Visible = False
            Me.btnCambvende.Visible = True
            Me.btncheckdel.Visible = True
            Me.btnchangepl.Visible = True
            Me.btncheckadd.Visible = True
            Me.btnaparecer.Visible = False
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnCambvende_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCambvende.Click

        Me.btnGuardarVende.Visible = True
        Me.btnCambvende.Visible = False
        Me.cboNuevoUsuario.Enabled = True

        ''
        Me.Panel_Cab.Enabled = True
        Me.ckProductoComprometidos.Enabled = False
        Me.cboEmpresa.Enabled = False
        Me.cboTienda.Enabled = False
        Me.cboSerie.Enabled = False
        Me.cboMoneda.Enabled = False
        Me.cboEstado.Enabled = False
        Me.cboCaja.Enabled = False
        Me.cboUsuarioComision.Enabled = False
        Me.txtFechaEmision.Enabled = False
        Me.txtFechaAEntregar.Enabled = False
        Me.cboTipoDocumento.Enabled = False
        Me.cboAlmacenReferencia.Enabled = False
        Me.cboTipoOperacion.Enabled = False
        Me.txtCodigoDocumento.Enabled = False
        Me.btnBuscarDocumentoxCodigo.Enabled = False

        ''

        Me.btnchangepl.Visible = False
        Me.btncheckadd.Visible = False
        Me.btncheckdel.Visible = False
        Me.lblnuevovendedor.Visible = True
        Me.cboNuevoUsuario.Visible = True
    End Sub




    Private Sub btnBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersonaGrilla.Click
        Try


            

            
            If (cboTipoDocumento.SelectedValue = 3) Then
                rdbTipoPersona.SelectedValue = "N"
            End If
          
            If (cboTipoDocumento.SelectedValue = 1101353002) Then
                rdbTipoPersona.SelectedValue = "N"
            End If

            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO


            If (CInt(Me.hddBusquedaMaestro.Value.Trim) = 1) Then

                ViewState.Add("rol", 3)  '******************** MAESTRO DE OBRA
                If cmbRol.Items.FindByValue("3") IsNot Nothing Then
                    cmbRol.SelectedValue = "3"
                End If

            Else

                ViewState.Add("rol", CInt(cmbRol.SelectedValue))  '******************** TODOS

            End If

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarGrilla_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGrilla_AddProducto.Click
        BuscarProductoCatalogo(0)
        Me.txtCodBarrasCapa.Text = ""
    End Sub

    Private Sub btnAddProductos_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProductos_AddProducto.Click
        addProducto_DetalleDocumento()
        Me.txtCodBarrasCapa.Text = ""
        cursorCodigoBarras()
    End Sub

    Private Sub exportar()
        Dim items As List(Of String) = New List(Of String)
        Dim sb As New StringBuilder()
        Using file As New StreamWriter("D:\" + Me.txtCodigoDocumento.Text + ".txt", True)
            Dim lista As New List(Of Entidades.be_facturacionElectronica)
            lista = (New Negocio.bl_estructuraFacturacion).estructuraFacturacion(cboTipoDocumento.SelectedValue)
            For i As Integer = 0 To lista.Count - 1
                sb.Append(lista(i).codigo)
                sb.Append(";")
                sb.Append(lista(i).campo)
                sb.Append(";")
                sb.Append("3")
                sb.Append(New LiteralControl("<br/>"))
            Next
            'Dim lineas As String = String.Join
            'Dim linea As String = String.Join(";", items.ToArray())
            file.WriteLine(sb.ToString())
            file.Close()
        End Using
    End Sub

    Private Sub btnImprimirFacElectronica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimirFacElectronica.Click
        ' Dim str As String = "<script>window.open('../../caja/reportes/frmImprimirFacElectrónica.aspx?idSerie=" & Me.cboSerie.SelectedValue & "&codigo=" & Me.txtCodigoDocumento.Text.Trim() & "&idtipoDocumento=" & Me.cboTipoDocumento.SelectedValue & "','popup','width=800,height=500') </script>"
        'Response.Write("<script>window.open('../../caja/reportes/frmImprimirFacElectrónica.aspx?idSerie='" & Me.cboSerie.SelectedValue & "'&codigo='" & Me.txtCodigoDocumento.Text.Trim() & "'&idtipoDocumento=" & Me.cboTipoDocumento.SelectedValue & "','popup','width=800,height=500') </script>")

        'Process.Start(path)
        'Response.Clear()
        'Response.ContentType = "application/pdf"
        '' la variable nombre es el nombre del archivo .pdf
        'Response.AddHeader("Content-disposition", "attachment; filename=" & "nombre")
        '' aqui va la ruta del archivo
        'Response.WriteFile(path)
        'Response.Flush()
        'Response.Close()
        'AgregarPrintScript(path, "D:\copias")
    End Sub

    Public Shared Sub AgregarPrintScript(ByVal Original As String, ByVal Copia As String)
        Dim reader As New PdfReader(Original)
        Dim stamper As New PdfStamper(reader, New FileStream(Copia, FileMode.Create))
        Dim fields As AcroFields = stamper.AcroFields
        stamper.JavaScript = "this.print(true);" & vbCr
        stamper.FormFlattening = True
        stamper.Close()
        reader.Close()
    End Sub

    Protected Sub btnGEnerarFacElectronica_Click(sender As Object, e As EventArgs) Handles btnGEnerarFacElectronica.Click
        Dim bl As New Negocio.bl_estructuraFacturacion
        Try
            bl.fun_GENERAR_PRINT_FAC_ELECTRONICA(cboSerie.SelectedValue, Trim(Me.txtCodigoDocumento.Text))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnNuevoCliente_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnNuevoCliente.Click

    End Sub

    Protected Sub btnBuscarCliente_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarCliente.Click

    End Sub
End Class
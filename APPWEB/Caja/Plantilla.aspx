﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="Plantilla.aspx.vb" Inherits="APPWEB.Plantilla" Title="Página sin título" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnOpen_DescuentoGlobal"
                    PopupControlID="Panel_DescuentoGlobal" BackgroundCssClass="modalBackground" Enabled="true"
                    RepositionMode="None" CancelControlID="btnClose_DescuentoGlobal">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: block;">
                    <table>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTitulo" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 25px">
                                            <asp:Image ID="btnClose_DescuentoGlobal" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            <asp:Label ID="lblPregunta" runat="server" Text=""></asp:Label>
                                        </td>                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="Button1" runat="server" Text="Aceptar" Width="80px" OnClientClick="return(  valOnClick_btnAceptar_DescuentoGlobal() );" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>

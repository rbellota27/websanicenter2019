﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmNotaCreditoAplicacion
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Private Const _Opcion_AppEfectivoCaja_NC As Integer = 1
    Private Const _Opcion_AppEfectivoCaja_ReciboEgreso As Integer = 2

    Private Sub ValidarPermisos()
        '**** REGISTRAR / Deshacer / Actualizar / Modificar monto Efectivo de Caja
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {79, 80, 81, 123})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnGuardar.Enabled = True
        Else
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* Deshacer Aplicación de Nota de Crédito
            Me.btnDeshacerAplicacion.Enabled = True
        Else
            Me.btnDeshacerAplicacion.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* Actualizar Saldo a Favor de Nota de Crédito
            Me.btnActualizarSaldoFavor.Enabled = True
        Else
            Me.btnActualizarSaldoFavor.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* Actualizar Saldo a Favor de Nota de Crédito
            Me.txtEfectivoCaja.Enabled = True
        Else
            Me.txtEfectivoCaja.Enabled = False
        End If

    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboTiendaxIdUsuario(Me.cboTienda_DocRef, CInt(Session("IdUsuario")), True)
                .LlenarCboMedioPago(Me.cboMedioPago, False)

                Dim IdMedioPagoPrincipal As Integer = (New Negocio.MedioPago).SelectIdMedioPagoPrincipal()

                If (Me.cboMedioPago.Items.FindByValue(CStr(IdMedioPagoPrincipal)) IsNot Nothing) Then
                    Me.cboMedioPago.SelectedValue = CStr(IdMedioPagoPrincipal)
                End If

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            End With

            '***************** Inicializamos la Interface
            Me.btnDeshacerAplicacion.Visible = False
            Me.btnActualizarSaldoFavor.Visible = False

            ValidarPermisos()

        End If
    End Sub

#Region "**************** BUSQUEDA DE PERSONA"


    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

   

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region




    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
            
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub mostrarDocumentosNotaCredito()
        Try

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoSelectCabFind_Aplicacion(CInt(Me.hddIdPersona.Value), CInt(Me.cboTienda_DocRef.SelectedValue), CInt(Me.cboFiltro_NotaCredito.SelectedValue))

            If (lista.Count > 0) Then

                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
                objScript.onCapa(Me, "capaDocumentosReferencia")

            Else

                Throw New Exception("No se hallaron registros.")

            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_DocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda_DocRef.SelectedIndexChanged
        mostrarDocumentosNotaCredito()
    End Sub

    Private Sub GV_DocumentosReferencia_Find_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_DocumentosReferencia_Find.PageIndexChanging
        Me.GV_DocumentosReferencia_Find.PageIndex = e.NewPageIndex
        mostrarDocumentosNotaCredito()
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged
        addNotaCreditoApp()
    End Sub

    Private Sub addNotaCreditoApp()
        Try

            Dim objDocumentoView As New Entidades.DocumentoView

            With objDocumentoView

                .NomTipoDocumento = HttpUtility.HtmlDecode(Me.GV_DocumentosReferencia_Find.SelectedRow.Cells(1).Text)
                .Numero = HttpUtility.HtmlDecode(CType(GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text)
                .IdDocumento = CInt(CType(GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value)
                .IdMoneda = CInt(CType(GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value)
                .FechaEmision = CStr(GV_DocumentosReferencia_Find.SelectedRow.Cells(3).Text)
                .FechaVenc = CDate(GV_DocumentosReferencia_Find.SelectedRow.Cells(4).Text)
                .Contador = CInt(GV_DocumentosReferencia_Find.SelectedRow.Cells(5).Text)

                .NomMoneda = HttpUtility.HtmlDecode(CType(GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblMonedaMonto_Find"), Label).Text)
                .TotalAPagar = CDec(CType(GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblMonto_Find"), Label).Text)
                .ImporteTotal = CDec(CType(GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblSaldo_Find"), Label).Text)

                .Contador = CInt(GV_DocumentosReferencia_Find.SelectedRow.Cells(5).Text)
                .NomPropietario = HttpUtility.HtmlDecode(CStr(GV_DocumentosReferencia_Find.SelectedRow.Cells(8).Text))
                .NomTienda = HttpUtility.HtmlDecode(CStr(GV_DocumentosReferencia_Find.SelectedRow.Cells(9).Text))

                '.NomEstadoCancelacion = HttpUtility.HtmlDecode(CStr(GV_DocumentosReferencia_Find.SelectedRow.Cells(10).Text))
                '.NomEstadoDocumento = HttpUtility.HtmlDecode(CStr(GV_DocumentosReferencia_Find.SelectedRow.Cells(11).Text))

                .IdEstadoCancelacion = CInt(CType(GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdEstadoCancelacion_Find"), HiddenField).Value)

            End With

            Dim lista As New List(Of Entidades.DocumentoView)
            lista.Add(objDocumentoView)

            Me.GV_NotaCreditoApp.DataSource = lista
            Me.GV_NotaCreditoApp.DataBind()

            Me.txtSaldoAFavorCliente_NotaCredito.Text = CStr(objDocumentoView.ImporteTotal)
            Me.lblMonedaSaldoAFavorCliente_NotaCredito.Text = objDocumentoView.NomMoneda
            Me.lblMonedaTotalAbono.Text = objDocumentoView.NomMoneda
            Me.lblMoneda_EfectivoCaja.Text = objDocumentoView.NomMoneda
            Me.lblMonedaSaldoRestante.Text = objDocumentoView.NomMoneda
            Me.hddIdMoneda.Value = CStr(objDocumentoView.IdMoneda)
            Me.hddIdDocumentoNotaCredito.Value = CStr(objDocumentoView.IdDocumento)

            '********************** ACTUALIZAMOS A LA OPCÓN POR DEFECTO
            actualizarTipoAplicacion(0)

            If (objDocumentoView.TotalAPagar > objDocumentoView.ImporteTotal) Then
                Me.btnActualizarSaldoFavor.Visible = True
                Me.btnDeshacerAplicacion.Visible = True
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularAbonoTotal();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarDocRef_NotaCredito_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocRef_NotaCredito.Click
        mostrarDocumentosNotaCredito()
    End Sub

    Private Sub actualizarTipoAplicacion(ByVal Opcion As Integer)
        Try

            '********* Limpiamos la grilla Deudas
            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()
            Me.txtTotalAbono.Text = "0"
            Me.txtEfectivoCaja.Text = "0"

            Me.Panel_Deuda.Visible = False
            Me.Panel_EfectivoCaja.Visible = False

            Select Case Opcion

                Case 0 '************ Seleccione una Opción

                Case 1 '************ Por Pago de Deudas
                    Dim lista As List(Of Entidades.Documento_MovCuenta) = (New Negocio.MovCuenta).SelectDeudaxIdPersona(CInt(Me.hddIdPersona.Value), CInt(Me.hddIdMoneda.Value))
                    If (lista.Count <= 0) Then
                        Throw New Exception("La Persona seleccionada no posee deudas pendientes.")
                        Return
                    Else

                        Me.Panel_Deuda.Visible = True
                        Me.GV_Deudas.DataSource = lista
                        Me.GV_Deudas.DataBind()

                    End If

                Case 2 '************ Efectivo contra caja
                    Me.Panel_EfectivoCaja.Visible = True

                    '***************** PARAMETRO GENERAL
                    Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {4})

                    Dim saldoFavor As Decimal = CDec(Me.txtSaldoAFavorCliente_NotaCredito.Text)
                    Me.txtEfectivoCaja.Text = CStr(Math.Round(saldoFavor * (listaParametros(0) / 100), 2))

            End Select

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularAbonoTotal();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnVerHistorial_Click(ByVal sender As Object, ByVal e As EventArgs)
        verHistorialAbonos(sender)
    End Sub
    Private Sub verHistorialAbonos(ByVal sender As Object)
        Try


            Dim gvrow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Dim IdMovCuenta As Integer = CInt(CType(gvrow.FindControl("hddIdMovCuenta"), HiddenField).Value)


            If (IdMovCuenta = Nothing) Then
                Throw New Exception("No se halló el Registro. Error del Sistema.")
            Else

                '************ MOSTRAMOS EL HISTORIAL DE ABONOS
                Dim listaAbonos As List(Of Entidades.Documento_MovCuenta) = (New Negocio.MovCuenta).SelectAbonosxIdMovCuenta(IdMovCuenta)
                If (listaAbonos.Count <= 0) Then
                    Throw New Exception("No se hallaron abonos para el documento seleccionado.")
                Else
                    Me.GV_Abonos.DataSource = listaAbonos
                    Me.GV_Abonos.DataBind()
                    objScript.onCapa(Me, "capaAbonos")
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTipoAplicacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAplicacion.SelectedIndexChanged
        actualizarTipoAplicacion_NC()
    End Sub
    Private Sub actualizarTipoAplicacion_NC()

        Try

            actualizarTipoAplicacion(CInt(Me.cboTipoAplicacion.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrarAplicacion_NotaCredito()
    End Sub
    Private Sub registrarAplicacion_NotaCredito()

        Try

            Dim IdDocumentoNotaCredito As Integer = CInt(Me.hddIdDocumentoNotaCredito.Value)

            Select Case CInt(Me.cboTipoAplicacion.SelectedValue)

                Case 0

                    If ((New Negocio.DocumentoNotaCredito).DocumentoNotaCredito_AplicacionDeudas(IdDocumentoNotaCredito, CInt(Me.rdbSaldoRestante.SelectedValue), New List(Of Entidades.MovCuenta))) Then

                        limpiarFrm()
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " registrarAplicacion_NotaCredito('" + Me.rdbSaldoRestante.SelectedValue + "','" + CStr(IdDocumentoNotaCredito) + "'); ", True)

                    Else
                        Throw New Exception("Problemas en la Operación.")
                    End If

                Case 1  '************** APLICACION DEUDAS

                    Dim listaAbono As List(Of Entidades.MovCuenta) = obtenerListaAbono()
                    If ((New Negocio.DocumentoNotaCredito).DocumentoNotaCredito_AplicacionDeudas(IdDocumentoNotaCredito, CInt(Me.rdbSaldoRestante.SelectedValue), listaAbono)) Then

                        limpiarFrm()
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " registrarAplicacion_NotaCredito('" + Me.rdbSaldoRestante.SelectedValue + "','" + CStr(IdDocumentoNotaCredito) + "'); ", True)

                    Else
                        Throw New Exception("Problemas en la Operación.")
                    End If

                Case 2  '************** APLICACION EFECTIVO CAJA

                    If ((New Negocio.Util).ValidarExistenciaxTablax1Campo("MovCaja", "IdDocumento", Me.hddIdDocumentoNotaCredito.Value) > 0) Then

                        Throw New Exception("EL DOCUMENTO NOTA DE CRÉDITO SELECCIONADO YA POSEE UNA APLICACIÓN CONTRA EFECTIVO DE CAJA. NO SE PERMITE LA OPERACIÓN.")

                    End If

                    Dim objMovCaja As Entidades.MovCaja = obtenerMovCaja()
                    Dim objPagoCaja As Entidades.PagoCaja = obtenerPagoCaja()

                    If ((New Negocio.DocumentoNotaCredito).DocumentoNotaCredito_AplicacionContraEfectivoCaja(objMovCaja, objPagoCaja, CInt(Me.rdbSaldoRestante.SelectedValue))) Then

                        Dim IdDocumentoNC As Integer = CInt(Me.hddIdDocumentoNotaCredito.Value)
                        Dim montoNC As Decimal = CDec(Me.txtEfectivoCaja.Text)
                        Dim IdMonedaNC As Integer = CInt(CType(Me.GV_NotaCreditoApp.Rows(0).FindControl("hddIdMonedaDocRef"), HiddenField).Value)

                        limpiarFrm()

                        Dim opcionAplicacionContraCaja As Integer = CInt((New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {14})(0))

                        Select Case opcionAplicacionContraCaja
                            Case _Opcion_AppEfectivoCaja_NC
                                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " registrarAplicacion_NotaCredito('" + Me.rdbSaldoRestante.SelectedValue + "','" + CStr(IdDocumentoNotaCredito) + "'); ", True)
                            Case _Opcion_AppEfectivoCaja_ReciboEgreso
                                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " registrarAplicacion_NotaCredito('" + Me.rdbSaldoRestante.SelectedValue + "','" + CStr(IdDocumentoNotaCredito) + "');    window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=1&IdDocumentoRef=" + CStr(IdDocumentoNC) + "&IdMonedaNC=" + CStr(IdMonedaNC) + "&montoNC=" + CStr(montoNC) + "', null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);              ", True)
                        End Select
                    Else
                        Throw New Exception("Problemas en la Operación.")
                    End If

            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerPagoCaja() As Entidades.PagoCaja

        Dim objPagoCaja As Entidades.PagoCaja = Nothing

        If (CDec(Me.txtEfectivoCaja.Text) > 0) Then

            objPagoCaja = New Entidades.PagoCaja

            With objPagoCaja

                If (Me.GV_NotaCreditoApp.Rows.Count <> 1) Then  '********** SOLO PODEMOS APLICAR UN DOCUMENTO

                    Throw New Exception("SE HA INGRESADO MÁS DE UN DOCUMENTO NOTA DE CRÉDITO PARA SU APLICACIÓN. NO SE PERMITE LA OPERACIÓN.")

                Else

                    .IdMoneda = CInt(CType(Me.GV_NotaCreditoApp.Rows(0).FindControl("hddIdMonedaDocRef"), HiddenField).Value)

                End If

                .IdDocumento = CInt(Me.hddIdDocumentoNotaCredito.Value)
                .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                .IdTipoMovimiento = 2 '***************** EGRESO
                .Factor = -1
                .Efectivo = CDec(Me.txtEfectivoCaja.Text)
                .Monto = CDec(Me.txtEfectivoCaja.Text)

            End With


        End If

        Return objPagoCaja
    End Function
    Private Function obtenerMovCaja() As Entidades.MovCaja

        Dim objMovCaja As Entidades.MovCaja = Nothing
        If (CDec(Me.txtEfectivoCaja.Text) > 0) Then

            objMovCaja = New Entidades.MovCaja

            With objMovCaja

                .IdDocumento = CInt(Me.hddIdDocumentoNotaCredito.Value)
                .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                .IdTienda = CInt(Me.cboTienda.SelectedValue)
                .IdCaja = CInt(Me.cboCaja.SelectedValue)
                .IdPersona = CInt(Session("IdUsuario"))
                .IdTipoMovimiento = 2 '*************** EGRESO

            End With



        End If
        Return objMovCaja
    End Function

    Private Sub limpiarFrm()

        Me.GV_NotaCreditoApp.DataSource = Nothing
        Me.GV_NotaCreditoApp.DataBind()

        Me.GV_Deudas.DataSource = Nothing
        Me.GV_Deudas.DataBind()

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        Me.txtSaldoAFavorCliente_NotaCredito.Text = "0"
        Me.txtEfectivoCaja.Text = "0"

        Me.hddIdDocumentoNotaCredito.Value = ""
        Me.hddIdMoneda.Value = ""

        Me.cboTipoAplicacion.SelectedValue = "0"
        Me.Panel_EfectivoCaja.Visible = False
        Me.Panel_Deuda.Visible = False


        Me.rdbSaldoRestante.SelectedValue = "0"

        Me.btnActualizarSaldoFavor.Visible = False
        Me.btnDeshacerAplicacion.Visible = False

    End Sub

    Private Function obtenerListaAbono() As List(Of Entidades.MovCuenta)

        Dim lista As New List(Of Entidades.MovCuenta)

        For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1

            If ((CDec(CType(Me.GV_Deudas.Rows(i).FindControl("txtAbono"), TextBox).Text)) > 0) Then

                Dim objMovCuenta As New Entidades.MovCuenta
                With objMovCuenta

                    .IdDocumento = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdDocumentoDeuda"), HiddenField).Value)
                    .IdMovCuenta = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdMovCuenta"), HiddenField).Value)
                    .Monto = CDec(CType(Me.GV_Deudas.Rows(i).FindControl("txtAbono"), TextBox).Text)


                End With

                lista.Add(objMovCuenta)

            End If

        Next

        Return lista

    End Function

    Private Sub deshacerAplicacionNC()
        Try
            If ((New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoAplicacion_DeshacerAplicacion(CInt(Me.hddIdDocumentoNotaCredito.Value), Me.chb_AppDeudas.Checked, Me.chb_AppEfectivoCaja.Checked)) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")

                Me.GV_DocumentosReferencia_Find.DataSource = Nothing
                Me.GV_DocumentosReferencia_Find.DataBind()

                Me.GV_NotaCreditoApp.DataSource = Nothing
                Me.GV_NotaCreditoApp.DataBind()

                Me.GV_Deudas.DataSource = Nothing
                Me.GV_Deudas.DataBind()

                Me.txtSaldoAFavorCliente_NotaCredito.Text = "0"
                Me.txtSaldoRestante.Text = "0"
                Me.txtTotalAbono.Text = "0"
                Me.txtEfectivoCaja.Text = "0"

                Me.hddIdDocumentoNotaCredito.Value = ""
                Me.hddIdMoneda.Value = ""

                Me.cboTipoAplicacion.SelectedValue = "0"
                Me.rdbSaldoRestante.SelectedValue = "0"

                Me.btnActualizarSaldoFavor.Visible = False
                Me.btnDeshacerAplicacion.Visible = False

                Panel_EfectivoCaja.Visible = False
                Panel_Deuda.Visible = False

            Else
                Throw New Exception("Problemas en la Operación.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnActualizarSaldoFavor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActualizarSaldoFavor.Click
        mostrarCapaUpdateSaldo()
    End Sub
    Private Sub mostrarCapaUpdateSaldo()

        Try

            Dim objDocumento As Entidades.DocumentoNotaCredito = (New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoSelectCab(0, 0, CInt(Me.hddIdDocumentoNotaCredito.Value))

            With objDocumento

                Me.lblMoneda_UpdateSaldo.Text = .NomMoneda
                Me.lblMonedaNuevoSaldo_UpdateSaldo.Text = .NomMoneda
                Me.lblMonedaSaldoRestante_UpdateSaldo.Text = .NomMoneda

                Me.lblNroDocumento_UpdateSaldo.Text = "Nro:  " + .Serie + " - " + .Codigo
                Me.lblSaldoOriginal_UpdateSaldo.Text = CStr(Math.Round(.TotalAPagar, 2))
                Me.lblSaldoRestante_UpdateSaldo.Text = CStr(Math.Round(.ImporteTotal, 2))
                Me.txtNuevoSaldo_UpdateSaldo.Text = "0"

            End With

            objScript.onCapa(Me, "capaUpdateSaldo")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnGuardar_UpdateSaldo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar_UpdateSaldo.Click

        UpdateSaldo()

    End Sub
    Private Sub UpdateSaldo()

        Try

            If ((New Negocio.DocumentoNotaCredito).DocumentoNotaCredito_UpdateSaldoFavorCliente(CInt(Me.hddIdDocumentoNotaCredito.Value), CDec(Me.txtNuevoSaldo_UpdateSaldo.Text))) Then

                Me.GV_DocumentosReferencia_Find.DataSource = Nothing
                Me.GV_DocumentosReferencia_Find.DataBind()

                Me.GV_NotaCreditoApp.DataSource = Nothing
                Me.GV_NotaCreditoApp.DataBind()

                Me.GV_Deudas.DataSource = Nothing
                Me.GV_Deudas.DataBind()

                Me.txtSaldoAFavorCliente_NotaCredito.Text = "0"
                Me.txtSaldoRestante.Text = "0"
                Me.txtTotalAbono.Text = "0"

                Me.hddIdDocumentoNotaCredito.Value = ""
                Me.hddIdMoneda.Value = ""

                Me.cboTipoAplicacion.SelectedValue = "0"
                Me.rdbSaldoRestante.SelectedValue = "0"

                Me.btnActualizarSaldoFavor.Visible = False
                Me.btnDeshacerAplicacion.Visible = False

                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else
                Throw New Exception("Problemas en la Operación.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged
        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            objScript.offCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.hddIdPersona.Value = CStr(objPersonaView.IdPersona)

        End If

    End Sub

    Protected Sub btnGuardar_DeshaceMov_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar_DeshaceMov.Click
        deshacerAplicacionNC()
    End Sub

    Private Sub btBuscarPersonaGrillas_Click(sender As Object, e As System.EventArgs) Handles btBuscarPersonaGrillas.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 1)  '******************** CLIENTE

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
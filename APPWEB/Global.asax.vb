﻿Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        'aquí se inicializan variables que por lo genral son variables de usuario
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
        Session.Timeout = 36000
        Session.Clear()
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
        Dim cookiename = FormsAuthentication.FormsCookieName
        Dim authCookie As HttpCookie
        authCookie = Context.Request.Cookies(cookiename)        
        If (authCookie Is Nothing) Then
            ' There is no authentication cookie.
            Return
        End If
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs

        'Server.Transfer("Exceptions/sesion.htm")

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
        Session.Clear()
        Session.Abandon()
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        Session.Abandon()
        ' Fires when the application end
    End Sub

End Class
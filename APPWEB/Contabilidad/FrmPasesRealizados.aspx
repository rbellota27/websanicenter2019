﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmPasesRealizados.aspx.vb" Inherits="APPWEB.FrmPasesRealizados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <table class="style1" width="100%">


           <tr>
            <td class="TituloCelda">
              PASES REALIZADOS
            </td>
           
        </tr>
        <tr>
            <td>    
                      <asp:GridView ID="GV_Pases" runat="server" AutoGenerateColumns="False" 
                                        Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                                        BorderWidth="1px" CellPadding="3"  >
                                       
                                <Columns>
                                  <asp:ButtonField Text = "Seleccionar" CommandName = "Select" ItemStyle-ForeColor="Blue" />
                                   <asp:TemplateField HeaderText="Código Pase" >
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblCodigoPase"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"CodigoPase")%>'></asp:Label>
                                           </td> 
                                           <td>
                                           <asp:HiddenField ID="hddIdPase" runat="server"   Value ='<%#DataBinder.Eval(Container.DataItem,"IdPase")%>' />
                                            </td>
                                             </tr>
                                             </table>                                        
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha Inicio Pase" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblFechaInicio"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"FechaInicio","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                                                       
                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                   
                                                                       
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Fecha Fin Pase" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblFechaFin"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"FechaFin","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>                      
                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />                  
                                   </asp:TemplateField>
                                   
                                   <asp:TemplateField HeaderText="Fecha Registro Pase" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblFechaRegistro"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"FechaRegistro","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>                   
                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />                
                                   </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Usuario Registro" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                      <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblUsuario"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label> </td>
                                         
                                                </tr>
                                                </table>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                     </Columns>
                                <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
                                <PagerStyle CssClass="GrillaPager"
                                        HorizontalAlign="Left" BackColor="White" ForeColor="#000066" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
                                        ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
                                        ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView>
                                   
                                   </td>
                </tr>
 </table>
 <table width="100%">
 <tr>
 
           <td class="SubTituloCelda">
             DETALLE DEL PASE
            </td>
            </tr>
            <tr>
            <td>
                <asp:Panel ID="PanelDocExterno" runat="server" Height="250px" ScrollBars="Vertical">
                        <asp:GridView ID="GV_DocExterno" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Eliminar" ShowSelectButton="True" />
                                                
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblDocumento" runat="server" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                            
                                             <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Nro.Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblNroDocumento" runat="server" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                       
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMonedaDocexterno" runat="server" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMontoDocExt" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            

                                             <asp:BoundField DataField="FechaEmision" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center"  DataFormatString="{0:dd/MM/yyyy}" />                                                    
            
                                           <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Beneficiario" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblBeneficiarioDocExt" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Doc. Referenciado" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblDocreferenciado" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"CadenaDocumentoRelacionado")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                             </asp:Panel>
            </td>
        
 </tr>
 
 </table>
 <asp:HiddenField Id="hddIdPase" runat="server" Value="0"/>
 <asp:HiddenField Id="hddIdDocumento" runat="server" Value="0"/>
</asp:Content>

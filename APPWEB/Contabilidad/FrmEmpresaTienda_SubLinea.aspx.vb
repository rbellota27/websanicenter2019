﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmEmpresaTienda_SubLinea
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad_Frm()
    End Sub

    Private Sub valOnLoad_Frm()
        Try

            If (Not Me.IsPostBack) Then

                inicializarFrm()

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()

        Dim objCbo As New Combo
        With objCbo

            .LlenarCboPropietario(Me.cboEmpresa, False)
            .llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), False)

            .LlenarCboLinea(Me.cboLinea, False)
            .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), True)

        End With

        verFrm(FrmModo.Inicio, True, True)

    End Sub

    Private Sub limpiarFrm()

        '************** COMBOS
        Me.txtCuentaCostoCompra.Text = ""
        Me.txtCuentaCostoVenta.Text = ""
        Me.txtCuentaDebeCompra.Text = ""
        Me.txtCuentaDebeVenta.Text = ""
        Me.txtCuentaHaberCompra.Text = ""
        Me.txtCuentaHaberVenta.Text = ""

    End Sub

    Private Sub actualizarControles()

        Me.btnNuevo.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnBuscar.Visible = False
        Me.Panel_Grilla.Enabled = False
        Me.Panel_NroCuenta.Visible = False

        Me.cboEmpresa.Enabled = True
        Me.cboTienda.Enabled = True
        Me.cboLinea.Enabled = True
        Me.cboSubLinea.Enabled = True

        Select Case CInt(Me.hddFrmModo.Value)
            Case FrmModo.Inicio

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.Panel_Grilla.Enabled = True

            Case FrmModo.Nuevo

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.Panel_NroCuenta.Visible = True

            Case FrmModo.Editar

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.Panel_NroCuenta.Visible = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboLinea.Enabled = False
                Me.cboSubLinea.Enabled = False

        End Select

    End Sub

    Private Sub verFrm(ByVal modo As Integer, ByVal limpiar As Boolean, ByVal cargarGrilla As Boolean)

        If (limpiar) Then
            limpiarFrm()
        End If

        If (cargarGrilla) Then
            Me.GV_EmpresaTienda_SubLinea.DataSource = (New Negocio.EmpresaTienda_SubLinea).SelectxParams_DT(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue))
            Me.GV_EmpresaTienda_SubLinea.DataBind()
        End If

        Me.hddFrmModo.Value = CStr(modo)
        actualizarControles()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
    End Sub


    Private Sub valOnClick_btnCancelar()
        Try

            verFrm(FrmModo.Inicio, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrar()
    End Sub
    Private Sub registrar()

        Try

            Dim lista As List(Of Entidades.EmpresaTienda_SubLinea) = obtenerListaEmpresaTienda_SubLinea()

            If ((New Negocio.EmpresaTienda_SubLinea).registrarEmpresaTienda_SubLinea(lista)) Then

                verFrm(FrmModo.Inicio, True, True)
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else

                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaEmpresaTienda_SubLinea() As List(Of Entidades.EmpresaTienda_SubLinea)

        Dim lista As New List(Of Entidades.EmpresaTienda_SubLinea)

        If (CInt(Me.cboSubLinea.SelectedValue) = 0) Then

            '******************* TODAS LA SUBLINEAS
            Dim listaSubLinea As List(Of Entidades.SubLinea) = (New Negocio.SubLinea).SelectCboxIdLinea(CInt(Me.cboLinea.SelectedValue))

            For i As Integer = 0 To listaSubLinea.Count - 1

                Dim objETS As New Entidades.EmpresaTienda_SubLinea
                With objETS

                    .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                    .IdTienda = CInt(Me.cboTienda.SelectedValue)
                    .IdSubLinea = listaSubLinea(i).Id

                    .CtaCostoCompra = Me.txtCuentaCostoCompra.Text
                    .CtaCostoVenta = Me.txtCuentaCostoVenta.Text
                    .CtaDebeCompra = Me.txtCuentaDebeCompra.Text
                    .CtaDebeVenta = Me.txtCuentaDebeVenta.Text
                    .CtaHaberCompra = Me.txtCuentaHaberCompra.Text
                    .CtaHaberVenta = Me.txtCuentaHaberVenta.Text

                End With
                lista.Add(objETS)

            Next


        Else

            '************** UNA SUB LINEA
            Dim objETS As New Entidades.EmpresaTienda_SubLinea
            With objETS

                .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                .IdTienda = CInt(Me.cboTienda.SelectedValue)
                .IdSubLinea = CInt(Me.cboSubLinea.SelectedValue)

                .CtaCostoCompra = Me.txtCuentaCostoCompra.Text
                .CtaCostoVenta = Me.txtCuentaCostoVenta.Text
                .CtaDebeCompra = Me.txtCuentaDebeCompra.Text
                .CtaDebeVenta = Me.txtCuentaDebeVenta.Text
                .CtaHaberCompra = Me.txtCuentaHaberCompra.Text
                .CtaHaberVenta = Me.txtCuentaHaberVenta.Text

            End With
            lista.Add(objETS)

        End If

        Return lista

    End Function

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub
    Private Sub valOnClick_btnNuevo()
        Try

            verFrm(FrmModo.Nuevo, True, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        valOnClick_btnBuscar()
    End Sub
    Private Sub valOnClick_btnBuscar()
        Try

            Me.GV_EmpresaTienda_SubLinea.DataSource = (New Negocio.EmpresaTienda_SubLinea).SelectxParams_DT(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue))
            Me.GV_EmpresaTienda_SubLinea.DataBind()

            If (Me.GV_EmpresaTienda_SubLinea.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As EventArgs)

        valOnClick_btnEliminar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs)

        valOnClick_btnEditar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Private Sub valOnClick_btnEditar(ByVal index As Integer)

        Try

            Dim IdEmpresa As Integer = CInt(CType(Me.GV_EmpresaTienda_SubLinea.Rows(index).FindControl("hddIdEmpresa"), HiddenField).Value)
            Dim IdTienda As Integer = CInt(CType(Me.GV_EmpresaTienda_SubLinea.Rows(index).FindControl("hddIdTienda"), HiddenField).Value)
            Dim IdSubLinea As Integer = CInt(CType(Me.GV_EmpresaTienda_SubLinea.Rows(index).FindControl("hddIdSubLinea"), HiddenField).Value)

            Dim objETS As Entidades.EmpresaTienda_SubLinea = (New Negocio.EmpresaTienda_SubLinea).SelectxIdEmpresaxIdTiendaxIdSubLinea(IdEmpresa, IdTienda, IdSubLinea)

            If (objETS IsNot Nothing) Then

                cargarETS_GUI(objETS)
                verFrm(FrmModo.Editar, False, False)

            Else

                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarETS_GUI(ByVal objets As Entidades.EmpresaTienda_SubLinea)

        Dim objCbo As New Combo

        With objets

            If (Me.cboEmpresa.Items.FindByValue(CStr(.IdEmpresa)) IsNot Nothing) Then
                Me.cboEmpresa.SelectedValue = CStr(.IdEmpresa)
                objCbo.llenarCboTiendaxIdEmpresa(Me.cboTienda, .IdEmpresa)
            End If

            If (Me.cboTienda.Items.FindByValue(CStr(.IdTienda)) IsNot Nothing) Then
                Me.cboTienda.SelectedValue = CStr(.IdTienda)
            End If

            If (Me.cboLinea.Items.FindByValue(CStr(.IdLinea)) IsNot Nothing) Then
                Me.cboLinea.SelectedValue = CStr(.IdLinea)
                objCbo.LlenarCboSubLineaxIdLinea(Me.cboSubLinea, .IdLinea, True)
            End If

            If (Me.cboSubLinea.Items.FindByValue(CStr(.IdSubLinea)) IsNot Nothing) Then
                Me.cboSubLinea.SelectedValue = CStr(.IdSubLinea)
            End If

            Me.txtCuentaCostoCompra.Text = .CtaCostoCompra
            Me.txtCuentaCostoVenta.Text = .CtaCostoVenta
            Me.txtCuentaDebeCompra.Text = .CtaDebeCompra
            Me.txtCuentaDebeVenta.Text = .CtaDebeVenta
            Me.txtCuentaHaberCompra.Text = .CtaHaberCompra
            Me.txtCuentaHaberVenta.Text = .CtaHaberVenta

        End With

    End Sub

    Private Sub valOnClick_btnEliminar(ByVal index As Integer)

        Try

            Dim IdEmpresa As Integer = CInt(CType(Me.GV_EmpresaTienda_SubLinea.Rows(index).FindControl("hddIdEmpresa"), HiddenField).Value)
            Dim IdTienda As Integer = CInt(CType(Me.GV_EmpresaTienda_SubLinea.Rows(index).FindControl("hddIdTienda"), HiddenField).Value)
            Dim IdSubLinea As Integer = CInt(CType(Me.GV_EmpresaTienda_SubLinea.Rows(index).FindControl("hddIdSubLinea"), HiddenField).Value)

            If ((New Negocio.EmpresaTienda_SubLinea).DeletexParams(IdEmpresa, IdTienda, IdSubLinea)) Then

                verFrm(FrmModo.Inicio, True, True)
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else

                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try

            Dim objCbo As New Combo
            With objCbo
                .llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), False)
            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        valOnChange_cboLinea()
    End Sub
    Private Sub valOnChange_cboLinea()
        Try

            Dim objCbo As New Combo
            With objCbo
                .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), True)
            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

End Class
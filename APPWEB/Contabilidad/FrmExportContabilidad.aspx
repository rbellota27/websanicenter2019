﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmExportContabilidad.aspx.vb" Inherits="APPWEB.FrmExportContabilidad" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td class="TituloCelda">
                Exportación - Contabilidad</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="text-align:right">
                            Empresa:</td>
                        <td colspan="4">
                            <asp:DropDownList ID="cboEmpresa" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="text-align:right">
                            Fecha Inicio:</td>
                        <td>
                        
                           <asp:TextBox ID="txtFechaInicio"  runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>                                                                                                
                                <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaInicio">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                        
                        </td>
                        <td class="Texto" style="text-align:right">
                            Fecha Fin:</td>
                        <td>
                        
                            <asp:TextBox ID="txtFechaFin"  runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>                                                                                                
                                <cc1:MaskedEditExtender ID="txtFechaFin_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaFin">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                            </cc1:CalendarExtender>
                        
                        </td>
                        <td>
                            <asp:Button ID="btnAceptar" OnClientClick="return(  valOnClickExportarCont()   );" runat="server" Text="Exportar a Contabilidad" Width="170px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript"  type="text/javascript">

        function valOnClickExportarCont() {
            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            var txtFechaInicio = document.getElementById('<%=txtFechaInicio.ClientID%>');
            var txtFechaFin = document.getElementById('<%=txtFechaFin.ClientID%>');

            var cad = '';
            cad = cad + 'Empresa      : ' + getCampoxValorCombo(cboEmpresa, cboEmpresa.value) + '\n';
            cad = cad + 'Fecha Inicio : ' + txtFechaInicio.value + '\n';
            cad = cad + 'Fecha Fin     : ' + txtFechaFin.value + '\n';
            cad = cad + 'Desea continuar con la Exportación de los Documentos de Venta al Sistema de Contabilidad [ SoluCont ] ?';
            
            
            return confirm(cad);
        }
    
    </script>
</asp:Content>

﻿Public Partial Class FrmPasesRealizados
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.LimpiarControles()
            Me.ListarPases()
        End If
    End Sub
    Private Sub LimpiarControles()
        Me.GV_Pases.DataSource = Nothing
        Me.GV_Pases.DataBind()
        Me.hddIdPase.Value = "0"
        Me.hddIdDocumento.Value = "0"
    End Sub
    Private Sub ListarPases()
        Dim lista As List(Of Entidades.Pase) = New Negocio.Documento().SelectPasesxIdEmpresa(1)
        Me.GV_Pases.DataSource = lista
        Me.GV_Pases.DataBind()
    End Sub

    Protected Sub GV_Pases_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Pases.SelectedIndexChanged
        Me.ListarDetallePase(Me.GV_Pases.SelectedIndex)
    End Sub
    Private Sub ListarDetallePase(ByVal index As Integer)
        Try
            Dim IdPase As Integer = CInt(CType(Me.GV_Pases.Rows(index).FindControl("hddIdPase"), HiddenField).Value)
            Me.hddIdPase.Value = CStr(IdPase)
            Me.BuscarDetallexIdPase(IdPase)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarDetallexIdPase(ByVal IdPase As Integer)
        Try
            Dim ListaDetallePase As List(Of Entidades.Documento) = New Negocio.Documento().SelectDetallexIdPase(IdPase)
            Me.GV_DocExterno.DataSource = ListaDetallePase
            Me.GV_DocExterno.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_DocExterno_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_DocExterno.SelectedIndexChanged
        Try
            Me.EliminarDocumentoPase(Me.GV_DocExterno.SelectedIndex)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub EliminarDocumentoPase(ByVal index As Integer)
        Try
            Dim IdDocumento As String = CStr(CType(Me.GV_DocExterno.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value)
            Dim DocRelacionado As String = CStr(CType(Me.GV_DocExterno.Rows(index).FindControl("lblDocreferenciado"), Label).Text)
            Me.hddIdDocumento.Value = IdDocumento
            If (DocRelacionado.Length > 0) Then
                objScript.mostrarMsjAlerta(Me, "El documento no se puede eliminar ya que tiene documentos referenciados. No procede la operación.")
            Else
                Dim confirmacion As Boolean = New Negocio.Documento().EliminarDocumentoxIdDocumento(Me.hddIdDocumento.Value)
                If (confirmacion = True) Then
                    objScript.mostrarMsjAlerta(Me, "El documento se eliminó correctamente.")
                    GV_DocExterno.DataSource = Nothing
                    GV_DocExterno.DataBind()
                    BuscarDetallexIdPase(CInt(Me.hddIdPase.Value))
                Else
                    objScript.mostrarMsjAlerta(Me, "Hubo problemas en la eliminación del documento. Favor de comunicarse con el área de sistemas.")
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

End Class
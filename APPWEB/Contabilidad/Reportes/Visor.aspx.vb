﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
Imports CrystalDecisions.CrystalReports.Engine
Partial Public Class FrmVisor
    Inherits System.Web.UI.Page
    Dim reporte As New reportdocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            If iReporte Is Nothing Then
                iReporte = ""
            End If

            ViewState.Add("iReporte", iReporte)
            Select Case iReporte
                Case "1" '*** Reporte Percepcion - PDT
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("IdTipoDocumento", Request.QueryString("IdTipoDocumento"))
                    ViewState.Add("IdMonedaDestino", Request.QueryString("IdMonedaDestino"))
                    ViewState.Add("FechaF", Request.QueryString("FechaF"))
                    ViewState.Add("FechaI", Request.QueryString("FechaI"))
                    ViewState.Add("TipoDocumento", Request.QueryString("TipoDocumento"))
            End Select
        End If

        mostrarReporte()
    End Sub
    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString
            Case "1"
                ReportePercepcion_PDT()
        End Select
    End Sub
    Private Sub ReportePercepcion_PDT()
        Try
            reporte = New CR_PercepcionPDT
            reporte.SetDataSource((New Negocio.Contabilidad).get_DS_ReportePercepcion_PDT(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdTipoDocumento")), CInt(ViewState("IdMonedaDestino")), CDate(ViewState("FechaI")), CDate(ViewState("FechaF"))))

            reporte.SetParameterValue("@FechaI", CDate(ViewState("FechaI")))
            reporte.SetParameterValue("@FechaF", CDate(ViewState("FechaF")))
            reporte.SetParameterValue("@TipoDocumento", CStr(ViewState("TipoDocumento")))

            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub

    Private Sub FrmVisor_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class
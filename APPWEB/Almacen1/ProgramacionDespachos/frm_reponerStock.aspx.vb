﻿Imports Negocio
Imports System.Xml.Serialization

Public Class frm_reponerStock
    Inherits System.Web.UI.Page
    Private obj_negocio_valorizado As New LNValorizadoCajas
    Private objScript As New ScriptManagerClass
    Dim sum As Decimal = 0

    Private Sub ValidarPermisos()
        '**** REGISTRAR / PROGRAMACIONES DE DESPACHOS
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {2100899002})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.gvProductos.Enabled = True
        Else
            Me.gvProductos.Enabled = False
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            InicializarFormulario()
        End If
    End Sub

    'Private Sub gvProductos_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvProductos.PageIndexChanging
    '    gvProductos.PageIndex = e.NewPageIndex
    '    cargarProductos()
    '    Me.DataBind()
    'End Sub

    Private Sub gvProductos_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvProductos.RowCancelingEdit
        Me.gvProductos.EditIndex = -1
        cargarProductos()
    End Sub

    Private Sub gvProductos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvProductos.RowCommand
        If e.CommandName = "CrearTono" Then
            idGvProducto.Value = e.CommandArgument            
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaCrearTono');    ", True)
        End If
    End Sub

    Private Sub gvProductos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvProductos.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim labelStockKardex As Label = TryCast(e.Row.FindControl("Label1"), Label)
            Dim lbaelTituloStockKArdex As Label = TryCast(Me.gvProductos.HeaderRow.FindControl("lblStockKardex"), Label)
            lbaelTituloStockKArdex.Text = labelStockKardex.Text
            Try
                Dim label As Label = DirectCast(e.Row.FindControl("lblStockTono"), Label)
                sum = sum + CDec(Val(label.Text))
            Catch ex As Exception
                Dim textbox As TextBox = DirectCast(e.Row.FindControl("txtCantidadStock"), TextBox)
                sum = sum + CDec(Val(textbox.Text))
            End Try
            Dim titulo As Label = TryCast(Me.gvProductos.HeaderRow.FindControl("lblTituloStockTono"), Label)
            titulo.Text = sum - CDec(labelStockKardex.Text)
        End If
    End Sub

    Private Sub gvProductos_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvProductos.RowDeleting
        Dim indice As Integer = e.RowIndex
        Dim datakey As DataKey = gvProductos.DataKeys(indice)
        Dim IdProducto As Integer = datakey(0)
        Dim idAlmacen As Integer = datakey(1)
        Dim idSector_antiguo As Integer = datakey(2)        
        Dim idTono As Integer = datakey(3)
        Dim objTonoEliminar As New bl_Tonos
        Try
            objTonoEliminar.eliminarTono(IdProducto, idTono, idAlmacen)
            cargarProductos()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        'If objTonoEliminar.eliminarTono(IdProducto, idTono, idAlmacen) Then

        '    objScript.mostrarMsjAlerta(Me, "Tono Eliminado.")
        'End If
    End Sub

    Private Sub gvProductos_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvProductos.RowEditing
        Me.gvProductos.EditIndex = e.NewEditIndex
        Dim lista As New List(Of Entidades.be_productoStock)
        lista = Session("ProductosStock")
        For i As Integer = 0 To lista.Count - 1
            With lista(i)
                .listaSector = (New Negocio.bl_Sector).bl_Sector()
            End With
        Next
        'primero lleno la grilla
        gvProductos.DataSource = lista
        gvProductos.DataBind()
        'ahora busco el combo y lo lleno        
        Dim indice As Integer = e.NewEditIndex
        Dim ddl As DropDownList = TryCast(gvProductos.Rows(indice).FindControl("ddlSectorGrilla"), DropDownList)
        ddl.DataSource = lista(indice).listaSector
        ddl.DataTextField = "nomSector"
        ddl.DataValueField = "idSector"
        ddl.DataBind()
        ddl.SelectedValue = lista(indice).idsector
    End Sub

    'Public Sub cargarTipoAlmacen()
    '    Dim dt As New DataTable
    '    dt = obj_negocio_valorizado.LN_ReturnDataTable("", "TBL_TIPO_ALMACEN")
    '    Me.ddlTipoAlmacen.DataSource = dt
    '    Me.ddlTipoAlmacen.DataValueField = "IdTipoAlmacen"
    '    Me.ddlTipoAlmacen.DataTextField = "Tipoalm_Nombre"
    '    Me.ddlTipoAlmacen.DataBind()
    'End Sub

    Private Sub InicializarFormulario()
        Dim objCbo As New Combo

        With objCbo
            Dim dt As New DataTable
            dt = obj_negocio_valorizado.LN_ReturnDataTable("", "TBL_ALMACEN_SECTOR")
            .LlenarCboEmpresaxIdUsuario(Me.ddlEmpresa, CInt(Session("IdUsuario")), False)   '******Carga empresa
            .LlenarCboTipoAlmacen(Me.ddlTipoAlmacen, True)    '****carga Tipo Almacen
            .llenarCboTipoExistencia(Me.ddlTipoExistencia, False)  '****Cargar tipo existencia            
            .llenarCboLineaxTipoExistencia(Me.ddlLinea, CInt(Me.ddlTipoExistencia.SelectedValue), True) '****Cargar Linea
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.ddlSublinea, CInt(Me.ddlLinea.SelectedValue), CInt(ddlTipoExistencia.SelectedValue), True)
            Me.ddlSector.DataSource = dt
            Me.ddlSector.DataValueField = "id_sector"
            Me.ddlSector.DataTextField = "nom_sector"
            Me.ddlSector.DataBind()
            Me.ddlSector.Items.Insert(0, New ListItem("---", "0"))
        End With
    End Sub

    Private Sub ddlLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLinea.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.ddlSublinea, CInt(Me.ddlLinea.SelectedValue), True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub ddlTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(ddlLinea, CInt(ddlTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.ddlSublinea, CInt(Me.ddlLinea.SelectedValue), CInt(ddlTipoExistencia.SelectedValue), True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        Call cargarProductos()
        Me.GV_Equivalencia.DataSource = (New Negocio.bl_Tonos).ln_SelectUnidadMedidaxProductos(Me.txtCodigo.Text)
        Me.GV_Equivalencia.DataBind()
        ValidarPermisos()
    End Sub

    Private Sub cargarProductos()
        Dim lista As New List(Of Entidades.be_productoStock)
        lista = (New Negocio.LN_TRAZABILIDAD_DISTRIBUCION).LN_trazabilidad_Select_Kardex(Me.ddlSector.SelectedValue, Me.ddlAlmacen.SelectedValue, Me.txtDescripcion.Text.Trim(), _
                                                                      Me.txtCodigo.Text.Trim(), Me.ddlTipoExistencia.SelectedValue, Me.ddlLinea.SelectedValue, _
                                                                      Me.ddlSublinea.SelectedValue)
        Session("ProductosStock") = lista
        Me.gvProductos.DataSource = lista

        Me.gvProductos.DataBind()
    End Sub

    Private Sub gvProductos_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvProductos.RowUpdating
        Dim indiceFila As Integer = e.RowIndex
        Dim datakey As DataKey = gvProductos.DataKeys(indiceFila)
        Dim IdProducto As Integer = datakey(0)
        Dim idAlmacen As Integer = datakey(1)
        Dim idSector_antiguo As Integer = datakey(2)
        Dim idSector As Integer = 0
        Dim idTono As Integer = datakey(3)
        Dim textbox As TextBox = DirectCast(gvProductos.Rows(indiceFila).FindControl("txtCantidadStock"), TextBox)
        Dim ProductoNombre As String = DirectCast(gvProductos.Rows(indiceFila).FindControl("Label5"), Label).Text
        Dim indice As Integer = e.RowIndex
        'obtiene el selectedvalue combo sector
        Dim idSector_nuevo As Integer = TryCast(gvProductos.Rows(indice).FindControl("ddlSectorGrilla"), DropDownList).SelectedValue

        If (New Negocio.LN_TRAZABILIDAD_DISTRIBUCION).LN_Update_Stock(IdProducto, idAlmacen, idSector_antiguo, idSector_nuevo, CDec(textbox.Text.Trim()), idTono) Then
            gvProductos.EditIndex = -1
            cargarProductos()
            objScript.mostrarMsjAlerta(Me, "Stock Actualizado para el código " & ProductoNombre)
        Else
            objScript.mostrarMsjAlerta(Me, "Error de actualización")
        End If
    End Sub

    Private Sub cargarAlmacen()
        Dim dt As New DataTable
        dt = obj_negocio_valorizado.LN_ReturnDataTable(Me.ddlTipoAlmacen.SelectedValue, "TBL_ALMACEN_X_TIPO")
        Me.ddlAlmacen.DataSource = dt
        Me.ddlAlmacen.DataValueField = "IdAlmacen"
        Me.ddlAlmacen.DataTextField = "alm_Nombre"
        Me.DataBind()
    End Sub

    Private Sub ddlTipoAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoAlmacen.SelectedIndexChanged
        cargarAlmacen()
    End Sub

    Private Sub btnNuevoTono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevoTono.Click
        Dim indice As Integer = idGvProducto.Value
        Dim datakey As DataKey = gvProductos.DataKeys(indice)
        Dim idProducto As Integer = datakey(0)
        Dim idSector As Integer = datakey(2)
        Dim objNegocioTrazabilidad As New Negocio.LN_TRAZABILIDAD_DISTRIBUCION
        'Me.listaDetalleDocumento = getListaDetalleDocumento()
        If (New Negocio.LN_TRAZABILIDAD_DISTRIBUCION).LN_mantenimiento_Tonos(Me.txtNombreTono.Text.Trim(), Me.txtDescripcionTono.Text.Trim(), _
                                                                             idProducto, Me.ddlAlmacen.SelectedValue, idSector) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "alert('Tono Creado');    ", True)
        End If
    End Sub
End Class
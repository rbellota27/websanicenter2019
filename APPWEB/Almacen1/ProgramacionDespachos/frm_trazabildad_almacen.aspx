﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frm_trazabildad_almacen.aspx.vb" Inherits="APPWEB.frm_trazabildad_almacen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
    <div style="width:100%">
        <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" />
        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClientClick="return(ValidarBuscar());" />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="return (validarGuardar());" />        
        <asp:Button ID="btnEditar" runat="server" Text="Editar" />
        <asp:Button ID="btnAnular" runat="server" Text="Anular" />
        <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" OnClick="btnExportar_Click" CommandName="pdf" />
        <asp:Button ID="btnImprimirEstandar" runat="server" Text="Imprimir Estandar" />
        <asp:Button ID="btnDocumentoReferencia" runat="server" Text="Doc Referencia" OnClientClick="return (mostratCapaOrdenCompra());" />
    </div>
    <div id="capaOrdenCompra" style="border: 3px solid blue; padding: 8px;
        width: 800px; height: auto; position: fixed; top: 50px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <div style="width:100%" align="right">
            <asp:ImageButton ID="btnCerrar" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
        </div>
        <div style="width:100%;float:left">
            <div style="width:100%;float:left">
                <table align="left">
                    <tr>
                    <td colspan="5">
                        <asp:RadioButtonList ID="rblistFiltros" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                            <asp:ListItem Text="Por Fecha Emisión" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Por Nro. Documento" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>                    
                    <tr>
                    <td>
                    <asp:Panel ID="pnlFiltroFechas" runat="server">
                        <table>
                            <tr>
                                <td>
                                    Fecha Inicio:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechainiciofiltro" runat="server"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFechainiciofiltro" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </td>
                                <td>
                                    Fecha Fin:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaFinFiltro" runat="server"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtFechaFinFiltro" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </td>
                                <td>
                                    <asp:Button ID="btnBuscarOrdenCompra" runat="server" Text="Buscar" CssClass="btnBuscar" CommandName="BuscarxFecha"
                                    OnClick="btnBuscarDocumento" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <asp:Panel ID="pnlFiltroNroDocumento" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td>
                                        Nro. Serie:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtnroSerie" runat="server" Width="70px"></asp:TextBox>
                                    </td>
                                    <td>
                                        Nro. Código:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtnroCodigo" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnBuscarPorCodigo" runat="server" Text="Buscar" CssClass="btnBuscar" CommandName="BuscarxCodigo"
                                        OnClick="btnBuscarDocumento" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>                        
                    </td>
                </tr>
                </table>
            </div>
            <div style="width:100%;float:left">
                <asp:GridView ID="gvOrdenCompra" runat="server" Width="100%" DataKeyNames="idDocumento"
                    AutoGenerateColumns="False">
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <Columns>
                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                            <asp:BoundField DataField="tdoc_NombreCorto" HeaderText="Tipo" />
                            <asp:BoundField DataField="nroDoc" HeaderText="Nro. Documento" />
                            <asp:BoundField DataField="doc_FechaEmision" HeaderText="Fec. Emisión" />
                            <asp:BoundField DataField="edoc_Nombre" HeaderText="Estado" />
                        </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                </asp:GridView>
                <asp:Label ID="lblMensajeOrdenCompra" runat="server" Text="*** El proceso de búsqueda realiza un filtrado de acuerdo al destinatario seleccionado. "
                ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
    <div class="TituloCelda" style="width:100%">
        <span>GUIA DE DISTRIBUCIÓN</span>
    </div>
    <asp:Panel ID="pnlDatosGenerales" runat="server">
    <div class="datosGenerales" style="width:100%;float:left">
        <div style="vertical-align:middle;margin:5px 5px 5px 5px;float:left">
            <table>
                <tr>
                    <td style="color:red">
                        Tipo Documento
                    </td>
                    <td colspan="6" class="Texto">
                        <asp:DropDownList ID="ddlTipoDocumento" runat="server" AutoPostBack="true" Width="100%">
                        </asp:DropDownList>
                    </td>
                    <td rowspan="3">
                        <table>
                            <tr align="center">
                                <td class="TituloCelda" colspan="2">
                                    <span>Leyenda</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="../../Imagenes/img_leyenda/cuadrado_rojo.png" />
                                </td>
                                <td style="font-style:italic;font-weight:bold;font-size:12px">
                                    El producto no tiene tono asociado.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="../../Imagenes/img_leyenda/cuadrado_verde.png" />
                                </td>
                                <td style="font-style:italic;font-weight:bold;font-size:12px">
                                    El producto sólo tiene un tono asociado.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="../../Imagenes/img_leyenda/cuadrado_amarillo.png" />
                                </td>
                                <td style="font-style:italic;font-weight:bold;font-size:12px">
                                    El producto tiene mas de un tono.
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Texto">
                        Empresa:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEmpresa" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td class="Texto">
                        Tienda:
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlTienda" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                    <td class="Texto">
                        Serie:
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlSerie" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="ddlTienda" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Panel ID="pnlBusqueda" runat="server" DefaultButton="imgbtnBuscar">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtnroDocumento" runat="server" Width="70px" ></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlSerie" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ddlTienda" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                    <td>
                        <asp:ImageButton ID="imgbtnBuscar" runat="server" ImageUrl="~/Caja/iconos/ok.gif" />
                    </td>
                </tr>
                <tr>
                    <td class="Texto">
                        Fecha Emisión:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFechaEmision" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaEmision" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </td>
                    <td class="Texto">
                        Estado:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEstado" runat="server" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="2100000001" />
                    </td>
                    <td>
                        <asp:HiddenField ID="hddidDocumento" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
        </div>
        
     </div>
     </asp:Panel>
    <div class="TituloCelda" style="width:100%;float:left;margin:5px 5px 5px 5px">
    <span >DOCUMENTO DE REFERENCIA</span>
    </div>
    <asp:Panel ID="pnlDocumentoReferencia" runat="server">
    <div style="width:100%;float:left;margin:5px 5px 5px 5px">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
        <asp:GridView ID="gvDocumentoReferencia" runat="server" AutoGenerateColumns="false" Width="100%">
        <HeaderStyle CssClass="GrillaHeader" />
        <RowStyle CssClass="GrillaRow" />
            <Columns>
                <asp:BoundField DataField="Nombre" HeaderText="Documento" />
                <asp:BoundField DataField="nroDocumento" HeaderText="Nro. Documento" />
                <asp:BoundField DataField="FechaEmision" HeaderText="Fec. Emisión" />                               
                <asp:BoundField DataField="Estado" HeaderText="Estado" />
            </Columns>
        </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imgbtnBuscar" EventName="Click" />
        </Triggers>
        </asp:UpdatePanel>
    </div>
    </asp:Panel>
    <div class="TituloCelda" style="width:100%;float:left;margin:5px 5px 5px 5px">
        <span>DISTRIBUCIÓN DE MERCADERIA</span>
    </div>
    <asp:Panel ID="pnlDistribucionMercaderia" runat="server">
    <div style="width:100%;float:left">
        <div style="width:100%;float:left;z-index:1">
            <asp:GridView ID="gvistribucionMercaderia" runat="server" DataKeyNames="IdProducto,IdUnidadMedida,id_sector"
                AutoGenerateColumns="False" Width="100%" Font-Size="8pt">
                <RowStyle HorizontalAlign="Center" CssClass="GrillaRowDistribucion" />
                <HeaderStyle HorizontalAlign="Center" CssClass="GrillaHeaderDistribucion" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblContador" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkSeleccionarHeader" runat="server" Checked="true" onclick="seleccionarTodoSectores(this);" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSeleccionarRow" runat="server" Checked="true" onclick="desSeleccionar(this);" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="prod_Codigo" HeaderText="Cod. SIGE" />   
                    <asp:BoundField DataField="nom_producto" HeaderText="Producto" />                    
                    <asp:BoundField DataField="dc_UMedida" HeaderText="U. Medida" />
                    <asp:BoundField DataField="dc_Cantidad" HeaderText="Cantidad" />
                    <asp:BoundField DataField="linea" HeaderText="linea" />
                    <asp:BoundField DataField="sublinea" HeaderText="SubLinea" />
                    <asp:BoundField DataField="per_NComercial" HeaderText="Proveedor" />
                    <asp:BoundField DataField="estadoProducto" HeaderText="Estado" />
                    <asp:TemplateField HeaderText="Ver Tono">
                        <ItemTemplate>
                            <asp:Button ID="btnAsignarSector" runat="server" Text="..." CommandName="btnSector" CommandArgument='<%# Container.DataItemIndex %>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatDirection="Horizontal" DataTextField="nom_sector"
                                    DataValueField="id_sector">
                                    </asp:CheckBoxList>
                                    <asp:HiddenField ID="hdfEstadoAsignacionTonos" runat="server" Value="1" />
                        </ItemTemplate>
                    </asp:TemplateField>                    
                </Columns>

            </asp:GridView>
        </div>
    </div>
    </asp:Panel>
        <div id="capaSector" style="border: 3px solid blue; padding: 8px;
            width: 850px; height: auto; position: fixed; top: 50px; left: 32px; background-color: white;
            z-index: 3; display: none;">
            <div style="width:100%" align="right" >
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
            </div>
            <div style="width:100%;margin-bottom:5px" class="TituloCelda">
                <asp:Label ID="lblTextoCabecera" runat="server"></asp:Label>                
            </div>
            <div style="width:100%">
                <table>
                    <tr>
                        <td colspan="6" align="center">
                            <asp:Label ID="lblTituloProducto" runat="server" CssClass="TituloCelda"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Linea:
                        </td>
                        <td>
                            <asp:Label ID="lblLinea" runat="server"></asp:Label>
                        </td>
                        <td class="Texto">
                            Sub linea:
                        </td>
                        <td>
                            <asp:Label ID="lblSublinea" runat="server"></asp:Label>
                        </td>
                        <td class="Texto">
                            Proveedor:
                        </td>
                        <td>
                            <asp:Label ID="lblProveedor" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            U. Medida:
                        </td>
                        <td>
                            <asp:Label ID="lblUnidadMedida" runat="server"></asp:Label>
                        </td>
                        <td class="Texto">
                            Cantidad:
                        </td>
                        <td>
                            <asp:Label ID="lblCantidad" runat="server"></asp:Label>
                        </td>
                        <td colspan="2">
                            <%--<asp:Button ID="btnGuardarSectores" runat="server" Text="Terminar Proceso..." />
                            OnClientClick="return (validarCantidadTotal());"--%>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width:100%">
                <hr class="Texto" />
            </div>
            <div style=" width:100%" class="TituloCelda">
                <span>DISTRIBUCIÓN DE PRODUCTOS POR SECTORES</span>
            </div>
            <div style="width:100%">
                <asp:GridView ID="gvSectores" runat="server" AutoGenerateColumns="false" Width="100%" DataKeyNames="id_sector">
                    <RowStyle CssClass="GrillaRow" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="GrillaHeader" HorizontalAlign="Center" />
                    <Columns>
                        <asp:BoundField DataField="nom_sector" HeaderText="Sector" />
                        <asp:BoundField DataField="controlador" HeaderText="Controlador" />
                        <asp:BoundField DataField="ProductNames" HeaderText="Sublinea" />
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblCantidadDisponible" runat="server" ForeColor="Yellow"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Button ID="btnAsignarTono" runat="server" Text="..." CommandName="imgAsignarTono" CommandArgument='<%# Container.DataItemIndex %>' />
                                <%--<asp:TextBox ID="txtCantidadxDistribuir" Width="50px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CantidadSaldo")%>'
                                onkeyup="return (teclearCantidad());" onkeypress="return (DesactivarTecla(event))"></asp:TextBox>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>

        <div id="capaAsignarTono" style="border: 3px solid blue; padding: 8px;
            width: 500px; height: auto; position: fixed; top: 100px; left: 100px; background-color: white;
            z-index: 4; display: none;">
            <div style="width:100%" align="right">
                <asp:ImageButton ID="imgButtonCerrar" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
            </div>
            <div style="width:100%;margin-bottom:5px" class="TituloCelda">
                <asp:Label ID="lblSector" runat="server" Text=""></asp:Label>                
            </div>
            <div style="width:100%" class="Texto">
                <asp:RadioButtonList ID="rBListSectores" runat="server" DataValueField="id_sector" DataTextField="nom_sector" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </div>
            <div style="width:100%">            
                <div id="divSector" style="float:left" runat="server" visible="false">
                    <span class="Texto">Sector</span><br />
                    <asp:DropDownList ID="ddlSector" runat="server" DataValueField="id_sector" DataTextField="nom_sector"></asp:DropDownList>
                </div>
                <div style="margin-left:20px;float:left">
                    <span class="Texto">Tono</span><br />
                    <asp:DropDownList ID="ddltono" runat="server" DataValueField="ID_REL_TONO_PROD" DataTextField="NOM_TONO"></asp:DropDownList>
                </div>
                <div style="padding:12px 5px 5px 5px">
                <asp:Button ID="btnAgregarTono" runat="server" Text="Agregar" />
                <asp:Button ID="btnCrearTono" runat="server" Text="Crear Tono" />
                <asp:Button ID="btnGuardarTonos" runat="server" Text="Guardar" OnClientClick="return (validarGuardarTonos());" />
                </div>
            </div>
            <div style="width:100%">
                <asp:GridView ID="gvTono" runat="server" AutoGenerateColumns="False">
                    <RowStyle CssClass="GrillaRow" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                        <asp:BoundField HeaderText="IdTono" DataField="ID_REL_TONO_PROD" />
                        <asp:BoundField HeaderText="Tono" DataField="NOM_TONO" />
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <label>Cantidad</label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtCantidadTono" runat="server" Width="50" onkeyup="return (validarCantidadTono(this));"
                                 Text='<%#DataBinder.Eval(Container.DataItem,"CANTIDAD") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>

         <div id="capaCrearTono" style="border: 3px solid blue; padding: 8px;
            width: 500px; height: auto; position: fixed; top: 150px; left: 180px; background-color: white;
            z-index: 5; display: none;">
            <div style="width:100%" align="right">
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
            </div>
            <div style="width:100%" align="center" class="TituloCelda">
                <span>Crear nuevo tono</span>
            </div>
            <div style="width:100%">
                <table>
                    <tr>
                        <td class="Texto">
                            Nombre del tono:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNombreTono" runat="server" Text=""></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Descripción:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescripcionTono" runat="server" Text=""></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button runat="server" id="btnTono" Text="Guardar" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvTonoLista" runat="server" AutoGenerateColumns="false" Width="100%">
                                <RowStyle CssClass="GrillaRow" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <Columns>
                                    <asp:BoundField DataField="ID_REL_TONO_PROD" HeaderText="Id" />
                                    <asp:BoundField DataField="NOM_TONO" HeaderText="Nombre" />
                                    <asp:BoundField DataField="DESC_TONO" HeaderText="Descripción" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
         </div>
</div>

<script language="javascript" type="text/javascript">

    function validarGuardarTonos() {
        var grilla = document.getElementById("<%=gvTono.ClientID %>");
        var cantidadTotal = document.getElementById("<%=lblCantidad.ClientID %>");
        var cantidadDigitada = 0
        for (var i = 1; i < grilla.rows.length; i++) {
            cantidadDigitada += parseFloat(grilla.rows[i].cells[2].children[0].value);
            }
        if (cantidadDigitada != parseFloat(cantidadTotal.innerHTML)) {
            alert("No se puede guardar, deberá ingresar la cantidad total del producto entrante.");
            return false;
        }
        return true;
    }

    function validarCantidadTono(textbox) {
        var grilla = document.getElementById("<%=gvTono.ClientID %>");
        var cantidadTotal = document.getElementById("<%=lblCantidad.ClientID %>");
        var cantidadDigitada = 0
        for (var i = 1; i < grilla.rows.length; i++) {
            cantidadDigitada += parseFloat(grilla.rows[i].cells[2].children[0].value);
            if (cantidadDigitada > parseFloat(cantidadTotal.innerHTML)) {
                alert("La cantidad ingresada no puede ser mayor a " + cantidadTotal.innerHTML);
                grilla.rows[i].cells[2].children[0].value = 0
                grilla.rows[i].cells[2].children[0].select();
                grilla.rows[i].cells[2].children[0].focus();                
                return false;
            }
        }
        return true;
    }

    function validarGuardar() {
        var grilla = document.getElementById("<%=gvistribucionMercaderia.ClientID %>");
        if (grilla = null) {return false;}
        var clicked = this;
        var AllChecked = true;
        var contadorMaximo = 1
        var hidden
        for (var i = 1; i < grilla.rows.length; i++) {
            var contador = 0
            var checkList = grilla.rows[i].cells[10].children[0].getElementsByTagName("input");
            hidden = grilla.rows[i].cells[10].children[1].value;
            for (var j = 0; j < checkList.length; j++) {
                if (checkList[j].checked == true) {
                    contador = contador + 1;
                    if (contador > contadorMaximo) {
                        alert("Un Código de producto no puede ser asignado a mas de un sector, revisar distribución y volver a intentar.")
                        return false;
                    }
                }
            }
            if (contador == 0) {
               alert("Existe un producto que no tiene asignado el sector destino, seleccione el sector para el producto\n " + grilla.rows[i].cells[2].innerHTML);
               return false;
            }
           if (hidden == 0) {
               alert("Aún no ha seleccionado los tonos para el producto\n " + grilla.rows[i].cells[2].innerHTML);
               return false;
            }
        }
        return true;
    }
    function seleccionarTodoSectores(checkbox) {
        var grilla = document.getElementById("<%=gvistribucionMercaderia.ClientID %>");  //obtiene elemento gridview                       
        for (var i = 1; i < grilla.rows.length; i++) {
            grilla.rows[i].cells[1].getElementsByTagName('INPUT')[0].checked = checkbox.checked;
        }
    }

    function desSeleccionar(objeto) {
        var atleastOneCheckBoxUnchecked = false;
        var grilla = document.getElementById("<%=gvistribucionMercaderia.ClientID %>") //obtiene elemento gridview                       
        for (var i = 1; i < grilla.rows.length; i++) {
            if (grilla.rows[i].cells[1].getElementsByTagName('INPUT')[0].checked == false) {
                var atleastOneCheckBoxUnchecked = true;
                break;
            }
        }
        grilla.rows[0].cells[1].getElementsByTagName('INPUT')[0].checked = !atleastOneCheckBoxUnchecked;
    }
    function mostratCapaOrdenCompra() {
        onCapa('capaOrdenCompra');
        return false;
    }
    function mostrarCapaSector() {
        onCapa('capaSector');        
    }
    function AsignarSector(btn) {
        var grilla = document.getElementById("<%=gvistribucionMercaderia.ClientID %>")
        var row = btn.parentNode.parentNode;
        var rowIndex = row.rowIndex;

        for (var i = 1; i < grilla.rows.length; i++) {
            document.getElementById("<%=lblTituloProducto.ClientID %>").innerHTML = grilla.rows[rowIndex].cells[2].innerHTML;
            document.getElementById("<%=lblUnidadMedida.ClientID %>").innerHTML = grilla.rows[rowIndex].cells[3].innerHTML;
            document.getElementById("<%=lblCantidad.ClientID %>").innerHTML = grilla.rows[rowIndex].cells[4].innerHTML;
            document.getElementById("<%=lblLinea.ClientID %>").innerHTML = grilla.rows[rowIndex].cells[5].innerHTML;
            document.getElementById("<%=lblSublinea.ClientID %>").innerHTML = grilla.rows[rowIndex].cells[6].innerHTML;
            document.getElementById("<%=lblProveedor.ClientID %>").innerHTML = grilla.rows[rowIndex].cells[7].innerHTML;
        }
        onCapa('capaSector');

    }

    function validarCantidadTotal() {
        var grilla = document.getElementById("<%=gvSectores.ClientID %>");
        var cantidadTotal = parseFloat(document.getElementById("<%=lblCantidad.ClientID %>").innerHTML);
        var nuevoTotal = 0;
        for (var i = 1; i < grilla.rows.length; i++) {
            nuevoTotal = nuevoTotal + parseFloat(grilla.rows[i].cells[3].children[0].value);
        }        
        if (parseFloat(nuevoTotal) == cantidadTotal) {
            return true;
        } else {
            alert("Las cantidades ingresadas no concuerdan con la cantidad total del producto.");            
            return false;        
        }
    }

    function teclearCantidad() {
        var grilla = document.getElementById("<%=gvSectores.ClientID %>");
        var cantidadTotal = parseFloat(document.getElementById("<%=lblCantidad.ClientID %>").innerHTML);
        //var cantidadTotal = parseFloat(grilla.rows[0].cells[3].innerHTML); //cabecera de la grilla que contiene la cantidad Total a distribuir

        var nuevoTotal = 0;
        var teclaPresionada = 0;
        for (var i = 1; i < grilla.rows.length; i++) {
            teclaPresionada = parseFloat(grilla.rows[i].cells[3].children[0].value);
            if (isNaN(teclaPresionada)) {teclaPresionada = 0;}
            nuevoTotal = parseFloat(nuevoTotal) + teclaPresionada            
            if (parseFloat(nuevoTotal) > parseFloat(cantidadTotal)) {
                alert("El número ingresado es mayor que la cantidad total [" + cantidadTotal + "]");
                grilla.rows[i].cells[3].children[0].value = 0;
                grilla.rows[i].cells[3].focus();
                return false;
            }
            grilla.rows[0].cells[3].innerHTML = nuevoTotal;
        }
    }

    function DesactivarTecla(elEvento) {
        //obtener caracter pulsado en todos los exploradores
        var evento = elEvento || window.event;
        var caracter = evento.charCode || evento.keyCode;
//        alert("El carácter pulsado es: " + caracter);
        if (caracter == 9) {
            return false;
        }
    }

    function ValidarBuscar() {
        var txtNroDoc = document.getElementById("<%=txtnroDocumento.ClientID %>")
        txtNroDoc.readOnly = false;
        txtNroDoc.select();
        txtNroDoc.focus();
        return false;
    }

</script>
</asp:Content>

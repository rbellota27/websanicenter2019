﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frm_Solicitud.aspx.vb" Inherits="APPWEB.frm_Solicitud" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="width:100%"><!--empieza div principal-->
<div style="width:100%">
    <table>
        <tr>
            <td>
                <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
            </td>
            <td>
                <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                    Text="Editar" ToolTip="Editar" Width="80px" />
            </td>
            <td>
                <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                    Text="Buscar" ToolTip="Buscar" Width="80px" />
            </td>
            <td>
                <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                    Text="Anular" ToolTip="Anular" Width="80px" />
            </td>
            <td>
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick=" return(     valSaveDocumento()       ); "
                    Width="80px" />
            </td>
            <td>
                <asp:Button ID="btnImprimir1" runat="server" OnClientClick="return(  valOnClick_btnImprimir(1)  );"
                    Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
            </td>
            <td>
                <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir(2)  );"
                    Text="Imprimir Estandar" ToolTip="Imprimir Documento" />
            </td>
            <td>
                <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Doc. Referencia" OnClientClick="  return(  onCapa('capaDocumentosReferencia')  );  "
                    ToolTip="Buscar Despachos Pendientes" Width="100px" />
            </td>
            <td>
            </td>
            <td align="right" style="width: 100%">
            </td>
        </tr>
    </table>
</div>
<div style="width:100%" class="TituloCelda">
<span>SOLICITUD DE CLIENTE</span>
</div>
<div style="width:100%"><!--Aca empieza el div cabecera-->
<asp:Panel ID="Panel_Cabecera" runat="server">
<table>
    <tr>
        <td align="right" class="Texto">
            Empresa:
        </td>
        <td>
            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="right" class="Texto">
            Tienda:
        </td>
        <td>
            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="right" class="Texto">
            Serie:
        </td>
        <td>
            <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">
            <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this,event)  );"
                ReadOnly="true" Width="100px"></asp:TextBox>
                </asp:Panel>
        </td>
        <td>
            <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - Número ]." />
        </td>
        <td>
            <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="Búsqueda Avanzada">Avanzado</asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td align="right" class="Texto">
            Fecha Emisión:
        </td>
        <td>
            <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                Width="100px"></asp:TextBox>
            <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
            </cc1:MaskedEditExtender>
            <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
            </cc1:CalendarExtender>
        </td>
        <td align="right" class="Texto">
            Estado:
        </td>
        <td>
            <asp:DropDownList ID="cboEstado" runat="server" Enabled="false" Width="100%">
            </asp:DropDownList>
        </td>
        <td align="right" class="Texto">
        </td>
        <td>
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="right" class="Texto">
            Inicio Traslado:
        </td>
        <td>
            <asp:TextBox ID="txtFechaInicioTraslado" runat="server" CssClass="TextBox_Fecha"
                onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>
            <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicioTraslado" runat="server"
                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicioTraslado">
            </cc1:MaskedEditExtender>
            <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicioTraslado" runat="server"
                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaInicioTraslado">
            </cc1:CalendarExtender>
        </td>
        <td align="right" class="Texto">
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td align="right" class="Texto">
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="right" class="Texto">
            Almacén:
        </td>
        <td colspan="5">
            <asp:DropDownList ID="cboAlmacen" runat="server" Width="100%" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
            &nbsp;&nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="right" class="Texto">
            Operación:
        </td>
        <td colspan="5">
            <asp:DropDownList ID="cboTipoOperacion" runat="server" Visible="true" AutoPostBack="true"
                Width="100%">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="right" class="Texto">
            Motivo Traslado:
        </td>
        <td colspan="5">
            <asp:DropDownList ID="cboMotivoTraslado" runat="server" Width="100%" AutoPostBack="false">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
</asp:Panel>
</div><!--Aca termina el div cabecera-->
<div style="width:100%" class="TituloCeldaLeft">
<asp:Image ID="imgDocumentoRef" runat="server" />&nbsp;DOCUMENTO DE REFERENCIA
                            <cc1:CollapsiblePanelExtender ID="cpeDocumentoRef" runat="server" TargetControlID="Panel_DocumentoRef"
                                ImageControlID="imgDocumentoRef" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="imgDocumentoRef" ExpandControlID="imgDocumentoRef">
                            </cc1:CollapsiblePanelExtender>
</div>
<div style="width:100%"><!--Empieza div Doc Referencia-->
<asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
<table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                                        HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
 
                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                                        <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                                                        <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomAlmacen" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                                                        HeaderText="Almacén" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                        <!--OnClick="OnClick_imgVerDetalle"-->
                                                                        <asp:ImageButton ID="imgVerDetalle" runat="server" 
                                                                            ImageUrl="~/Imagenes/search_add.ico" Width="25px" Height="25px" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef_Detalle" runat="server" AutoGenerateColumns="False"
                                                Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:BoundField DataField="CodigoProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Cód." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                  
                                                  <asp:BoundField DataField="NroDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Cód." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />                                                 
                                                    <asp:BoundField DataField="NomProducto" ItemStyle-Width="400px" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Producto" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="UMedida" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="U.M." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="PrecioCD" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="P. Venta" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" Visible="false" />
                                                    <asp:BoundField DataField="Cantidad" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="CantxAtender" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Pendiente" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="CantidadTransito" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Cant. Transito" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
</asp:Panel>
</div><!--Termina div de documento referencia-->
<div style="width:100%" class="TituloCelda">
<span>DETALLE</span>
</div>
<div style="width:100%">    <!--Empieza div detalle-->
<asp:Panel ID="Panel_Detalle" runat="server">
<table width="100%">
    <tr>
        <td>
            <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="ma_IdUMPrincipal">
                <HeaderStyle CssClass="GrillaHeader" />
                <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                                        <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>

                                        <asp:LinkButton CssClass="Texto" ID="lkbQuitar" Font-Bold="true" runat="server" >Quitar</asp:LinkButton>
                        </ItemTemplate>
                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="prod_Codigo" HeaderText="Código" HeaderStyle-Height="25px"
                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="prod_Nombre" HeaderText="Descripción" HeaderStyle-Height="25px"
                        HeaderStyle-HorizontalAlign="Center" >
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="U. Medida">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlUmPrincipal" runat="server"></asp:DropDownList>
                            <!--<asp:Label ID="Label1" runat="server" Text='<%# Bind("ma_UMPrincipal") %>'></asp:Label>-->
                        </ItemTemplate>
                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Height="25px" 
                        HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" 
                        ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:TextBox ID="txtCantidad" runat="server" ReadOnly="true" TabIndex="500" 
                                Text='<%#DataBinder.Eval(Container.DataItem,"ma_CantxAtender","{0:G29}")%>' 
                                Width="90px"></asp:TextBox>
                            <asp:HiddenField ID="hddma_IdUMPrincipal" runat="server" 
                                Value='<%#DataBinder.Eval(Container.DataItem,"ma_IdUMPrincipal","{0:F2}")%>' />
                        </ItemTemplate>
                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="GrillaFooter" />
                <PagerStyle CssClass="GrillaPager" />
                <RowStyle CssClass="GrillaRow" />
                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                <EditRowStyle CssClass="GrillaEditRow" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table>
                <tr>
                    <td class="Texto" style="font-weight: bold">
                        Peso Total:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPesoTotal" Width="70px" CssClass="TextBox_ReadOnlyLeft" onKeyPress=" return( false ); "
                            runat="server" Text="0"></asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboUnidadMedida_PesoTotal" runat="server" Enabled="false">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Panel>
</div>
<div style="width:100%" class="TituloCelda">
<span>OBSERVACIONES</span>
</div>
<div style="width:100%">
<asp:Panel ID="Panel_Obs" runat="server">
    <table width="100%">
        <tr>
            <td>
                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" runat="server"
                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos);"
                    Height="100px"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Panel>
                <asp:HiddenField ID="hddIdRemitente" runat="server" />
                <asp:HiddenField ID="hddIdDestinatario" runat="server" />
                <asp:HiddenField ID="hddIdTransportista" runat="server" />
                <asp:HiddenField ID="hddIdChofer" runat="server" />
                <asp:HiddenField ID="hddIdVehiculo" runat="server" />
                <asp:HiddenField ID="hddIdAgente" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIndex_GV_Detalle" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="2100000002" />
                <asp:HiddenField ID="hddOpcionBusquedaPersona" runat="server" Value="0" />
                <asp:HiddenField ID="hddOpcionBusquedaAgente" runat="server" Value="0" />
                <asp:HiddenField ID="hddNroFilasxDocumento" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="HddIdProd" runat="server" />
                <asp:HiddenField ID="hddPoseeGRecepcion" runat="server" />
</div>
</div><!--Termina div general de la página-->
<div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 950px;
        height: auto; position: fixed; background-color: white; z-index: 2; display: none;
        top: 100px; left: 20px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisión</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel9" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. Código:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel10" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%" AllowPaging="True" PagerSettings-Position="TopAndBottom" PagerStyle-ForeColor="White">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                    <asp:HiddenField ID="hddIdEmpresa_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                    <asp:HiddenField ID="hddIdAlmacen_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                    <asp:HiddenField ID="hddIdPersona_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisión"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripción" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomAlmacen" HeaderText="Almacén" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="MotivoTraslado" HeaderText="Motivo Traslado" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TipoOperacion" HeaderText="Tipo Operacion" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="Texto" style="text-align: left; font-weight: bold">
                    *** El filtro de los despachos pendientes se realiza de acuerdo a la Empresa / Almacén
                    seleccionados.
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

﻿Imports System.Linq
Public Class frm_Solicitud
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Public Shared listaDoc_ref As List(Of Entidades.DocumentoView)
    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ValidarPermisos()
            inicializarFrm()
        End If
    End Sub

    Private Sub ValidarPermisos()

        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA TRASLADO / OPCION MOVER STOCK FISICO / DESPACHAR 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {59, 60, 61, 62, 63, 147, 148, 143, 197})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True

        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False

        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* FECHA TRASLADO
            Me.txtFechaInicioTraslado.Enabled = True
        Else
            Me.txtFechaInicioTraslado.Enabled = False
        End If

        'If listaPermisos(6) > 0 Then  '********* OPCION MOVER STOCK FISICO
        '    Me.chb_MoverAlmacen.Enabled = True
        'Else
        '    Me.chb_MoverAlmacen.Enabled = False
        'End If

        If listaPermisos(7) > 0 Then  '********* DESPACHAR
            Me.btnBuscarDocumentoRef.Enabled = True
        Else
            Me.btnBuscarDocumentoRef.Enabled = False
        End If

        'If listaPermisos(8) > 0 Then  '********* DESPACHAR
        '    Me.rbl_UtilizarRedondeo.Enabled = True
        'Else
        '    Me.rbl_UtilizarRedondeo.Enabled = False
        'End If

    End Sub

    Private Sub inicializarFrm()

        Try
            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)   '******Carga empresa
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)    '****Carga Tienda
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))  '***Carga Serie
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(hddIdTipoDocumento.Value), 2, False)  '****Carga tipoDocumento de la capa DocREferencia
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)    '*****Carga tipo de operacion
                .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)   '****Carga tipo de traslado
                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)  '******Carga  almacen
            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            actualizarOpcionesBusquedaDocRef(0, False)  '*****Oculta el paneles de busqueda a criterio del Documento por referencia
            'Me.txtFechaI.Text = Me.txtFechaEmision.Text
            'Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicioTraslado.Text = Me.txtFechaEmision.Text

            'actualizarOpcionesBusquedaDocRef(0, False)

            'If (Request.QueryString("IdDocumentoRef") = Nothing) Then
            '    Me.hddOpcionBusquedaPersona.Value = "0" '************* REMITENTE
            '    Me.hddOpcionBusquedaAgente.Value = "0" '*******agente
            '    cargarPersona(CInt(Me.cboEmpresa.SelectedValue))

            'End If

            Me.hddNroFilasxDocumento.Value = CStr((New Negocio.DocumentoView).SelectCantidadFilasDocumento(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))

            'verFrm(FrmModo.Nuevo, False, True, True, True, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub


    Private Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRef.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdDestinatario.Value.Trim.Length > 0 And IsNumeric(Me.hddIdDestinatario.Value)) Then
                If (CInt(Me.hddIdDestinatario.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdDestinatario.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            '***********Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, fechaInicio, fechafin, False)
            ' Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).DocumentoSelectDocxDespachar_V2(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), fechaInicio, fechafin, serie, codigo, IdPersona, CInt(Me.hddIdTipoDocumento.Value), CInt(CboTipoDocumento.SelectedValue))

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).DocumentoSelectDocxDespachar_V2_Detalle(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), fechaInicio, fechafin, serie, codigo, IdPersona, CInt(Me.hddIdTipoDocumento.Value), CInt(Me.cboTipoOperacion.SelectedValue), CInt(CboTipoDocumento.SelectedValue))

            listaDoc_ref = lista
            'ViewState("lista_documentos") = lista
            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            'If (CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdRemitente.Value)) Then
            '    actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)
            'End If

            'GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged
        Dim obj_dropDownList As New Negocio.LNValorizadoCajas
        Dim indicefila As Integer = Me.GV_DocumentosReferencia_Find.SelectedIndex
        Dim lista As List(Of Entidades.DocumentoView) = DirectCast(listaDoc_ref, List(Of Entidades.DocumentoView))
        Dim hidden_id As HiddenField = DirectCast(GV_DocumentosReferencia_Find.Rows(indicefila).FindControl("hddIdDocumentoReferencia_Find"), HiddenField)
        Dim listaFiltro As List(Of Entidades.DocumentoView) = (From row In lista
                                                               Where row.Id = hidden_id.Value
                                                               Select row).ToList()
        Me.GV_DocumentoRef.DataSource = listaFiltro
        Me.GV_Detalle.DataSource = obj_dropDownList.LN_ReturnDataTable(hidden_id.Value, "SOLICITUD_DETALLE_DOC")
        Me.GV_DocumentoRef.DataBind()
        Me.GV_Detalle.DataBind()
    End Sub

    Private Sub cargarDetalle(ByVal id As Integer)

    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanging
        Dim indicefila As Integer = Me.GV_DocumentosReferencia_Find.SelectedIndex
        
        'cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value) _
        ', CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdEmpresa_Find"), HiddenField).Value) _
        ', CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdAlmacen_Find"), HiddenField).Value) _
        ', CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdPersona_Find"), HiddenField).Value))
    End Sub

    'Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
    '    Dim ddl As DropDownList = DirectCast(e.Row.FindControl("ddlUmPrincipal"), DropDownList)
    '    Dim datakey As DataKey = GV_Detalle.DataKeys(e.Row.RowIndex)

    'End Sub
End Class
﻿Imports Entidades
Imports Negocio
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Reflection
Imports System.Linq
Imports System.IO

Public Class frm_VistaDespacho_Zona
    Inherits System.Web.UI.Page
    Private objNegocio As New Negocio.LN_Despachos
    Private objScript As New ScriptManagerClass
    Private objDropDownList As New Negocio.LNValorizadoCajas
    Private _cantidadDespachada As Decimal = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.btnimprimir)
        If Not IsPostBack Then
            ViewState("Filtro") = "TODO"
            ViewState("Filtro_vuelta") = "TODO"
            Me.txtFechaFiltro.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            Dim listaObjetos As New List(Of Object)
            listaObjetos = (New Negocio.blpacking).EventoLoad(Val(Me.ddlSector.SelectedValue), Me.checkFiltroTransporte.Checked, CDate(Me.txtFechaFiltro.Text), _
                                                              Val(Me.txtCodigoTransportista.Text), ddlTurno.SelectedValue)

            Dim listaSectores As New List(Of Entidades.be_Sector)
            listaSectores = listaObjetos(0)
            cargarSectores(listaSectores)

            Dim listaAgencias As New List(Of Entidades.be_transportista)
            listaAgencias = listaObjetos(1)
            CargarAgenciasTransporte(listaAgencias)

            Dim listaPicking As New List(Of Entidades.be_picking)
            listaPicking = listaObjetos(2)
            'cargarDespachos_generico(listaPicking) 'vista para todos

            Dim Placas As New List(Of Entidades.Vehiculo)
            Placas = listaObjetos(3)
            Session("placas") = Placas
            'listaPlacas(Placas)

            'BuscarPicking()            
        End If
    End Sub

    Public Shared Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
        Dim dt As New DataTable()
        Dim propiedades As PropertyInfo() = GetType(T).GetProperties
        For Each p As PropertyInfo In propiedades
            dt.Columns.Add(p.Name, p.PropertyType)
        Next
        For Each item As T In list
            Dim row As DataRow = dt.NewRow
            For Each p As PropertyInfo In propiedades
                row(p.Name) = p.GetValue(item, Nothing)
            Next
            dt.Rows.Add(row)
        Next
        Return dt
    End Function

    Private Sub cargarSectores(ByVal lista As List(Of Entidades.be_Sector))
        Me.ddlSector.DataSource = lista
        Me.ddlSector.DataValueField = "idSector"
        Me.ddlSector.DataTextField = "nomSector"
        Me.ddlSector.DataBind()
        Me.ddlSector.Items.Insert(0, New ListItem("Seleccione un Sector", "-1"))
    End Sub

    Private Sub crearTextboxDinamicos(ByVal e As GridViewRowEventArgs)
        Dim datakey As DataKey = gv_Zonas_Despachos.DataKeys(e.Row.RowIndex)
        Dim idProducto As Integer = datakey(3)
        Dim checkBoxxList As CheckBoxList = TryCast(e.Row.FindControl("chklTonos"), CheckBoxList)
        Dim placeHolderDisponible As PlaceHolder = TryCast(e.Row.FindControl("phDisponible"), PlaceHolder)
        Dim placeHolderDespachado As PlaceHolder = TryCast(e.Row.FindControl("phDespachado"), PlaceHolder)
        Dim dt As New DataTable
        dt = objDropDownList.LN_ReturnDataTable(idProducto, "LISTAR_TONO_X_PRODUCTO_DESPACHOS")
        checkBoxxList.DataSource = dt
        checkBoxxList.DataValueField = "ID_REL_TONO_PROD"
        checkBoxxList.DataTextField = "NOM_TONO"
        checkBoxxList.DataBind()
        placeHolderDisponible.Controls.Add(New LiteralControl("<table cellspacing='8px'>"))
        For i As Integer = 0 To checkBoxxList.Items.Count - 1
            Dim labelDisponible As New Label
            labelDisponible.ID = "lblDisponible_" & i
            labelDisponible.ForeColor = System.Drawing.Color.Aqua.Red
            labelDisponible.Text = IIf(dt.Rows(i).Item(2).ToString() = "", 0, dt.Rows(i).Item(2).ToString())
            placeHolderDisponible.Controls.Add(New LiteralControl("<tr><td>"))
            placeHolderDisponible.Controls.Add(labelDisponible)
            placeHolderDisponible.Controls.Add(New LiteralControl("</td></tr>"))

        Next
        placeHolderDisponible.Controls.Add(New LiteralControl("</table>"))
    End Sub

    Private Function BuscarControl(ByVal pform As Control, ByVal pControlPadre As Control, ByVal pControlNombre As String) As Control
        If pControlPadre.ID = pControlNombre Then
            Return pControlPadre
        End If

        For Each subcontrol As Control In pControlPadre.Controls
            Dim resultado As Control = BuscarControl(pform, subcontrol, pControlNombre)
            If Not IsNothing(resultado) Then
                Return resultado
            End If
        Next
        Return Nothing
    End Function

    Protected Sub filtroAtender_click(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = TryCast(sender, DropDownList)
        Dim filtro As String = ddl.SelectedItem.Text.ToString()
        Dim dt As New DataTable
        dt = ViewState("grilla")
        Dim dv As New DataView(dt)
        Select Case filtro
            Case "TODOS"
                dv.RowFilter = "flag_atendido in (1,0)"
            Case "ATENDIDO"
                dv.RowFilter = "flag_atendido = 1"
            Case "POR ATENDER"
                dv.RowFilter = "flag_atendido = 0"
        End Select
        Me.gv_Zonas_Despachos.DataSource = dv
        Me.gv_Zonas_Despachos.DataBind()
        '
        'Busco el dropdownlist en la cabecera de la grilla y le asigno el valor del último filtro seleccionado
        '
        Dim ddlFiltro As DropDownList = DirectCast(Me.gv_Zonas_Despachos.HeaderRow.FindControl("ddlFiltro"), DropDownList)
        ddlFiltro.Items.FindByValue(filtro.ToString()).Selected = True
    End Sub

    Protected Sub evento_cambiarVehiculo(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddlPlacaVehiculo As DropDownList = DirectCast(sender, DropDownList)
        ViewState("Filtro") = ddlPlacaVehiculo.SelectedValue
        Call llenaGrilla_filtrosPlaca(ddlPlacaVehiculo.SelectedItem.ToString())
    End Sub

    Private Sub llenaGrilla_filtrosPlaca(ByVal filtroPlaca As String)
        Dim lista As New List(Of Entidades.be_picking)
        lista = Session("picking")
        If filtroPlaca = "TODO" Then
            gv_Zonas_Despachos.DataSource = lista
            gv_Zonas_Despachos.DataBind()
        Else
            lista = (From item In lista
                Where item.veh_placa.Contains(filtroPlaca)
                Select item).ToList()
            gv_Zonas_Despachos.DataSource = lista
            gv_Zonas_Despachos.DataBind()
            Session("picking_filtro_placa") = lista
        End If

        Dim ddlPlacaVehiculo As DropDownList = DirectCast(gv_Zonas_Despachos.HeaderRow.FindControl("ddlPlacaVehiculo"), DropDownList)
        Dim Placas As New List(Of Entidades.Vehiculo)
        Placas = Session("placas")
        Session("picking_reporte") = lista
        Call listaPlacas(Placas)
    End Sub

    Private Sub listaPlacas(ByVal lista As List(Of Entidades.Vehiculo))
        Session("placas") = lista
        Dim ddlPlacaVehiculo As DropDownList = DirectCast(gv_Zonas_Despachos.HeaderRow.FindControl("ddlPlacaVehiculo"), DropDownList)
        ddlPlacaVehiculo.DataSource = lista
        ddlPlacaVehiculo.DataValueField = "Placa"
        ddlPlacaVehiculo.DataTextField = "Placa"
        ddlPlacaVehiculo.DataBind()
        ddlPlacaVehiculo.SelectedValue = ViewState("Filtro")        
    End Sub

    'Private Sub listarVueltas()
    '    Dim lista As New List(Of Entidades.be_picking)
    '    lista = Session("picking")
    '    Dim listaEntero As List(Of Integer)
    '    listaEntero = (From item In lista
    '           Select item.nroVuelta Distinct).ToList

    '    Dim ddlVueltas As DropDownList = DirectCast(gv_Zonas_Despachos.HeaderRow.FindControl("ddlVueltas"), DropDownList)
    '    For i As Integer = 0 To listaEntero.Count - 1
    '        ddlVueltas.Items.Add(New ListItem(listaEntero(i), listaEntero(i)))
    '    Next

    '    'ddlVueltas.DataBind()
    '    ddlVueltas.SelectedValue = ViewState("Filtro_vuelta")
    '    Session("vueltas") = listaEntero
    'End Sub

    Protected Sub evento_filtroVueltas(ByVal sender As Object, ByVal e As EventArgs)
        Dim lista As New List(Of Entidades.be_picking)
        Dim filtroVuelta As String = DirectCast(sender, DropDownList).SelectedValue
        lista = Session("picking_filtro_placa")
        If filtroVuelta = "TODO" Then
            gv_Zonas_Despachos.DataSource = lista
            gv_Zonas_Despachos.DataBind()
        Else
            lista = (From item In lista
                Where item.nroVuelta.ToString.Contains(filtroVuelta)
                Select item).ToList()
            gv_Zonas_Despachos.DataSource = lista
            gv_Zonas_Despachos.DataBind()
            'Session("picking_filtro_placa") = lista
        End If
        ViewState("Filtro_vuelta") = filtroVuelta
        Dim ddl As DropDownList = DirectCast(gv_Zonas_Despachos.HeaderRow.FindControl("ddlVueltas"), DropDownList)
        ddl.SelectedValue = ViewState("Filtro_vuelta")
        'Llenar placa y capturar valor
        Dim ddlPlacaVehiculo As DropDownList = DirectCast(gv_Zonas_Despachos.HeaderRow.FindControl("ddlPlacaVehiculo"), DropDownList)
        Dim Placas As New List(Of Entidades.Vehiculo)
        Placas = Session("placas")
        Session("picking_reporte") = lista
        Call listaPlacas(Placas)
    End Sub

    Private Sub GV_Transportista_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_Transportista.PageIndexChanging
        Me.GV_Transportista.PageIndex = e.NewPageIndex
        Dim ds As DataSet = Session("vistaTransportistas")
        Me.GV_Transportista.DataSource = ds.Tables(0)
        Me.GV_Transportista.DataBind()
    End Sub

    Private Sub GV_Transportista_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Transportista.SelectedIndexChanged
        Dim indice As Integer = GV_Transportista.SelectedIndex
        Dim datakey As DataKey = GV_Transportista.DataKeys(indice)
        Dim idPersona As Integer = datakey(0)
        Dim label As Label = TryCast(GV_Transportista.Rows(indice).FindControl("lblTransportista"), Label)
        Me.txtTransportista.Text = label.Text
        Me.txtCodigoTransportista.Text = idPersona
    End Sub

    Private Sub CargarAgenciasTransporte(ByVal lista As List(Of Entidades.be_transportista))
        Session("vistaTransportistas") = lista
        Me.GV_Transportista.DataSource = lista
        Me.GV_Transportista.DataBind()
    End Sub

    Private Sub btnBuscarPicking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPicking.Click
        BuscarPicking()
    End Sub

    Private Sub BuscarPicking()
        Dim listaObjetos As New List(Of Object)
        Dim listaPlacasssss As New List(Of Entidades.Vehiculo)
        listaPlacasssss = Session("placas")

        Dim lista As New List(Of Entidades.be_picking)
        listaObjetos = (New blpacking).EventoClickBusqueda(Val(Me.ddlSector.SelectedValue), Me.checkFiltroTransporte.Checked, CDate(Me.txtFechaFiltro.Text), _
                                                              Val(Me.txtCodigoTransportista.Text), ddlTurno.SelectedValue)
        lista = listaObjetos(0)
        Session("picking") = lista
        Session("picking_reporte") = lista
        Session("picking_filtro_placa") = lista
        Me.gv_Zonas_Despachos.DataSource = lista
        Me.gv_Zonas_Despachos.DataBind()
        listaPlacas(listaPlacasssss)
    End Sub

    Protected Sub ontextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim filtro As String = DirectCast(GV_Transportista.HeaderRow.FindControl("TxtFiltroTransportista"), TextBox).Text.ToUpper()
        Dim lista As New List(Of Entidades.be_transportista)
        lista = Session("vistaTransportistas")
        lista = (From item In lista
                 Where (item.transportista.Contains(filtro))
                 Select item).ToList()
        Me.GV_Transportista.DataSource = lista
        Me.GV_Transportista.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaTransporte');", True)
    End Sub

    Private Function getListaDetalleDocumento() As List(Of Entidades.be_picking)
        Return CType(Session.Item(CStr(ViewState("lista_tono"))), List(Of Entidades.be_picking))
    End Function

    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.be_picking))
        Session.Remove(CStr(ViewState("lista_tono")))
        Session.Add(CStr(ViewState("lista_tono")), lista)
    End Sub

    Private Sub cargarDespachos_generico(ByVal lista As List(Of Entidades.be_picking))
        Session("picking") = lista
        Me.gv_Zonas_Despachos.DataSource = lista
        Me.gv_Zonas_Despachos.DataBind()
    End Sub
    Public Shared Function ListaATabla(Of T)(ByVal lista As IList(Of T)) As DataTable
        Dim tabla As New DataTable
        'Crear la Estructura de la Tabla a partir de la Lista de Objetos
        Dim propiedades As PropertyInfo() = lista(0).[GetType]().GetProperties()
        For i As Integer = 0 To propiedades.Length - 1
            tabla.Columns.Add(propiedades(i).Name, propiedades(i).PropertyType)
        Next
        'Llenar la Tabla desde la Lista de Objetos
        Dim fila As DataRow = Nothing
        For i As Integer = 0 To lista.Count - 1
            propiedades = lista(i).[GetType]().GetProperties()
            fila = tabla.NewRow()
            For j As Integer = 0 To propiedades.Length - 1
                fila(j) = propiedades(j).GetValue(lista(i), Nothing)
            Next
            tabla.Rows.Add(fila)
        Next
        Return (tabla)
    End Function
    Private Sub btnimprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnimprimir.Click
        Dim reporte As New ReportDocument
        Dim tipoFormato As ExportFormatType
        Dim dt As New DataTable
        dt = ListaATabla(Session("picking_reporte"))
        reporte = New rpt_picking
        ' 
        reporte.SetDataSource(dt)
        reporte.SetParameterValue("@Sector", ddlSector.SelectedItem.Text)
        tipoFormato = ExportFormatType.PortableDocFormat
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        reporte.ExportToHttpResponse(tipoFormato, Response, True, "Picking")
        Response.End()
        'Dim Path As String = "prueba"
        'Dim FI As New FileInfo(Path)
        'Dim stringWriter As New StringWriter()
        'Dim htmlWrite As New HtmlTextWriter(stringWriter)
        'Dim DataGrd As New DataGrid()
        'DataGrd.DataSource = Session("picking")
        'DataGrd.DataBind()

        'DataGrd.RenderControl(htmlWrite)
        'Dim directory__1 As String = Path.Substring(0, Path.LastIndexOf("\"))
        ' GetDirectory(Path);
        'If Not Directory.Exists(directory__1) Then
        '    Directory.CreateDirectory(directory__1)
        'End If

        'Dim vw As New System.IO.StreamWriter(Path, True)
        'stringWriter.ToString().Normalize()
        'vw.Write(stringWriter.ToString())
        'vw.Flush()
        'vw.Close()
        'WriteAttachment(FI.Name, "application/vnd.ms-excel", stringWriter.ToString())
        '=======================================
        'Response.ClearContent()
        'Response.AddHeader("content-dispotition", "attachment;filename=gvExcel.xls")
        'Response.ContentType = "application/excel"
        'Dim sw As System.IO.StringWriter = New System.IO.StringWriter()
        'Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        'Me.gv_Zonas_Despachos.RenderControl(htw)
        'Response.Write(sw.ToString())
        'Response.End()
    End Sub
End Class
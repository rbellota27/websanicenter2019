﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmGenerarDespachoAutomatico
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
        End If
    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo
            With objCbo

                .llenarCboAlmacen(Me.cboAlmacen, False)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento_OD.Value), False)

            End With

            Me.txtFecha_Inicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFecha_Fin.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        generarDespacho()
    End Sub

    Private Sub generarDespacho()
        Try

            '*********** GENERACIÓN MASIVA
            If ((New Negocio.DocOrdenDespacho).DocumentoOrdenDespacho_GenerarDespachoAutomaticoxParams(CInt(Me.cboAlmacen.SelectedValue), CDate(Me.txtFecha_Inicio.Text), CDate(Me.txtFecha_Fin.Text), CInt(Session("IdUsuario")), CInt(Me.hddIdTipoDocumento_OD.Value), CInt(Me.cboTipoOperacion.SelectedValue), 0)) Then
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else
                Throw New Exception("Problemas en la Operación.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
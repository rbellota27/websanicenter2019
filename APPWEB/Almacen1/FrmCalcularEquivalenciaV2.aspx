<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmCalcularEquivalenciaV2.aspx.vb" Inherits="APPWEB.FrmCalcularEquivalenciaV2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda">
                C�LCULO DE EQUIVALENCIAS
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:ImageButton ID="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                     onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                    onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" OnClientClick="return(valOnClick_btnBuscarProducto());" />                   
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_Producto" runat="server" AutoGenerateColumns="False" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="C�digo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion" ItemStyle-HorizontalAlign="Center" HeaderText="Descripci�n"
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        <asp:BoundField DataField="Linea" ItemStyle-HorizontalAlign="Center" HeaderText="L�nea"
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        <asp:BoundField DataField="SubLinea" ItemStyle-HorizontalAlign="Center" HeaderText="Sub L�nea"
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto">
                            Ingrese Cantidad:
                        </td>
                        <td>
                            <asp:TextBox ID="txtCantidad_Ingreso" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                onFocus=" return(  aceptarFoco(this)  ); " Width="100px" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboUnidadMedida_Ingreso" runat="server" DataTextField="NombreCortoUM"
                                DataValueField="IdUnidadMedida">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnCalcular" OnClientClick=" return(  valOnClick_btnCalcular()   ); "
                                runat="server" Text="Calcular" ToolTip="Calcular equivalencias" Width="80px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Resultado:
                        </td>
                        <td>
                            <asp:TextBox ID="txtCantidad_Salida" Font-Bold="true" CssClass="TextBox_ReadOnlyLeft"
                                Text="" ReadOnly="true" Width="100px" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboUnidadMedida_Salida" runat="server" DataTextField="NombreCortoUM"
                                DataValueField="IdUnidadMedida">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="GV_CalcularEquivalencia" runat="server" AutoGenerateColumns="False"
                    Width="400px">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chb_UnidadMedida" runat="server" Checked="true" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unidad Medida" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblUnidadMedida" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblUnidadMedidaAbv" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NombreCortoUM")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="GV_ResuldoEQ" runat="server" AutoGenerateColumns="False" Width="400px">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedida" HeaderText=""
                            ItemStyle-Height="25px" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" />
                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedidaAbv" HeaderText="U.M."
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="Cantidad" HeaderText="Cantidad"
                            DataFormatString="{0:F4}" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" />
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:RadioButtonList ID="rbl_UtilizarRedondeo" runat="server" RepeatDirection="Horizontal"
                    CssClass="Label">
                    <asp:ListItem Selected="True" Value="0">No Utilizar Redondeo</asp:ListItem>
                    <asp:ListItem Value="1">Redondeo [ arriba ]</asp:ListItem>
                    <asp:ListItem Value="2">Redondeo [ abajo ]</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdProducto" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 70px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    AutoPostBack="true" DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="text-align: right">
                                            C�d.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"
                                                TabIndex="205"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='/Imagenes/Buscar_b.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="85px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:TemplateField HeaderText="C�digo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                            <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        $(document).ready(function () {
            $('#Button1').click(function () {
                alert("mensaje");
            });
        });
    </script>
    <script language="javascript" type="text/javascript">
        function valKeyPressCodigoSL(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }


        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        function valOnClick_btnCalcular() {
            var hddIdProducto = document.getElementById('<%=hddIdProducto.ClientID %>');
            if (isNaN(parseFloat(hddIdProducto.value)) || hddIdProducto.value.length <= 0) {
                alert('DEBE SELECCIONAR UN PRODUCTO.');
                return false;
            }
            var cboUnidadMedida_Ingreso = document.getElementById('<%=cboUnidadMedida_Ingreso.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Ingreso.value)) || cboUnidadMedida_Ingreso.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE INGRESO.');
                return false;
            }
            var cboUnidadMedida_Salida = document.getElementById('<%=cboUnidadMedida_Salida.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Salida.value)) || cboUnidadMedida_Salida.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE SALIDA.');
                return false;
            }                        
            var txtCantidad = document.getElementById('<%=txtCantidad_Ingreso.ClientID %>');
            if (isNaN(parseFloat(txtCantidad.value)) || txtCantidad.value.length <= 0) {
                alert('INGRESE UN VALOR V�LIDO.');
                txtCantidad.select();
                txtCantidad.focus();
                return false;
            }            
            return true;
        }
   
    </script>

</asp:Content>

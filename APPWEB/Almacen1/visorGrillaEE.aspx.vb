﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
Partial Public Class visorGrillaEE
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim nro As Integer = CInt(Session("nro"))
        Dim NomCol() As String = CType(Session("nomcol"), String())
        llenarGrillaProd_E(gvwExportaExcel, nro, NomCol)

        Me.grvTitulo.DataSource = addRowGvTitulo()
        grvTitulo.DataBind()

        Me.gvwExportaExcel.DataSource = Session("grillaPedAprob")
        gvwExportaExcel.DataBind()
        prepararParaExportar(gvwExportaExcel)




    End Sub
    Private Sub prepararParaExportar(ByVal grilla As GridView)
        For Each row As GridViewRow In gvwExportaExcel.Rows
            'total Sublinea
            If row.Cells(0).Text = "999999" Then
                row.BackColor = Drawing.Color.SkyBlue
                row.Cells(0).Text = ""
            End If
        Next
        Dim Nombre As String
        Nombre = "ComparativoAnualdeCompras.xls"
        ExportGrid(gvwExportaExcel, "Ventas por Empresa y Stock por Almacen")
    End Sub
    Public Sub llenarGrillaProd_E(ByVal objGridView As GridView, ByVal NroCol As Integer, ByVal NombreCol As String())
        objGridView.Columns.Clear()

        For x As Integer = 0 To NroCol - 2
            agregarColumnaGrilla(objGridView, NombreCol(x), True, NombreCol(x))
        Next
    End Sub
    Private Sub agregarColumnaGrilla(ByVal objGridView As GridView, ByVal headerText As String, ByVal isVisible As Boolean, ByVal dataField As String)
        Dim column As New BoundField
        column.HeaderText = headerText
        column.Visible = isVisible
        column.DataField = dataField
        objGridView.Columns.Add(column)
    End Sub
    Private Sub ExportGrid(ByVal grilla As GridView, ByVal FilleNameExt As String)

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As System.IO.StringWriter = New System.IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form As HtmlForm = New HtmlForm
        grilla.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(grilla)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=data.xls")
        Response.Charset = "UTF-8"

        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()


        'Response.Clear()
        'Response.Buffer = True
        'Response.AddHeader("content-disposition", "attachment;filename=VentasyStock.xls")
        'Response.Charset = ""
        'Response.ContentType = "application/vnd.ms-excel"

        'Response.ContentEncoding = System.Text.Encoding.UTF8

        'Dim strWriter As System.IO.StringWriter
        'strWriter = New System.IO.StringWriter
        'Dim htmlTextWriter As System.Web.UI.HtmlTextWriter
        'htmlTextWriter = New System.Web.UI.HtmlTextWriter(strWriter)

        'gvwExportaExcel.RenderControl(htmlTextWriter)

        'EnableViewState = False

        'Response.Write(strWriter.ToString)
        'gvwExportaExcel.Dispose()

        'Response.End()



    End Sub

    Public Function addRowGvTitulo() As DataTable
        Dim tabla As New DataTable
        Dim tb As New DataTable
        tb.Columns.Add("Titulo", GetType(System.String))
        tb.Columns.Add("Titulo2", GetType(System.String))
        tb.Columns.Add("Titulo3", GetType(System.String))

        Dim dr As DataRow = tb.NewRow()
        dr(0) = "Ventas"
        dr(1) = "Stock"
        dr(2) = "Por Almacen"
        tb.Rows.Add(dr)
        Return tb

    End Function

End Class
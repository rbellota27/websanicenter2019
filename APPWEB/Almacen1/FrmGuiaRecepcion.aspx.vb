﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmGuiaRecepcion
    Inherits System.Web.UI.Page


    Private objScript As New ScriptManagerClass
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaProductoView As List(Of Entidades.ProductoView)

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Dim valorx As Integer = 0


    Public ReadOnly Property Cantidadp() As Decimal
        Get
            Return CType(Session.Item("SCantidad"), Decimal)
        End Get
    End Property

#Region "************************ MANEJO DE SESSION"
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item(CStr(ViewState("DetalleDocumento"))), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove(CStr(ViewState("DetalleDocumento")))
        Session.Add(CStr(ViewState("DetalleDocumento")), lista)
    End Sub
    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item(CStr(ViewState("listaDocumentoRef"))), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(CStr(ViewState("listaDocumentoRef")))
        Session.Add(CStr(ViewState("listaDocumentoRef")), lista)
    End Sub

    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("DetalleDocumento", "DetalleDocumentoGR" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaDocumentoRef", "listaDocumentoRefGR" + Aleatorio.Next(0, 100000).ToString)

    End Sub

#End Region


    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
        Documento_EnProceso = 7
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then

            onLoad_Session()

            ConfigurarDatos()
            ValidarPermisos()
            inicializarFrm()
            If (Request.QueryString("IdDocumentoRef") <> Nothing) Then
                cargarDocumentoReferencia(CInt(Request.QueryString("IdDocumentoRef")))
            End If

        End If
    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / MODIFICAR OPCIÓN MOVER STOCK FÍSICO
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {54, 55, 56, 57, 58, 146})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True

        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False

        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If (listaPermisos(5) > 0) Then
            Me.chb_MoverAlmacen.Enabled = True
        Else
            Me.chb_MoverAlmacen.Enabled = False
        End If

    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)

                '************** PUNTO PARTIDA
                .LLenarCboDepartamento(Me.cboDepto_Partida)
                .LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
                .LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))

                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)
                '---lineas---

                .llenarCboTipoExistencia(CboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(hddIdTipoDocumento.Value), 2, False)
                

            End With
            'CargarTD(CInt(CboTipoDocumento.SelectedValue))
            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text



            actualizarOpcionesBusquedaDocRef(1, False)

            '***************** NRO FILAS X DOCUMENTO
            Me.hddNroFilasxDocumento.Value = CStr((New Negocio.DocumentoView).SelectCantidadFilasDocumento(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))

            verFrm(FrmModo.Nuevo, False, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove(CStr(ViewState("DetalleDocumento")))
            Session.Remove(CStr(ViewState("listaDocumentoRef")))
        End If

        If (initSession) Then

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})

        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False


                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False



                Me.btnBuscarDocumentoRef.Visible = True


                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Remitente.Enabled = True
                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboAlmacen.Enabled = True
                Me.btnBuscarRemitente.Enabled = True
                Me.btnBuscarProducto.Enabled = True
                Me.btnLimpiarDetalleDocumento.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Remitente.Enabled = True
                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False



                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True


                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False


                Me.btnBuscarDocumentoRef.Visible = False

                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False


                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub


    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto

        '************** DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        '************ REMITENTE
        Me.txtNombre_Remitente.Text = ""
        Me.txtIdRemitente.Text = ""
        Me.txtDNI_Remitente.Text = ""
        Me.txtRUC_Remitente.Text = ""

        '*********** PUNTO PARTIDA
        Me.cboDepto_Partida.SelectedIndex = 0
        Me.cboProvincia_Partida.SelectedIndex = 0
        Me.cboDistrito_Partida.SelectedIndex = 0
        Me.txtDireccion_Partida.Text = ""
        Me.cboTipoOperacion.SelectedIndex = 0
        Me.cboTipoOperacion.SelectedValue = "0"
        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()


        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIdRemitente.Value = ""
        Me.hddIndex_GV_Detalle.Value = ""
        Me.hddIndexGlosa_Gv_Detalle.Value = ""

        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
        Me.gvBuscar.DataSource = Nothing
        Me.gvBuscar.DataBind()

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)
        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub


    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer, ByVal cargarPuntoPartida As Boolean)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        Dim objCbo As New Combo
        Me.txtNombre_Remitente.Text = objPersona.Descripcion
        Me.txtIdRemitente.Text = CStr(objPersona.IdPersona)
        Me.txtDNI_Remitente.Text = objPersona.Dni
        Me.txtRUC_Remitente.Text = objPersona.Ruc
        Me.hddIdRemitente.Value = CStr(objPersona.IdPersona)

        If (cargarPuntoPartida) Then
            '**************** PARTIDA
            Dim ubigeo As String = objPersona.Ubigeo
            If (ubigeo.Trim.Length <= 0) Then
                ubigeo = "000000"
            End If

            Dim codDepto As String = ubigeo.Substring(0, 2)
            Dim codProv As String = ubigeo.Substring(2, 2)
            Dim codDist As String = ubigeo.Substring(4, 2)
            Me.cboDepto_Partida.SelectedValue = codDepto
            objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, codDepto)
            Me.cboProvincia_Partida.SelectedValue = codProv
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, codDepto, codProv)
            Me.cboDistrito_Partida.SelectedValue = codDist
            Me.txtDireccion_Partida.Text = objPersona.Direccion
        End If

        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region


#Region "************************** BUSQUEDA PRODUCTO"

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacen.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia.SelectedValue)
            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                          ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                          ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, _
                                          ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)

            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, 0, 0, (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

    Private Sub addProducto_DetalleDocumento()
        Try
            GV_ComponenteKit.DataBind()

            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaDetalleDocumento()

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

                Dim cantidad As Decimal = CDec(CType(Me.DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)

                If (cantidad > 0) Then

                    Dim IdProducto_Find As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto_Find"), HiddenField).Value)
                    Dim idSector As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdSector"), HiddenField).Value)
                    Dim IdUnidadMedida_Find As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
                    Dim Kit_Find As Boolean = False : Try : Kit_Find = CBool(CType(Me.DGV_AddProd.Rows(i).FindControl("hddKit"), HiddenField).Value) : Catch ex As Exception : End Try

                    Dim valCant As Boolean = False
                    For k As Integer = 0 To listaDetalleDocumento.Count - 1

                        If listaDetalleDocumento(k).IdProducto = IdProducto_Find And listaDetalleDocumento(k).IdUnidadMedida = IdUnidadMedida_Find Then

                            listaDetalleDocumento(k).Cantidad = listaDetalleDocumento(k).Cantidad + cantidad
                            listaDetalleDocumento(k).Kit = Kit_Find
                          
                            valCant = True
                            Exit For

                        End If

                    Next

                    If valCant = False Then
                        Dim objDetalle As New Entidades.DetalleDocumento
                        With objDetalle                        
                            .listaSector = (New Negocio.bl_Sector).bl_Sector()
                            .IdAlmacen = cboAlmacen.SelectedValue
                            .idSector = idSector
                            .IdProducto = IdProducto_Find
                            .listaTonos = (New Negocio.bl_Tonos).SelectTonosxProducto(.IdProducto, .IdDocumento, .IdUnidadMedida)
                            .NomProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(2).Text))
                            .Cantidad = cantidad
                            .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                            .IdUnidadMedida = IdUnidadMedida_Find
                            .CodigoProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(1).Text))

                        End With
                        Me.listaDetalleDocumento.Add(objDetalle)
                    End If

                    If Kit_Find Then ' AGREGANDO COMPONENTES

                        Dim ListaKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponentexIdKit(IdProducto_Find)

                        For k As Integer = 0 To ListaKit.Count - 1


                            Dim valCant2 As Boolean = False
                            For z As Integer = 0 To listaDetalleDocumento.Count - 1
                                If listaDetalleDocumento(z).IdProducto = ListaKit(k).IdComponente And listaDetalleDocumento(z).IdUnidadMedida = ListaKit(k).IdUnidadMedida_Comp Then
                                    listaDetalleDocumento(z).Cantidad = listaDetalleDocumento(z).Cantidad + cantidad
                                    'listaDetalleDocumento(z).listaTonos = (New Negocio.bl_Tonos).SelectTonosxProducto(listaDetalleDocumento(z).IdProducto, listaDetalleDocumento(z).IdDocumento, listaDetalleDocumento(z).IdUnidadMedida)
                                    valCant2 = True
                                    Exit For
                                End If
                            Next
                            If valCant2 = False Then

                                Dim objDetalle2 As New Entidades.DetalleDocumento
                                With objDetalle2

                                    .IdUnidadMedidaPrincipal = ListaKit(k).IdUnidadMedida_Comp
                                    .IdProducto = ListaKit(k).IdComponente
                                    .NomProducto = ListaKit(k).Componente
                                    .Cantidad = ListaKit(k).Cantidad_Comp * cantidad
                                    .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                                    .listaTonos = (New Negocio.bl_Tonos).SelectTonosxProducto(.IdProducto, .IdDocumento, .IdUnidadMedida)
                                    .IdUnidadMedida = ListaKit(k).IdUnidadMedida_Comp
                                    .CodigoProducto = ListaKit(k).CodigoProd_Comp

                                End With

                                Me.listaDetalleDocumento.Add(objDetalle2)
                            End If
                        Next

                    End If ' KIT

                End If ' CANTIDAD

            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  onCapa('capaBuscarProducto_AddProd');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub ActualizarListaDetalleDocumento(Optional ByVal Index As Integer = -1)
        Dim blSector As New Negocio.bl_Sector
        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            If Index = i Then
                Me.listaDetalleDocumento(i).IdUMold = Me.listaDetalleDocumento(i).IdUnidadMedida
            End If

            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedItem.ToString)
            Me.listaDetalleDocumento(i).idSector = CInt(IIf(CType(Me.GV_Detalle.Rows(i).FindControl("chkSectores"), RadioButtonList).SelectedValue = "", 0, CType(Me.GV_Detalle.Rows(i).FindControl("chkSectores"), RadioButtonList).SelectedValue))
        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub
    Dim objDrop As New Negocio.LNValorizadoCajas

    Dim CantidadReq As Decimal
    Private Sub GV_Detalle_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_Detalle.RowCommand
        If e.CommandName = "btnTono" Then
            Dim indice As Integer = e.CommandArgument            
            Dim RadioButtonList As RadioButtonList = TryCast(Me.GV_Detalle.Rows(indice).FindControl("chkSectores"), RadioButtonList)
            Dim valorRadio As String = RadioButtonList.SelectedValue
            '
            'Asigno el id del detalle a una hidden
            '
            Me.idCapasignarTono.Value = indice

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            If valorRadio = "" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "alert('Debe elegir un sector');", True)
            Else

                '
                'obtengo la valor actualizado de la nueva cantidad
                '
                Dim cantidad As Decimal = 0
                cantidad = CDec(TryCast(Me.GV_Detalle.Rows(indice).FindControl("txtCantidad"), TextBox).Text)
                Me.listaDetalleDocumento(indice).Cantidad = cantidad
                Me.listaDetalleDocumento(indice).idSector = valorRadio


                'Me.ddltono.DataSource = Me.listaDetalleDocumento(indice).listaTonos

                Me.ddltono.DataSource = objDrop.LN_ReturnDataTableTonos(Me.listaDetalleDocumento(indice).IdProducto, "LISTAR_TONO_X_PRODUCTO", listaDetalleDocumento(indice).Cantidad)

                ' CDec(Session("SCantidad")) = (listaDetalleDocumento(indice).Cantidad)

                Session("SCantidad") = (listaDetalleDocumento(indice).Cantidad)

                'Me.ddltono.DataTextField = "NOM_TONO"
                'Me.ddltono.DataValueField = "ID_REL_TONO_PROD"

                Me.ddltono.DataBind()
                '===============
                Dim list As New List(Of Entidades.be_tonoXProducto)
                list = Me.listaDetalleDocumento(indice).listaTonos

                If list.Count = 1 Then
                    For i As Integer = 0 To list.Count - 1
                        list(i).cantidadTono = Me.listaDetalleDocumento(indice).Cantidad
                    Next
                End If

                Me.gvTono.DataSource = list
                Me.gvTono.DataBind()
                setListaDetalleDocumento(Me.listaDetalleDocumento)
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');", True)
            End If
        End If

    End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                'qwe
                Dim list As New List(Of Entidades.be_tonoXProducto)
                Dim listLength As Integer = 0

                Dim btnTonos As ImageButton = CType(e.Row.FindControl("btnTonos"), ImageButton)
                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                Dim radioButtonList As New RadioButtonList
                radioButtonList = TryCast(e.Row.FindControl("chkSectores"), RadioButtonList)
                radioButtonList.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).idSector
                cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString

                If Me.listaDetalleDocumento(e.Row.RowIndex).Kit Then

                    Dim btnMostrarComponenteKit As ImageButton = CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton)
                    btnMostrarComponenteKit.Visible = True
                End If

                'Lista de tonos
                list = Me.listaDetalleDocumento(e.Row.RowIndex).listaTonos
                listLength = list.Count
                Dim totalCantidad As Decimal = 0
                'If listLength <> 0 Then
                For i As Integer = 0 To listLength - 1
                    totalCantidad += list(i).cantidadTono
                Next
                'End If
                '------------------------------------------------

                '------------------------------------------------
                setListaDetalleDocumento(Me.listaDetalleDocumento)

                If totalCantidad = Me.listaDetalleDocumento(e.Row.RowIndex).Cantidad Then
                    btnTonos.ImageUrl = "~/Imagenes/A.jpg"
                    btnTonos.ToolTip = "Distribución Correcta."                    
                ElseIf totalCantidad > Me.listaDetalleDocumento(e.Row.RowIndex).Cantidad Or totalCantidad < Me.listaDetalleDocumento(e.Row.RowIndex).Cantidad Then
                    btnTonos.ImageUrl = "~/Imagenes/B.jpg"
                    btnTonos.ToolTip = "Distribución no configurada."
                End If
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnMostrarComponenteKit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Me.ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim ListaKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponentexIdKit(Me.listaDetalleDocumento(index).IdProducto)

            Me.GV_ComponenteKit.DataSource = ListaKit
            Me.GV_ComponenteKit.DataBind()

            Me.setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_ComponenteKit.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

   
    Protected Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        limpiarDetalleDocumento()
    End Sub
    Private Sub limpiarDetalleDocumento()
        Try

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaProductoView(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaProductoView(e.Row.RowIndex).IdUnidadMedida.ToString

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboDepto_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepto_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboProvincia_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As String = ""
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdRemitente.Value.Trim.Length > 0 And IsNumeric(Me.hddIdRemitente.Value)) Then
                If (CInt(Me.hddIdRemitente.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdRemitente.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CStr(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            '*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocGuiaRecepcion).DocumentoGuiaRecepcion_BuscarDocumentoRef2(CInt(Me.cboEmpresa.SelectedValue), 0, CInt(Me.cboAlmacen.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, fechaInicio, fechafin, CInt(CboTipoDocumento.SelectedValue), CInt(cboTipoOperacion.SelectedValue))

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")                
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_DocumentosReferencia_Find.PageIndexChanging
        GV_DocumentosReferencia_Find.PageIndex = e.NewPageIndex        
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged
        'Dim referencia As Integer
        'referencia = (CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub



    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function

    Private Function obtenerListaDetalleDocumento_Ref(ByVal IdDocumento As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento)
        'Dim objDocumentoRef As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)

        If IdTipoDocumento > 3 Then
            listaDetalle = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
        Else
            listaDetalle = (New Negocio.DetalleDocumento).DetalleDocumentoRefxGuiaRecep(IdDocumento)
        End If


        For i As Integer = (listaDetalle.Count - 1) To 0 Step -1

            If ((listaDetalle(i).IdTipoDocumento = 16 Or listaDetalle(i).IdTipoDocumento = 6)) Then  '**** SI EL DOCUMENTO DE REFERENCIA ES ORDEN DE COMPRA O ES UNA GUIA DE REMISION

                listaDetalle(i).Cantidad = listaDetalle(i).CantidadTransito
                listaDetalle(i).CantidadTransito = listaDetalle(i).CantidadTransito

            End If

            If (listaDetalle(i).Cantidad = 0) Then
                listaDetalle.RemoveAt(i)
            Else
                listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
                listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
                listaDetalle(i).CantxAtender = Nothing
                listaDetalle(i).CantidadTransito = listaDetalle(i).CantidadTransito

            End If

        Next

        Return listaDetalle

    End Function
    Private Sub cargarDocumentoRef_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento As List(Of Entidades.Documento), ByVal objRemitente As Entidades.PersonaView, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal cargarGuiaRecepcion As Boolean)

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboEmpresa.Items.FindByValue(CStr(objDocumento.IdEmpresa)) IsNot Nothing) Then
                Me.cboEmpresa.SelectedValue = CStr(objDocumento.IdEmpresa)
            End If

            If (Me.cboAlmacen.Items.FindByValue(CStr(objDocumento.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            End If

            If (cargarGuiaRecepcion) Then

                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
                Me.txtCodigoDocumento.Text = .Codigo

                If (.FechaEmision <> Nothing) Then
                    Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
                End If

                Me.hddIdDocumento.Value = CStr(.Id)
                Me.hddCodigoDocumento.Value = .Codigo

            End If

        End With

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '******************* REMITENTE
        If (objRemitente IsNot Nothing) Then

            Me.txtNombre_Remitente.Text = objRemitente.Descripcion
            Me.txtDNI_Remitente.Text = objRemitente.Dni
            Me.txtRUC_Remitente.Text = objRemitente.Ruc
            Me.txtIdRemitente.Text = CStr(objRemitente.IdPersona)
            Me.hddIdRemitente.Value = CStr(objRemitente.IdPersona)

            If (objPuntoPartida Is Nothing) Then
                '**************** PARTIDA
                Dim ubigeo As String = objRemitente.Ubigeo
                If (ubigeo.Trim.Length <= 0) Then
                    ubigeo = "000000"
                End If

                Dim codDepto As String = ubigeo.Substring(0, 2)
                Dim codProv As String = ubigeo.Substring(2, 2)
                Dim codDist As String = ubigeo.Substring(4, 2)
                Me.cboDepto_Partida.SelectedValue = codDepto
                objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, codDepto)
                Me.cboProvincia_Partida.SelectedValue = codProv
                objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, codDepto, codProv)
                Me.cboDistrito_Partida.SelectedValue = codDist
                Me.txtDireccion_Partida.Text = objRemitente.Direccion
            End If

        End If

        '****************** PUNTO PARTIDA
        If (objPuntoPartida IsNot Nothing) Then

            If (Me.cboDepto_Partida.Items.FindByValue(objPuntoPartida.Ubigeo.Substring(0, 2)) IsNot Nothing) Then
                Me.cboDepto_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(0, 2)
            End If

            objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, Me.cboDepto_Partida.SelectedValue)

            If (Me.cboProvincia_Partida.Items.FindByValue(objPuntoPartida.Ubigeo.Substring(2, 2)) IsNot Nothing) Then
                Me.cboProvincia_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(2, 2)
            End If

            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, Me.cboProvincia_Partida.SelectedValue)

            If (Me.cboDistrito_Partida.Items.FindByValue(objPuntoPartida.Ubigeo.Substring(4, 2)) IsNot Nothing) Then
                Me.cboDistrito_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Partida.Text = objPuntoPartida.Direccion

        End If

        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()


        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

        '**************** LISTA DOC REF
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            '*************** QUITAMOS EL DETALLE DEL DOCUMENTO DE REFERENCIA
            Dim i As Integer = (Me.listaDetalleDocumento.Count - 1)
            While i >= 0

                If (CInt(Me.listaDetalleDocumento(i).IdDocumento) = Me.listaDocumentoRef(index).Id) Then
                    Me.listaDetalleDocumento.RemoveAt(i)
                End If

                i = i - 1

            End While

            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region


    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        btnGuardar.Visible = False
        registrarDocumento()
        If (valorx <> 2) Then
            btnGuardar.Visible = True
        End If

    End Sub

    Private Sub validarFrm()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        If (Me.listaDetalleDocumento.Count > CInt(Me.hddNroFilasxDocumento.Value)) Then
            Throw New Exception("EL NÚMERO MÁXIMO DE FILAS PARA EL DOCUMENTO ES DE < " + CStr(Math.Round(CInt(Me.hddNroFilasxDocumento.Value), 0)) + " >. NO SE PERMITE LA OPERACIÓN.")
        End If

    End Sub

    Private Sub registrarDocumento()
        Try

            ActualizarListaDetalleDocumento()

            validarFrm()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            '****************** ACTUALIZAMOS COSTO DEL PRODUCTO
            actualizarCostoProducto()
            Me.listaDetalleDocumento = getListaDetalleDocumento()


            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim objPuntoPartida As Entidades.PuntoPartida = obtenerPuntoPartida()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Dim moverStockFisico As Boolean = Me.chb_MoverAlmacen.Checked

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo


                    For Each gvrowx As GridViewRow In GV_Detalle.Rows
                        Dim canttransito As HiddenField = DirectCast(gvrowx.FindControl("hddCanttransito"), HiddenField)
                        Dim canttotal As TextBox = DirectCast(gvrowx.FindControl("txtCantidad"), TextBox)

                        If (CDec(canttransito.Value) = 0) Then
                            valorx = 0
                            Exit For
                        ElseIf ((CDec(canttransito.Value)) < (CDec(canttotal.Text))) Then
                            valorx = 1
                            Exit For
                        Else
                            valorx = 2
                        End If
                    Next

                    For Each gvrowx As GridViewRow In GV_DocumentoRef.Rows
                        Dim valorNombreDoc As Label = DirectCast(gvrowx.FindControl("lblNomDocumento"), Label)

                        If (valorNombreDoc.Text = "F/V" Or valorNombreDoc.Text = "B/V" Or valorNombreDoc.Text = "OD" Or valorNombreDoc.Text = "F/V ELECTRONICA" _
                            Or valorNombreDoc.Text = "B/V ELECTRONICA") Then
                            valorx = 3
                        End If
                    Next

                    'objDocumento.Id = (New Negocio.DocGuiaRecepcion).registrarGuiaRecepcion(objDocumento, Me.listaDetalleDocumento, objObservaciones, objPuntoPartida, listaRelacionDocumento, 1, moverStockFisico)
                    'Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    'objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case FrmModo.Editar
                    Dim validacion As Integer = 0
                    Dim CantidadAntesUpdate As New HiddenField
                    Dim CantidadTotalTransitoDocRef As New HiddenField
                    Dim CantidadIngresada As New TextBox

                    For Each gv As GridViewRow In GV_Detalle.Rows
                        CantidadAntesUpdate = DirectCast(gv.FindControl("hddCantidadtot"), HiddenField)
                        CantidadTotalTransitoDocRef = DirectCast(gv.FindControl("hddCanttransito"), HiddenField)
                        CantidadIngresada = DirectCast(gv.FindControl("txtCantidad"), TextBox)
                        valorx = 9
                        If (CDec(CantidadIngresada.Text) > (CDec(CantidadAntesUpdate.Value) + CDec(CantidadTotalTransitoDocRef.Value))) Then
                            validacion = 0
                            Exit For
                        Else
                            validacion = 1
                        End If

                    Next

                    If (validacion = 0) Then
                        objScript.mostrarMsjAlerta(Me, "La cantidad ingresada supera la cantidad en transito del doc.de referencia. No procede la operación.")
                    Else
                        objDocumento.Id = (New Negocio.DocGuiaRecepcion).actualizarGuiaRecepcion(objDocumento, Me.listaDetalleDocumento, objObservaciones, objPuntoPartida, listaRelacionDocumento, 1, moverStockFisico)
                        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False)
                        objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")
                    End If
            End Select

            If (valorx = 0) Then
                objScript.mostrarMsjAlerta(Me, "Verifique que los productos cuenten con Cant.Transito")
            ElseIf (valorx = 1) Then
                objScript.mostrarMsjAlerta(Me, "Ingrese una cantidad menor o igual a la Cant. Transito disponible")
            ElseIf (valorx = 9) Then

            Else

                objDocumento.Id = (New Negocio.DocGuiaRecepcion).registrarGuiaRecepcion(objDocumento, Me.listaDetalleDocumento, objObservaciones, objPuntoPartida, listaRelacionDocumento, 1, moverStockFisico)
                Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                Dim codigo, descripcion As String
                codigo = "007"
                Dim obj_Proceso As New Negocio.OrdenCompra
                descripcion = obj_Proceso.DefinirProceso(codigo)
                objScript.mostrarMsjAlerta(Me, "Se realizo: " + descripcion)
                objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")
                verFrm(FrmModo.Documento_Save_Exito, False, False, False, False)
                'LimpiarCajas()

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
    'Private Sub LimpiarCajas()
    '    txtCodigo_BuscarDocRef.Text = ""
    '    txtCodigoDocumento.Text = ""
    '    txtCodigoProducto.Text = ""
    '    txtDescripcionProd_AddProd.Text = ""
    '    txtDireccion_Partida.Text = ""
    '    txtDNI_Remitente.Text = ""
    '    txtFechaEmision.Text = ""
    '    txtGlosa_CapaGlosa.Text = ""
    '    txtIdRemitente.Text = ""
    '    txtNombre_Remitente.Text = ""
    '    txtObservaciones.Text = ""
    '    txtPageIndex_Productos.Text = ""
    '    txtPageIndexGO_Productos.Text = ""
    '    txtRUC_Remitente.Text = ""
    '    txtSerie_BuscarDocRef.Text = ""
    '    txtFechaF.Text = ""
    '    txtFechaI.Text = ""
    '    txtFechaFin_DocRef.Text = ""
    '    txtFechaInicio_DocRef.Text = ""
    '    btnGuardar.Visible = False
    'End Sub
    Private Sub actualizarCostoProducto()

        Me.listaDetalleDocumento = getListaDetalleDocumento()
        Me.listaDocumentoRef = getlistaDocumentoRef()

        Dim IdEmpresa As Integer = CInt(Me.cboEmpresa.SelectedValue)
        Dim IdAlmacen As Integer = CInt(Me.cboAlmacen.SelectedValue)
        Dim Fecha As Date = CDate(Me.txtFechaEmision.Text)
        Dim IdDocumentoRef As Integer = 0
        If (Me.listaDocumentoRef.Count > 0) Then
            IdDocumentoRef = Me.listaDocumentoRef(0).Id
        End If

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            Me.listaDetalleDocumento(i).CostoMovIngreso = (New Negocio.DocGuiaRecepcion).DocumentoGuiaRecepcionSelectCostoProducto(Me.listaDetalleDocumento(i).IdProducto, IdEmpresa, IdAlmacen, Fecha, IdDocumentoRef)

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub

    'Private Sub validarDocumentosRef()

    '    Me.listaDocumentoRef = getlistaDocumentoRef()
    '    Dim IdDestinatario As Integer = CInt(Me.hddIdDestinatario.Value)
    '    Dim IdEmpresa As Integer = CInt(Me.cboEmpresa.SelectedValue)
    '    Dim IdAlmacen As Integer = CInt(Me.cboAlmacen.SelectedValue)

    '    For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

    '        If (Me.listaDocumentoRef(i).IdPersona <> IdDestinatario) Then Throw New Exception("EL DESTINATARIO DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON EL DESTINATARIO SELECCIONADO. NO SE PERMITE LA OPERACIÓN.")
    '        If (Me.listaDocumentoRef(i).IdEmpresa <> IdEmpresa) Then Throw New Exception("LA EMPRESA DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON LA EMPRESA SELECCIONADA. NO SE PERMITE LA OPERACIÓN.")
    '        If (Me.listaDocumentoRef(i).IdAlmacen <> IdAlmacen) Then Throw New Exception("EL ALMACÉN DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON EL ALMACÉN SELECCIONADO. NO SE PERMITE LA OPERACIÓN.")

    '    Next

    'End Sub

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)
        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

            End With

            lista.Add(objRelacionDocumento)

        Next

        Return lista

    End Function

    Private Function obtenerPuntoPartida() As Entidades.PuntoPartida

        Dim objPuntoPartida As Entidades.PuntoPartida = Nothing

        objPuntoPartida = New Entidades.PuntoPartida

        With objPuntoPartida

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .Direccion = Me.txtDireccion_Partida.Text.Trim
            .IdAlmacen = Nothing

            .Ubigeo = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue

        End With

        Return objPuntoPartida

    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEstadoEntrega = 2 '************* ENTREGADO

            .IdPersona = CInt(Me.hddIdRemitente.Value)
            .IdRemitente = CInt(Me.hddIdRemitente.Value)
            .IdDestinatario = CInt(Me.cboEmpresa.SelectedValue)

            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

            '************* VALORES ADICIONALES
            .FactorMov = 1 '************** Mov de Almacén

        End With

        Return objDocumento

    End Function
    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try
            'qwe
            ActualizarListaDetalleDocumento()

            '******************** INICIALIZAMOS EL FRM CUANDO NO EXISTE UN DOC. DE REFERENCIA
            If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
                verFrm(FrmModo.Nuevo, True, True, True, True)
            Else
                If (Not validar_AddDocumentoRef(IdDocumentoRef)) Then Throw New Exception("No se permite la Operación.")

            End If

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            'Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objDocumento As Entidades.Documento = (New Negocio.DocGuiaRecepcion).SelectxIdDocumentoGuiarecepcion(IdDocumentoRef)

            validar_AddDocumentoRef_TipoOrdenCompra(IdDocumentoRef, objDocumento.IdTipoDocumento)

            Dim objRemitente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)
            Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumento_Ref(objDocumento.Id, objDocumento.IdTipoDocumento))
            Dim listaDeTonos As New List(Of Entidades.be_tonoXProducto)
            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
                With listaDetalleDocumento(i)
                    .listaSector = (New Negocio.bl_Sector).bl_Sector()
                    listaDeTonos = (New Negocio.bl_Tonos).SelectTonosxProducto(listaDetalleDocumento(i).IdProducto, listaDetalleDocumento(i).IdDocumento, listaDetalleDocumento(i).IdUnidadMedida)
                    If listaDeTonos.Count = 0 Or listaDeTonos.Count > 1 Then                        
                        .listaTonos = New List(Of Entidades.be_tonoXProducto)
                    Else
                        .listaTonos=listaDeTonos
                    End If

                End With

                'Me.listaDetalleDocumento(i).listaTonos = (New Negocio.bl_Tonos).SelectTonosxProducto(listaDetalleDocumento(i).IdProducto, listaDetalleDocumento(i).IdDocumento, listaDetalleDocumento(i).IdUnidadMedida)
            Next
         
            '*************** GUARDAMOS EN SESSION
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** 
            cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objRemitente, objPuntoPartida, Me.listaDetalleDocumento, Nothing, False)


            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Sub validar_AddDocumentoRef_TipoOrdenCompra(ByVal IdDocumentoRef As Integer, ByVal IdTipoDocumento As Integer)
        If IdTipoDocumento = 16 Then 'Orden de Compra
            Dim PoseeAprobacion As Nullable(Of Integer)
            PoseeAprobacion = (New Negocio.OrdenCompra).Validar_Aprobacion_Documento(IdDocumentoRef)
            If PoseeAprobacion = 0 Then Throw New Exception("El Documento no Posee Aprobación")
            'Dim PoseeCosteoImportacion As Nullable(Of Integer)
            'PoseeCosteoImportacion = (New Negocio.OrdenCompra).Validar_CosteoImportacion(IdDocumentoRef)
            'If PoseeCosteoImportacion = 0 Then Throw New Exception("El Documento no posee costeo de importación")
        End If
    End Sub


#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdRemitente.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        cargarDocumentoGuiaRecepcion(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
    End Sub

    Private Sub cargarDocumentoGuiaRecepcion(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '**************** LIMPIAMOS EL FORMULARIO
        verFrm(FrmModo.Inicio, True, True, True, True)

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO
        Dim objDocumento As Entidades.Documento = Nothing

        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        Dim objRemitente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)
        Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)

        Me.listaDetalleDocumento = obtenerListaDetalleDocumento_GuiaRecepcion(objDocumento.Id)

        Me.listaDocumentoRef = (New Negocio.DocGuiaRemision).DocumentoGuiaRemisionSelectDocumentoRef(objDocumento.Id)

        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '*************** GUARDAMOS EN SESSION
        setListaDetalleDocumento(Me.listaDetalleDocumento)
        setlistaDocumentoRef(Me.listaDocumentoRef)

        '************** 
        cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objRemitente, objPuntoPartida, Me.listaDetalleDocumento, objObservaciones, True)
        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

    End Sub

    Private Function obtenerListaDetalleDocumento_GuiaRecepcion(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumentoValidacionTransito(IdDocumento) ''Anterior --SelectxIdDocumento--
        For i As Integer = 0 To listaDetalle.Count - 1
            With listaDetalle(i)
                .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
                .IdDocumento = .IdDocumentoRef '*********** COLOCAMOS EL DOCUMENTO REF
                .listaSector = (New Negocio.bl_Sector).bl_Sector()
                '.listaTonos = (New Negocio.bl_Tonos).SelectTonosxProductos_Editar_GuiaRecepcion(IdDocumento)
                .listaTonos = (New Negocio.bl_Tonos).SelectTonosxProductos_Editar(IdDocumento, .IdDetalleDocumento)
            End With
        Next
        Return listaDetalle
    End Function
#End Region
    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try
            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100009}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.hddIdTipoDocumento.Value)
            Select Case listaPermisos(0)
                Case 0
                    objScript.mostrarMsjAlerta(Me, "No está permitido editar el documento con fecha anterior a la de hoy")
                Case 1
                    verFrm(FrmModo.Editar, False, False, False, False)


                    Dim PoseeRegularizacion = (New Negocio.OrdenCompra).Validar_Regularizacion(hddIdDocumento.Value)
                    If PoseeRegularizacion > 0 Then
                        Throw New Exception("NO SE PUEDE EDITAR, EL DOCUMENTO YA CUENTA CON UNA REGULARIZACION")
                    End If

                    Me.listaDetalleDocumento = getListaDetalleDocumento()
                    Me.listaDocumentoRef = getlistaDocumentoRef()

                    Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                    Me.GV_Detalle.DataBind()

                    Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
                    Me.GV_DocumentoRef.DataBind()
            End Select



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnular.Click
        anularDocumentoGuiaRecepcion()
    End Sub
    Private Sub anularDocumentoGuiaRecepcion()
        Try
            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100008}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.hddIdTipoDocumento.Value)
            Select Case listaPermisos(0)
                Case 1

                    Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

                    If ((New Negocio.DocGuiaRecepcion).anularGuiaRecepcion(IdDocumento)) Then
                        objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                        verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False)
                    Else
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If

                Case 0

                    objScript.mostrarMsjAlerta(Me, "No procede [Anular] un documento de un mes anterior al actual.")

            End Select
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            cargarDocumentoGuiaRecepcion(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value.Trim), 0)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True)
    End Sub

#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub valOnChange_cboUnidadMedida(ByVal sender As Object, ByVal e As System.EventArgs)
        actualizarEquivalencia_Peso(CType(CType(sender, DropDownList).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Private Sub actualizarEquivalencia_Peso(ByVal index As Integer)
        Try

            ActualizarListaDetalleDocumento(index)

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            With (Me.listaDetalleDocumento(index))
                Dim NewCantidad As Decimal = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUMold, .IdUnidadMedida, .Cantidad)
                .Cantidad = NewCantidad
            End With

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalleDocumento(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Private Sub quitarDetalleDocumento(ByVal Index As Integer)

        Try
            GV_ComponenteKit.DataBind()
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            If Me.listaDetalleDocumento(Index).Kit Then
                Me.listaDetalleDocumento.RemoveAll(Function(k As Entidades.DetalleDocumento) k.IdKit = Me.listaDetalleDocumento(Index).IdProducto)
            End If

            If Me.listaDetalleDocumento(Index).IdKit > 0 Then
                Dim infoProducto As String = ""
                Dim obj As Entidades.DetalleDocumento = Me.listaDetalleDocumento.Find(Function(k As Entidades.DetalleDocumento) k.IdProducto = Me.listaDetalleDocumento(Index).IdKit)
                If Not obj Is Nothing Then
                    infoProducto = obj.CodigoProducto + " - " + obj.NomProducto
                End If

                Me.listaDocumentoRef = getlistaDocumentoRef()

                If IsNothing(Me.listaDocumentoRef) Then

                    For p As Integer = 0 To Me.listaDocumentoRef.Count - 1

                        If Me.listaDocumentoRef(p).IdTipoDocumento <> 16 Then

                            Throw New Exception("EL PRODUCTO [ " + Me.listaDetalleDocumento(Index).NomProducto + " ] \n ES UN COMPONENTE DEL PRODUCTO [ " + infoProducto + " ].")
                            Return

                        End If

                    Next

                End If

            End If



            Me.listaDetalleDocumento.RemoveAt(Index)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnBuscarGrilla_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGrilla_AddProducto.Click
        BuscarProductoCatalogo(0)
    End Sub

    Private Sub btnAddProductos_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProductos_AddProducto.Click
        addProducto_DetalleDocumento()
    End Sub

    Private Sub btnAgregarTono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarTono.Click
        If ddltono.Items.Count > 0 Then
            'Obtiene posición del detalle
            Dim i As Integer = idCapasignarTono.Value

            'Obtiene la lista de detalle
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim list As New List(Of Entidades.be_tonoXProducto)
            list = Me.listaDetalleDocumento(i).listaTonos
           
            Dim validacion As Boolean = False
            For j As Integer = 0 To list.Count - 1
                If Me.ddltono.SelectedValue = list(j).idTono Then
                    validacion = True
                    Exit For
                End If
            Next
            If validacion = False Then
                Dim objetoTono As New Entidades.be_tonoXProducto

                'Asigna valores del tono al objeto tono y actualiza el objeto existente
                objetoTono.idTono = Me.ddltono.SelectedValue
                objetoTono.nomtono = Me.ddltono.SelectedItem.Text
                objetoTono.cantidadTono = Session("SCantidad")
                list.Add(objetoTono)

                Me.gvTono.DataSource = list
                Me.gvTono.DataBind()
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');    ", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');    ", True)
            End If
        End If
    End Sub

    Private Sub gvTono_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTono.SelectedIndexChanged
        'Obtiene posición del detalle
        Dim i As Integer = idCapasignarTono.Value

        Dim indice As Integer = gvTono.SelectedIndex
        Me.listaDetalleDocumento = getListaDetalleDocumento()
        Dim idTono As Integer = CInt(Me.gvTono.Rows(indice).Cells(1).Text)
        Dim list As List(Of Entidades.be_tonoXProducto)

        list = listaDetalleDocumento(i).listaTonos
        list.RemoveAll(Function(prod As Entidades.be_tonoXProducto) prod.idTono.ToString().Contains(idTono.ToString()))
        Me.gvTono.DataSource = list
        Me.gvTono.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');    ", True)
    End Sub

    Private Sub imgButtonCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonCerrar.Click                            
        Dim indiceCapaAsignarTono As Integer = idCapasignarTono.Value
        Me.listaDetalleDocumento = getListaDetalleDocumento()
        Dim list As New List(Of Entidades.be_tonoXProducto)
        Dim objeto As New Entidades.be_tonoXProducto

        Dim cantidadTotal As Integer = 0
        For Each row As GridViewRow In Me.gvTono.Rows
            objeto = New Entidades.be_tonoXProducto
            objeto.idTono = CInt(row.Cells(1).Text)
            objeto.nomtono = CStr(row.Cells(2).Text)
            objeto.cantidadTono = CDec(TryCast(row.FindControl("txtCantidadTono"), TextBox).Text)
            list.Add(objeto)
        Next
        If list.Count > 0 Then
            Me.listaDetalleDocumento(indiceCapaAsignarTono).listaTonos = list
            setListaDetalleDocumento(Me.listaDetalleDocumento)
        End If

        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()
    End Sub

    Private Sub btnNuevoTono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevoTono.Click
        Dim idProducto As Integer = TryCast(Me.GV_Detalle.Rows(idCapasignarTono.Value).FindControl("hddIdProducto"), HiddenField).Value
        Dim objNegocioTrazabilidad As New Negocio.LN_TRAZABILIDAD_DISTRIBUCION
        Me.listaDetalleDocumento = getListaDetalleDocumento()
        If objNegocioTrazabilidad.LN_Insert_Tono(Me.txtNombreTono.Text.Trim(), Me.txtDescripcionTono.Text.Trim(), idProducto) Then
            With Me.listaDetalleDocumento(idCapasignarTono.Value)
                Try
                    .listaTonos = (New Negocio.bl_Tonos).SelectTonosxProducto(.IdProducto, .IdDocumento, .IdUnidadMedida)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                Me.gvTono.DataSource = .listaTonos
                Me.gvTono.DataBind()
            End With

            Dim objDrop As New Negocio.LNValorizadoCajas            
            ddltono.DataSource = objDrop.LN_ReturnDataTable(Me.listaDetalleDocumento(idCapasignarTono.Value).IdProducto, "LISTAR_TONO_X_PRODUCTO")
            ddltono.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');    ", True)
        End If
    End Sub

    Private Sub btBuscarPersonaGrillas_Click(sender As Object, e As System.EventArgs) Handles btBuscarPersonaGrillas.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    'Protected Sub btnRegularizar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRegularizar.Click

    '    Response.Redirect("FrmRegularizacion.aspx")

    'End Sub

    Protected Sub txtCodigoDocumento_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtCodigoDocumento.TextChanged

    End Sub
End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmTomaInventarioReporteador.aspx.vb" MasterPageFile="~/Principal.Master" Inherits="APPWEB.FrmTomaInventarioReporteador" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td class="TituloCelda">TOMA DE INVENTARIO</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" Width="80px" OnClientClick="return( valGenerarDoc()  );" runat="server" Text="Nuevo"  ToolTip="Generar Documento de Toma de Inventario" />

                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" Width="80px" OnClientClick="return( valBuscar() );" runat="server" Text="Buscar" ToolTip="Buscar Documento" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" Width="80px" runat="server" Text="Editar" ToolTip="Editar" OnClientClick="return(  valEditar() );" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" Width="80px" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick="return(  valSave()  );" />
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <asp:Button ID="btnCancelar" Width="80px" OnClientClick="return(confirm('Desea cancelar la Operación ?'));" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>

                        <tr>
                            <td class="Label">Id de Inventario:</td>
                            <td>
                                <asp:TextBox ID="txtBuscarIdInventario" Width="100px" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                <asp:Button ID="btnBuscarTomaInventario" Width="150px" OnClientClick="return(valBuscarTomaInventario());" runat="server" Text="Buscar" ToolTip="Buscar " />
                            </td>
                            <td class="Label"></td>
                            <td></td>
                            <td class="Label"></td>
                            <td></td>
                            
                        </tr>

                        <tr>
                            <td class="Label">Tienda: </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" Enabled="false">
                                    <asp:ListItem>Seleccionar</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="Label">Almacén:</td>
                            <td>
                                <asp:DropDownList ID="cboAlmacen" runat="server" Enabled="false">
                                    <asp:ListItem>Seleccionar</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="Label"></td>
                            <td></td>
                        </tr>

                    </table>

                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />

    <script language="javascript" type="text/javascript">


        function valSave() {

            var IdInventario = document.getElementById('<%=txtBuscarIdInventario%>');
            if (isNaN(IdInventario.value) || IdInventario.value.length == 0) {
                alert('No se ha cargado ningún Inventario.');
                return false;
            }

            var IdTienda = document.getElementById('<%=cboTienda%>');
            if (isNaN(IdTienda.value) || IdTienda.value.length == 0) {
                alert('No se ha cargado ningún Inventario.');
                return false;
            }

            var IdAlmacen = document.getElementById('<%=cboAlmacen%>');
            if (isNaN(IdAlmacen.value) || IdAlmacen.value.length == 0) {
                alert('No se ha cargado ningún Inventario.');
                return false;
            }

            return (confirm('Desea continuar con la operación ?'));
        }

        function valBuscarTomaInventario() {

            var IdInventario = document.getElementById('<%= txtBuscarIdInventario.ClientID %>')
            if (isNaN(IdInventario.value) || IdInventario.value.length == 0) {
                alert('Ingrese un ID de Inventario.');
                IdInventario.select();
                IdInventario.focus();
                return false;
            }

            return true;
        }


        function valEditar() {

            var txtBuscarIdInventario = document.getElementById('<%=txtBuscarIdInventario%>');
            if (isNaN(txtBuscarIdInventario.value) || txtBuscarIdInventario.value.length == 0) {
                alert('No se ha seleccionado ningún Inventario al cual hacer referencia.');
                return false;
            }
            return true;
        }
        function valGenerarDoc_Aceptar() {
            var txtIdInventario = document.getElementById('<%=txtBuscarIdInventario%>');

            return (confirm('El Inventario a modificar es: ' + txtIdInventario.value + '. Desea continuar con la operación ?'));
        }
    </script>


</asp:Content>




﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmFlete1
    Inherits System.Web.UI.Page

#Region "variables"
    Private objScript As New ScriptManagerClass
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
    End Enum
    Private objFlete As Entidades.Flete
    Private ListFlete As List(Of Entidades.Flete)
    Private drop As Combo
#End Region

#Region "Procedimientos"

    Private Sub limpiarForm()
        tbCodigo.Text = ""
        tbCostoFijo.Text = ""
        tbCostoxKilo.Text = ""

        dldpto1.SelectedValue = "00"
        dldpto2.SelectedValue = "00"
        dlprov1.Items.Clear()
        dlprov2.Items.Clear()
        dldist1.Items.Clear()
        dldist2.Items.Clear()
    End Sub

    Private Sub cargarDropDepartamento2()
        For x As Integer = 0 To dldpto1.Items.Count - 1
            dldpto2.Items.Add(New ListItem(dldpto1.Items(x).Text, dldpto1.Items(x).Value))
        Next
    End Sub

    Private Sub llenarGrillaFlete()
        ListFlete = (New Negocio.Flete).SelectFlete
        gvBusqueda.DataSource = ListFlete
        gvBusqueda.DataBind()
        Me.setDataSource(gvBusqueda.DataSource)
    End Sub

    Private Sub cargarAlIniciar()
        drop = New Combo
        drop.LLenarCboDepartamento(dldpto1)
        cargarDropDepartamento2()
        drop.LlenarCboMoneda(dlMoneda, True)
        llenarGrillaFlete()
    End Sub

    Private Sub verBotones(ByVal modo As String)
        Select Case modo
            Case "N"
                btNuevo.Visible = False
                btGuardar.Visible = True
                btCancelar.Visible = True
                pnlPrincipal.Enabled = True
                pnlbusqueda.Enabled = False
            Case "L"
                btNuevo.Visible = True
                btGuardar.Visible = False
                btCancelar.Visible = False
                pnlPrincipal.Enabled = False
                pnlbusqueda.Enabled = True
            Case "G"
                btNuevo.Visible = True
                btGuardar.Visible = False
                btCancelar.Visible = False
                pnlPrincipal.Enabled = False
                pnlbusqueda.Enabled = True
        End Select
    End Sub

    Private Sub Mantenimiento(ByVal tipo As Integer)

        Dim prov1 As String = CStr(IIf(dldpto1.SelectedValue <> "00", dlprov1.SelectedValue, "00"))
        Dim prov2 As String = CStr(IIf(dldpto2.SelectedValue <> "00", dlprov2.SelectedValue, "00"))

        Dim dist1 As String = CStr(IIf(dldist1.Items.Count = 0, "00", dldist1.SelectedValue))
        Dim dist2 As String = CStr(IIf(dldist2.Items.Count = 0, "00", dldist2.SelectedValue))


        objFlete = New Entidades.Flete
        With objFlete
            .IdFlete = CInt(IIf(tbCodigo.Text = "", 0, tbCodigo.Text))
            .pp_UbigeoInicio = dldpto1.SelectedValue + prov1 + dist1
            .pp_UbigeoDestino = dldpto2.SelectedValue + prov2 + dist2
            .IdMoneda = CInt(dlMoneda.SelectedValue)
            .fle_CostoxKilo = CDec(tbCostoxKilo.Text)
            .fle_CostoFijo = CDec(tbCostoFijo.Text)
        End With

        If ValidarExistente(objFlete) = False Then
            objScript.mostrarMsjAlerta(Me, "Existe un registro igual al que desea registrar")
            Exit Sub
        End If

        Select Case tipo
            Case operativo.GuardarNuevo

                tbCodigo.Text = CStr((New Negocio.Flete).InsertFlete(objFlete))
                objScript.mostrarMsjAlerta(Me, "El registro ha sido guardado")
            Case operativo.Actualizar
                Dim update As Boolean = (New Negocio.Flete).UpdateFlete(objFlete)
                If update = True Then
                    llenarGrillaFlete()
                    objScript.mostrarMsjAlerta(Me, "El registro ha sido actualizado")
                End If

        End Select

        llenarGrillaFlete()
        verBotones("G")

    End Sub

    Private Function ValidarExistente(ByVal objxflete As Entidades.Flete) As Boolean
        Dim cont% = 0
        ListFlete = CType(getDataSource(), List(Of Entidades.Flete))
        With ListFlete
            For x As Integer = 0 To .Count - 1
                cont += 1
                If .Item(x).pp_UbigeoInicio = objxflete.pp_UbigeoInicio And _
                     .Item(x).pp_UbigeoDestino = objxflete.pp_UbigeoDestino And _
                     .Item(x).IdMoneda = objxflete.IdMoneda And _
                     .Item(x).IdFlete <> objxflete.IdFlete Then
                    gvBusqueda.PageIndex = CInt((cont / gvBusqueda.PageSize))
                    gvBusqueda.DataSource = ListFlete
                    gvBusqueda.DataBind()
                    ViewState.Add("SelectedRow", x)
                    gvBusqueda.Rows(x).RowState = DataControlRowState.Selected
                    Return False
                End If
            Next
        End With
        Return True
    End Function

#End Region

#Region "Eventos Principales"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                cargarAlIniciar()
                verBotones("L")
                ViewState.Add("operativo", operativo.Ninguno)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btNuevo.Click
        Try
            limpiarForm()
            verBotones("N")
            ViewState.Add("operativo", operativo.GuardarNuevo)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Try
            Mantenimiento(CInt(ViewState("operativo")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        Try
            limpiarForm()
            verBotones("L")
            ViewState.Add("operativo", operativo.Ninguno)
            gvBusqueda.DataSource = getDataSource()
            gvBusqueda.DataBind()
            If ViewState("SelectedRow") IsNot Nothing Then gvBusqueda.Rows(CInt(ViewState("SelectedRow"))).RowState = DataControlRowState.Selected
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

#Region "Seccion de busqueda"
    Private Function getDataSource() As Object
        Return Session.Item("datasource")
    End Function

    Private Sub setDataSource(ByVal obj As Object)
        Session.Remove("datasource")
        Session.Add("datasource", obj)
    End Sub

    Private Sub gvBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBusqueda.PageIndexChanging
        Try
            gvBusqueda.PageIndex = e.NewPageIndex
            gvBusqueda.DataSource = Me.getDataSource
            gvBusqueda.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Seccion Punto Inicio"
    Private Sub dldpto1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dldpto1.SelectedIndexChanged
        Try
            dlprov1.Items.Clear()
            dldist1.Items.Clear()
            If dldpto1.SelectedValue <> "00" Then
                drop = New Combo
                drop.LLenarCboProvincia(dlprov1, dldpto1.SelectedValue)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub dlprov1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlprov1.SelectedIndexChanged
        Try
            dldist1.Items.Clear()
            If dlprov1.SelectedValue <> "00" Then
                drop = New Combo
                drop.LLenarCboDistrito(dldist1, dldpto1.SelectedValue, dlprov1.SelectedValue)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Seccion Punto Destino"
    Private Sub dldpto2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dldpto2.SelectedIndexChanged
        Try
            dlprov2.Items.Clear()
            dldist2.Items.Clear()
            If dldpto2.SelectedValue <> "00" Then
                drop = New Combo
                drop.LLenarCboProvincia(dlprov2, dldpto2.SelectedValue)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub dlprov2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlprov2.SelectedIndexChanged
        Try
            dldist2.Items.Clear()
            If dlprov2.SelectedValue <> "00" Then
                drop = New Combo
                drop.LLenarCboDistrito(dldist2, dldpto2.SelectedValue, dlprov2.SelectedValue)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Seccion Edicion y eliminacion"
    Protected Sub lkEditarClick(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lk As LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lk.NamingContainer, GridViewRow)
            limpiarForm()

            dldpto1.SelectedValue = CType(fila.Cells(2).FindControl("hdd_inicio"), HiddenField).Value.Substring(0, 2)
            dldpto1_SelectedIndexChanged(sender, e)
            dlprov1.SelectedValue = CType(fila.Cells(2).FindControl("hdd_inicio"), HiddenField).Value.Substring(2, 2)
            dlprov1_SelectedIndexChanged(sender, e)
            dldist1.SelectedValue = CType(fila.Cells(2).FindControl("hdd_inicio"), HiddenField).Value.Substring(4, 2)

            dldpto2.SelectedValue = CType(fila.Cells(2).FindControl("hdd_destino"), HiddenField).Value.Substring(0, 2)
            dldpto2_SelectedIndexChanged(sender, e)
            dlprov2.SelectedValue = CType(fila.Cells(2).FindControl("hdd_destino"), HiddenField).Value.Substring(2, 2)
            dlprov2_SelectedIndexChanged(sender, e)
            dldist2.SelectedValue = CType(fila.Cells(2).FindControl("hdd_destino"), HiddenField).Value.Substring(4, 2)

            dlMoneda.SelectedValue = CType(fila.Cells(2).FindControl("hdd_idmoneda"), HiddenField).Value

            tbCodigo.Text = CStr(fila.Cells(3).Text)

            tbCostoxKilo.Text = fila.Cells(7).Text
            tbCostoFijo.Text = fila.Cells(8).Text

            verBotones("N")

            ViewState.Add("operativo", operativo.Actualizar)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub lkEliminarClick(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lk As LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lk.NamingContainer, GridViewRow)
            Dim delete As Boolean = (New Negocio.Flete).deleteFlete(CInt(fila.Cells(3).Text))
            If delete = True Then
                llenarGrillaFlete()
                objScript.mostrarMsjAlerta(Me, "EL registro ha sido eliminado")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

End Class
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmFlete.aspx.vb" Inherits="APPWEB.frmFlete1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td>
                <asp:Button ID="btNuevo" Width="80px" runat="server" Text="Nuevo" />
                <asp:Button ID="btGuardar" Width="80px" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());" />
                <asp:Button ID="btCancelar" OnClientClick="return(confirm('Desea ir al modo Inicio?'));"
                    Width="80px" runat="server" Text="Cancelar" />
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Flete
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlPrincipal" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td class="Texto">
                                Codigo:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCodigo" Enabled="false" CssClass="TextBox_ReadOnly" runat="server"
                                    Width="80px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" class="SubTituloCelda">
                                Punto Origen
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                &nbsp;Departamento:
                            </td>
                            <td>
                                <asp:DropDownList ID="dldpto1" runat="server" AutoPostBack="true" Width="180px">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;Provincia:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlprov1" runat="server" AutoPostBack="true" Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;Distrito:
                            </td>
                            <td>
                                <asp:DropDownList ID="dldist1" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" class="SubTituloCelda">
                                Punto Destino
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                &nbsp;Departamento:
                            </td>
                            <td>
                                <asp:DropDownList ID="dldpto2" runat="server" AutoPostBack="true" Width="180px">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;Provincia:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlprov2" runat="server" AutoPostBack="true" Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;Distrito:
                            </td>
                            <td>
                                <asp:DropDownList ID="dldist2" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlMoneda" runat="server" Width="60px">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Costo x Kilo:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCostoxKilo" onKeypress="return(validarNumeroPunto(event));" runat="server"
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Costo Fijo:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCostoFijo" onKeypress="return(validarNumeroPunto(event));" runat="server"
                                    Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                &nbsp; Registros
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlbusqueda" runat="server">
                    <asp:GridView ID="gvBusqueda" runat="server" AutoGenerateColumns="False" GridLines="None"
                        Width="100%" AllowPaging="True" PageSize="10">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:LinkButton ID="linkEditar" runat="server" OnClick="lkEditarClick" Text="Editar"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Eliminar">
                                <ItemTemplate>
                                    <asp:LinkButton ID="linkEliminar" runat="server" OnClientClick="return(validarEliminar());"
                                        OnClick="lkEliminarClick" Text="Eliminar"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_inicio" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"pp_UbigeoInicio") %>' />
                                    <asp:HiddenField ID="hdd_destino" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"pp_UbigeoDestino") %>' />
                                    <asp:HiddenField ID="hdd_idmoneda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="idFlete" HeaderText="Codigo" />
                            <asp:BoundField DataField="strInicio" HeaderText="Punto Origen">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="strDestino" HeaderText="Punto Destino">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="strMoneda" HeaderText="Moneda" />
                            <asp:BoundField DataField="fle_costoxkilo" HeaderText="Costo x Kilo" DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="fle_costofijo" DataFormatString="{0:F2}" HeaderText="Costo FIjo" />
                        </Columns>
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        function validarGuardar() {
            var moneda = document.getElementById('<%=dlMoneda.ClientID %>');
            var departamento1 = document.getElementById('<%=dldpto1.ClientID %>');
            var departamento2 = document.getElementById('<%=dldpto2.ClientID %>');
            var costoxkilo = document.getElementById('<%=tbCostoxKilo.ClientID %>');
            var costofijo = document.getElementById('<%=tbCostoFijo.ClientID %>');

            if (departamento1.value == '00') {
                alert('No ha seleccionado el punto de origen');
                departamento1.focus();
                return false;
            }
            if (departamento2.value == '00') {
                alert('No ha seleccionado el punto de destino');
                departamento2.focus();
                return false;
            }
            if (costoxkilo.value == '' || isNaN(costoxkilo.value)) {
                alert('Verifique el valor de costo por peso.');
                costoxkilo.focus();
                costoxkilo.select();
                return false;
            }
            if (costofijo.value == '' || isNaN(costofijo.value)) {
                alert('verifique el valor de costo fijo');
                costofijo.focus();
                costofijo.select();
                return false;
            }
            if (moneda.value == '0') {
                alert('No ha seleccionado la moneda');
                moneda.focus();
                return false;
            }
            return confirm('Desea, continuar ?');
        }
        //++++++++++++++++++++++++++
        function validarEliminar() {
            return confirm('Desea, eliminar el registro ?');
        }
    </script>

</asp:Content>

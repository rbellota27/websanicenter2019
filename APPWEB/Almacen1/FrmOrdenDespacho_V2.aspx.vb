﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmOrdenDespacho_V2
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaProductoView As List(Of Entidades.ProductoView)

#Region "************************ MANEJO DE SESSION"
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item(ViewState("listaDetalleDocumento").ToString), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove(ViewState("listaDetalleDocumento").ToString)
        Session.Add(ViewState("listaDetalleDocumento").ToString, lista)
    End Sub
    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item(ViewState("listaDocumentoRef").ToString), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(ViewState("listaDocumentoRef").ToString)
        Session.Add(ViewState("listaDocumentoRef").ToString, lista)
    End Sub


    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("listaDetalleDocumento", "listaDetalleDocumentoOD" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaDocumentoRef", "listaDocumentoRefOD" + Aleatorio.Next(0, 100000).ToString)

    End Sub

#End Region

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then

            onLoad_Session()

            ConfigurarDatos()
            ValidarPermisos()
            inicializarFrm()

            If (Request.QueryString("IdDocumentoRef") <> Nothing) Then
                cargarDocumentoReferencia(CInt(Request.QueryString("IdDocumentoRef")), Nothing, Nothing, Nothing)
            End If

        End If

    End Sub

    Private Sub ValidarPermisos()

        '**** REGISTRAR / CONSULTAR / ANULAR / MODIFICAR FECHA EMISION
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {190, 191, 192, 193})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* MODIFICAR FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

                .LLenarCboEstadoDocumento(Me.cboEstado)
                '--Listar Tipo Documento---
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(hddIdTipoDocumento.Value), 2, False)
                '---------
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), False)
                .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), False)

                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

            actualizarOpcionesBusquedaDocRef(0, False)

            verFrm(FrmModo.Nuevo, True, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal generarCodigoDoc As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove(ViewState("listaDetalleDocumento").ToString)
            Session.Remove(ViewState("listaDocumentoRef").ToString)
        End If

        If (initSession) Then
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (generarCodigoDoc) Then
            GenerarCodigoDocumento()
        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboEstado.SelectedValue = "1"

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.btnBuscarDocumentoRef.Visible = True


                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Destinatario.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboAlmacen.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnBuscarDocumentoRef.Visible = False

                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub


    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto

        '************** DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        '************ DESTINATARIO
        Me.txtDestinatario.Text = ""
        Me.txtIdDestinatario.Text = ""
        Me.txtDni_Destinatario.Text = ""
        Me.txtRuc_Destinatario.Text = ""

        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIdPersona.Value = "0"

        '************* OTROS        
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub ActualizarListaDetalleDocumento()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedItem.ToString)

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_Detalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Detalle.SelectedIndexChanged
        quitarDetalleDocumento()
    End Sub
    Private Sub quitarDetalleDocumento()

        Try

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAt(Me.GV_Detalle.SelectedRow.RowIndex)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub




    Protected Sub cboAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboAlmacen.SelectedIndexChanged
        Try

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).DocumentoSelectDocxDespachar_V2(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), fechaInicio, fechafin, serie, codigo, IdPersona, CInt(Me.hddIdTipoDocumento.Value), CInt(Me.cboTipoOperacion.SelectedValue), CInt(CboTipoDocumento.SelectedValue))

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_DocumentosReferencia_Find.PageIndexChanging
        Me.GV_DocumentosReferencia_Find.PageIndex = e.NewPageIndex
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdEmpresa_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdAlmacen_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdPersona_Find"), HiddenField).Value))
    End Sub

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        If ((CInt(Me.cboEmpresa.SelectedValue) <> IdEmpresa) Or (CInt(Me.cboAlmacen.SelectedValue) <> IdAlmacen)) Then Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE EMPRESA/ALMACÉN DISTINTOS A LOS SELECCIONADOS. NO SE PERMITE LA OPERACIÓN.")

        If (CInt(Me.hddIdPersona.Value) <> IdPersona) Then Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE UN DESTINATARIO DISTINTO AL SELECCIONADO.")

        Return True

    End Function

    Private Function obtenerListaDetalleDocumento_Ref(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).DetalleDocumentoSelectDetallexAtenderxIdDocumento(IdDocumento, CInt(Me.cboTipoOperacion.SelectedValue))

        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
            listaDetalle(i).Cantidad = listaDetalle(i).CantxAtender
            listaDetalle(i).CantidadxAtenderText = CStr(Math.Round(listaDetalle(i).CantxAtender, 4)) + " " + listaDetalle(i).UMedida
            listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
            listaDetalle(i).Stock = (New Negocio.Util).fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFin(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), listaDetalle(i).IdProducto, (New Negocio.FechaActual).SelectFechaActual)
        Next

        Return listaDetalle

    End Function
    Private Sub cargarDocumentoRef_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento As List(Of Entidades.Documento), ByVal objDestinatario As Entidades.PersonaView, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal cargarDocumentoPropio As Boolean)

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboEmpresa.Items.FindByValue(CStr(objDocumento.IdEmpresa)) IsNot Nothing) Then
                Me.cboEmpresa.SelectedValue = CStr(objDocumento.IdEmpresa)
            End If

            If (Me.cboAlmacen.Items.FindByValue(CStr(objDocumento.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)

                objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), False)

                If (Me.cboMotivoTraslado.Items.FindByValue(CStr(objDocumento.IdMotivoT)) IsNot Nothing) Then
                    Me.cboMotivoTraslado.SelectedValue = CStr(objDocumento.IdMotivoT)
                End If

            End If

            If (cargarDocumentoPropio) Then

                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
                Me.txtCodigoDocumento.Text = .Codigo

                If (.FechaEmision <> Nothing) Then
                    Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
                End If

                Me.hddIdDocumento.Value = CStr(.Id)
                Me.hddCodigoDocumento.Value = .Codigo

            End If

        End With

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '***************** DESTINATARIO
        If (objDestinatario IsNot Nothing) Then

            Me.txtDestinatario.Text = objDestinatario.Descripcion
            Me.txtDni_Destinatario.Text = objDestinatario.Dni
            Me.txtRuc_Destinatario.Text = objDestinatario.Ruc
            Me.txtIdDestinatario.Text = CStr(objDestinatario.IdPersona)
            Me.hddIdPersona.Value = CStr(objDestinatario.IdPersona)

        End If

        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()

        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

        '**************** LISTA DOC REF
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub

    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            '*************** QUITAMOS EL DETALLE DEL DOCUMENTO DE REFERENCIA
            Dim i As Integer = (Me.listaDetalleDocumento.Count - 1)
            While i >= 0

                If (CInt(Me.listaDetalleDocumento(i).IdDocumento) = Me.listaDocumentoRef(index).Id) Then
                    Me.listaDetalleDocumento.RemoveAt(i)
                End If

                i = i - 1

            End While

            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            '******************* 
            If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
                '***************** ACTIVAMOS LOS COMBOS DE EMPRESA/ALMACÉN

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Editar
                        Me.cboEmpresa.Enabled = False
                        Me.cboTienda.Enabled = False
                    Case Else
                        Me.cboEmpresa.Enabled = True
                        Me.cboTienda.Enabled = True
                End Select

                Me.cboAlmacen.Enabled = True


            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region


    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click

        registrarDocumento()

    End Sub


    Private Sub validarFrm()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

    End Sub


    Private Sub registrarDocumento()

        Try

            ActualizarListaDetalleDocumento()

            If (Me.GV_DocumentoRef.Rows.Count > 0) Then
                validarDocumentosRef()
            End If

            '******************* VALIDAMOS EL FRM
            validarFrm()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Dim validarDespacho As Boolean = True
            Dim moverStockFisico As Boolean = True

            If ((moverStockFisico) And (objDocumento.IdEmpresa <> objDocumento.IdRemitente)) Then Throw New Exception("LA OPCIÓN < MOVER STOCK FÍSICO > ESTÁ ACTIVO. LA EMPRESA SELECCIONADA NO COINCIDE CON EL REMITENTE SELECCIONADO. NO SE PERMITE LA OPERACIÓN.")

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    objDocumento.Id = (New Negocio.DocOrdenDespacho).registrarDocumento(objDocumento, Me.listaDetalleDocumento, objObservaciones, listaRelacionDocumento, -1, moverStockFisico, validarDespacho)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case FrmModo.Editar

                    '******************* ESTE DOCUMENTO NO TIENE EDICION

            End Select

            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub validarDocumentosRef()

        Me.listaDocumentoRef = getlistaDocumentoRef()
        Dim IdDestinatario As Integer = CInt(Me.hddIdPersona.Value)
        Dim IdEmpresa As Integer = CInt(Me.cboEmpresa.SelectedValue)
        Dim IdAlmacen As Integer = CInt(Me.cboAlmacen.SelectedValue)

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).IdPersona <> IdDestinatario) Then Throw New Exception("EL DESTINATARIO DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON EL DESTINATARIO SELECCIONADO. NO SE PERMITE LA OPERACIÓN.")
            If (Me.listaDocumentoRef(i).IdEmpresa <> IdEmpresa) Then Throw New Exception("LA EMPRESA DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON LA EMPRESA SELECCIONADA. NO SE PERMITE LA OPERACIÓN.")
            If (Me.listaDocumentoRef(i).IdAlmacen <> IdAlmacen) Then Throw New Exception("EL ALMACÉN DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON EL ALMACÉN SELECCIONADO. NO SE PERMITE LA OPERACIÓN.")

        Next


    End Sub

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)
        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

            End With

            lista.Add(objRelacionDocumento)

        Next

        Return lista

    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .FechaIniTraslado = Nothing
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdMotivoT = CInt(Me.cboMotivoTraslado.SelectedValue)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEstadoEntrega = 2 '************* ENTREGADO

            .IdPersona = CInt(Me.hddIdPersona.Value)
            .IdRemitente = CInt(Me.cboEmpresa.SelectedValue)
            .IdDestinatario = CInt(Me.hddIdPersona.Value)

            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

            '************* VALORES ADICIONALES
            .FactorMov = -1 '************** Mov de Almacén

        End With

        Return objDocumento

    End Function
    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer)

        Try

            ActualizarListaDetalleDocumento()

            '******************** INICIALIZAMOS EL FRM CUANDO NO EXISTE UN DOC. DE REFERENCIA
            If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
                verFrm(FrmModo.Nuevo, True, True, True, True)
            Else
                If (Not validar_AddDocumentoRef(IdDocumentoRef, IdEmpresa, IdAlmacen, IdPersona)) Then Throw New Exception("NO SE PERMITE LA OPERACIÓN.")
            End If

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objDestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumento_Ref(objDocumento.Id))

            '*************** GUARDAMOS EN SESSION
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** 
            cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objDestinatario, Me.listaDetalleDocumento, Nothing, False)


            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            '***************** DESACTIVAMOS LOS COMBOS DE EMPRESA/ALMACÉN
            Me.cboEmpresa.Enabled = False
            Me.cboTienda.Enabled = False
            Me.cboAlmacen.Enabled = False


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoOrdenDespacho(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoOrdenDespacho(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '**************** LIMPIAMOS EL FORMULARIO
        verFrm(FrmModo.Inicio, True, True, True, True)

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO
        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If



        Dim objDestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)


        Me.listaDetalleDocumento = obtenerListaDetalleDocumento_OrdenDespacho(objDocumento.Id)
        Me.listaDocumentoRef = (New Negocio.DocGuiaRemision).DocumentoGuiaRemisionSelectDocumentoRef(objDocumento.Id)

        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '*************** GUARDAMOS EN SESSION
        setListaDetalleDocumento(Me.listaDetalleDocumento)
        setlistaDocumentoRef(Me.listaDocumentoRef)

        '************** 
        cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objDestinatario, Me.listaDetalleDocumento, objObservaciones, True)

        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        Dim activarControlesRef As Boolean = False
        If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
            activarControlesRef = True
        End If

        '***************** CONFIGURAMOS LOS CONTROLES
        Me.cboEmpresa.Enabled = activarControlesRef
        Me.cboTienda.Enabled = activarControlesRef
        Me.cboAlmacen.Enabled = activarControlesRef


    End Sub

    Private Function obtenerListaDetalleDocumento_OrdenDespacho(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DocGuiaRemision).DocumentoGuiaRemisionSelectDetalle(IdDocumento)
        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
            listaDetalle(i).IdDocumento = listaDetalle(i).IdDocumentoRef '*********** COLOCAMOS EL DOCUMENTO REF

        Next

        Return listaDetalle

    End Function

#End Region

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try

            verFrm(FrmModo.Editar, False, False, False, False)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDocumentoRef = getlistaDocumentoRef()

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnular.Click
        anularDocumento()
    End Sub
    Private Sub anularDocumento()
        Try

            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

            If ((New Negocio.DocOrdenDespacho).AnularxIdDocumento(IdDocumento)) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                Me.cboEstado.SelectedValue = "2"  '*********** ANULADO
                verFrm(FrmModo.Documento_Save_Exito, False, False, False, False)
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            cargarDocumentoOrdenDespacho(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value.Trim), 0)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True)
    End Sub

    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



End Class
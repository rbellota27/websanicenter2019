﻿Public Class FrmTomaInventarioReporteador
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6 : OCULTAR TODOS LOS BOTONES
    '**********************************************

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub

    Private Sub inicializarFrm()
        Try

            '************************** cargamos los controles
            Dim objCombo As New Combo
            With objCombo

                '************ Controles del Frm Principal
                '.llenarCboTiendaxIdEmpresa(Me.cboTienda, 1, False)
                Me.cboTienda.Items.Insert(0, "-Seleccionar-")

                .llenarCboAlmacenxIdEmpresa(Me.cboAlmacen, 1, True)

            End With

            hddFrmModo.Value = "0"  '*********** NUEVO
            actualizarControlesFrmModo()

            ValidarPermisos()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos [ inicializarFrm() ].")
        End Try
    End Sub


    Private Sub ValidarPermisos()
        '**** EDITAR / CONSULTAR
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {2106800007, 2106800008})

        If listaPermisos(0) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarTomaInventario.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarTomaInventario.Enabled = False
        End If

    End Sub

    Private Sub actualizarControlesFrmModo()

        Select Case CInt(hddFrmModo.Value)

            Case 0 '************* INICIO
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Panel_Cab.Enabled = True
                Me.cboTienda.Enabled = False
            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarTomaInventario.Visible = False
                Me.txtBuscarIdInventario.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Panel_Cab.Enabled = True
            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarTomaInventario.Visible = False
                Me.txtBuscarIdInventario.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Panel_Cab.Enabled = True
                Me.cboTienda.Enabled = True
            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarTomaInventario.Visible = True
                Me.txtBuscarIdInventario.ReadOnly = False
                Me.txtBuscarIdInventario.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True

                Me.txtBuscarIdInventario.Text = ""
                Me.txtBuscarIdInventario.Focus()


                Panel_Cab.Enabled = True
            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarTomaInventario.Visible = False
                Me.txtBuscarIdInventario.ReadOnly = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True


                Panel_Cab.Enabled = False

            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarTomaInventario.Visible = False
                Me.txtBuscarIdInventario.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True

                Panel_Cab.Enabled = False

            Case 6  '*********** Inhabilitamos todos los controles excepto cancelar

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarTomaInventario.Visible = False
                Me.txtBuscarIdInventario.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True

                Panel_Cab.Enabled = False

        End Select
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        hddFrmModo.Value = "3"  '*********** BUSCAR
        actualizarControlesFrmModo()
    End Sub

    Protected Sub btnBuscarTomaInventario_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarTomaInventario.Click

        If (Me.txtBuscarIdInventario.Text <> "") Then
            cargarDocumento(CInt(Me.txtBuscarIdInventario.Text))
        End If

    End Sub

    Private Sub cargarDocumento(ByVal idInventario As Integer)

        Try


            Dim obj As Entidades.TomaInventarioReporteador = (New Negocio.TomaInventarioReporteador).GetTomaInventario(idInventario)
            If (obj Is Nothing) Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
                Return
            Else


                '************ Cargamos la cabecera
                cargarData(obj)

                '*********** Carga
                hddFrmModo.Value = "4"
                actualizarControlesFrmModo()


            End If




        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de la toma de inventario")

        End Try


    End Sub

    Private Sub cargarData(ByVal obj As Entidades.TomaInventarioReporteador)

        txtBuscarIdInventario.Text = CStr(obj.Idtinventario)

        If (obj.IdTienda <> 0) Then
            Me.btnEditar.Visible = False
        Else
            Me.btnEditar.Visible = True
        End If

        If (obj.IdAlmacen <> vbNull) Then
            Me.cboAlmacen.SelectedValue = CStr(obj.IdAlmacen)

            Dim objCombo As New Combo
            With objCombo
                '************ Llenar tienda
                Me.cboTienda.Items.Clear()
                .llenarCboTiendaxIdAlmacen(Me.cboTienda, obj.IdAlmacen, True)

                Me.cboTienda.SelectedValue = CStr(obj.IdTienda)

            End With

        Else
            Me.cboAlmacen.SelectedValue = 0
        End If

        'Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)
        'Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
        'Me.txtFechaTomaInv.Text = Format(objDocumento.FechaEntrega, "dd/MM/yyyy")

    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click

        hddFrmModo.Value = "2"
        actualizarControlesFrmModo()

    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click

        limpiarFrm()
        hddFrmModo.Value = "0"
        actualizarControlesFrmModo()

    End Sub

    Private Sub limpiarFrm()

        txtBuscarIdInventario.Text = ""
        Me.cboTienda.Items.Insert(0, "-Seleccionar-")
        Me.cboTienda.SelectedValue = 0
        Me.cboAlmacen.SelectedValue = 0
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click

        registrarDocumento(CInt(txtBuscarIdInventario.Text))


    End Sub

    Private Sub registrarDocumento(ByVal IdDocumento As Integer)

        Try

            Dim obj As Entidades.TomaInventarioReporteador = obtenerDocumento()

            If ((New Negocio.TomaInventarioReporteador).Update(obj) <= 0) Then

                Throw New Exception

            Else

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "alert('La operación finalizó con éxito.');  ", True)

                '*********** Carga
                hddFrmModo.Value = "4"
                actualizarControlesFrmModo()
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la operación.")
        End Try

    End Sub


    Private Function obtenerDocumento() As Entidades.TomaInventarioReporteador

        Dim obj As New Entidades.TomaInventarioReporteador

        With obj

            .Idtinventario = CInt(Me.txtBuscarIdInventario.Text)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)

        End With

        Return obj

    End Function
End Class
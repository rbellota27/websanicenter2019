﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmProcesoInventarioInicial
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboPropietario(Me.cboEmpresa, False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), False)

                '***************** CARGAMOS EL IDDOCUMENTO
                Me.hddIdDocumento.Value = CStr((New Negocio.DocumentoInvInicial).DocumentoInventarioInicialSelectIdDocumentoxParams(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))

                If (CInt(Me.hddIdDocumento.Value) <> Nothing) Then
                    Me.txtFechaEmision.Text = Format(((New Negocio.Documento).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value)).FechaEmision), "dd/MM/yyyy")
                Else
                    Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
                End If
                '---lineas--------
                .llenarCboTipoExistencia(Me.cboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cboLinea, CInt(cboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(cboLinea.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
                '.llenarCboTipoExistencia(cboTipoExistencia, False)
                '.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
                '.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
                ''.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                '.LlenarCboLinea(Me.cboLinea, False)
                ' .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), False)

            End With

            limpiarDetalle()

            ValidarPermisos()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try


    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {144, 145})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnGuardar.Enabled = True
        Else
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), False)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged

        Try

            Me.hddIdDocumento.Value = CStr((New Negocio.DocumentoInvInicial).DocumentoInventarioInicialSelectIdDocumentoxParams(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))
            If (CInt(Me.hddIdDocumento.Value) <> Nothing) Then
                Me.txtFechaEmision.Text = Format(((New Negocio.Documento).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value)).FechaEmision), "dd/MM/yyyy")
            Else
                Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            End If

            limpiarDetalle()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Try

            Dim objCbo As New Combo

            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            Me.hddIdDocumento.Value = CStr((New Negocio.DocumentoInvInicial).DocumentoInventarioInicialSelectIdDocumentoxParams(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))

            If (CInt(Me.hddIdDocumento.Value) <> Nothing) Then
                Me.txtFechaEmision.Text = Format(((New Negocio.Documento).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value)).FechaEmision), "dd/MM/yyyy")
            Else
                Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            End If

            limpiarDetalle()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAceptar_Detalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_Detalle.Click

        cargarListaProductos(CInt(Me.hddIdDocumento.Value), CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue))

    End Sub

    Private Sub cargarListaProductos(ByVal IdDocumento As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer)
        Try

            Dim prod_Nombre As String = String.Empty
            Dim prod_Codigo As String = String.Empty
            Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

            Select Case CInt(rbTipoBusqueda.SelectedValue)
                Case 0
                    prod_Codigo = txt_descripcion.Text.Trim
                Case 1
                    prod_Nombre = txt_descripcion.Text.Trim
            End Select

            Me.GV_Detalle.DataSource = (New Negocio.DetalleDocumento).SelectxIdSubLineaxIdDocumento(IdSubLinea, IdDocumento, prod_Codigo, prod_Nombre, tableTipoTabla)
            Me.GV_Detalle.DataBind()

            If (Me.GV_Detalle.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrarDocumentoInvInicial()
    End Sub

    Private Sub registrarDocumentoInvInicial()

        Try

            validarFrm()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim listaDetalleDocumento As List(Of Entidades.DetalleDocumento) = obtenerListaDetalleDocumento()

            Dim IdDocumento As Integer = (New Negocio.DocumentoInvInicial).registrarDocumento(objDocumento, listaDetalleDocumento)
            If (IdDocumento > 0) Then
                Me.hddIdDocumento.Value = CStr(IdDocumento)
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If

            limpiarDetalle()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub validarFrm()

        If (Me.GV_Detalle.Rows.Count <= 0) Then
            Throw New Exception("DEBE SELECCIONAR PRODUCTOS. NO SE PERMITE LA OPERACIÓN.")
        End If

    End Sub

    Private Function obtenerListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Dim objDetalle As New Entidades.DetalleDocumento

            With objDetalle

                .IdProducto = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                .IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdUnidadMedida"), HiddenField).Value)
                .Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
                .UMedida = HttpUtility.HtmlDecode(Me.GV_Detalle.Rows(i).Cells(2).Text)
                .PrecioCD = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCosto"), TextBox).Text)

            End With

            lista.Add(objDetalle)

        Next

        Return lista

    End Function

    Private Sub limpiarDetalle()

        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

    End Sub

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)
            .IdEstadoDoc = 1 '********** ACTIVO
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdTienda = CInt(Me.cboTienda.SelectedValue)

        End With

        Return objDocumento

    End Function

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try

            Dim objCbo As New Combo

            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            Me.hddIdDocumento.Value = CStr((New Negocio.DocumentoInvInicial).DocumentoInventarioInicialSelectIdDocumentoxParams(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))

            If (CInt(Me.hddIdDocumento.Value) <> Nothing) Then
                Me.txtFechaEmision.Text = Format(((New Negocio.Documento).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value)).FechaEmision), "dd/MM/yyyy")
            Else
                Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            End If

            limpiarDetalle()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        Try
            Dim obj2 As New Combo
            obj2.llenarCboLineaxTipoExistencia(cboLinea, CInt(cboTipoExistencia.SelectedValue), True)
            obj2.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLinea, CInt(cboLinea.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub cboSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSubLinea.SelectedIndexChanged
        Dim cbo As New Combo
        cbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cboSubLinea.SelectedValue), False)
        GV_FiltroTipoTabla.DataBind()
    End Sub



#Region "******************* Filtro de busquedad avanza de productos"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0

            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue), CInt(cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1
            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)
        Next

        Return dt
    End Function

    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

#End Region

End Class
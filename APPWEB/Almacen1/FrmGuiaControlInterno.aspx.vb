﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmGuiaControlInterno
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaProductoView As List(Of Entidades.ProductoView)

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

#Region "************************ MANEJO DE SESSION"
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item(CStr(ViewState("DetalleDocumento"))), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove(CStr(ViewState("DetalleDocumento")))
        Session.Add(CStr(ViewState("DetalleDocumento")), lista)
    End Sub
    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item(CStr(ViewState("listaDocumentoRef"))), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(CStr(ViewState("listaDocumentoRef")))
        Session.Add(CStr(ViewState("listaDocumentoRef")), lista)
    End Sub

    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("DetalleDocumento", "DetalleDocumentoGCI" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaDocumentoRef", "listaDocumentoRefGCI" + Aleatorio.Next(0, 100000).ToString)

    End Sub

#End Region

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then

            onLoad_Session()

            ConfigurarDatos()
            ValidarPermisos()
            inicializarFrm()
        End If
    End Sub

    Private Sub ValidarPermisos()

        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / MOD REMITENTE / MOD DESTINATARIO / MOD OPCION MOVER STOCK FISICO
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {170, 171, 172, 173, 174, 175, 176, 177})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True

        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False

        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* MOD REMITENTE
            Me.btnBuscarRemitente.Enabled = True
        Else
            Me.btnBuscarRemitente.Enabled = False
        End If

        If listaPermisos(6) > 0 Then  '********* MOD DESTINATARIO
            Me.btnBuscarDestinatario.Enabled = True
        Else
            Me.btnBuscarDestinatario.Enabled = False
        End If

        If listaPermisos(7) > 0 Then  '********* MOD OPCION < MOVER STOCK FISICO >
            Me.chb_MoverAlmacen.Enabled = True
        Else
            Me.chb_MoverAlmacen.Enabled = False
        End If

    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

                .LLenarCboEstadoDocumento(Me.cboEstado)
                Dim cadenaPermisosTipOperacion As String = System.Configuration.ConfigurationManager.AppSettings("idPermisosTipoOperacionGCI").ToString()
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True, CInt(Session("IdUsuario")), cadenaPermisosTipOperacion)
                .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), False)
                '------lineas-------
                '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
                .llenarCboTipoExistencia(CboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)




                ' .LlenarCboLinea(Me.cmbLinea_AddProd, True)
                '.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)

                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


                .LlenarCboTipoAlmacen(Me.cboTipoAlmacen_Destinatario, False)
                .LlenarCboTipoAlmacen(Me.cboTipoAlmacen_Remitente, False)

                .LLenarCboTipoDocumentoRefxIdTipoDocumento(cboTipoDocumento, CInt(Me.hddIdTipoDocumento.Value), 2, False)
            End With

            actualizarOpcionesBusquedaDocRef(0, False)

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
        

            Me.hddNroFilasxDocumento.Value = CStr((New Negocio.DocumentoView).SelectCantidadFilasDocumento(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))

            verFrm(FrmModo.Nuevo, False, True, True, True, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, ByVal inicializarRemitente As Boolean, ByVal inicializarDestinatario As Boolean, ByVal generarCodigo As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove(ViewState("DetalleDocumento").ToString)
            Session.Remove(ViewState("listaDocumentoRef").ToString)
        End If

        If (initSession) Then
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})

        End If

        If (inicializarRemitente) Then

            Me.hddOpcionBusquedaPersona.Value = "0"
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue))

        End If

        If (inicializarDestinatario) Then

            Me.hddOpcionBusquedaPersona.Value = "1"
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue))

        End If

        If (generarCodigo) Then

            GenerarCodigoDocumento()

        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Remitente.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False





                Me.Panel_Cab.Enabled = True
                Me.Panel_Remitente.Enabled = True
                Me.Panel_Destinatario.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.btnBuscarRemitente.Enabled = True
                Me.btnBuscarDestinatario.Enabled = True
                Me.btnBuscarProducto.Enabled = True
                Me.btnLimpiarDetalleDocumento.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False



                Me.Panel_Cab.Enabled = True

                Me.Panel_Remitente.Enabled = True

                Me.Panel_Destinatario.Enabled = True

                Me.Panel_Detalle.Enabled = True

                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                'Me.btnNuevo.Visible = True
                'Me.btnBuscar.Visible = True
                'Me.btnBuscarDocumentoxCodigo.Visible = True
                'Me.txtCodigoDocumento.ReadOnly = False
                'Me.txtCodigoDocumento.Focus()
                'Me.btnEditar.Visible = False
                'Me.btnGuardar.Visible = False
                'Me.btnAnular.Visible = False
                'Me.btnImprimir.Visible = False


                'Me.btnBuscarDocumentoRef.Visible = False

                'Me.Panel_Cab.Enabled = True
                'Me.Panel_DocumentoRef.Enabled = False
                'Me.Panel_Remitente.Enabled = False
                'Me.Panel_PuntoPartida.Enabled = False
                'Me.Panel_Destinatario.Enabled = False
                'Me.Panel_PuntoLlegada.Enabled = False
                'Me.Panel_Detalle.Enabled = False
                'Me.Panel_Transportista.Enabled = False
                'Me.Panel_Chofer.Enabled = False
                'Me.Panel_Vehiculo.Enabled = False
                'Me.Panel_Obs.Enabled = False

                'Me.cboEmpresa.Enabled = True
                'Me.cboTienda.Enabled = True
                'Me.cboSerie.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True


                Me.Panel_Cab.Enabled = False

                Me.Panel_Remitente.Enabled = False

                Me.Panel_Destinatario.Enabled = False

                Me.Panel_Detalle.Enabled = False

                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False



                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False

                Me.Panel_Remitente.Enabled = False

                Me.Panel_Destinatario.Enabled = False

                Me.Panel_Detalle.Enabled = False

                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False

                Me.Panel_Remitente.Enabled = False

                Me.Panel_Destinatario.Enabled = False

                Me.Panel_Detalle.Enabled = False

                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub


    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto

        '************ REMITENTE
        Me.txtRemitente.Text = ""
        Me.txtIdRemitente.Text = ""
        Me.txtDni_Remitente.Text = ""
        Me.txtRuc_Remitente.Text = ""

        '************ DESTINATARIO
        Me.txtDestinatario.Text = ""
        Me.txtIdDestinatario.Text = ""
        Me.txtDni_Destinatario.Text = ""
        Me.txtRuc_Destinatario.Text = ""

        '************ PERSONA DOC REF
        Me.txtDescripcionPersona.Text = ""
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""

        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDestinatario.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIdRemitente.Value = ""
        Me.hddIdDocumentoRef.value = ""
        Me.hddIdPersonaDocRef.Value = ""
        Me.hddOpcionBusquedaPersona.Value = "0"  '**** REMITENTE

        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.gvBuscar.DataSource = Nothing
        Me.gvBuscar.DataBind()

        Me.GV_DocumentoRef.DataBind()

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            GenerarCodigoDocumento()


            Me.hddOpcionBusquedaPersona.Value = "0" '**** REMITENTE
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue))

            Me.hddOpcionBusquedaPersona.Value = "1" '**** DESTINATARIO
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue))



        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            GenerarCodigoDocumento()

            Me.hddOpcionBusquedaPersona.Value = "0" '**** REMITENTE
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue))

            Me.hddOpcionBusquedaPersona.Value = "1" '**** DESTINATARIO
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()        
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)

        If (CInt(Me.hddOpcionBusquedaPersona.Value) = 3) Then   '*********** CHOFER
            listaPersona = (New Negocio.Chofer).ChoferSelectActivoxParams(dni, ruc, RazonApe, tipo, index, gv.PageSize, estado)
        Else
            listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        End If

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub


    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer, Optional ByVal mostrarDocumentosRef As Boolean = True)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        Dim objCbo As New Combo

        Select Case CInt(Me.hddOpcionBusquedaPersona.Value)

            Case 0  '****** REMITENTE
                Me.txtRemitente.Text = objPersona.Descripcion
                Me.txtIdRemitente.Text = CStr(objPersona.IdPersona)
                Me.txtDni_Remitente.Text = objPersona.Dni
                Me.txtRuc_Remitente.Text = objPersona.Ruc
                Me.hddIdRemitente.Value = CStr(objPersona.IdPersona)

                If (IdPersona = CInt(Me.cboEmpresa.SelectedValue)) Then

                    objCbo.LLenarCboAlmacenxIdTipoAlmacenxIdTienda(Me.cboAlmacen_Remitente, CInt(Me.cboTipoAlmacen_Remitente.SelectedValue), CInt(Me.cboTienda.SelectedValue), False)

                Else

                    Me.cboAlmacen_Remitente.Items.Clear()

                End If

            Case 1  '****** DESTINATARIO
                Me.txtDestinatario.Text = objPersona.Descripcion
                Me.txtIdDestinatario.Text = CStr(objPersona.IdPersona)
                Me.txtDni_Destinatario.Text = objPersona.Dni
                Me.txtRuc_Destinatario.Text = objPersona.Ruc
                Me.hddIdDestinatario.Value = CStr(objPersona.IdPersona)

                If (IdPersona = CInt(Me.cboEmpresa.SelectedValue)) Then

                    objCbo.LLenarCboAlmacenxIdTipoAlmacenxIdTienda(Me.cboAlmacen_Destinatario, CInt(Me.cboTipoAlmacen_Destinatario.SelectedValue), CInt(Me.cboTienda.SelectedValue), False)

                Else

                    Me.cboAlmacen_Destinatario.Items.Clear()

                End If

            Case 2 ' *********** DOCUMENTO REFERENCIA

                Me.txtDescripcionPersona.Text = objPersona.Descripcion
                Me.txtDni.Text = objPersona.Dni
                Me.txtRuc.Text = objPersona.Ruc
                Me.hddIdPersonaDocRef.Value = CStr(objPersona.IdPersona)

                Me.GV_BusquedaAvanzado.DataSource = Nothing
                Me.GV_BusquedaAvanzado.DataBind()

                If mostrarDocumentosRef Then
                    mostrarDocumentosRef_Find()
                End If

                Return

        End Select

        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()

        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region


#Region "************************** BUSQUEDA PRODUCTO"

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try
            Dim IdAlmacen As Integer = 0
            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)

                    If (Me.cboAlmacen_Remitente.Items.Count > 0) Then
                        IdAlmacen = CInt(Me.cboAlmacen_Remitente.SelectedValue)
                    End If

                    ViewState.Add("IdAlmacen_BuscarProducto", IdAlmacen)


                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, _
                                          ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        '*************Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)

        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try

            Dim IdAlmacen As Integer = 0
            If (Me.cboAlmacen_Remitente.Items.Count > 0) Then
                IdAlmacen = CInt(Me.cboAlmacen_Remitente.SelectedValue)
            End If

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            IdAlmacen, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            Dim IdAlmacen As Integer = 0
            If (Me.cboAlmacen_Remitente.Items.Count > 0) Then
                IdAlmacen = CInt(Me.cboAlmacen_Remitente.SelectedValue)
            End If

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            IdAlmacen, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            Dim IdAlmacen As Integer = 0
            If (Me.cboAlmacen_Remitente.Items.Count > 0) Then
                IdAlmacen = CInt(Me.cboAlmacen_Remitente.SelectedValue)
            End If
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            IdAlmacen, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)

            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, 0, 0, (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub mostrarCapaStockPrecioxProducto1(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)

            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, 0, 0, (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)



            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
  
#End Region

    Private Sub addProducto_DetalleDocumento()
        Try

            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaDetalleDocumento()

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

                Dim cantidad As Decimal = CDec(CType(Me.DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)

                If (cantidad > 0) Then

                    Dim IdProducto_Find As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto_Find"), HiddenField).Value)
                    Dim IdUnidadMedida_Find As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
                    Dim Kit_Find As Boolean = False : Try : Kit_Find = CBool(CType(Me.DGV_AddProd.Rows(i).FindControl("hddKit"), HiddenField).Value) : Catch ex As Exception : End Try



                    Dim valCant As Boolean = False
                    For k As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                        If listaDetalleDocumento(k).IdProducto = IdProducto_Find And listaDetalleDocumento(k).IdUnidadMedida = IdUnidadMedida_Find Then

                            Me.listaDetalleDocumento(k).Cantidad = Me.listaDetalleDocumento(k).Cantidad + cantidad
                            Me.listaDetalleDocumento(k).Kit = Kit_Find

                            valCant = True
                            Exit For

                        End If

                    Next

                    If valCant = False Then
                        Dim objDetalle As New Entidades.DetalleDocumento
                        With objDetalle
                            .CodigoProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(1).Text))
                            .IdProducto = IdProducto_Find
                            .NomProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(2).Text))
                            .Cantidad = cantidad                            
                            .listaTonos = (New Negocio.bl_Tonos).ln_ListarTonosxProducto_y_cantidades(.IdProducto, Me.cboAlmacen_Remitente.SelectedValue)
                            '.listaTonos = (New Negocio.bl_Tonos).ln_ListarTonosxProducto(.IdProducto)
                            .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                            .IdUnidadMedida = IdUnidadMedida_Find
                            Try : .Kit = CBool(CType(Me.DGV_AddProd.Rows(i).FindControl("hddKit"), HiddenField).Value) : Catch ex As Exception : End Try

                        End With
                        Me.listaDetalleDocumento.Add(objDetalle)

                    End If

                    If Kit_Find Then ' AGREGANDO COMPONENTES

                        Dim ListaKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponentexIdKit(IdProducto_Find)

                        For k As Integer = 0 To ListaKit.Count - 1


                            Dim valCant2 As Boolean = False
                            For z As Integer = 0 To listaDetalleDocumento.Count - 1
                                If listaDetalleDocumento(z).IdProducto = ListaKit(k).IdComponente And listaDetalleDocumento(z).IdUnidadMedida = ListaKit(k).IdUnidadMedida_Comp Then
                                    listaDetalleDocumento(z).Cantidad = listaDetalleDocumento(z).Cantidad + cantidad

                                    valCant2 = True
                                    Exit For
                                End If
                            Next
                            If valCant2 = False Then

                                Dim objDetalle2 As New Entidades.DetalleDocumento
                                With objDetalle2

                                    .CodigoProducto = ListaKit(k).CodigoProd_Comp
                                    .IdProducto = ListaKit(k).IdComponente
                                    .NomProducto = ListaKit(k).Componente
                                    .Cantidad = ListaKit(k).Cantidad_Comp * cantidad
                                    .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                                    .IdUnidadMedida = ListaKit(k).IdUnidadMedida_Comp
                                    .listaTonos = (New Negocio.bl_Tonos).ln_ListarTonosxProducto_y_cantidades(.IdProducto, Me.cboAlmacen_Remitente.SelectedValue)
                                End With

                                Me.listaDetalleDocumento.Add(objDetalle2)
                            End If
                        Next

                    End If ' KIT

                End If ' CANTIDAD


            Next


            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
                Me.listaDetalleDocumento(i).Stock = (New Negocio.Util).fx_StockReal(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen_Remitente.SelectedValue), listaDetalleDocumento(i).IdProducto)
            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  onCapa('capaBuscarProducto_AddProd');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub ActualizarListaDetalleDocumento(Optional ByVal Index As Integer = -1)
        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            If Index = i Then Me.listaDetalleDocumento(i).IdUMold = Me.listaDetalleDocumento(i).IdUnidadMedida

            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedItem.ToString)
            Me.listaDetalleDocumento(i).idTono = Val(CType(Me.GV_Detalle.Rows(i).FindControl("ddlTonos"), DropDownList).SelectedValue)
        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub

    Protected Sub cboUnidadMedida_GV_Detalle(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

            ActualizarListaDetalleDocumento(i)

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento(i).Cantidad = (New Negocio.Producto).fx_getValorEquivalenteProducto(Me.listaDetalleDocumento(i).IdProducto, Me.listaDetalleDocumento(i).IdUMold, Me.listaDetalleDocumento(i).IdUnidadMedida, Me.listaDetalleDocumento(i).Cantidad)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            GV_Detalle.DataSource = Me.listaDetalleDocumento
            GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub crearControles(ByVal indice As Integer)
        Me.listaDetalleDocumento = getListaDetalleDocumento()
        'Dim indice As Integer = Convert.ToInt32(e.CommandArgument)
        Dim random As Integer = CInt(Int((500 * Rnd()) + 1))
        Dim row As GridViewRow = Me.GV_Detalle.Rows(indice)
        Dim placeHolderCantidades As PlaceHolder = TryCast(row.FindControl("phCantidades"), PlaceHolder)
        Dim placeHolderTonos As PlaceHolder = TryCast(row.FindControl("phTonos"), PlaceHolder)
        'Agrega el textbox
        Dim textbox As New TextBox
        textbox.ID = "txt_" & random
        textbox.Text = 800
        textbox.Width = "90"
        textbox.Attributes.Add("onfocus", "return(aceptarFoco(this));")
        textbox.Attributes.Add("onkeypress", "return(validarNumeroPunto(event));")
        textbox.Attributes.Add("onblur", "return(valBlur(event));")
        placeHolderCantidades.Controls.Add(New LiteralControl("</br>"))
        placeHolderCantidades.Controls.Add(textbox)
        '====================
        'Agrega nueva fila de tonos
        Dim dropDownList As New DropDownList

        dropDownList.DataSource = Me.listaDetalleDocumento(indice).listaTonos
        dropDownList.DataValueField = "idTono"
        dropDownList.DataTextField = "nomtono"
        dropDownList.DataBind()
        placeHolderTonos.Controls.Add(New LiteralControl("</br>"))
        placeHolderTonos.Controls.Add(dropDownList)
        
    End Sub

    'Private Sub GV_Detalle_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_Detalle.RowCommand
    '    'Dim indice As Integer = Convert.ToInt32(e.CommandArgument)
    '    'Me.listaDetalleDocumento = getListaDetalleDocumento()
    '    'Dim ddl As DropDownList = TryCast(Me.GV_Detalle.Rows(indice).FindControl("ddlTonos"), DropDownList)
    '    'Dim cantidadItemsDDL As Integer = ddl.Items.Count
    '    'Select Case e.CommandName
    '    '    Case "btnAgregarTono"
    '    '        If Me.listaDetalleDocumento(indice).nroContador < cantidadItemsDDL - 1 Then
    '    '            Me.listaDetalleDocumento(indice).nroContador += 1
    '    '        End If
    '    '        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
    '    '        Me.GV_Detalle.DataBind()
    '    'End Select
    'End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                Dim ddlTono As DropDownList = CType(e.Row.FindControl("ddlTonos"), DropDownList)

                ddlTono.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).listaTonos
                ddlTono.DataBind()
                ddlTono.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).idTono.ToString

                cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString


                If Me.listaDetalleDocumento(e.Row.RowIndex).Kit Then
                    e.Row.ControlStyle.ForeColor = Drawing.Color.Red
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_Detalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Detalle.SelectedIndexChanged
        quitarDetalleDocumento()
    End Sub
    Private Sub quitarDetalleDocumento()

        Try            
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAt(Me.GV_Detalle.SelectedRow.RowIndex)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        limpiarDetalleDocumento()
    End Sub
    Private Sub limpiarDetalleDocumento()
        Try
            'inicializa variable global de crear textbox            

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaProductoView(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaProductoView(e.Row.RowIndex).IdUnidadMedida.ToString

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click

        registrarDocumento()

    End Sub


    Private Sub validarFrm()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        If (Me.listaDetalleDocumento.Count > CInt(Me.hddNroFilasxDocumento.Value)) Then
            Throw New Exception("EL NÚMERO MÁXIMO DE FILAS PARA EL DOCUMENTO ES DE < " + CStr(Math.Round(CInt(Me.hddNroFilasxDocumento.Value), 0)) + " >. NO SE PERMITE LA OPERACIÓN.")
        End If

    End Sub
    Private Sub actualizarCostosIngreso()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        Dim IdAlmacen As Integer = Nothing
        If (CInt(Me.hddIdRemitente.Value) = CInt(Me.cboEmpresa.SelectedValue) And Me.cboAlmacen_Remitente.Items.Count > 0) Then
            IdAlmacen = CInt(Me.cboAlmacen_Remitente.SelectedValue)
        ElseIf (CInt(Me.hddIdDestinatario.Value) = CInt(Me.cboEmpresa.SelectedValue) And Me.cboAlmacen_Destinatario.Items.Count > 0) Then
            IdAlmacen = CInt(Me.cboAlmacen_Destinatario.SelectedValue)
        End If

        If (IdAlmacen <> Nothing) Then

            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                Me.listaDetalleDocumento(i).CostoMovIngreso = (New Negocio.Util).fx_getCostoProducto(CInt(Me.cboEmpresa.SelectedValue), IdAlmacen, Me.listaDetalleDocumento(i).IdProducto, CDate(Me.txtFechaEmision.Text))

            Next

        End If



        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub

    Private Sub registrarDocumento()

        Try            

            '            cargarControles()
            ActualizarListaDetalleDocumento()

            '******************* VALIDAMOS EL FRM
            validarFrm()

            '************* OBTENEMOS COSTOS DE INGRESO
            actualizarCostosIngreso()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Me.listaDetalleDocumento = getListaDetalleDocumento()



            Dim objPuntoPartida As Entidades.PuntoPartida = obtenerPuntoPartida()
            Dim objPuntoLlegada As Entidades.PuntoLlegada = obtenerPuntoLlegada()

            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

            Dim moverStockFisico As Boolean = Me.chb_MoverAlmacen.Checked



            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    objDocumento.Id = (New Negocio.DocumentoGuiaControlInterno).registrarDocumento(objDocumento, Me.listaDetalleDocumento, objObservaciones, objPuntoPartida, objPuntoLlegada, listaRelacionDocumento, moverStockFisico)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case FrmModo.Editar

                    objDocumento.Id = (New Negocio.DocumentoGuiaControlInterno).actualizarDocumento(objDocumento, Me.listaDetalleDocumento, objObservaciones, objPuntoPartida, objPuntoLlegada, listaRelacionDocumento, moverStockFisico)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")

            End Select

            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try


    End Sub
    Private Function obtenerPuntoLlegada() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing

        objPuntoLlegada = New Entidades.PuntoLlegada
        With objPuntoLlegada

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            If (Me.cboAlmacen_Destinatario.Items.Count > 0 And CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdDestinatario.Value)) Then
                .IdAlmacen = CInt(Me.cboAlmacen_Destinatario.SelectedValue)
            Else
                .IdAlmacen = Nothing
            End If

        End With

        Return objPuntoLlegada

    End Function
    Private Function obtenerPuntoPartida() As Entidades.PuntoPartida

        Dim objPuntoPartida As Entidades.PuntoPartida = Nothing


        objPuntoPartida = New Entidades.PuntoPartida

        With objPuntoPartida

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            If (Me.cboAlmacen_Remitente.Items.Count > 0 And CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdRemitente.Value)) Then
                .IdAlmacen = CInt(Me.cboAlmacen_Remitente.SelectedValue)
            Else
                .IdAlmacen = Nothing
            End If



        End With


        Return objPuntoPartida

    End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdMotivoT = CInt(Me.cboMotivoTraslado.SelectedValue)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)

            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEstadoEntrega = 2 '************* ENTREGADO

            .IdPersona = CInt(Me.hddIdDestinatario.Value)
            .IdRemitente = CInt(Me.hddIdRemitente.Value)
            .IdDestinatario = CInt(Me.hddIdDestinatario.Value)

            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

            '************ ANALISI ALMACEN
            Dim IdAlmacen As Integer = Nothing
            If (Me.cboAlmacen_Remitente.Items.Count > 0) Then
                IdAlmacen = CInt(Me.cboAlmacen_Remitente.SelectedValue)
            ElseIf (Me.cboAlmacen_Destinatario.Items.Count > 0) Then
                IdAlmacen = CInt(Me.cboAlmacen_Destinatario.SelectedValue)
            Else
                IdAlmacen = Nothing
            End If

            .IdAlmacen = IdAlmacen

        End With

        Return objDocumento

    End Function

#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdDestinatario.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoGuiaInterna(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoGuiaInterna(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)
        '**************** LIMPIAMOS EL FORMULARIO
        verFrm(FrmModo.Inicio, True, True, True, True, True, True, False)

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO
        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        Dim objRemitente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)
        Dim objDestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)

        Me.listaDetalleDocumento = obtenerListaDetalleDocumento_Load(objDocumento.Id)

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
            Me.listaDetalleDocumento(i).Stock = (New Negocio.Util).fx_StockReal(objDocumento.IdEmpresa, objDocumento.IdAlmacen, listaDetalleDocumento(i).IdProducto)
        Next

        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
        Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)

        cargarDocumento_GUI(objDocumento, objRemitente, objDestinatario, Me.listaDetalleDocumento, objObservaciones, objPuntoPartida, objPuntoLlegada)

        '*************** GUARDAMOS EN SESSION
        setListaDetalleDocumento(Me.listaDetalleDocumento)

        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False, False, False)

    End Sub
    Private Sub cargarDocumento_GUI(ByVal objDocumento As Entidades.Documento, ByVal objRemitente As Entidades.PersonaView, ByVal objDestinatario As Entidades.PersonaView, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada)

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)

                objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), False)

                If (Me.cboMotivoTraslado.Items.FindByValue(CStr(objDocumento.IdMotivoT)) IsNot Nothing) Then
                    Me.cboMotivoTraslado.SelectedValue = CStr(objDocumento.IdMotivoT)
                End If

            End If

            Me.txtCodigoDocumento.Text = .Codigo

            If (.FechaEmision <> Nothing) Then
                Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            End If
          

            Me.hddIdDocumento.Value = CStr(.Id)
            Me.hddCodigoDocumento.Value = .Codigo

            If (Me.cboEstado.Items.FindByValue(CStr(objDocumento.IdEstadoDoc)) IsNot Nothing) Then
                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
            End If

            If .IdEstadoDoc = 3 Then
                Return
            End If


           

        End With

        '******************* REMITENTE
        If (objRemitente IsNot Nothing) Then

            Me.txtRemitente.Text = objRemitente.Descripcion
            Me.txtDni_Remitente.Text = objRemitente.Dni
            Me.txtRuc_Remitente.Text = objRemitente.Ruc
            Me.txtIdRemitente.Text = CStr(objRemitente.IdPersona)
            Me.hddIdRemitente.Value = CStr(objRemitente.IdPersona)

        End If

        '****************** PUNTO PARTIDA
        If (objPuntoPartida IsNot Nothing) Then
            If (Me.cboTipoAlmacen_Remitente.Items.FindByValue(CStr(objPuntoPartida.IdTipoAlmacen)) IsNot Nothing) Then
                Me.cboTipoAlmacen_Remitente.SelectedValue = CStr(objPuntoPartida.IdTipoAlmacen)
            End If

            '************ LLENO EL ALMACEN X TIPO
            If (objRemitente.IdPersona = CInt(Me.cboEmpresa.SelectedValue)) Then

                objCbo.LLenarCboAlmacenxIdTipoAlmacenxIdTienda(Me.cboAlmacen_Remitente, CInt(Me.cboTipoAlmacen_Remitente.SelectedValue), CInt(Me.cboTienda.SelectedValue), False)
                If (Me.cboAlmacen_Remitente.Items.FindByValue(CStr(objPuntoPartida.IdAlmacen)) IsNot Nothing) Then
                    Me.cboAlmacen_Remitente.SelectedValue = CStr(objPuntoPartida.IdAlmacen)
                End If

            Else

                Me.cboAlmacen_Remitente.Items.Clear()

            End If

        End If

        '***************** DESTINATARIO
        If (objDestinatario IsNot Nothing) Then

            Me.txtDestinatario.Text = objDestinatario.Descripcion
            Me.txtDni_Destinatario.Text = objDestinatario.Dni
            Me.txtRuc_Destinatario.Text = objDestinatario.Ruc
            Me.txtIdDestinatario.Text = CStr(objDestinatario.IdPersona)
            Me.hddIdDestinatario.Value = CStr(objDestinatario.IdPersona)

        End If

        '****************** PUNTO LLEGADA
        If (objPuntoLlegada IsNot Nothing) Then
            If (Me.cboTipoAlmacen_Destinatario.Items.FindByValue(CStr(objPuntoLlegada.IdTipoAlmacen)) IsNot Nothing) Then
                Me.cboTipoAlmacen_Destinatario.SelectedValue = CStr(objPuntoLlegada.IdTipoAlmacen)
            End If

            '************ LLENO EL ALMACEN X TIPO
            If (objDestinatario.IdPersona = CInt(Me.cboEmpresa.SelectedValue)) Then

                objCbo.LLenarCboAlmacenxIdTipoAlmacenxIdTienda(Me.cboAlmacen_Destinatario, CInt(Me.cboTipoAlmacen_Destinatario.SelectedValue), CInt(Me.cboTienda.SelectedValue), False)
                If (Me.cboAlmacen_Destinatario.Items.FindByValue(CStr(objPuntoLlegada.IdAlmacen)) IsNot Nothing) Then
                    Me.cboAlmacen_Destinatario.SelectedValue = CStr(objPuntoLlegada.IdAlmacen)
                End If

            Else

                Me.cboAlmacen_Destinatario.Items.Clear()

            End If

        End If

        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()

        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

    End Sub
    Private Function obtenerListaDetalleDocumento_Load(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoGuiaControlInterno).DocumentoGuiaControlInterno_SelectDetalle(IdDocumento)
        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
            listaDetalle(i).listaTonos = (New Negocio.bl_Tonos).ln_ListarTonosxProducto_y_cantidades(listaDetalle(i).IdProducto, Me.cboAlmacen_Remitente.SelectedValue)
        Next

        Return listaDetalle

    End Function

#End Region

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try

            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100010}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.hddIdTipoDocumento.Value)
            Select Case listaPermisos(0)
                Case 0
                    objScript.mostrarMsjAlerta(Me, "No está permitido editar el documento con fecha anterior a la de hoy")
                Case 1
                    verFrm(FrmModo.Editar, False, False, False, False, False, False, False)

                    Me.listaDetalleDocumento = getListaDetalleDocumento()

                    Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                    Me.GV_Detalle.DataBind()
            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnular.Click
        anularDocumento()
    End Sub
    Private Sub anularDocumento()
        Try
            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100008}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.hddIdTipoDocumento.Value)
            Select Case listaPermisos(0)
                Case 1
                    Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

                    If ((New Negocio.DocumentoGuiaControlInterno).anularDocumento(IdDocumento)) Then
                        objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False, False)
                    Else
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If
                Case 0
                    objScript.mostrarMsjAlerta(Me, "No procede [Anular] un documento de un mes anterior al actual.")
            End Select
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            cargarDocumentoGuiaInterna(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value.Trim), 0)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True, True, True)
    End Sub

    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscarRemitente_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarRemitente.Click

        mostrarCapaPersona("0")

    End Sub
    Private Sub mostrarCapaPersona(ByVal opcion As String)

        Try

            Me.gvBuscar.DataSource = Nothing
            Me.gvBuscar.DataBind()

            Me.hddOpcionBusquedaPersona.Value = opcion

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad  ", "      mostrarCapaPersona();        ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTipoAlmacen_Remitente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAlmacen_Remitente.SelectedIndexChanged

        Try

            Dim objCbo As New Combo
            If (CInt(Me.hddIdRemitente.Value) = CInt(Me.cboEmpresa.SelectedValue)) Then

                objCbo.LLenarCboAlmacenxIdTipoAlmacenxIdTienda(Me.cboAlmacen_Remitente, CInt(Me.cboTipoAlmacen_Remitente.SelectedValue), CInt(Me.cboTienda.SelectedValue), False)

            Else

                Me.cboAlmacen_Remitente.Items.Clear()

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cboTipoAlmacen_Destinatario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAlmacen_Destinatario.SelectedIndexChanged

        Try

            Dim objCbo As New Combo
            If (CInt(Me.hddIdDestinatario.Value) = CInt(Me.cboEmpresa.SelectedValue)) Then

                objCbo.LLenarCboAlmacenxIdTipoAlmacenxIdTienda(Me.cboAlmacen_Destinatario, CInt(Me.cboTipoAlmacen_Destinatario.SelectedValue), CInt(Me.cboTienda.SelectedValue), False)

            Else

                Me.cboAlmacen_Destinatario.Items.Clear()

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#Region "**************************    CALCULAR EQUIVALENCIAS"

    Protected Sub btnEquivalencia_PR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Try

            ActualizarListaDetalleDocumento()
            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)

            Me.listaDetalleDocumento = getListaDetalleDocumento()


            '******************** CARGAMOS LOS CONTROLES
            Me.cboUnidadMedida_Ingreso.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.cboUnidadMedida_Ingreso.DataBind()

            Me.cboUnidadMedida_Salida.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.cboUnidadMedida_Salida.DataBind()

            Me.GV_CalcularEquivalencia.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_CalcularEquivalencia.DataBind()

            Me.GV_ResuldoEQ.DataSource = Nothing
            Me.GV_ResuldoEQ.DataBind()

            Me.txtCantidad_Ingreso.Text = CStr(Me.listaDetalleDocumento(index).Cantidad)
            Me.txtCantidad_Salida.Text = ""

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaEquivalencia_PR();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Protected Sub btnCalcular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCalcular.Click
        calcularEquivalencia()
    End Sub
    Private Sub calcularEquivalencia()
        Try

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()


            Dim tableUM As DataTable = obtenerTablaUM(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_ResuldoEQ.DataSource = (New Negocio.Producto).Producto_ConsultaEquivalenciaProductoxParams(Me.listaDetalleDocumento(index).IdProducto, CInt(Me.cboUnidadMedida_Ingreso.SelectedValue), CInt(Me.cboUnidadMedida_Salida.SelectedValue), CDec(Me.txtCantidad_Ingreso.Text), tableUM, CInt(Me.rbl_UtilizarRedondeo.SelectedValue))
            Me.GV_ResuldoEQ.DataBind()

            Me.GV_ResuldoEQ.Rows(Me.GV_ResuldoEQ.Rows.Count - 1).BackColor = Drawing.Color.Yellow

            Dim cantidadEq As Decimal = CDec(Me.GV_ResuldoEQ.Rows(Me.GV_ResuldoEQ.Rows.Count - 1).Cells(2).Text)
            Me.txtCantidad_Salida.Text = CStr(cantidadEq)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaEquivalencia_PR();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerTablaUM(ByVal IdProducto As Integer) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        For i As Integer = 0 To Me.GV_CalcularEquivalencia.Rows.Count - 1

            If (CType(Me.GV_CalcularEquivalencia.Rows(i).FindControl("chb_UnidadMedida"), CheckBox).Checked) Then
                Dim IdUnidadMedida As Integer = CInt(CType(Me.GV_CalcularEquivalencia.Rows(i).FindControl("hddIdUnidadMedida"), HiddenField).Value)
                dt.Rows.Add(IdProducto, IdUnidadMedida)
            End If

        Next

        Return dt


    End Function
    Private Sub aceptarEquivalencia_PR()
        Try

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim cantidadNew As Decimal = CDec(Me.txtCantidad_Salida.Text)
            Me.listaDetalleDocumento(index).Cantidad = cantidadNew

            If Not Me.listaDetalleDocumento(index).ListaUM.Find(Function(k As Entidades.ProductoUMView) k.IdUnidadMedida = CInt(Me.cboUnidadMedida_Salida.SelectedValue)) Is Nothing Then
                Me.listaDetalleDocumento(index).IdUnidadMedida = CInt(Me.cboUnidadMedida_Salida.SelectedValue)
                Me.listaDetalleDocumento(index).UMedida = CStr(Me.cboUnidadMedida_Salida.SelectedItem.ToString)
            End If

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     offCapa('capaEquivalenciaProducto');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub btnAceptar_EquivalenciaPR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_EquivalenciaPR.Click

        aceptarEquivalencia_PR()

    End Sub


#End Region


#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As String = "0"
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersonaDocRef.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersonaDocRef.Value)) Then
                If (CInt(Me.hddIdPersonaDocRef.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersonaDocRef.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CStr(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef2(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(Me.cboTipoDocumento.SelectedValue), fechaInicio, fechafin, False)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objObservacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(IdDocumentoRef)
           
            '*************** GUARDAMOS EN SESSION
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** 
            cargarDocumentoRef_GUI(IdDocumentoRef, objDocumento, Nothing, Nothing, Nothing, Nothing, objObservacion)

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function obtenerListaDetalleDocumento_Load_DocRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoCotizacion).DocumentoVenta_SelectDetxIdDocumentoRef(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(lista(i).IdProducto, IdTienda, IdTipoPV)

        Next

        Return lista

    End Function

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function


    Private Sub cargarDocumentoRef_GUI(ByVal IdDocumentoRef As Integer, ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservaciones As Entidades.Observacion)

        '*************** PERSONA
        If IsNumeric(Me.hddIdPersonaDocRef.Value) Then
            If objDocumento.IdPersona <> CInt(Me.hddIdPersonaDocRef.Value) Then
                cargarPersona(objDocumento.IdPersona, False)
            End If
        End If

        ActualizarListaDetalleDocumento()
        Me.listaDetalleDocumento = getListaDetalleDocumento()

        Me.listaDetalleDocumento = (New Negocio.DetalleDocumento).SelectxIdDocumento(objDocumento.Id) ' obtenerListaDetalleDocumento_Load_DocRef(IdDocumentoRef, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CDate(Me.txtFechaEmision.Text), IdTipoPv, objDocumento.IdPersona, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboUsuarioComision.SelectedValue))

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            Me.listaDetalleDocumento(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(i).IdProducto)
            Me.listaDetalleDocumento(i).Stock = (New Negocio.Util).fx_StockReal(objDocumento.IdEmpresa, objDocumento.IdAlmacen, Me.listaDetalleDocumento(i).IdProducto)
            Me.listaDetalleDocumento(i).listaTonos = (New Negocio.bl_Tonos).ln_ListarTonosxProducto_y_cantidades(Me.listaDetalleDocumento(i).IdProducto, Me.cboAlmacen_Remitente.SelectedValue)
        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)
        '***************** DETALLE DOCUMENTO
        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()

    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub


    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)
        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

            End With

            lista.Add(objRelacionDocumento)

        Next

        Return lista

    End Function


#End Region


    Private Sub btnBuscarGrilla_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGrilla_AddProducto.Click
        BuscarProductoCatalogo(0)
    End Sub

    Private Sub btnAddProductos_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProductos_AddProducto.Click
        addProducto_DetalleDocumento()
    End Sub

    Private Sub btnBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)            
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function nroclick() As Integer
        Throw New NotImplementedException
    End Function
End Class
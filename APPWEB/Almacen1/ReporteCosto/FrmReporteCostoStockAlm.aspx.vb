﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Partial Public Class FrmReporteCostoStockAlm
    Inherits System.Web.UI.Page
    Dim cbo As Combo
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            cbo = New Combo
            With cbo
                .LLenarCboTienda(Me.ddlTienda)
                .llenarCboTipoExistencia(Me.ddlTipoExistencia)
                .llenarCboAlmacenxIdTienda(Me.ddlAlmacen, CInt(ddlTienda.SelectedValue))
            End With
            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            cargarDatosLinea(Me.ddlLinea)
            cargarDatosSubLinea(Me.ddlSubLinea, CInt(Me.ddlLinea.SelectedValue))
        End If
    End Sub

    Private Sub cargarDatosLinea(ByVal combo As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal combo As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        combo.DataSource = lista
        combo.DataBind()
    End Sub

    Protected Sub ddlLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLinea.SelectedIndexChanged
        cargarDatosSubLinea(Me.ddlSubLinea, CInt(Me.ddlLinea.SelectedValue))
    End Sub

    Protected Sub ddlTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTienda.SelectedIndexChanged
        Dim combo As New Combo
        With combo
            .llenarCboAlmacenxIdTienda(Me.ddlAlmacen, CInt(ddlTienda.SelectedValue))
        End With
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        ReporteAlmacenCxPVxA()
    End Sub

    Private Sub ReporteAlmacenCxPVxA()
        Dim Linea As Integer = 0
        Dim SubLinea As Integer = 0
        Dim Fecha As String = ""
        Dim Tienda As Integer = 0
        Dim IdAlmacen As Integer = 0

        Linea = CInt(ddlLinea.SelectedValue)
        SubLinea = CInt(ddlSubLinea.SelectedValue)
        Fecha = txtFechaEmision.Text
        Tienda = CInt(ddlTienda.SelectedValue)
        IdAlmacen = CInt(ddlAlmacen.SelectedValue)

        Dim ListaReporte As List(Of Entidades.ReporteAlmacen) = New Negocio.Producto().ReporteAlmacen(Linea, SubLinea, Fecha, Tienda, IdAlmacen)
        GV_ReporteAlmacenSCP.DataSource = ListaReporte
        GV_ReporteAlmacenSCP.DataBind()

    End Sub
    Private Sub ExportToExcel(ByVal nameReport As String, ByVal wControl As GridView)
        Dim responsePage As HttpResponse = Response
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim pageToRender As New Page()
        Dim form As New HtmlForm()
        Dim form2 As New HtmlForm()
        form.Controls.Add(wControl)
        pageToRender.Controls.Add(form)
        responsePage.Clear()
        responsePage.Buffer = True
        responsePage.ContentType = "application/vnd.ms-excel"
        responsePage.AddHeader("Content-Disposition", "attachment;filename=" & nameReport)
        responsePage.Charset = "UTF-8"
        responsePage.ContentEncoding = Encoding.Default
        pageToRender.RenderControl(htw)
        responsePage.Write(sw.ToString())
        responsePage.End()
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
        If (GV_ReporteAlmacenSCP.Rows.Count = 0) Then
            objScript.mostrarMsjAlerta(Me, "Por Favor Ingrese los Criterios de Búsqueda")
        Else
            ExportToExcel("ReporteInventarioxMesxAnio.xls", GV_ReporteAlmacenSCP)
        End If
    End Sub
End Class
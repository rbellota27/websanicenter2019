﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Web.UI
Imports System.IO

Partial Public Class FrmDocumentoTomaInventario
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaEmpleado As List(Of Entidades.PersonaView)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
            ValidarPermisos()
        End If
    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {69, 70, 71, 72, 73, 168, 169})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* FECHA TOMA INVENTARIO
            Me.txtFechaTomaInv.Enabled = True
        Else
            Me.txtFechaTomaInv.Enabled = False
        End If

        If listaPermisos(6) > 0 Then  '********* RECALCULAR SALDOS
            Me.btnRecalcularSaldos.Enabled = True
        Else
            Me.btnRecalcularSaldos.Enabled = False
        End If

    End Sub
    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), False)
                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)



                '****************
                '.LlenarCboLinea(Me.cboLinea_Filtro, True)
                '.LlenarCboSubLineaxIdLinea(Me.cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), True)


                '*********************** BUSQUEDA PRODUCTo
                '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
                '.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
                '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
                .llenarCboTipoExistencia(CboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                'Llenar Tipo Documento xLineas--- FILTRO
                .llenarCboTipoExistencia(CboTipoExistenciaDocumento, False)
                .llenarCboLineaxTipoExistencia(cboLinea_Filtro, CInt(Me.CboTipoExistenciaDocumento.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.CboTipoExistenciaDocumento.SelectedValue), False)
            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaTomaInv.Text = Me.txtFechaEmision.Text()
            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text



            actualizarOpcionesBusquedaDocRef(1, False) '******** POR NRO DOCUMENTO

            verFrm(FrmModo.Inicio, True, True, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()

            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setListaDocumentoRef(Me.listaDocumentoRef)

            Me.GV_DocumentoRef.DataSource = Nothing
            Me.GV_DocumentoRef.DataBind()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()


            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setListaDocumentoRef(Me.listaDocumentoRef)

            Me.GV_DocumentoRef.DataSource = Nothing
            Me.GV_DocumentoRef.DataBind()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub
#Region "************************ MANEJO DE SESSION"
    Private Function getListaEmpleado() As List(Of Entidades.PersonaView)
        Return CType(Session.Item("listaEmpleadoTInv"), List(Of Entidades.PersonaView))
    End Function
    Private Sub setListaEmpleado(ByVal lista As List(Of Entidades.PersonaView))
        Session.Remove("listaEmpleadoTInv")
        Session.Add("listaEmpleadoTInv", lista)
    End Sub
    Private Function getListaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRefTInv"), List(Of Entidades.Documento))
    End Function
    Private Sub setListaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRefTInv")
        Session.Add("listaDocumentoRefTInv", lista)
    End Sub

    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("listaDetalleDocumentoTinv"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("listaDetalleDocumentoTinv")
        Session.Add("listaDetalleDocumentoTinv", lista)
    End Sub
#End Region

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, ByVal generarCodigo As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove("listaEmpleadoTInv")
            Session.Remove("listaDocumentoRefTInv")
        End If

        If (initSession) Then

            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setListaDocumentoRef(Me.listaDocumentoRef)

            Me.listaEmpleado = New List(Of Entidades.PersonaView)
            setListaEmpleado(Me.listaEmpleado)

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})

        End If

        If (generarCodigo) Then
            GenerarCodigoDocumento()
        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Me.btnRecalcularSaldos.Visible = False
        Me.btnBuscarDocumentoRef.Visible = False
        Me.Panel_DocumentoRef.Enabled = False
        Me.chb_Consolidado.Enabled = False
        Me.btnGuardar_Detalle.Visible = True
        Me.btnGenerar_Lista.Visible = True
        Me.cboGenerarLista.Visible = True

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnInicio.Visible = False
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnExportar.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = False
                Me.Panel_CargaProductos.Enabled = False
                Me.Panel_FiltroDetalle.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboAlmacen.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                Me.chb_Consolidado.Enabled = True

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnExportar.Visible = False
                Me.btnRecalcularSaldos.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = True
                Me.Panel_CargaProductos.Enabled = True
                Me.Panel_FiltroDetalle.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False


                Me.cboEstado.SelectedValue = "1"


                If (Me.GV_DocumentoRef.Rows.Count > 0) Then
                    Me.Panel_CargaProductos.Enabled = False
                    Me.btnGuardar_Detalle.Visible = False
                    Me.btnGenerar_Lista.Visible = False
                    Me.cboGenerarLista.Visible = False
                End If



            Case FrmModo.Editar '*********************** Editar
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnExportar.Visible = False
                Me.btnRecalcularSaldos.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = True
                Me.Panel_CargaProductos.Enabled = True
                Me.Panel_FiltroDetalle.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False

                If (Me.GV_DocumentoRef.Rows.Count > 0) Then
                    Me.Panel_CargaProductos.Enabled = False
                    Me.btnGuardar_Detalle.Visible = False
                    Me.btnGenerar_Lista.Visible = False
                    Me.cboGenerarLista.Visible = False
                End If



            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                'Me.btnNuevo.Visible = True
                'Me.btnBuscar.Visible = True
                'Me.btnBuscarDocumentoxCodigo.Visible = True
                'Me.txtCodigoDocumento.ReadOnly = False
                'Me.txtCodigoDocumento.Focus()
                'Me.btnEditar.Visible = False
                'Me.btnGuardar.Visible = False
                'Me.btnAnular.Visible = False
                'Me.btnImprimir.Visible = False


                'Me.btnBuscarDocumentoRef.Visible = False

                'Me.Panel_Cab.Enabled = True
                'Me.Panel_DocumentoRef.Enabled = False
                'Me.Panel_Empleado.Enabled = False
                'Me.Panel_CargaProductos.Enabled = False
                'Me.Panel_FiltroDetalle.Enabled = False
                'Me.Panel_Detalle.Enabled = False
                'Me.Panel_Obs.Enabled = False


                'Me.cboEmpresa.Enabled = True
                'Me.cboTienda.Enabled = True
                'Me.cboSerie.Enabled = True
                'Me.cboAlmacen.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True
                Me.btnExportar.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = False
                Me.Panel_CargaProductos.Enabled = False
                Me.Panel_FiltroDetalle.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False

            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnImprimir.Visible = True
                Me.btnExportar.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = False
                Me.Panel_CargaProductos.Enabled = False
                Me.Panel_FiltroDetalle.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnExportar.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = False
                Me.Panel_CargaProductos.Enabled = False
                Me.Panel_FiltroDetalle.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False

                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub
    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub
#Region "************************** BUSQUEDA PRODUCTO"

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacen.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                          ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                            ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, _
                                            ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        'Dim listaProductoView As List(Of Entidades.ProductoView) = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Dim codigoProducto As String = Me.txtCodigoProducto_AddProd.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        Dim listaProductoView As List(Of Entidades.ProductoView) = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objCbo.LlenarCboTotalPag_LineaxSubLinea(Me.cboPageIndex, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboPageSize.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_AddProd.SelectedIndexChanged

        Try

            Dim IdProducto As Integer = CInt(CType(Me.DGV_AddProd.SelectedRow.FindControl("hddIdProducto"), HiddenField).Value)
            Dim IdDocumento As Integer = 0

            If (IsNumeric(Me.hddIdDocumento.Value) And Me.hddIdDocumento.Value.Trim.Length > 0) Then
                IdDocumento = CInt(Me.hddIdDocumento.Value)
            End If

            cargarProducto_Detalle_AddProducto(False, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), 0, 0, IdProducto, IdDocumento, CDate(Me.txtFechaTomaInv.Text), "", Nothing)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaBuscarProducto_AddProd');   calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try



    End Sub
    Private Sub cargarProducto_Detalle_AddProducto(ByVal replaceGrillaDetalle As Boolean, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal IdDocumento As Integer, ByVal FechaTomaInv As Date, ByVal CodigoProducto As String, ByVal MyIndex As Integer)

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        If (replaceGrillaDetalle) Then

            Me.listaDetalleDocumento = (New Negocio.TomaInventario).DocumentoTomaInventario_CargarProductoDetallexParams(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea, IdProducto, IdDocumento, FechaTomaInv, CInt(Me.cboPageIndex.SelectedValue) - 1, CInt(Me.cboPageSize.SelectedValue), "")


        Else

            actualizarListaDetalleDocumento()

            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                If IsNothing(Me.listaDetalleDocumento(i).CodigoProducto) Then Me.listaDetalleDocumento(i).CodigoProducto = ""

                If ((Me.listaDetalleDocumento(i).IdProducto = IdProducto And Me.listaDetalleDocumento(i).IdProducto > 0) Or (Me.listaDetalleDocumento(i).CodigoProducto = CodigoProducto And Me.listaDetalleDocumento(i).CodigoProducto.Length > 0)) Then

                    Throw New Exception("EL PRODUCTO YA HA SIDO AGREGADO. NO SE PERMITE LA OPERACIÓN.")

                End If

            Next

            If CodigoProducto.Length > 0 Then

                Dim Lista_DetalleDocumento As List(Of Entidades.DetalleDocumento) = (New Negocio.TomaInventario).DocumentoTomaInventario_CargarProductoDetallexParams(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea, IdProducto, IdDocumento, FechaTomaInv, 0, 1, CodigoProducto)

                If Not IsNothing(Lista_DetalleDocumento) Then

                    Me.listaDetalleDocumento.RemoveAt(MyIndex)
                    Me.listaDetalleDocumento.InsertRange(MyIndex, Lista_DetalleDocumento)

                End If


            Else

                Me.listaDetalleDocumento.AddRange((New Negocio.TomaInventario).DocumentoTomaInventario_CargarProductoDetallexParams(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea, IdProducto, IdDocumento, FechaTomaInv, 0, 1, ""))

            End If



        End If

        setListaDetalleDocumento(Me.listaDetalleDocumento)

        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()


    End Sub
#End Region


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)
        listaPersona = (New Negocio.Empleado).EmpleadoSelectActivoxParams_Find(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try



            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        Me.listaEmpleado = getListaEmpleado()


        For i As Integer = 0 To Me.listaEmpleado.Count - 1

            If (Me.listaEmpleado(i).IdPersona = IdPersona) Then
                Throw New Exception("LA PERSONA SELECCIONADA YA HA SIDO INGRESADA. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next


        Me.listaEmpleado.Add(objPersona)
        setListaEmpleado(Me.listaEmpleado)

        Me.GV_Empleado.DataSource = Me.listaEmpleado
        Me.GV_Empleado.DataBind()

        objScript.onCapa(Me, "capaPersona")

    End Sub

#End Region



    Protected Sub GV_Empleado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Empleado.SelectedIndexChanged
        quitarEmpleado(Me.GV_Empleado.SelectedIndex)
    End Sub
    Private Sub quitarEmpleado(ByVal Index As Integer)

        Try


            Me.listaEmpleado = getListaEmpleado()
            Me.listaEmpleado.RemoveAt(Index)
            setListaEmpleado(Me.listaEmpleado)

            Me.GV_Empleado.DataSource = Me.listaEmpleado
            Me.GV_Empleado.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGrilla_AddProd.Click

        BuscarProductoCatalogo(0)

    End Sub

    Private Sub cboLinea_Filtro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea_Filtro.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnCargaProductoMasivo_AddProd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCargaProductoMasivo_AddProd.Click

        Try

            Dim IdDocumento As Integer = 0

            If (IsNumeric(Me.hddIdDocumento.Value) And Me.hddIdDocumento.Value.Trim.Length > 0) Then
                IdDocumento = CInt(Me.hddIdDocumento.Value)
            End If

            cargarProducto_Detalle_AddProducto(True, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), 0, IdDocumento, CDate(Me.txtFechaTomaInv.Text), "", Nothing)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaBuscarProducto_AddProd');   calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub actualizarListaDetalleDocumento()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1



            Me.listaDetalleDocumento(i).DetalleGlosa = HttpUtility.HtmlDecode(CType(Me.GV_Detalle.Rows(i).FindControl("txtDetalleGlosa"), TextBox).Text).Trim

            Me.listaDetalleDocumento(i).dc_Descuento1 = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad1"), TextBox).Text)
            Me.listaDetalleDocumento(i).dc_Descuento2 = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad2"), TextBox).Text)

            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)

        Next


        setListaDetalleDocumento(Me.listaDetalleDocumento)


    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click

        GenerarDocumentoTomaInventario()



    End Sub
    Private Sub validarFrm()

        If (Me.chb_Consolidado.Checked And Me.GV_DocumentoRef.Rows.Count <= 0) Then

            Throw New Exception("SE HA ACTIVADO LA OPCIÓN < CONSOLIDADO >. DEBE SELECCIONAR DOCUMENTO(S) DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")

        End If

    End Sub
    Private Sub GenerarDocumentoTomaInventario()
        Try

            validarFrm()


            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()
            Dim listaDocumento_Persona As List(Of Entidades.Documento_Persona) = obtenerListaDocumento_Persona()

            objDocumento.Id = (New Negocio.TomaInventario).DocumentoTomaInventario_GenerarDocumento(objDocumento, listaRelacionDocumento, listaDocumento_Persona)

            If (objDocumento.Id > 0) Then


                Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                verFrm(FrmModo.Nuevo, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If




        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Inicio
                    .Id = Nothing
                Case Else
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select




            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdDestinatario = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)
            .FechaEntrega = CDate(Me.txtFechaTomaInv.Text)


        End With

        Return objDocumento

    End Function

    Protected Sub btnGuardar_Detalle_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar_Detalle.Click
        registrarCambios_DetalleDocumento()
    End Sub

    Private Sub registrarCambios_DetalleDocumento()

        Try

            actualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAll(Function(D As Entidades.DetalleDocumento) D.IdProducto <= 0)

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            If ((New Negocio.TomaInventario).DocumentoTomaInventario_UpdateDetalle(objDocumento, Me.listaDetalleDocumento) > 0) Then

                Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                Me.GV_Detalle.DataBind()

                Try


                    LlenarPageIndexDoc(objDocumento.Id)
                Catch ex As Exception

                End Try

                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onload ", " calcularCantidades('1'); alert('El Proceso finalizó con éxito.'); ", True)

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        ActualizarDocumento()

    End Sub

    Private Sub ActualizarDocumento()

        Try

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            actualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Dim listaDocumento_Persona As List(Of Entidades.Documento_Persona) = obtenerListaDocumento_Persona()

            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

            objDocumento.Id = (New Negocio.TomaInventario).DocumentoTomaInventario_UpdateDocumento(objDocumento, Me.listaDetalleDocumento, objObservaciones, listaDocumento_Persona, listaRelacionDocumento)
            If (objDocumento.Id > 0) Then

                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)

        Me.listaDocumentoRef = getListaDocumentoRef()
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

            End With

            lista.Add(objRelacionDocumento)

        Next

        Return lista

    End Function
    Private Function obtenerListaDocumento_Persona() As List(Of Entidades.Documento_Persona)

        Dim lista As New List(Of Entidades.Documento_Persona)

        Me.listaEmpleado = getListaEmpleado()
        For i As Integer = 0 To Me.listaEmpleado.Count - 1

            Dim obj As New Entidades.Documento_Persona
            With obj

                .IdPersona = Me.listaEmpleado(i).IdPersona

            End With

            lista.Add(obj)

        Next

        Return lista

    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function

    Private Sub quitarDetalle(ByVal Index As Integer)
        Try

            actualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAt(Index)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalle(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub


    Protected Sub btnFiltrar_Filtro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFiltrar_Filtro.Click
        filtarDetalleDocumento()
    End Sub
    Private Sub filtarDetalleDocumento()
        Try

            Dim IdDocumento As Integer = 0
            If (IsNumeric(Me.hddIdDocumento.Value) And Me.hddIdDocumento.Value.Trim.Length > 0) Then
                IdDocumento = CInt(Me.hddIdDocumento.Value)
            End If

            Dim IndexPageDoc As Integer = 1
            If IsNumeric(Me.cboPageIndexDoc.SelectedValue) Then
                IndexPageDoc = CInt(Me.cboPageIndexDoc.SelectedValue)
            End If

            If (Me.GV_DocumentoRef.Rows.Count <= 0) Then

                Me.listaDetalleDocumento = (New Negocio.TomaInventario).DocumentoTomaInventario_FiltroDetalle(IdDocumento, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboSubLinea_Filtro.SelectedValue), "", Me.txtDescripcionProd_Filtro.Text, Me.txtCodigoProd_Filtro.Text, CInt(Me.CboTipoExistenciaDocumento.SelectedValue), IndexPageDoc - 1, CInt(Me.cboPageSizeDoc.SelectedValue))
                setListaDetalleDocumento(Me.listaDetalleDocumento)
                Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                Me.GV_Detalle.DataBind()
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

            Else


                '********************* OPERACIONES A REALIZAR < CONSOLIDADO >
                Dim ds As DataSet = (New Negocio.TomaInventario).DocumentoTomaInventario_FiltroDetalle_Consolidado(IdDocumento, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboSubLinea_Filtro.SelectedValue), "", Me.txtDescripcionProd_Filtro.Text, Me.txtCodigoProd_Filtro.Text, CInt(Me.CboTipoExistenciaDocumento.SelectedValue), 0, CInt(Me.cboPageSizeDoc.SelectedValue))

                Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
                setListaDetalleDocumento(Me.listaDetalleDocumento)

                Me.GV_Detalle_Consolidado.DataSource = ds.Tables("DT_Detalle")
                Me.GV_Detalle_Consolidado.DataBind()

                '***************** VERIFICAMOS LAS DIFERENCIAS
                For i As Integer = 0 To Me.GV_Detalle_Consolidado.Rows.Count - 1

                    Dim nColumn As Integer = Me.GV_Detalle_Consolidado.Rows(i).Cells.Count

                    Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 1).Font.Bold = True
                    Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 1).ForeColor = Drawing.Color.Red

                    Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 2).Font.Bold = True
                    Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 2).ForeColor = Drawing.Color.Red

                    Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 3).Font.Bold = True

                    Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 4).Font.Bold = True

                    If (CDec(Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 1).Text) <> 0 Or CDec(Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 2).Text) <> 0) Then

                        '************ FALTANTE<>0 AND SOBRANTE<>0
                        Me.GV_Detalle_Consolidado.Rows(i).BackColor = Drawing.Color.Yellow

                    End If

                Next

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            If (Me.GV_BusquedaAvanzado.Rows.Count > 0) Then
                objScript.onCapa(Me, "capaBusquedaAvanzado")
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "    alert('NO SE HALLARON REGISTROS.');   onCapa('capaBusquedaAvanzado');   ", True)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try

            cargarDocumentoTomaInventario(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoTomaInventario(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '**************** LIMPIAMOS EL FORMULARIO
        verFrm(FrmModo.Inicio, True, True, True, True, True)

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO
        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        Me.listaEmpleado = (New Negocio.Documento_Persona).Documento_PersonaSelectxIdDocumento(objDocumento.Id)
        Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumento.Id)


        LlenarPageIndexDoc(objDocumento.Id)
        Dim PageIndex As Integer = 0
        If Me.cboPageIndexDoc.Items.Count > 0 Then PageIndex = CInt(Me.cboPageIndexDoc.SelectedValue) - 1


        If (Me.listaDocumentoRef.Count > 0) Then

            '********************* OPERACIONES A REALIZAR < CONSOLIDADO >
            Dim ds As DataSet = (New Negocio.TomaInventario).DocumentoTomaInventario_FiltroDetalle_Consolidado(objDocumento.Id, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboSubLinea_Filtro.SelectedValue), "", Me.txtDescripcionProd_Filtro.Text, Me.txtCodigoProd_Filtro.Text, CInt(Me.CboTipoExistenciaDocumento.SelectedValue), PageIndex, CInt(Me.cboPageSizeDoc.SelectedValue))

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle_Consolidado.DataSource = ds.Tables("DT_Detalle")
            Me.GV_Detalle_Consolidado.DataBind()

            '***************** VERIFICAMOS LAS DIFERENCIAS
            For i As Integer = 0 To Me.GV_Detalle_Consolidado.Rows.Count - 1

                Dim nColumn As Integer = Me.GV_Detalle_Consolidado.Rows(i).Cells.Count

                Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 1).Font.Bold = True
                Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 1).ForeColor = Drawing.Color.Red

                Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 2).Font.Bold = True
                Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 2).ForeColor = Drawing.Color.Red

                Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 3).Font.Bold = True

                Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 4).Font.Bold = True

                If (CDec(Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 1).Text) <> 0 Or CDec(Me.GV_Detalle_Consolidado.Rows(i).Cells(nColumn - 2).Text) <> 0) Then

                    '************ FALTANTE<>0 AND SOBRANTE<>0
                    Me.GV_Detalle_Consolidado.Rows(i).BackColor = Drawing.Color.Yellow

                End If

            Next

            Me.chb_Consolidado.Checked = True

        Else
            'Me.listaDetalleDocumento = (New Negocio.DetalleDocumento).SelectxIdDocumento(objDocumento.Id)

            Me.listaDetalleDocumento = (New Negocio.TomaInventario).DocumentoTomaInventario_FiltroDetalle(objDocumento.Id, 0, 0, "", "", "", CInt(Me.CboTipoExistenciaDocumento.SelectedValue), PageIndex, CInt(Me.cboPageSizeDoc.SelectedValue))
            Me.chb_Consolidado.Checked = False

        End If

        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '*************** GUARDAMOS EN SESSION
        setListaDetalleDocumento(Me.listaDetalleDocumento)
        setListaDocumentoRef(Me.listaDocumentoRef)
        setListaEmpleado(Me.listaEmpleado)

        '************** 
        cargarDocumentoTomaInventario_GUI(objDocumento, Me.listaDocumentoRef, Me.listaEmpleado, Me.listaDetalleDocumento, objObservaciones)
        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

    End Sub

    Private Sub cargarDocumentoTomaInventario_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento As List(Of Entidades.Documento), ByVal listaEmpleado As List(Of Entidades.PersonaView), ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion)

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboAlmacen.Items.FindByValue(CStr(objDocumento.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            End If

            Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
            Me.txtCodigoDocumento.Text = .Codigo

            If (.FechaEmision <> Nothing) Then
                Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            End If

            If (.FechaEntrega <> Nothing) Then
                Me.txtFechaTomaInv.Text = Format(.FechaEntrega, "dd/MM/yyyy")
            End If

            Me.hddIdDocumento.Value = CStr(.Id)
            Me.hddCodigoDocumento.Value = .Codigo

        End With

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '**************** LISTA EMPLEADO
        Me.GV_Empleado.DataSource = listaEmpleado
        Me.GV_Empleado.DataBind()

        '**************** LISTA DETALLE
        Me.GV_Detalle.DataSource = listaDetalle
        Me.GV_Detalle.DataBind()

        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

    End Sub

#End Region

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try

            verFrm(FrmModo.Editar, False, False, False, False, False)

            Me.listaEmpleado = getListaEmpleado()
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDocumentoRef = getListaDocumentoRef()

            Me.GV_Empleado.DataSource = Me.listaEmpleado
            Me.GV_Empleado.DataBind()

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            cargarDocumentoTomaInventario(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click

        anularDocumento()

    End Sub

    Private Sub anularDocumento()

        Try


            If ((New Negocio.TomaInventario).DocumentoTomaInventario_AnularDocumento(CInt(Me.hddIdDocumento.Value))) Then

                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False)
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   alert('El proceso finalizó con éxito.');   calcularCantidades('1');  ", True)

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub chb_Consolidado_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chb_Consolidado.CheckedChanged

        actualizarControl_Consolidado(Me.chb_Consolidado.Checked)

    End Sub

    Private Sub actualizarControl_Consolidado(ByVal consolidado As Boolean)

        Try

            Me.btnBuscarDocumentoRef.Visible = consolidado
            Me.Panel_DocumentoRef.Enabled = consolidado
            Me.Panel_Empleado.Enabled = consolidado

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.TomaInventario).DocumentoTomaInventario_SelectDocRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), serie, codigo, fechaInicio, fechafin)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try
            validar_AddDocumentoRef(IdDocumentoRef)

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setListaDocumentoRef(Me.listaDocumentoRef)

            '******************* CARGAMOS LA LISTA DE DOCUMENTOS REF
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            '****************** CARGAMOS LA LISTA DE EMPLEADOS 
            Me.listaEmpleado = getListaEmpleado()
            Dim listaEmpleadoRef As List(Of Entidades.PersonaView) = (New Negocio.Documento_Persona).Documento_PersonaSelectxIdDocumento(IdDocumentoRef)

            For i As Integer = listaEmpleadoRef.Count - 1 To 0 Step -1

                For j As Integer = 0 To Me.listaEmpleado.Count - 1

                    If (listaEmpleadoRef(i).IdPersona = Me.listaEmpleado(j).IdPersona) Then

                        listaEmpleadoRef.RemoveAt(i)

                        Exit For

                    End If

                Next

            Next

            Me.listaEmpleado.AddRange(listaEmpleadoRef)
            setListaEmpleado(Me.listaEmpleado)

            Me.GV_Empleado.DataSource = Me.listaEmpleado
            Me.GV_Empleado.DataBind()

            Me.cboEmpresa.Enabled = False
            Me.cboTienda.Enabled = False
            Me.cboAlmacen.Enabled = False

            Dim hecho As Boolean = False
            For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

                For j As Integer = 0 To Me.listaDocumentoRef.Count - 1

                    If (Me.listaDocumentoRef(i).FechaEntrega = Me.listaDocumentoRef(j).FechaEntrega) Then
                        Me.lblCad_DocumentoRef.Visible = False
                    Else
                        Me.lblCad_DocumentoRef.Visible = True
                        hecho = True
                        Exit For

                    End If

                Next

                If (hecho = True) Then
                    Exit For
                End If

            Next

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub


    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getListaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setListaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            If (Me.listaDocumentoRef.Count <= 0) Then
                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboAlmacen.Enabled = True
            Else
                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboAlmacen.Enabled = False
            End If

            Dim hecho As Boolean = False
            For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

                For j As Integer = 0 To Me.listaDocumentoRef.Count - 1

                    If (Me.listaDocumentoRef(i).FechaEntrega = Me.listaDocumentoRef(j).FechaEntrega) Then
                        Me.lblCad_DocumentoRef.Visible = False
                    Else
                        Me.lblCad_DocumentoRef.Visible = True
                        hecho = True
                        Exit For

                    End If

                Next

                If (hecho = True) Then
                    Exit For
                End If

            Next

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region

    Protected Sub btnInicio_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnInicio.Click
        Try

            verFrm(FrmModo.Inicio, True, True, True, True, True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaTomaInv.Text = Me.txtFechaEmision.Text
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto
        Me.chb_Consolidado.Checked = False


        '************** DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        '************* EMPLEADOS
        Me.GV_Empleado.DataSource = Nothing
        Me.GV_Empleado.DataBind()


        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

        '*********** DETALLE CONSOLIDADO
        Me.GV_Detalle_Consolidado.DataSource = Nothing
        Me.GV_Detalle_Consolidado.DataBind()


        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '************** HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""


        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
        Me.gvBuscar.DataSource = Nothing
        Me.gvBuscar.DataBind()

        '***************
        Me.cboLinea_Filtro.SelectedValue = "0"
        If Not IsNothing(Me.cboSubLinea_Filtro.Items.FindByValue("0")) Then
            Me.cboSubLinea_Filtro.SelectedValue = "0"
        End If
        Me.txtCodigoProd_Filtro.Text = ""
        Me.txtDescripcionProd_Filtro.Text = ""

    End Sub

    Private Sub btnRecalcularSaldos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecalcularSaldos.Click
        recalcularSaldos()
    End Sub
    Private Sub recalcularSaldos()

        Try

            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

            If ((New Negocio.TomaInventario).RecalcularSaldosDocTomaInv(IdDocumento, CDate(Me.txtFechaTomaInv.Text), -1)) Then

                Me.GV_Detalle.DataSource = Nothing
                Me.GV_Detalle.DataBind()

                Me.GV_Detalle_Consolidado.DataSource = Nothing
                Me.GV_Detalle_Consolidado.DataBind()

                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objCbo.LlenarCboTotalPag_LineaxSubLinea(Me.cboPageIndex, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboPageSize.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)

            objcbo.LlenarCboTotalPag_LineaxSubLinea(Me.cboPageIndex, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboPageSize.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub CboTipoExistenciaDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistenciaDocumento.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cboLinea_Filtro, CInt(CboTipoExistenciaDocumento.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(CboTipoExistenciaDocumento.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPageSize.SelectedIndexChanged

        Try
            Dim objcbo As New Combo

            objcbo.LlenarCboTotalPag_LineaxSubLinea(Me.cboPageIndex, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboPageSize.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub imgBuscarCodigoProducto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim RowIndex As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex
        Dim IdDocumento As Integer = 0
        Dim CodigoProducto As String = CType(Me.GV_Detalle.Rows(RowIndex).FindControl("txtCodigoProducto"), TextBox).Text.Trim

        If (IsNumeric(Me.hddIdDocumento.Value) And Me.hddIdDocumento.Value.Trim.Length > 0) Then
            IdDocumento = CInt(Me.hddIdDocumento.Value)
        End If


        Try

            cargarProducto_Detalle_AddProducto(False, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), 0, 0, 0, IdDocumento, CDate(Me.txtFechaTomaInv.Text), CodigoProducto, RowIndex)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try



    End Sub

    Private Sub btnGenerar_Lista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerar_Lista.Click

        Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To CInt(Me.cboGenerarLista.SelectedValue) - 1

            Me.listaDetalleDocumento.Add((New Entidades.DetalleDocumento()))


        Next

        Me.setListaDetalleDocumento(Me.listaDetalleDocumento)

        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()

    End Sub


    Private Sub cboPageSizeDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPageSizeDoc.SelectedIndexChanged

        Dim IdDocumento As Integer = 0
        If (IsNumeric(Me.hddIdDocumento.Value) And Me.hddIdDocumento.Value.Trim.Length > 0) Then
            IdDocumento = CInt(Me.hddIdDocumento.Value)
        End If

        LlenarPageIndexDoc(IdDocumento)
    End Sub

    Private Sub LlenarPageIndexDoc(ByVal IdDocumento As Integer)
        Try
            Dim objcbo As New Combo

            objcbo.LlenarCboTotalPag_DetalleDocumento(Me.cboPageIndexDoc, IdDocumento, CInt(Me.cboPageSizeDoc.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboPageIndexDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPageIndexDoc.SelectedIndexChanged
        filtarDetalleDocumento()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        ExportaExcel(Me.GV_Detalle_Consolidado, "Inv" + cboSerie.SelectedItem.Text + Me.txtCodigoDocumento.Text)
    End Sub

    Public Sub ExportaExcel(ByVal grilla As GridView, ByVal FilleNameExt As String)

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form As New HtmlForm

        grilla.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(grilla)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("content-disposition", "attachment;filename=" + FilleNameExt)
        Response.Charset = "UTF-8"

        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()

    End Sub


End Class
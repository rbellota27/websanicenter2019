﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmValorReferencial_Transporte.aspx.vb" Inherits="APPWEB.FrmValorReferencial_Transporte" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" Width="70px" runat="server" Text="Nuevo" ToolTip="Nuevo" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="  return( valOnClick_btnGuardar()  ); "
                                Width="70px" runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar" Width="70px" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                VALOR REFERENCIAL - TRANSPORTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Registro" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Descripción:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescripcion" Width="450px" Font-Bold="true" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Valor Ref.:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboMoneda" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtValorReferencial" Text="0" Width="70px" Font-Bold="true" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" onBlur="onBlurTextTransform(this,configurarDatos); return( valBlur(event) ); "
                                                runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Estado:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rdbEstado" CssClass="Texto" Font-Bold="true" runat="server"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Origen:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 20px;">
                                        </td>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Depto.:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboDepto_Origen" runat="server" AutoPostBack="true" Width="210px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Prov.:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboProvincia_Origen" runat="server" AutoPostBack="true" Width="210px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Distrito:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboDistrito_Origen" runat="server" Width="210px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Destino:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 20px;">
                                        </td>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Depto.:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboDepto_Destino" runat="server" AutoPostBack="true" Width="210px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Prov.:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboProvincia_Destino" runat="server" AutoPostBack="true" Width="210px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Distrito:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboDistrito_Destino" runat="server" Width="210px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Busqueda" runat="server">
                    <table width="100%">
                        <tr>
                            <td class="SubTituloCelda">
                                BÚSQUEDA
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Descripción:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDescripcion_Busqueda" runat="server" Width="300px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" onKeyPress="return ( valonKeyPressBuscar(this) );"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" ToolTip="Buscar" Width="70px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Estado:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rdbEstado_Busqueda" runat="server" CssClass="Texto" Font-Bold="true"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_ValorReferencial" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEditar" runat="server" OnClick="btnEditar_Click">Editar</asp:LinkButton>
                                                <asp:HiddenField ID="hddIdValorReferencial" Value='<%# DataBinder.Eval(Container.DataItem,"IdValorReferenciaT") %>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMoneda" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Moneda") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMonto" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Monto","{0:F3}") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Estado" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chbEstado" Enabled="false" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdValorReferencial" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function valOnClick_btnGuardar() {

            var txtDescripcion = document.getElementById('<%=txtDescripcion.ClientID%>');

            if (txtDescripcion.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACIÓN.');
                txtDescripcion.select();
                txtDescripcion.focus();
                return false;
            }

            return confirm('Desea continuar con la Operación ?');
        }
        ///////
        function valonKeyPressBuscar(obj) {
            if (event.Keycode == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscar.ClientID %>').focus();
                return true;
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>

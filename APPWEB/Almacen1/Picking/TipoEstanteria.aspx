﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="TipoEstanteria.aspx.vb" Inherits="APPWEB.TipoEstanteria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div>
    <table class="style1">
        <tr>
            <td style="text-align: center; color: #0000FF; text-decoration: underline">
                <strong>Tipo Estanteria</strong></td>
        </tr>
        <tr>
            <td>
               <div id="CapaTipoEstanteria" >
               </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</div>

<div id="CapaMantenimiento" style="border: 3px solid blue; padding: 4px; width: 750px;
            height: 267px; position: absolute; top: 0px; left: 250px; background-color: #EFFBFB;
            z-index: 3; display:none;">
             <table border="0" cellspacing="0px" style="width: 750px">
                   <tr>
                    <td>
                          <table border="0">
                                        <tr class="BarraTitulo">
                                        <td style="width:90%; background-color: #0066CC;"><span id="TituloPopup"></span></td>
                                        <td style="width:4%"><img src='../../ImagenesForm/imgcerrar.png' class="Iconoventanas" 
                                                title="Cerrar Ventana" onclick="return cerrardetalle()"/></td>
                                        </tr>
                          </table>
                    </td>
                    </tr>
                    <tr>
                    <td>

             <div id="TablaMantenimiento">
                 <table class="style1" style="width: 98%">
        <tr>
            <td colspan="3" style="text-align: right">
                <b></b>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; width: 171px">
                <b>Id :</b></td>
            <td style="width: 314px">
                <input id="txtId" type="text" readonly/></td>
            <td rowspan="7">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: right; width: 171px">
                <b>Nombre Tipo Estanteria :</b></td>
            <td style="width: 314px">
                <input id="txtNombreTipoEstanteria" style="width: 307px" type="text" /></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 171px">
                <b>Codigo Estanteria :</b></td>
            <td style="width: 314px">
                <input id="txtcodigoEstanteria" type="text" /></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 171px">
                <b>Estado :</b></td>
            <td style="width: 314px">
                 <select id="cboEstado" name="D2" style="width: 182px">
                                <option></option>
                            </select></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 171px">
                <b>Ubicacion :</b></td>
            <td style="width: 314px">
                <input id="txtUbicacion" type="text" /></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 171px">
                <b>Observacion :</b></td>
            <td style="width: 314px">
                <input id="txtObservacion" type="text" style="width: 304px" /></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 171px">
                <b>Imagen :</b></td>
            <td style="width: 314px">
                <input id="txtrutaImagen" type="text" style="width: 309px" /></td>
        </tr>
    </table>
      </td>
         </tr>
                    </table>
                

             </div>

<script type="text/javascript" language="javascript">


    window.onload = varios();


    function varios() {
        ConsultaTipoEstanteria();

    }


    function ConsultaTipoEstanteria() {

        var opcion = "ConsultaRegistrosTipoEstanteria";

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "TipoEstanteria.aspx?flag=" + opcion, true);

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                fun_listaTipoEstanteria(xhr.responseText);
            }
        }
        xhr.send(null);
        return false;
    }




    function fun_listaTipoEstanteria(lista) {

        filas = lista.split("▼");
        crearTabla();
        crearMatriz();
        mostrarMatriz();

    }

    function crearTabla() {
        var nRegistros = filas.length;
        var cabeceras = ["", "IdTipoEstanteria", "NombreTipoEstanteria", "CodEstanteria", "Estado", "ParametroUbic", "Observacion", "Imagen"];
        var nCabeceras = cabeceras.length;
        var contenido = "<table><thead>";
        contenido += "<tr class='GrillaHeader'>";

        for (var j = 0; j < nCabeceras; j++) {

            if (j == 0) {
                contenido += "<th>";
                contenido += "<input id='btnNuevo' type='image' img src='../../ImagenesForm/imgnuevo.png' onclick='return NuevoTipoEstanteria()' >";
                contenido += "</th>";
            }
            if (j > 0) {
                contenido += "<th>";
                contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                contenido += "</th>";
            }
        }
        contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
        contenido += "</table>";
        var div = document.getElementById("CapaTipoEstanteria");
        div.innerHTML = contenido;

    }


    function crearMatriz() {
        matriz = [];
        var nRegistros = filas.length;
        var nCampos;
        var campos;
        var c = 0;
        var exito;
        var textos = document.getElementsByClassName("texto");
        var nTextos = textos.length;
        var texto;
        var x;
        for (var i = 0; i < nRegistros; i++) {
            exito = true;
            campos = filas[i].split("|");
            nCampos = campos.length;
            for (var j = 0; j < nTextos; j++) {
                texto = textos[j];
                x = j;
                if (x < 10) {
                    if (texto.value.length > 0) {
                        if (campos[x] != campos[0]) {
                            exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                    }
                }
                if (x == 10 && x < 13) {
                    if (texto.value.length > 0) {
                        if (campos[x] != campos[0]) {
                            exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                    }
                }

                if (!exito) break;
            }
            if (exito) {
                matriz[c] = [];
                for (var j = 0; j < nCampos; j++) {
                    if (isNaN(campos[j])) matriz[c][j] = campos[j];

                    else matriz[c][j] = campos[j];
                }
                c++;
            }
        }
    }


    function mostrarMatriz() {
        var nRegistros = matriz.length;
        var contenido = "";
        if (nRegistros > 0) {
            var nCampos = matriz[0].length;
            for (var i = 0; i < nRegistros; i++) {

                for (var j = 0; j < nCampos; j++) {

                    if (j == 0) {
                        contenido += "<td>";
                        contenido += "<input id='btnmas' type='image' img src='../../ImagenesForm/imgeditar.png' onclick='return MostrarDetalle(";
                        contenido += matriz[i][0];
                        contenido += ',"' + matriz[i][1] + '"';
                        contenido += "," + '"' + matriz[i][2] + '"';
                        contenido += "," + '"' + matriz[i][3] + '"';
                        contenido += "," + '"' + matriz[i][4] + '"';
                        contenido += "," + '"' + matriz[i][5] + '"';
                        contenido += ")' >";
                        contenido += "</td>";
                        contenido += "<td>";
                        contenido += matriz[i][j];
                        contenido += "</td>";
                    }

                    if (j > 0) {
                        contenido += "<td>";
                        contenido += matriz[i][j];
                        contenido += "</td>";
                    }
                }
                contenido += "</tr>";
            }
        }
        var spnMensaje = document.getElementById("nroFilas");
        var tabla = document.getElementById("tbDocumentos");
        tabla.innerHTML = contenido;
    }



    function NuevoTipoEstanteria() {

        document.getElementById('CapaMantenimiento').style.display = 'block';
        document.getElementById('txtId').value = "0";
        document.getElementById('txtNombreTipoEstanteria').value = "";
        document.getElementById('txtcodigoEstanteria').value = "";
//        document.getElementById('cboEstado').value = "";
        document.getElementById('txtUbicacion').value = "";
        document.getElementById('txtObservacion').value = "";
        document.getElementById('txtrutaImagen').value = "";

        return false;

    }

   

    function MostrarDetalle(Id, NombreTipoEstanteria, codigoEstanteria,Estado,ubicacion,observacion,rutaimagen) {

        document.getElementById('CapaMantenimiento').style.display = 'block';

        document.getElementById('txtId').value = Id;
        document.getElementById('txtNombreTipoEstanteria').value = NombreTipoEstanteria;
        document.getElementById('txtcodigoEstanteria').value = codigoEstanteria;
        document.getElementById('txtUbicacion').value = ubicacion;
        document.getElementById('txtObservacion').value = observacion;
        document.getElementById('txtrutaImagen').value = rutaimagen;

        if (Estado > 0) {
            document.getElementById('cboEstado').value = Estado;
        }

        return false;

    }


    function cerrardetalle() {
        document.getElementById('CapaMantenimiento').style.display = 'none';
        ConsultaTipoEstanteria();
        return false;
    }



    
</script>






</asp:Content>

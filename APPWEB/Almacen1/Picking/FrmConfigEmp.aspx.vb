﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador

Public Class FrmConfigEmp
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass

    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Usuario As Integer = CInt(Session("IdUsuario"))
        Dim flag As String = Request.QueryString("flag")

        Select Case flag
            Case "ConsultaPersonal"
                Dim lista As New List(Of bePersonalMilla)
                Dim rptapersonal As String = ""
                Try
                    lista = (New blPersonal).listaPersMilla()

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptapersonal = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptapersonal)
                    Response.End()
                Else
                    Response.Write(rptapersonal)
                    Response.End()
                End If


            Case "ConsultaEmpleados"
                Dim lista As New List(Of bePersonal)
                Dim rptapersonal As String = ""
                Try
                    lista = (New blPersonal).listarPersonal()

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptapersonal = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptapersonal)
                    Response.End()
                Else
                    Response.Write(rptapersonal)
                    Response.End()
                End If


            Case "GuardarPersonal"
        
                Dim txtId As String = Request.QueryString("txtId")
                Dim txtNombrePersonal As String = Request.QueryString("txtNombrePersonal")

                Dim lista As New List(Of bePersonalMilla)
                Dim rpta As String = ""
                Try

                    lista = (New blPersonalMilla).GuardarPersonal(txtId, txtNombrePersonal)

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If
               

        End Select




    End Sub

End Class
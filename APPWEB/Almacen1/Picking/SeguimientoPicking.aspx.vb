﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador

Public Class SeguimientoPicking
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass


    Dim DetCodigo As String
    Dim DetMontoDetraccion As String
    Dim DetEstadoDet As String
    Dim DetIDDocumentoGenera As String

    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim Usuario As Integer = CInt(Session("IdUsuario"))
        
        Dim flag As String = Request.QueryString("flag")
        Select Case flag
            Case "ConsultaOrdenes"

                Dim lista As New List(Of OrdenPedidoPicking)
                Dim rptaTipope As String = ""
                Try
                    lista = (New blOrdenPedidoPicking).ListarOrdenPedido(1, 7, "20/02/2018", "20/02/2018")
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptaTipope = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptaTipope)
                    Response.End()
                Else
                    Response.Write(rptaTipope)
                    Response.End()
                End If
        End Select




    End Sub

End Class
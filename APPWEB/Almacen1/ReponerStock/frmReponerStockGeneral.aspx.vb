﻿Imports System.Linq
Imports Negocio
Imports Entidades
Imports general.librerias.serializador
Imports System.IO
Imports Microsoft.Office.Interop
Imports System.Reflection
Imports System.ComponentModel

Public Class frmReponerStockGeneral
    Inherits System.Web.UI.Page
    Dim objScript As New ScriptManagerClass

    Public Sub listaReposicion()
        Dim sb As New StringBuilder()
        Dim txtFechaInicio As String = Request.QueryString("txtInicio")
        Dim txtFechaFin As String = Request.QueryString("txtFin")
        Dim idEmpresa As String = Request.QueryString("idEmpresa")
        Dim idTienda As String = Request.QueryString("idTienda")

        Dim rpta As String = ""
        Dim lista As List(Of Entidades.be_listaReponerStock)
        lista = (New bl_reposicionStock).listar_reposicionStock_lista(txtFechaInicio, txtFechaFin, idTienda, idEmpresa, "7,20000016")
        Session("reponerStock") = lista
        If lista IsNot Nothing And lista.Count > 0 Then
            rpta = (New objeto).SerializarLista(lista, "|", ",", True, "", False)
            rpta.Split(",")
            Response.Write(rpta)
            Response.End()
            'Return rpta
        End If
    End Sub

    Public Sub CrearCotizaciones()
        Dim respuesta As Boolean = False
        Dim lista As New List(Of be_reposicionStock)
        Dim objeto As be_reposicionStock = Nothing
        Dim idUsuario As Integer = 0
        idUsuario = Session("IdUsuario")

        Dim cadena As String = Request.Form("datos2")
        Dim filas As Array = cadena.Split("▼")
        Dim nroFilas As Integer = filas.Length
        Dim columnas As Array
        Dim nroColumnas As Integer = 0
        For i As Integer = 0 To nroFilas - 1
            objeto = New be_reposicionStock
            columnas = filas(i).ToString.Split("|")
            nroColumnas = columnas.Length
            With objeto
                .idDocumento = 0
                .idProducto = CInt(columnas(0))
                .idUnidadMedida = CInt(columnas(1))
                .cantidadProducto = CDec(columnas(2))
            End With
            lista.Add(objeto)
        Next
        If lista.Count > 0 And lista IsNot Nothing Then
            respuesta = (New bl_reposicionStock).CrearCotizacion(idUsuario, lista)
        End If
        If respuesta Then
            Response.Write("Se generó una cotización con " & nroFilas & " productos seleccionados.")
            Response.End()
        Else
            Response.Write("Ocurrió un error. Cerrar sesión y volver a intentar, si persiste el error deberá reportarlo.")
            Response.End()
        End If

    End Sub

    Public Function listaLoadForm(Of T)(ByVal lista As List(Of T)) As String
        Dim rpta As String = ""
        If lista IsNot Nothing And lista.Count > 0 Then
            rpta = (New objeto).SerializarLista(lista, "|", ";", False, "", False)
        End If
        Return rpta
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.btnPruebas)

        Dim flag_registrar As String = Request.Form("flag")
        Dim flagbtn As String = Request.QueryString("flag")

        'Si encuentra la letra C tiene que registrar cotizaciones
        Select Case flag_registrar
            Case "C"
                CrearCotizaciones()
        End Select

        'Si encuentra la letra G tiene que calcular reposición
        Select Case flagbtn
            Case "G"
                listaReposicion()
        End Select

        If Not IsPostBack Then
            Dim fechaActual As DateTime = DateTime.Today
            Dim primerDiaDelMes As Date = New DateTime(fechaActual.Year, fechaActual.Month, 1)
            Dim ultimoDiaDelMes As Date = New DateTime(fechaActual.Year, IIf(fechaActual.Month = 12, 12, fechaActual.Month + 1), 1).AddDays(-1)

            '98 dias en 14 semanas
            'Dim wD As Long = DateDiff(DateInterval.Weekday, fechaActual.AddDays(-98), fechaActual)

            Me.txtFecInicio.Text = fechaActual.AddDays(-98)
            Me.txtFecFin.Text = fechaActual
            Dim id As String = Request.QueryString("idUsuario")
            If id IsNot Nothing Then
                Dim obj As New be_reponerStockListarObjetos
                'Dim idUsuario As Integer = Session("IdUsuario")
                obj = (New bl_reposicionStock).listarObjetosReposicionStock(id)

                Dim rpta As String = ""
                rpta = listaLoadForm(obj.listaTienda)
                rpta += "*"
                rpta += listaLoadForm(obj.listaEmpresa)
                rpta += "*"
                rpta += listaLoadForm(obj.listaAlmacen)
                Response.Write(rpta)
                Response.End()
            End If
        End If
    End Sub

    Private Sub llenarCombo(Of T)(ByVal combo As DropDownList, ByVal lista As List(Of T), Optional flag As Boolean = False, Optional value As Integer = 0)
        combo.DataSource = lista
        combo.DataValueField = "campo1"
        combo.DataTextField = "campo2"
        combo.DataBind()
        If flag Then
            combo.SelectedValue = value
        End If
    End Sub

    Private Sub exportar(dt As DataTable)
        Dim grilla As New GridView()
        grilla.DataSource = dt
        grilla.DataBind()

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        'grilla.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(grilla)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=data.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())

        Response.End()
    End Sub

    Private Sub btnPruebas_Click(sender As Object, e As System.EventArgs) Handles btnPruebas.Click

        Dim dt As DataTable
        Dim lista As New List(Of Entidades.be_listaReponerStock)
        lista = Session("reponerStock")
        dt = ToDataTable(lista)
        exportar(dt)
    End Sub


    '<System.Runtime.CompilerServices.Extension()> _
    Public Shared Function ToDataTable(Of T)(data As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, prop.PropertyType)
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In data
            For i As Integer = 0 To values.Length - 1
                values(i) = props(i).GetValue(item)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function

End Class
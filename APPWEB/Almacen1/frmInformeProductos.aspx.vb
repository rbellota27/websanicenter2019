﻿Public Class frmInformeProductos
    Inherits System.Web.UI.Page

#Region "Variables"
    Private objScript As New ScriptManagerClass
    Private drop As New Combo
    Private obj_OrdenCompraPre As New Negocio.OrdenCompraPreliminar
    Private ListaProductos As List(Of Entidades.DetalleDocumentoView)
    Private listaKit As List(Of Entidades.Kit)
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Public estado As New Entidades.Estado


    Private Enum operativo
        Onload = 0
        Nuevo = 1
        Buscar = 2
        Buscar_Exito = 3
        Editar = 4
        Buscar_Inf = 5
        GuardarInf = 6
    End Enum
#End Region


    Private Function getListaKit() As List(Of Entidades.Kit)
        Return CType(Session.Item(CStr(ViewState("ListaKit"))), List(Of Entidades.Kit))
    End Function
    Sub setListaKit(ByVal Lista As List(Of Entidades.Kit))
        Session.Remove(CStr(ViewState("ListaKit")))
        Session.Add(CStr(ViewState("ListaKit")), Lista)
    End Sub

    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumentoView)
        Return CType(Session.Item(CStr(ViewState("DetalleDocumento"))), List(Of Entidades.DetalleDocumentoView))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumentoView))
        Session.Remove(CStr(ViewState("DetalleDocumento")))
        Session.Add(CStr(ViewState("DetalleDocumento")), lista)
    End Sub

    Private Property ListaDetalleDocumentoView() As List(Of Entidades.DetalleDocumentoView)
        Get
            Return CType(Session(CStr(ViewState("DetalleDocumento"))), List(Of Entidades.DetalleDocumentoView))
        End Get
        Set(ByVal value As List(Of Entidades.DetalleDocumentoView))
            Session.Remove(CStr(ViewState("DetalleDocumento")))
            Session.Add(CStr(ViewState("DetalleDocumento")), value)
        End Set
    End Property


#Region "Limpiar formulario"

    Private Sub Limpiar()
        '==== Limpiar Valores Decimales
        tbProveedor.Text = ""
        tbRuc.Text = ""
        gvProducto.DataBind()

        hdd_Idproveedor.Value = "0"
        hddIdDocumento.Value = "0"
        hdd_IdDocRelacionado.Value = "0"
        hdd_MovCuenta.Value = "0"

        For Each radio As RadioButton In rbtipodeoc.Controls
            radio.Checked = False
        Next

        ListaProductos = New List(Of Entidades.DetalleDocumentoView)
        setListaKit(Nothing)

    End Sub

    Private Sub LimpiarCombos()

        dlTienda.Items.Clear()
        dlSerie.Items.Clear()

    End Sub

#End Region

    Private Sub GenerarCodigoDocumento()
        If (IsNumeric(Me.dlSerie.SelectedValue)) Then
            Me.tbCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.dlSerie.SelectedValue))
        Else
            Me.tbCodigoDocumento.Text = ""
        End If
    End Sub

#Region "HabilitarBotones"

    Private Sub habilitarControles(ByVal valor As Boolean)
        dlTienda.Enabled = valor
        dlSerie.Enabled = valor
        tbCodigoDocumento.Enabled = valor
    End Sub

    Private Sub habilitarControles2(ByVal valor As Boolean)
        TextMes.Enabled = valor
        Txtpais.Enabled = valor
        dlPropietario.Enabled = valor
        dlTienda.Enabled = valor

    End Sub

    Private Sub MostrarBotones()

        Select Case CInt(Me.hddOperativo.Value)

            Case operativo.Onload

                pnlDetalle.Enabled = False
                btbuscar.Visible = True
                btBuscarI.Visible = True
                btbuscar.Enabled = True
                btBuscarI.Enabled = True
                bteditar.Visible = False
                btnCancelar.Visible = False
                btguardar.Visible = False
                btnBuscarDocumento.Visible = False
                btnAgregar.Visible = False
                habilitarControles(True)


            Case operativo.Nuevo

                pnlDetalle.Enabled = True
                btbuscar.Visible = False
                bteditar.Visible = False
                btguardar.Visible = True
                btBuscarI.Visible = True
                'btnAgregar.Visible = False
                btnBuscarDocumento.Visible = False
                habilitarControles(True)
                GenerarCodigoDocumento()

            Case operativo.Buscar

                pnlDetalle.Enabled = True
                btnCancelar.Visible = True
                btbuscar.Visible = True
                bteditar.Visible = False
                btguardar.Visible = False
                'btnAgregar.Visible = False
                btbuscar.Enabled = False
                btBuscarI.Visible = False
                btnBuscarDocumento.Visible = True


            Case operativo.Buscar_Exito

                pnlDetalle.Enabled = False
                Panel_Detalle_producto.Enabled = False
                btbuscar.Visible = False
                btBuscarI.Visible = False
                bteditar.Visible = True
                btguardar.Visible = False
                'btnAgregar.Visible = False
                btnBuscarDocumento.Visible = False

            

            Case operativo.Editar

                pnlDetalle.Enabled = True
                Panel_Detalle_producto.Enabled = True
                btbuscar.Visible = False
                btBuscarI.Visible = False
                bteditar.Visible = False
                btnAgregar.Visible = True
                btguardar.Visible = True
                btnBuscarDocumento.Visible = False
                habilitarControles2(False)


            Case operativo.Buscar_Inf

                pnlDetalle.Enabled = True
                btnCancelar.Visible = True
                btbuscar.Visible = False
                bteditar.Visible = False
                btguardar.Visible = False
                btBuscarI.Visible = True
                btBuscarI.Enabled = False
                btnBuscarDocumento.Visible = True

            Case operativo.GuardarInf

                pnlDetalle.Enabled = False
                Panel_Detalle_producto.Enabled = False
                btbuscar.Visible = True
                btBuscarI.Visible = True
                bteditar.Visible = False
                btguardar.Visible = False
                btnBuscarDocumento.Visible = False


        End Select

    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Onload_OrdenCompra()
    End Sub

    Private Sub Onload_OrdenCompra()
        If Not IsPostBack Then
            hdd_IdUsuario.Value = CStr(Session("IdUsuario"))

            Call JavaScript()
            onLoad_Session()
            ConfigurarDatos()

            If (hdd_IdUsuario.Value <> "" Or hdd_IdUsuario.Value <> Nothing) Then
                ValidarPermisos()
            Else
                hdd_IdUsuario.Value = CStr(Request.QueryString("IdUsuario"))
            End If

            OnInit_OrdenCompra()

            hddOperativo.Value = CStr(operativo.Onload)
            verOrdenCompra(False, True, False)

            If Request.QueryString("IdDocumento") IsNot Nothing Then
                Me.OnClick_BuscarDocumento(CInt(Request.QueryString("IdDocumento")))
                Hide_Button()
            End If

        End If
    End Sub

    Protected Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumento.Click
        Try

            OnClick_BuscarDocumento()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub


    Private Sub OnClick_BuscarDocumento(Optional ByVal IdDocumento As Integer = 0)

        If btbuscar.Visible = True Then
            ' ********************************** ENTIDADES 
            Dim obj_Documento As Entidades.Documento = (New Negocio.OrdenCompra).SelectOrdenCompra(CInt(Me.dlSerie.SelectedValue), CInt(tbCodigoDocumento.Text), IdDocumento)
            Dim obj_Observacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(obj_Documento.Id)

            'Dim PoseeCosteoImportacion As Nullable(Of Integer)
            'PoseeCosteoImportacion = (New Negocio.OrdenCompra).Validar_CosteoImportacion(obj_Documento.Id)

            'If PoseeCosteoImportacion <= 0 Then
            '    Throw New Exception("EL DOCUMENTO NO POSEE COSTEO DE IMPORTACION... NO SE PUEDE CONTINUAR ")
            'End If
            'Dim PoseeGuiaRecepcion As Nullable(Of Integer)
            'PoseeGuiaRecepcion = (New Negocio.OrdenCompra).Validar_guiaRecepcion(obj_Documento.Id)

            'If PoseeGuiaRecepcion <= 0 Then
            '    Throw New Exception("EL DOCUMENTO NO POSEE GUIA DE RECEPCION... NO SE PUEDE CONTINUAR ")
            'End If
            Dim PoseeInforme As Nullable(Of Integer)
            PoseeInforme = (New Negocio.OrdenCompra).Validar_Informe(obj_Documento.Id)
            If PoseeInforme > 0 Then
                Throw New Exception("EL DOCUMENTO DE REFERENCIA YA CUENTA CON UN INFORME ")
            End If


            ' ********************************** LISTAS
            Dim list_CondicionComercial As List(Of Entidades.CondicionComercial) = (New Negocio.Documento_CondicionC).SelectxIdDocumento(obj_Documento.Id)
            Dim list_DetalleDocumentoView As List(Of Entidades.DetalleDocumentoView) = (New Negocio.OrdenCompra).DetalleDocumentoViewSelectxIdDocumento2(obj_Documento.Id)
            list_DetalleDocumentoView = obtenerTonos(obj_Documento.Id)

            CargarOrdenCompra_GUI(obj_Documento, list_DetalleDocumentoView, list_CondicionComercial, obj_Observacion, listaDetalleDocumento)
            hddOperativo.Value = CStr(operativo.Buscar_Exito)
            verOrdenCompra(False, False, False)

        Else

            Dim obj_Documento As Entidades.Documento = (New Negocio.DocInformeProducto).SelectInforme(CInt(Me.dlSerie.SelectedValue), CInt(tbCodigoDocumento.Text))
            Dim PoseeRegularizacion As Nullable(Of Integer)
      

            Dim list_DetalleDocumentoView As List(Of Entidades.DetalleDocumentoView) = (New Negocio.DocInformeProducto).DetalleInformeProducto(obj_Documento.Id)
            list_DetalleDocumentoView = obtenerTonos2(obj_Documento.Id)
            CargarInforme_GUI(obj_Documento, list_DetalleDocumentoView, listaDetalleDocumento)
            hddOperativo.Value = CStr(operativo.Buscar_Exito)
            verOrdenCompra(False, False, False)

        End If




    End Sub


    Private Function obtenerTonos(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)

        Dim listaTono As List(Of Entidades.Tono)
        Dim listaDetalle As List(Of Entidades.DetalleDocumentoView) = (New Negocio.OrdenCompra).DetalleDocumentoViewSelectxIdDocumento2(IdDocumento)
        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).listaTo = (New Negocio.bl_Tonos).SelectTonos(listaDetalle(i).IdProducto)
            listaTono = listaDetalle(i).listaTo
        Next

        Return listaDetalle

    End Function

    Private Function obtenerTonos2(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)

        Dim listaTono As List(Of Entidades.Tono)
        Dim listaDetalle As List(Of Entidades.DetalleDocumentoView) = (New Negocio.DocInformeProducto).DetalleInformeProducto(IdDocumento)
        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).listaTo = (New Negocio.bl_Tonos).SelectTonos2(listaDetalle(i).IdProducto, IdDocumento)
            listaTono = listaDetalle(i).listaTo
        Next

        Return listaDetalle

    End Function

    Private Sub verOrdenCompra(ByVal LimpiarFrm As Boolean, ByVal InicializarFrm As Boolean, ByVal IniSesion As Boolean)

        If LimpiarFrm Then
            Me.Limpiar()
        End If

        If InicializarFrm Then

            GenerarCodigoDocumento()
            Me.tbFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")


        End If

        If IniSesion Then
            Me.ListaDetalleDocumentoView = New List(Of Entidades.DetalleDocumentoView)

        End If

        MostrarBotones()


    End Sub


    Private Sub CargarOrdenCompra_GUI(ByVal objDocumento As Entidades.Documento, ByVal listDetalleDocumentoView As List(Of Entidades.DetalleDocumentoView), ByVal listCondicionComercial As List(Of Entidades.CondicionComercial), ByVal objObservacion As Entidades.Observacion, ByVal ListaDetalle As List(Of Entidades.DetalleDocumento))
        ' ************************************* ENTIDADES DOCUMENTO

        With objDocumento
            Me.dlPropietario.SelectedValue = CStr(.IdEmpresa)
            Me.dlTienda.SelectedValue = CStr(.IdTienda)
            Me.hddIdDocumento.Value = CStr(.Id)
            Me.Txtpais.Text = objDocumento.Pais
            Me.TextMes.Text = objDocumento.Mes
            Me.tbCodigoDocumento.Text = objDocumento.Codigo

            If .IdEstadoDoc = 3 Then
                Me.hddOperativo.Value = CStr(operativo.Buscar_Exito)
                verOrdenCompra(False, False, False)
                Return
            End If

            '==== Montos del documento
            Me.rbtipodeoc.SelectedValue = .doc_Importacion.ToString

            '==== Fechas del documento
            Me.tbFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")

            '==== Otros
            Me.hdd_MovCuenta.Value = CStr(CInt(.ValorReferencial))
            Me.hdd_IdDocRelacionado.Value = CStr(.IdDocRelacionado)

        End With

        ' ********************************************** ALMACEN DE DESTINO
        If Not Me.dlAlmacen.Items.FindByValue(CStr(objDocumento.LugarEntrega)) Is Nothing Then
            Me.dlAlmacen.SelectedValue = CStr(objDocumento.LugarEntrega)
        End If

        ' ********************************************** PERSONA
        Me.CargarProveedor(objDocumento.IdPersona, objDocumento.IdDestinatario)

        ' ********************************************** DETALLE DOCUMENTO
        Me.ListaProductos = DetalleDocumentoView_OnLoad(listDetalleDocumentoView)
        Me.ListaDetalleDocumentoView = ListaProductos
        Me.gvProducto.DataSource = ListaProductos
        Me.gvProducto.DataBind()



    End Sub

    Private Sub CargarInforme_GUI(ByVal objDocumento As Entidades.Documento, ByVal listDetalleDocumentoView As List(Of Entidades.DetalleDocumentoView), ByVal ListaDetalle As List(Of Entidades.DetalleDocumento))
        ' ************************************* ENTIDADES DOCUMENTO

        With objDocumento

            Me.dlPropietario.SelectedValue = CStr(.IdEmpresa)
            Me.dlTienda.SelectedValue = CStr(.IdTienda)
            Me.hddIdDocumento.Value = CStr(.Id)
            Me.Txtpais.Text = objDocumento.Pais
            Me.TextMes.Text = objDocumento.Mes
            Me.tbCodigoDocumento.Text = objDocumento.Codigo

            If .IdEstadoDoc = 3 Then
                Me.hddOperativo.Value = CStr(operativo.Buscar_Exito)
                verOrdenCompra(False, False, False)
                Return
            End If

            '==== Montos del documento
            'Me.rbtipodeoc.SelectedValue = .doc_Importacion.ToString

            '==== Fechas del documento
            Me.tbFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")

            '==== Otros
            Me.hdd_MovCuenta.Value = CStr(CInt(.ValorReferencial))
            Me.hdd_IdDocRelacionado.Value = CStr(.IdDocRelacionado)

        End With

        ' ********************************************** ALMACEN DE DESTINO
        If Not Me.dlAlmacen.Items.FindByValue(CStr(objDocumento.IdAlmacen)) Is Nothing Then
            Me.dlAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
        End If

        ' ********************************************** PERSONA
        Me.CargarProveedor(objDocumento.IdPersona, objDocumento.IdDestinatario)

        ' ********************************************** DETALLE DOCUMENTO
        Me.ListaProductos = DetalleDocumentoView_OnLoad(listDetalleDocumentoView)
        Me.ListaDetalleDocumentoView = ListaProductos
        Me.gvProducto.DataSource = ListaProductos
        Me.gvProducto.DataBind()



    End Sub


    Private Sub CargarProveedor(ByVal IdProveedor As Integer, Optional ByVal IdContacto As Integer = -1)

        Try

            Dim objProveedor As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdProveedor)

            'Select Case Me.rbtipodeoc.SelectedValue

            '    Case "True" ' ************************ ORDEN DE COMPRA IMPORTADA


            '    Case Else   ' ************************ ORDEN DE COMPRA NACIONAL || ACTIVOS
            '        If objProveedor.Ruc = "" Then
            '            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('verBusquedaProveedor'); alert('NO SE HA REGISTRADO RUC DEL PROVEEEDOR.'); ", True)
            '            Return
            '        End If
            'End Select

            With objProveedor
                tbProveedor.Text = HttpUtility.HtmlDecode(.Descripcion)
                tbRuc.Text = HttpUtility.HtmlDecode(.Ruc)

            End With

            hdd_Idproveedor.Value = CStr(IdProveedor)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function DetalleDocumentoView_OnLoad(ByVal listDetalleDocumentoView As List(Of Entidades.DetalleDocumentoView)) As List(Of Entidades.DetalleDocumentoView)

        Me.listaKit = New List(Of Entidades.Kit)

        For x As Integer = 0 To listDetalleDocumentoView.Count - 1
            If listDetalleDocumentoView(x).Kit Then
                listaKit.AddRange((New Negocio.Kit).SelectComponentexIdKitDOC_OC(listDetalleDocumentoView(x).IdProducto, CInt(Me.hddIdDocumento.Value)))
            End If

            listDetalleDocumentoView(x).ListaProdUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listDetalleDocumentoView(x).IdProducto)

        Next

        Me.setListaKit(listaKit)

        Return listDetalleDocumentoView

    End Function


    Private Sub dlPropietario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlPropietario.SelectedIndexChanged
        OnChange_Propietario()
    End Sub

#Region "Eventos Principales"
    Private Sub OnChange_Propietario()
        Try

            With drop

                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.dlTienda, CInt(Me.dlPropietario.SelectedValue), CInt(Me.hdd_IdUsuario.Value), False)
                .LLenarCboSeriexIdUsuarioValidacion(Me.dlSerie, CInt(Session("IdUsuario")), CInt(Me.dlPropietario.SelectedValue), CInt(Me.dlTienda.SelectedValue), 16)
                GenerarCodigoDocumento()
                .llenarCboAlmacenxIdEmpresa(Me.dlAlmacen, CInt(Me.dlPropietario.SelectedValue), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub OnChange_Informe()
        Try

            With drop

                .LlenarCboTiendaInforme(Me.dlTienda, CInt(Me.dlPropietario.SelectedValue), CInt(Me.hdd_IdUsuario.Value), False)
                .LLenarCboSeriexIdUsuarioValidacion(Me.dlSerie, CInt(Session("IdUsuario")), CInt(Me.dlPropietario.SelectedValue), CInt(Me.dlTienda.SelectedValue), 2106800005)
                GenerarCodigoDocumento()

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Public Sub JavaScript()
        Me.btguardar.Attributes.Add("onClick", "return(valSaveDocumento());")
        Me.bteditar.Attributes.Add("onClick", "return(validarEditar());")
        Me.tbFechaEmision.Attributes.Add("onblur", "return(valFecha_Blank(this));")
        Me.tbRuc.Attributes.Add("onKeyDown", "return(false);")
        Me.tbCodigoDocumento.Attributes.Add("onKeyPress", "return(validarBuscar(event));")
        Me.btnBuscarDocumento.Attributes.Add("onClick", "return(validarBuscarDocumento());")

    End Sub

    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("DetalleDocumento", "DetalleDocumentoOC" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("CondicionComercial", "CondicionComercialOC" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("ListaKit", "ListaKitOC" + Aleatorio.Next(0, 100000).ToString)

    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub


    Private Sub ValidarPermisos()

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Me.hdd_IdUsuario.Value), New Integer() {2106800009, 2106800010, 2106800011})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR

            Me.btguardar.Enabled = True
        Else

            Me.btguardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.bteditar.Enabled = True
        Else
            Me.bteditar.Enabled = True
        End If



        If listaPermisos(2) > 0 Then  '********* CONSULTAR
            Me.btbuscar.Enabled = True
        Else
            Me.btbuscar.Enabled = False
        End If

        'If listaPermisos(4) > 0 Then  '********* IMPORTACION

        'Else  ' No puede hacer O/C importacion
        '    For x As Integer = 0 To rbtipodeoc.Items.Count - 1
        '        If rbtipodeoc.Items(x).Value = "True" Then
        '            rbtipodeoc.Items(x).Enabled = False
        '            Exit For
        '        End If
        '    Next
        'End If

        'If listaPermisos(5) > 0 Then
        '    hdd_perc.Value = "1"
        'End If

    End Sub


    Private Sub OnInit_OrdenCompra()


        With drop
            .LlenarCboEmpresaxIdUsuario(Me.dlPropietario, CInt(Me.hdd_IdUsuario.Value), False)

            ' ***************** AQUI SE CARGA TODA LA CABECERA QUE DEPENDE DE LA TIENDA
            OnChange_Propietario()

        End With

        With New Negocio.Impuesto
            Me.hddIGV.Value = CStr(.SelectTasaIGV)
        End With


    End Sub


#End Region

    Private Sub Hide_Button()

        pnlDetalle.Enabled = True
        btbuscar.Visible = False
        bteditar.Visible = False

        btguardar.Visible = False

        btnBuscarDocumento.Visible = False
        habilitarControles(False)
      

    End Sub

    Public Sub btbuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btbuscar.Click

        rbtipodeoc.Visible = True
        hdd_IdTipoDocumento.Value = 16
        hddOperativo.Value = CStr(operativo.Buscar)
        verOrdenCompra(True, True, True)

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "setFocus_tbCodigoDocumento();", True)


    End Sub

    Protected Sub dlTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlTienda.SelectedIndexChanged
        OnChange_Tienda()
    End Sub


    Public Sub OnChange_Tienda()

        If (btbuscar.Visible = True) Then
            Try

                With drop
                    dlSerie.Items.Clear()
                    .LLenarCboSeriexIdUsuarioValidacion(Me.dlSerie, CInt(Session("IdUsuario")), CInt(Me.dlPropietario.SelectedValue), CInt(Me.dlTienda.SelectedValue), 16)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "setFocus_tbCodigoDocumento();", True)
                End With

                GenerarCodigoDocumento()

            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try

        Else
            Try

                'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onclick();", True)
                With drop
                    dlSerie.Items.Clear()
                    .LLenarCboSeriexIdUsuarioValidacion(Me.dlSerie, CInt(Session("IdUsuario")), CInt(Me.dlPropietario.SelectedValue), CInt(Me.dlTienda.SelectedValue), CInt(Me.hdd_IdTipoDocumento.Value))
                End With

                GenerarCodigoDocumento()

            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try
        End If



    End Sub

    Protected Sub dlSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlSerie.SelectedIndexChanged
        Try
            If CInt(hddOperativo.Value) = operativo.Buscar Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "setFocus_tbCodigoDocumento();", True)
            End If

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Dim objDrop As New Negocio.LNValorizadoCajas

    Dim CantidadReq As Decimal

    Private Sub gvProducto_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvProducto.RowCommand

        If e.CommandName = "btnTono" Then
            Dim indice As Integer = e.CommandArgument
            Me.idCapasignarTono.Value = indice
            Me.ListaDetalleDocumentoView = getListaDetalleDocumento()
            Dim cantidad As Decimal = 0.0
            cantidad = CDec(TryCast(Me.gvProducto.Rows(indice).FindControl("txtCantidad"), TextBox).Text)
            Me.ListaDetalleDocumentoView(indice).Cantidad = cantidad
            Me.sumalertaTono.Text = "CANTIDAD PRODUCTO:" + cantidad.ToString() + ""
            'Me.ddltono.DataSource = objDrop.LN_ReturnDataTableTonos(Me.ListaDetalleDocumentoView(indice).IdProducto, "LISTAR_TONO_X_PRODUCTO", ListaDetalleDocumentoView(indice).Cantidad)

            Session("SCantidad") = (ListaDetalleDocumentoView(indice).Cantidad)

            'Me.ddltono.DataBind()
            '===============
            Dim list As New List(Of Entidades.Tono)
            list = Me.ListaDetalleDocumentoView(indice).listaTo

         
            If list.Count = 1 Then
                For i As Integer = 0 To list.Count - 1
                    list(i).cantidadTono = Me.ListaDetalleDocumentoView(indice).Cantidad
                Next
            End If

            Me.gvTono.DataSource = list
            Me.gvTono.DataBind()

            setListaDetalleDocumento(Me.ListaDetalleDocumentoView)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');", True)
        End If

    End Sub

    Protected Sub bteditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles bteditar.Click

        If (hdd_IdTipoDocumento.Value() = 2106800005) Then


            Try

                Me.ListaProductos = Me.ListaDetalleDocumentoView
                gvProducto.DataSource = ListaProductos
                gvProducto.DataBind()

                rbtipodeoc.Enabled = False

                hddOperativo.Value = CStr(operativo.Editar)
                verOrdenCompra(False, False, False)
                drop.LLenarCboSeriexIdUsuarioValidacion2(Me.dlSerie, CInt(Session("IdUsuario")), CInt(Me.dlPropietario.SelectedValue), CInt(Me.dlTienda.SelectedValue), 2106800005)
                'GenerarCodigoDocumento()

            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try

        Else

            Try

                Me.ListaProductos = Me.ListaDetalleDocumentoView
                gvProducto.DataSource = ListaProductos
                gvProducto.DataBind()

                rbtipodeoc.Enabled = False

                hddOperativo.Value = CStr(operativo.Editar)
                verOrdenCompra(False, False, False)
                drop.LLenarCboSeriexIdUsuarioValidacion2(Me.dlSerie, CInt(Session("IdUsuario")), CInt(Me.dlPropietario.SelectedValue), CInt(Me.dlTienda.SelectedValue), 2106800005)
                GenerarCodigoDocumento()

            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try

        End If


       

    End Sub

    Private Sub imgButtonCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonCerrar.Click

        Dim indiceCapaAsignarTono As Integer = idCapasignarTono.Value
        Me.ListaDetalleDocumentoView = getListaDetalleDocumento()
        Dim list As New List(Of Entidades.Tono)
        Dim objeto As New Entidades.Tono
        Dim cant As Decimal

        Dim cantidadTotal As Integer = 0


        For Each row As GridViewRow In Me.gvTono.Rows
            objeto = New Entidades.Tono
            objeto.idTono = CInt(row.Cells(1).Text)
            objeto.nomtono = CStr(row.Cells(2).Text)
            objeto.cantidadTono = CDec(TryCast(row.FindControl("txtCantidadTono"), TextBox).Text)
            If (objeto.cantidadTono >= 0) Then
                list.Add(objeto)
            End If

        Next
        If list.Count > 0 Then
            Me.ListaDetalleDocumentoView(indiceCapaAsignarTono).listaTo = list
            setListaDetalleDocumento(Me.ListaDetalleDocumentoView)
        End If

        'Me.gvProducto.DataSource = Me.ListaDetalleDocumentoView
        'Me.gvProducto.DataBind()
    End Sub

    Private Sub btnNuevoTono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevoTono.Click
        Dim idProducto As Integer = TryCast(Me.gvProducto.Rows(idCapasignarTono.Value).FindControl("hddIdProducto"), HiddenField).Value
        Dim objNegocioTrazabilidad As New Negocio.LN_TRAZABILIDAD_DISTRIBUCION
        Me.ListaDetalleDocumentoView = getListaDetalleDocumento()
        Dim nombre As String = Me.txtNombreTono.Text.Trim()
        Dim desc As String = Me.txtDescripcionTono.Text.Trim()
        If objNegocioTrazabilidad.LN_Insert_Tono(Me.txtNombreTono.Text.Trim(), Me.txtDescripcionTono.Text.Trim(), idProducto) Then
            With Me.ListaDetalleDocumentoView(idCapasignarTono.Value)
                Try
                    .listaTo = (New Negocio.bl_Tonos).SelectTonosxProducto2(.IdProducto, nombre, desc, .IdDocumento, .IdUMedida)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try



                Me.gvTono.DataSource = .listaTo
                Me.gvTono.DataBind()
            End With

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');    ", True)
        End If
    End Sub


    Private Sub gvTono_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTono.SelectedIndexChanged
        'Obtiene posición del detalle
        Dim i As Integer = idCapasignarTono.Value

        Dim indice As Integer = gvTono.SelectedIndex
        Me.ListaDetalleDocumentoView = getListaDetalleDocumento()
        Dim idTono As Integer = CInt(Me.gvTono.Rows(indice).Cells(1).Text)
        Dim list As List(Of Entidades.Tono)

        list = ListaDetalleDocumentoView(i).listaTo
        list.RemoveAll(Function(prod As Entidades.Tono) prod.idTono.ToString().Contains(idTono.ToString()))
        Me.gvTono.DataSource = list
        Me.gvTono.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');    ", True)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        hddOperativo.Value = CStr(operativo.Onload)
        OnChange_Propietario()

        verOrdenCompra(True, True, True)
    End Sub

    Private Sub btguardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btguardar.Click
     
        Try
            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            Dim OrdenAnterior As Integer
            OrdenAnterior = hddIdDocumento.Value

            '****************** ACTUALIZAMOS COSTO DEL PRODUCTO

            ActualizarDetalleDocumentoView()
            Me.ListaDetalleDocumentoView = getListaDetalleDocumento()


            'Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            If (hdd_IdTipoDocumento.Value = 16) Then
                Select Case CInt(Me.hddOperativo.Value)


                    Case operativo.Editar
                        objDocumento.Id = (New Negocio.DocInformeProducto).InsertarInformeproducto(objDocumento, ListaDetalleDocumentoView)
                        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                        obj_OrdenCompraPre.Relacion(OrdenAnterior, objDocumento.Id)
                        hddOperativo.Value = CStr(operativo.Onload)
                        Panel_Detalle_producto.Enabled = False
                        verOrdenCompra(False, False, False)
                        objScript.mostrarMsjAlerta(Me, "El Documento se Guardó con éxito.")
                        'LimpiarCajas()

                End Select
            Else

                Select Case CInt(Me.hddOperativo.Value)


                    Case operativo.Editar

                        Dim PoseeRegularizacion = (New Negocio.OrdenCompra).Validar_Regularizacion(objDocumento.Id)
                        If PoseeRegularizacion > 0 Then
                            Throw New Exception("NO SE PUEDE EDITAR, EL DOCUMENTO YA CUENTA CON UNA REGULARIZACION")
                        End If

                        objDocumento.Id = (New Negocio.DocInformeProducto).actualizarInforme(objDocumento, ListaDetalleDocumentoView)
                        verOrdenCompra(False, False, False)
                        objScript.mostrarMsjAlerta(Me, "El Documento se Guardó con éxito.")

                        'LimpiarCajas()

                End Select
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddOperativo.Value)

                Case operativo.Nuevo
                    .Id = Nothing
                Case operativo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.dlPropietario.SelectedValue)
            .IdSerie = CInt(Me.dlSerie.SelectedValue)
            .Serie = CStr(Me.dlSerie.SelectedItem.ToString)
            .Codigo = Me.tbCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.tbFechaEmision.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdAlmacen = CInt(Me.dlAlmacen.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEstadoEntrega = 2 '************* ENTREGADO

            .IdPersona = CInt(Me.hdd_Idproveedor.Value)
            .IdRemitente = CInt(Me.hdd_Idproveedor.Value)
            .IdDestinatario = CInt(Me.dlPropietario.SelectedValue)

            .IdTipoDocumento = 2106800005

         

        End With

        Return objDocumento

    End Function


    Private Sub ActualizarDetalleDocumentoView(Optional ByVal Index As Integer = -1)

        Me.ListaProductos = Me.ListaDetalleDocumentoView
        For i As Integer = 0 To Me.ListaProductos.Count - 1
            Dim cantidad As Decimal = 0
            If Index = i Then Me.ListaProductos(i).IdUMPrincipal = Me.ListaProductos(i).IdUMedida
          
            Me.ListaProductos(i).Cantidad = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.ListaProductos(i).nan = CBool(CType(Me.gvProducto.Rows(i).FindControl("chk_nan"), CheckBox).Checked)
            Me.ListaProductos(i).CAD = CType(Me.gvProducto.Rows(i).FindControl("chk_cad"), CheckBox).Checked
            Me.ListaProductos(i).PesoCaja = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtpeso"), TextBox).Text)
            Me.ListaProductos(i).MCaja = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtM2"), TextBox).Text)
            Me.ListaProductos(i).PzaCaja = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtPz"), TextBox).Text)
            Me.ListaProductos(i).NroCaja = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtNC"), TextBox).Text)
            Me.ListaProductos(i).Pzas = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtP"), TextBox).Text)
            Me.ListaProductos(i).CalibRe = CBool(CType(Me.gvProducto.Rows(i).FindControl("chk_CalibRe"), CheckBox).Checked)
            Me.ListaProductos(i).JGO = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtJGO"), TextBox).Text)
            Me.ListaProductos(i).NPallet = CInt(CType(Me.gvProducto.Rows(i).FindControl("txtNro"), TextBox).Text)
            Me.ListaProductos(i).Total = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtcant"), TextBox).Text)
            Me.ListaProductos(i).TipoEmpaque = CStr(CType(Me.gvProducto.Rows(i).FindControl("txtTipo"), TextBox).Text)
            Me.ListaProductos(i).EstadoPallet = CType(Me.gvProducto.Rows(i).FindControl("cboEstado"), DropDownList).SelectedItem.Text
            Me.ListaProductos(i).Rotulado = CBool(CType(Me.gvProducto.Rows(i).FindControl("chk_Rotulado"), CheckBox).Checked)
            Me.ListaProductos(i).Observaciones = CStr(CType(Me.gvProducto.Rows(i).FindControl("txtObs"), TextBox).Text)
            Me.ListaProductos(i).Sucursal = CStr(CType(Me.dlAlmacen.FindControl("dlAlmacen"), DropDownList).SelectedItem.Text)
        Next


        Me.ListaDetalleDocumentoView = ListaProductos


    End Sub

    Private Sub verInforme(ByVal LimpiarFrm As Boolean, ByVal InicializarFrm As Boolean, ByVal IniSesion As Boolean)

        If LimpiarFrm Then
            Me.Limpiar()
        End If

        If InicializarFrm Then

            GenerarCodigoDocumento()
            Me.tbFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")


        End If

        If IniSesion Then
            Me.ListaDetalleDocumentoView = New List(Of Entidades.DetalleDocumentoView)

        End If

        MostrarBotones()


    End Sub


    Private Sub btBuscarI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btBuscarI.Click

        rbtipodeoc.Visible = False
        hdd_IdTipoDocumento.Value = 2106800005
        LimpiarCombos()
        OnChange_Informe()
        hddOperativo.Value = CStr(operativo.Buscar_Inf)
        verInforme(True, True, True)

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "setFocus_tbCodigoDocumento();", True)
    End Sub

    
    Private Sub btnProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProducto.Click
        'Me.ListaProductos = DetalleDocumentoView_OnLoad(listDetalleDocumentoView)
        'Me.ListaDetalleDocumentoView = ListaProductos
        'Me.gvProducto.DataSource = ListaProductos
        'Me.gvProducto.DataBind()

        Dim objProducto As New Negocio.LN_TRAZABILIDAD_DISTRIBUCION
        Dim addProducto As New Entidades.DetalleDocumentoView
        Dim nombre As String = Me.txtNomProd.Text.Trim()
        Dim producto As Integer
        producto = objProducto.CrearProducto(nombre)
 
        With addProducto
            .IdProducto = producto
            .Descripcion = nombre
            .listaTo = (New Negocio.bl_Tonos).SelectTonos(producto)
        End With
        ListaDetalleDocumentoView.Add(addProducto)

        Me.gvProducto.DataSource = ListaDetalleDocumentoView
        Me.gvProducto.DataBind()

        'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaAsignarTono');    ", True)

    End Sub
End Class
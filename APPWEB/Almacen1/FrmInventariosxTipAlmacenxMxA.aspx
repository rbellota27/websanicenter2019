﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmInventariosxTipAlmacenxMxA.aspx.vb" Inherits="APPWEB.FrmInventariosxTipAlmacenxMxA" 
    title="Página sin título" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <table class="style1">
   <tr style="background-color: Yellow">
            <td style="width: 350px">
                <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                    runat="server">Config. Formulario</asp:LinkButton>
            </td>
        </tr>
<tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nueva Búsqueda" ToolTip="Nueva Busqueda" Width="120px" />
                        </td>
                       
                        <td>
                            <asp:Button ID="btnBuscar" runat="server"  Text="Buscar" ToolTip="Buscar" Width="80px"  />
                        </td>
                       
                       
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" Visible="false" ToolTip="Imprimir Documento" Width="80px"
                            />
                        </td>
                          <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
           <tr>
            <td class="TituloCelda">
                REPORTE DE VALORIZADO DE INVENTARIO POR MES/ AÑO/ TIPO ALMACEN
            </td>
        </tr>
        <tr>
        <td>
        <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                                        <tr>
                                                <td align="right" class="Texto">
                                                Tipo Almacen:
                                                </td>
                                                
                                                <td >
                                                <asp:DropDownList ID="cbotipoalmacen" runat="server" Enabled="true" >
                                                </asp:DropDownList>
                                                </td>
                                              
                                              
                                                  <td> &nbsp;</td>
                                                  <td>
                                              &nbsp;
                                                  </td>
                                                  <td align="right" class="Texto">
                                                Año:
                                                </td>
                                                <td>
                                                <asp:DropDownList ID="cboanio" runat="server" Enabled="true" >
                                                     <asp:ListItem Text="2010" Value="1" ></asp:ListItem>
                                                     <asp:ListItem Text="2011" Value="2" ></asp:ListItem>
                                                     <asp:ListItem Text="2012" Value="3" ></asp:ListItem>
                                                     <asp:ListItem Text="2013" Value="4" ></asp:ListItem>
                                                     <asp:ListItem Text="2014" Value="5" ></asp:ListItem>
                                                     <asp:ListItem Text="2015" Value="6" ></asp:ListItem>
                                                     <asp:ListItem Text="2016" Value="7" ></asp:ListItem>
                                                     <asp:ListItem Text="2017" Value="8" ></asp:ListItem>
                                                     <asp:ListItem Text="2018" Value="9" ></asp:ListItem>
                                                     <asp:ListItem Text="2019" Value="10" ></asp:ListItem>
                                                      <asp:ListItem Text="2020" Value="11" ></asp:ListItem>
                                                     <asp:ListItem Text="2021" Value="12" ></asp:ListItem>                           
                                                </asp:DropDownList>
                                                </td>
                                                <td> &nbsp;</td>
                                        </tr>
                                        <tr>
                                        <td></td>
                                          <td>
                                                <asp:Button ID="add" Text="Agregar Almacen" runat="server" OnClientClick=" return ( Almacen_add() ); " ToolTip="Replicar almacenes"  />
                                                </td>
                                                <td>&nbsp;
                                                </td>
                                                <td>&nbsp;</td>
                                                 <td align="right" class="Texto">
                                                Mes Inicio:
                                                </td>
                                                <td>
                                                <asp:DropDownList ID="cboMesInicio" runat="server" Enabled="true" >
                                                         <asp:ListItem Text="Enero" Value="1" ></asp:ListItem>
                                                         <asp:ListItem Text="Febrero" Value="2" ></asp:ListItem>
                                                         <asp:ListItem Text="Marzo" Value="3" ></asp:ListItem>
                                                         <asp:ListItem Text="Abril" Value="4" ></asp:ListItem>
                                                         <asp:ListItem Text="Mayo" Value="5" ></asp:ListItem>
                                                         <asp:ListItem Text="Junio" Value="6" ></asp:ListItem>
                                                         <asp:ListItem Text="Julio" Value="7" ></asp:ListItem>
                                                         <asp:ListItem Text="Agosto" Value="8" ></asp:ListItem>
                                                         <asp:ListItem Text="Septiembre" Value="9" ></asp:ListItem>
                                                         <asp:ListItem Text="Octubre" Value="10" ></asp:ListItem>
                                                          <asp:ListItem Text="Noviembre" Value="11" ></asp:ListItem>
                                                         <asp:ListItem Text="Diciembre" Value="12" ></asp:ListItem>      
                                                </asp:DropDownList>
                                                </td>
                                        </tr>
                                        <tr>
                                        <td>&nbsp;</td>
                                         <td>&nbsp;</td>
                                          <td>&nbsp;</td>
                                           <td>&nbsp;</td>
                                              
                                                 <td align="right" class="Texto">
                                                Mes Final:
                                                </td>
                                                <td>
                                                        <asp:DropDownList ID="cboMesFinal" runat="server"  Enabled="true" >
                                                                 <asp:ListItem Text="Enero" Value="1" ></asp:ListItem>
                                                                 <asp:ListItem Text="Febrero" Value="2" ></asp:ListItem>
                                                                 <asp:ListItem Text="Marzo" Value="3" ></asp:ListItem>
                                                                 <asp:ListItem Text="Abril" Value="4" ></asp:ListItem>
                                                                 <asp:ListItem Text="Mayo" Value="5" ></asp:ListItem>
                                                                 <asp:ListItem Text="Junio" Value="6" ></asp:ListItem>
                                                                 <asp:ListItem Text="Julio" Value="7" ></asp:ListItem>
                                                                 <asp:ListItem Text="Agosto" Value="8" ></asp:ListItem>
                                                                 <asp:ListItem Text="Septiembre" Value="9" ></asp:ListItem>
                                                                 <asp:ListItem Text="Octubre" Value="10" ></asp:ListItem>
                                                                  <asp:ListItem Text="Noviembre" Value="11" ></asp:ListItem>
                                                                 <asp:ListItem Text="Diciembre" Value="12" ></asp:ListItem>      
                                                        </asp:DropDownList>
                                                </td> 
                                                  <td> &nbsp;</td> 
                                             <td>&nbsp;</td>
                                              <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                               
                                        </tr>
                                        <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                        <asp:GridView ID="GV_TipoAlmacen" runat="server" AutoGenerateColumns="False" 
                                                Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                                                BorderWidth="1px" CellPadding="3" >
                                <Columns>
                                    <asp:TemplateField HeaderText="Opciones">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lkbAlmacenRemove" runat="server" OnClick="lkbAlmacenRemove_Click">Quitar</asp:LinkButton>
                                                    </td>
                                                  <td>
                                                        <asp:HiddenField ID="hddIdTipoAlmacen" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdtipoAlmacen") %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField HeaderText="Tipo Almacen" DataField="Tipoalm_Nombre" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    </asp:BoundField>--%>
                                   <asp:TemplateField HeaderText="Tipo Almacen">
                                   <ItemTemplate>
                                         <asp:Label ID="lbltipoalmacen"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Tipoalm_Nombre")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                    
                                </Columns>
                                <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
                                <PagerStyle CssClass="GrillaPager" BackColor="White" ForeColor="#000066" 
                                                HorizontalAlign="Left" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
                                                ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
                                                ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView>
                                        </td>
                                          <td>&nbsp;
                                                </td>
                                         </tr>                            
                                </table>
             </asp:Panel>
        </td>
          
      
        </tr>
              
</table>


 <script language="javascript" type="text/javascript">

  function Almacen_add() {

            var cbotipoalmacen = document.getElementById('<%=cbotipoalmacen.ClientID %>');
            var GV_TipoAlmacen = document.getElementById('<%=GV_TipoAlmacen.ClientID %>');

            if (GV_TipoAlmacen != null) {

                for (var i = 1; i < GV_TipoAlmacen.rows.length; i++) {
                    var rowElem = GV_TipoAlmacen.rows[i];

                    if (rowElem.cells[0].children[0].cells[2].children[0].value == cbotipoalmacen.value) {

                        alert('LA LISTA CONTIENE EL TIPO ALMACEN [ ' + cbotipoalmacen.options[cbotipoalmacen.selectedIndex].text + ' ] SELECCIONADO. \n NO SE PERMITE LA OPERACIÓN.');
                        return false;
                    }

                }

            }

            return true;
        }
        
           </script>

</asp:Content>

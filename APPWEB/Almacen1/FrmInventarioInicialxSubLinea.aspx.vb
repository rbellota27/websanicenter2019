﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmInventarioInicialxSubLinea
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub verFrmINICIO()
        Panel_Cabecera.Enabled = True
        btnGuardar.Enabled = False
        btnAtras.Enabled = False
        btnEditar.Enabled = False
        'btnImprimir.Enabled = True
        DGVDetalle.DataSource = Nothing
        DGVDetalle.DataBind()
        hddIdDocumento.Value = "-1"
    End Sub
    Private Sub verFrmNUEVO()
        Panel_Cabecera.Enabled = False
        'btnImprimir.Enabled = True
        btnAtras.Enabled = True
        btnGuardar.Enabled = True
    End Sub
#Region "CARGAR DE DATOS INICIALES"
    Private Sub inicializarFrm()
        Try
            cargarDatosCboEmpresa(Me.cmbEmpresa)
            cargarDatosCboAlmacen(Me.cmbAlmacen, CInt(Me.cmbEmpresa.SelectedValue))
            cargarDatosLinea(Me.cmbLinea)
            cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
            verFrmINICIO()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub cargarDatosCboAlmacen(ByVal cbo As DropDownList, ByVal IdEmpresa As Integer)
        Dim objCombo As New Combo
        objCombo.llenarCboAlmacenxIdTienda(Me.cmbAlmacen, IdEmpresa)
    End Sub
    Private Sub cargarDatosCboEmpresa(ByVal cbo As DropDownList)
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboEmpresaxIdUsuario(cbo, CInt(Session("IdUsuario")), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosLinea(ByVal comboBox As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal comboBox As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        cargarDatosCboAlmacen(Me.cmbAlmacen, CInt(Me.cmbEmpresa.SelectedValue))
    End Sub
    Private Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea.SelectedIndexChanged
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
    End Sub
#End Region
    Protected Sub btnAceptar_SubLinea_Cab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_SubLinea_Cab.Click
        cargarDataxSubLinea(CInt(cmbSubLinea.SelectedValue))
    End Sub
    Private Sub cargarDataxSubLinea(ByVal IdSubLinea As Integer)
        Try
            Dim objMovAlmacenNeg As New Negocio.MovAlmacen
            Dim IdDocumento As Integer = objMovAlmacenNeg.SelectIdDocumentoInvInicial(CInt(cmbEmpresa.SelectedValue), CInt(cmbAlmacen.SelectedValue), CInt(cmbTipoOperacion.SelectedValue))
            hddIdDocumento.Value = IdDocumento.ToString
            Dim objDetalleDocumentoNeg As New Negocio.DetalleDocumento

            'ya no se utiliza este form

            ''DGVDetalle.DataSource = objDetalleDocumentoNeg.SelectxIdSubLineaxIdDocumento(IdSubLinea, IdDocumento)
            DGVDetalle.DataBind()
            verFrmNUEVO()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos de los productos.")
        End Try
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        registrarInventarioInicial(CInt(hddIdDocumento.Value))
    End Sub
    Private Sub registrarInventarioInicial(ByVal IdDocumento As Integer)
        Try
            '*************** obtengo los datos del documento
            Dim objDocumento As Entidades.Documento
            Dim listaDetalleDoc As List(Of Entidades.DetalleDocumento)
            Dim listaMovAlmacen As List(Of Entidades.MovAlmacen)
            objDocumento = getObjDocumento()
            listaDetalleDoc = getListaDetalleDoc()
            listaMovAlmacen = getListaMovAlmacen()

            '*************************guardo los datos
            Dim objDocumentoNegocio As New Negocio.Documento
            If IdDocumento <= 0 Then
                objDocumento.FechaEmision = (New Negocio.FechaActual).SelectFechaActual
                IdDocumento = objDocumentoNegocio.InsertDocumentoInvInicial(objDocumento, listaDetalleDoc, listaMovAlmacen)
            Else
                IdDocumento = objDocumentoNegocio.UpdateDocumentoInvInicial(objDocumento.Id, objDocumento.IdAlmacen, CInt(Me.cmbSubLinea.SelectedValue), listaDetalleDoc)
            End If

            '*********** imprimo mensajes
            If IdDocumento <= 0 Then
                Throw New Exception("Problemas en la Operación")
            Else
                hddIdDocumento.Value = IdDocumento.ToString
                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function getObjDocumento() As Entidades.Documento
        Dim obj As New Entidades.Documento
        With obj
            .IdTienda = Nothing
            .Id = CInt(hddIdDocumento.Value)
            .IdEmpresa = CInt(cmbEmpresa.SelectedValue)
            .IdEstadoDoc = CInt(cboEstadoDocumento.SelectedValue)
            .IdTipoDocumento = CInt(cmbTipoDocumento.SelectedValue)
            .IdTipoOperacion = CInt(cmbTipoOperacion.SelectedValue)
            .IdAlmacen = CInt(Me.cmbAlmacen.SelectedValue)
        End With
        Return obj
    End Function
    Private Function getListaDetalleDoc() As List(Of Entidades.DetalleDocumento)
        Dim lista As New List(Of Entidades.DetalleDocumento)
        For i As Integer = 0 To DGVDetalle.Rows.Count - 1
            Dim obj As New Entidades.DetalleDocumento
            With obj
                .Cantidad = CDec(CType(DGVDetalle.Rows(i).Cells(5).FindControl("txtCantidad"), TextBox).Text)
                .IdAlmacen = CInt(cmbAlmacen.SelectedValue)
                .IdDocumento = CInt(hddIdDocumento.Value)
                .IdProducto = CInt(DGVDetalle.Rows(i).Cells(0).Text)
                .IdUnidadMedida = CInt(DGVDetalle.Rows(i).Cells(2).Text)
                .UMedida = (DGVDetalle.Rows(i).Cells(3).Text)
                .IdDetalleDocumento = CInt(DGVDetalle.Rows(i).Cells(6).Text)
            End With
            lista.Add(obj)
        Next
        Return lista
    End Function
    Private Function getListaMovAlmacen() As List(Of Entidades.MovAlmacen)
        Dim lista As New List(Of Entidades.MovAlmacen)
        Dim objMetValNegocio As New Negocio.MetodoValuacion


        '*********** verificamos la existencia de un metodo de valuacion
        Dim IdMetodoValuacion As Integer = objMetValNegocio.SelectIdMetValVigente
        If IdMetodoValuacion <= 0 Then Throw New Exception


        For i As Integer = 0 To DGVDetalle.Rows.Count - 1
            Dim obj As New Entidades.MovAlmacen
            With obj
                .CantidadMov = CDec(CType(DGVDetalle.Rows(i).Cells(5).FindControl("txtCantidad"), TextBox).Text)
                .Comprometido = False
                .Factor = 1
                .IdAlmacen = CInt(cmbAlmacen.SelectedValue)
                .IdDocumento = CInt(hddIdDocumento.Value)
                .IdEmpresa = CInt(cmbEmpresa.SelectedValue)
                .IdProducto = CInt(DGVDetalle.Rows(i).Cells(0).Text)

                '*********** Tienda NULO
                .IdTienda = Nothing

                .IdDetalleDocumento = CInt(DGVDetalle.Rows(i).Cells(6).Text)
                .IdMetodoV = IdMetodoValuacion
                .IdTipoOperacion = CInt(cmbTipoOperacion.SelectedValue)
                .IdUMPrincipal = CInt(DGVDetalle.Rows(i).Cells(2).Text)
                .IdUnidadMedida = CInt(DGVDetalle.Rows(i).Cells(2).Text)
                .UMPrincipal = (DGVDetalle.Rows(i).Cells(3).Text)
            End With
            lista.Add(obj)
        Next
        Return lista
    End Function
    Protected Sub btnAtras_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras.Click
        verFrmINICIO()
    End Sub

    Protected Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        imprimirDocumentoInvInicial()
    End Sub
    Private Sub imprimirDocumentoInvInicial()
        Dim objMovAlmacenNeg As New Negocio.MovAlmacen
        Dim IdDocumento As Integer = objMovAlmacenNeg.SelectIdDocumentoInvInicial(CInt(cmbEmpresa.SelectedValue), CInt(cmbAlmacen.SelectedValue), CInt(cmbTipoOperacion.SelectedValue))
        If IdDocumento <= 0 Then
            objScript.mostrarMsjAlerta(Me, "No existe un Inventario Inicial para la Empresa/Almacén seleccionado.")
        Else
            Response.Redirect("~/Reportes/visor.aspx?iReporte=1&IdDocumento=" & IdDocumento, True)
        End If
    End Sub
End Class
﻿Partial Public Class FrmAsigObjetivo
    Inherits System.Web.UI.Page

    Dim anio As String = ""
    Dim mes As String = ""
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboTiendaxObjetivo(Me.cbotienda)
                '.SelectEscalaTienda(Me.ddlescala)
            End With
            LimpiarFormulario()
            'LlenaGridEscala()
        End If

    End Sub
    Private Sub LimpiarFormulario()
        Me.btnBuscar.Enabled = True
        Me.btnBuscar.Visible = True

        Me.btneditar.Visible = True
        Me.btnNuevo.Visible = True
        Me.fastsearch.Visible = False

        Me.txtobjetivo.Text = "0"
        Me.txtobjetivo.Enabled = True
        Me.txtobjetivo.Visible = True

        Me.btnGuardar.Visible = False
        Me.btnGuardar.Enabled = False

        Me.GV_TiendasxObjetivo.DataSource = Nothing
        Me.GV_TiendasxObjetivo.DataBind()

        Me.lbl.Visible = False
        Me.ddlanio.Visible = True
        Me.ddlmes.Visible = True
        Me.ddlanio.Enabled = True
        Me.ddlmes.Enabled = True
        'Me.ddlescala.Enabled = True
        Me.cbotienda.Enabled = True
        Me.cbotienda.Visible = True
        Me.ddlmes.SelectedValue = "1"
        Me.btnbuscareditar.Visible = False
        Me.lblnombretienda.Visible = False
        Me.lblnuevodato.Visible = False
        Me.btnbuscareditar.Visible = False
        Me.fastsearch.Visible = False
        Me.btnsaveeditar.Visible = False
        Me.lblNuevoMes.Visible = False
        Me.lblnuevoobjetivo.Visible = False
        Me.txtnuevoobjetivo.Visible = False
        Me.cbonuevoanio.Visible = False
        Me.cbonuevomes.Visible = False
        Me.Nuevoanio.Visible = False
        Me.txtIdObj.Visible = False
        Me.lblidobj.Visible = False
        Me.lblobjetivo.Visible = True
        Me.lbltienda.Visible = True
        Me.lblanio.Visible = True
        Me.lblmes.Visible = True


    End Sub

    Private Sub LimpiarFormularioNuevo()
        Me.btnBuscar.Enabled = True
        Me.btneditar.Visible = True
        Me.btnNuevo.Visible = True
        Me.fastsearch.Visible = False
        Me.txtobjetivo.Text = "0"
        Me.txtobjetivo.Enabled = True
        Me.btnGuardar.Visible = True
        Me.btnGuardar.Enabled = True
        Me.GV_TiendasxObjetivo.DataSource = Nothing
        Me.GV_TiendasxObjetivo.DataBind()
        Me.lbl.Visible = False
        Me.ddlanio.Enabled = True
        Me.ddlmes.Enabled = True
        'Me.ddlescala.Enabled = True
        Me.cbotienda.Enabled = True
        Me.ddlmes.SelectedValue = "1"
        Me.btnbuscareditar.Visible = False
        Me.lblnombretienda.Visible = False
        Me.lblnuevodato.Visible = False
        Me.btnbuscareditar.Visible = False
        Me.fastsearch.Visible = False
        Me.btnsaveeditar.Visible = False
        Me.lblNuevoMes.Visible = False
        Me.lblnuevoobjetivo.Visible = False
        Me.txtnuevoobjetivo.Visible = False
        Me.cbonuevoanio.Visible = False
        Me.cbonuevomes.Visible = False
        Me.Nuevoanio.Visible = False
        Me.txtIdObj.Visible = False
        Me.lblidobj.Visible = False
        Me.btneditar.Visible = False
        Me.btnBuscar.Visible = False
        Me.btnRegresar.Visible = True
        Me.lblobjetivo.Visible = True
        Me.lbltienda.Visible = True
        Me.lblanio.Visible = True
        Me.lblmes.Visible = True
    End Sub


    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        'txtobjetivo.Attributes.Add("onkeydown", "return isNumeric(event.keyCode);")
        If (CInt(txtobjetivo.Text) <= 0 Or txtobjetivo.Text = "") Then
            objScript.mostrarMsjAlerta(Me, "Ingrese un Objetivo Correcto.")
           
        Else
            Dim EObjexTienda As Entidades.Tienda = New Entidades.Tienda
            EObjexTienda.Id = CInt(cbotienda.SelectedValue)
            EObjexTienda.ObjetivoxTienda = CInt(txtobjetivo.Text)
            EObjexTienda.Mes = CStr(ddlmes.SelectedItem.Text)
            EObjexTienda.Anio = CStr(ddlanio.SelectedItem.Text)
            'EObjexTienda.IdEscala = CInt(ddlescala.SelectedValue)

            Dim result As Boolean = (New Negocio.Tienda).InsertObjxTienda(EObjexTienda)
            If (result = True) Then
                objScript.mostrarMsjAlerta(Me, "Grabación Éxitosa")
                Me.btnBuscar.Enabled = False
                Me.btnBuscar.Visible = False
                Me.btnNuevo.Enabled = True
                Me.btnNuevo.Visible = True
                Me.btnGuardar.Enabled = False
                Me.btnGuardar.Visible = False
                Me.fastsearch.Visible = False
                Me.cbotienda.Enabled = False
                Me.ddlanio.Enabled = False
                Me.ddlmes.Enabled = False
                'Me.ddlescala.Enabled = False
                Me.txtobjetivo.Enabled = False
            Else
                objScript.mostrarMsjAlerta(Me, "La tienda seleccionada ya posee un objetivo registrado.")
            End If

        End If
    End Sub
    Private Sub LlenarGrillaObjetivoxTienda()


        mes = ddlmes.SelectedItem.Text
        anio = ddlanio.SelectedItem.Text

        Dim obj As Entidades.Tienda = New Entidades.Tienda
        obj.Anio = anio
        obj.Mes = mes

        Dim lista As List(Of Entidades.Tienda) = (New Negocio.Tienda).SelectAllObjetivoxTienda(obj)
        Me.GV_TiendasxObjetivo.DataSource = lista
        Me.GV_TiendasxObjetivo.DataBind()

    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        Me.cbotienda.Visible = False
        Me.btnGuardar.Visible = False
        Me.fastsearch.Visible = True
        Me.ddlanio.Enabled = True
        Me.ddlmes.Enabled = True
        Me.txtobjetivo.Visible = False
        Me.btneditar.Visible = False
        Me.btnsaveeditar.Visible = False
        Me.btnNuevo.Visible = False
        Me.btnbuscareditar.Visible = False
        Me.txtIdObj.Visible = False
        Me.ddlmes.SelectedValue = "1"
        Me.btnbuscareditar.Visible = False
        Me.lblnombretienda.Visible = False
        Me.lblNuevoMes.Visible = False
        Me.lblnuevoobjetivo.Visible = False
        Me.txtnuevoobjetivo.Visible = False
        Me.cbonuevoanio.Visible = False
        Me.cbonuevomes.Visible = False
        Me.Nuevoanio.Visible = False
        Me.txtIdObj.Visible = False
        Me.lblidobj.Visible = False
        Me.lblnuevodato.Visible = False
        Me.btnRegresar.Visible = True
        Me.lblobjetivo.Visible = False
        Me.lbltienda.Visible = False
    End Sub

    Protected Sub fastsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles fastsearch.Click
        LlenarGrillaObjetivoxTienda()
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        cbotienda.DataSource = Nothing
        cbotienda.DataBind()

        Dim objCbo As New Combo
        With objCbo
            .LlenarCboTiendaxObjetivo(Me.cbotienda)
            '.SelectEscalaTienda(Me.ddlescala)
        End With
        LimpiarFormularioNuevo()
    End Sub

    Private Sub LlenaGridEscala()

        Dim dt As New DataTable
        dt.Columns.Add("-")
        dt.Columns.Add("Indice")
        dt.Columns.Add("Rotación Almacen")

        Dim row1 As DataRow = dt.NewRow
        row1.Item("-") = "EXCELENTE"
        row1.Item("Indice") = "<10.00 - 12.00>"
        row1.Item("Rotación Almacen") = "30 días a 36 días"

        Dim row2 As DataRow = dt.NewRow
        row2.Item("-") = "BUENO"
        row2.Item("Indice") = "<8.00 - 10.00>"
        row2.Item("Rotación Almacen") = "36 días a 45 días"
        Dim row3 As DataRow = dt.NewRow
        row3.Item("-") = "PROMEDIO"
        row3.Item("Indice") = "<6.00 - 8.00>"
        row3.Item("Rotación Almacen") = "45 días a 60 días"

        dt.Rows.Add(row1)
        dt.Rows.Add(row2)
        dt.Rows.Add(row3)
        gvEscala.DataSource = dt
        gvEscala.DataBind()
        gvEscala.Visible = True


    End Sub

    Protected Sub btneditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btneditar.Click
        Me.btnGuardar.Visible = False
        Me.fastsearch.Visible = False
        Me.btnbuscareditar.Visible = True
        Me.txtobjetivo.Enabled = False
        Me.cbotienda.Enabled = True
        Me.ddlanio.Enabled = True
        Me.ddlmes.Enabled = True
        Me.GV_TiendasxObjetivo.DataSource = Nothing
        Me.GV_TiendasxObjetivo.DataBind()
        Me.txtobjetivo.Text = "0"
        Me.txtIdObj.Visible = False
        Me.btnRegresar.Visible = True
        Me.btnBuscar.Visible = False
        Me.btnNuevo.Visible = False
        Me.txtIdObj.Visible = False
        Me.txtnuevoobjetivo.Visible = False
        Me.cbonuevoanio.Visible = False
        Me.cbonuevomes.Visible = False
        Me.lblnuevodato.Visible = False
        Me.lblNuevoMes.Visible = False
        Me.Nuevoanio.Visible = False
        Me.lblidobj.Visible = False
        Me.lblnombretienda.Visible = False
        Me.lblnuevoobjetivo.Visible = False
    End Sub
    Protected Sub btnbuscareditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnbuscareditar.Click


        lblnombretienda.Text = CStr(cbotienda.SelectedItem.Text) + "]"
   
      
        Dim obj As Entidades.Tienda = New Entidades.Tienda
        obj.Mes = CStr(ddlmes.SelectedItem.Text)
        obj.Anio = CStr(ddlanio.SelectedItem.Text)
        obj.Id = CInt(cbotienda.SelectedValue)

        Dim tienda As Entidades.Tienda = (New Negocio.Tienda).SelectxIdTiendaObjetivo(obj)
        If (tienda.Id = 0) Then
            objScript.mostrarMsjAlerta(Me, "No se encontro ningún registro.")
        Else
            With tienda
                txtnuevoobjetivo.Text = CStr(.ObjetivoxTienda)
                cbonuevomes.SelectedIndex = ddlmes.SelectedIndex
                cbonuevoanio.SelectedItem.Text = CStr(.Anio)
                Me.txtIdObj.Text = CStr(.IdObjetivo)

                'If (Me.cbotienda.Items.FindByValue(CStr(.Id)) IsNot Nothing) Then
                '    Me.cbotienda.SelectedValue = CStr(.Id)
                'End If
                LimpiarEditar()
            End With
        End If

    End Sub
    Private Sub LimpiarEditar()
        Me.lblnombretienda.Visible = True
        Me.lblnuevodato.Visible = True
        Me.lblNuevoMes.Visible = True
        Me.Nuevoanio.Visible = True
        Me.lblnuevoobjetivo.Visible = True
        Me.cbonuevoanio.Visible = True
        Me.cbonuevomes.Visible = True
        Me.cbonuevoanio.Enabled = True
        Me.cbonuevomes.Enabled = True
        Me.txtnuevoobjetivo.Visible = True
        Me.btnGuardar.Visible = False
        Me.btnsaveeditar.Visible = True
        Me.txtIdObj.Visible = True
        Me.ddlanio.Enabled = False
        Me.ddlmes.Enabled = False
        Me.txtnuevoobjetivo.Enabled = True
        Me.lblidobj.Visible = True
        Me.cbotienda.Enabled = False
        Me.btneditar.Visible = False
        Me.lblobjetivo.Visible = True
        Me.lbltienda.Visible = True
    End Sub

    Protected Sub btnsaveeditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsaveeditar.Click

        If (Not IsNumeric(txtnuevoobjetivo.Text)) Then
            objScript.mostrarMsjAlerta(Me, "Ingrese solo números en el campo Objetivo.")
        Else
            Dim objtienda As Entidades.Tienda = New Entidades.Tienda
            objtienda.IdObjetivo = CInt(txtIdObj.Text)
            objtienda.Mes = CStr(cbonuevomes.SelectedItem.Text)
            objtienda.Anio = CStr(cbonuevoanio.SelectedItem.Text)
            objtienda.ObjetivoxTienda = CInt(txtnuevoobjetivo.Text)
            objtienda.Id = CInt(cbotienda.SelectedValue)
            Dim tienda As Boolean = (New Negocio.Tienda).UpdateObjetivoxTienda(objtienda)
            If (tienda = False) Then
                objScript.mostrarMsjAlerta(Me, "Ya existe un objetivo registrado en el mes Seleccionado")
            Else
                objScript.mostrarMsjAlerta(Me, "Modificación Éxitosa")
                limpiarModificacionExitosa()
            End If
        End If

    End Sub
    Private Sub limpiarModificacionExitosa()
        Me.cbonuevoanio.Enabled = False
        Me.cbonuevomes.Enabled = False
        Me.txtnuevoobjetivo.Enabled = False
        Me.btnGuardar.Visible = False
        Me.btnsaveeditar.Visible = False
        Me.txtIdObj.Enabled = False
        Me.btnbuscareditar.Visible = False
        Me.btneditar.Visible = True

    End Sub


    Protected Sub btnRegresar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRegresar.Click
        LimpiarFormulario()
        Me.btnRegresar.Visible = False
    End Sub
End Class
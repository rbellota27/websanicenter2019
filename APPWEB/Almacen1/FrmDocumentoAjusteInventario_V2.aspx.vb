﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmDocumentoAjusteInventario_V2
    Inherits System.Web.UI.Page


    Private objScript As New ScriptManagerClass
    Private listaEmpleado As List(Of Entidades.PersonaView)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
            ValidarPermisos()
        End If
    End Sub
    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {64, 65, 66, 67, 68})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub
    Private Sub visualizarMoneda()

        Me.lblMoneda_CostoFaltante.Text = (New Negocio.Moneda).SelectCboMonedaBase(0).Simbolo
        Me.lblMoneda_CostoSobrante.Text = Me.lblMoneda_CostoFaltante.Text

    End Sub
    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), False)
                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

                '**************** FILTRO
                .llenarCboTipoExistencia(cboTipoExistencia_Filtro, True)
                .llenarCboLineaxTipoExistencia(cboLinea_Filtro, CInt(Me.cboTipoExistencia_Filtro.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboTipoExistencia_Filtro.SelectedValue), True)
                '.LlenarCboLinea(Me.cboLinea_Filtro, True)
                '.LlenarCboSubLineaxIdLinea(Me.cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), True)
                '---------*********tipo documento------
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CbotipoDocumento, CInt(Me.hddIdTipoDocumento.Value), 2, False)
                '----
            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

            actualizarOpcionesBusquedaDocRef(1, False) '******** POR NRO DOCUMENTO
            visualizarMoneda()

            verFrm(FrmModo.Inicio, True, True, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub
#Region "************************ MANEJO DE SESSION"
    Private Function getListaEmpleado() As List(Of Entidades.PersonaView)
        Return CType(Session.Item("listaEmpleado"), List(Of Entidades.PersonaView))
    End Function
    Private Sub setListaEmpleado(ByVal lista As List(Of Entidades.PersonaView))
        Session.Remove("listaEmpleado")
        Session.Add("listaEmpleado", lista)
    End Sub
    Private Function getListaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setListaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub

    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("listaDetalleDocumento"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("listaDetalleDocumento")
        Session.Add("listaDetalleDocumento", lista)
    End Sub
#End Region

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, ByVal generarCodigo As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove("listaEmpleado")
            Session.Remove("listaDocumentoRef")
        End If

        If (initSession) Then

            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setListaDocumentoRef(Me.listaDocumentoRef)

            Me.listaEmpleado = New List(Of Entidades.PersonaView)
            setListaEmpleado(Me.listaEmpleado)

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})

        End If

        If (generarCodigo) Then
            GenerarCodigoDocumento()
        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()


        Me.btnBuscarDocumentoRef.Visible = False
        Me.Panel_DocumentoRef.Enabled = False


        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnInicio.Visible = False
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = False
                Me.Panel_FiltroDetalle.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboAlmacen.Enabled = True

                Me.cboEstado.SelectedValue = "1"

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = True

                Me.Panel_FiltroDetalle.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False


                Me.cboEstado.SelectedValue = "1"




            Case FrmModo.Editar '*********************** Editar
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = True
                Me.Panel_FiltroDetalle.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False



            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                'Me.btnNuevo.Visible = True
                'Me.btnBuscar.Visible = True
                'Me.btnBuscarDocumentoxCodigo.Visible = True
                'Me.txtCodigoDocumento.ReadOnly = False
                'Me.txtCodigoDocumento.Focus()
                'Me.btnEditar.Visible = False
                'Me.btnGuardar.Visible = False
                'Me.btnAnular.Visible = False
                'Me.btnImprimir.Visible = False


                'Me.btnBuscarDocumentoRef.Visible = False

                'Me.Panel_Cab.Enabled = True
                'Me.Panel_DocumentoRef.Enabled = False
                'Me.Panel_Empleado.Enabled = False
                'Me.Panel_CargaProductos.Enabled = False
                'Me.Panel_FiltroDetalle.Enabled = False
                'Me.Panel_Detalle.Enabled = False
                'Me.Panel_Obs.Enabled = False


                'Me.cboEmpresa.Enabled = True
                'Me.cboTienda.Enabled = True
                'Me.cboSerie.Enabled = True
                'Me.cboAlmacen.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = False

                Me.Panel_FiltroDetalle.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False

            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = False

                Me.Panel_FiltroDetalle.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa
                Me.btnInicio.Visible = True
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Empleado.Enabled = False

                Me.Panel_FiltroDetalle.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboAlmacen.Enabled = False

                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub
    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)
        listaPersona = (New Negocio.Empleado).EmpleadoSelectActivoxParams_Find(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try



            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        Me.listaEmpleado = getListaEmpleado()


        For i As Integer = 0 To Me.listaEmpleado.Count - 1

            If (Me.listaEmpleado(i).IdPersona = IdPersona) Then
                Throw New Exception("LA PERSONA SELECCIONADA YA HA SIDO INGRESADA. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next


        Me.listaEmpleado.Add(objPersona)
        setListaEmpleado(Me.listaEmpleado)

        Me.GV_Empleado.DataSource = Me.listaEmpleado
        Me.GV_Empleado.DataBind()

        objScript.onCapa(Me, "capaPersona")

    End Sub

#End Region



    Protected Sub GV_Empleado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Empleado.SelectedIndexChanged
        quitarEmpleado(Me.GV_Empleado.SelectedIndex)
    End Sub
    Private Sub quitarEmpleado(ByVal Index As Integer)

        Try


            Me.listaEmpleado = getListaEmpleado()
            Me.listaEmpleado.RemoveAt(Index)
            setListaEmpleado(Me.listaEmpleado)

            Me.GV_Empleado.DataSource = Me.listaEmpleado
            Me.GV_Empleado.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboLinea_Filtro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea_Filtro.SelectedIndexChanged
        Try
            Dim c As New Combo
            With c
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboTipoExistencia_Filtro.SelectedValue), True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub actualizarListaDetalleDocumento()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)

        Next


        setListaDetalleDocumento(Me.listaDetalleDocumento)


    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click

        GenerarDocumentoAjusteInventario()



    End Sub
    Private Sub GenerarDocumentoAjusteInventario()
        Try

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()
            Dim listaDocumento_Persona As List(Of Entidades.Documento_Persona) = obtenerListaDocumento_Persona()

            objDocumento.Id = (New Negocio.DocumentoAjusteInventario).DocumentoTomaInventario_GenerarDocumento(objDocumento, listaRelacionDocumento, listaDocumento_Persona)

            If (objDocumento.Id > 0) Then

                Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                verFrm(FrmModo.Nuevo, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Inicio
                    .Id = Nothing
                Case Else
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select




            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdDestinatario = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

        End With

        Return objDocumento

    End Function


    Private Sub registrarCambios_DetalleDocumento()

        Try

            actualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            If ((New Negocio.TomaInventario).DocumentoTomaInventario_UpdateDetalle(objDocumento, Me.listaDetalleDocumento) > 0) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        ActualizarDocumento()

    End Sub

    Private Sub ActualizarDocumento()

        Try

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()
            Dim listaDocumento_Persona As List(Of Entidades.Documento_Persona) = obtenerListaDocumento_Persona()
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

            Dim listaDetalleDocumento_Sobrante As List(Of Entidades.DetalleDocumento) = obtenerListaDetalle_Sobrante()
            Dim listaDetalleDocumento_Faltante As List(Of Entidades.DetalleDocumento) = obtenerListaDetalle_Faltante()

            objDocumento.Id = (New Negocio.DocumentoAjusteInventario).DocumentoAjusteInventario_UpdateDocumento(objDocumento, 1, -1, listaDetalleDocumento_Sobrante, listaDetalleDocumento_Faltante, objObservaciones, listaDocumento_Persona)
            If (objDocumento.Id > 0) Then

                Me.GV_Detalle.DataSource = Nothing
                Me.GV_Detalle.DataBind()
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)

        Me.listaDocumentoRef = getListaDocumentoRef()
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

            End With

            lista.Add(objRelacionDocumento)

        Next

        Return lista

    End Function
    Private Function obtenerListaDocumento_Persona() As List(Of Entidades.Documento_Persona)

        Dim lista As New List(Of Entidades.Documento_Persona)

        Me.listaEmpleado = getListaEmpleado()
        For i As Integer = 0 To Me.listaEmpleado.Count - 1

            Dim obj As New Entidades.Documento_Persona
            With obj

                .IdPersona = Me.listaEmpleado(i).IdPersona

            End With

            lista.Add(obj)

        Next

        Return lista

    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function

    Private Sub GV_Detalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Detalle.SelectedIndexChanged

        quitarDetalle(Me.GV_Detalle.SelectedIndex)

    End Sub

    Private Sub quitarDetalle(ByVal Index As Integer)
        Try

            actualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAt(Index)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnFiltrar_Filtro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFiltrar_Filtro.Click

        Me.listaDocumentoRef = getListaDocumentoRef()
        Dim IdDocumentoTomaInv As Integer = 0
        If (Me.listaDocumentoRef.Count = 1) Then

            IdDocumentoTomaInv = Me.listaDocumentoRef(0).Id

        Else
            Throw New Exception("NO SE HA SELECCIONADO UN DOCUMENTO DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")
        End If


        Me.ViewState.Add("_IdDocumento", Me.hddIdDocumento.Value)
        Me.ViewState.Add("_IdDocumentoTomaInv", IdDocumentoTomaInv)
        Me.ViewState.Add("_Linea", Me.cboLinea_Filtro.SelectedValue)
        Me.ViewState.Add("_SubLinea", Me.cboSubLinea_Filtro.SelectedValue)

        Me.ViewState.Add("_Descripcion", Me.txtDescripcionProd_Filtro.Text.Trim)
        Me.ViewState.Add("_CodigoProducto", Me.txtCodigoProd_Filtro.Text.Trim)
        Me.ViewState.Add("_TipoExistencia", Me.cboTipoExistencia_Filtro.SelectedValue)

        filtarDetalleDocumento(0)

    End Sub
    Private Sub filtarDetalleDocumento(ByVal TipoMov As Integer)
        Try

          
            Dim index As Integer = 0
            Select Case tipoMov
                Case 0 '********************************************************* INICIO
                    index = 0
                Case 1 '********************************************************* Anterior
                    index = (CInt(Me.txtPageIndex_Det.Text) - 1) - 1
                Case 2 '********************************************************* Posterior
                    index = (CInt(Me.txtPageIndex_Det.Text) - 1) + 1
                Case 3 '********************************************************* IR
                    index = (CInt(Me.txtPageIndexGo_Det.Text) - 1)
            End Select

            Dim lista As List(Of Entidades.DetalleDocumentoTomaInventario) = (New Negocio.DocumentoAjusteInventario).DocumentoAjusteInventario_FiltroDetalle(CInt(Me.ViewState("_IdDocumento")), CInt(ViewState("_IdDocumentoTomaInv")), CInt(Me.ViewState("_Linea")), CInt(Me.ViewState("_SubLinea")), "", CStr(Me.ViewState("_Descripcion")), CStr(Me.ViewState("_CodigoProducto")), CInt(Me.ViewState("_TipoExistencia")), index, CInt(Me.cboPageSizeDoc.SelectedValue))

            If Not IsNothing(lista) Then

                If lista.Count > 0 Then

                    Me.txtPageIndex_Det.Text = CStr(index + 1)

                    Me.GV_Detalle.DataSource = lista
                    Me.GV_Detalle.DataBind()

                    calcularCostoTotal(lista)

                End If

            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub btnAnterior_Det_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Det.Click
        filtarDetalleDocumento(1)
    End Sub

    Private Sub btnSiguiente_Det_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguiente_Det.Click
        filtarDetalleDocumento(2)
    End Sub

    Private Sub btnIr_det_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_det.Click
        filtarDetalleDocumento(3)
    End Sub



    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim gvrow As GridViewRow = CType(e.Row.Cells(12).NamingContainer, GridViewRow)

            If (CType(gvrow.FindControl("hddIsAjustado"), HiddenField).Value = "0") Then
                gvrow.BackColor = Drawing.Color.Yellow
            End If

        End If

    End Sub

    Private Sub calcularCostoTotal(ByVal lista As List(Of Entidades.DetalleDocumentoTomaInventario))
        Dim costoFaltante As Decimal = 0
        Dim costoSobrante As Decimal = 0

        For i As Integer = 0 To lista.Count - 1

            '******** Calculamos los costos de solo los PENDIENTES
            If (lista(i).Ajustado = "0") Then
                costoFaltante = costoFaltante + lista(i).CostoFaltante
                costoSobrante = costoSobrante + lista(i).CostoSobrante
            End If

        Next

        Me.txtCostoFaltante.Text = CStr(Math.Round(costoFaltante, 2))
        Me.txtCostoSobrante.Text = CStr(Math.Round(costoSobrante, 2))

    End Sub
#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            If (Me.GV_BusquedaAvanzado.Rows.Count > 0) Then
                objScript.onCapa(Me, "capaBusquedaAvanzado")
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "    alert('NO SE HALLARON REGISTROS.');   onCapa('capaBusquedaAvanzado');   ", True)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try

            cargarDocumentoAjusteInventario(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoAjusteInventario(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '**************** LIMPIAMOS EL FORMULARIO
        verFrm(FrmModo.Inicio, True, True, True, True, True)

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO
        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        Me.listaEmpleado = (New Negocio.Documento_Persona).Documento_PersonaSelectxIdDocumento(objDocumento.Id)
        Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumento.Id)

        '****************** DETALLE AJUSTE DE INVENTARIO

        If (Me.listaDocumentoRef.Count = 1) Then

            Me.ViewState.Add("_IdDocumento", objDocumento.Id)
            Me.ViewState.Add("_IdDocumentoTomaInv", Me.listaDocumentoRef(0).Id)
            Me.ViewState.Add("_Linea", Me.cboLinea_Filtro.SelectedValue)
            Me.ViewState.Add("_SubLinea", Me.cboSubLinea_Filtro.SelectedValue)

            Me.ViewState.Add("_Descripcion", Me.txtDescripcionProd_Filtro.Text.Trim)
            Me.ViewState.Add("_CodigoProducto", Me.txtCodigoProd_Filtro.Text.Trim)
            Me.ViewState.Add("_TipoExistencia", Me.cboTipoExistencia_Filtro.SelectedValue)

            filtarDetalleDocumento(0)
        
        End If

        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '*************** GUARDAMOS EN SESSION
        setListaDocumentoRef(Me.listaDocumentoRef)
        setListaEmpleado(Me.listaEmpleado)

        '************** 
        cargarDocumentoAjusteInventario_GUI(objDocumento, Me.listaDocumentoRef, Me.listaEmpleado, objObservaciones)
        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

    End Sub

    Private Sub cargarDocumentoAjusteInventario_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento As List(Of Entidades.Documento), ByVal listaEmpleado As List(Of Entidades.PersonaView), ByVal objObservaciones As Entidades.Observacion)

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboAlmacen.Items.FindByValue(CStr(objDocumento.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            End If

            Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
            Me.txtCodigoDocumento.Text = .Codigo

            If (.FechaEmision <> Nothing) Then
                Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            End If

            Me.hddIdDocumento.Value = CStr(.Id)
            Me.hddCodigoDocumento.Value = .Codigo

        End With

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '**************** LISTA EMPLEADO
        Me.GV_Empleado.DataSource = listaEmpleado
        Me.GV_Empleado.DataBind()

        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

    End Sub

#End Region

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try

            verFrm(FrmModo.Editar, False, False, False, False, False)

            Me.listaEmpleado = getListaEmpleado()
            Me.listaDocumentoRef = getListaDocumentoRef()

            Me.GV_Empleado.DataSource = Me.listaEmpleado
            Me.GV_Empleado.DataBind()

            filtarDetalleDocumento(0)

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try

            cargarDocumentoAjusteInventario(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularCantidades('1'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click

        anularDocumento()

    End Sub

    Private Sub anularDocumento()

        Try


            If ((New Negocio.DocumentoAjusteInventario).DocumentoAjusteInventario_Anular(CInt(Me.hddIdDocumento.Value))) Then

                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoAjusteInventario).DocumentoAjusteInventario_SelectDocRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), serie, codigo, fechaInicio, fechafin, CInt(Me.CbotipoDocumento.SelectedValue))

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDocumentoRef.Clear()
            Me.listaDocumentoRef.Add(objDocumento)
            setListaDocumentoRef(Me.listaDocumentoRef)

            '******************* CARGAMOS LA LISTA DE DOCUMENTOS REF
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            '****************** CARGAMOS LA LISTA DE EMPLEADOS 
            Me.listaEmpleado = (New Negocio.Documento_Persona).Documento_PersonaSelectxIdDocumento(IdDocumentoRef)
            setListaEmpleado(Me.listaEmpleado)

            Me.GV_Empleado.DataSource = Me.listaEmpleado
            Me.GV_Empleado.DataBind()

            Me.cboEmpresa.Enabled = False
            Me.cboTienda.Enabled = False
            Me.cboAlmacen.Enabled = False

            Me.btnNuevo.Visible = True

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setListaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            '************ EMPLEADO
            Me.listaEmpleado = getListaEmpleado()
            Me.listaEmpleado.Clear()
            setListaEmpleado(Me.listaEmpleado)

            Me.GV_Empleado.DataSource = Me.listaEmpleado
            Me.GV_Empleado.DataBind()

            Me.cboEmpresa.Enabled = True
            Me.cboTienda.Enabled = True
            Me.cboAlmacen.Enabled = True
            Me.btnNuevo.Visible = True

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region

    Protected Sub btnInicio_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnInicio.Click
        Try

            verFrm(FrmModo.Inicio, True, True, True, True, True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto

        '************** DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        '************* EMPLEADOS
        Me.GV_Empleado.DataSource = Nothing
        Me.GV_Empleado.DataBind()


        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()
        Me.txtCostoFaltante.Text = ""
        Me.txtCostoSobrante.Text = ""


        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '************** HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""


        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
        Me.gvBuscar.DataSource = Nothing
        Me.gvBuscar.DataBind()

    End Sub



    Private Function obtenerListaDetalle_Faltante() As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Dim fila As GridViewRow = Me.GV_Detalle.Rows(i)

            If (CType(fila.FindControl("chbAjustar"), CheckBox).Checked And CDec(fila.Cells(6).Text) > 0) Then

                Dim objDetalle As New Entidades.DetalleDocumento

                With objDetalle

                    .IdProducto = CInt(CType(fila.FindControl("hddIdProducto"), HiddenField).Value)
                    .IdDetalleAfecto = CInt(CType(fila.FindControl("hddIdDetalleDocumento"), HiddenField).Value)
                    .IdUnidadMedida = CInt(CType(fila.FindControl("hddIdUnidadMedida"), HiddenField).Value)
                    .UMedida = CStr(fila.Cells(2).Text)
                    .CostoMovIngreso = CDec(fila.Cells(8).Text)
                    .Cantidad = CDec(fila.Cells(6).Text)
                    .Factor = -1
                    lista.Add(objDetalle)

                End With

            End If

        Next


        Return lista

    End Function

    Private Function obtenerListaDetalle_Sobrante() As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Dim fila As GridViewRow = Me.GV_Detalle.Rows(i)

            If (CType(fila.FindControl("chbAjustar"), CheckBox).Checked And CDec(fila.Cells(7).Text) > 0) Then

                Dim objDetalle As New Entidades.DetalleDocumento

                With objDetalle

                    .IdProducto = CInt(CType(fila.FindControl("hddIdProducto"), HiddenField).Value)
                    .IdDetalleAfecto = CInt(CType(fila.FindControl("hddIdDetalleDocumento"), HiddenField).Value)
                    .IdUnidadMedida = CInt(CType(fila.FindControl("hddIdUnidadMedida"), HiddenField).Value)
                    .UMedida = CStr(fila.Cells(2).Text)
                    .CostoMovIngreso = CDec(fila.Cells(8).Text)
                    .Cantidad = CDec(fila.Cells(7).Text)
                    .Factor = 1
                    lista.Add(objDetalle)

                End With

            End If

        Next


        Return lista

    End Function

    Protected Sub btnEliminarAjuste_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim IdProducto As Integer = CInt(CType((CType((CType(sender, ImageButton).NamingContainer), GridViewRow).FindControl("hddIdProducto")), HiddenField).Value)


            If ((New Negocio.DocumentoAjusteInventario).EliminarProcesoxIdProductoxIdDocumentoAjusteInv(CInt(Me.hddIdDocumento.Value), IdProducto)) Then

                Me.GV_Detalle.DataSource = Nothing
                Me.GV_Detalle.DataBind()

                Me.txtCostoFaltante.Text = ""
                Me.txtCostoSobrante.Text = ""

                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try


    End Sub

    Private Sub cboTipoExistencia_Filtro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia_Filtro.SelectedIndexChanged
        Try
            Dim c As New Combo
            With c
                .llenarCboLineaxTipoExistencia(cboLinea_Filtro, CInt(Me.cboTipoExistencia_Filtro.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboTipoExistencia_Filtro.SelectedValue), True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

   
End Class
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmProcesoInventarioInicial.aspx.vb" Inherits="APPWEB.FrmProcesoInventarioInicial" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick=" return(     valSaveDocumento()       ); "
                                Width="80px" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                INVENTARIO INICIAL
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Almac�n:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboAlmacen" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                LISTADO DE PRODUCTOS
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto">
                            Tipo Existencia:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            L�nea:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Sub L�nea:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboSubLinea" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnAceptar_Detalle" runat="server" Text="Aceptar" Width="80px" OnClientClick=" return( onclick_Aceptar_Detalle() ); " />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="Texto">
                                        <asp:RadioButtonList ID="rbTipoBusqueda" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0">C�digo</asp:ListItem>
                                            <asp:ListItem Value="1" Selected="True">Descripci�n</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnAceptar_Detalle">                                        
                                        <asp:TextBox ID="txt_descripcion" MaxLength="50" runat="server" Width="300px"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                                SuppressPostBack="true">
                            </cc1:CollapsiblePanelExtender>
                            <asp:Image ID="Image21_11" runat="server" Height="16px" />
                            <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label><asp:Panel
                                ID="Panel_BusqAvanzadoProd" runat="server">
                                <table width="100">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Atributo:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );"
                                                            Style="cursor: hand;" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" Style="cursor: hand;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                Width="650px">
                                                <RowStyle CssClass="GrillaRow" />
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                        HeaderStyle-Height="25px" ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label><asp:HiddenField
                                                                ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaCabecera" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pl_Detalle" runat="server" Height="15000px" ScrollBars="Vertical">
                    <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="CodigoProducto" ItemStyle-Font-Bold="true" HeaderText="C�digo"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="UM" ItemStyle-Font-Bold="true" HeaderText="U.M." HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCantidad" runat="server" Width="75px" onFocus=" return( aceptarFoco(this)  );  "
                                                    onKeypress="return(validarNumeroPuntoPositivo('event'));" onblur="return(valBlur(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F4}")%>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUMedida")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Costo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMoneda" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCosto" runat="server" Width="75px" onFocus=" return( aceptarFoco(this)  );  "
                                                    onKeypress="return(validarNumeroPuntoPositivo('event'));" onblur="return(valBlur(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD","{0:F2}")%>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="26" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        function valSaveDocumento() {


            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }



            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }



            var cboAlmacen = document.getElementById('<%=cboAlmacen.ClientID%>');
            if (isNaN(parseInt(cboAlmacen.value)) || cboAlmacen.value.length <= 0) {
                alert('Debe seleccionar un Almac�n.');
                return false;
            }



            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {
                alert('DEBE SELECCIONAR PRODUCTOS. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            return confirm('Desea continuar con la Operaci�n ?');
        }

        ////////////////////// Busqueda Avanzada
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        //////////////////////////////

        function onclick_Aceptar_Detalle() {
            var txt_descripcion = document.getElementById('<%=txt_descripcion.ClientID %>');
            var IdSubLinea = parseInt(document.getElementById('<%=cboSubLinea.ClientID %>').value);
            if (isNaN(IdSubLinea)) { IdSubLinea = 0; }

            if (IdSubLinea <= 0 && txt_descripcion.value.length <= 0) {

                var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
                if (grilla == null) {
                    alert('Los filtros de busqueda son:\nAl nivel de Sub-L�nea.\nPor c�digo o descripci�n de producto (Se recomienda ser especifico).');
                    return false;
                }
            }

            return true;
        }
        
    </script>

</asp:Content>

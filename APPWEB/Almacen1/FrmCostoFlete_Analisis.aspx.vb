﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmCostoFlete_Analisis
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaDocumentoRef2 As List(Of Entidades.Documento)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad_Frm()
    End Sub

    Private Sub valOnLoad_Frm()
        Try


            If (Not Me.IsPostBack) Then

                inicializarFrm()

            End If

            actualizarMoneda()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()
        With objCbo

            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)

            .llenarCboTiendaxIdEmpresa(Me.cboSucursalOrigen, CInt(Me.cboEmpresa.SelectedValue), False)
            .llenarCboTiendaxIdEmpresa(Me.cboSucursalDestino, CInt(Me.cboEmpresa.SelectedValue), False)

            .llenarCboAlmacenxIdTienda(Me.cboAlmacenOrigen, CInt(Me.cboSucursalOrigen.SelectedValue), False)
            .llenarCboAlmacenxIdTienda(Me.cboAlmacenDestino, CInt(Me.cboSucursalDestino.SelectedValue), False)

            .LlenarCboTipoPV(Me.cboTipoPrecioVenta, False)

            .LlenarCboMoneda(Me.cboMoneda, False)
            .LlenarCboTipoDocumentoExterno(Me.cboTipoDocumento)

        End With



        Dim IdTipoPrecioVenta As Integer = (New Negocio.TipoPrecioV).SelectIdTipoPrecioVDefault

        If (Me.cboTipoPrecioVenta.Items.FindByValue(CStr(IdTipoPrecioVenta)) IsNot Nothing) Then
            Me.cboTipoPrecioVenta.SelectedValue = CStr(IdTipoPrecioVenta)
        End If

        Dim objCalendario As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxFecha((New Negocio.FechaActual).SelectFechaActual)
        Me.txtFechaInicio_DocRef.Text = Format(objCalendario.cal_FechaIni, "dd/MM/yyyy")
        Me.txtFechaFin_DocRef.Text = Format(objCalendario.cal_FechaFin, "dd/MM/yyyy")
        Me.txtFechaInicio_DocRef2.Text = Format(objCalendario.cal_FechaIni, "dd/MM/yyyy")
        Me.txtFechaFin_DocRef2.Text = Format(objCalendario.cal_FechaFin, "dd/MM/yyyy")

        Me.listaDocumentoRef = New List(Of Entidades.Documento)
        setlistaDocumentoRef(Me.listaDocumentoRef)

        Me.listaDocumentoRef2 = New List(Of Entidades.Documento)
        setlistaDocumentoRef2(Me.listaDocumentoRef2)

        actualizarOpcionesBusquedaDocRef(0, False)
        actualizarOpcionesBusquedaDocRef2(0, False)
    End Sub

    Private Sub actualizarMoneda()

        Me.lblMoneda_CostoTotal.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMoneda_CostoTotal_Transp.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMoneda_CostoTotalxPeso.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMoneda_CostoTotalxUnd.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMoneda_CostoTotalxVolumen.Text = Me.cboMoneda.SelectedItem.ToString

    End Sub

    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub

    Private Function getlistaDocumentoRef2() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef2"), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef2(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef2")
        Session.Add("listaDocumentoRef2", lista)
    End Sub

#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdEmpresa As Integer = 0

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            ''*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            Dim lista As List(Of Entidades.Documento) = (New Negocio.DocGuiaRemision).DocumentoGuiaRemision_CostoFlete_SelectxParams(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacenOrigen.SelectedValue), CInt(Me.cboAlmacenDestino.SelectedValue), fechaInicio, fechafin, serie, codigo)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            Me.GV_DocumentoRef_GR.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef_GR.DataBind()

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub


    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function



    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef_GR.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef_GR.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()

            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef_GR.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef_GR.DataBind()


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try

            With objCbo

                .llenarCboTiendaxIdEmpresa(Me.cboSucursalOrigen, CInt(Me.cboEmpresa.SelectedValue), False)
                .llenarCboTiendaxIdEmpresa(Me.cboSucursalDestino, CInt(Me.cboEmpresa.SelectedValue), False)

                .llenarCboAlmacenxIdTienda(Me.cboAlmacenOrigen, CInt(Me.cboSucursalOrigen.SelectedValue), False)
                .llenarCboAlmacenxIdTienda(Me.cboAlmacenDestino, CInt(Me.cboSucursalDestino.SelectedValue), False)

            End With


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSucursalOrigen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSucursalOrigen.SelectedIndexChanged
        valOnChange_cboSucursalOrigen()
    End Sub
    Private Sub valOnChange_cboSucursalOrigen()

        Try

            With objCbo

                .llenarCboAlmacenxIdTienda(Me.cboAlmacenOrigen, CInt(Me.cboSucursalOrigen.SelectedValue), False)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub valOnChange_cboSucursalDestino()

        Try

            With objCbo

                .llenarCboAlmacenxIdTienda(Me.cboAlmacenDestino, CInt(Me.cboSucursalDestino.SelectedValue), False)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboSucursalDestino_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSucursalDestino.SelectedIndexChanged
        valOnChange_cboSucursalDestino()
    End Sub

    Private Sub btnRealizarAnalisis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRealizarAnalisis.Click

        realizarAnalisis()

    End Sub
    Private Sub realizarAnalisis()

        Try

            Dim objDataTable As DataTable = obtenerDataTable()

            Me.GV_Cuadro_Analisis.DataSource = (New Negocio.DocGuiaRemision).CostoFlete_AnalisisCostoFlete_PrecioVenta(objDataTable, Nothing, Nothing, Nothing, Nothing, CInt(Me.cboSucursalDestino.SelectedValue), CInt(Me.cboTipoPrecioVenta.SelectedValue), CInt(Me.cboSucursalOrigen.SelectedValue), CInt(Me.cboMoneda.SelectedValue))
            Me.GV_Cuadro_Analisis.DataBind()

            calcularTotales()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub calcularTotales()

        Dim costoTotalxPeso As Decimal = 0
        Dim costoTotalxUnd As Decimal = 0
        Dim costoTotalxVolumen As Decimal = CDec(Me.txtCostoTotalxVolumen.Text)
        Dim costoTransportista As Decimal

        For i As Integer = 0 To Me.GV_DocumentoRef_TR.Rows.Count - 1
            Dim valor As Decimal = (New Negocio.Util).CalcularValorxTipoCambio(CDec(CType(Me.GV_DocumentoRef_TR.Rows(i).FindControl("lblMonto"), Label).Text), CInt(CType(Me.GV_DocumentoRef_TR.Rows(i).FindControl("hddIdMoneda"), HiddenField).Value), CInt(Me.cboMoneda.SelectedValue), "E", (New Negocio.FechaActual).SelectFechaActual)
            costoTransportista = costoTransportista + valor
        Next
        Me.txtCostoTotal_Transp.Text = Format(costoTransportista, "F3")

        Dim costoTotal As Decimal = 0
        Dim desviacion As Decimal = 0

        For i As Integer = 0 To Me.GV_Cuadro_Analisis.Rows.Count - 1

            Dim TipoCalculoFlete As String = CStr(CType(Me.GV_Cuadro_Analisis.Rows(i).FindControl("hddTipoCalculoFlete"), HiddenField).Value)
            Dim costoTotalxPeso_GVR As Decimal = CDec(CType(Me.GV_Cuadro_Analisis.Rows(i).FindControl("lblCostoTotal"), Label).Text)
            Dim costoTotalxUnd_GVR As Decimal = CDec(CType(Me.GV_Cuadro_Analisis.Rows(i).FindControl("lblCostoTotal"), Label).Text)

            Select Case TipoCalculoFlete

                Case "P"

                    costoTotalxPeso = costoTotalxPeso + costoTotalxPeso_GVR
                    Me.GV_Cuadro_Analisis.Rows(i).Cells(6).BackColor = Drawing.Color.Gold

                Case "U"

                    costoTotalxUnd = costoTotalxUnd + costoTotalxUnd_GVR
                    Me.GV_Cuadro_Analisis.Rows(i).Cells(7).BackColor = Drawing.Color.Gold

            End Select

        Next

        costoTotal = costoTotalxPeso + costoTotalxUnd + costoTotalxVolumen
        If (costoTransportista > 0) Then
            desviacion = (costoTotal / costoTransportista) * 100
        End If

        '************************** PRINT
        Me.txtCostoTotalxPeso.Text = Format(costoTotalxPeso, "F3")
        Me.txtCostoTotalxUnd.Text = Format(costoTotalxUnd, "F3")
        Me.txtCostoTotal.Text = Format(costoTotal, "F3")
        Me.txtDesviacion.Text = Format(desviacion, "F3")

    End Sub

    Private Function obtenerDataTable() As DataTable

        Dim objDataTable As New DataTable
        objDataTable.Columns.Add("Columna1")

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            objDataTable.Rows.Add(Me.listaDocumentoRef(i).Id)

        Next

        Return objDataTable

    End Function

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        OnClick_Guardar()
    End Sub



    Dim lista_ProductoTipoPV As List(Of Entidades.ProductoTipoPV)
    Dim obj_ProductoTipoPV As Entidades.ProductoTipoPV

    Private Sub OnClick_Guardar()
        Try

            lista_ProductoTipoPV = New List(Of Entidades.ProductoTipoPV)

            For Each row As GridViewRow In Me.GV_Cuadro_Analisis.Rows

                If CType(row.FindControl("chb_Update"), CheckBox).Checked Then

                    obj_ProductoTipoPV = New Entidades.ProductoTipoPV

                    With obj_ProductoTipoPV

                        .IdProducto = CInt(CType(row.FindControl("hddIdProducto"), HiddenField).Value)
                        .Estado = "1"    ' ******************* ESTADO ACTIVO
                        .IdTienda = CInt(Me.cboSucursalDestino.SelectedValue)
                        .IdTipoPv = CInt(Me.cboTipoPrecioVenta.SelectedValue)
                        .IdUnidadMedida = CInt(CType(row.FindControl("hddIdUnidadMedida_UMP"), HiddenField).Value)
                        .IdUsuario = CInt(Session("IdUsuario"))
                        .PUtilFijo = 0 ' CDec(CType(DGV_SL_UMP.Rows(i).Cells(4).FindControl("txtPorcentUtilidad_SL_UMP"), TextBox).Text)
                        .PUtilVariable = 0
                        .Utilidad = 0 ' CDec(CType(DGV_SL_UMP.Rows(i).Cells(5).FindControl("txtUtilidad_SL_UMP"), TextBox).Text)
                        .IdMoneda = CInt(CType(row.FindControl("hddIdMoneda"), HiddenField).Value)

                        If .IdMoneda = 1 Or .IdMoneda = 2 Then '********* SOLES | DOLARES
                            .Valor = CDec(CType(row.FindControl("txtPrecioFinal"), TextBox).Text)
                        Else
                            Throw New Exception("PROBLEMAS EN LA LECTURA DE LA MONEDA DESTINO. MONEDAS PERMITIDAS ( S/. ) ( $ ).")
                        End If

                        .UnidadPrincipal = True
                        .Retazo = False

                    End With

                    Me.lista_ProductoTipoPV.Add(obj_ProductoTipoPV)
                    obj_ProductoTipoPV = Nothing

                End If

            Next


            If (New Negocio.ProductoTipoPV).InsertaProductoTipoPVT(Me.lista_ProductoTipoPV, True) Then
                objScript.mostrarMsjAlerta(Me, "La operación se realizó con éxito.")
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef2.Click

        mostrarDocumentosRef_Find2(0)

    End Sub

    Private Sub mostrarDocumentosRef_Find2(ByVal TipoMov As Integer)
        Try

            Dim serie As String = ""
            Dim codigo As String = ""
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdEmpresa As Integer = 0

            Select Case CInt(Me.rdbBuscarDocRef2.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef2.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef2.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = Me.txtSerie_BuscarDocRef2.Text.Trim
                    codigo = Me.txtCodigo_BuscarDocRef2.Text.Trim
            End Select


            Dim index As Integer = 0
            Select Case TipoMov
                Case 0 '********************************************************* INICIO
                    index = 0
                Case 1 '********************************************************* Anterior
                    index = (CInt(txtPageIndex_Documento.Text) - 1) - 1
                Case 2 '********************************************************* Posterior
                    index = (CInt(txtPageIndex_Documento.Text) - 1) + 1
                Case 3 '********************************************************* IR
                    index = (CInt(txtPageIndex_Documento.Text) - 1)
            End Select


            ''*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)

            Dim lista As List(Of Entidades.Documento) = _
            (New Negocio.Documento).SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExt( _
            CInt(Me.cboTipoDocumento.SelectedValue), 0, serie, codigo, index, Me.GV_DocumentosReferencia_Find2.PageSize)

            '(New Negocio.Documento).Documento_BuscarDocumentoRef_Externo(0, 0, 0, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, 0, fechaInicio, fechafin, False)



            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find2.DataSource = lista
                Me.GV_DocumentosReferencia_Find2.DataBind()
                Me.txtPageIndex_Documento.Text = CStr(index + 1)
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia2")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo2.Click
        mostrarDocumentosRef_Find2(0)
    End Sub

    Protected Sub rdbBuscarDocRef2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef2.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef2(CInt(Me.rdbBuscarDocRef2.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find2.SelectedIndexChanged

        cargarDocumentoReferencia2(CInt(CType(Me.GV_DocumentosReferencia_Find2.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia2(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef2(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            Me.listaDocumentoRef2 = getlistaDocumentoRef2()
            Me.listaDocumentoRef2.Add(objDocumento)
            setlistaDocumentoRef2(Me.listaDocumentoRef2)

            Me.GV_DocumentoRef_TR.DataSource = Me.listaDocumentoRef2
            Me.GV_DocumentoRef_TR.DataBind()

            objScript.onCapa(Me, "capaDocumentosReferencia2")

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub


    Private Function validar_AddDocumentoRef2(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef2 = getlistaDocumentoRef2()

        For i As Integer = 0 To Me.listaDocumentoRef2.Count - 1

            If (Me.listaDocumentoRef2(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function

    Private Sub actualizarOpcionesBusquedaDocRef2(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha2.Visible = True
                Me.Panel_BuscarDocRefxCodigo2.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha2.Visible = False
                Me.Panel_BuscarDocRefxCodigo2.Visible = True
        End Select

        Me.rdbBuscarDocRef2.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia2")
        End If

    End Sub

    Private Sub GV_DocumentoRef_TR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef_TR.SelectedIndexChanged
        quitarDocumentoRef_TR(Me.GV_DocumentoRef_TR.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef_TR(ByVal index As Integer)

        Try

            Me.listaDocumentoRef2 = getlistaDocumentoRef2()

            Me.listaDocumentoRef2.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef2(Me.listaDocumentoRef2)

            '************ GUI
            Me.GV_DocumentoRef_TR.DataSource = Me.listaDocumentoRef2
            Me.GV_DocumentoRef_TR.DataBind()


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub


    Private Sub btnAnterior_Documento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Documento.Click
        mostrarDocumentosRef_Find2(1)
    End Sub

    Private Sub btnPosterior_Documento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Documento.Click
        mostrarDocumentosRef_Find2(2)
    End Sub

    Private Sub btnIr_Documento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Documento.Click
        mostrarDocumentosRef_Find2(3)
    End Sub

#End Region


End Class
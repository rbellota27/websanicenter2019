﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmRptInventario.aspx.vb" Inherits="APPWEB.FrmRptInventario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
<tr><td class="TituloCelda" >Inventario no Valorizado </td> </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_Principal" runat="server">
                <ContentTemplate>
                <table>
                    <tr>
                        <td class="Label" align="right">
                            Empresa:</td>
                        <td>
                            <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" align="right">
                            Almacén:</td>
                        <td>
                            <asp:DropDownList ID="cmbAlmacen" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack ="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    <tr>
                        <td class="Label" align="right">
                            Línea:</td>
                        <td>
                            <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" align="right">
                            Sub Línea:</td>
                        <td>
                            <asp:DropDownList ID="cmbSubLinea" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        <asp:ImageButton ID="btnMostrarReporte" runat="server" 
                        ImageUrl="~/Imagenes/Aceptar_B.JPG" onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';"
                        onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                        OnClientClick="return(mostrarReporte());"
                         />
                        </td>
                    </tr>
                </table>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 800px"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
        function mostrarReporte() {
            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            var IdAlmacen = document.getElementById('<%= cmbAlmacen.ClientID%>').value;
            var IdLinea= document.getElementById('<%= cmbLinea.ClientID%>').value;
            var IdSubLinea= document.getElementById('<%= cmbSubLinea.ClientID%>').value;

            var cbo = document.getElementById('<%= cmbEmpresa.ClientID%>');
            var NomEmpresa = '';
            if (IdEmpresa == 0) {
                for (var i = 1; i < cbo.length; i++) {
                    if (i == (cbo.length - 1)) {
                        NomEmpresa = NomEmpresa + cbo[i].text;
                        break;
                    } else {
                        NomEmpresa = cbo[i].text + ' / ';                   
                    }    
                    
                }
            } else {
                for (var i = 0; i < cbo.length; i++) {
                    if (IdEmpresa == cbo[i].value) {
                        NomEmpresa = cbo[i].text;
                        break;
                    }
                }
            }


            frame1.location.href = 'visor.aspx?iReporte=1&IdEmpresa='+IdEmpresa+'&IdAlmacen='+IdAlmacen+'&IdLinea='+IdLinea+'&IdSubLinea='+IdSubLinea+'&NomEmpresa='+NomEmpresa;
            return false;
        }
    </script>
    
</asp:Content>

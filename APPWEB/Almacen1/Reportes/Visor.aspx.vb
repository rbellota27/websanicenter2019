﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports CrystalDecisions.CrystalReports.Engine
Partial Public Class Visor1
    Inherits System.Web.UI.Page
    Dim reporte As ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")
            If iReporte Is Nothing Then
                iReporte = ""
            End If
            ViewState.Add("iReporte", iReporte)
            Select Case iReporte
                Case "1" '************* Inventario NO Valorizado
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("NomEmpresa", Request.QueryString("NomEmpresa"))
                Case "2" ''inventario Valorizado
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("NomEmpresa", Request.QueryString("NomEmpresa"))
                Case "3"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTipoDocumento", Request.QueryString("IdTipoDocumento"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                Case "4"
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                    ViewState.Add("Opcion", Request.QueryString("Opcion"))
                Case "5"
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                    ViewState.Add("Opcion", Request.QueryString("Opcion"))
                Case "6"
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "7"
                    ViewState.Add("linea", Request.QueryString("linea"))
                    ViewState.Add("sublinea", Request.QueryString("sublinea"))
                    ViewState.Add("idproducto", Request.QueryString("idproducto"))
                Case "8"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))
                    ViewState.Add("IdTipoExistencia", Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("IdProducto", Request.QueryString("Idproducto"))
                    ViewState.Add("Signo", Request.QueryString("Signo"))
                    ViewState.Add("Cantidad", Request.QueryString("Cantidad"))
                    ViewState.Add("NomEmpresa", Request.QueryString("NomEmpresa"))

                Case "9"
                    ViewState.Add("Anio", Request.QueryString("Anio"))
                    ViewState.Add("MesInicial", Request.QueryString("MesInicial"))
                    ViewState.Add("MesFinal", Request.QueryString("MesFinal"))
                    ViewState.Add("CadenaTipoAlmacen", Request.QueryString("CadenaTipoAlmacen"))
                Case "10"
                    ViewState.Add("Anio", Request.QueryString("Anio"))
                    ViewState.Add("Mes", Request.QueryString("Mes"))
                    ViewState.Add("CadenaTipoAlmacen", Request.QueryString("CadenaTipoAlmacen"))
                    'ViewState.Add("Linea", Request.QueryString("Linea"))
                    'ViewState.Add("SubLinea", Request.QueryString("SubLinea"))
                    'ViewState.Add("Pais", Request.QueryString("Pais"))
                Case "11"
                    ViewState.Add("Anio", Request.QueryString("Anio"))
                    ViewState.Add("MesInicial", Request.QueryString("MesInicial"))
                    ViewState.Add("MesFinal", Request.QueryString("MesFinal"))
                    ViewState.Add("CadenaTipoAlmacen", Request.QueryString("CadenaTipoAlmacen"))
                Case "19"
                    ViewState.Add("Anio", Request.QueryString("Anio"))
                    ViewState.Add("Mes", Request.QueryString("Mes"))
                    ViewState.Add("CadenaTipoAlmacen", Request.QueryString("CadenaTipoAlmacen"))
                    ViewState.Add("Linea", Request.QueryString("Linea"))
                    ViewState.Add("SubLinea", Request.QueryString("SubLinea"))
                    ViewState.Add("Pais", Request.QueryString("Pais"))
                Case "20"
                    ViewState.Add("Anio", Request.QueryString("Anio"))
                    ViewState.Add("MesInicial", Request.QueryString("MesInicial"))
                    ViewState.Add("MesFinal", Request.QueryString("MesFinal"))
                    ViewState.Add("CadenaTipoAlmacen", Request.QueryString("CadenaTipoAlmacen"))
                    ViewState.Add("Linea", Request.QueryString("Linea"))
                    ViewState.Add("SubLinea", Request.QueryString("SubLinea"))
                    ViewState.Add("Pais", Request.QueryString("Pais"))
                Case "21"
                    ViewState.Add("Anio", Request.QueryString("Anio"))
                    ViewState.Add("MesInicial", Request.QueryString("MesInicial"))
                    ViewState.Add("MesFinal", Request.QueryString("MesFinal"))
                    ViewState.Add("CadenaTipoAlmacen", Request.QueryString("CadenaTipoAlmacen"))
                    ViewState.Add("Linea", Request.QueryString("Linea"))
                    ViewState.Add("SubLinea", Request.QueryString("SubLinea"))
                    ViewState.Add("Pais", Request.QueryString("Pais"))
                    ViewState.Add("idTienda", Request.QueryString("idTienda"))
            End Select
        End If
        mostrarReporte()
    End Sub

    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString
            Case "1"
                ReporteInvNOValorizado()
            Case "2"
                ReporteInventarioValorizado()
            Case "3"
                ReporteGuiaRecepcionDespacho()
            Case "4"
                ReporteDocTomaInventarioFormato()
            Case "5"
                ReporteDocAjusteInventario()
            Case "6"
                ReporteDocGuiaRecepcion()
            Case "7"
                rptListarProducto()
            Case "8"
                ReportekardexValorizado()
            Case "9"
                ReporteTipAlmacenxanioxmesxtipoalmacen()
            Case "10"
                ReporteTipAlmacenxanioxmes()
            Case "11"
                ResumendeIndicadoresTipAlmacen()
            Case "19"
                ReporteInventarioxMxA2()
            Case "20"
                ReporteInventarioResumen2()
            Case "21"
                ReporteTipoAlmacenAnioMesLineaSubLinea()
        End Select
    End Sub

    Private Sub ReporteTipoAlmacenAnioMesLineaSubLinea()
        Try
            Dim obj As New Negocio.TipoAlmacen

            reporte = New CR_RotacionInventario

            Dim Anio As String = CStr(ViewState.Item("Anio"))
            Dim MesInicial As String = CStr(ViewState.Item("MesInicial"))
            Dim MesFinal As String = CStr(ViewState.Item("MesFinal"))
            Dim CadenaTipoAlmacen As String = CStr(ViewState.Item("CadenaTipoAlmacen"))
            Dim Linea As String = CStr(ViewState.Item("Linea"))
            Dim SubLinea As String = CStr(ViewState.Item("SubLinea"))
            Dim Pais As String = CStr(ViewState.Item("Pais"))
            Dim idTienda As String = CStr(ViewState.Item("idTienda"))

            reporte.SetDataSource(obj.RotacionInventarioAllLineaSubLineaAnio(Anio, MesInicial, MesFinal, CadenaTipoAlmacen, Linea, SubLinea, Pais, idTienda))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

    Private Sub ReporteInventarioResumen2()
        Try
            Dim obj As New Negocio.TipoAlmacen
            reporte = New CR_ResumenIndicadoresxMes

            Dim Anio As String = CStr(ViewState.Item("Anio"))
            Dim MesInicial As String = CStr(ViewState.Item("MesInicial"))
            Dim MesFinal As String = CStr(ViewState.Item("MesFinal"))
            Dim CadenaTipoAlmacen As String = CStr(ViewState.Item("CadenaTipoAlmacen"))
            Dim Linea As String = CStr(ViewState.Item("Linea"))
            Dim SubLinea As String = CStr(ViewState.Item("SubLinea"))
            Dim Pais As String = CStr(ViewState.Item("Pais"))

            reporte.SetDataSource(obj.ResumenIndicadoresLineasubLineaAnio(Anio, MesInicial, MesFinal, CadenaTipoAlmacen, Linea, SubLinea, Pais))
            CRViewer.ReportSource = reporte

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

    Private Sub ReporteInventarioxMxA2()
        Try
            Dim obj As New Negocio.TipoAlmacen

            reporte = New CR_RotacionInventarioMesAnio

            Dim Anio As String = CStr(ViewState.Item("Anio"))
            Dim Mes As String = CStr(ViewState.Item("Mes"))
            Dim Cadena As String = CStr(ViewState.Item("CadenaTipoAlmacen"))
            Dim Linea As String = CStr(ViewState.Item("Linea"))
            Dim SubLinea As String = CStr(ViewState.Item("SubLinea"))
            Dim Pais As String = CStr(ViewState.Item("Pais"))
            Dim mesfinal As String = meses(Mes)


            reporte.SetDataSource(obj.ReporteRotacionInvxAnioxMesLineaSubLinea(Cadena, Anio, Mes, Linea, SubLinea, Pais))
            CRViewer.ReportSource = reporte

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub ResumendeIndicadoresTipAlmacen()
        Try
            Dim obj As New Negocio.TipoAlmacen
            reporte = New CR_ResumenIndicadoresxMes

            Dim Anio As String = CStr(ViewState.Item("Anio"))
            Dim MesInicial As String = CStr(ViewState.Item("MesInicial"))
            Dim MesFinal As String = CStr(ViewState.Item("MesFinal"))
            Dim CadenaTipoAlmacen As String = CStr(ViewState.Item("CadenaTipoAlmacen"))

            Dim ETipoAlmacen As Entidades.TipoAlmacen = New Entidades.TipoAlmacen

            ETipoAlmacen.Anio = Anio
            ETipoAlmacen.CadenaTipoAlmacen = CadenaTipoAlmacen
            ETipoAlmacen.MesInicial = MesInicial
            ETipoAlmacen.MesFinal = MesFinal

            reporte.SetDataSource(obj.ResumenIndicadores(ETipoAlmacen))
            CRViewer.ReportSource = reporte

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

    Private Sub ReporteTipAlmacenxanioxmes()
        Try
            Dim obj As New Negocio.TipoAlmacen
            reporte = New CR_RotacionInventarioMesAnio

            Dim Anio As String = CStr(ViewState.Item("Anio"))
            Dim Mes As String = CStr(ViewState.Item("Mes"))
            Dim Cadena As String = CStr(ViewState.Item("CadenaTipoAlmacen"))
            'Dim Linea As String = CStr(ViewState.Item("Linea"))
            'Dim SubLinea As String = CStr(ViewState.Item("SubLinea"))
            'Dim Pais As String = CStr(ViewState.Item("Pais"))
            Dim mesfinal As String = meses(Mes)

            'lblmensaje.Text = "El reporte mostrado corresponde al : [mes: " + mesfinal + "]" + " y [año: " + Anio + "]"
            'lblmensaje.Visible = True
            reporte.SetDataSource(obj.ReporteRotacionInvxAnioxMes(Cadena, Anio, Mes))
            CRViewer.ReportSource = reporte

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub ReporteTipAlmacenxanioxmesxtipoalmacen()
        Try
            Dim obj As New Negocio.TipoAlmacen
            reporte = New CR_RotacionInventario

            Dim Anio As String = CStr(ViewState.Item("Anio"))
            Dim MesInicial As String = CStr(ViewState.Item("MesInicial"))
            Dim MesFinal As String = CStr(ViewState.Item("MesFinal"))
            Dim CadenaTipoAlmacen As String = CStr(ViewState.Item("CadenaTipoAlmacen"))

            Dim ETipoAlmacen As Entidades.TipoAlmacen = New Entidades.TipoAlmacen

            ETipoAlmacen.Anio = Anio
            ETipoAlmacen.CadenaTipoAlmacen = CadenaTipoAlmacen
            ETipoAlmacen.MesInicial = MesInicial
            ETipoAlmacen.MesFinal = MesFinal

            reporte.SetDataSource(obj.RotacioninventarioAll(ETipoAlmacen))
            CRViewer.ReportSource = reporte


        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

    Private Sub rptListarProducto()
        Try
            Dim obj As New Negocio.Reportes
            reporte = New CR_ListaProducto

            Dim idproducto As Integer

            If CStr(ViewState.Item("idproducto")) = "" Then
                idproducto = 0
            Else
                idproducto = CInt(ViewState.Item("idproducto"))
            End If

            reporte.SetDataSource(obj.rptListarProductos(CInt(ViewState("linea")), CInt(ViewState("sublinea")), idproducto))

            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub


    Private Sub ReporteDocGuiaRecepcion()
        Try
            Dim obj As New Negocio.Reportes
            reporte = New CR_DocGuiaRecepcion

            reporte.SetDataSource(obj.getDataSetDocGuiaRecepcion(CInt(ViewState("IdDocumento"))))

            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub ReporteDocAjusteInventario()
        Try
            Dim obj As New Negocio.TomaInventario
            Dim opcion As Integer = 0

            If IsNumeric(ViewState("Opcion")) Then opcion = CInt(ViewState("Opcion"))

            If opcion = 0 Then
                reporte = New CR_DocAjusteInventario
            Else
                reporte = New CR_DocAjusteInventarioV2
            End If

            reporte.SetDataSource(obj.getDataSet_DocAjusteInventario(CInt(ViewState("IdDocumento")), opcion))

            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteDocTomaInventarioFormato()
        Try
            Dim obj As New Negocio.TomaInventario
            reporte = New CR_DocTomaInventarioFormato

            reporte.SetDataSource(obj.getDataSet_DocTomaInvFormato(CInt(ViewState("IdDocumento")), CStr(ViewState("Opcion"))))

            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteInvNOValorizado()
        Try
            Dim obj As New Negocio.Reportes
            reporte = New CR_InventarioNOValorizado

            reporte.SetDataSource(obj.getDataSetInvNOValorizado(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea"))))
            reporte.SetParameterValue("@NomEmpresa", ViewState("NomEmpresa"))

            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteInventarioValorizado()
        Try
            Dim obj As New Negocio.CXCResumenView
            reporte = New CR_InventarioValorizado
            reporte.SetDataSource(obj.getInventarioValorizado(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea"))))
            'reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            'reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            reporte.SetParameterValue("@NomEmpresa", ViewState("NomEmpresa"))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub ReporteGuiaRecepcionDespacho()
        Try
            Dim obj As New Negocio.CXCResumenView
            Dim idTipoDoc As Integer
            idTipoDoc = CInt(ViewState.Item("IdTipoDocumento"))
            reporte = reporteTipoGrOD(idTipoDoc)
            Dim objtable As New DataTable
            objtable = obj.getGuiaRecepcionOrdendespacho(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdTipoDocumento")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin"))).Tables("DT_DetalleGuiaRecepcionDespacho")
            'Dim producto As String
            'For Each Fila As DataRow In objtable.Rows
            '    producto = Fila("prod_Nombre").ToString
            'Next
            'reporte.SetDataSource(obj.getGuiaRecepcionOrdendespacho(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdTipoDocumento")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin"))))
            reporte.SetDataSource(objtable)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Function reporteTipoGrOD(ByVal idtipo As Integer) As ReportDocument
        If (idtipo = 25) Then
            reporte = New CR_DetallesGuiaRecepcionOrdenDespacho
        Else
            reporte = New CR_DetallesOrdenDespacho
        End If
        Return reporte
    End Function

    Private Sub ReportekardexValorizado()
        Try
            Dim obj As New Negocio.CXCResumenView
            reporte = New CR_KardexValorizado
            reporte.SetDataSource(obj.getKardexValorizado(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("IdProducto")), CStr(ViewState("Signo")), CDec(ViewState("Cantidad"))))
            'reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            'reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            'reporte.SetParameterValue("@NomEmpresa", ViewState("NomEmpresa"))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Function meses(ByVal valor As String) As String
        Dim mes As String = ""

        If (valor = "1") Then
            mes = "Enero"
        ElseIf (valor = "2") Then
            mes = "Febrero"
        ElseIf (valor = "3") Then
            mes = "Marzo"
        ElseIf (valor = "4") Then
            mes = "Abril"
        ElseIf (valor = "5") Then
            mes = "Mayo"
        ElseIf (valor = "6") Then
            mes = "Junio"
        ElseIf (valor = "7") Then
            mes = "Julio"
        ElseIf (valor = "8") Then
            mes = "Agosto"
        ElseIf (valor = "9") Then
            mes = "Septiembre"
        ElseIf (valor = "10") Then
            mes = "Octubre"
        ElseIf (valor = "11") Then
            mes = "Noviembre"
        Else
            mes = "Diciembre"
        End If

        Return mes

    End Function

    Private Sub Visor1_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmInformeProductos.aspx.vb" Inherits="APPWEB.frmInformeProductos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <asp:Panel ID="PanelButtons" runat="server">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btguardar" runat="server" Text="Guardar" Style="cursor: hand; width: 90px" />
                            </td>
                            <td>
                                <asp:Button ID="btbuscar" runat="server" Text="BuscarOC" Style="cursor: hand; width: 90px" />
                            </td>
                            <td>
                                <asp:Button ID="btBuscarI" runat="server" Text="BuscarINF" Style="cursor: hand; width: 90px" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Style="cursor: hand; width: 90px" />
                            </td>
                            <td>
                                <asp:Button ID="bteditar" runat="server" Text="Editar" Style="cursor: hand; width: 90px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">Informe de Producto
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlDetalle" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td align="center">
                                <asp:RadioButtonList ID="rbtipodeoc" runat="server" AutoPostBack="True" CssClass="Texto"
                                    RepeatDirection="Horizontal" Style="cursor: hand;">
                                    <asp:ListItem Value="True" Text="IMPORTADA">IMPORTADA</asp:ListItem>
                                    <asp:ListItem Value="False" Text="NACIONAL">NACIONAL</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 84px" align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">Empresa:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlPropietario" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">Tienda:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlTienda" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">Serie:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlSerie" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Panel ID="pnlBuscarDocumento" runat="server" DefaultButton="btnBuscarDocumento">
                                                <!-- -->
                                                <asp:TextBox ID="tbCodigoDocumento" runat="server" Width="90px" CssClass="TextBox_ReadOnly"></asp:TextBox>
                                            </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarDocumento" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                                ToolTip="Buscar Documento por [ Serie - Número ]." Height="16px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">Mes:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextMes" runat="server" Width="90px"></asp:TextBox>
                                        </td>
                                        <td class="Texto">Fecha Emisi&oacute;n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbFechaEmision" runat="server" Width="90px" CssClass="TextBox_Fecha"></asp:TextBox>
                                        </td>
                                        <td class="Texto">Pais:
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="Txtpais" runat="server" Width="90px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="Texto">Sucursal:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlAlmacen" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">Datos del Proveedor
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto">Raz&oacute;n Social:
                        </td>
                        <td colspan="3" align="left">
                            <asp:TextBox ID="tbProveedor" CssClass="TextBoxReadOnly" runat="server" Width="450px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">R.U.C.:
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="tbRuc" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">Listado de productos
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnAgregar" runat="server" Text="Agregar Producto" OnClientClick="return(CapaCrearProducto());" />
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel_Detalle_producto" runat="server">
                    <asp:GridView ID="gvProducto" runat="server" AutoGenerateColumns="False" Width="100%"
                        HeaderStyle-Height="25px" RowStyle-Height="25px">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:BoundField DataField="CodigoProducto" HeaderText="Código" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad" runat="server" MaxLength="10" Width="45px" Enabled="false"
                                        Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F3}")%>'></asp:TextBox>
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                    <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto","{0:F1}")%>' />
                                    <asp:HiddenField ID="hddCantidadtot" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F3}")%>' />
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nan">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_nan" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"nan","{0:F2}") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="C/Ad">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_cad" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"CAD","{0:F2}") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PesoXCaja">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtpeso" runat="server" MaxLength="10" Width="45px" onfocus="return(aceptarFoco(this));"
                                        Text='<%#DataBinder.Eval(Container.DataItem,"PesoCaja","{0:F2}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="M2XCaja">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtM2" runat="server" MaxLength="10" Width="45px" onfocus="return(aceptarFoco(this));"
                                        Text='<%#DataBinder.Eval(Container.DataItem,"MCaja","{0:F2}") %>' onKeyUp="return(calcularImporte());"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PzaXCaja">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPz" runat="server" MaxLength="10" Width="45px" onfocus="return(aceptarFoco(this));"
                                         Text='<%#DataBinder.Eval(Container.DataItem,"PzaCaja","{0:F2}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="N°Cajas">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtNC" runat="server" MaxLength="10" Width="45px" onfocus="return(aceptarFoco(this));"
                                        onKeyUp="return(calcularImporte());" Text='<%#DataBinder.Eval(Container.DataItem,"NroCaja","{0:F2}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pzas">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtP" runat="server" MaxLength="10" Width="45px" onfocus="return(aceptarFoco(this));"
                                       onKeyUp="return(calcularPiezas(this));" Text='<%#DataBinder.Eval(Container.DataItem,"Pzas","{0:F2}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CalibRe">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_CalibRe" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"CalibRe","{0:F2}") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="JGO">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtJGO" runat="server" MaxLength="10" Width="45px" onfocus="return(aceptarFoco(this));"
                                        onKeyUp="return(calcularJuego(this));" Text='<%#DataBinder.Eval(Container.DataItem,"JGO","{0:F2}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NroPallets">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtNro" runat="server" MaxLength="10" Width="45px" onfocus="return(aceptarFoco(this));"
                                        Text='<%#DataBinder.Eval(Container.DataItem,"NPallet","{0:F2}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtcant" runat="server" MaxLength="10" Width="45px" onfocus="return(aceptarFoco(this));"
                                        Enabled="false" Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F3}")%>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TipoEmpaque">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTipo" runat="server" MaxLength="10" Width="85px" onfocus="return(aceptarFoco(this));"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EstadoPallet">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboEstado" runat="server">
                                        <asp:ListItem Value="1">BUENO</asp:ListItem>
                                        <asp:ListItem Value="2">MALO</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rotulado">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_Rotulado" runat="server" Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Observaciones">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtObs" runat="server" MaxLength="10" Width="90px" onfocus="return(aceptarFoco(this));"
                                        Text='<%#DataBinder.Eval(Container.DataItem,"Observaciones","{0:F3}")%>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tonos">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnTonos" runat="server" CommandName="btnTono" ImageUrl="~/Caja/iconos/ok.gif"
                                        CommandArgument='<%# Container.DataItemindex %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddidObs" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdRemitente" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIGV" runat="server" />
                <asp:HiddenField ID="hddOperativo" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_Idproveedor" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_perc" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_IdDocRelacionado" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_MovCuenta" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_IdTipoDocumento" runat="server" />
                <asp:HiddenField ID="hdd_IdUsuario" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <%--POP UP PARA ASIGNACION DE TONO--%>
    <div id="capaAsignarTono" style="border: 3px solid blue; padding: 8px; border: 3px solid blue; border-image: none; left: 40%; top: 10px; height: auto; display: block; position: fixed; z-index: 4; background-color: white; z-index: 4; display: none;">

        <div style="width: 100%" align="right">
            <asp:ImageButton ID="imgButtonCerrar" runat="server" ImageUrl="~/Imagenes/Cerrar.gif"
                OnClientClick="return(ValidarSumaTonos());" />
        </div>
        <div style="width: 100%; margin-bottom: 5px" class="TituloCelda">
            <asp:Label ID="lblSector" runat="server" Text=""></asp:Label>
        </div>
        <div style="width: 100%">
            <div id="divSector" style="float: left" runat="server" visible="false">
                <span class="Texto">Sector</span><br />
                <asp:DropDownList ID="ddlSector" runat="server" DataValueField="id_sector" DataTextField="nom_sector">
                </asp:DropDownList>
            </div>
            <%--<div style="margin-left:20px;float:left">
                    <span class="Texto">Tono</span><br />
                    <asp:DropDownList ID="ddltono" runat="server" DataValueField="ID_REL_TONO_PROD" DataTextField="NOM_TONO"></asp:DropDownList>
                </div>
                <div style="padding:12px 5px 5px 5px">
            <%--    <asp:Button ID="btnAgregarTono" runat="server" Text="Agregar" />--%>
            <asp:Button ID="btnCrearTono" runat="server" Text="Crear Tono" OnClientClick="return(CapaCrearTono());" />
            <%--<asp:Label ID="sumalertaTono" runat="server" Text="" style="color:red;"></asp:Label>     --%>
            <asp:Label ID="sumalertaTono" runat="server" Style="color: red; font-size: medium;" />
        </div>
        <div style="width: 100%">
            <asp:GridView ID="gvTono" runat="server" AutoGenerateColumns="False">
                <RowStyle CssClass="GrillaRow" />
                <HeaderStyle CssClass="GrillaHeader" />
                <Columns>
                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                    <asp:BoundField HeaderText="IdTono" DataField="idTono" />
                    <asp:BoundField HeaderText="Tono" DataField="nomtono" />
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <label>
                                Cantidad</label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtCantidadTono" runat="server" Width="50" onfocus="return(aceptarFoco(this));"
                                Text='<%#DataBinder.Eval(Container.DataItem,"cantidadTono") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div id="capaCrearTono" style="border: 3px solid blue; padding: 8px; border: 3px solid blue; border-image: none; left: 15%; top: 10px; height: auto; display: block; position: fixed; z-index: 5; background-color: white; z-index: 5; display: none;">
        <div style="width: 100%" align="right">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
        </div>
        <div style="width: 100%" align="center" class="TituloCelda">
            <span>Crear nuevo tono</span>
        </div>
        <div style="width: 100%">
            <table>
                <tr>
                    <td class="Texto">Nombre del tono:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombreTono" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="Texto">Descripción:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDescripcionTono" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button runat="server" ID="btnNuevoTono" Text="Guardar" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="capaCrearProducto" style="border: 3px solid blue; padding: 8px; width: 500px; height: auto; position: fixed; top: 150px; left: 180px; background-color: white; z-index: 5; display: none;">
        <div style="width: 100%" align="right">
            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
        </div>
        <div style="width: 100%" align="center" class="TituloCelda">
            <span>Crear Producto</span>
        </div>
        <div style="width: 100%">
            <table>
                <tr>
                    <td class="Texto">Nombre del Producto:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNomProd" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button runat="server" ID="btnProducto" Text="Guardar" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:HiddenField ID="idCapasignarTono" runat="server" Value="-1" />
    <script type="text/javascript" language="javascript">

        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };

        function onFocus_Readonly(caja) {
            caja.readOnly = true;
            return false;
        }



        function valSaveDocumento() {

            var cbo = document.getElementById('<%=dlSerie.ClientID %>');
            calcularPesoTotalmasdeUnaCaja();
            if (cbo.value == '') {
                alert("EL DOCUMENTO NO HA GENERADO UNA SERIE.");
                return false;
            }

            //*************** Validamos los productos
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            if (grilla == null) {
                alert('LA CUADRICULA DE PRODUCTOS NO CONTIENE REGISTROS.');
                return false;
            } else {
                //************* Validamos las cantidades
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    var cantidad = redondear(parseFloat(rowElem.cells[6].children[0].value), 3);
                    if (isNaN(cantidad)) {
                        cantidad = 0;
                    }
                    var precio = redondear(parseFloat(rowElem.cells[12].children[0].value), 3);
                    if (isNaN(precio)) {
                        precio = 0;
                    }
                    if (cantidad == 0) {
                        alert('INGRESE UNA CANTIDAD MAYOR A CERO PARA EL PRODUCTO ' + rowElem.cells[3].innerHTML + '.');
                        rowElem.cells[6].children[0].select();
                        rowElem.cells[6].children[0].focus();
                        return false;
                    }

                    if (rowElem.cells[7].children[0].selectedIndex != 0) {
                        alert('SELECCIONE LA UNIDAD DE MEDIDA PRINCIPAL PARA EL PRODUCTO ' + rowElem.cells[3].innerHTML + '.');
                        return false;
                    }

                    //if (precio == 0) {
                    //alert('INGRESE SU PRECIO DE ' + rowElem.cells[3].innerHTML + '.');
                    //rowElem.cells[10].children[0].select();
                    //rowElem.cells[10].children[0].focus();
                    //return false;
                    //}


                }
            }



            //****************** Validamos al proveedor
            var idproveedor = document.getElementById('<%=hdd_Idproveedor.ClientID %>');
            if (idproveedor.value == '0') {
                alert('DEBE INGRESAR UN PROVEEDOR PARA EL DOCUMENTO.');
                return false;
            }




            var fechaemi = document.getElementById('<%=tbFechaEmision.ClientID %>');
            if (compararFechas(fechaemi, fechaentrega)) {
                alert('LA FECHA DE ENTREGA DE PRODUCTOS.\nNO PUEDE SER MENOR A LA FECHA DE EMISIÓN DEL DOCUMENTO.');
                return false;
            }

            var IdAlmacen = parseInt(document.getElementById('<%=dlAlmacen.ClientID %>').value);
            if (isNaN(IdAlmacen)) { IdAlmacen = 0; }

            if (IdAlmacen <= 0) {
                alert('SELECCIONE EL ALMACÉN A RECEPCIONAR LA MERCADERIA. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var tbcodigo = document.getElementById('<%=tbCodigoDocumento.ClientID %>');
            if (tbcodigo.value == '') {
                alert('EL NUMERO DEL DOCUMENTO NO HA SIDO GENERADO.');
                return false;
            }



        }

        /////////////////////////////////////////////////////////////////////////////////////////////


        function validarBuscar(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            var boton = document.getElementById('<%=btnBuscarDocumento.ClientID %>');
            if (key == 13) {
                boton.focus();
                return true;
            }
        }


        /////////////////////////////////////////////////////////////////////////////////////////////

        function validarBuscarDocumento() {
            var caja = document.getElementById('<%=tbCodigoDocumento.ClientID %>');
            if (CajaEnBlanco(caja)) {
                alert('INGRESE UN CÓDIGO DE BUSQUEDA.');
                caja.focus();
                return false;
            }
            var cbo = document.getElementById('<%=dlSerie.ClientID %>');
            if (cbo.value == '') {
                alert("GENERE UNA SERIE PARA REALIZAR UNA BUSQUEDA.");
                return false;
            }
            return true;
        }


        function onClick() {

            var oc = document.getElementById('<%=btbuscar.ClientID %>')
            var informe = document.getElementById('<%=btBuscarI.ClientID %>')

            if (oc == true) {

                document.getElementById('<%=hdd_IdTipoDocumento.ClientID %>').value = 16

            } else {
                document.getElementById('<%=hdd_IdTipoDocumento.ClientID %>').value = 2106800005
            }

        }
        ///////////////////////////////////////////////////////////////////////////////////////////
        function setFocus_tbCodigoDocumento() {
            var tbCodigoDocumento = document.getElementById('<%=tbCodigoDocumento.CLientID %>');
            tbCodigoDocumento.focus();
            tbCodigoDocumento.select();
        }

        function click() {

            document.getElementById('<%=hddIdDocumento.ClientID %>').value = 16
        }

        function CapaCrearTono() {
            onCapa("capaCrearTono");
            return false;
        }
        function CapaTono() {
            onCapa("capaAsignarTono");
            return false;
        }
        function CapaCrearProducto() {
            onCapa("capaCrearProducto");
            return false;
        }

        function validarEditar() {
            return true;
        }

        function calcularImporte() {

            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            var Caja = 0;
            var Metros = 0;
            var Total;


            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                Caja = rowElem.cells[8].children[0].value;
                Metros = rowElem.cells[6].children[0].value;

                Total = Caja * Metros;
                rowElem.cells[2].children[0].value = Total;

            } //next

        }
        function ValidarSumaTonos() {
            var indice = document.getElementById("ContentPlaceHolder1_idCapasignarTono").value;
            var cantidadproductoseleccionado = document.getElementById("ContentPlaceHolder1_gvProducto_txtCantidad_" + indice).value;
            var cantidadproductoseleccionadodecimal = parseFloat(cantidadproductoseleccionado).toFixed(3);
            var cantidadtono = 0;
            var SumTotalTonoDecimal = 0;

            //            document.getElementById("sumalertaTono").textContent = "CANTIDAD PRODUCTO :" + cantidadproductoseleccionado;            
            var grilla = document.getElementById('<%=gvTono.ClientID %>');
            for (var i = 0; i < grilla.rows.length - 1; i++) {
                cantidadtono = document.getElementById("ContentPlaceHolder1_gvTono_txtCantidadTono_" + i).value;
                var Cantidadtonodecimal = Number(parseFloat(cantidadtono).toFixed(3));
                SumTotalTonoDecimal = Number(SumTotalTonoDecimal) + Number(Cantidadtonodecimal);
            }
            if (SumTotalTonoDecimal != parseFloat(cantidadproductoseleccionadodecimal).toFixed(3)) {

                document.getElementById("ContentPlaceHolder1_sumalertaTono").textContent = "";
                document.getElementById("ContentPlaceHolder1_sumalertaTono").textContent = "LA SUMA DE MONTOS DE LOS TONOS ES DIFERENTE A LA DE PRODUCTOS";
                return false;
            }
            else {
                document.getElementById("ContentPlaceHolder1_sumalertaTono").textContent = "";
                return true;
            }
        }

        function calcularJuego(obj) {
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            var jgo = 0;
            var Metros = 0;
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                jgo = rowElem.cells[11].children[0].value;
                Metros = rowElem.cells[6].children[0].value;
                if ((parseFloat(Metros)).toFixed(3) == 0.000) {
                    rowElem.cells[2].children[0].value = jgo;
                }

            } //next
            return true;
        }

        function calcularPiezas(obj) {
            debugger;
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            var pzas = 0; Metros = 0; jgo = 0;
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                pzas = rowElem.cells[9].children[0].value;
                jgo = rowElem.cells[11].children[0].value;
                Metros = rowElem.cells[6].children[0].value;
                if (((parseFloat(Metros)).toFixed(3) == 0.000) && ((parseFloat(jgo)).toFixed(3) == 0.000)) {
                    rowElem.cells[2].children[0].value = pzas;
                }

            } //next
            return true;
        }


    </script>
</asp:Content>

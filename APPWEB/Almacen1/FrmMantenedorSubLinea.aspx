<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantenedorSubLinea.aspx.vb" Inherits="APPWEB.FrmMantenedorSubLinea" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
    function valRegimen() {
        var combo = document.getElementById('<%=cmbRegimen.ClientID %>');
        var grilla = document.getElementById('<%=DGV_Regimen.ClientID %>');
        if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                var rowElem = grilla.rows[i];
                if (rowElem.cells[1].innerHTML == combo.value) {
                    alert('El Tipo de R�gimen seleccionado ya ha sido ingresado');
                    return false;
                }

            }
        }
        return true;
    }
    function validarNumeroPunto(elEvento) {
        //obtener caracter pulsado en todos los exploradores
        var evento = elEvento || window.event;
        var caracter = evento.charCode || evento.keyCode;
        //alert("El car�cter pulsado es: " + caracter);
        if (key == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                return false;
            }
        }
    }
    function valGrillaBlur() {
        var id = event.srcElement.id;
        var caja = document.getElementById(id);
        if (CajaEnBlanco(caja)) {
            caja.focus();
            alert('Debe ingresar un valor.');
        }
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();
            alert('Valor no v�lido.');
        }
    }
    function valSave() {
        var caja = document.getElementById('<%=txtNomre.ClientID %>');
        var cajaLong = document.getElementById('<%=txtLongitud.ClientID %>');
        var cajaCodSubLinea = document.getElementById('<%=txtCodigoSubLineaOld.ClientID %>');
        if (CajaEnBlanco(caja)) {
            caja.focus();
            alert('Debe ingresar un nombre para la Sub L�nea.');
            return false;
        }
        if (CajaEnBlanco(cajaLong)) {
            cajaLong.focus();
            alert('Debe ingresar una longitud.');
            return false;
        }
        if (CajaEnBlanco(cajaCodSubLinea)) {
            cajaCodSubLinea.focus();
            alert('Debe ingresar c�digo Sub L�nea.');
            return false;
        }
        return (confirm('Desea continuar con la operaci�n?'));
    }

    function valCodLinea(caja) {
        var cajaLongitud = document.getElementById('<%=txtLongitud.ClientID %>');
        if (cajaLongitud.value.length > 0) {
            if (parseInt(cajaLongitud.value) <= caja.value.length) {
                return false;
            }
            return true ;
        }        
        return false;
    }
    

    function valLongitudColor(caja) {
        var cajaLongitud = document.getElementById('<%=txtCodigoSubLineaOld.ClientID %>');
                      
        if (caja.value.length > 0) {
            cajaLongitud.style.backgroundColor = 'white';
        }
        else {
            cajaLongitud.style.backgroundColor = '#E2E2E2';
            cajaLongitud.value = '';
        }
        
        
        return true;
    }
    
    
    
    </script>

    <asp:UpdatePanel ID="UpdatePanel_Principal" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseout="this.src='../Imagenes/Nuevo_b.JPG';"
                            onmouseover="this.src='../Imagenes/Nuevo_A.JPG';" />
                        <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                            OnClientClick="return(valSave());" onmouseout="this.src='../Imagenes/Guardar_B.JPG';"
                            onmouseover="this.src='../Imagenes/Guardar_A.JPG';" />
                        <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                            OnClientClick="return(confirm('Desea retroceder?'));" onmouseout="this.src='../Imagenes/Arriba_B.JPG';"
                            onmouseover="this.src='../Imagenes/Arriba_A.JPG';" ToolTip="Atr�s" />
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        SUB L�NEA
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_SubLineaEdicion" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="text-align: right; width: 117px;">
                                                    <asp:Label ID="Label5" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                                </td>
                                                <td colspan="6">
                                                    <asp:DropDownList ID="cmbLinea" runat="server" DataTextField="Descripcion" DataValueField="Id">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 117px;">
                                                    <asp:Label ID="Label7" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                                                </td>
                                                <td colspan="6">
                                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtNomre" runat="server" Width="430px" MaxLength="50"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodigoSubLinea" CssClass="TextBoxReadOnly" Width="80px" runat="server"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 117px;">
                                                    <asp:Label ID="Label22" runat="server" CssClass="Label" Text="Longitud:"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtLongitud" runat="server" Width="90px" onKeyup="return(valLongitudColor(this));"
                                                        OnKeypress="return (onKeyPressEsNumero('event'));" MaxLength="1"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label21" runat="server" CssClass="Label" Text="C�d. SubLinea:"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtCodigoSubLineaOld" runat="server" MaxLength="30" CssClass="TextoUPPERCASE"
                                                        Width="200px" onKeyPress="return (valCodLinea(this));"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 117px">
                                                    <asp:Label ID="Label8" runat="server" CssClass="Label" Text="Cta. Debe Compra:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCtaDebeCompra" Width="100px" runat="server" MaxLength="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label11" runat="server" CssClass="Label" Text="Cta. Haber Compra:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCtaHaberCompra" Width="100px" runat="server" MaxLength="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label9" runat="server" CssClass="Label" Text="Cta. Debe Venta:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCtaDebeVenta" Width="100px" runat="server" MaxLength="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label10" runat="server" CssClass="Label" Text="Cta. Haber Venta:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCtaHaberVenta" Width="100px" runat="server" MaxLength="12"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 117px;">
                                                    <asp:Label ID="Label12" runat="server" CssClass="Label" Text="Costo de Compra:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCostoCompra" Width="100px" runat="server" MaxLength="12"></asp:TextBox>
                                                </td>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label13" runat="server" CssClass="Label" Text="Costo de Venta:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCostoVenta" Width="100px" runat="server" MaxLength="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 117px;">
                                                    <asp:Label ID="Label20" runat="server" CssClass="Label" Text="R�gimen:"></asp:Label>
                                                </td>
                                                <td colspan="7">
                                                    <asp:DropDownList ID="cmbRegimen" DataTextField="Nombre" DataValueField="Id" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:ImageButton ID="btnAgregarUM" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        OnClientClick="return(valRegimen());" onmouseout="this.src='../Imagenes/Agregar_B.JPG';"
                                                        onmouseover="this.src='../Imagenes/Agregar_A.JPG';" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 117px">
                                                </td>
                                                <td colspan="7" style="text-align: left">
                                                    <asp:GridView ID="DGV_Regimen" runat="server" AllowPaging="True" AutoGenerateColumns="False">
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <Columns>
                                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                            <asp:BoundField DataField="Id" HeaderText="Id" NullDisplayText="0" />
                                                            <asp:BoundField DataField="Nombre" HeaderText="R�gimen" NullDisplayText="-----" />
                                                            <asp:TemplateField HeaderText="Tasa (%)">
                                                                <ItemTemplate>
                                                                    <asp:TextBox onKeypress="return(validarNumeroPunto(event));" onblur="return(valGrillaBlur());"
                                                                        ID="txtTasa" Text='<%#DataBinder.Eval(Container.DataItem,"getPorcentUser")%>'
                                                                        runat="server"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label14" runat="server" Text="Afecto a Percepci�n:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rdbAfectoPercepcion" runat="server" RepeatDirection="Horizontal"
                                                        CssClass="Label">
                                                        <asp:ListItem Value="1" Selected="True">S�</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td style="width: 50px;">
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label15" runat="server" Text="Afecto a Detracci�n:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rdbAfectoDetraccion" runat="server" RepeatDirection="Horizontal"
                                                        CssClass="Label">
                                                        <asp:ListItem Value="1" Selected="True">S�</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; height: 12px;">
                                                    <asp:Label ID="Label16" runat="server" Text="Afecto I.G.V.:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rdbIGV" runat="server" RepeatDirection="Horizontal" CssClass="Label">
                                                        <asp:ListItem Value="1" Selected="True">S�</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                </td>
                                                <td style="text-align: right; height: 12px;">
                                                    <asp:Label ID="Label17" runat="server" Text="Estado:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td style="height: 12px">
                                                    <asp:RadioButtonList ID="rdbEstado" runat="server" RepeatDirection="Horizontal" CssClass="Label">
                                                        <asp:ListItem Value="1" Selected="true">Activo</asp:ListItem>
                                                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_busqueda" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label18" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbLinea_Busqueda" runat="server" DataTextField="Descripcion"
                                                        DataValueField="Id" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Estado:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rdbEstado_Busqueda" runat="server" CssClass="Label" RepeatDirection="Horizontal"
                                                        AutoPostBack="True">
                                                        <asp:ListItem Value="">Todos</asp:ListItem>
                                                        <asp:ListItem Selected="true" Value="1">Activo</asp:ListItem>
                                                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="DGV_SubLineaBusqueda" runat="server" AutoGenerateColumns="False"
                                            Width="742px" AllowPaging="True" PageSize="20">
                                            <RowStyle CssClass="GrillaRow" />
                                            <Columns>
                                                <asp:CommandField SelectText="Editar" ShowSelectButton="True" />
                                                <asp:BoundField DataField="CodigoSubLinea" HeaderText="C�digo" NullDisplayText="" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" NullDisplayText="-----" />
                                                <asp:BoundField DataField="NomLinea" HeaderText="L�nea" NullDisplayText="-----" />
                                                <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="-----" />
                                                <asp:BoundField DataField="Id" HeaderText="" NullDisplayText="0" />
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>    

    <script language="javascript" type="text/javascript">
    //////////////////////////////////////////
    var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
    /////////////////////////////////////////
    </script>

</asp:Content>

﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmPrincipal_Inicio.aspx.vb" Culture="Auto" UICulture="Auto"
    Inherits="APPWEB.FrmPrincipal_Inicio" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>.::Sistema Integral de Gestión Empresarial::. - SIGE</title>
    <link href="Estilos/stlGeneral.css" rel="stylesheet" type="text/css" />
    <link href="Estilos/Menu.css" rel="stylesheet" type="text/css" />
    <link href="Estilos/Controles.css" rel="stylesheet" type="text/css" />
    <link href="Estilos/Menu/css/menu.css" rel="stylesheet" />
    <link href="css/jquerysctipttop.css" rel="stylesheet" />
    <link href="css/animate.min.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/bootstrap-dropdownhover.css" rel="stylesheet" />
    <link href="Estilos/Menu/css/font-awesome.css" rel="stylesheet" />
    <link href="Estilos/bootstrap/Menu_drop.css" rel="stylesheet" />
    <style>
        .dropdown-inline {
            display: inline-block;
            position: relative;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-default" style="width: 100%">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="color: white">SANICENTER</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="navbar-nav navbar">
                    <li>
                        <div class="dropdown dropdown-inline">
                            <a href="#" style="color: white" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">Compras<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Mantenimiento</a></li>
                                <li class="dropdown">
                                    <a href="#">Emitir Documentos<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                        <li><a href="#">OrdenCompra1</a></li>
                                        <li><a href="#">CancelarTransitoOC</a></li>
                                        <li><a href="#">Aprobacion_oCompra</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Costeo</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown dropdown-inline">
                            <a href="#" style="color: white" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">Ventas<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Mantenimiento</a></li>
                                <li class="dropdown">
                                    <a href="#">Emitir Documentos<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                        <li><a href="#">OrdenCompra1</a></li>
                                        <li><a href="#">CancelarTransitoOC</a></li>
                                        <li><a href="#">Aprobacion_oCompra</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Costeo</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown dropdown-inline">
                            <a href="#" style="color: white" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">Caja<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Mantenimiento</a></li>
                                <li class="dropdown">
                                    <a href="#">Emitir Documentos<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                        <li><a href="#">OrdenCompra1</a></li>
                                        <li><a href="#">CancelarTransitoOC</a></li>
                                        <li><a href="#">Aprobacion_oCompra</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Costeo</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown dropdown-inline">
                            <a href="#" style="color: white" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">Almacen<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Mantenimiento</a></li>
                                <li class="dropdown">
                                    <a href="#">Emitir Documentos<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                        <li><a href="#">OrdenCompra1</a></li>
                                        <li><a href="#">CancelarTransitoOC</a></li>
                                        <li><a href="#">Aprobacion_oCompra</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Costeo</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown dropdown-inline">
                            <a href="#" style="color: white" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">Finanz y Contab.<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Mantenimiento</a></li>
                                <li class="dropdown">
                                    <a href="#">Emitir Documentos<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                        <li><a href="#">OrdenCompra1</a></li>
                                        <li><a href="#">CancelarTransitoOC</a></li>
                                        <li><a href="#">Aprobacion_oCompra</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Costeo</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown dropdown-inline">
                            <a href="#" style="color: white" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">Administracion<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Mantenimiento</a></li>
                                <li class="dropdown">
                                    <a href="#">Emitir Documentos<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                        <li><a href="#">OrdenCompra1</a></li>
                                        <li><a href="#">CancelarTransitoOC</a></li>
                                        <li><a href="#">Aprobacion_oCompra</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Costeo</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown dropdown-inline">
                            <a href="#" style="color: white" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">Inform. Gerenc.<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Mantenimiento</a></li>
                                <li class="dropdown">
                                    <a href="#">Emitir Documentos<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                        <li><a href="#">OrdenCompra1</a></li>
                                        <li><a href="#">CancelarTransitoOC</a></li>
                                        <li><a href="#">Aprobacion_oCompra</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Costeo</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown dropdown-inline">
                            <a href="#" style="color: white" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">Marketing<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Mantenimiento</a></li>
                                <li class="dropdown">
                                    <a href="#">Emitir Documentos<span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                        <li><a href="#">OrdenCompra1</a></li>
                                        <li><a href="#">CancelarTransitoOC</a></li>
                                        <li><a href="#">Aprobacion_oCompra</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Costeo</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <form id="form1" runat="server">
        <table style="background: White; width: 100%" cellpadding="1" cellspacing="1">
            <tr>
                <td width="50px">
                    <asp:Button ID="xbtnProducto" runat="server" Text="Ctrl+Shift+P"></asp:Button>
                </td>
                <td align="left" class="Texto">Usuario:
                </td>
                <td>
                    <asp:Label ID="lblUsuario" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                </td>
                <td align="left" class="Texto">Tienda:             
                </td>
                <td>
                    <asp:Label ID="Lb_Tienda" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                </td>
                <td class="Texto" align="left">Fecha Ingreso:
                </td>
                <td>
                    <asp:Label ID="reloj" runat="server" CssClass="LabelRojo" Text=""></asp:Label>
                </td>
                <td class="Texto">T.C. SUNAT (Of$) : 
                </td>
                <td>
                    <asp:Label ID="lblTC_Sunat" CssClass="LabelRojo" runat="server" Text="0"></asp:Label>
                </td>
                <td class="Texto">T.C. INTERNO (Of$) : 
                </td>
                <td>
                    <asp:Label ID="lblTC_Indusferr" CssClass="LabelRojo" runat="server" Text="0"></asp:Label>
                </td>
                <td align="right">
                    <asp:ImageButton ID="btnCerrarSesion" runat="server" ImageUrl="~/Imagenes/CerrarSesion_B.JPG"
                        ImageAlign="AbsMiddle" onmouseout="this.src='Imagenes/CerrarSesion_B.JPG';"
                        onmouseover="this.src='Imagenes/CerrarSesion_A.JPG';" />
                </td>
            </tr>
        </table>

        <asp:Label type="hidden" ID="idusuario" value="" runat="server" ForeColor="Transparent" />
        <%--        <iframe name="frame_Content" id="frame_Content" style="width: 100%; max-width: 100%; border-bottom: 0px; border-left: 0px; background-color: White"
            onload='resizeIframe(this);'></iframe>--%>
    </form>
    <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="JS/bootstrap_js/bootstrap-dropdownhover.js"></script>
    <script type="text/javascript">
        //document.oncontextmenu = function () { return false }
        //function resizeIframe(obj) {
        //    obj.style.width = 100 + '%';
        //    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        //}
    </script>

    <%--<script src="JS/bootstrap_js/jquery-3.3.1.js"></script>
        <script src="JS/bootstrap_js/jquery-3.3.1.min.js"></script>    --%>


    <%--    <script src="JS/JS_Fecha.js" type="text/javascript"></script>
    <script src="JS/JS_Impresion.js" type="text/javascript"></script>
    <script src="JS/Util.js" type="text/javascript"></script>--%>
    <script type="text/javascript">

        function startTime() {
            var today = new Date();
            var dia = today.getDate();
            var mes = today.getMonth() + 1;
            var anio = today.getFullYear();
            //Concateno 0 si fuese necesario
            if (dia.toString().length == 1) { dia = "0" + dia; }
            if (mes.toString().length == 1) { mes = "0" + mes; }

            h = today.getHours();
            m = today.getMinutes();
            s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('reloj').innerHTML = dia + "/" + mes + "/" + anio + " " + h + ":" + m + ":" + s; t = setTimeout('startTime()', 500);
        }
        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        //window.onload = function () {
        //    startTime();
        //}

        //******* Detectando Tecla ESC
        function detectar_tecla(elEvento) {
            <%--var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;            
            with (elEvento) {
                if ((caracter == 80 && ctrlKey && shiftKey)) {  //**  Ctrl + Shift + 1
                    document.getElementById('<%=xbtnProducto.ClientID %>').click();
                    return false;
                    //window.open('../Finanzas/FrmPage_Load.aspx?Tipo=3', null, 'resizable=yes,width=800,height=700,scrollbars=1', null);
                }
            }--%>
        }
        //document.onkeydown = detectar_tecla('event');


        $(document).ready(function () {
            //CargaMenu();

            //debugger;
            //$('.dropdown').hover(                
            //    function () {
            //            debugger;
            //            $(this).toggleClass('open');
            //        },
            //        function () {
            //            $(this).toggleClass('open');
            //        }
            //    );

            /* MAIN MENU */

            //$('#main-menu > li:has(ul.sub-menu)').addClass('parent');
            //$('ul.sub-menu > li:has(ul.sub-menu) > a').addClass('parent');

            //$('#menu-toggle').click(function () {
            //    $('#main-menu').slideToggle(300);
            //    return false;
            //});
        });

        function CargaMenu() {
            var usuario = document.getElementById("idusuario").textContent;
            var objData = {};
            objData["idusuario"] = usuario;
            $.ajax({
                type: "POST",
                url: "FrmPrincipal_Inicio.aspx/Menu",
                data: JSON.stringify(objData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var div = document.getElementById("Cabecera");
                    var resultado = response["d"];
                    for (var i = 1; i < resultado.length; i++) {
                        var nombrecombo = resultado[i];
                        var divcombo = document.createElement('div');
                        var a = document.createElement('a');
                        divcombo.className = "dropdown dropdown-inline";
                        div.appendChild(divcombo);
                        var span = document.createElement('span');

                        a.className = "dropdown-toggle";
                        a.href = "#";
                        span.className = "caret";
                        var attog = document.createAttribute("data-toggle");
                        var att = document.createAttribute("data-hover");
                        var attani = document.createAttribute("data-animations");
                        attog.value = "dropdown";
                        att.value = "dropdown";
                        attani.value = "zoomIn zoomIn zoomIn zoomIn";
                        a.setAttributeNode(attog);
                        a.setAttributeNode(att);
                        a.setAttributeNode(attani);
                        a.appendChild(document.createTextNode(nombrecombo.Nombre));
                        a.appendChild(span);
                        divcombo.appendChild(a);
                        var ulsubmenu = document.createElement('ul');
                        var attrole = document.createAttribute("role");
                        attrole.value = "menu";
                        ulsubmenu.className = "dropdown-menu";
                        ulsubmenu.setAttributeNode = attrole;
                        ulsubmenu.id = nombrecombo.Id;
                        divcombo.appendChild(ulsubmenu);
                        submenu(nombrecombo, usuario);
                    }
                }
            });
        }
        function submenu(obj1, obj2) {
            var id = obj1.Id;
            var objData = {};
            objData["Id"] = id;
            objData["idusuario"] = obj2;
            $.ajax({
                type: "POST",
                url: "FrmPrincipal_Inicio.aspx/SubMenu",
                data: JSON.stringify(objData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (respuesta) {
                    var ulsubmenuselect = document.getElementById("" + id + "");
                    var resultado = respuesta["d"];
                    for (var i = 0; i < resultado.length; i++) {
                        var nombrecombo = resultado[i];
                        var lisubmenu = document.createElement('li');
                        var asubmenu = document.createElement('a');
                        var spancaret = document.createElement('span');
                        spancaret.className = "caret";
                        asubmenu.href = "#";
                        lisubmenu.id = nombrecombo.Id;
                        ulsubmenuselect.appendChild(lisubmenu);
                        lisubmenu.appendChild(asubmenu);
                        asubmenu.appendChild(document.createTextNode(nombrecombo.Nombre));
                        if (nombrecombo.Formulario === '') {
                            lisubmenu.className = "dropdown";
                            asubmenu.appendChild(spancaret);
                            submenuDerecha(nombrecombo.Id, obj2);
                        }
                    }
                }
            });
        }
        function submenuDerecha(obj1, obj2) {
            debugger;
            var id = obj1;
            var objData = {};
            objData["Id"] = id;
            objData["idusuario"] = obj2;
            $.ajax({
                type: "POST",
                url: "FrmPrincipal_Inicio.aspx/SubMenuDerecha",
                data: JSON.stringify(objData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (respuesta) {
                    var resultado = respuesta["d"];
                    var lisubmenuderecha = document.getElementById("" + id + "");
                    var ulopcionesderecha = document.createElement('ul');
                    ulopcionesderecha.className = "dropdown-menu dropdownhover-right";
                    lisubmenuderecha.appendChild(ulopcionesderecha);
                    for (var i = 0; i < resultado.length; i++) {
                        var nombrecomboderecha = resultado[i];
                        var liopcionesderecha = document.createElement('li');
                        var atextoderecha = document.createElement('a');
                        ulopcionesderecha.appendChild(liopcionesderecha);
                        liopcionesderecha.appendChild(atextoderecha);
                        atextoderecha.href = "#";
                        atextoderecha.appendChild(document.createTextNode(nombrecomboderecha.Nombre));


                    }
                }
            });
        }
    </script>
</body>
</html>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmCambiarPassword.aspx.vb" Inherits="APPWEB.FrmCambiarPassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table width="100%">

<tr style="height:25px">
<td class="TituloCelda">Cambio de Contraseña</td>
</tr>
<tr>
<td align="center">
    <table>
        <tr>
            <td align="right" class="Texto">
                Contraseña actual:</td>
            <td >
                <asp:TextBox ID="txtClaveOld" TextMode="Password" MaxLength="25" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" class="Texto">
                Ingrese su nueva Contraseña:</td>
            <td>
            <asp:TextBox ID="txtClaveNew" TextMode="Password" MaxLength="25" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" class="Texto">
                Confirmar nueva Contraseña:</td>
            <td>
                <asp:TextBox ID="txtClaveNewConfirm" TextMode="Password" MaxLength="25" runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnGuardar" OnClientClick="return(   valOnClick_btnGuardar()  );" Width="70px" runat="server" Text="Guardar" ToolTip="Guardar" />
             </td>
        </tr>
    </table>
    </td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>

</table>
<script language="javascript" type="text/javascript">

    function valOnClick_btnGuardar() {

        var txtClaveOld = document.getElementById('<%=txtClaveOld.ClientID%>');
        if (txtClaveOld.value.length <= 0) {
            alert('Campo requerido [ Contraseña Actual ]');
            txtClaveOld.select();
            txtClaveOld.focus();
            return false;
        }
        var txtClaveNew = document.getElementById('<%=txtClaveNew.ClientID%>');
        if (txtClaveNew.value.length <= 0) {
            alert('Campo requerido [ Contraseña Actual ]');
            txtClaveNew.select();
            txtClaveNew.focus();
            return false;
        }
        var txtClaveNewConfirm = document.getElementById('<%=txtClaveNewConfirm.ClientID%>');
        if (txtClaveNewConfirm.value.length <= 0) {
            alert('Campo requerido [ Contraseña Actual ]');
            txtClaveNewConfirm.select();
            txtClaveNewConfirm.focus();
            return false;
        }
        if (txtClaveNewConfirm.value != txtClaveNew.value) {
            alert('Las claves no coinciden. Vuelva a confirmar su Nueva Contraseña.');
            txtClaveNewConfirm.select();
            txtClaveNewConfirm.focus();
            return false;
        }
    
        return confirm('Desea continuar con la Operación ?');
    }

</script>


</asp:Content>

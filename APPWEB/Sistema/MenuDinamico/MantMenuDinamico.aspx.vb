﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class MantMenuDinamico
    Inherits System.Web.UI.Page
    Dim Combo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegOpcion As New Negocio.opcion
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
        Dim i As Integer
        'Make the selected menu item reflect the correct imageurl
        For i = 0 To Menu1.Items.Count - 1
            If i = CInt(e.Item.Value) Then
                Menu1.Items(i).ImageUrl = "/imagenes/barramenu.JPG"
            Else
                Menu1.Items(i).ImageUrl = "/imagenes/barramenu.JPG"
            End If
        Next
    End Sub

    ''-------------------------
    Public Sub CarganodosTreeViewTable(ByVal CtrlTreeview As TreeView)
        Dim dtTreeNodo As New DataTable
        dtTreeNodo = objNegOpcion.CargaNodoTreeView
        Dim idmenu, idopcion As Integer
        For Each drTreeViewItem As Data.DataRow In dtTreeNodo.Rows

            idmenu = CInt(drTreeViewItem("IdMenu"))
            idopcion = CInt(drTreeViewItem("IdOpcion"))

            If drTreeViewItem("IdMenu").Equals(drTreeViewItem("IdOpcion")) Then
                Dim TvwTreeNodo As New TreeNode
                TvwTreeNodo.ShowCheckBox = True
                TvwTreeNodo.Value = drTreeViewItem("IdOpcion").ToString
                TvwTreeNodo.Text = drTreeViewItem("op_Nombre").ToString
                TvwTreeNodo.NavigateUrl = drTreeViewItem("op_Formulario").ToString
                CtrlTreeview.Nodes.Add(TvwTreeNodo)
                AddTreeViewItem(TvwTreeNodo, dtTreeNodo)

            End If
        Next
    End Sub
    Private Sub AddTreeViewItem(ByRef mnuMenuItem As TreeNode, ByVal dtTreeViewItems As Data.DataTable)
        Dim idopcion, idmenu, mnuitem As Integer

        For Each drTreeViewItem As Data.DataRow In dtTreeViewItems.Rows
            idopcion = CInt(drTreeViewItem("IdOpcion"))
            idmenu = CInt(drTreeViewItem("IdMenu"))
            mnuitem = CInt(mnuMenuItem.Value)

            If CInt(drTreeViewItem("IdMenu")).Equals(CInt(mnuMenuItem.Value)) AndAlso _
            Not CInt(drTreeViewItem("IdOpcion")).Equals(CInt(drTreeViewItem("IdMenu"))) Then
                Dim TvwNewTreeNode As New TreeNode
                TvwNewTreeNode.ShowCheckBox = True
                TvwNewTreeNode.Value = drTreeViewItem("IdOpcion").ToString
                TvwNewTreeNode.Text = drTreeViewItem("op_Nombre").ToString
                TvwNewTreeNode.NavigateUrl = drTreeViewItem("op_Formulario").ToString
                mnuMenuItem.ChildNodes.Add(TvwNewTreeNode)
                AddTreeViewItem(TvwNewTreeNode, dtTreeViewItems)
            End If
        Next
    End Sub
    ''-------------------------
    'carga treeviiew tb2
    Public Sub cargaTreeviewPerfiles(ByVal idperfil As Integer, ByVal ctrlTreeview As TreeView)
        Dim listaopciones As New List(Of Entidades.Opcion)
        listaopciones = objNegOpcion.SelectOpcionxPerfil(idperfil)
        CargaNodosTreeView(ctrlTreeview, listaopciones)
    End Sub
    'carga treeview tb3
    Public Sub CargaTreeViewUsuarioPerfiles(ByVal idperfil As Integer, ByVal idusuario As Integer, ByVal ctrlTreeview As TreeView)
        Dim listaopciones As New List(Of Entidades.Opcion)
        listaopciones = objNegOpcion.SelectOpcionxUsuarioxPerfil(idperfil, idusuario)
        CargaNodosTreeView(ctrlTreeview, listaopciones)
    End Sub
    Public Sub CargaNodosTreeView(ByVal ctrlTreeview As TreeView, ByVal listaOpcion As List(Of Entidades.Opcion))

        For Each opcion As Entidades.Opcion In listaOpcion
            Dim id, idmenu As Integer
            id = opcion.Id
            idmenu = opcion.IdMenu
            'si padre es igual al hijo entonces es menu padre.
            If (opcion.Id.Equals(opcion.IdMenu)) Then
                Dim TvwTreeNodo As New TreeNode
                TvwTreeNodo.ShowCheckBox = True

                If opcion.IsPermitido = "1" Then
                    TvwTreeNodo.Checked = True
                Else
                    TvwTreeNodo.Checked = False
                End If
                TvwTreeNodo.Value = opcion.Id.ToString
                TvwTreeNodo.Text = opcion.Nombre.ToString
                'TvwTreeNodo.NavigateUrl = opcion.Formulario.ToString
                TvwTreeNodo.ToolTip = opcion.Formulario.ToString
                TvwTreeNodo.Expand()
                TvwTreeNodo.SelectAction = TreeNodeSelectAction.None
                ctrlTreeview.Nodes.Add(TvwTreeNodo)
                'hacemos un llamado al metodo recursivo encargado de generar el árbol del menú.
                AddTreeViewItem(TvwTreeNodo, listaOpcion)
            End If
        Next
    End Sub
    Private Sub AddTreeViewItem(ByRef mnuMenuItem As TreeNode, ByVal listaOpciones As List(Of Entidades.Opcion))
        'recorremos cada elemento del datatable para poder determinar cuales son elementos hijos
        'del menuitem dado pasado como parametro ByRef.
        For Each opcion As Entidades.Opcion In listaOpciones
            Dim id, idmenu, mnu As Integer
            id = opcion.Id
            idmenu = opcion.IdMenu
            mnu = CInt(mnuMenuItem.Value)

            If opcion.IdMenu.Equals(CInt(mnuMenuItem.Value)) AndAlso Not opcion.Id.Equals(CInt(opcion.IdMenu)) Then
                Dim TvwNewTreeNode As New TreeNode
                TvwNewTreeNode.ShowCheckBox = True
                If opcion.IsPermitido = "1" Then
                    TvwNewTreeNode.Checked = True
                Else
                    TvwNewTreeNode.Checked = False
                End If
                TvwNewTreeNode.Value = opcion.Id.ToString
                TvwNewTreeNode.Text = opcion.Nombre.ToString
                'TvwNewTreeNode.NavigateUrl = opcion.Formulario.ToString
                TvwNewTreeNode.ToolTip = opcion.Formulario.ToString
                TvwNewTreeNode.SelectAction = TreeNodeSelectAction.None
                'Agregamos el Nuevo MenuItem al MenuItem que viene de un nivel superior.
                mnuMenuItem.ChildNodes.Add(TvwNewTreeNode)
                'llamada recursiva para ver si el nuevo menú ítem aun tiene elementos hijos.
                AddTreeViewItem(TvwNewTreeNode, listaOpciones)
            End If
        Next
    End Sub

    Public Function GetNodosFromTreeview(ByVal ctrlTreeview As TreeView) As List(Of Entidades.Opcion)
        Dim listaOpcion As New List(Of Entidades.Opcion)
        For Each TvwNodos As TreeNode In ctrlTreeview.Nodes
            Dim opcion As New Entidades.Opcion
            'solo tomo los nodos que estan chequeados
            If TvwNodos.Checked = True Then
                opcion.IsPermitido = "1"
            Else
                opcion.IsPermitido = "0"
            End If
            opcion.Id = CInt(TvwNodos.Value)
            opcion.Idperfil = CInt(Me.cboPerfil.SelectedValue)
            opcion.Nombre = TvwNodos.Text
            'opcion.Formulario = TvwNodos.NavigateUrl
            opcion.Formulario = TvwNodos.ToolTip
            listaOpcion.Add(opcion)
            getSubNodos(listaOpcion, TvwNodos)
        Next
        Return listaOpcion
    End Function
    Public Sub getSubNodos(ByVal listaopcion As List(Of Entidades.Opcion), ByVal TreeviewNodo As TreeNode)

        For Each Nodo As TreeNode In TreeviewNodo.ChildNodes
            Dim opcion As New Entidades.Opcion
            'solo tomo los nodos que estan chequeados
            If Nodo.Checked = True Then
                opcion.IsPermitido = "1"
            Else
                opcion.IsPermitido = "0"
            End If
            opcion.Id = CInt(Nodo.Value)
            opcion.Idperfil = CInt(Me.cboPerfil.SelectedValue)
            opcion.Nombre = Nodo.Text
            'opcion.Formulario = Nodo.NavigateUrl
            opcion.Formulario = Nodo.ToolTip
            listaopcion.Add(opcion)
            getSubNodos(listaopcion, Nodo)

        Next
    End Sub
    Protected Sub iBtnAgregar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnAgregar.Click
        'addMenusGrilla()
        If Me.TvwPerfiles.Nodes Is Nothing Then
            If TvwPerfiles.SelectedNode Is Nothing Then
                objScript.mostrarMsjAlerta(Me, " No ha Selecionado un Nodo")
            Else
                If CInt(ViewState("modo")) = 1 Then
                    'agrega al Teeview y guarda en la bd
                    AddNodo(Me.TreeViewPerfil.SelectedNode)
                    Me.CerrarCapaAsignaMenu()
                    'limpiaControlesMantMenu()
                    Me.TreeViewPerfil.Nodes.Clear()
                    objScript.mostrarMsjAlerta(Me, "Se Inserto con Exito en la Base Datos")
                    SetNodosMantMenu()

                Else
                    'agrega al Teeview y guarda en la bd
                    AgregarNodoModificado(Me.TreeViewPerfil.SelectedNode)
                    Me.CerrarCapaAsignaMenu()
                    'limpiaControlesMantMenu()
                    Me.TreeViewPerfil.Nodes.Clear()
                    objScript.mostrarMsjAlerta(Me, "Se Modifico con Exito en la Base Datos")
                    SetNodosMantMenu()

                End If
            End If
        Else
            If CInt(ViewState("modo")) = 1 Then
                AddNodo(Me.TreeViewPerfil.SelectedNode)
                Me.CerrarCapaAsignaMenu()
                'agrega al Teeview y guarda en la bd
                'limpiaControlesMantMenu()
                Me.TreeViewPerfil.Nodes.Clear()
                objScript.mostrarMsjAlerta(Me, "Se inserto en Base Datos")
                SetNodosMantMenu()

            Else
                AgregarNodoModificado(Me.TreeViewPerfil.SelectedNode)
                Me.CerrarCapaAsignaMenu()
                'agrega al Teeview y guarda en la bd
                'limpiaControlesMantMenu()
                Me.TreeViewPerfil.Nodes.Clear()
                objScript.mostrarMsjAlerta(Me, "Se Modifico con Exito en la Base Datos")
                SetNodosMantMenu()
            End If
        End If
    End Sub
    Public Function GetNodoGuardar(ByVal nodoInsertUpdate As TreeNode) As Entidades.Opcion
        Dim opcion As New Entidades.Opcion

        opcion.IdMenu = CInt(nodoInsertUpdate.ImageToolTip)
        opcion.Nombre = nodoInsertUpdate.Text
        opcion.Formulario = nodoInsertUpdate.ToolTip
        opcion.Id = CInt(nodoInsertUpdate.Value)
        opcion.Orden = CDec(Me.txtOrden_Opcion.Text)

        'opcion.Id = CInt(nodoInsertUpdate.Value)

        If nodoInsertUpdate.Checked = True Then
            opcion.Estado = "1"
        Else
            opcion.Estado = "0"
        End If
        Return opcion

    End Function

    Private Sub DGV_BuscarPersona_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_BuscarPersona.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboperPersona As DropDownList = CType(e.Row.Cells(5).FindControl("CboPerfilesPersona"), DropDownList)

                Dim perfil As New List(Of Entidades.Perfil)
                perfil = objNegOpcion.SelectPerfilxUsuario(CInt(e.Row.Cells(1).Text))

                For i As Integer = 0 To perfil.Count - 1
                    cboperPersona.Items.Insert(i, New ListItem(perfil(i).NomPerfil, CStr(perfil(i).IdPerfil)))
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub DGV_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarPersona.SelectedIndexChanged
        ' Session("idpersonaAll") = CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text.Trim)
        ViewState.Add("IdPersona", CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text.Trim))
        Dim persona As Integer = CInt(ViewState("IdPersona"))
        ViewState("nombreUsuario") = (DGV_BuscarPersona.SelectedRow.Cells(2).Text.Trim)
        ViewState("Login") = (DGV_BuscarPersona.SelectedRow.Cells(3).Text.Trim)

        Me.txtnombreUsuario.Text = HttpUtility.HtmlDecode(CStr(ViewState("nombreUsuario")))
        txtLogin.Text = HttpUtility.HtmlDecode(CStr(ViewState("Login")))
        Combo.LlenarCboPerfilUsuario(Me.cboPerfilesUsuarios, CInt(ViewState("IdPersona")), True)
        CierrarCapa()
        'Habilita controles para Asiganar perfil usuaio opcion
        HabilitaCtrlUsuario()
        Me.TvwUsuarios.Nodes.Clear()
    End Sub
    Private Sub LLenarCliente(ByVal cliente As Entidades.PersonaView)
        Dim NegPersonaTipoAgente As New Negocio.PersonaTipoAgente
        Dim TipoAgente As New Entidades.TipoAgente
        TipoAgente = NegPersonaTipoAgente.SelectIdAgenteTasaxIdPersona(cliente.IdPersona)
    End Sub
    Private Sub addPersona(ByVal IdPersona As Integer)
        Try
            Dim objCliente As Entidades.PersonaView = (New Negocio.Cliente).SelectxId(IdPersona)
            Me.LLenarCliente(objCliente)
            '********* Para 
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "addPersona_Venta();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "El Cliente no existe.")
        End Try
    End Sub
    Private Sub CierrarCapa()
        Try
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "CierraPersona();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "El Cliente no existe.")
        End Try
    End Sub

    Private Sub BuscarClientexDni()

        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarClientexRuc()

        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarClientexId(ByVal IdCliente As Integer)
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Cliente = obj.SelectxId(IdCliente)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            'MsgBox1.ShowMessage(ex.Message)
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarDatosPersonaGrilla(ByVal grilla As GridView, ByVal texto As String, ByVal opcion As Integer, ByVal tipoMov As Integer)
        Try
            '********** opcion 0: descripcion
            '********** opcion 1: ruc
            '********** opcion 2: dni
            Dim index As Integer = 0
            Select Case tipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) + 1
                Case 3 '***************** IR
                    index = (CInt(txtPageIndexGO_Persona.Text) - 1)
            End Select

            Dim listaPV As List(Of Entidades.PersonaView) = (New Negocio.PersonaView).SelectActivoxParam_PaginadoUsuario(texto, opcion, grilla.PageSize, index)

            If listaPV.Count <= 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                grilla.DataSource = listaPV
                grilla.DataBind()
                txtPageIndex_Persona.Text = CStr(index + 1)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos Persona.")
        End Try
    End Sub

    Private Sub btnBuscarPersona_Grilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersona_Grilla.Click
        ViewState.Add("TextoBuscarPersona", txtBuscarPersona_Grilla.Text)
        ViewState.Add("OpcionBuscarPersona", Me.cmbFiltro_BuscarPersona.SelectedValue)
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, CInt(Me.cmbFiltro_BuscarPersona.SelectedValue), 0)
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaPersona');", True)
        'txtPageIndex_Persona.Text = "1"
        'txtPageIndexGo_Persona.Text = "1"
    End Sub

    Private Sub DGV_BuscarPersona_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_BuscarPersona.PageIndexChanging
        'DGV_BuscarPersona.PageIndex = e.NewPageIndex
        'cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, CInt(Me.cmbFiltro_BuscarPersona.SelectedValue))
    End Sub
    Private Sub btnAnterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 1)
    End Sub
    Private Sub btnPosterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 2)
    End Sub
    Private Sub btnIr_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 3)
    End Sub

    Private Sub btnGuardarPerfiles_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardarPerfiles.Click
        If CInt(cboPerfil.SelectedValue) = 0 Then

        Else
            If objNegOpcion.InsertaPerfilOpcion(GetNodosFromTreeview(Me.TvwPerfiles), CInt(Me.cboPerfil.SelectedValue), Me.chb_AplicarAll.Checked) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso Finalizó Con Éxito.")
                Me.TvwPerfiles.Nodes.Clear()
                Me.cargaTreeviewPerfiles(CInt(Me.cboPerfil.SelectedValue), TvwPerfiles)
            End If
            'cargamos los datos ingresados

        End If
    End Sub
    Private Sub btnGuardarPerfilUsuario_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardarPerfilUsuario.Click

        If TvwUsuarios.Nodes.Count >= 1 Then
            If objNegOpcion.InsertaPerfilOpcionUsuario(GetNodosFromTreeview(Me.TvwUsuarios), CInt(Me.cboPerfilesUsuarios.SelectedValue), CInt(ViewState("IdPersona"))) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso Finalizó Con Éxito.")
                'cargamos los datos ingresados
                Me.TvwUsuarios.Nodes.Clear()
                Me.CargaTreeViewUsuarioPerfiles(CInt(Me.cboPerfilesUsuarios.SelectedValue), CInt(ViewState("IdPersona")), TvwUsuarios)
                'InicioFormulario()
                limpiatreeview()
            Else
                objScript.mostrarMsjAlerta(Me, "Errores en la Insercion.")
            End If
        Else
            objScript.mostrarMsjAlerta(Me, "No hay Elementos.")
        End If

    End Sub

    Private Sub Tab2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tab2.Load
        If Not IsPostBack Then
            Combo.LlenarCboPerfil(Me.cboPerfil, True)
        End If
    End Sub

    Public Sub PrimerNodos(ByVal treeview As TreeView)
        Dim trenodo As New TreeNode
        'si no haY NODOS agregamos el primero al treeview
        If Me.TreeViewPerfil.Nodes.Count < 1 Then
            trenodo.Text = "Sige"
            trenodo.Value = "1"
            trenodo.ShowCheckBox = True
            trenodo.Selected = True
            trenodo.SelectAction = TreeNodeSelectAction.Select
            'trenodo.ToggleExpandState()
            TreeViewPerfil.Nodes.Add(trenodo)
        End If
    End Sub
    Public Function siNodoExite(ByVal oldTrenodo As TreeNode, ByVal NewTrenodo As TreeNode) As Boolean
        Dim bool As Boolean = False
        Dim n, v As String
        n = NewTrenodo.Text
        v = oldTrenodo.Text
        For Each nodov As TreeNode In oldTrenodo.ChildNodes
            If nodov.Text.Trim.Equals(NewTrenodo.Text.Trim) Then
                bool = True
                'Exit Function
            End If
            siNodoExite(nodov, NewTrenodo)
        Next
        Return bool
    End Function
    Public Sub AddNodo(ByVal nodoSelectionado As TreeNode)
        Dim newtreenodo As New TreeNode
        If Me.TreeViewPerfil.Nodes.Count >= 1 Then
            If siNodoExite(nodoSelectionado, SetNewNodo(nodoSelectionado)) = False Then
                'nodoSelectionado.ChildNodes.Add(SetNewNodo(nodoSelectionado))
                'guarda en la bd el nodo nuevo
                objNegOpcion.InsertaOpcion(GetNodoGuardar(SetNewNodo(nodoSelectionado)))
            Else
                objScript.mostrarMsjAlerta(Me, "Un valor(es) ya existe en Nodo Actual")
                Me.CerrarCapaAsignaMenu()
            End If
        End If
    End Sub
    'asigna los valores al nodo nuevo 
    Public Function SetNewNodo(ByVal nodoSelecionado As TreeNode) As TreeNode

        Dim nodonew As New TreeNode
        nodonew.Text = Me.txtTexto.Text
        nodonew.ShowCheckBox = True
        nodonew.Checked = getEstadoMenu(Me.rbtEstado)
        nodonew.ToolTip = Me.txtUrl.Text
        'nodo ImageToolTip guarda el padre al cual pertenece idmenu
        'nodoSelecionado.Value es el nodo al cual se agregara el nodo
        nodonew.ImageToolTip = nodoSelecionado.Value
        'guardamos un idopcion "nuevo",esto p saber si esta o no en la BD
        If CInt(ViewState("modo")) = 1 Then
            'SI EL NODO ES NUEVO le asignamos un valor(por comodidad ,ya que idopcion no se inserta)
            nodonew.Value = "9999"
        End If
        nodonew.SelectAction = TreeNodeSelectAction.Select
        nodonew.SelectAction = TreeNodeSelectAction.Expand
        Return nodonew
    End Function
    Public Function getEstadoMenu(ByVal rbtestado As RadioButtonList) As Boolean
        Dim bool As Boolean
        If rbtestado.Items.Item(0).Selected = True Then
            bool = True
        Else
            bool = False
        End If
        Return bool
    End Function
    Public Sub DeleteNodo(ByVal NodoSelecionado As TreeNode)
        If Sitienehijos(NodoSelecionado) = True Then
            objScript.mostrarMsjAlerta(Me, " El Nodo Tiene Subnodos")
        Else
            NodoSelecionado.Parent.ChildNodes.Remove(NodoSelecionado)
        End If
    End Sub
    Public Function Sitienehijos(ByVal NodoSeleccionado As TreeNode) As Boolean
        Dim bool As Boolean
        If NodoSeleccionado.ChildNodes.Count > 0 Then
            bool = True
        Else
            bool = False
        End If
        Return bool
    End Function

    Private Sub Tab1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tab1.Load
        If Not IsPostBack Then
            SetNodosMantMenu()
            If Me.TreeViewPerfil.Nodes.Count < 1 Then
                PrimerNodos(Me.TreeViewPerfil)
            End If
        End If
    End Sub
    Private Sub btnEliminarNodo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarNodo.Click
        If TreeViewPerfil.SelectedNode Is Nothing Then
            objScript.mostrarMsjAlerta(Me, "No Seleccino un Nodo")
        Else
            Try
                Dim nodosel As TreeNode
                nodosel = Me.TreeViewPerfil.SelectedNode
                ' en la bd
                If objNegOpcion.Elimina(CInt(nodosel.Value), Me.chb_DeleteAll.Checked) = True Then
                    'en el arbol
                    DeleteNodo(nodosel)
                    objScript.mostrarMsjAlerta(Me, "Se Elimino el Nodo de la Base Datos")
                End If
            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message())
            End Try
        End If
    End Sub

    Private Sub btnNuevoNodo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevoNodo.Click
        If TreeViewPerfil.SelectedNode Is Nothing Then
            objScript.mostrarMsjAlerta(Me, "No Seleccino un Nodo")
        Else
            ViewState("modo") = 1
            limpiaControlesMantMenu()
            mostrarCapaasinaMenu()

            Dim nodoSelecionado As New TreeNode
            nodoSelecionado = TreeViewPerfil.SelectedNode

            Me.TxtNodoPadre.Text = nodoSelecionado.Text

        End If

        '  objScript.mostrarMsjAlerta(Me, CStr(Session("IdUsuario")))

    End Sub
    'llena el menu opciones
    Public Sub SetNodosMantMenu()
        Dim listaopcion As New List(Of Entidades.Opcion)
        listaopcion = objNegOpcion.SelectOpcionMantenimiento()
        CargaNodosTreeViewMant(Me.TreeViewPerfil, listaopcion)
    End Sub
    Public Function GetNodosFromTreeViewMantmenu() As List(Of Entidades.Opcion)
        Dim listamenu As New List(Of Entidades.Opcion)
        listamenu = GetNodosFromTreeViewMantmenu(Me.TreeViewPerfil)
        Return listamenu
    End Function
    ''carga el treview nodos
    Public Sub CargaNodosTreeViewMant(ByVal ctrlTreeview As TreeView, ByVal listaOpcion As List(Of Entidades.Opcion))

        For Each opcion As Entidades.Opcion In listaOpcion
            Dim id, idmenu As Integer
            id = opcion.Id
            idmenu = opcion.IdMenu
            'si padre es igual al hijo entonces es menu padre.
            If (opcion.Id.Equals(opcion.IdMenu)) Then

                Dim TvwTreeNodo As New TreeNode
                TvwTreeNodo.ShowCheckBox = True
                If opcion.Estado = "1" Then
                    TvwTreeNodo.Checked = True
                Else
                    TvwTreeNodo.Checked = False
                End If
                TvwTreeNodo.Value = opcion.Id.ToString
                TvwTreeNodo.Text = opcion.Nombre.ToString
                TvwTreeNodo.ImageToolTip = opcion.IdMenu.ToString
                TvwTreeNodo.ToolTip = opcion.Formulario
                TvwTreeNodo.Expand()

                ctrlTreeview.Nodes.Add(TvwTreeNodo)


                'hacemos un llamado al metodo recursivo encargado de generar el árbol del menú.
                AddTreeViewItemManT(TvwTreeNodo, listaOpcion)
            End If
        Next
    End Sub
    Private Sub AddTreeViewItemManT(ByRef mnuMenuItem As TreeNode, ByVal listaOpciones As List(Of Entidades.Opcion))
        'recorremos cada elemento del datatable para poder determinar cuales son elementos hijos
        'del menuitem dado pasado como parametro ByRef.
        For Each opcion As Entidades.Opcion In listaOpciones
            Dim id, idmenu, mnu As Integer
            id = opcion.Id
            idmenu = opcion.IdMenu
            mnu = CInt(mnuMenuItem.Value)

            If opcion.IdMenu.Equals(CInt(mnuMenuItem.Value)) AndAlso Not opcion.Id.Equals(CInt(opcion.IdMenu)) Then
                Dim TvwNewTreeNode As New TreeNode
                TvwNewTreeNode.ShowCheckBox = True
                If opcion.Estado = "1" Then
                    TvwNewTreeNode.Checked = True
                Else
                    TvwNewTreeNode.Checked = False
                End If

                TvwNewTreeNode.Value = opcion.Id.ToString
                TvwNewTreeNode.Text = opcion.Nombre.ToString
                TvwNewTreeNode.ToolTip = opcion.Formulario
                TvwNewTreeNode.ImageToolTip = opcion.IdMenu.ToString
                TvwNewTreeNode.Collapse()
                'Agregamos el Nuevo Nodo al MenuItem que viene de un nivel superior.
                mnuMenuItem.ChildNodes.Add(TvwNewTreeNode)

                'llamada recursiva para ver si el nuevo menú ítem aun tiene elementos hijos.
                AddTreeViewItemManT(TvwNewTreeNode, listaOpciones)

            End If
        Next

    End Sub

    Public Function GetNodosFromTreeviewMantMenu(ByVal ctrlTreeview As TreeView) As List(Of Entidades.Opcion)
        Dim listaOpcion As New List(Of Entidades.Opcion)
        For Each TvwNodos As TreeNode In ctrlTreeview.Nodes
            Dim opcion As New Entidades.Opcion
            'solo tomo los nodos que estan chequeados
            If TvwNodos.Checked = True Then
                opcion.Estado = "1"
            Else
                opcion.Estado = "0"
            End If
            opcion.Id = CInt(TvwNodos.Value)
            opcion.IdMenu = CInt(TvwNodos.ImageToolTip.ToString)
            opcion.Nombre = TvwNodos.Text
            opcion.Formulario = CStr(TvwNodos.ToolTip)
            listaOpcion.Add(opcion)
            getSubNodosMantMenu(listaOpcion, TvwNodos)
        Next
        Return listaOpcion
    End Function
    Public Sub getSubNodosMantMenu(ByVal listaopcion As List(Of Entidades.Opcion), ByVal TreeviewNodo As TreeNode)

        For Each Nodo As TreeNode In TreeviewNodo.ChildNodes
            Dim opcion As New Entidades.Opcion
            'solo tomo los nodos que estan chequeados
            If Nodo.Checked = True Then
                opcion.Estado = "1"
            Else
                opcion.Estado = "0"
            End If
            opcion.Id = CInt(Nodo.Value)
            opcion.IdMenu = CInt(Nodo.ImageToolTip.ToString)
            opcion.Nombre = Nodo.Text
            opcion.Formulario = Nodo.ToolTip
            listaopcion.Add(opcion)
            getSubNodosMantMenu(listaopcion, Nodo)
        Next
    End Sub

    Public Function GetNodoModificado(ByVal nodoSelecionado As TreeNode) As TreeNode
        nodoSelecionado.Value = nodoSelecionado.Value
        nodoSelecionado.ImageToolTip = nodoSelecionado.ImageToolTip
        nodoSelecionado.Text = Me.txtTexto.Text
        nodoSelecionado.ToolTip = Me.txtUrl.Text
        nodoSelecionado.Checked = getEstadoMenu(Me.rbtEstado)

        Return nodoSelecionado
    End Function

    Public Sub NodoModificado(ByVal nodoselecionado As TreeNode)
        'muestra el nodo padre
        Dim objOpcion As Entidades.Opcion = (New Negocio.opcion).SelectxIdOpcion(CInt(nodoselecionado.Value))
        If nodoselecionado.Value = "1" Then
            Me.txtTexto.Text = objOpcion.Nombre
            Me.txtUrl.Text = objOpcion.Formulario
            Me.txtOrden_Opcion.Enabled = False
        Else ' nuestra el nodo hijo
            Me.TxtNodoPadre.Text = objOpcion.ParentName
            Me.txtTexto.Text = objOpcion.Nombre
            Me.txtUrl.Text = objOpcion.Formulario
            Me.txtOrden_Opcion.Enabled = True
            Me.txtOrden_Opcion.Text = CStr(Math.Round(objOpcion.Orden, 2))
        End If
        'nodoSelecionado .ImageToolTip =Me.
    End Sub
    Public Sub AgregarNodoModificado(ByVal nodoSelecionado As TreeNode)
        'si el nodo a modificar es el padre
        If nodoSelecionado.Value = "1" Then
            TreeViewPerfil.Nodes.AddAt(0, GetNodoModificado(nodoSelecionado))
            'guarda los cambio del nodo padre
            objNegOpcion.ActualizaOpcion(GetNodoGuardar(GetNodoModificado(nodoSelecionado)))

        Else 'nodos hijos
            If siNodoExite(nodoSelecionado, GetNodoModificado(nodoSelecionado)) = True Then
                objScript.mostrarMsjAlerta(Me, "El Nodo ya Existe")
            Else
                Dim index As Integer = 0
                index = nodoSelecionado.Parent.ChildNodes.IndexOf(nodoSelecionado)

                'nodoSelecionado.Parent.ChildNodes.AddAt(index, GetNodoModificado(nodoSelecionado))

                'guarda los cambios de los nodos hijos en la bd
                objNegOpcion.ActualizaOpcion(GetNodoGuardar(GetNodoModificado(nodoSelecionado)))
            End If
        End If
    End Sub

    Private Sub BtnModoficar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnModoficar.Click
        Try
            If TreeViewPerfil.SelectedNode Is Nothing Then
                objScript.mostrarMsjAlerta(Me, "No Seleciono un Nodo")
            Else
                limpiaControlesMantMenu()
                mostrarCapaasinaMenu()
                ViewState("modo") = 2
                'limpiaControlesMantMenu()
                NodoModificado(TreeViewPerfil.SelectedNode)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        
    End Sub
    'llama al metodo de jscrip q cierrar la capa
    Private Sub CerrarCapaAsignaMenu()
        Try
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "CerrarCapaAsignaMenu();", True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'llama al metodo de jscrip q muestra la capa
    Private Sub mostrarCapaasinaMenu()
        Try
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaasinaMenu();", True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub limpiaControlesMantMenu()
        Me.TxtNodoPadre.Text = ""
        Me.txtTexto.Text = ""
        Me.txtUrl.Text = ""
        Me.txtOrden_Opcion.Text = ""
    End Sub

    Private Sub cboPerfil_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPerfil.SelectedIndexChanged
        If CInt(cboPerfil.SelectedValue) <> 0 Then
            Me.TvwPerfiles.Nodes.Clear()
            Me.cargaTreeviewPerfiles(CInt(Me.cboPerfil.SelectedValue), TvwPerfiles)
        Else
            '
            Me.TvwPerfiles.Nodes.Clear()
        End If
    End Sub

    Private Sub HabDesabControlesTb3()

        btnBuscarCliente.Visible = False
        btnGuardarPerfilUsuario.Visible = False
        btnCancelarPerfilUsuario.Visible = False
    End Sub

    'Private Sub btnAceptarPerfilUsuarioOpcion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarPerfilUsuarioOpcion.Click
    '    'Me.TvwUsuarios.Nodes.Clear()
    '    'Me.CargaTreeViewUsuarioPerfiles(CInt(Me.cboPerfilesUsuarios.SelectedValue), CInt(ViewState("IdPersona")), TvwUsuarios)

    'End Sub
    Private Sub limpiatreeview()
        TvwUsuarios.Nodes.Clear()
    End Sub

    Private Sub InicioFormulario()
        btnCancelarPerfilUsuario.Visible = False
        btnGuardarPerfilUsuario.Visible = False
        btnBuscarCliente.Visible = True
        lblnombreUsuario.Visible = False
        lblPerfilUsuario.Visible = False
        txtnombreUsuario.Visible = False
        cboPerfilesUsuarios.Visible = False
        txtLogin.Visible = False
        lblLogin.Visible = False
        TvwUsuarios.Nodes.Clear()
        'btnAceptarPerfilUsuarioOpcion.Visible = False
    End Sub
    Private Sub HabilitaCtrlUsuario()
        btnCancelarPerfilUsuario.Visible = True
        btnGuardarPerfilUsuario.Visible = True
        btnBuscarCliente.Visible = True
        lblnombreUsuario.Visible = True
        lblPerfilUsuario.Visible = True
        txtnombreUsuario.Visible = True
        txtLogin.Visible = True
        lblLogin.Visible = True
        cboPerfilesUsuarios.Visible = True
        'btnAceptarPerfilUsuarioOpcion.Visible = True
    End Sub

    Private Sub Tab3_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tab3.Load
        If Not IsPostBack Then
            InicioFormulario()
        End If
    End Sub
    Private Sub btnCancelarPerfilUsuario_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelarPerfilUsuario.Click
        InicioFormulario()
    End Sub

    Private Sub cboPerfilesUsuarios_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPerfilesUsuarios.SelectedIndexChanged

        If CInt(cboPerfilesUsuarios.SelectedValue) <> 0 Then
            Me.TvwUsuarios.Nodes.Clear()
            Me.CargaTreeViewUsuarioPerfiles(CInt(Me.cboPerfilesUsuarios.SelectedValue), CInt(ViewState("IdPersona")), TvwUsuarios)
        Else
            Me.TvwUsuarios.Nodes.Clear()
            '
        End If
        
    End Sub
End Class

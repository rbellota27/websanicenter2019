﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="MantMenuDinamico.aspx.vb" Inherits="APPWEB.MantMenuDinamico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
     <table  width ="100%"> 
 <tr > 

 <td >
     <asp:Menu ID="Menu1" Width="168px" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
         OnMenuItemClick="Menu1_MenuItemClick" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
         Font-Size="8pt" ForeColor="DarkRed" StaticSubMenuIndent="10px" Style="vertical-align: top"
         StaticDisplayLevels="2">
        
     <StaticSelectedStyle BackColor="#5D7B9D" />
     <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
     <DynamicHoverStyle BackColor="Peru" ForeColor="White" />
     <DynamicMenuStyle BackColor="#F7F6F3" />
     <DynamicSelectedStyle BackColor="SteelBlue" />
     <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
     <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
    <Items>
        <asp:MenuItem  
                      Text="Mantenimiento Menus " Value="0"></asp:MenuItem>
        <asp:MenuItem 
                      Text=" Asignar Menus a Perfiles" Value="1"></asp:MenuItem>
        <asp:MenuItem 
                      Text="Asignar Menus a Usuarios" Value="2"></asp:MenuItem>
    </Items>
</asp:Menu>
</td>
 </tr>
 <tr>
 <td> <asp:MultiView 
    ID="MultiView1"
    runat="server"
    ActiveViewIndex="0"  >
    
    <asp:View ID="Tab1" runat="server">
        <table width="100%" style="height: 400">
            <tr style="width: 40%">
                <td class="TituloCelda " style="width: 100%">
                    Asignar Menus
                </td>
            </tr>
        </table>
        <table width="100%" style="height: 400" cellpadding="0" cellspacing="0">

            <tr style =" width :100%" align="left">

            <td  style = " width :60%"> 
             <asp:UpdatePanel ID ="upMantmenu" runat ="server" UpdateMode ="Conditional" >
            <ContentTemplate >
              <table > 
              <tr>
              <td>
            <asp:TreeView  ID ="TreeViewPerfil"  runat ="server"  SelectedNodeStyle-BackColor="#FFCC99" CssClass ="LabelLeft" > </asp:TreeView>  
              </td>
              </tr>
              </table>
            </ContentTemplate>
            <Triggers >
            <asp:AsyncPostBackTrigger ControlID ="iBtnAgregar" EventName ="Click"  />
            </Triggers>
            </asp:UpdatePanel> 
            </td>
            
            <td style =" width :40%">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevoNodo" runat="server" Text="Nuevo" 
                                 Width="69px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="BtnModoficar" runat="server" Text="Modificar" 
                                Width="69px" />
                        </td>
                    </tr>
                                        
                    <tr>
                        <td>
                            <asp:Button ID="btnEliminarNodo" runat="server" Text="Eliminar" OnClientClick="return( confirm('Desea continuar con la Operación de Eliminación ?')   );" />  
                            <asp:CheckBox ID="chb_DeleteAll" runat="server" Text="Eliminar Registros Relacionados." CssClass="LabelRojo" /> 
                        </td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
    </asp:View>
    <asp:View ID="Tab2" runat="server">
        <table width="100%">
            <tr>
                <td class="TituloCelda " style="width: 100%">
                    Asignar Menus a los Perfiles
                </td>
            </tr>
              <tr>
                <td>
                    <asp:ImageButton ID="btnGuardarPerfiles" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                        ValidationGroup="valGuardar" OnClientClick="return(Guardarperfil());"  />
                    <asp:CheckBox ID="chb_AplicarAll" runat="server" Text="Aplicar la configuración a todos los Usuarios del perfil seleccionado." CssClass="LabelRojo" />
                </td>
            </tr>          
        </table>
        <table width="100%">
            <tr>
                <td align="left">
                    &nbsp;
                    <asp:Label ID="lblperfiles" Text="Perfiles:" runat="server" CssClass="Label"></asp:Label>
                    <asp:DropDownList ID="cboPerfil" runat="server" CssClass="Label" 
                        AutoPostBack="True" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table width="100%" style="height: 400" cellpadding="0" cellspacing="0">
            <tr>
                <td >
                    <asp:TreeView ID="TvwPerfiles" runat="server" ExpandDepth="3" CssClass ="LabelLeft"  >
                        <Nodes>
                            <asp:TreeNode SelectAction="Expand"></asp:TreeNode>
                        </Nodes>
                    </asp:TreeView>
                </td>
            </tr>
        </table>
        
    </asp:View>
    
    <asp:View ID="Tab3" runat="server">
        <table width="100%" style="height: 400" cellpadding="0 " cellspacing="0">
            
            <tr>
                <td class="TituloCelda ">
                    Asignar Menus a Usuarios por Perfil
                </td>
                
            </tr>
            <tr>
                <td>
                    <table width ="100%">
                       
                        <tr style ="width : "100%">
                            <td> 
                            <asp:UpdatePanel  ID ="upDatosUsuario" runat ="server" UpdateMode ="Conditional" >
                            <ContentTemplate >
                             <asp:Panel  ID ="panelUsuario" runat ="server" Width="752px" >
                            <table width ="100%" style="width: 117%">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                            OnClientClick="return(mostrarCapaPersona());" />
                                     </td>
                                     <td>
                                        <asp:ImageButton ID="btnGuardarPerfilUsuario" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                            onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                            ValidationGroup="valGuardar" OnClientClick="return(ValSavePerfilUsuario());"/>
                                          
                                         <asp:ImageButton ID="btnCancelarPerfilUsuario" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                            onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                                             OnClientClick ="return(CancelarperfUsuOp());" />   
                                     </td>

                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblnombreUsuario" Text="Nombre Usuario:" runat="server" Width="108px"
                                            CssClass="Label "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtnombreUsuario" runat="server" CssClass="TextBoxReadOnly " ReadOnly="True"
                                            Width="362px"></asp:TextBox>
                                    </td>

                                    
                                    <td>
                                        <asp:Label ID="lblPerfilUsuario" Text="Perfil Usuario:" runat="server" CssClass="Label "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboPerfilesUsuarios" runat="server" CssClass="Label" 
                                            AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                            
                               <tr >
                                    <td>
                                    <asp:Label  ID ="lblLogin" Text ="Login" runat ="server" CssClass ="Label" ></asp:Label>
                                    </td>
                                     <td>
                                    <asp:TextBox  ID ="txtLogin" runat ="server"      CssClass ="TextBox_ReadOnly "  ReadOnly ="true" ></asp:TextBox>
                                    </td>
                               </tr>
                            
                            
                            </table>
                            </asp:Panel>                            
                            </ContentTemplate>
                            <Triggers > 
                            <asp:AsyncPostBackTrigger ControlID ="DGV_BuscarPersona" EventName ="SelectedIndexChanged" />
                            <asp:PostBackTrigger ControlID ="cboPerfilesUsuarios" />
                            <asp:PostBackTrigger ControlID="btnGuardarPerfilUsuario" />
                            <asp:PostBackTrigger ControlID="btnCancelarPerfilUsuario" />
                            </Triggers>
                            </asp:UpdatePanel>

                            </td>

                            </tr>
                            
                    </table>
                </td>
                
            </tr>
            <tr valign="top">
                <td style="width: 100%">
                    <table>
                        <tr>
                            <td >
                                <asp:TreeView ID="TvwUsuarios" runat="server" CssClass ="LabelLeft"  >
                                </asp:TreeView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:View>
</asp:MultiView>
</td>
 </tr>
 </table>
 
<div  id="capaPersona" 
        style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; top:239px; left:27px; background-color:white; z-index:2; display :none; ">
        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server"   >
        <ContentTemplate>
        <table style="width: 100%;">                                    
                                <tr><td align="right">
                                       <asp:ImageButton ID="btnCerrar_Capa" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaPersona'));" />   
                                </td>
                                </tr>
                                    <tr class="Label">
                                        <td align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Filtro:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cmbFiltro_BuscarPersona" runat="server"  >
                                                        <asp:ListItem Selected="True" Value="0"> Descripción</asp:ListItem>
                                                        <asp:ListItem Value="1">R.U.C.</asp:ListItem>
                                                        <asp:ListItem Value="2">D.N.I.</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        Texto:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" Width="250px"></asp:TextBox>
                                                        <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" 
                                                            CausesValidation="false" ImageUrl="~/Imagenes/Buscar_b.JPG"                                                             
                                                            onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                                PageSize="20" Width="100%">
                                                <Columns>
                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                                    <asp:BoundField DataField="getNombreParaMostrar" HeaderText="Descripción" NullDisplayText="---" />
                                                    <asp:BoundField DataField="Login" HeaderText="Login" NullDisplayText="---" />
                                                    
                                                    <asp:BoundField DataField="Direccion" HeaderText="Dirección" NullDisplayText="---" />

                                            <asp:TemplateField HeaderText="Perfiles">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="CboPerfilesPersona" runat="server"                                               
                                                TabIndex="211"
                                                Width ="180px">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                                
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr> 
                                    <tr>
                                    
                                    <td>
                                    
                                        <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                                        <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                                        <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionPersona('2'));" />
                                        <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                    
                                    </td>
                                    </tr>                                     
                 
                 </table>      
        </ContentTemplate>
        </asp:UpdatePanel>
        </div>
<div id="CapaAsigaNodos" style="border: 3px solid blue; padding: 8px; width: 900px; height: auto; position: absolute; top: 239px; left: 27px; background-color: white; z-index: 2; display: none ;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(cerrarAsignarMenus());" />
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCeldaLeft">
                            Agregar Sub Nodos
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width ="100%">
                                <tr>
                                    <td class ="Label">
                                        Nodo Padre:
                                    </td>
                                    <td style ="width :100%" >
                                        <asp:TextBox ID="TxtNodoPadre" runat="server" Width="80%"  ReadOnly ="true" > </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class ="Label" >
                                        Nombre:
                                    </td>
                                    <td style ="width :80%">
                                        <asp:TextBox ID="txtTexto" runat="server" Width="80%"> </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class ="Label" >
                                        Url:
                                    </td>
                                    <td style ="width :100%">
                                        <asp:TextBox ID="txtUrl" runat="server" Width="80%"> </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Label">
                                        Posición:</td>
                                    <td style="width :100%">
                                        <asp:TextBox ID="txtOrden_Opcion" runat="server"  onKeypress="  return(   validarNumeroPuntoPositivo('event')    ); "  ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="rbtEstado" runat="server" RepeatDirection="Horizontal" CssClass ="Label">
                                            <asp:ListItem Value="1" Text="Activo" Selected="True" ></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Inactivo"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td style="width: 100%" class="Label">
                                    </td>
                                </tr>
                                                                
                                <tr>
                                    <td>
                                    </td>
                                    <td >
                                        <asp:ImageButton ID="iBtnAgregar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                            onmouseout="this.src='/Imagenes/Guardar_B.JPG';" onmouseover="this.src='/Imagenes/Guardar_A.JPG';" 
                                           OnClientClick ="return(validarSaveMenus());"  />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnNuevoNodo" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="BtnModoficar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
<script language="javascript" type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
    Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
        if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
            offsetX = Math.floor(offsetX);
            offsetY = Math.floor(offsetY);
        }
        this._origOnFormActiveElement(element, offsetX, offsetY);
    };
    function CancelarperfUsuOp() {
        return (confirm('Desea Cancelar  la Operacion?'));
    }
     function ValSavePerfilUsuario() {
        return (confirm('Desea Continuar con la Operacion?'));
    }
    function Guardarperfil() {
        var cboPerfil = document.getElementById('<%=cboPerfil.ClientID%>');
        if (isNaN(parseInt(cboPerfil.value)) || parseInt(cboPerfil.value) <= 0) {
            alert('Debe seleccionar un Perfil.');
            return false;
        }
    
        return (confirm('Desea continuar con la Operacion?'));
    }

    function cerrarAsignarMenus() {
        offCapa('CapaAsigaNodos');
        return false;

    }

    function limpiarFrmTab3() {
        document.getElementById('<%=txtnombreUsuario.ClientID%>').value = '';
        return false;
    }

    function validarSaveMenus() {

        var caja = document.getElementById('<%=txtTexto.ClientID%>');
        if (caja.value.length == 0) {
            alert('Debe Ingresar un Nombre Para el Menu.');
            caja.select();
            caja.focus();
            return false;
        }
        var txtOrden_Opcion = document.getElementById('<%=txtOrden_Opcion.ClientID%>');
        if (txtOrden_Opcion.value.length == 0 || isNaN(parseInt(txtOrden_Opcion.value))) {
            txtOrden_Opcion.value = '0';
        }
        return confirm('Desea continuar con la Operación ? ');
    }

    function valBlur(event) {
        var id = event.srcElement.id;
        var caja = document.getElementById(id);
        if (CajaEnBlanco(caja)) {
            caja.value = 0;
            //alert('Debe ingresar un valor.');
        }
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();
            alert('Valor no válido.');
        }
    }

    function valCajaEscalar() {
        validarDecimal();
    }
    function validarNumeroPunto(event) {
        var key = event.keyCode;
        if (key == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                return false;
            }
        }
    }

    function validarKeyPressEq() {
        if (esEnter(event) == true) {
            return false;
        }
        return true;
    }
    function validarCajaNumero() {
        if (onKeyPressValidarEnter_EsNumero() == false) {
            alert('Caracter no válido');
            return false;
        }
        return true;
    }
    //para levantar las capas
    function onCapaCliente(s) {
        LimpiarControlesCliente();
        onCapa(s);
        document.getElementById('<%= txtBuscarPersona_Grilla.ClientID%>').focus();
        return false;
    }
    function CierraPersona() {
        offCapa('capaPersona');
        return false;
    }

    void function LimpiarControlesCliente() {
        return false;
    }

    function validarNumeroPunto(event) {
        var key = event.keyCode;
        if (key == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                return false;
            }
        }
    }
    //Buscar SubLinea
    function ValidarEnteroSinEnter() {
        if (onKeyPressEsNumero('event') == false) {
            alert('Caracter no válido. Solo se permiten números Enteros');
            return false;
        }
        return true;
    }
    //End Buscar SubLinea
    //Buscar Cliente

    function BuscarCliente() {

        return true;
    }
    function ValidarEnter() {
        return (!esEnter(event));
    }
    function ValidaEnter() {
        if (esEnter(event) == true) {
            alert('Tecla No permitida');
            caja.focus();
            return false;
        }
        return true;
    }
    //End Buscar Cliente
    function mostrarCapaPersona() {
        //******************* validamos que exista detalle

        onCapa('capaPersona');

        document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
        document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
        //} else {
        //    alert('Debe ingresar primero el detalle del documento.');
        //}CapaAsigaNodos
        return false;
    }


    function addPersona_Venta() {
        offCapa('CapaAsigaNodos');

        return true;
    }
    function aceptarFoco(caja) {
        caja.select();
        caja.focus();
        return true;
    }
    function valNavegacionPersona(tipoMov) {
        var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
        if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
            alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
            return false;
        }
        switch (tipoMov) {
            case '0':   //************ anterior
                if (index <= 1) {
                    alert('No existen páginas con índice menor a uno.');
                    return false;
                }
                break;
            case '1':
                break;
            case '2': //************ ir
                index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                if (isNaN(index) || index == null || index.length == 0) {
                    alert('Ingrese una Página de navegación.');
                    document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                    document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                    return false;
                }
                if (index < 1) {
                    alert('No existen páginas con índice menor a uno.');
                    document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                    document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                    return false;
                }
                break;
        }
        return true;
    }


    function MostrarAsigaNodos() {
        //******************* validamos que exista detalle
        onCapa('CapaAsigaNodos');
        document.getElementById('<%=txtTexto.ClientID%>').focus();
        document.getElementById('<%=txtUrl.ClientID%>').select();
        //} else {
        //    alert('Debe ingresar primero el detalle del documento.');
        //}
        return true;
    }
    function mostrarCapaasinaMenu() {
        onCapa('CapaAsigaNodos');
        document.getElementById('<%=txtTexto.ClientID%>').select();
        document.getElementById('<%=txtTexto.ClientID%>').focus();
    }

    function CerrarCapaAsignaMenu() {
        offCapa('CapaAsigaNodos');
        return true;
    }
</script>

</asp:Content>

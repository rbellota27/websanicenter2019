﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmRptCajaDiarioxFechas
    Inherits System.Web.UI.Page


    Private objscript As New ScriptManagerClass
    Private objCombo As New Combo
    Private objNegUsu As New Negocio.UsuarioView

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            inicializarFrm()
            hddFechaActual.Value = ""

            If Request.QueryString("Opcion") IsNot Nothing Then
                Panel_Fechas.Visible = False
                hddFechaActual.Value = txtFechaInicio.Text
            End If

        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            objCombo.LlenarCboPropietario(Me.cmbEmpresa, True)
            'objCombo.llenarCboTiendaxIdEmpresa(Me.cmbTienda, CInt(Me.cmbEmpresa.SelectedValue), True)

            If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 4) > 0 Then
                objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, CInt(Session("IdUsuario")), True)
            Else
                objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, CInt(Session("IdUsuario")), False)
            End If

            objCombo.LlenarCboCajaxIdTienda(Me.cmbCaja, CInt(Me.cmbTienda.SelectedValue), True)
            objCombo.LlenarCboMedioPagoCredito(Me.cmbMedioPago, True, True)

            objCombo.LlenarCboTipoDocumentosCaja(Me.cboTipoDocumento, True)

            Dim objFechaActual As New Negocio.FechaActual
            txtFechaInicio.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")

            Me.cmbTienda.Enabled = True
            Me.cmbCaja.Enabled = True
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Private Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        'Try
        '    objCombo.llenarCboTiendaxIdEmpresa(Me.cmbTienda, CInt(Me.cmbEmpresa.SelectedValue), True)
        '    objCombo.LlenarCboCajaxIdTienda(Me.cmbCaja, CInt(Me.cmbTienda.SelectedValue), True)
        'Catch ex As Exception
        '    objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        'End Try
    End Sub

    Protected Sub cmbTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTienda.SelectedIndexChanged
        Try
            objCombo.LlenarCboCajaxIdTienda(Me.cmbCaja, CInt(Me.cmbTienda.SelectedValue), True)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
End Class
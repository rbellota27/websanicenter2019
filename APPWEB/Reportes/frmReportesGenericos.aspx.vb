﻿Imports Negocio
Imports AjaxControlToolkit
Imports System.IO

Public Class frmReportesGenericos
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected cadenaFiltros As String = ""
    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.Button1)
        If Not IsPostBack Then
            Dim objetoReportes As New bl_Reportes
            Dim IdUsuario As Integer
            IdUsuario = CInt(Session("IdUsuario"))
            Me.gvlistaReportes.DataSource = objetoReportes.rptGenerico(IdUSuario)
            Me.gvlistaReportes.DataBind()
        End If
    End Sub

    'Protected Sub onclick_verFiltros(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim imgButton As ImageButton = DirectCast(sender, ImageButton)
    '    Dim index As Integer = DirectCast(imgButton.NamingContainer, GridViewRow).RowIndex
    '    Dim idReporte As Integer = CInt(Me.gvlistaReportes.Rows(index).Cells(0).Text)
    '    Dim dt As New DataTable
    '    dt = (New bl_Reportes).rptFiltros(idReporte)
    '    dt.DefaultView.Sort = "orden"
    '    Dim html As New StringBuilder(String.Empty)
    '    html.Append("<table>")

    '    For i As Integer = 0 To dt.Rows.Count - 1
    '        Dim TipoObjeto As String = dt.Rows(i).Item("nombreFiltro").ToString
    '        Dim orden As Integer = CInt(dt.Rows(i).Item("orden"))
    '        Select Case TipoObjeto.ToUpper()
    '            Case "TEXTBOXFECHA"
    '                html.Append("<tr>")
    '                html.Append("<td>")
    '                html.Append("<asp:Label runat=""server"" ID=""lbTituloFiltro" + i.ToString() + """ CssClass=""Texto"" >" + dt.Rows(i)("nombreTitulo").ToString() + "</asp:Label>")
    '                html.Append("</td>")
    '                'label textBox Fecha
    '                html.Append("<td>")
    '                html.Append("<input ID=""txtObjeto" + i.ToString() + """ type =""text""></input>")
    '                'html.Append("<asp:TextBox runat=""server"" ID=""txtObjeto" + i.ToString() + """ ></asp:TextBox>")
    '                'html.Append("<cc1:CalendarExtender runat=""server"" ID=""calendar" + i.ToString() + """ Format=""dd/MM/yyyy"" TargetControlID=""txtObjeto" + i.ToString() + """ ></cc1:CalendarExtender>")
    '                html.Append("</td>")
    '                html.Append("</tr>")
    '            Case "DROPDOWNLIST"
    '                html.Append("<tr>")
    '                html.Append("<td>")
    '                html.Append("<asp:Label runat=""server"" ID=""lbTituloFiltro" + i.ToString() + """ CssClass=""Texto"" >" + dt.Rows(i)("nombreTitulo").ToString() + "</asp:Label>")
    '                html.Append("</td>")
    '                Dim datasource As String = dt.Rows(i).Item("dataSource").ToString
    '                Dim dtSource As New DataTable()
    '                dtSource = (New Negocio.bl_Reportes).ejecutaScripts(datasource)

    '                html.Append("<td>")
    '                html.Append("<select ID=""txtObjeto" + i.ToString() + """>")
    '                For j As Integer = 0 To dtSource.Rows.Count - 1
    '                    html.Append("<option value="" " + dtSource.Rows(j)("id").ToString() + " "" >" + dtSource.Rows(j)("nombre").ToString() + "</option>")
    '                Next
    '                'html.Append("<asp:TextBox runat=""server"" ID=""txtObjeto" + i.ToString() + """ ></asp:TextBox>")
    '                html.Append("</td>")
    '                html.Append("</tr>")
    '            Case "TEXTBOX"
    '                html.Append("<tr>")
    '                html.Append("<td>")
    '                html.Append("<asp:Label runat=""server"" ID=""lbTituloFiltro" + i.ToString() + """ CssClass=""Texto"" >" + dt.Rows(i)("nombreTitulo").ToString() + "</asp:Label>")
    '                html.Append("</td>")
    '                'label textBox Fecha
    '                html.Append("<td>")
    '                html.Append("<input ID=""txtObjeto" + i.ToString() + """ type =""text""></input>")
    '                'html.Append("<asp:TextBox runat=""server"" ID=""txtObjeto" + i.ToString() + """ ></asp:TextBox>")
    '                'html.Append("<cc1:CalendarExtender runat=""server"" ID=""calendar" + i.ToString() + """ Format=""dd/MM/yyyy"" TargetControlID=""txtObjeto" + i.ToString() + """ ></cc1:CalendarExtender>")
    '                html.Append("</td>")
    '                html.Append("</tr>")
    '            Case "CHECKBOXLIST"
    '                html.Append("<tr>")
    '                html.Append("<td>")
    '                html.Append("<asp:Label runat=""server"" ID=""lbTituloFiltro" + i.ToString() + """ CssClass=""Texto"" >" + dt.Rows(i)("nombreTitulo").ToString() + "</asp:Label>")
    '                html.Append("</td>")
    '                Dim datasource As String = dt.Rows(i).Item("dataSource").ToString
    '                Dim dtSource As New DataTable()
    '                dtSource = (New Negocio.bl_Reportes).ejecutaScripts(datasource)

    '                html.Append("<td>")
    '                For j As Integer = 0 To dtSource.Rows.Count - 1
    '                    html.Append("<input type=""checkbox"" /> " + dtSource.Rows(j)("nombre").ToString() + " <br />")
    '                Next
    '                'html.Append("<asp:TextBox runat=""server"" ID=""txtObjeto" + i.ToString() + """ ></asp:TextBox>")
    '                html.Append("</td>")
    '                html.Append("</tr>")
    '        End Select
    '    Next
    '    html.Append("<tr>")
    '    html.Append("<td>")
    '    html.Append("<input id=""btnBuscarReporte"" type=""submit"" value=""Ver Reporte"" onclick=""return(mostrarMensaje());""></input>")
    '    html.Append("<asp:Button ID=""Button1"" runat=""server"" Text=""Button"" onclick=""mostrarMensaje"" />")
    '    html.Append("</td>")
    '    html.Append("</tr>")
    '    html.Append("</table>")
    '    divFiltros.InnerHtml = html.ToString()
    '    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onclick", "mostrarCapa()", True)
    'End Sub
    Public Shared _idReporte As Integer

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim placeholder As PlaceHolder = DirectCast(buscaControl(Page, "phControles"), PlaceHolder)
        Dim placeholder As PlaceHolder = DirectCast(Page.FindControl("phControles"), PlaceHolder)

        Dim btn As Button = DirectCast(sender, Button)
        Dim obj As New Negocio.bl_Reportes
        Dim dt As New DataTable
        cadenaFiltros = filtrosString.Value
        cadenaFiltros = cadenaFiltros.Remove(cadenaFiltros.Length - 1, 1)
        dt = obj.ejecutaProceduresReportes(_idReporte, Me.cadenaFiltros)
        'Call exportarDatatableTOExcel(dt)
        Call DTToExcel(dt)
    End Sub

    Private Sub exportarDatatableTOExcel(ByVal dt As DataTable)
        Dim attachment As String = "attachment; filename=reporte.xls"
        Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/vnd.ms-excel"
        Dim tab As String = ""
        For Each dc As DataColumn In dt.Columns
            Response.Write(tab + dc.ColumnName)
            tab = "\t"
        Next
        Response.Write("\n")
        Dim i As Integer

        For Each row As DataRow In dt.Rows
            tab = ""

            For i = 0 To dt.Columns.Count - 1
                Response.Write(tab + row(i).ToString())
                tab = "\t"
            Next
            Response.Write("\n")
        Next
        Response.Close()
        Response.End()
    End Sub

    Public Sub DTToExcel(ByVal dt As DataTable)
        ' dosya isimleri ileride aynı anda birden fazla kullanıcı aynı dosya üzerinde işlem yapmak ister düşüncesiyle guid yapıldı. 
        Dim FileName As String = Guid.NewGuid().ToString()

        Dim f As New FileInfo(Server.MapPath("Downloads") + String.Format("\{0}.xlsx", FileName))
        If f.Exists Then
            f.Delete()
        End If
        ' delete the file if it already exist.
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.ClearHeaders()
        response.ClearContent()
        response.Charset = Encoding.UTF8.WebName
        response.AddHeader("content-disposition", (Convert.ToString("attachment; filename=") & FileName) + ".xls")
        response.AddHeader("Content-Type", "application/Excel")
        response.ContentType = "application/vnd.xlsx"
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default
        'response.AddHeader("Content-Length", file.Length.ToString());

        Using sw As New StringWriter()
            Using htw As New HtmlTextWriter(sw)
                Dim dg As New DataGrid()
                If dt.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "alert('No se hallaron datos');", True)
                Else
                    dg.DataSource = dt
                    dg.DataBind()
                    dg.RenderControl(htw)
                    response.Write(sw.ToString())
                    dg.Dispose()
                    dt.Dispose()
                    response.[End]()
                End If
            End Using
        End Using
    End Sub

    Protected Sub verReporte(ByVal sender As Object, ByVal e As EventArgs)
        Dim linkButton As New LinkButton
        linkButton = DirectCast(sender, LinkButton)
        Dim indice As Integer = 0
        indice = DirectCast(linkButton.NamingContainer, GridViewRow).RowIndex
        _idReporte = CInt(Me.gvlistaReportes.Rows(indice).Cells(0).Text)
        Dim IdUsuario As Integer
        IdUsuario = CInt(Session("IdUsuario"))

        ' Dim imgButton As ImageButton = DirectCast(sender, ImageButton)
        'Dim index As Integer = DirectCast(imgButton.NamingContainer, GridViewRow).RowIndex
        'Dim idReporte As Integer = CInt(Me.gvlistaReportes.Rows(index).Cells(0).Text)
        Dim dt As New DataTable
        dt = (New bl_Reportes).rptFiltros(_idReporte, IdUsuario)
        'dt.DefaultView.Sort = "orden asc"
        Dim html As New StringBuilder(String.Empty)
        html.Append("<table style=""width:100%"">")

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim TipoObjeto As String = dt.Rows(i).Item("nombreFiltro").ToString
            Dim orden As Integer = CInt(dt.Rows(i).Item("orden"))
            Select Case TipoObjeto.ToUpper()
                Case "TEXTBOXFECHA"
                    html.Append("<tr>")
                    html.Append("<td>")
                    html.Append("<asp:Label runat=""server"" ID=""lbTituloFiltro" + i.ToString() + """ CssClass=""Texto"" >" + dt.Rows(i)("nombreTitulo").ToString() + "</asp:Label>")
                    html.Append("</td>")
                    'label textBox Fecha
                    html.Append("<td>")
                    html.Append("<input ID=""txtObjeto" + i.ToString() + """ name=""filtro"" type =""date""></input>")
                    'html.Append("<asp:TextBox runat=""server"" ID=""txtObjeto" + i.ToString() + """ ></asp:TextBox>")
                    html.Append("</td>")
                    html.Append("</tr>")
                Case "DROPDOWNLIST"
                    html.Append("<tr>")
                    html.Append("<td>")
                    html.Append("<asp:Label runat=""server"" ID=""lbTituloFiltro" + i.ToString() + """ CssClass=""Texto"" >" + dt.Rows(i)("nombreTitulo").ToString() + "</asp:Label>")
                    html.Append("</td>")
                    Dim datasource As String = dt.Rows(i).Item("dataSource").ToString
                    Dim dtSource As New DataTable()
                    dtSource = (New Negocio.bl_Reportes).ejecutaScripts(datasource)

                    html.Append("<td>")
                    html.Append("<select name=""filtro"" ID=""txtObjeto" + i.ToString() + """>")
                    For j As Integer = 0 To dtSource.Rows.Count - 1
                        html.Append("<option value="" " + dtSource.Rows(j)(0).ToString() + " "" >" + dtSource.Rows(j)(1).ToString() + "</option>")
                    Next
                    'html.Append("<asp:TextBox runat=""server"" ID=""txtObjeto" + i.ToString() + """ ></asp:TextBox>")
                    html.Append("</td>")
                    html.Append("</tr>")
                Case "TEXTBOX"
                    html.Append("<tr>")
                    html.Append("<td>")
                    html.Append("<asp:Label runat=""server"" ID=""lbTituloFiltro" + i.ToString() + """ CssClass=""Texto"" >" + dt.Rows(i)("nombreTitulo").ToString() + "</asp:Label>")
                    html.Append("</td>")
                    'label textBox Fecha
                    html.Append("<td>")
                    html.Append("<input ID=""txtObjeto" + i.ToString() + """ name=""filtro"" type =""text""></input>")
                    'html.Append("<asp:TextBox runat=""server"" ID=""txtObjeto" + i.ToString() + """ ></asp:TextBox>")
                    'html.Append("<cc1:CalendarExtender runat=""server"" ID=""calendar" + i.ToString() + """ Format=""dd/MM/yyyy"" TargetControlID=""txtObjeto" + i.ToString() + """ ></cc1:CalendarExtender>")
                    html.Append("</td>")
                    html.Append("</tr>")
                Case "CHECKBOXLIST"
                    html.Append("<tr>")
                    html.Append("<td>")
                    html.Append("<asp:Label runat=""server"" ID=""lbTituloFiltro" + i.ToString() + """ CssClass=""Texto"" >" + dt.Rows(i)("nombreTitulo").ToString() + "</asp:Label>")
                    html.Append("</td>")
                    Dim datasource As String = dt.Rows(i).Item("dataSource").ToString
                    Dim dtSource As New DataTable()
                    dtSource = (New Negocio.bl_Reportes).ejecutaScripts(datasource)

                    html.Append("<td>")
                    For j As Integer = 0 To dtSource.Rows.Count - 1
                        html.Append("<input name=""filtro"" type=""checkbox"" value=""" + dtSource.Rows(j)(0).ToString() + """ /> " + dtSource.Rows(j)(1).ToString() + " <br />")
                    Next
                    'html.Append("<asp:TextBox runat=""server"" ID=""txtObjeto" + i.ToString() + """ ></asp:TextBox>")
                    html.Append("</td>")
                    html.Append("</tr>")
            End Select
        Next
        html.Append("</table>")
        divFiltros.InnerHtml = html.ToString()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaFiltros');", True)

    End Sub
End Class
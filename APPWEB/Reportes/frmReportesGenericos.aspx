﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmReportesGenericos.aspx.vb" Inherits="APPWEB.frmReportesGenericos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="width:100%">
    <div style="width:100%">
        <div class="TituloCelda" style="width:100%">
        <span>REPORTEADOR SIGE</span>
        </div>
        <div style="width:100%">
            <asp:GridView ID="gvlistaReportes" runat="server" AutoGenerateColumns="false" Width="100%">
            <HeaderStyle CssClass="GrillaHeader" />
            <RowStyle CssClass="GrillaRow" />
            <Columns>
                <asp:BoundField DataField="idtblReporte" HeaderText="ID" />
                <asp:BoundField DataField="area" HeaderText="ÁREA" />
                <asp:BoundField DataField="nombreReporte" HeaderText="NOMBRE" />
                <asp:BoundField DataField="descripcionReporte" HeaderText="DESCRIPCIÓN" />
                <asp:BoundField DataField="version" HeaderText="VERSIÓN" />
                <asp:TemplateField ItemStyle-HorizontalAlign="Center">                
                    <ItemTemplate>
                        <asp:ImageButton ID="imgVerReporte" runat="server" ImageUrl="~/Imagenes/eyeEditar.png" Enabled ="false" 
                        CommandName="VER_REPROTE" CommandArgument="<%# Container.DataItemIndex %>" />
                        <asp:LinkButton ID="lbVerReporte" runat="server" Text="VerReporte" OnClick ="verReporte"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:GridView>
        </div>
    </div>
</div>
<!--Capa Filtros-->
<div id="capaFiltros" style="width:500px;background-color:White;top:100px;left:300px;border:1px solid blue;position:absolute;display:none;z-index:15" >
    <div style="width:100%" class="SubTituloCelda" >
        <span>LISTA DE FILTROS </span>
        <img style="text-align:right" src="../Imagenes/Cerrar.gif" alt="" onclick="offCapa('capaFiltros');" />
    </div>
    <div runat="server" style="width:100%" id="divFiltros">
    </div>
    <asp:PlaceHolder ID="phControles" runat="server"></asp:PlaceHolder>
    <asp:Button ID="Button1" runat="server" Text="Exportar Reporte >>>" OnClientClick="mostrarMensaje()" />
    
    <input type="hidden" id="filtrosString" runat="server"/>
</div>
<script type="text/javascript" language="javascript">

    function mostrarMensaje() {
        var final;
        var div = document.getElementById('<%= divFiltros.ClientID %>');
        var table = div.getElementsByTagName("table");

        var td = table[0].getElementsByTagName("td");

        var combo;
        var indexCombo;
        var concatenaFiltro="";

        for (var i = 1; i < td.length; i++) {
            if (td[i].children[0].type == "text") {
                concatenaFiltro += "'" + td[i].children[0].value + "'" + ",";
            }

            if (td[i].children[0].type == "select-one") {
                combo = td[i].children[0];
                indexCombo = combo.selectedIndex;
                concatenaFiltro += "'" + combo.options[indexCombo].value + "'" + ",";
            }

            if (td[i].children[0].type == "checkbox") {
                var contadorINPUT = td[i].getElementsByTagName("input").length;
                var valorINPUT = "";
                for (var j = 0; j < contadorINPUT; j++) {
                    if (td[i].getElementsByTagName("input")[j].checked) {
                        valorINPUT += td[i].getElementsByTagName("input")[j].value + ",";
                    }
                }
                valorINPUT = valorINPUT.substring(valorINPUT.length - 1, 1);
                concatenaFiltro += "'" + valorINPUT + "'";
            }
            i++;
        }

        document.getElementById('<%= filtrosString.ClientID %>').value = concatenaFiltro
    }
    function mostrarCapa() {
        var div = document.getElementById('capaFiltros');
        div.style.display = 'block';
        return false;
    }
</script>
</asp:Content>

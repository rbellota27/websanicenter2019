﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmRptInvInicial
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            cargarDatosCboEmpresa(cmbEmpresa)
            cargarDatosCboAlmacen(cmbAlmacen)
            Dim objCbo As New Combo

            With objCbo


                '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
                .llenarCboTipoExistencia(cboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)

            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub cargarDatosCboEmpresa(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Propietario
        Dim lista As List(Of Entidades.Propietario) = obj.SelectCbo
        Dim objPropietario As New Entidades.Propietario
        objPropietario.Id = 0
        objPropietario.NombreComercial = "-----"
        lista.Insert(0, objPropietario)
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Private Sub cargarDatosCboAlmacen(ByVal cbo As DropDownList)
        Dim objAlmacen As New Negocio.Almacen
        Dim lista As List(Of Entidades.Almacen) = objAlmacen.SelectCboxIdEmpresa(CInt(cmbEmpresa.SelectedValue))
        Dim obj As New Entidades.Almacen(0, "-----")
        lista.Insert(0, obj)
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Private Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        cargarDatosCboAlmacen(cmbAlmacen)
    End Sub

    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
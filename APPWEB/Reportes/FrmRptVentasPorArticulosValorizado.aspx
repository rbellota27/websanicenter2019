<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmRptVentasPorArticulosValorizado.aspx.vb" Inherits="APPWEB.FrmRptVentasPorArticulosValorizado" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td colspan="10" align="center " class="TituloCelda">
                Ventas de Articulos Valorizado
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="Texto">
                                                Empresa:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto">
                                                Tienda:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbTienda" runat="server" AutoPostBack="true" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="Texto">
                                                Tipo Existencia:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="True" Enabled="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto">
                                                L&iacute;nea:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbLinea1" runat="server" AutoPostBack="True" Enabled="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto">
                                                Sub-L&iacute;nea:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbSubLinea1" runat="server" AutoPostBack="True" Style="height: 22px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="Label">
                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Value="0" Selected="True">Resumido</asp:ListItem>
                                                    <asp:ListItem Value="1">Detallado</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnBProducto" runat="server" Visible="false" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                                    OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                                                    onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                    onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            DE:
                        </td>
                        <td class="Label">
                            A�o:
                            <asp:DropDownList ID="idYearI" runat="Server">
                            </asp:DropDownList>
                            Semana:
                            <asp:DropDownList ID="idSemanaI" runat="Server">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            HASTA:
                        </td>
                        <td class="Label">
                            A�o:
                            <asp:DropDownList ID="idYearF" runat="Server">
                            </asp:DropDownList>
                            Semana:
                            <asp:DropDownList ID="idSemanaF" runat="Server">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            <asp:CheckBox ID="chbFiltrarSemana" runat="server" Text="Filtrar Por Semana" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Fechas" runat="server">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Label">
                                            Fecha Inicio:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                TargetControlID="txtFechaInicio">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="Label">
                                            Fecha Fin:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                TargetControlID="txtFechaFin">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="Desproducto" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="Prod_Codigo" HeaderText="C�digo" HeaderStyle-Height="25px"
                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                            <asp:BoundField DataField="UnidadMedida" HeaderText="UM Principal" HeaderStyle-Height="25px"
                                                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdfIdProductodt" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="hddidproducto" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="DGV_AddProd" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="btnLimpiarDetalleDocumento" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="RadioButtonList1" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                            onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../Imagenes/Aceptar_A.JPG';"
                            OnClientClick="return(MostrarReportesAll());" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                <br />
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="udpbProducto" runat="server">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                            DataTextField="Descripcion" DataValueField="Id">
                                        </asp:DropDownList>
                                        <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                        <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                            DataValueField="Id" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                        onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );"></asp:TextBox>
                                                </td>
                                                <td class="Texto">
                                                    C�d.:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                        onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );"
                                                        TabIndex="205"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                        TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                        OnClientClick="return(valAddProductos());" TabIndex="208" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                        onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                        OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                                SuppressPostBack="true">
                            </cc1:CollapsiblePanelExtender>
                            <asp:Image ID="Image21_11" runat="server" Height="16px" />
                            <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                            <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                                <table width="100">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Atributo:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                Width="650px">
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                        HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="GrillaRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="Codigo" HeaderText="C�digo" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                    <asp:BoundField DataField="UnidadMedida" HeaderText="UMP" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdfIdproducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                                Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                            <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                                Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                            <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                                CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                            <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                                Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                            <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">

        function valKeyPressCodigoSL() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function CierraCapaProd() {
            offCapa('capaBuscarProducto_AddProd');
            return false;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }

            //            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
            //                var rowElem = grilla.rows[i];
            //                var txtCantidad = rowElem.cells[0].children[0];
            //                if (parseFloat(txtCantidad.value) > 0) {
            //                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
            //                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
            //                    //                        txtCantidad.select();
            //                    //                        txtCantidad.focus();
            //                    //     return false;
            //                    //  } else if (parseFloat(txtCantidad.value) > 0) {
            //                    cont = cont + 1;
            //                    //  }
            //                }
            //            }

            //            if (cont == 0) {
            //                alert('No se seleccionaron productos.');
            //                return false;
            //            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

                if (grillaDetalle.rows.length > 1) {

                    //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

                    if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd() {
            if (event.keyCode == 13) {
                document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        //***********************************

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }


        function getCampoxValorCombo(combo, value) {
            var campo = '';
            if (combo != null) {
                for (var i = 0; i < combo.length; i++) {
                    if (combo[i].value == value) {
                        campo = combo[i].innerHTML;
                        return campo;
                    }

                }
            }
            return campo;
        }

        function DesLineaSublinearptdetallado() {

            if (rdbList.cells[1].children[0].status == true) {
                document.getElementById('<%= cmbLinea1.ClientID%>').disabled = true;
                document.getElementById('<%= cmbSubLinea1.ClientID%>').disabled = true;
            }
        }

        function DesbilitaFecha() {

            document.getElementById('<%= txtFechaInicio.ClientID%>').disable = true;
            document.getElementById('<%= txtFechaFin.ClientID%>').disable = true;
        }




        function mostrarControles() {

        }
        function MostrarReportesAll() {
            var rdbList = document.getElementById('<%=RadioButtonList1.ClientID %>');
            var radio = rdbList.getElementsByTagName("INPUT");
            if (radio[0].checked == true) {
                mostrarReporteResumido();

            } else if (radio[1].checked == true) {

                mostrarReporteDetallado();
            } else {
                alert('Selecione un tipo de reporte.');
            }

            return false;

        }
        function mostrarReporteResumido() {

            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;

            var IdTienda = document.getElementById('<%= cmbTienda.ClientID%>').value;

            var Idlinea = document.getElementById('<%= cmbLinea1.ClientID%>').value;
            if (document.getElementById('<%= cmbLinea1.ClientID%>') == null) {
                Idlinea = 0
            }
            var IdSublinea = document.getElementById('<%= cmbSubLinea1.ClientID%>').value;
            if (document.getElementById('<%= cmbSubLinea1.ClientID%>') == null) {
                IdSublinea = 0
            }

            var yeari = document.getElementById('<%= idYearI.ClientID%>').value;
            if (document.getElementById('<%= idYearI.ClientID%>') == null) {
                yeari = 0
            }
            var semanai = document.getElementById('<%= idSemanaI.ClientID%>').value;
            if (document.getElementById('<%= idSemanaI.ClientID%>') == null) {
                semanai = 0
            }


            var yearf = document.getElementById('<%= idYearF.ClientID%>').value;
            if (document.getElementById('<%= idYearF.ClientID%>') == null) {
                yearf = 0
            }
            var semanaf = document.getElementById('<%= idSemanaF.ClientID%>').value;
            if (document.getElementById('<%= idSemanaF.ClientID%>') == null) {
                semanaf = 0
            }


            var check = document.getElementById('<%= chbFiltrarSemana.ClientID%>')

            var filtrarsemana = 0;
            if (check.status == true) {
                var filtrarsemana = 1;
            }
            else {
                var filtrarsemana = 0;
            }

            var IdProducto = 0;
            var fechaInicio;
            var fechaFin;

            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }
            frame1.location.href = 'visor.aspx?iReporte=17&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&Idlinea=' + Idlinea + '&IdSublinea=' + IdSublinea + '&IdProducto=' + IdProducto + '&yeari=' + yeari + '&semanai=' + semanai + '&yearf=' + yearf + '&semanaf=' + semanaf + '&filtrarsemana=' + filtrarsemana;
            return false;
        }

        function mostrarReporteDetallado() {
            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            if (document.getElementById('<%= cmbEmpresa.ClientID%>' == null)) {
                IdEmpresa = 0
            }
            var IdTienda = document.getElementById('<%= cmbTienda.ClientID%>').value;
            if (document.getElementById('<%= cmbTienda.ClientID%>') == null) {
                IdTienda = 0
            }
            var Idlinea = document.getElementById('<%= cmblinea1.ClientID%>').value;
            if (document.getElementById('<%= cmbSubLinea1.ClientID%>') == null) {
                IdSublinea = 0
            }
            var IdSublinea = document.getElementById('<%= cmbSubLinea1.ClientID%>').value;
            if (document.getElementById('<%= cmbSubLinea1.ClientID%>') == null) {
                IdSublinea = 0
            }

            idproducto = document.getElementById('<%= hddidproducto.ClientID%>').value;
            if (document.getElementById('<%= hddidproducto.ClientID%>') == null) {
                idproducto = 0;
            }

            var yeari = document.getElementById('<%= idYearI.ClientID%>').value;
            if (document.getElementById('<%= idYearI.ClientID%>') == null) {
                yeari = 0
            }
            var semanai = document.getElementById('<%= idSemanaI.ClientID%>').value;
            if (document.getElementById('<%= idSemanaI.ClientID%>') == null) {
                semanai = 0
            }


            var yearf = document.getElementById('<%= idYearF.ClientID%>').value;
            if (document.getElementById('<%= idYearF.ClientID%>') == null) {
                yearf = 0
            }
            var semanaf = document.getElementById('<%= idSemanaF.ClientID%>').value;
            if (document.getElementById('<%= idSemanaF.ClientID%>') == null) {
                semanaf = 0
            }


            var check = document.getElementById('<%= chbFiltrarSemana.ClientID%>')

            var filtrarsemana = 0;
            if (check.status == true) {
                var filtrarsemana = 1;
            }
            else {
                var filtrarsemana = 0;
            }

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('El tipo Reporte es Detallado,debe seleccionar un Producto .');
                return false;
            }

            var fechaInicio;
            var fechaFin;
            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }

            IdTipoExistencia = parseInt(document.getElementById('<%=cboTipoExistencia.ClientID %>').value);
            if (isNaN(IdTipoExistencia)) { IdTipoExistencia = 0; }
                        
            frame1.location.href = 'visor.aspx?iReporte=16&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&Idempresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&IdTipoExistencia=' + IdTipoExistencia + '&Idlinea=' + Idlinea + '&IdSublinea=' + IdSublinea + '&idproducto=' + idproducto + '&yeari=' + yeari + '&semanai=' + semanai + '&yearf=' + yearf + '&semanaf=' + semanaf + '&filtrarsemana=' + filtrarsemana;
            return false;
        }
    </script>

</asp:Content>

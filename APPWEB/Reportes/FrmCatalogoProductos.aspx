﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"     MasterPageFile="~/Principal.Master"    CodeBehind="FrmCatalogoProductos.aspx.vb" Inherits="APPWEB.FrmCatalogoProductos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="Label" align="right">
                                    Línea:</td>
                                <td>
                                    <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label" align="right">
                                    Sub Línea:</td>
                                <td>
                                    <asp:DropDownList ID="cmbSubLinea" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label" align="right">
                                    U. Medida:</td>
                                <td>
                                    <asp:DropDownList ID="cmbUMedida" runat="server">
                                    <asp:ListItem Value="0">Todos</asp:ListItem>
                                    <asp:ListItem Value="1">U. M. Principal</asp:ListItem>
                                    <asp:ListItem Value="2">U. M. Retazo</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:ImageButton ID="btnMostrarReporte" runat="server" 
                                        ImageUrl="~/Imagenes/Aceptar_B.JPG" OnClientClick="return(mostrarReporte());" 
                                        onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                                        onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
     <script language="javascript" type="text/javascript">
        function mostrarReporte() {            
            var IdLinea = document.getElementById('<%= cmbLinea.ClientID%>').value;
            var IdSubLinea = document.getElementById('<%= cmbSubLinea.ClientID%>').value;
            var UMedida = document.getElementById('<%= cmbUMedida.ClientID%>').value;
            frame1.location.href = 'visor.aspx?iReporte=12&IdLinea=' + IdLinea+ '&IdSubLinea=' + IdSubLinea+'&UMedida='+UMedida ;
            return false;
        }
    </script>
</asp:Content>

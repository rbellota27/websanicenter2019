<%@ Page Title="" Language="vb" AutoEventWireup="false"     MasterPageFile="~/Principal.Master"       CodeBehind="FrmRptDetalleGuiasRemision1.aspx.vb" Inherits="APPWEB.FrmRptDetalleGuiasRemision1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <%--<asp:UpdatePanel ID ="upprincipal" runat="server"  >
<ContentTemplate >--%>
<table style="width: 100%">
        <tr>
            <td style="width: 664px">
                <table width ="100%">
                    <tr> <td align ="center" colspan ="8" class ="TituloCelda"> Detalle de Guias de Remision </td></tr>
                    <tr>
                    <td style="height: 68px" >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                            <table> 
                                <tr>
                                    <td style="height: 117px" >
                                            <table style="height: 78px; width: 867px;" > 
                                            <tr >
                                            <td class="Label" style="height: 103px; text-align: left; width: 178px">
                                                Empresa:<asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="true" >
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                TipoOperacion:<asp:DropDownList ID="cboTipoOperacion" runat="server">
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                            </td>
                                            <td class="Label" style="height: 103px">
                                                Almacen Origen:<asp:DropDownList ID="cmbIdAlmacenOrigen" runat="server"  Enabled="true" >
                                                </asp:DropDownList>
                                           
                                             <td class="Label" style="height: 103px" >
                                                <asp:Label ID="Label1" runat="server" Text="Almacen Destino:"></asp:Label>
                                                <asp:DropDownList ID="cmbidAlmacenDestino" runat="server"  
                                                 Enabled="true">

                                                </asp:DropDownList>
                                              </td> 
                                              <td class ="Label " style="height: 103px">
                                              <asp:CheckBox ID= "chbMercaTransito" runat ="server"  Text = "Mercaderia en Transito" />
                                              
                                              </td>
                                              
                                              </td> 
                                            </tr>
                                            </table> 
                                    </td>
                                </tr>
                                 <%--taable--%>
                                                          
                            </table>    
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    
                    </td>
                    </tr>
                    <tr>
                       <td >
                           <asp:Panel ID="Panel_Fechas" runat="server">
                           
                           <table>
                                    <tr>
                                     <td>
                                     <table >
                                            <tr >
                                                    <td class="Label">
                                                    Fecha Inicio:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                                         Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                   <td class="Label">
                                                    Fecha Fin:</td>
                                                    <td>
                                                    <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" 
                                                      Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                                    </cc1:CalendarExtender>
                                                    </td>
                                     </tr>
                                     
                                     </table>
                                     </td>

                                      
                                    </tr>
                                   

                           </table>
                           
                           </asp:Panel>
                       </td>
                    </tr>

                    <tr>
                        <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                        <asp:ImageButton ID="btnMostrarReporte" runat="server" 
                        ImageUrl="~/Imagenes/Aceptar_B.JPG" onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';"                                                
                        onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                        OnClientClick="return(MostrarReporte());"
                         />
                               
                             </ContentTemplate>
                        </asp:UpdatePanel>
                    
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td style="width: 664px">
            <iframe name="frame1" id="frame1" scrolling="yes" 
                    style="height: 1130px; width: 131%; margin-right: 0px;"></iframe>
            </td>
        </tr>
        <tr>
            <td style="width: 664px">
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="6" />
                <br />
            </td>
        </tr>
</table>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>
 <script language="javascript" type="text/javascript">

      function MostrarReporte() {
          var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
          var idAlmacenOrigen = document.getElementById('<%=cmbIdAlmacenOrigen.ClientID%>').value;
          var idAlmacenDestino = document.getElementById('<%=cmbidAlmacenDestino.ClientID%>').value;
          var IdTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>').value;
         
          if (idAlmacenOrigen == idAlmacenDestino) {
              alert("El Almacen de Destino debe ser Distinto Del Almacen de Origen ")
          }

          var check = document.getElementById('<%= chbMercaTransito.ClientID%>')

          var merTransito = 0;
          if (check.status == true) {
               merTransito = 1;
          }
          else {
               merTransito = 0;
          }
          
          if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
              fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
              fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
          } else {
              fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
              fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
          }
          frame1.location.href = 'visorRG.aspx?iReporte=3&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + idempresa + '&IdAlmacenOrigen=' + idAlmacenOrigen + '&idAlmacenDestino=' + idAlmacenDestino + '&mercaTransito=' + merTransito + '&IdTipoOperacion=' + IdTipoOperacion ;
          return false;

      }
   
    
  </script>



</asp:Content>

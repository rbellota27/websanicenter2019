﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmConsultaCxCResumenes

    '''<summary>
    '''CollapsiblePanelExtender5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender5 As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''Image5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image5 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''PanelMoneda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelMoneda As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlEmpresa As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboTienda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboTienda As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ImageButton1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImageButton1 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''gvwCxCxMoneda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvwCxCxMoneda As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Label3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtMontoSoles control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMontoSoles As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtMontoDolares control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMontoDolares As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lbl_Mensaje control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_Mensaje As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cmd_Imprimir_Cliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmd_Imprimir_Cliente As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''ddlidEmpresaCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlidEmpresaCliente As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlIdTiendaCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlIdTiendaCliente As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnAceptaCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAceptaCliente As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''cmd_BuscaCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmd_BuscaCliente As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''GvwCxCxClientes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GvwCxCxClientes As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''lblMensajeCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMensajeCliente As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ImprimirDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImprimirDoc As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''gvwCxCxDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvwCxCxDocumento As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''lblMensajeDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMensajeDocumento As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Label38 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label38 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cmd_Digrafic control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmd_Digrafic As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''hddIdpersona control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hddIdpersona As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''btnCerrar_Capa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCerrar_Capa As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''cmbFiltro_BuscarPersona control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmbFiltro_BuscarPersona As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Panel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtBuscarPersona_Grilla control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBuscarPersona_Grilla As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnBuscarPersona_Grillas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBuscarPersona_Grillas As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''DGV_BuscarPersona control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DGV_BuscarPersona As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnAnterior_Persona control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAnterior_Persona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnPosterior_Persona control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPosterior_Persona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtPageIndex_Persona control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPageIndex_Persona As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnIr_Persona control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnIr_Persona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtPageIndexGO_Persona control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPageIndexGO_Persona As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ImageButton3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImageButton3 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''gvwDetalleAbonos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvwDetalleAbonos As Global.System.Web.UI.WebControls.GridView
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmConsultaDeudaCliente
    Inherits System.Web.UI.Page
    Dim Mensajes As New ScriptManagerClass
    Dim cbo As New Combo
    Dim fecha As New Negocio.FechaActual
    Dim BuscarDato As New Negocio.Util

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '***** Carga el formulario...
        If Not IsPostBack Then
            '****** Crea la sesion para cargar los datos en las grillas Telefono y Correos...
            'CargaCombos()
            CargaGrillaCxCDeudaC()
        End If
    End Sub
    Private Sub CargaGrillaCxCDeudaC()
        '***** Carga los datos de la Grilla
        Dim CxCGrilla As New Grilla
        Dim CxCDeuda As New Negocio.ConsultaDeudaCxC_View
        CxCGrilla.LlenarGrillaCxCDeuda(Me.gvCuentasxCobrar)
        Dim LCxCDeuda As New List(Of Entidades.ConsultaDeudaCxC_View)
        LCxCDeuda = CxCDeuda.SelectGrillaCxCDeuda()
        If LCxCDeuda.Count = 0 Then
            Me.lbl_Mensaje.Text = "No hay datos para mostrar"
            Me.lbl_Mensaje.Visible = True
        Else
            Me.lbl_Mensaje.Visible = False
            Me.lbl_Mensaje.Text = ""
        End If
        gvCuentasxCobrar.DataSource = LCxCDeuda
        gvCuentasxCobrar.DataBind()
    End Sub
    Protected Sub cmd_Buscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Buscar.Click
        '***** Muestra las opciones de búsqueda para un Cliente
        cmd_VerTodos.Visible = False
        OcultaControlesBusqueda()
        pnl_Busqueda.Visible = True
    End Sub
    Private Sub OcultaControlesBusqueda()
        LimpiaBusquedaCliente()
        Me.rbl_BuscaNombre.Visible = False
        Me.rbl_BuscaDNI.Visible = False
        Me.txt_Buscar.Visible = False
        Me.ddl_Estado.Visible = False
        Me.cmd_BuscaCliente.Visible = False
        Me.cmd_Limpiar.Visible = False
    End Sub
    Private Sub LimpiaBusquedaCliente()
        '***** Limpia los controles para la busqueda
        Me.ddl_BuscaPor.SelectedIndex = 0
        Me.rbl_BuscaNombre.SelectedIndex = 0
        Me.rbl_BuscaDNI.SelectedIndex = 0
        Me.txt_Buscar.Text = ""
    End Sub
    Protected Sub cmd_OcultaBusqueda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_OcultaBusqueda.Click
        '***** Oculta el Panel de busqueda del Cliente
        OcultaControlesBusqueda()
        pnl_Busqueda.Visible = False
    End Sub
    Protected Sub ddl_BuscaPor_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_BuscaPor.SelectedIndexChanged
        '***** Activa las opciones de busqueda...
        Select Case ddl_BuscaPor.SelectedValue
            Case "0"
                '***** No hace nada...
                OcultaControlesBusqueda()
            Case "1"
                '***** Muestra todos los Clientes..
                Me.txt_Buscar.Visible = False
                Me.ddl_Estado.Visible = False
                Me.cmd_BuscaCliente.Visible = False
                Me.rbl_BuscaDNI.Visible = False
                Me.rbl_BuscaNombre.Visible = False
                Me.cmd_Limpiar.Visible = False
                CargaGrillaCxCDeudaC()
                Me.ddl_BuscaPor.SelectedValue = "0"
                Me.txt_Buscar.Text = ""
                Me.rbl_BuscaNombre.SelectedIndex = 0
                Me.ddl_Estado.SelectedValue = "1"
                Me.ddl_BuscaPor.Focus()
            Case "2"
                '***** Busca por Nombre/Razon Social
                Me.txt_Buscar.Width = 218
                Me.txt_Buscar.Visible = True
                Me.ddl_Estado.Visible = False
                Me.cmd_BuscaCliente.Visible = True
                Me.rbl_BuscaDNI.Visible = False
                Me.rbl_BuscaNombre.Visible = True
                Me.cmd_Limpiar.Visible = True
                Me.txt_Buscar.Text = ""
                Me.rbl_BuscaNombre.SelectedIndex = 0
                Me.ddl_Estado.SelectedValue = "1"
                Me.txt_Buscar.Focus()
            Case "3"
                '***** Busca por RUC/DNI
                Me.txt_Buscar.Width = 118
                Me.txt_Buscar.Visible = True
                Me.ddl_Estado.Visible = False
                Me.cmd_BuscaCliente.Visible = True
                Me.rbl_BuscaNombre.Visible = False
                Me.rbl_BuscaDNI.Visible = True
                Me.cmd_Limpiar.Visible = True
                Me.txt_Buscar.Text = ""
                Me.rbl_BuscaNombre.SelectedIndex = 0
                Me.ddl_Estado.SelectedValue = "1"
                Me.txt_Buscar.Focus()
            Case "4"
                '***** Busca por Codigo (ID_Persona)
                Me.txt_Buscar.Width = 118
                Me.txt_Buscar.Visible = True
                Me.ddl_Estado.Visible = False
                Me.cmd_BuscaCliente.Visible = True
                Me.rbl_BuscaDNI.Visible = False
                Me.rbl_BuscaNombre.Visible = False
                Me.cmd_Limpiar.Visible = True
                Me.txt_Buscar.Text = ""
                Me.rbl_BuscaNombre.SelectedIndex = 0
                Me.ddl_Estado.SelectedValue = "1"
                Me.txt_Buscar.Focus()
            Case "5"
                '***** Buscar por Estado (Activo/Inactivo)
                Me.txt_Buscar.Visible = False
                Me.ddl_Estado.Visible = True
                Me.cmd_BuscaCliente.Visible = False
                Me.rbl_BuscaDNI.Visible = False
                Me.rbl_BuscaNombre.Visible = False
                Me.cmd_Limpiar.Visible = False
                Me.txt_Buscar.Text = ""
                Me.rbl_BuscaNombre.SelectedIndex = 0
                Me.ddl_Estado.SelectedValue = "1"
                Me.ddl_Estado.Focus()
        End Select
    End Sub
    Protected Sub cmd_Limpiar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Limpiar.Click
        LimpiaBusquedaCliente()
    End Sub

    Protected Sub gvCuentasxCobrar_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvCuentasxCobrar.SelectedIndexChanged
        Dim NegMovCuenta As New Negocio.MovCuenta
        Me.GridView1.DataSource = NegMovCuenta.SelectDeudaxIdPersona(CInt(Me.gvCuentasxCobrar.Rows(Me.gvCuentasxCobrar.SelectedIndex).Cells(1).Text))
        Me.GridView1.DataBind()
    End Sub
End Class
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmConsultaCxCResumenes.aspx.vb" Inherits="APPWEB.frmConsultaCxCResumenes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender5" runat="server" CollapseControlID="Image5"
                                Collapsed="true" CollapsedImage="~/Imagenes/Mas_B.JPG" CollapsedSize="0" CollapsedText="Total Por Moneda"
                                ExpandControlID="Image5" ExpandDirection="Vertical" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                ExpandedText="Total Por Cobrar" ImageControlID="Image5" TargetControlID="PanelMoneda"
                                TextLabelID="Lbl_Natural" />
                            <asp:Image ID="Image5" runat="server" Height="16px" />
                            Total Por Empresa y Tienda
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Panel ID="PanelMoneda" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="LabelTab">
                                                        Empresa:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlEmpresa" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="LabelTab">
                                                        Tienda:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTienda" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Aceptar_b.JPG"
                                                            onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../Imagenes/Aceptar_A.JPG';"
                                                            ToolTip="Efect�a la B�squeda del Cliente" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvwCxCxMoneda" runat="server" AutoGenerateColumns="False" DataKeyNames="idmoneda"
                                                Width="60%" HeaderStyle-Height="25px" RowStyle-Height="25px">
                                                <Columns>
                                                    <asp:BoundField DataField="mon_simbolo" HeaderText="Moneda" />
                                                    <asp:BoundField DataField="Subtotal" HeaderText="SubTotal" />
                                                    <asp:BoundField DataField="tc_CompraC" HeaderText="Cambio Comercial" />
                                                    <asp:BoundField DataField="mon_base" HeaderText="S/." />
                                                    <asp:BoundField DataField="TotalResumen" HeaderText="Total Soles" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Label ">
                                                        <asp:Label ID="Label3" runat="server" Text="Total S/." Visible="false"></asp:Label>
                                                    </td>
                                                    <td class="Label ">
                                                        <asp:TextBox ID="txtMontoSoles" runat="server" ReadOnly="True" CssClass="TextBoxReadOnly_Right"
                                                            Visible="false"></asp:TextBox>
                                                    </td>
                                                    <td class="Label ">
                                                        <asp:Label ID="Label4" runat="server" Text="Total $." Visible="false"></asp:Label>
                                                    </td>
                                                    <td class="Label ">
                                                        <asp:TextBox ID="txtMontoDolares" runat="server" ReadOnly="True" CssClass="TextBoxReadOnly_Right"
                                                            Visible="false"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_Mensaje" runat="server" CssClass="Label" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            Resumen Cuentas Por Cobrar Por Cliente
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="cmd_Imprimir_Cliente" runat="server" cToolTip="Imprimir" ImageUrl="~/Imagenes/Imprimir_B.JPG"
                                                                    OnClientClick="return(ImpClientes());" onmouseout="this.src='../Imagenes/Imprimir_B.JPG';"
                                                                    onmouseover="this.src='../Imagenes/Imprimir_A.JPG';" />
                                                            </td>
                                                            <td class="Label">
                                                                Empresa:
                                                                <asp:DropDownList ID="ddlidEmpresaCliente" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="Label">
                                                                Tienda:
                                                                <asp:DropDownList ID="ddlIdTiendaCliente" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="btnAceptaCliente" runat="server" ImageUrl="~/Imagenes/Aceptar_b.JPG"
                                                                    onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../Imagenes/Aceptar_A.JPG';"
                                                                    ToolTip="Efect�a la B�squeda del Cliente" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="cmd_BuscaCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                    onmouseout="this.src='../Imagenes/Buscar_B.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';"
                                                                    ToolTip="Efect�a la B�squeda del Cliente" OnClientClick="return(mostrarCapaPersona());" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="GvwCxCxClientes" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                        Width="100%" HeaderStyle-Height="25px" RowStyle-Height="25px">
                                                        <Columns>
                                                            <asp:CommandField ShowSelectButton="True" />
                                                            <asp:BoundField DataField="IdPersona" HeaderText="Id" />
                                                            <asp:BoundField DataField="RucDni" HeaderText="RucDni" />
                                                            <asp:BoundField DataField="nombre" HeaderText="RazonSocial">
                                                                <ItemStyle HorizontalAlign="Left" Width="300px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="SalSol" DataFormatString="{0:F2}" HeaderText="Importe Soles" />
                                                            <asp:BoundField DataField="SalDol" DataFormatString="{0:F2}" HeaderText="Importe Dolares" />
                                                        </Columns>
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblMensajeCliente" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="SubTituloCelda">
                            Resumen Cuentas Por Cobrar Por Documentos
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="ImprimirDoc" runat="server" cToolTip="Imprimir" Visible="false"
                                                        ImageUrl="~/Imagenes/Imprimir_B.JPG" OnClientClick="return(ImpDocumento());"
                                                        onmouseout="this.src='../Imagenes/Imprimir_B.JPG';" onmouseover="this.src='../Imagenes/Imprimir_A.JPG';" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvwCxCxDocumento" runat="server" AutoGenerateColumns="False" Width="100%"
                                            HeaderStyle-Height="25px" RowStyle-Height="25px">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" SelectText="Ver Detalle" />
                                                <asp:BoundField DataField="idDocumento" HeaderText="Id" />
                                                <asp:BoundField DataField="Numero" HeaderText="Numero" />
                                                <asp:BoundField DataField="tdoc_NombreCorto" HeaderText="Documento" />
                                                <asp:BoundField DataField="doc_FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="FechaEmision" />
                                                <asp:BoundField DataField="doc_FechaVencimiento" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="FechaVencimiento" />
                                                <asp:BoundField DataField="nroDias" HeaderText="Dias Vencidos" />
                                                <asp:BoundField DataField="mon_simbolo" HeaderText="Moneda" />
                                                <asp:BoundField DataField="doc_TotalPagar" DataFormatString="{0:F2}" HeaderText="Importe" />
                                                <asp:BoundField DataField="abono" DataFormatString="{0:F2}" HeaderText="TotalABonos" />
                                                <asp:BoundField DataField="saldo" DataFormatString="{0:F2}" HeaderText="Saldo" />
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <RowStyle CssClass="GrillaRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMensajeDocumento" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family: Tahoma; font-size: x-small; font-weight: bold; color: #2B54A3;
                                        text-align: center;" valign="middle" align="center">
                                        <asp:Label ID="Label38" runat="server" Text="Powered by" Width="63px"></asp:Label>
                                        <asp:ImageButton ID="cmd_Digrafic" runat="server" AlternateText="Digrafic S.R.L."
                                            ImageUrl="~/Imagenes/LogoDigrafic_01.jpg" PostBackUrl="http://www.digrafic.com.pe"
                                            Width="83px" ImageAlign="Middle" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdpersona" runat="server" Value="hddidpersona" />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr class="Label">
                <td align="left">
                    <table>
                        <tr>
                            <td>
                                Filtro:
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbFiltro_BuscarPersona" runat="server">
                                    <asp:ListItem Selected="True" Value="0">Descripci�n</asp:ListItem>
                                    <asp:ListItem Value="1">R.U.C.</asp:ListItem>
                                    <asp:ListItem Value="2">D.N.I.</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Texto:
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersona_Grillas">                                
                                <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" Width="250px"></asp:TextBox>
                               <%-- <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" CausesValidation="false"
                                    ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                    onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                    <asp:Button ID="btnBuscarPersona_Grillas" runat="server" Text="Buscar" CssClass="btnBuscar"
                                    OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                    </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                            <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                            <asp:BoundField DataField="getNombreParaMostrar" HeaderText="Descripci�n" NullDisplayText="---" />
                            <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                            <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                            <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---" />
                            <asp:BoundField DataField="PorcentPercepcion" HeaderText="Percep. (%)" NullDisplayText="0"
                                DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="PorcentRetencion" HeaderText="Retenc. (%)" NullDisplayText="0"
                                DataFormatString="{0:F2}" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px"
                        Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                    <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px"
                        Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                    <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox>
                    <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                        ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                    <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDetalleAbonos" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaDetalleAbonos'));" />
                </td>
            </tr>
            <tr>
                <td class="TextoRojoMedium" style="font-weight: bold">
                    Detalle De Abonos Por Documento
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="gvwDetalleAbonos" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        PageSize="20" Width="100%" HeaderStyle-Height="25px" RowStyle-Height="25px">
                        <Columns>
                            <asp:BoundField DataField="Numero" HeaderText="Numero" />
                            <asp:BoundField DataField="tdoc_NombreCorto" HeaderText="Documento" />
                            <asp:BoundField DataField="doc_FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="FechaMovimiento" />
                            <asp:BoundField DataField="mon_simbolo" HeaderText="Moneda" />
                            <asp:BoundField DataField="doc_TotalPagar" DataFormatString="{0:F2}" HeaderText="Importe" />
                            <asp:BoundField DataField="abono" DataFormatString="{0:F2}" HeaderText="TotalAbonos" />
                            <asp:BoundField DataField="Saldo" DataFormatString="{0:F2}" HeaderText="Saldo" />
                            <asp:BoundField DataField="CuentaTipo" HeaderText="Cuenta Tipo " />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" language="javascript">

        function ImpClientes() {

            if (!confirm('Desea mandar a impresi�n el documento?')) {
                return false;
            }

            window.open('./Reportes/visorRG.aspx?iReporte=1');

            return false;
        }

        function ImpDocumento() {

            if (!confirm('Desea mandar a impresi�n el documento?')) {
                return false;
            }

            window.open('./Reportes/visorRG.aspx?iReporte=2');

            return false;
        }


        //para levantar las capas
        function onCapaCliente(s) {
            LimpiarControlesCliente();
            onCapa(s);
            document.getElementById('<%= txtBuscarPersona_Grilla.ClientID%>').focus();
            return false;
        }
        function CierraPersona() {
            offCapa('capaPersona');
            return false;
        }
        function onCapaDetalleAbonos() {
            var grilla = document.getElementById('<%=gvwDetalleAbonos.ClientID %>');
            if (grilla == null) {
                alert("no tiene registros Para mostrar");
                return false;
            }

            onCapa("capaDetalleAbonos");
            return false;
        }

        void function LimpiarControlesCliente() {
            return false;
        }

        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }



        //Buscar SubLinea
        function ValidarEnteroSinEnter() {
            if (onKeyPressEsNumero('event') == false) {
                alert('Caracter no v�lido. Solo se permiten n�meros Enteros');
                return false;
            }
            return true;
        }


        //End Buscar SubLinea
        //Buscar Cliente

        function BuscarCliente() {

            return true;
        }


        function ValidarEnter() {
            return (!esEnter(event));
        }
        function ValidaEnter() {
            if (esEnter(event) == true) {
                alert('Tecla No permitida');
                caja.focus();
                return false;
            }
            return true;
        }
        //End Buscar Cliente


        function mostrarCapaPersona() {
            //******************* validamos que exista detalle

            onCapa('capaPersona');
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
            //} else {
            //    alert('Debe ingresar primero el detalle del documento.');
            //}
            return false;
        }
        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }
        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }
        function valNavegacionPersona(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

    </script>

</asp:Content>

﻿/*
'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

*/
function getPrint(print_area) {

    //Creating new page

    //var pp = window.open('target-new=tab');
    var pp = window.open();

    //Adding HTML opening tag with <HEAD> … </HEAD> portion

    pp.document.writeln('<HTML><HEAD><title>Print Preview</title>')

    //pp.document.writeln('<LINK href=Styles.css type="text/css" rel="stylesheet">')

    pp.document.writeln('<LINK ../Estilo/EstiloGeneral.css type="text/css" rel="stylesheet" media="print">')

    pp.document.writeln('<base target="_self"></HEAD>')

    //Adding Body Tag

    pp.document.writeln('<body MS_POSITIONING="GridLayout" bottomMargin="0"');

    pp.document.writeln(' leftMargin="0" topMargin="0" rightMargin="0">');

    //Adding form Tag

    pp.document.writeln('<form method="post">');

    //Creating two buttons Print and Close within a HTML table

    pp.document.writeln('<TABLE width=100%><TR><TD></TD></TR><TR><TD align=right>');

    pp.document.writeln('<INPUT ID="Imprimir" type="button" value="Imprimir" ');

    pp.document.writeln('onclick=" location.reload(true);window.print();">');

    pp.document.writeln('<INPUT ID="Cerrar" type="button" value="Cerrar" onclick="window.close();">');

    pp.document.writeln('</TD></TR><TR><TD></TD></TR></TABLE>');

    //Writing print area of the calling page

    pp.document.writeln(document.getElementById(print_area).innerHTML);

    //Ending Tag of </form>, </body> and </HTML>

    pp.document.writeln('</form></body></HTML>');

} 


       function printPreviewDiv(NomDiv) {
           var printContent = document.getElementById(NomDiv);
           var windowUrl = 'about:blank';
           var uniqueName = new Date();
           var windowName = 'Print' + uniqueName.getTime();
           var printWindow = window.open(windowUrl, windowName, 'width=10000,height=10000,resizable=yes');
           
           printWindow.document.write('<head> <link href="../../Estilos/Controles.css" rel="stylesheet" type="text/css" /> </head>');
           
           var printPreviewObject = '<object id="printPreviewElement" width="0" height="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2">' +
               '<PARAM NAME="ExtentX" VALUE="7938">' +
               '<PARAM NAME="ExtentY" VALUE="3969">' +
               '<PARAM NAME="ViewMode" VALUE="0">' +
               '<PARAM NAME="Offline" VALUE="0">' +
               '<PARAM NAME="Silent" VALUE="0">' +
               '<PARAM NAME="RegisterAsBrowser" VALUE="0">' +
               '<PARAM NAME="RegisterAsDropTarget" VALUE="1">' +
               '<PARAM NAME="AutoArrange" VALUE="0">' +
               '<PARAM NAME="NoClientEdge" VALUE="0">' +
               '<PARAM NAME="AlignLeft" VALUE="0">' +
               '<PARAM NAME="NoWebView" VALUE="0">' +
               '<PARAM NAME="HideFileNames" VALUE="0">' +
               '<PARAM NAME="SingleClick" VALUE="0">' +
               '<PARAM NAME="SingleSelection" VALUE="0">' +
               '<PARAM NAME="NoFolders" VALUE="0">' +
               '<PARAM NAME="Platform" VALUE="256">' +
               '<PARAM NAME="FormatAsHTML" VALUE="0">' +
               '<param NAME="SCALE" VALUE="ExactFit">' +
               '<PARAM NAME="FontSize" VALUE="9">' +
               '<PARAM NAME="Transparent" VALUE="0">' +
               '<PARAM NAME="ViewID" VALUE="{0057D0E0-3573-11CF-AE69-08002B2E1262}">' +
               '</object>';
                printWindow.document.write(printContent.innerHTML);
                printWindow.document.write(printPreviewObject);                
                printWindow.document.write('<script language=JavaScript>');
                printWindow.document.write('printPreviewElement.ExecWB(6,1,null,null);');
                printWindow.document.write('printPreviewElement.outerHTML = "";');
                printWindow.document.write('</' + 'script>');
                printWindow.document.close();
                //printWindow.focus();
                //printWindow.close();
    }   



/*
'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

*/

var cmThemeTheme13Base = '.';

// the follow block allows user to re-define theme base directory
// before it is loaded.
try
{
	if (myThemeTheme13Base)
	{
		cmThemeTheme13Base = myThemeTheme13Base;
	}
}
catch (e)
{
}

var cmThemeTheme13 =
{
  	    mainFolderLeft: '<div style="width: 14px; height: 27px" class="themeSpacerDiv" />',
        mainFolderRight: '<div style="width: 19px; height: 27px" class="themeSpacerDiv" />',
        mainItemLeft: '<div style="width: 14px; height: 27px" class="themeSpacerDiv" />',
        mainItemRight: '<div style="width: 19px; height: 27px" class="themeSpacerDiv" />',
        folderLeft: '<div style="width: 14px; height: 27px" class="themeSpacerDiv" />',
        folderRight: '<div style="width: 19px; height: 27px" class="themeSpacerDiv" />',
        itemLeft: '<div style="width: 14px; height: 27px" class="themeSpacerDiv" />',
        itemRight: '<div style="width: 19px; height: 27px" class="themeSpacerDiv" />',
        mainSpacing: 0,
        subSpacing: 0,
        delay: 100
};

var cmThemeTheme13HSplit = [_cmNoClick, '<td  class="ThemeTheme13MenuSplitLeft"><div></div></td>' +
					                          '<td  class="ThemeTheme13MenuSplitText"><div></div></td>' +
					                          '<td  class="ThemeTheme13MenuSplitRight"><div></div></td>'
		                         ];

var cmThemeTheme13MainVSplit = [_cmNoClick, '<div>' +
                            '<table height="29" width="10" ' +
                            ' cellspacing="0"><tr><td class="ThemeTheme13HorizontalSplit">' +
                           '<div class="themeSpacerDiv" style=" width: 1px; height: 1px" /></td></tr></table></div>'];

var cmThemeTheme13MainHSplit = [_cmNoClick, '<td  class="ThemeTheme13MainSplitLeft"><div></div></td>' +
					                          '<td  class="ThemeTheme13MainSplitText"><div></div></td>' +
					                          '<td  class="ThemeTheme13MainSplitRight"><div></div></td>'
		                           ];    
 
     
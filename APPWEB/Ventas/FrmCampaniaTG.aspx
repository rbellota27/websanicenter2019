﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmCampaniaTG.aspx.vb" Inherits="APPWEB.FrmCampaniaTG" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <asp:Button ID="btn_nuevo" runat="server" Text="Nuevo" Width="90px" />
                <asp:Button ID="btn_guardar" runat="server" Text="Guardar" OnClientClick=" return ( onClick_Guardar() ); "
                    Width="90px" />
                <asp:Button ID="btn_cancelar" runat="server" Text="Cancelar" Width="90px" />
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Transferencia gratuita
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_empresa" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_tienda" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Campa&ntilde;a:
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddl_campania" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Incluir:
                        </td>
                        <td colspan="3">
                            <asp:CheckBox ID="cb_guardarAll" runat="server" CssClass="Texto" Text="Todos los productos de la campaña" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                Producto en campa&ntilde;a
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="btn_buscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                    OnClientClick="return(onclick_buscarProducto(1));" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                    onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_Campania_Detalle" runat="server" AutoGenerateColumns="False"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Código" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCodigoProducto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CodigoProducto") %>'
                                                Font-Bold="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />
                                            <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCampania") %>' />
                                            <asp:HiddenField ID="hddIdCampaniaDetalle" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCampaniaDetalle") %>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Producto" HeaderStyle-HorizontalAlign="Center" HeaderText="Producto"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderText="Precio Actual ( Comercial )">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMoneda_PrecioActual" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Moneda_PrecioActual") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPrecioActual" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"PrecioActual","{0:F2}") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderText="U.M.">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida" runat="server" DataValueField="Id" DataTextField="DescripcionCorto"
                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"getListaUnidadMedida") %>'>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow"
                            HeaderStyle-ForeColor="Blue" ItemStyle-HorizontalAlign="Center" HeaderText="Precio">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboMoneda" runat="server" DataValueField="Id" DataTextField="Simbolo"
                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaMoneda") %>'>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPrecioCampania" TabIndex="200" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container.DataItem,"Precio","{0:F2}") %>'
                                                Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow"
                            HeaderStyle-ForeColor="Blue" ItemStyle-HorizontalAlign="Center" HeaderText="Cantidad Mín.">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCantidadMin" TabIndex="210" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container.DataItem,"CantidadMin","{0:F2}") %>'
                                                Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                Producto en transferencia
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="btn_buscarProducto2" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                    OnClientClick="return(onclick_buscarProducto(2));" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                    onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="cb_productoTG" runat="server" Text="Buscar productos en tranferencia de la campaña seleccionada"
                    CssClass="Texto" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_DetalleTG" runat="server" AutoGenerateColumns="False" Width="100%">
                    <Columns>
                        <asp:CommandField SelectText="Quitar" ShowSelectButton="true" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Height="25px"></ItemStyle>
                        </asp:CommandField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Código" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCodigoProducto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CodigoProducto") %>'
                                                Font-Bold="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />
                                            <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCampania") %>' />
                                            <asp:HiddenField ID="hddIdCampaniaDetalle" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCampaniaDetalle") %>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Producto" HeaderStyle-HorizontalAlign="Center" HeaderText="Producto"
                            ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderText="Precio Actual ( Comercial )">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPrecioActual" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"PrecioActual","{0:F2}") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderText="U.M.">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida" runat="server" DataValueField="Id" DataTextField="DescripcionCorto"
                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"getListaUnidadMedida") %>'>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderText="Precio">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboMoneda" runat="server" DataValueField="Id" DataTextField="Simbolo"
                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaMoneda") %>'>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPrecioCampania" TabIndex="200" runat="server" Width="70px" onFocus="return ( aceptarFoco(this) );"
                                                onKeyPress=" return( validarNumeroPuntoPositivo('event') ); " onblur=" return( valBlurClear('0',event) ); "
                                                Text='<%# DataBinder.Eval(Container.DataItem,"cdtg_Precio","{0:F2}") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderText="Cantidad">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCantidadMin" TabIndex="210" runat="server" Width="70px" onFocus="return ( aceptarFoco(this) );"
                                                onKeyPress=" return( validarNumeroPuntoPositivo('event') ); " onblur=" return( valBlurClear('0',event) ); "
                                                Text='<%# DataBinder.Eval(Container.DataItem,"cdtg_Cantidad","{0:F2}") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Incrementar por cantidad campaña">
                            <ItemTemplate>
                                <asp:CheckBox ID="cb_cantCampania" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"cdtg_cantCampania") %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="120px" />
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddBusquedaProducto" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 70px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    AutoPostBack="true" DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripción:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarGrilla_AddProd">                                            
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td class="Texto" style="text-align: right">
                                            Cód.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProd">
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );"
                                                TabIndex="205"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="15">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="85px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:TemplateField HeaderText="Código" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                <asp:HiddenField ID="hddIdCampaniaDetalle" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampaniaDetalle")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodigoProducto")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Producto" HeaderText="Descripción" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                            <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function onclick_buscarProducto(opc) {
            var ddl_campania = document.getElementById('<%=ddl_campania.ClientID %>');
            var hddBusquedaProducto = document.getElementById('<%=hddBusquedaProducto.ClientID %>');
            if (isNaN(ddl_campania.value) || ddl_campania.value.length <= 0) {
                alert('Seleccione una campaña');
                return false;
            }
            hddBusquedaProducto.value = opc;

            switch (parseInt(opc)) {
                case 1:
                    break;
                case 2:
                    var GV_Campania_Detalle = document.getElementById('<%=GV_Campania_Detalle.ClientID %>');
                    var cb_guardarAll = document.getElementById('<%=cb_guardarAll.ClientID %>');
                    var cb_productoTG = document.getElementById('<%=cb_productoTG.ClientID %>');

                    if (GV_Campania_Detalle == null && cb_guardarAll.status == false && cb_productoTG.status == false) {
                        alert('Seleccione un detalle de campaña');
                        return false;
                    }
                    break;
            }


            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        //
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        //
        function valKeyPressDescripcionProd() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        //
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }
        //
        function onClick_Guardar() {

            var cantidad = 0;
            var GV_DetalleTG = document.getElementById('<%=GV_DetalleTG.ClientID %>');
            if (GV_DetalleTG != null) {

                for (var i = 1; i < GV_DetalleTG.rows.length; i++) {
                    var rowElem = GV_DetalleTG.rows[i];
                    var txtcantidad = rowElem.cells[6].children[0].cells[0].children[0];
                    cantidad = txtcantidad.value;

                    if (isNaN(parseInt(cantidad))) { cantidad = 0; }

                    if (cantidad <= 0) {
                        var prod_nombre = rowElem.cells[2].innerText;
                        alert('Ingrese una cantidad mayor a 0 para el producto ' + prod_nombre + '.\nNo se permite la operación');
                        return false;
                    }
                }

            }
            return confirm('Desea continuar con la operación');
        }
        
    </script>

</asp:Content>

<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="ConfiguracionDescuentoAvanzado.aspx.vb" Inherits="APPWEB.ConfiguracionDescuentoAvanzado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btNuevo" Width="85px" runat="server" Text="Nuevo" />
                        </td>
                        <td>
                            <asp:Button ID="btGuardar" Width="85px" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());" />
                        </td>
                        <td>
                            <asp:Button ID="btCancelar" OnClientClick="return(confirm('Desea ir al modo Inicio?'));"
                                Width="85px" runat="server" Text="Cancelar" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Configuraci�n Descuento AVANZADO</td>
        </tr>
        <tr>
            <td style="height: 174px">
                <asp:Panel ID="pnlPrincipal" runat="server">
                    <table style="height: 202px" border="1" cellspacing="0">
                        <tr>
                            <td class="Texto" style="width: 130px; font-size: x-small">
                                Codigo:&nbsp;
                            </td>
                            <td style="width: 100px; background-color: #D0FBFA; font-size: small;">
                                <asp:TextBox ID="tbCodigo" runat="server" CssClass="TextBoxReadOnly_Right" 
                                    ReadOnly="true" Width="80px"></asp:TextBox>
                            </td>
                            <td style="width: 107px; color: #0000FF; text-align: right; font-size: small;">
                                Tipo Precio:</td>
                            <td style="width: 91px; background-color: #D0FBFA;">
                                <asp:DropDownList ID="dlprecio" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 97px; color: #0000FF; font-size: small;">
                                Pais Producto :</td>
                            <td style="width: 91px; background-color: #D0FBFA;">
                                <asp:DropDownList ID="dlPais" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="height: 28px; width: 130px; font-size: x-small">
                                &nbsp;Perfil:
                            </td>
                            <td style="width: 100px; height: 28px; background-color: #D0FBFA;">
                                <asp:DropDownList ID="dlperfil" runat="server" AutoPostBack="True" 
                                    style="font-size: small">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 107px; color: #0000FF; text-align: right; height: 28px; font-size: small;">
                                Medio Pago:
                            </td>
                            <td style="width: 91px; height: 28px; background-color: #D0FBFA;">
                                <asp:DropDownList ID="dlmedio" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 97px; color: #0000FF; height: 28px; font-size: small;">
                                Existencia :</td>
                            <td style="width: 91px; height: 28px; background-color: #D0FBFA;">
                                <asp:DropDownList ID="dlExistencia" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="height: 29px; width: 130px; font-size: x-small">
                                Condicion de Pago: &nbsp;&nbsp;
                            </td>
                            <td style="width: 100px; height: 29px; background-color: #D0FBFA;">
                                <asp:DropDownList ID="dlcondicion" runat="server" style="font-size: small">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 107px; color: #0000FF; text-align: right; height: 29px; font-size: small;">
                                TipoOperacion :</td>
                            <td style="width: 91px; height: 29px; background-color: #D0FBFA;">
                                <asp:DropDownList ID="dlTipoOperacion" runat="server" style="font-size: small">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 97px; color: #0000FF; height: 29px; font-size: small;">
                                Linea Producto:</td>
                            <td style="width: 91px; height: 29px; background-color: #D0FBFA;">
                                <asp:DropDownList ID="dlLinea" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="width: 130px; font-size: small; height: 33px;">
                                &nbsp;<span style="font-size: x-small"> % Descuento: </span>
                            </td>
                            <td style="width: 100px; background-color: #D0FBFA; height: 33px;">
                                <asp:TextBox ID="tbdescuento" runat="server" 
                                    onblur="return(   valBlur(event)     );" onFocus="return(   aceptarFoco(this)   );" 
                                    onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" 
                                    style="font-size: small" Width="100px"></asp:TextBox>
                            </td>
                            <td style="width: 107px; color: #0000FF; text-align: right; font-size: small; height: 33px;">
                                Estado:</td>
                            <td style="width: 91px; background-color: #D0FBFA; height: 33px;">
                                <asp:RadioButtonList ID="rbestado" runat="server" CssClass="Label" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td style="width: 97px; font-size: small; height: 33px;">
                                </td>
                            <td style="width: 91px; background-color: #D0FBFA; height: 33px;">
                                </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="width: 130px; font-size: small">
                                <span style="font-size: x-small">Precio Base dscto.</span> </td>
                            <td style="width: 100px; background-color: #D0FBFA;">
                                <asp:RadioButtonList ID="rbtPBD" runat="server" RepeatDirection="Horizontal" 
                                    style="font-weight: 700; font-size: small" Width="143px">
                                    <asp:ListItem Selected="True" Text="Precio Lista" Value="PL"></asp:ListItem>
                                    <asp:ListItem Text="Precio Comercial" Value="PC"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td style="width: 107px; color: #336699; text-align: right; font-size: small;">
                                &nbsp;</td>
                            <td style="width: 91px; background-color: #D0FBFA;">
                                &nbsp;</td>
                            <td style="width: 97px; font-size: small;">
                                &nbsp;</td>
                            <td style="width: 91px; background-color: #D0FBFA;" colspan="0">
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        
        
        <tr>
            <td style="color: #4277AD">
            <b>USUARIO ASIGNADO</b></td>
        </tr>
        <tr>
            <td>
                                                <asp:Label ID="Label24" CssClass="Label" 
                    runat="server" Text="Ap. y Nombres:"></asp:Label>
                                                <asp:TextBox ID="txtTextoABuscar" Width="300px" 
                    MaxLength="100" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="btnBuscarPersona0" 
                    runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                                                    
                    onmouseout="this.src='../../Imagenes/Buscar_B.JPG';" onmouseover="this.src='../../Imagenes/Buscar_A.JPG';"
                                                    Visible="True" />
                <br />
            </td>
        </tr>
        <tr>
            <td style="background-color: #4277AD">
                                                <b><span style="background-color: #4277AD">
                                                <span style="color: #FFFFFF">Codigo                                                <span style="color: #3366FF">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span 
                                                    style="background-color: #4277AD"><span style="color: #FFFFFF"> </span>
                                                </span>
                                                </b>
                                                <span style="color: #FFFFFF"><span style="background-color: #4277AD">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                </span></span>
                                                <span style="color: #3366FF"> <b>
                                                <span style="color: #FFFFFF; background-color: #4277AD">Apellidos y Nombres</span></b></span></td>
        </tr>
        <tr>
            <td style="height: 7px">
                                                <asp:TextBox ID="TxtIdUsuario" Width="101px" 
                    MaxLength="100" runat="server"></asp:TextBox>
                                                <asp:TextBox ID="txtNombreUsuario" Width="498px" 
                    MaxLength="100" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            
             <asp:Panel ID="PanelSecundario" runat="server">
                <table class="style1" style="margin-top: 0px">
                    <tr>
                        <td>
                            <asp:GridView ID="DGVUsuario" runat="server" AllowPaging="True" 
                                AutoGenerateColumns="False" PageSize="15" Width="100%">
                                <Columns>
                                
                                <asp:CommandField ShowSelectButton="True" />
                           <%--         <asp:TemplateField HeaderText="Marcar">
                                    <ItemTemplate> <asp:CheckBox ID="Marcar" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Marcar")%>' />
                                    </ItemTemplate>
                                    
                                    </asp:TemplateField>--%>
                                    
                                    
                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="---" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Ap. y Nombres" NullDisplayText="---" />
                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" />
                                    <asp:BoundField DataField="Login" HeaderText="Login" NullDisplayText="---" />
                                    <asp:BoundField DataField="DescFechaAlta" HeaderText="Fecha Alta" NullDisplayText="---" />
                                    <asp:BoundField DataField="DescFechaBaja" HeaderText="Fecha Baja" NullDisplayText="---" />
                                    <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="---" />
                                    <asp:BoundField ControlStyle-Width="0px" DataField="IdMotivoBaja" HeaderStyle-Width="0px" HeaderText="" ItemStyle-Width="0px" 
                                NullDisplayText="---" Visible="true" >
                                
                                
<ControlStyle Width="0px"></ControlStyle>

<HeaderStyle Width="0px"></HeaderStyle>

<ItemStyle Width="0px"></ItemStyle>
</asp:BoundField>
<asp:BoundField ControlStyle-Width="0px" DataField="Estado" 
                                        HeaderStyle-Width="0px" HeaderText="" ItemStyle-Width="0px" 
                                        NullDisplayText="---" Visible="true" >
<ControlStyle Width="0px"></ControlStyle>

<HeaderStyle Width="0px"></HeaderStyle>

<ItemStyle Width="0px"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="color: #4277AD">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="color: #4277AD">
                            <b>PRODUCTO ESPECIFICO<asp:Label ID="lblmensaje" runat="server"></asp:Label></b></td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
            <td style="background-color: #4277AD">
                                                <span style="background-color: #4277AD">
                                                <span style="color: #FFFFFF">
                                                <b>Codigo<span style="color: #3366FF">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                <span style="color: #3366FF"> <b>
                                                <span style="color: #FFFFFF; background-color: #4277AD">Nombre del Productos</span></b></span></td>
                                        </tr>
                    <tr>
                        <td>
                                                <asp:TextBox ID="TxtIdProducto" Width="101px" 
                    MaxLength="100" runat="server"></asp:TextBox>
                                                <asp:TextBox ID="txtNombreProd" Width="498px" 
                    MaxLength="100" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="btnBuscarProducto" 
                    runat="server" ImageUrl="~/Imagenes/BuscarProducto_A.jpg"
                                                    
                    onmouseout="this.src='../../Imagenes/Buscar_B.JPG';" onmouseover="this.src='../../Imagenes/Buscar_A.JPG';"
                                                    Visible="True" />
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                                                                    <asp:RadioButtonList ID="rdbCodNombre" 
                                runat="server" CssClass="Texto" AutoPostBack="True"
                                                                        RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1">Codigo</asp:ListItem>
                                                                        <asp:ListItem Value="2">Descripci�n</asp:ListItem>
                                                                        <asp:ListItem Value="3">Cod.Prov</asp:ListItem>
                                                                    </asp:RadioButtonList>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                        <asp:Panel ID="Panel_B" runat="server">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="DGV_B" runat="server" AutoGenerateColumns="False" 
                                            PageSize="10" Width="99%">
                                            <Columns>
                                                <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                                                <asp:TemplateField HeaderText="Codigo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblprodCodigo" runat="server" 
                                                            Text='<%#DataBinder.Eval(Container.DataItem,"prod_Codigo")%>'></asp:Label>
                                                        <asp:HiddenField ID="hddIdProducto" runat="server" 
                                                            Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="NomProducto" HeaderText="Descripcion" />
                                                <asp:BoundField DataField="Cod_Proveedor" HeaderText="Cod.Prov" />
                                                <asp:BoundField DataField="NomLinea" HeaderText="L�nea" />
                                                <asp:BoundField DataField="NomSubLinea" HeaderText="Sub L�nea" />
                                                <asp:BoundField DataField="NoVisible" HeaderText="Origen" />
                                            </Columns>
                                            <RowStyle CssClass="GrillaRow" />
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btn_anterior" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                            OnClientClick="return(valNavegacion('0'));" Style="cursor: hand;" />
                                        <asp:Button ID="btn_siguiente" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                            OnClientClick="return(valNavegacion('1'));" Style="cursor: hand;" />
                                        <asp:TextBox ID="txt_PageIndex" runat="server" Width="50px" 
                                            CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                                        <asp:Button ID="btn_Ir" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                                            OnClientClick="return(valNavegacion('2'));" Style="cursor: hand;" />
                                        <asp:TextBox ID="txt_PageIndexGO" Width="50px" runat="server" 
                                            onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                                                                    &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 84px">
                            </td>
                        <td style="height: 84px">
                            </td>
                        <td style="height: 84px">
                            </td>
                        <td style="height: 84px">
                            </td>
                        <td style="height: 84px">
                            </td>
                        <td style="height: 84px">
                            </td>
                        <td style="height: 84px">
                            </td>
                        <td style="height: 84px">
                            </td>
                    </tr>
                </table>
             </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                &nbsp;busqueda
            /td>
                   <asp:Panel ID="pnlbusqueda" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            &nbsp;Perfil:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboPerfil" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            &nbsp;Tipo Precio:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoPrecio" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            &nbsp; Condicion de Pago:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboCondPago" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            &nbsp;Medio Pago:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboMedPago" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:RadioButtonList ID="rbver" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="2">Todos</asp:ListItem>
                                                <asp:ListItem Value="1">Activo</asp:ListItem>
                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td colspan="2">
                                            <asp:RadioButtonList ID="rbtlPrecioBase" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Precio Lista" Value="PL" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Precio Comercial" Value="PC"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="90px" Style="cursor: hand;" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gvBusqueda" runat="server" AutoGenerateColumns="False" Width="100%"
                                    AllowPaging="True" HeaderStyle-Height="25px" RowStyle-Height="25px" PageSize="10"
                                    GridLines="Horizontal">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Editar">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="linkEditar" runat="server" OnClick="lkEditarClick" Text="Editar"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                            
                                                <asp:HiddenField ID="hdd_IdPerfil" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPerfil") %>' />
                                                <asp:HiddenField ID="hdd_IdTipoPV" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdtipoPV") %>' />
                                                <asp:HiddenField ID="hdd_IdCondicionPago" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCondicionPago") %>' />
                                                <asp:HiddenField ID="hdd_IdMedioPago" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMedioPago") %>' />
                                                <asp:HiddenField ID="hdd_IdTipoOperacion" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoOperacion") %>' />
                                                <asp:HiddenField ID="hdd_IdLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdLinea") %>' />
                                                <asp:HiddenField ID="hdd_IdPais" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPais") %>' />
                                                <asp:HiddenField ID="Hdd_IdUsuario" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdUsuario") %>' />
                                                <asp:HiddenField ID="hdd_User" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"NUsuario") %>' />
                                           
                                           
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="IdDescuento" HeaderText="Codigo" />
                                        <asp:BoundField DataField="Perfil" HeaderText="Perfil" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="TipoPrecioV" HeaderText="TipoPrecio" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="CondicionPago" HeaderText="Condicion Pago" />
                                        <asp:BoundField DataField="MedioPago" HeaderText="Medio Pago" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="desc_Porcentaje" DataFormatString="{0:F2}" HeaderText="Dscto Porcentaje"
                                        ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" />
                                        <asp:BoundField DataFormatString="{0:d}" HeaderText="Fecha Registro" DataField="desc_FechaRegistro" />
                                        <asp:BoundField DataField="desc_Estado" HeaderText="Estado" />
                                        <asp:BoundField DataField="Desc_PrecioBaseDscto" HeaderText="Precio Base Descuento" />
                                        <asp:BoundField DataField="NUsuario" HeaderText="Tipo Configuracion" />
                                          <asp:BoundField DataField="TipoOperacion" HeaderText="Tipo Operacion" />
                                  
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Left" CssClass="GrillaPager" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function validarGuardar() {
            var descuento = parseFloat(document.getElementById('<%=tbdescuento.CLientID %>').value);
            if (isNaN(descuento)) { descuento = 0; }

            if (descuento == 0) {
                alert('Ingrese un valor mayor a cero');
                return false;
            }

            return confirm('Desea, continuar ?');
        }
        //++++++++++++++++++++++++++
        function validarEliminar() {
            return confirm('Desea, eliminar el registro ?');
        }
    </script>

</asp:Content>

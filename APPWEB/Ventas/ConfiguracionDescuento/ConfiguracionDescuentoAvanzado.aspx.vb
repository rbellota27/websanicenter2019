﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class ConfiguracionDescuentoAvanzado
    Inherits System.Web.UI.Page

#Region "variables"
    Private objScript As New ScriptManagerClass
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
    End Enum
    Private objConfDescuento As Entidades.ConfiguracionDescuentoAvanzado
    Private objConfDescuentoAvanzado As Entidades.ConfiguracionDescuentoAvanzado
    Private drop As Combo


    Private Sub setDSUsuario(ByVal objDSBusuario1 As Object)
        Session.Remove("objDSUsuario")
        Session.Add("objDSUsuario", objDSBusuario1)
    End Sub

#End Region

#Region "Procedimientos"

    Private Sub CargarAlIniciar()
        drop = New Combo
        drop.LlenarCboCondicionPago(dlcondicion)
        drop.LlenarCboMedioPago(dlmedio)
        drop.LlenarCboPerfil(dlperfil)
        drop.LlenarCboTipoPV(dlprecio)
        'drop.llenarCboTipoOperacion(dlTipoOperacion)
        drop.LlenarCboPaisTodos(dlPais, True)
        'drop.LlenarCboLinea(dlLinea)
        'drop.llenarCboTipoExistencia(dlExistencia)
        drop.LlenarCboCondicionPago(cboCondPago)
        drop.LlenarCboMedioPago(cboMedPago, True)
        drop.LlenarCboPerfil(cboPerfil, True)
        drop.LlenarCboTipoPV(cboTipoPrecio)
        DGVUsuario.Visible = False

        cargarControles()

        Dim objCbo As New Combo

        With objCbo
            .llenarCboTipoOperacionxIdTpoDocumento(Me.dlTipoOperacion, CInt(14), False)
        End With


    End Sub

    Private Sub cargarControles()
        Dim nComboBox As New Negocio.ComboBox
        'nComboBox.llenarComboBoxUMedida(M_cmbUMedida)
        'nComboBox.llenarComboBoxTipoProd(M_cmbTipoProd)
        'Dim objCombo As New Combo
        'objCombo.LlenarCboMagnitud(Me.cmbMagnitud)


        'drop = New Combo
        drop.llenarCboTipoExistencia(dlExistencia)
        drop.llenarCboLineaxTipoExistencia(dlLinea, CInt(dlExistencia.SelectedValue), True)
        'drop.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cmbSubLinea_AddProd, CInt(cmbLinea_AddProd.SelectedValue), CInt(cbo_TipoExistencia.SelectedValue), True)
        'drop.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

    End Sub

    Private Sub verBotones(ByVal modo As String)
        Select Case modo
            Case "N"
                btNuevo.Visible = False
                btGuardar.Visible = True
                btCancelar.Visible = True
                pnlPrincipal.Enabled = True
                PanelSecundario.Enabled = True
                Panel_B.Enabled = True
                pnlbusqueda.Enabled = False
            Case "L"
                btNuevo.Visible = True
                btGuardar.Visible = False
                btCancelar.Visible = False
                pnlPrincipal.Enabled = False
                PanelSecundario.Enabled = False
                pnlbusqueda.Enabled = True
                Panel_B.Enabled = False
            Case "G"
                btNuevo.Visible = True
                btGuardar.Visible = False
                btCancelar.Visible = False
                pnlPrincipal.Enabled = False
                PanelSecundario.Enabled = False
                pnlbusqueda.Enabled = True
                Panel_B.Enabled = False

        End Select
    End Sub

    Private Sub MantenimientoAvanzado(ByVal tipo As Integer)
        objConfDescuentoAvanzado = New Entidades.ConfiguracionDescuentoAvanzado

        If CInt(dlTipoOperacion.SelectedValue) = 4 Then

            If TxtIdUsuario.Text = "" Then

                objScript.mostrarMsjAlerta(Me, "PARA ESTE TIPO DE OPERACION SE DEBE CONSIGNAR UN USUARIO ESPECIFICO. VERIFIQUE")
                Return

            End If
         
        End If

        If CInt(Me.dlLinea.SelectedValue) = 0 And CInt(Me.dlTipoOperacion.SelectedValue) = 4 Then


            objScript.mostrarMsjAlerta(Me, "PARA ESTE TIPO DE OPERACION SE DEBE CONSIGNAR UNA LINEA DE PRODUCTO")
            Return


        End If

        With objConfDescuentoAvanzado
            .IdDescuento = CInt(IIf(tbCodigo.Text = "", 0, tbCodigo.Text))
            .IdPerfil = CInt(dlperfil.SelectedValue)
            .IdCondicionPago = CInt(dlcondicion.SelectedValue)
            .IdMedioPago = CInt(dlmedio.SelectedValue)
            .IdTipoPV = CInt(dlprecio.SelectedValue)
            .desc_Porcentaje = CDec(tbdescuento.Text)
            .desc_Estado = rbestado.SelectedValue
            .Desc_PrecioBaseDscto = rbtPBD.SelectedValue
            .IdTipoOperacion = CInt(dlTipoOperacion.SelectedValue)

            If TxtIdUsuario.Text = "" Then
                .IdUsuario = 0
            End If
            If TxtIdUsuario.Text <> "" Then
                .IdUsuario = CInt(TxtIdUsuario.Text)
            End If

            If (CInt(dlExistencia.SelectedValue) > 0) Then
                .IdExistencia = CInt(dlExistencia.SelectedValue)
            End If

            If (CInt(dlExistencia.SelectedValue) = 0) Then
                .IdExistencia = CInt(0)
            End If


            If (CInt(dlLinea.SelectedValue) > 0) Then
                .IdLinea = CInt(dlLinea.SelectedValue)
            End If

            If (CInt(dlLinea.SelectedValue) = 0) Then
                .IdLinea = CInt(0)
            End If

            If TxtIdProducto.Text = "" Then
                .CodigoP = 0
            End If
            If TxtIdProducto.Text <> "" Then
                .CodigoP = CInt(Me.TxtIdProducto.Text)
            End If

            If (CInt(dlPais.SelectedValue) > 0) Then
                .idpais = CInt(dlPais.SelectedValue)
            End If

            If (CInt(dlPais.SelectedValue) = 0) Then
                .idpais = CInt(0)
            End If


            ' .CodigoP=CInt(Me.txt)



        End With

        Select Case tipo
            Case operativo.GuardarNuevo
                tbCodigo.Text = CStr((New Negocio.ConfiguracionDescuentoAvanzado).InsertConfiguracionDescuentoAvanzado(objConfDescuentoAvanzado))
                objScript.mostrarMsjAlerta(Me, "El registro ha sido guardado")
            Case operativo.Actualizar
                tbCodigo.Text = CStr((New Negocio.ConfiguracionDescuentoAvanzado).InsertConfiguracionDescuentoAvanzado(objConfDescuentoAvanzado))
                objScript.mostrarMsjAlerta(Me, "El registro ha sido Actualizado")
        End Select

        'llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(cboMedPago.SelectedValue), CInt(cboPerfil.SelectedValue), CInt(cboTipoPrecio.SelectedValue), CInt(cboCondPago.SelectedValue), rbtlPrecioBase.SelectedValue)
        llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(dlmedio.SelectedValue), CInt(dlperfil.SelectedValue), CInt(dlprecio.SelectedValue), CInt(dlcondicion.SelectedValue), rbtlPrecioBase.SelectedValue)
        verBotones("L")

    End Sub

    Private Sub Mantenimiento(ByVal tipo As Integer)
        objConfDescuento = New Entidades.ConfiguracionDescuentoAvanzado
        With objConfDescuento
            .IdDescuento = CInt(IIf(tbCodigo.Text = "", 0, tbCodigo.Text))
            .IdPerfil = CInt(dlperfil.SelectedValue)
            .IdCondicionPago = CInt(dlcondicion.SelectedValue)
            .IdMedioPago = CInt(dlmedio.SelectedValue)
            .IdTipoPV = CInt(dlprecio.SelectedValue)
            .desc_Porcentaje = CDec(tbdescuento.Text)
            .desc_Estado = rbestado.SelectedValue

            .Desc_PrecioBaseDscto = rbtPBD.SelectedValue
        End With

        Select Case tipo
            Case operativo.GuardarNuevo
                tbCodigo.Text = CStr((New Negocio.ConfiguracionDescuentoAvanzado).InsertConfiguracionDescuento(objConfDescuento))
                objScript.mostrarMsjAlerta(Me, "El registro ha sido guardado")
            Case operativo.Actualizar
                Dim update As Boolean = (New Negocio.ConfiguracionDescuentoAvanzado).UpdateConfiguracionDescuento(objConfDescuento)
                If update = True Then objScript.mostrarMsjAlerta(Me, "El registro ha sido actualizado")
        End Select

        'llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(cboMedPago.SelectedValue), CInt(cboPerfil.SelectedValue), CInt(cboTipoPrecio.SelectedValue), CInt(cboCondPago.SelectedValue), rbtlPrecioBase.SelectedValue)
        llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(dlmedio.SelectedValue), CInt(dlperfil.SelectedValue), CInt(dlprecio.SelectedValue), CInt(dlcondicion.SelectedValue), rbtlPrecioBase.SelectedValue)
        verBotones("L")

    End Sub




    Private Function getDataSource() As Object
        Return Session.Item("datasource")
    End Function

    Private Sub setDataSource(ByVal obj As Object)
        Session.Remove("datasource")
        Session.Add("datasource", obj)
    End Sub


    Private Sub llenarGrillaConfDescuento(ByVal ver As Integer, ByVal idMedPago As Integer, ByVal idperfil As Integer, ByVal idtipopv As Integer, ByVal idCondicionPago As Integer, ByVal desc_PrecioBaseDscto As String)
        gvBusqueda.DataSource = (New Negocio.ConfiguracionDescuentoAvanzado).SelectConfiguracionDescuentoAvanzado(ver, idMedPago, idperfil, idtipopv, idCondicionPago, desc_PrecioBaseDscto)
        gvBusqueda.DataBind()
        Me.setDataSource(gvBusqueda.DataSource)
    End Sub

    Private Sub LimpiarForm()
        tbdescuento.Text = ""
        tbCodigo.Text = ""
    End Sub

#End Region

#Region "Eventos Principales"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                CargarAlIniciar()
                ViewState.Add("operativo", operativo.Ninguno)
                verBotones("L")
                llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(dlmedio.SelectedValue), CInt(dlperfil.SelectedValue), CInt(dlprecio.SelectedValue), CInt(dlcondicion.SelectedValue), rbtlPrecioBase.SelectedValue)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



#End Region

#Region "Edicion "
    Protected Sub lkEditarClick(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lk As LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lk.NamingContainer, GridViewRow)

            Dim IdCondicionPago As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdCondicionPago"), HiddenField).Value)
            Dim IdMedioPago As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdMedioPago"), HiddenField).Value)
            Dim IdPerfil As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdPerfil"), HiddenField).Value)
            Dim IdTipoPV As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdTipoPV"), HiddenField).Value)
            Dim IdTipoOperacion As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdTipoOperacion"), HiddenField).Value)
            Dim IdPais As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdPais"), HiddenField).Value)
            Dim IdLinea As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdLinea"), HiddenField).Value)
            Dim IdUser As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdUsuario"), HiddenField).Value)
            Dim User As String = (CType(fila.Cells(1).FindControl("hdd_User"), HiddenField).Value)



            If Not dlcondicion.Items.FindByValue(CStr(IdCondicionPago)) Is Nothing Then
                dlcondicion.SelectedValue = CStr(IdCondicionPago)
            End If
            If Not dlmedio.Items.FindByValue(CStr(IdMedioPago)) Is Nothing Then
                dlmedio.SelectedValue = CStr(IdMedioPago)
            End If
            If Not dlperfil.Items.FindByValue(CStr(IdPerfil)) Is Nothing Then
                dlperfil.SelectedValue = CStr(IdPerfil)
            End If
            If Not dlprecio.Items.FindByValue(CStr(IdTipoPV)) Is Nothing Then
                dlprecio.SelectedValue = CStr(IdTipoPV)
            End If

            If Not dlTipoOperacion.Items.FindByValue(CStr(IdTipoOperacion)) Is Nothing Then
                dlTipoOperacion.SelectedValue = CStr(IdTipoOperacion)
            End If


            If Not dlPais.Items.FindByValue(CStr(IdPais)) Is Nothing Then
                dlPais.SelectedValue = CStr(IdPais)
            End If

            If Not dlLinea.Items.FindByValue(CStr(IdLinea)) Is Nothing Then
                dlLinea.SelectedValue = CStr(IdLinea)
            End If


            TxtIdUsuario.Text = CStr(IdUser)
            txtNombreUsuario.Text=User
            tbCodigo.Text = fila.Cells(2).Text
            tbdescuento.Text = fila.Cells(7).Text
            rbestado.SelectedValue = CStr(IIf(fila.Cells(9).Text = "Activo", "1", "0"))
            rbtPBD.SelectedValue = CStr(IIf(fila.Cells(10).Text = "Precio Lista", "PL", "PC"))
            ViewState.Add("operativo", operativo.Actualizar)
            verBotones("N")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


    Private Sub btNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btNuevo.Click
        Try
            LimpiarForm()
            verBotones("N")
            ViewState.Add("operativo", operativo.GuardarNuevo)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Try
            MantenimientoAvanzado(CInt(ViewState("operativo")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        Try
            LimpiarForm()
            verBotones("L")
            ViewState.Add("operativo", operativo.Ninguno)
            gvBusqueda.DataSource = Me.getDataSource()
            gvBusqueda.DataBind()
            DGVUsuario.Columns.Clear()
            DGVUsuario.Visible = False


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(cboMedPago.SelectedValue), CInt(cboPerfil.SelectedValue), CInt(cboTipoPrecio.SelectedValue), CInt(cboCondPago.SelectedValue), rbtlPrecioBase.SelectedValue)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBusqueda.PageIndexChanging
        Me.gvBusqueda.PageIndex = e.NewPageIndex
        Me.gvBusqueda.DataSource = Me.getDataSource
        Me.gvBusqueda.DataBind()
    End Sub



    Protected Sub dlperfil_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlperfil.SelectedIndexChanged

        Dim obj As New ScriptManagerClass
        Try
            Me.txtNombreUsuario.Text = ""
            Me.TxtIdUsuario.Text = ""
            Dim objUV As New Negocio.UsuarioView
            DGVUsuario.DataSource = objUV.SelectxEstadoxNombre_IdPerfil("", "1", CInt(dlperfil.SelectedValue))
            DGVUsuario.DataBind()
            Me.setDSUsuario(DGVUsuario.DataSource)
            If DGVUsuario.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try

    End Sub

    Protected Sub gvBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvBusqueda.SelectedIndexChanged
        Dim obj As New ScriptManagerClass
        Try
            Dim objUV As New Negocio.UsuarioView
            DGVUsuario.DataSource = objUV.SelectxEstadoxNombre_IdPerfil("", "1", CInt(dlperfil.SelectedValue))
            DGVUsuario.DataBind()
            Me.setDSUsuario(DGVUsuario.DataSource)
            If DGVUsuario.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try
    End Sub

    Private Sub DGVUsuario_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGVUsuario.PageIndexChanging

        Dim obj As New ScriptManagerClass
        Dim objUV As New Negocio.UsuarioView
        Me.DGVUsuario.PageIndex = e.NewPageIndex
        Me.DGVUsuario.DataSource = objUV.SelectxEstadoxNombre_IdPerfil("", "1", CInt(dlperfil.SelectedValue))
        Me.DGVUsuario.DataBind()

    End Sub

    Protected Sub btnBuscarPersona0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersona0.Click
        DGVUsuario.Visible = True

        Dim obj As New ScriptManagerClass
        Try
            Me.DGVUsuario.Visible = True
            Me.txtNombreUsuario.Text = ""
            Me.TxtIdUsuario.Text = ""

            Dim objUV As New Negocio.UsuarioView
            DGVUsuario.DataSource = objUV.SelectxEstadoxNombre_IdPerfil(txtTextoABuscar.Text, "1", CInt(dlperfil.SelectedValue))
            DGVUsuario.DataBind()
            Me.setDSUsuario(DGVUsuario.DataSource)

            If DGVUsuario.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try

    End Sub



    Protected Sub DGVUsuario_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVUsuario.SelectedIndexChanged

        cargarDatosusuario()

    End Sub

    Private Sub cargarDatosusuario()

        Try
            Dim objUsuario As Entidades.UsuarioView = (New Negocio.UsuarioView).SelectxIdPersona(CInt(DGVUsuario.SelectedRow.Cells(1).Text))
            Me.txtNombreUsuario.Text = objUsuario.Nombre
            Me.TxtIdUsuario.Text = objUsuario.IdPersona.ToString

            If TxtIdUsuario.Text <> "" Then
                DGVUsuario.Visible = False
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos")
        End Try
    End Sub

    Protected Sub dlExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlExistencia.SelectedIndexChanged
        'DGV_B.DataBind()
        'verFrmInicio()
        cargarDatosLinea(dlLinea)
        'cargarDatosSubLinea(M_B_cmbSubLinea, CInt(cmbTipoExistencia.SelectedValue), CInt(M_B_cmbLinea.SelectedValue))


    End Sub


    Private Sub cargarDatosLinea(ByVal comboBox As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxLinea(comboBox)
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectActivoxTipoExistencia(CInt(dlExistencia.SelectedValue))
        '  If hddModo.Value <> CStr(operativo.Insert) And hddModo.Value <> CStr(operativo.Update) Then
        Dim objLinea As New Entidades.Linea
        objLinea.Id = 0
        objLinea.IdTipoExistencia = 0
        objLinea.Descripcion = "-----"
        lista.Insert(0, objLinea)
        ' End If
        comboBox.DataSource = lista
        comboBox.DataTextField = "Descripcion"
        comboBox.DataValueField = "Id"
        comboBox.DataBind()
    End Sub


    'Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
    '    Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
    '    For i As Integer = 0 To dgvFiltroTipoTabla.Rows.Count - 1
    '        With dgvFiltroTipoTabla.Rows(i)
    '            Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.dgvFiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.dgvFiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
    '            lista.Add(obj)
    '        End With
    '    Next
    '    Return lista
    'End Function
    Private Sub mostrarMensaje(ByVal isVisible As Boolean, ByVal texto As String)
        lblMensaje.Visible = isVisible
        lblMensaje.Text = texto
    End Sub

    Private Sub BuscarMantenimientoProducto(ByVal tipoMov As Integer)
        BuscarMantenimientoProducto_Paginado(CInt(ViewState("_IdTipoExistencia")), CInt(ViewState("_IdLinea")), CInt(ViewState("_IdSubLinea")), CInt(ViewState("_Estado")), CStr(ViewState("_nombre")), CStr(ViewState("_codigo")), CStr(ViewState("_codprov")), tipoMov, DGV_B)
    End Sub

    Protected Sub btnBuscarProducto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarProducto.Click

        Try
            mostrarMensaje(False, "")

            Dim IdTipoExistencia As Integer = 0
            Dim idLinea As Integer = 0
            Dim idSubLinea As Integer = 0
            Dim idEstadoProd As Integer = 0
            Dim nombre As String = ""
            Dim codigo As String = ""
            Dim codprov As String = ""
            If IsNumeric(dlExistencia.SelectedValue) Then IdTipoExistencia = CInt(dlExistencia.SelectedValue)
            If IsNumeric(dlLinea.SelectedValue) Then idLinea = CInt(dlLinea.SelectedValue)

            'If IsNumeric(M_B_cmbSubLinea.SelectedValue) Then idSubLinea = CInt(M_B_cmbSubLinea.SelectedValue)
            'If IsNumeric(M_B_cmbEstado.SelectedValue) Then idEstadoProd = CInt(M_B_cmbEstado.SelectedValue)

            Select Case CStr(Me.rdbCodNombre.SelectedValue)
                Case "3"
                    codprov = txtNombreProd.Text.Trim
                Case "2"
                    nombre = txtNombreProd.Text.Trim
                Case "1"
                    codigo = txtNombreProd.Text.Trim
            End Select

            Dim Tb As List(Of Entidades.ProductoTipoTablaValor)

            ViewState.Add("_IdTipoExistencia", IdTipoExistencia)
            ViewState.Add("_IdLinea", idLinea)
            ViewState.Add("_IdSubLinea", idSubLinea)
            ViewState.Add("_Estado", idEstadoProd)
            ViewState.Add("_nombre", nombre)
            ViewState.Add("_codigo", codigo)
            ViewState.Add("_codprov", codprov)
            BuscarMantenimientoProducto(0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


  
    End Sub



    Private Sub BuscarMantenimientoProducto_Paginado(ByVal IdTipoExistencia As Integer, ByVal idLinea As Integer, _
                                                         ByVal idSubLinea As Integer, ByVal idEstadoProd As Integer, _
                                                         ByVal nombre As String, ByVal codigo As String, ByVal codprov As String, _
                                                         ByVal TipoMov As Integer, ByVal Grilla As GridView)


        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txt_PageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txt_PageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txt_PageIndexGO.Text) - 1)
        End Select

        Dim Tb As List(Of Entidades.ProductoTipoTablaValor)

        Dim lista As List(Of Entidades.GrillaProducto_M) = (New Negocio.Producto).BuscarProducto_PaginadoConsulta(IdTipoExistencia, idLinea, Me.txtNombreProd.Text.Trim, Me.TxtIdProducto.Text.Trim, Tb, index, Grilla.PageSize)
        If lista.Count > 0 Then
            Panel_B.Visible = True
            txt_PageIndex.Text = CStr(index + 1)
            Grilla.DataSource = lista
            Grilla.DataBind()
        Else
            objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
        End If

    End Sub

    Protected Sub btn_siguiente_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_siguiente.Click
        BuscarMantenimientoProducto(2)
    End Sub

    Protected Sub btn_anterior_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_anterior.Click
        BuscarMantenimientoProducto(1)
    End Sub

    Protected Sub btn_Ir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Ir.Click
        BuscarMantenimientoProducto(3)
    End Sub

    'Protected Sub DGV_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_B.SelectedIndexChanged
    '    cargarDatosProducto()
    'End Sub



    Private Sub cargarDatosProducto()


    End Sub

    Protected Sub DGV_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_B.SelectedIndexChanged

        Dim hddIdProducto As HiddenField = CType(DGV_B.SelectedRow.Cells(1).FindControl("hddIdProducto"), HiddenField)
        'llenarEntidades()
        Selectcamposproductos(CInt(hddIdProducto.Value))


        '  Me.txtNombreProd.Text = Me.DGV_B.Rows(Me.DGV_B.SelectedIndex).Cells(1).Text()

        '.Cells(0).Text()
        ' Me.TxtIdProducto.Text = Me.DGV_B.Rows(Me.DGV_B.SelectedIndex).Cells(1).Text

    End Sub
    Private Sub Selectcamposproductos(ByVal IdProducto As Integer)

        Dim objScript As New ScriptManagerClass
        Dim nProducto As New Negocio.Producto
        Dim objProd As Entidades.Producto = nProducto.ProductoMercaderiaSelectxIdProducto(IdProducto)

        If objProd IsNot Nothing Then

            Me.TxtIdProducto.Text = objProd.Id.ToString
            ' Me.TxtIdProducto.Text = objProd.Codigo.ToString
            Me.txtNombreProd.Text = objProd.Descripcion

        End If

    End Sub


End Class
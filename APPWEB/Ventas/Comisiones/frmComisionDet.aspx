<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmComisionDet.aspx.vb" Inherits="APPWEB.frmComisionDet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td>
                <asp:Button ID="btn_guardar" runat="server" Text="Guardar" Width="80px" OnClientClick="return ( onclick_Guardar() );" />
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                registrar detalle de comisi&oacute;n
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <table cellspacing="1" cellpadding="2">
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_empresa" runat="server" AutoPostBack="true" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_tienda" runat="server" Width="400px">
                            </asp:DropDownList>
                            <asp:CheckBox ID="ckb_Tienda" runat="server" Text="Aplicar la configuraci�n a todas las Tienda"
                                CssClass="Texto" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Tipo de Comis&iacute;on:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_tipocomision" runat="server" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                Asignar Comisi&oacute;n Por Productos
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pnl_ConfigProducto"
                                            CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="img_ConfigProducto"
                                            CollapseControlID="img_ConfigProducto" TextLabelID="lbl_configProducto" ImageControlID="img_ConfigProducto"
                                            CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                            CollapsedText="Asignar valores a la cuadricula de productos" ExpandedText="Asignar valores a la cuadricula de productos"
                                            ExpandDirection="Vertical" SuppressPostBack="true">
                                        </cc1:CollapsiblePanelExtender>
                                        <asp:Image ID="img_ConfigProducto" runat="server" Height="16px" />
                                        <asp:Label ID="lbl_configProducto" runat="server" Text="" CssClass="Label"></asp:Label>
                                        <asp:Panel ID="pnl_ConfigProducto" runat="server">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Valor de Comisi&oacute;n:
                                                    </td>
                                                    <td>
                                                        <table cellspacing="1" cellpadding="2">
                                                            <tr>
                                                                <td style="width: 210px">
                                                                    <asp:TextBox ID="txt_comision" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        CssClass="TextBox_ReadOnly" onFocus="return ( aceptarFoco(this) );"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="Button3" runat="server" Text="Aceptar" OnClientClick="return ( onclick_comision() );"
                                                                        Style="cursor: Hand;" ToolTip="Cambiar el valor de comision a la cuadricula de productos" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Tipo Calculo:
                                                    </td>
                                                    <td>
                                                        <table cellspacing="1" cellpadding="2">
                                                            <tr>
                                                                <td style="width: 210px">
                                                                    <asp:RadioButtonList ID="rdb_tipoaplicacion" runat="server" CssClass="Texto" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Selected="True" Value="P">Porcentaje(%)</asp:ListItem>
                                                                        <asp:ListItem Value="M">U. Monetaria</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="Button5" runat="server" Text="Aceptar" OnClientClick="return ( onclick_tipoCalculo() );"
                                                                        Style="cursor: Hand;" ToolTip="Cambiar el valor de comision a la cuadricula de productos" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Precio Base Comisi&oacute;n:
                                                    </td>
                                                    <td>
                                                        <table cellspacing="1" cellpadding="2">
                                                            <tr>
                                                                <td style="width: 210px">
                                                                    <asp:RadioButtonList ID="rdb_precioComision" runat="server" CssClass="Texto" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Selected="True" Value="PV">Venta Final</asp:ListItem>
                                                                        <asp:ListItem Value="PL">Precio Lista</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="Button6" runat="server" Text="Aceptar" OnClientClick="return ( onclick_precioBaseComision() );"
                                                                        Style="cursor: Hand;" ToolTip="Cambiar el valor de comision a la cuadricula de productos" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Incluye:
                                                    </td>
                                                    <td>
                                                        <table cellspacing="1" cellpadding="2" width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:CheckBox ID="ckbigv" runat="server" CssClass="Texto" Text="I.G.V. sobre el precio base de comisi�n" />
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Button ID="Button4" runat="server" Text="Aceptar" OnClientClick="return ( onclick_igv() );"
                                                                        Style="cursor: Hand;" ToolTip="Cambiar el IGV a la cuadricula de productos" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Fecha:
                                                    </td>
                                                    <td>
                                                        <table cellspacing="1" cellpadding="2">
                                                            <tr>
                                                                <td class="Texto" style="font-weight: bold; width: 80px">
                                                                    Inicio �
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txt_fechaini" runat="server" Width="90px" onblur="return(  valFecha_Blank(this) );"
                                                                        CssClass="TextBox_Fecha"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="CalendarExtender_txt_fechaini" runat="server" TargetControlID="txt_fechaini">
                                                                    </cc1:CalendarExtender>
                                                                </td>
                                                                <td class="Texto" style="font-weight: bold; width: 80px">
                                                                    Fin �
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txt_fechafin" runat="server" Width="90px" onblur="return(  valFecha_Blank(this) );"
                                                                        CssClass="TextBox_Fecha"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="CalendarExtender_txt_fechafin" runat="server" TargetControlID="txt_fechafin">
                                                                    </cc1:CalendarExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="Button1" runat="server" Text="Aceptar" OnClientClick="return ( onclick_Fecha() );"
                                                                        Style="cursor: Hand;" ToolTip="Cambiar la fechas a la cuadricula de productos" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Descuesto:
                                                    </td>
                                                    <td>
                                                        <table cellspacing="1" cellpadding="2">
                                                            <tr>
                                                                <td class="Texto" style="font-weight: bold; width: 80px">
                                                                    De �
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_montoini" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        CssClass="TextBox_ReadOnly" onFocus="return ( aceptarFoco(this) );"></asp:TextBox>
                                                                </td>
                                                                <td class="Texto" style="font-weight: bold; width: 80px">
                                                                    Hasta �
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txt_montofin" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        CssClass="TextBox_ReadOnly" onFocus="return ( aceptarFoco(this) );"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="Button2" runat="server" Text="Aceptar" OnClientClick="return ( onclick_descuento() );"
                                                                        Style="cursor: Hand;" ToolTip="Cambiar los montos de descuentos a la cuadricula de productos" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" valign="top">
                            <table>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="btn_buscarproductos" runat="server" ImageUrl="~/Imagenes/BuscarProducto_B.jpg"
                                            onmouseout="this.src='../../Imagenes/BuscarProducto_B.JPG';" onmouseover="this.src='../../Imagenes/BuscarProducto_A.JPG';"
                                            OnClientClick="return ( onCapa('capaBuscarProducto_AddProd') );" ToolTip="Adicionar registros a la cuadricula de productos" />
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btn_limpiarproductos" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                            onmouseout="this.src='../../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';"
                                            OnClientClick="return ( confirm('Esta seguro de continuar con la operaci�n. ?') );"
                                            TabIndex="209" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel_Producto" runat="server" ScrollBars="Vertical" Height="350px">
                    <asp:GridView ID="gv_productos" runat="server" AutoGenerateColumns="False" GridLines="None"
                        AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                        HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                            <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Producto" HeaderText="Descripci�n" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="U.M.">
                                <ItemTemplate>
                                    <asp:DropDownList ID="gv_ddl_um" runat="server" DataTextField="DescripcionCorto"
                                        DataValueField="Id" DataSource='<%# DataBinder.Eval(Container.DataItem,"getListaUnidadMedida") %>'
                                        SelectedValue='<%# DataBinder.Eval(Container.DataItem,"IdUnidadMedida") %>'>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="gv_hdd_IdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />
                                    <asp:HiddenField ID="gv_hdd_IdComisionDet" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdComisionDet") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Moneda">
                                <ItemTemplate>
                                    <asp:DropDownList ID="gv_ddl_moneda" runat="server" DataValueField="Id" DataTextField="Simbolo"
                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaMoneda") %>' SelectedValue='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>'>
                                    </asp:DropDownList>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo Calculo">
                                <ItemTemplate>
                                    <asp:DropDownList ID="gv_ddl_tipocalculo" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem,"TipoCalculoCom") %>'>
                                        <asp:ListItem Value="P">Porcentaje</asp:ListItem>
                                        <asp:ListItem Value="M">U. Monetaria</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comisi�n">
                                <ItemTemplate>
                                    <asp:TextBox ID="gv_txt_valorcomision" runat="server" Width="65px" Style="text-align: right"
                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                        Text='<%# DataBinder.Eval(Container.DataItem,"ValorComision","{0:F3}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dscto. Desde">
                                <ItemTemplate>
                                    <asp:TextBox ID="gv_txt_dsctodesde" runat="server" Width="65px" Style="text-align: right"
                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                        Text='<%# DataBinder.Eval(Container.DataItem,"Dcto_Inicio","{0:F3}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dscto Hasta">
                                <ItemTemplate>
                                    <asp:TextBox ID="gv_txt_dsctohasta" runat="server" Width="65px" Style="text-align: right"
                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                        Text='<%# DataBinder.Eval(Container.DataItem,"Dcto_Fin","{0:F3}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="F. Inicio">
                                <ItemTemplate>
                                    <asp:TextBox ID="gv_txt_fechaini" runat="server" Width="65px" onblur="return(  valFecha_Blank(this) );"
                                        Text='<%# DataBinder.Eval(Container.DataItem,"FechaInicio","{0:d}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="F. Fin">
                                <ItemTemplate>
                                    <asp:TextBox ID="gv_txt_fechafin" runat="server" Width="65px" onblur="return(  valFecha_Blank(this) );"
                                        Text='<%# DataBinder.Eval(Container.DataItem,"FechaFin","{0:d}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio Comisi�n">
                                <ItemTemplate>
                                    <asp:DropDownList ID="gv_ddl_precioComision" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem,"PrecioBaseComision") %>'>
                                        <asp:ListItem Value="PV">Venta Final</asp:ListItem>
                                        <asp:ListItem Value="PL">Precio Lista</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IGV.">
                                <ItemTemplate>
                                    <asp:CheckBox ID="gv_cbk_igv" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"IncluyeIgv") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EST.">
                                <ItemTemplate>
                                    <asp:CheckBox ID="gv_cbk_estado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button TabIndex="212" ID="btnAnterior_ComisionDet" runat="server" Font-Bold="true"
                                Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionComisionDet('0'));" />
                        </td>
                        <td>
                            <asp:Button TabIndex="213" ID="btnSiguiente_ComisionDet" runat="server" Font-Bold="true"
                                Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionComisionDet('1'));" />
                        </td>
                        <td>
                            <asp:TextBox TabIndex="214" ID="txtPageIndex_ComisionDet" Width="50px" ReadOnly="true"
                                CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button TabIndex="215" ID="btnIr_ComisionDet" runat="server" Font-Bold="true"
                                Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionComisionDet('2'));" />
                        </td>
                        <td>
                            <asp:TextBox TabIndex="216" ID="txtPageIndexGO_ComisionDet" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboPageSize" runat="server">
                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                <asp:ListItem Text="50" Value="50"></asp:ListItem>                                                      
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdUsuario" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="1" cellspacing="0">
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    DataValueField="Id" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarGrilla_AddProd">                                            
                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" TabIndex="204"
                                                ID="txtDescripcionProd_AddProd" runat="server" Width="300px" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this) );"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td class="Texto">
                                            C�d.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProd">   
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this) );" TabIndex="205"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                onmouseout="this.src='../../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../../Imagenes/Agregar_A.JPG';"
                                                OnClientClick="return(valAddProductos());" TabIndex="208" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                            </td>
                            <td>
                                <asp:CheckBox ID="chb_CargaMasiva" runat="server" CssClass="Texto" Font-Bold="true"
                                    onclick=" return (validarCargaMasiva(this)); " Text="Agregar los productos contenidos en la < L�nea / Sub L�nea > seleccionada." />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                            </td>
                            <td>
                                <asp:CheckBox ID="chb_SoloComisionDet" runat="server" CssClass="Texto" Font-Bold="true"
                                    Text="Agregar Productos registrados de la < Comisi�n > selecccionada." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="20">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chb_AddDetalle" runat="server" />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" OnClick="selectAll(this)" />
                                </HeaderTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Codigo" HeaderText="C�digo" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
		
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;

        function valKeyPressDescripcionProd(obj) {
            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }

        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {
            var chb_CargaMasiva = document.getElementById('<%=chb_CargaMasiva.ClientID %>');
            var chb_SoloComisionDet = document.getElementById('<%=chb_SoloComisionDet.ClientID %>');
            if (chb_SoloComisionDet.checked == true) {
                return true;
            }

            if (chb_CargaMasiva.checked == true) {
                var cmbSubLinea_AddProd = document.getElementById('<%=cmbSubLinea_AddProd.ClientID %>');

                if (cmbSubLinea_AddProd.value == '0') {
                    alert('NO HA SELECCIONADO UNA SUBL�NEA');
                    return false;
                }

            } else {

                var DGV_AddProd = document.getElementById('<%=DGV_AddProd.ClientID %>');
                var cont = 0;
                if (DGV_AddProd != null) {
                    for (var i = 1; i < DGV_AddProd.rows.length; i++) {
                        var rowElem = DGV_AddProd.rows[i];
                        if (rowElem.cells[0].children[0].cells[0].children[0].checked == true) {
                            cont = cont + 1;
                        }
                    }
                    if (cont == 0) {
                        alert('NO HA SELECCIONADO UN PRODUCTO.');
                        return false;
                    }
                } else {
                    alert('LA CUADRICULA DE PRODUCTOS NO CONTIENE REGISTROS');
                    return false;
                }
            }
            return true;
        }


        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function onclick_Fecha() {
            var txt_fechaini = document.getElementById('<%=txt_fechaini.ClientID %>');
            var txt_fechafin = document.getElementById('<%=txt_fechafin.ClientID %>');
            var gv_productos = document.getElementById('<%=gv_productos.ClientID %>');
            if (txt_fechaini.value == '') {
                alert('Ingrese una fecha de inicio');
                return false;
            }
            if (txt_fechafin.value == '') {
                alert('Ingrese una fecha de fin');
                return false;
            }
            if (compararFechas(txt_fechaini, txt_fechafin)) {
                alert('LA FECHA DE FIN NO PUEDE SER MENOR A LA FECHA DE INICIO.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }
            if (gv_productos != null) {
                for (var i = 1; i < gv_productos.rows.length; i++) {
                    var rowElem = gv_productos.rows[i];
                    rowElem.cells[9].children[0].value = txt_fechaini.value;
                    rowElem.cells[10].children[0].value = txt_fechafin.value;
                }
            }
            return false;
        }
        function onclick_descuento() {
            var txt_montoini = document.getElementById('<%=txt_montoini.ClientID %>');
            var txt_montofin = document.getElementById('<%=txt_montofin.ClientID %>');
            var gv_productos = document.getElementById('<%=gv_productos.ClientID %>');

            if (isNaN(txt_montoini.value) || txt_montoini.value.length == 0) {
                txt_montoini.value = 0;
            }

            if (isNaN(txt_montofin.value) || txt_montofin.value.length == 0) {
                txt_montofin.value = 0;
            }

            if (gv_productos != null) {
                for (var i = 1; i < gv_productos.rows.length; i++) {
                    var rowElem = gv_productos.rows[i];
                    rowElem.cells[7].children[0].value = parseFloat(txt_montoini.value);
                    rowElem.cells[8].children[0].value = parseFloat(txt_montofin.value);
                }
            }
            return false;
        }

        function onclick_comision() {
            var txt_comision = document.getElementById('<%=txt_comision.ClientID %>');
            var gv_productos = document.getElementById('<%=gv_productos.ClientID %>');

            if (isNaN(txt_comision.value) || txt_comision.value.length == 0) {
                txt_comision.value = 0;
            }

            if (gv_productos != null) {
                for (var i = 1; i < gv_productos.rows.length; i++) {
                    var rowElem = gv_productos.rows[i];
                    rowElem.cells[6].children[0].value = parseFloat(txt_comision.value);
                }
            }
            return false;
        }

        function onclick_igv() {
            var ckbigv = document.getElementById('<%=ckbigv.ClientID %>');
            var gv_productos = document.getElementById('<%=gv_productos.ClientID %>');

            if (gv_productos != null) {
                for (var i = 1; i < gv_productos.rows.length; i++) {
                    var rowElem = gv_productos.rows[i];
                    rowElem.cells[12].children[0].checked = ckbigv.checked;
                }
            }
            return false;

        }

        function onclick_tipoCalculo() {
            var radio_rdb_tipoaplicacion = document.getElementById('<%=rdb_tipoaplicacion.ClientID %>');
            var ctrl_radio_rdb_tipoaplicacion = radio_rdb_tipoaplicacion.getElementsByTagName('input');
            var gv_productos = document.getElementById('<%=gv_productos.ClientID %>');
            var tipo_aplicacion = '';
            for (var k = 0; k < ctrl_radio_rdb_tipoaplicacion.length; k++) {
                if (ctrl_radio_rdb_tipoaplicacion[k].checked == true) {
                    tipo_aplicacion = ctrl_radio_rdb_tipoaplicacion[k].value;
                    break;
                }
            }
            if (gv_productos != null) {
                for (var i = 1; i < gv_productos.rows.length; i++) {
                    var rowElem = gv_productos.rows[i];
                    rowElem.cells[5].children[0].value = tipo_aplicacion;
                }
            }
            return false;
        }
        //
        function onclick_precioBaseComision() {
            var radio_rdb_precioComision = document.getElementById('<%=rdb_precioComision.ClientID %>');
            var ctrl_radio_rdb_precioComision = radio_rdb_precioComision.getElementsByTagName('input');
            var gv_productos = document.getElementById('<%=gv_productos.ClientID %>');
            var tipo_precioBaseComision = '';

            for (var k = 0; k < ctrl_radio_rdb_precioComision.length; k++) {
                if (ctrl_radio_rdb_precioComision[k].checked == true) {
                    tipo_precioBaseComision = ctrl_radio_rdb_precioComision[k].value;
                    break;
                }
            }

            if (gv_productos != null) {
                for (var i = 1; i < gv_productos.rows.length; i++) {
                    var rowElem = gv_productos.rows[i];
                    rowElem.cells[11].children[0].value = tipo_precioBaseComision;
                }
            }
            return false;

        }
        //
        function onclick_Guardar() {
            var gv_productos = document.getElementById('<%=gv_productos.ClientID %>');
            if (gv_productos != null) {
                for (var i = 1; i < gv_productos.rows.length; i++) {
                    var rowElem = gv_productos.rows[i];

                    if (rowElem.cells[9].children[0].value.length == 0 || rowElem.cells[9].children[0].value == '') {
                        alert('INGRESE UNA FECHA DE INICIO PARA EL PRODUCTO ' + rowElem.cells[2].innerText + '.');
                        rowElem.cells[9].children[0].focus();
                        return false;
                    }
                    if (rowElem.cells[10].children[0].value.length == 0 || rowElem.cells[10].children[0].value == '') {
                        alert('INGRESE UNA FECHA DE INICIO PARA EL PRODUCTO ' + rowElem.cells[2].innerText + '.');
                        rowElem.cells[10].children[0].focus();
                        return false;
                    }
                    if (compararFechas(rowElem.cells[9].children[0], rowElem.cells[10].children[0])) {
                        alert('LA FECHA DE FIN NO PUEDE SER MENOR A LA FECHA DE INICIO.\nNO SE PERMITE LA OPERACI�N.');
                        rowElem.cells[10].children[0].focus();
                        rowElem.cells[10].children[0].select();
                        return false;
                    }

                }
            } else {
                alert('LA CUADRICULA DE PRODUCTOS NO CONTIENE REGISTROS');
                return false;
            }
            return (confirm('DESEA CONTINUAR CON LA OPERACI�N.?'));
        }
        //
        function compararFechas(fecha, fecha2) {
            var xMonth = fecha.value.substring(3, 5);
            var xDay = fecha.value.substring(0, 2);
            var xYear = fecha.value.substring(6, 10);
            var yMonth = fecha2.value.substring(3, 5);
            var yDay = fecha2.value.substring(0, 2);
            var yYear = fecha2.value.substring(6, 10);

            if (xYear > yYear) {
                return (true)
            }
            else {
                if (xYear == yYear) {
                    if (xMonth > yMonth) {
                        return (true)
                    }
                    else {
                        if (xMonth == yMonth) {
                            if (xDay > yDay)
                                return (true);
                            else
                                return (false);
                        }
                        else
                            return (false);
                    }
                }
                else
                    return (false);
            }
        }

        ///

        function selectAll(invoker) {
            var grillaTipoTablaConfigxValor = document.getElementById('<%=DGV_AddProd.ClientID %>');

            if (grillaTipoTablaConfigxValor != null) {
                for (var i = 1; i < grillaTipoTablaConfigxValor.rows.length; i++) {
                    var rowElem = grillaTipoTablaConfigxValor.rows[i];
                    rowElem.cells[0].children[0].cells[0].children[0].checked = invoker.checked;
                }
            }
            return false;

        }

        function valNavegacionComisionDet(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_ComisionDet.ClientID%>').value);
            var grilla = document.getElementById('<%=gv_productos.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_ComisionDet.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
            }
            return true;
        }

        function validarCargaMasiva(obj) {
            var chb_CargaMasiva = document.getElementById('<%=chb_CargaMasiva.ClientID %>');
            var chb_SoloComisionDet = document.getElementById('<%=chb_SoloComisionDet.ClientID %>');

            if (obj.id == chb_CargaMasiva.id) {
                chb_SoloComisionDet.checked = false;
            }

            if (obj.id == chb_SoloComisionDet.id) {
                chb_CargaMasiva.checked = false;
            }


        }
        
    </script>

</asp:Content>

﻿Imports System.IO
Imports Entidades
Imports Negocio
Public Class FrmComisionXSucursal
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.btnexportar)
        If Not IsPostBack Then
            Dim fechaActual As DateTime = Date.Today
            Dim fechaInicial As Date = New DateTime(fechaActual.Year, fechaActual.Month, 1)
            Dim fechaFinal As Date = New DateTime(fechaActual.Year, fechaActual.Month + 1, 1).AddDays(-1)
            Me.txtFechaInicio.Text = fechaInicial
            Me.txtFechaFin.Text = fechaFinal
        End If
    End Sub

    Private Sub exportar()
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        'grilla.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(Me.gv_DAtos)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=comisiones.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())

        Response.End()
    End Sub

    Private Sub btnexportar_Click(sender As Object, e As System.EventArgs) Handles btnexportar.Click
        Call exportar()
    End Sub

    Private Sub btnVer_Click(sender As Object, e As System.EventArgs) Handles btnVer.Click
        Dim lista As New List(Of be_comision)
        lista = (New bl_comison).comisionxSucursal(Me.txtFechaInicio.Text, Me.txtFechaFin.Text)
        Me.gv_DAtos.DataSource = lista
        Me.gv_DAtos.DataBind()
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmComisionXSucursal.aspx.vb" Inherits="APPWEB.FrmComisionXSucursal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="width:100%">
    <table>
        <tr>
            <td colspan="6" class="TituloCelda"><span>COMISIÓN DE VENTAS</span></td>
        </tr>
        <tr>
            <td class="Texto">Fecha Inicio:</td>
            <td>
            <asp:TextBox id="txtFechaInicio" runat="server" Text=""></asp:TextBox>
            <cc1:MaskedEditExtender ID="txtFechaInicio_Mask" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaInicio" />
            </td>
            <td class="Texto">Fecha Fin:</td>
            <td>
            <asp:TextBox id="txtFechaFin" runat="server" Text=""></asp:TextBox>
            <cc1:MaskedEditExtender ID="txtFechaFin_Mask" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFechaFin" />
            </td>
            <td>
            <asp:Button ID="btnVer" runat="server" Text="Buscar" CssClass="btnBuscar" OnClientClick="disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
            <asp:Button ID="btnexportar" runat="server" Text="Exportar a Excel" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="gv_DAtos" runat="server" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="nomComisionista" HeaderText="VENDEDOR" />
                                    <asp:BoundField DataField="nomtienda" HeaderText="TIENDA" />
                                    <asp:BoundField DataField="primerPorcPago" HeaderText="PAGO 1%" />
                                    <asp:BoundField DataField="segundoPorcPago" HeaderText="PAGO 0.5%" />
                                    <asp:BoundField DataField="subTotalPagoComision" HeaderText="TOTAL PAGO" />
                                    <asp:BoundField DataField="totalComisionPremium" HeaderText="TOTAL COMISION PREMIUM" />
                                    <asp:BoundField DataField="Pagoadicional" HeaderText="ADICIONAL" />
                                    <asp:BoundField DataField="pagoTotalComision" HeaderText="COMISIÓN" />
                                </Columns>
                            <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</asp:Content>

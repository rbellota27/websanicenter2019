﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmComisionista
    Inherits System.Web.UI.Page


    Private cbo As Combo
    Private ListaHelpComision As List(Of Entidades.Comisionista)
    Dim objScript As New ScriptManagerClass


    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Private Sub actualizarControles_BuscarPersona(ByVal opcionBuscarPersona As Integer)
        Me.pnlBusquedaPersona.Visible = False
        Me.Panel_BusquedaPersona_Usuario.Visible = False

        Select Case opcionBuscarPersona
            Case 0 '****************** GENERAL
                Me.pnlBusquedaPersona.Visible = True
            Case 1 '****************** EMPRESA / TIENDA
                Me.Panel_BusquedaPersona_Usuario.Visible = True
        End Select


    End Sub
    Private Sub LLenar_Combo_onLoad()
        cbo = New Combo
        With cbo

            .llenarCboComsionCab(ddl_tipocomision, CInt(hdd_IdTipoComision.Value))
            .LlenarCboEmpresaxIdUsuario(ddl_empresa, CInt(hddIdUsuario.Value), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(ddl_tienda, CInt(ddl_empresa.SelectedValue), CInt(hddIdUsuario.Value), False)
            .LlenarCboRol(Me.cboRol, False)

            .LlenarCboPropietario(Me.cboEmpresa_BuscarPersona, False)
            .llenarCboTiendaxIdEmpresa(Me.cboTienda_BuscarPersona, CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), False)
            .LlenarCboPerfil(Me.cboPerfil_BuscarPersona, False)
            .LlenarCboPerfil(ddl_Perfil)
            .LlenarCboRol(ddl_Rol)

            If Not IsNothing(Me.ddl_Rol.Items.FindByValue("3")) Then
                Me.ddl_Rol.SelectedValue = "3" ' *************** MAESTRO DE OBRA
            End If

            actualizarControles_BuscarPersona(CInt(Me.rdbTipoBusqueda_Persona.SelectedValue))

        End With
    End Sub
    Private Sub on_load()
        If Not IsPostBack Then
            hddIdUsuario.Value = CStr(Session("IdUsuario"))

            LLenar_Combo_onLoad()
            ConfigurarDatos()
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        on_load()
    End Sub
    '++++++++++++++++++++++++++++++++++++++++++++++++++ Page_Load   
    Private Sub onChange_ddl_empresa(ByVal idEmpresa As String)
        cbo = New Combo
        cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(ddl_tienda, CInt(idEmpresa), CInt(hddIdUsuario.Value), False)
    End Sub
    Private Sub ddl_empresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_empresa.SelectedIndexChanged
        onChange_ddl_empresa(ddl_empresa.SelectedValue)
    End Sub
    '++++++++++++++++++++++++++++++++++++++++++++++++++ ddl_empresa.SelectedIndexChanged
    Private Sub val_consulta_x_Tienda()
        If gv_Perfil.Rows.Count > 0 Or gv_Rol.Rows.Count > 0 Then
            ddl_empresa.Enabled = False
            ddl_tienda.Enabled = False
        Else
            ddl_empresa.Enabled = True
            ddl_tienda.Enabled = True
        End If
    End Sub


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)
        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub
    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub
    Private Sub cargarPersona(ByVal IdPersona As Integer, ByVal cargarPuntoPartida As Boolean)

        'Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        'If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        'Dim objCbo As New Combo
        'Me.txtNombre_Remitente.Text = objPersona.Descripcion
        'Me.txtIdRemitente.Text = CStr(objPersona.IdPersona)
        'Me.txtDNI_Remitente.Text = objPersona.Dni
        'Me.txtRUC_Remitente.Text = objPersona.Ruc
        'Me.hddIdRemitente.Value = CStr(objPersona.IdPersona)

        'If (cargarPuntoPartida) Then
        '    '**************** PARTIDA
        '    Dim ubigeo As String = objPersona.Ubigeo
        '    If (ubigeo.Trim.Length <= 0) Then
        '        ubigeo = "000000"
        '    End If

        '    Dim codDepto As String = ubigeo.Substring(0, 2)
        '    Dim codProv As String = ubigeo.Substring(2, 2)
        '    Dim codDist As String = ubigeo.Substring(4, 2)
        '    Me.cboDepto_Partida.SelectedValue = codDepto
        '    objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, codDepto)
        '    Me.cboProvincia_Partida.SelectedValue = codProv
        '    objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, codDepto, codProv)
        '    Me.cboDistrito_Partida.SelectedValue = codDist
        '    Me.txtDireccion_Partida.Text = objPersona.Direccion
        'End If

        'Me.GV_BusquedaAvanzado.DataSource = Nothing
        'Me.GV_BusquedaAvanzado.DataBind()
        'Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        'Me.GV_DocumentosReferencia_Find.DataBind()

        'objScript.offCapa(Me, "capaPersona")

    End Sub

    Private Sub cboEmpresa_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa_BuscarPersona.SelectedIndexChanged
        valOnChange_cboEmpresa_BuscarPersona()
    End Sub
    Private Sub valOnChange_cboEmpresa_BuscarPersona()
        Try

            cbo.llenarCboTiendaxIdEmpresa(Me.cboTienda_BuscarPersona, CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), False)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub btnBuscarPersona_Usuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersona_Usuario.Click
        buscarPersona_EmpresaTienda()
    End Sub
    Private Sub buscarPersona_EmpresaTienda()
        Try

            Me.GV_BuscarPersona_Usuario.DataSource = (New Negocio.Usuario).Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_Distinct(CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), CInt(Me.cboTienda_BuscarPersona.SelectedValue), CInt(Me.cboPerfil_BuscarPersona.SelectedValue))
            Me.GV_BuscarPersona_Usuario.DataBind()

            If (Me.GV_BuscarPersona_Usuario.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub rdbTipoBusqueda_Persona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbTipoBusqueda_Persona.SelectedIndexChanged
        valOnChange_rdbTipoBusqueda_Persona()
    End Sub
    Private Sub valOnChange_rdbTipoBusqueda_Persona()
        Try

            actualizarControles_BuscarPersona(CInt(Me.rdbTipoBusqueda_Persona.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Persona_General_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar_Persona_General.Click
        valOnClick_btnBuscar_Persona_General()
    End Sub
    Private Sub valOnClick_btnBuscar_Persona_General()
        Try

            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", Me.cboRol.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Perfil Persona"

    Private listaPerfilPersona As List(Of Entidades.Comisionista)

    Private Sub gv_Perfil0_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv_Perfil0.SelectedIndexChanged
        listaPerfilPersona = getListaPerfilPersona()
        listaPerfilPersona.RemoveAt(gv_Perfil0.SelectedIndex)
        gv_Perfil0.DataSource = listaPerfilPersona
        gv_Perfil0.DataBind()
    End Sub

    Private Sub btnAdd_Persona_Usuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd_Persona_Usuario.Click
        valOnClick_btnAdd_Persona_Usuario()
    End Sub
    Private Sub valOnClick_btnAdd_Persona_Usuario()
        Try

            listaPerfilPersona = getListaPerfilPersona()

            For i As Integer = 0 To Me.GV_BuscarPersona_Usuario.Rows.Count - 1
                If (CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("chb_Select"), CheckBox).Checked) Then

                    Dim IdPersona As Integer = CInt(CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("hddIdPersona"), HiddenField).Value)
                    Dim IdEmpresa As Integer = CInt(CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("hddIdEmpresa"), HiddenField).Value)
                    Dim Idtienda As Integer = CInt(CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("hddIdtienda"), HiddenField).Value)
                    Dim IdPerfil As Integer = CInt(CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("hddIdPerfil"), HiddenField).Value)

                    For x As Integer = 0 To Me.listaPerfilPersona.Count - 1
                        If (Me.listaPerfilPersona(x).IdPersona = IdPersona) Then
                            Throw New Exception("LA PERSONA < " + Me.listaPerfilPersona(x).Descripcion + " > YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                        End If
                    Next

                    objComision = (New Negocio.Comisionista).Comisionista_Select_Personas(CInt(ddl_tipocomision.SelectedValue), IdEmpresa, Idtienda, IdPersona, IdPerfil, Nothing)
                    With objComision
                        .Descripcion = HttpUtility.HtmlDecode(Me.GV_BuscarPersona_Usuario.Rows(i).Cells(2).Text)
                        .Perfil = HttpUtility.HtmlDecode(Me.GV_BuscarPersona_Usuario.Rows(i).Cells(4).Text + " " + Me.GV_BuscarPersona_Usuario.Rows(i).Cells(5).Text + " " + Me.GV_BuscarPersona_Usuario.Rows(i).Cells(6).Text)
                        .Configurado = True
                    End With


                    Me.listaPerfilPersona.Add(objComision)

                End If

            Next


            Me.gv_Perfil0.DataSource = Me.listaPerfilPersona
            Me.gv_Perfil0.DataBind()

            objScript.onCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function getListaPerfilPersona() As List(Of Entidades.Comisionista)
        listaPerfilPersona = New List(Of Entidades.Comisionista)

        For i As Integer = 0 To Me.gv_Perfil0.Rows.Count - 1
            objComision = New Entidades.Comisionista
            With objComision
                .IdComisionCab = CInt(ddl_tipocomision.SelectedValue)

                .IdPersona = CInt(CType(Me.gv_Perfil0.Rows(i).FindControl("gv_lbl_Idpersona_perfil0"), Label).Text)
                .Descripcion = HttpUtility.HtmlDecode(Me.gv_Perfil0.Rows(i).Cells(2).Text)
                .IdEmpresa = CInt(CType(Me.gv_Perfil0.Rows(i).FindControl("gv_hdd_idempresa_perfil0"), HiddenField).Value)
                .IdTienda = CInt(CType(Me.gv_Perfil0.Rows(i).FindControl("gv_hdd_idtienda_perfil0"), HiddenField).Value)
                .IdPerfil = CInt(CType(Me.gv_Perfil0.Rows(i).FindControl("gv_hdd_idperfil_perfil0"), HiddenField).Value)
                .Perfil = HttpUtility.HtmlDecode(Me.gv_Perfil0.Rows(i).Cells(3).Text)
                .valPorcentaje = CDec(CType(Me.gv_Perfil0.Rows(i).FindControl("gv_txt_valComision_perfil0"), TextBox).Text)
                .baseComision = CType(Me.gv_Perfil0.Rows(i).FindControl("gv_ddl_baseComision_perfil0"), DropDownList).SelectedValue
                .IncluyeIgv = CType(Me.gv_Perfil0.Rows(i).FindControl("gv_ckb_igv_perfil0"), CheckBox).Checked
                .Estado = CType(Me.gv_Perfil0.Rows(i).FindControl("gv_ckb_est_Perfil0"), CheckBox).Checked

                Dim strValPorcentaje As String = CType(Me.gv_Perfil0.Rows(i).FindControl("gv_txt_valComision_perfil0"), TextBox).Text
                If strValPorcentaje = "" Then
                    .valPorcentaje = Decimal.Zero
                Else
                    .valPorcentaje = CDec(strValPorcentaje)
                End If

                .Configurado = True
            End With

            listaPerfilPersona.Add(objComision)

        Next

        Return listaPerfilPersona
    End Function

#End Region

#Region "Rol Persona"

    Private listaRolPersona As List(Of Entidades.Comisionista)

    Private Sub gv_Rol0_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv_Rol0.SelectedIndexChanged
        listaRolPersona = getListaRollPersona()
        listaRolPersona.RemoveAt(gv_Rol0.SelectedIndex)
        gv_Rol0.DataSource = listaRolPersona
        gv_Rol0.DataBind()
    End Sub

    Private Sub btnAgregar_Persona_General_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregar_Persona_General.Click
        valOnClick_btnAgregar_Persona_General()
    End Sub
    Private Sub valOnClick_btnAgregar_Persona_General()
        Try

            listaRolPersona = getListaRollPersona()

            For i As Integer = 0 To Me.gvBuscar.Rows.Count - 1
                If (CType(Me.gvBuscar.Rows(i).FindControl("chb_Select"), CheckBox).Checked) Then

                    Dim IdPersona As Integer = CInt(CType(Me.gvBuscar.Rows(i).FindControl("hddIdPersona"), HiddenField).Value)
                    Dim IdEmpresa As Integer = CInt(cboEmpresa_BuscarPersona.SelectedValue)
                    Dim Idtienda As Integer = CInt(cboTienda_BuscarPersona.SelectedValue)
                    cboRol.SelectedValue = CStr(ViewState("rol"))
                    Dim IdRol As Integer = CInt(cboRol.SelectedValue)

                    For x As Integer = 0 To Me.listaRolPersona.Count - 1
                        If (Me.listaRolPersona(x).IdPersona = IdPersona) Then
                            Throw New Exception("LA PERSONA < " + Me.listaRolPersona(x).Descripcion + " > YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                        End If
                    Next

                    objComision = (New Negocio.Comisionista).Comisionista_Select_Personas(CInt(ddl_tipocomision.SelectedValue), IdEmpresa, Idtienda, IdPersona, Nothing, IdRol)
                    With objComision
                        .Descripcion = HttpUtility.HtmlDecode(Me.gvBuscar.Rows(i).Cells(2).Text)
                        .IdRol = IdRol
                        .Rol = HttpUtility.HtmlDecode("[ " + cboEmpresa_BuscarPersona.SelectedItem.Text + " ] [ " + Me.cboTienda_BuscarPersona.SelectedItem.Text + " ] [" + Me.cboRol.SelectedItem.Text + " ]")
                        .Configurado = True
                    End With


                    Me.listaRolPersona.Add(objComision)

                End If

            Next


            Me.gv_Rol0.DataSource = Me.listaRolPersona
            Me.gv_Rol0.DataBind()

            objScript.onCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function getListaRollPersona() As List(Of Entidades.Comisionista)
        listaRolPersona = New List(Of Entidades.Comisionista)

        For i As Integer = 0 To Me.gv_Rol0.Rows.Count - 1
            objComision = New Entidades.Comisionista
            With objComision
                .IdComisionCab = CInt(ddl_tipocomision.SelectedValue)

                .IdPersona = CInt(CType(Me.gv_Rol0.Rows(i).FindControl("gv_lbl_Idpersona_rol0"), Label).Text)
                .Descripcion = HttpUtility.HtmlDecode(Me.gv_Rol0.Rows(i).Cells(2).Text)
                .IdEmpresa = CInt(CType(Me.gv_Rol0.Rows(i).FindControl("gv_hdd_idempresa_rol0"), HiddenField).Value)
                .IdTienda = CInt(CType(Me.gv_Rol0.Rows(i).FindControl("gv_hdd_idtienda_rol0"), HiddenField).Value)
                .IdRol = CInt(CType(Me.gv_Rol0.Rows(i).FindControl("gv_hdd_idrol_rol0"), HiddenField).Value)
                .Rol = HttpUtility.HtmlDecode(Me.gv_Rol0.Rows(i).Cells(3).Text)
                .valPorcentaje = CDec(CType(Me.gv_Rol0.Rows(i).FindControl("gv_txt_valComision_rol0"), TextBox).Text)
                .baseComision = CType(Me.gv_Rol0.Rows(i).FindControl("gv_ddl_baseComision_rol0"), DropDownList).SelectedValue
                .IncluyeIgv = CType(Me.gv_Rol0.Rows(i).FindControl("gv_ckb_igv_rol0"), CheckBox).Checked
                .Estado = CType(Me.gv_Rol0.Rows(i).FindControl("gv_ckb_est_rol0"), CheckBox).Checked

                Dim strValPorcentaje As String = CType(Me.gv_Rol0.Rows(i).FindControl("gv_txt_valComision_rol0"), TextBox).Text
                If strValPorcentaje = "" Then
                    .valPorcentaje = Decimal.Zero
                Else
                    .valPorcentaje = CDec(strValPorcentaje)
                End If

                .Configurado = True
            End With

            listaRolPersona.Add(objComision)

        Next

        Return listaRolPersona
    End Function
#End Region

#Region "Add Perfil"
    Dim ListaComisionPerfil As List(Of Entidades.Comisionista)
    Dim objComision As Entidades.Comisionista

    Private Sub gv_Perfil_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv_Perfil.SelectedIndexChanged
        ListaComisionPerfil = getComisionPerfil()
        ListaComisionPerfil.RemoveAt(gv_Perfil.SelectedIndex)

        gv_Perfil.DataSource = ListaComisionPerfil
        gv_Perfil.DataBind()
        val_consulta_x_Tienda()
    End Sub

    Protected Sub btn_AddPerfil_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_AddPerfil.Click
        llenarPefil()
        val_consulta_x_Tienda()
    End Sub
    Private Sub llenarPefil()
        ListaComisionPerfil = getComisionPerfil()
        objComision = New Entidades.Comisionista

        With objComision
            .IdEmpresa = CInt(ddl_empresa.SelectedValue)
            .IdTienda = CInt(ddl_tienda.SelectedValue)
            .IdComisionCab = CInt(ddl_tipocomision.SelectedValue)
            .IdPerfil = CInt(ddl_Perfil.SelectedValue)
            .Perfil = CStr(ddl_Perfil.SelectedItem.Text)
            .valPorcentaje = Decimal.Zero
            .baseComision = "PV"
            .IncluyeIgv = False
            .Estado = True

            ListaHelpComision = (New Negocio.Comisionista).Comisionista_Select_Personas_Lista(.IdComisionCab, .IdEmpresa, .IdTienda, .IdPerfil, Nothing, False)
            If ListaHelpComision.Count > 0 Then
                .valPorcentaje = ListaHelpComision(0).valPorcentaje
                .baseComision = ListaHelpComision(0).baseComision
                .IncluyeIgv = ListaHelpComision(0).IncluyeIgv
                .TipoCalculo = ListaHelpComision(0).TipoCalculo
                .Estado = ListaHelpComision(0).Estado
            End If
            .Configurado = False
        End With



        ListaComisionPerfil.Add(objComision)

        gv_Perfil.DataSource = ListaComisionPerfil
        gv_Perfil.DataBind()
    End Sub
    Private Function getComisionPerfil() As List(Of Entidades.Comisionista)
        ListaComisionPerfil = New List(Of Entidades.Comisionista)

        For Each row As GridViewRow In gv_Perfil.Rows
            objComision = New Entidades.Comisionista
            With objComision
                .IdEmpresa = CInt(ddl_empresa.SelectedValue)
                .IdTienda = CInt(ddl_tienda.SelectedValue)
                .IdComisionCab = CInt(ddl_tipocomision.SelectedValue)

                .IdPerfil = CInt(CType(row.FindControl("gv_hdd_idperfil"), HiddenField).Value)
                .Perfil = CStr(CType(row.FindControl("gv_lbl_perfil"), Label).Text)

                Dim strValPorcentaje As String = CType(row.FindControl("gv_txt_valComision_perfil"), TextBox).Text
                If strValPorcentaje = "" Then
                    .valPorcentaje = Decimal.Zero
                Else
                    .valPorcentaje = CDec(strValPorcentaje)
                End If

                .baseComision = CStr(CType(row.FindControl("gv_ddl_baseComision_perfil"), DropDownList).SelectedValue)
                .IncluyeIgv = CBool(CType(row.FindControl("gv_ckb_igv_perfil"), CheckBox).Checked)
                .Estado = CBool(CType(row.FindControl("gv_ckb_est_Perfil"), CheckBox).Checked)
                .Configurado = False
                .TipoCalculo = CBool(CType(row.FindControl("gv_dl_TipoCalculo"), DropDownList).SelectedValue)
            End With
            ListaComisionPerfil.Add(objComision)
        Next
        Return ListaComisionPerfil
    End Function

#End Region

#Region "Add Rol"

    Private Sub gv_Rol_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv_Rol.SelectedIndexChanged
        ListaComsionRol = getComisionRol()
        ListaComsionRol.RemoveAt(gv_Rol.SelectedIndex)

        gv_Rol.DataSource = ListaComsionRol
        gv_Rol.DataBind()
        val_consulta_x_Tienda()
    End Sub

    Protected Sub btn_AddRol_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_AddRol.Click
        llenarRol()
        val_consulta_x_Tienda()
    End Sub

    Dim ListaComsionRol As List(Of Entidades.Comisionista)

    Private Sub llenarRol()
        ListaComsionRol = getComisionRol()

        objComision = New Entidades.Comisionista
        With objComision
            .IdEmpresa = CInt(ddl_empresa.SelectedValue)
            .IdTienda = CInt(ddl_tienda.SelectedValue)
            .IdComisionCab = CInt(ddl_tipocomision.SelectedValue)
            .IdRol = CInt(ddl_Rol.SelectedValue)
            .Rol = CStr(ddl_Rol.SelectedItem.Text)
            .valPorcentaje = Decimal.Zero
            .baseComision = "SP"
            .IncluyeIgv = False
            .Estado = True

            ListaHelpComision = (New Negocio.Comisionista).Comisionista_Select_Personas_Lista(.IdComisionCab, .IdEmpresa, .IdTienda, Nothing, .IdRol, False)
            If ListaHelpComision.Count > 0 Then
                .valPorcentaje = ListaHelpComision(0).valPorcentaje
                .baseComision = ListaHelpComision(0).baseComision
                .IncluyeIgv = ListaHelpComision(0).IncluyeIgv
                .Estado = ListaHelpComision(0).Estado
            End If

            .Configurado = False
        End With

        ListaComsionRol.Add(objComision)

        gv_Rol.DataSource = ListaComsionRol
        gv_Rol.DataBind()
    End Sub
    Private Function getComisionRol() As List(Of Entidades.Comisionista)

        ListaComsionRol = New List(Of Entidades.Comisionista)

        For Each row As GridViewRow In gv_Rol.Rows
            objComision = New Entidades.Comisionista
            With objComision
                .IdEmpresa = CInt(ddl_empresa.SelectedValue)
                .IdTienda = CInt(ddl_tienda.SelectedValue)
                .IdComisionCab = CInt(ddl_tipocomision.SelectedValue)

                .IdRol = CInt(CType(row.FindControl("gv_hdd_idrol"), HiddenField).Value)
                .Rol = CStr(CType(row.FindControl("gv_lbl_rol"), Label).Text)

                Dim strValPorcentaje As String = CType(row.FindControl("gv_txt_valComision_rol"), TextBox).Text
                If strValPorcentaje = "" Then
                    .valPorcentaje = Decimal.Zero
                Else
                    .valPorcentaje = CDec(strValPorcentaje)
                End If

                .baseComision = CStr(CType(row.FindControl("gv_ddl_baseComision_rol"), DropDownList).SelectedValue)
                .IncluyeIgv = CBool(CType(row.FindControl("gv_ckb_igv_rol"), CheckBox).Checked)
                .Estado = CBool(CType(row.FindControl("gv_ckb_est_Rol"), CheckBox).Checked)
                .Configurado = False
            End With
            ListaComsionRol.Add(objComision)
        Next

        Return ListaComsionRol
    End Function

#End Region

#Region "Registrar Perfil o Rol Masivo"
    Private Sub btn_guardarPerfil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_guardarPerfil.Click
        onClick_btn_guardarPerfil()
    End Sub   ' Guardar Por Perfil
    Private Sub onClick_btn_guardarPerfil()
        Try
            Dim IdComisionPerfil As Integer = 0
            ListaComisionPerfil = getComisionPerfil()

            If ckb_tienda.Checked = True Then
                For i As Integer = 0 To ListaComisionPerfil.Count - 1
                    ListaComisionPerfil(i).IdTienda = 0
                Next
            End If

            If ckbTodos.Checked = True Then
                For i As Integer = 0 To ListaComisionPerfil.Count - 1
                    ListaComisionPerfil(i).Configurado = True
                Next
            End If

            IdComisionPerfil = (New Negocio.Comisionista).Comisionista_Insert_por_grupo(ListaComisionPerfil)

            If IdComisionPerfil <> 0 Then
                objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btn_guardarRol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_guardarRol.Click
        onClick_btn_guardarRol()
    End Sub      ' Guardar Por Rol
    Private Sub onClick_btn_guardarRol()
        Try

            Dim IdComisionRol As Integer = 0
            ListaComsionRol = getComisionRol()

            If ckb_tienda.Checked = True Then
                For i As Integer = 0 To ListaComsionRol.Count - 1
                    ListaComsionRol(i).IdTienda = 0
                Next
            End If
            If ckbTodos.Checked = True Then
                For i As Integer = 0 To ListaComsionRol.Count - 1
                    ListaComsionRol(i).Configurado = Nothing
                Next
            End If

            IdComisionRol = (New Negocio.Comisionista).Comisionista_Insert_por_grupo(ListaComsionRol)

            If IdComisionRol <> 0 Then
                objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#End Region


    Private Sub btn_guardar_x_Persona_Perfil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_guardar_x_Persona_Perfil.Click
        onclick_btn_guardar_x_Persona_Perfil()
    End Sub

    Private Sub onclick_btn_guardar_x_Persona_Perfil()
        Dim IdPersona As Integer = 0
        Try
            listaPerfilPersona = getListaPerfilPersona()
            IdPersona = (New Negocio.Comisionista).Comisionista_Insert_por_grupo_Persona(listaPerfilPersona)

            If IdPersona <> 0 Then
                objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btn_guardar_x_Persona_Rol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_guardar_x_Persona_Rol.Click
        onclick_btn_guardar_x_Persona_Rol()
    End Sub

    Private Sub onclick_btn_guardar_x_Persona_Rol()
        Dim IdPersona As Integer = 0
        Try
            listaRolPersona = getListaRollPersona()
            IdPersona = (New Negocio.Comisionista).Comisionista_Insert_por_grupo_Persona(listaRolPersona)

            If IdPersona <> 0 Then
                objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "Limpiar"
    Private Sub btn_cancelarPerfil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancelarPerfil.Click
        gv_Perfil.DataBind()
        val_consulta_x_Tienda()
    End Sub

    Private Sub btn_cancelarRol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancelarRol.Click
        gv_Rol.DataBind()
        val_consulta_x_Tienda()
    End Sub

    Private Sub btn_cancelarPerfil0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancelarPerfil0.Click
        gv_Perfil0.DataBind()
    End Sub

    Private Sub btn_cancelarRol0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancelarRol0.Click
        gv_Rol0.DataBind()
    End Sub
#End Region

End Class
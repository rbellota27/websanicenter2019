<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmComisionista.aspx.vb" Inherits="APPWEB.frmComisionista" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                comisi&oacute;n pOR Persona
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Tipo de Comis&iacute;on:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_tipocomision" runat="server" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td class="TituloCelda" style="font-weight: bold">
                Asignaci�n grupal
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_empresa" runat="server" AutoPostBack="true" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_tienda" runat="server" Width="400px">
                            </asp:DropDownList>
                            <asp:CheckBox ID="ckb_tienda" runat="server" Text="Aplicar a todas las tiendas de la [ Empresa ] seleccionada."
                                CssClass="Texto" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:CheckBox ID="ckbTodos" runat="server" Text="Nivelar comisiones incluyendo personas las cuales fueron asignadas individualmente"
                                CssClass="Texto" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%">
                    <tr>
                        <td class="SubTituloCelda">
                            Perfil
                        </td>
                        <td class="SubTituloCelda">
                            ROL
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddl_Perfil" runat="server">
                            </asp:DropDownList>
                            <asp:Button ID="btn_AddPerfil" runat="server" Text="Agregar" Width="80px" Style="cursor: hand;"
                                OnClientClick="return ( onclick_btn_AddPerfil() );" />
                            <asp:Button ID="btn_guardarPerfil" runat="server" Text="Guardar" Width="80px" OnClientClick="return ( onclick_GuardarPerfil() );"
                                Style="cursor: hand;" />
                            <asp:Button ID="btn_cancelarPerfil" runat="server" Text="Cancelar" Width="80px" OnClientClick="return ( confirm('DESEA CANCELAR LA OPERACI�N.') );"
                                Style="cursor: hand;" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Rol" runat="server" Enabled="false">
                            </asp:DropDownList>
                            <asp:Button ID="btn_AddRol" runat="server" Text="Agregar" Width="80px" Style="cursor: hand;"
                                OnClientClick="return ( onclick_btn_AddRol() );" />
                            <asp:Button ID="btn_guardarRol" runat="server" Text="Guardar" Width="80px" OnClientClick="return ( onclick_GuardarRol() );"
                                Style="cursor: hand;" />
                            <asp:Button ID="btn_cancelarRol" runat="server" Text="Cancelar" Width="80px" OnClientClick="return ( confirm('DESEA CANCELAR LA OPERACI�N.') );"
                                Style="cursor: hand;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%" valign="top">
                            <br />
                            <asp:GridView ID="gv_Perfil" runat="server" AutoGenerateColumns="False" GridLines="None"
                                AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow"
                                Width="99%">
                                <RowStyle CssClass="GrillaRow"></RowStyle>
                                <Columns>
                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Perfil">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="gv_hdd_idPerfil" runat="server" Value='<%# DataBinder.Eval(Container.dataItem,"IdPerfil") %>' />
                                            <asp:Label ID="gv_lbl_perfil" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Perfil") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comision (%)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gv_txt_valComision_perfil" runat="server" Style="text-align: right"
                                                onKeypress="return(validarNumeroPuntoPositivo('event'));" Width="70px" onFocus="return ( aceptarFoco(this) );"
                                                Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"valPorcentaje","{0:F3}") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Precio Comision">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="gv_ddl_baseComision_perfil" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem,"baseComision") %>'>
                                                <asp:ListItem Value="PV">Venta Final</asp:ListItem>
                                                <asp:ListItem Value="PL">Precio Lista</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="I.G.V.">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gv_ckb_igv_perfil" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"IncluyeIgv") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comisi�n">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="gv_dl_TipoCalculo" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem,"TipoCalculo") %>'>
                                                <asp:ListItem Value="False">Ventas Personales</asp:ListItem>
                                                <asp:ListItem Value="True">Ventas por Tienda</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estado">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gv_ckb_est_Perfil" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="GrillaSelectedRow"></SelectedRowStyle>
                                <HeaderStyle CssClass="GrillaCabecera"></HeaderStyle>
                                <EditRowStyle CssClass="GrillaEditRow"></EditRowStyle>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating"></AlternatingRowStyle>
                            </asp:GridView>
                            <br />
                        </td>
                        <td style="width: 50%" valign="top">
                            <br />
                            <asp:GridView ID="gv_Rol" runat="server" AutoGenerateColumns="False" GridLines="None"
                                AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow"
                                Width="99%">
                                <RowStyle CssClass="GrillaRow"></RowStyle>
                                <Columns>
                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Rol">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="gv_hdd_idrol" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdRol") %>' />
                                            <asp:Label ID="gv_lbl_rol" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Rol") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comision (%)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gv_txt_valComision_rol" runat="server" Style="text-align: right"
                                                onKeypress="return(validarNumeroPuntoPositivo('event'));" Width="70px" onFocus="return ( aceptarFoco(this) );"
                                                Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"valPorcentaje","{0:F3}") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Precio Comision">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="gv_ddl_baseComision_rol" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem,"baseComision") %>'>
                                                <asp:ListItem Value="SP">Sobre Precio</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="I.G.V.">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gv_ckb_igv_Rol" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"IncluyeIgv") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estado">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gv_ckb_est_Rol" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="GrillaSelectedRow"></SelectedRowStyle>
                                <HeaderStyle CssClass="GrillaCabecera"></HeaderStyle>
                                <EditRowStyle CssClass="GrillaEditRow"></EditRowStyle>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating"></AlternatingRowStyle>
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Asignacion individual
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Button3" runat="server" Text="Buscar Personas" OnClientClick="return ( mostrarCapaPersona() );" />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                perfil
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_guardar_x_Persona_Perfil" runat="server" Text="Guardar" Width="80px"
                    OnClientClick="return ( btn_guardar_x_Persona_Perfil() );" />
                <asp:Button ID="btn_cancelarPerfil0" runat="server" Text="Cancelar" Width="80px"
                    OnClientClick="return ( confirm('DESEA CANCELAR LA OPERACI�N.') );" Style="cursor: hand;" />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="gv_Perfil0" runat="server" AutoGenerateColumns="False" GridLines="None"
                    AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                    HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow"
                    Width="99%">
                    <RowStyle CssClass="GrillaRow"></RowStyle>
                    <Columns>
                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                        <asp:TemplateField HeaderText="C�digo">
                            <ItemTemplate>
                                <asp:Label ID="gv_lbl_Idpersona_perfil0" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"IdPersona") %>'></asp:Label>
                                <asp:HiddenField ID="gv_hdd_idempresa_perfil0" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>' />
                                <asp:HiddenField ID="gv_hdd_idtienda_perfil0" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>' />
                                <asp:HiddenField ID="gv_hdd_idperfil_perfil0" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPerfil") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" />
                        <asp:BoundField DataField="Perfil" HeaderText="Empresa - Tienda - Perfil" />
                        <asp:TemplateField HeaderText="Comision (%)">
                            <ItemTemplate>
                                <asp:TextBox ID="gv_txt_valComision_perfil0" runat="server" Style="text-align: right"
                                    onKeypress="return(validarNumeroPuntoPositivo('event'));" Width="70px" onFocus="return ( aceptarFoco(this) );"
                                    Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"valPorcentaje","{0:F3}") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio Comision">
                            <ItemTemplate>
                                <asp:DropDownList ID="gv_ddl_baseComision_perfil0" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem,"baseComision") %>'>
                                    <asp:ListItem Value="PV">Venta Final</asp:ListItem>
                                    <asp:ListItem Value="PL">Precio Lista</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="I.G.V.">
                            <ItemTemplate>
                                <asp:CheckBox ID="gv_ckb_igv_perfil0" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"IncluyeIgv") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estado">
                            <ItemTemplate>
                                <asp:CheckBox ID="gv_ckb_est_Perfil0" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="GrillaSelectedRow"></SelectedRowStyle>
                    <HeaderStyle CssClass="GrillaCabecera"></HeaderStyle>
                    <EditRowStyle CssClass="GrillaEditRow"></EditRowStyle>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating"></AlternatingRowStyle>
                </asp:GridView>
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                rol
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_guardar_x_Persona_Rol" runat="server" Text="Guardar" Width="80px"
                    OnClientClick="return ( btn_guardar_x_Persona_Rol() );" />
                <asp:Button ID="btn_cancelarRol0" runat="server" Text="Cancelar" Width="80px" OnClientClick="return ( confirm('DESEA CANCELAR LA OPERACI�N.') );"
                    Style="cursor: hand;" /><br />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gv_Rol0" runat="server" AutoGenerateColumns="False" GridLines="None"
                    AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                    HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow"
                    Width="99%">
                    <RowStyle CssClass="GrillaRow"></RowStyle>
                    <Columns>
                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                        <asp:TemplateField HeaderText="C�digo">
                            <ItemTemplate>
                                <asp:Label ID="gv_lbl_Idpersona_rol0" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"IdPersona") %>'></asp:Label>
                                <asp:HiddenField ID="gv_hdd_idempresa_rol0" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>' />
                                <asp:HiddenField ID="gv_hdd_idtienda_rol0" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>' />
                                <asp:HiddenField ID="gv_hdd_idrol_rol0" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"idRol") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" />
                        <asp:BoundField DataField="Rol" HeaderText="Empresa - Tienda - Rol" />
                        <asp:TemplateField HeaderText="Comision (%)">
                            <ItemTemplate>
                                <asp:TextBox ID="gv_txt_valComision_rol0" runat="server" Style="text-align: right"
                                    onKeypress="return(validarNumeroPuntoPositivo('event'));" Width="70px" onFocus="return ( aceptarFoco(this) );"
                                    Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"valPorcentaje","{0:F3}") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio Comision">
                            <ItemTemplate>
                                <asp:DropDownList ID="gv_ddl_baseComision_rol0" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem,"baseComision") %>'>
                                    <asp:ListItem Value="SP">Sobre Precio</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="I.G.V.">
                            <ItemTemplate>
                                <asp:CheckBox ID="gv_ckb_igv_Rol0" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"IncluyeIgv") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estado">
                            <ItemTemplate>
                                <asp:CheckBox ID="gv_ckb_est_Rol0" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="GrillaSelectedRow"></SelectedRowStyle>
                    <HeaderStyle CssClass="GrillaCabecera"></HeaderStyle>
                    <EditRowStyle CssClass="GrillaEditRow"></EditRowStyle>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating"></AlternatingRowStyle>
                </asp:GridView>
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_IdTipoComision" runat="server" Value="1" />
                <asp:HiddenField ID="hddIdUsuario" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 4px; width: 900px;
        height: auto; position: absolute; top: 170px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_capaPersona" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButtonList ID="rdbTipoBusqueda_Persona" runat="server" CssClass="Texto"
                                Font-Bold="true" RepeatDirection="Horizontal" AutoPostBack="true">
                                <asp:ListItem Value="0">General</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">Empresa/Tienda/Perfil</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                        Empresa:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboEmpresa_BuscarPersona" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                        Tienda:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTienda_BuscarPersona" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlBusquedaPersona" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td colspan="5">
                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                            AutoPostBack="false">
                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                            <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto">
                                                        Raz�n Social / Nombres:
                                                    </td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                            Width="450px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto">
                                                        D.N.I.:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td class="Texto">
                                                        R.U.C.:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                            MaxLength="11"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btnBuscar_Persona_General" runat="server" Text="Buscar" Width="80px"
                                                                        ToolTip="Buscar" />
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnAgregar_Persona_General" runat="server" Text="Agregar" Width="80px"
                                                                        ToolTip="Agregar" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto">
                                                        Rol:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboRol" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:Label ID="lbl_aviso_persona" runat="server" Text="La < Empresa > y  < Tienda > selecccionada son utilizadas para la asignacion de la comisi�n."
                                                            CssClass="LabelRojo"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                PageSize="20" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chb_Select" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="IdPersona" HeaderText="C�d." NullDisplayText="0" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre / Raz�n Social" NullDisplayText="---"
                                                        HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                                    <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_BusquedaPersona_Usuario" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                                        Perfil:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboPerfil_BuscarPersona" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarPersona_Usuario" runat="server" Text="Buscar" Width="80px"
                                                            ToolTip="Buscar" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAdd_Persona_Usuario" runat="server" Text="Agregar" Width="80px"
                                                            ToolTip="Agregar los comisionistas seleccionados." />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_BuscarPersona_Usuario" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                PageSize="30" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chb_Select" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                                        <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTienda")%>' />
                                                                        <asp:HiddenField ID="hddIdPerfil" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPerfil")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="IdPersona" HeaderText="C�digo" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Dni" HeaderText="D.N.I." HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                                    <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Perfil" HeaderText="Perfil" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAdd_Persona_Usuario" />
                <asp:PostBackTrigger ControlID="btnAgregar_Persona_General" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">

        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;

        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //

        function onclick_GuardarPerfil() {
            var gv_Perfil = document.getElementById('<%=gv_Perfil.ClientID %>');
            if (gv_Perfil != null) {
                for (var i = 1; i < gv_Perfil.rows.length; i++) {
                    var rowElem = gv_Perfil.rows[i];
                    var cont = 0;
                    for (var k = 1; k < gv_Perfil.rows.length; k++) {
                        var rowElemX = gv_Perfil.rows[k];
                        if (rowElemX.cells[1].children[0].value == rowElem.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert('Ha seleccionado el mismo perfil [ ' + rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text + ' ].');
                            return false;
                        }
                    }
                }
            } else {
                alert('LA CUADRICULA DE PERFIL NO TIENE REGISTROS.');
                return false;
            }
            return confirm('DESEA CONTINUAR CON LA OPERACI�N.');
        }
        //
        function onclick_GuardarRol() {
            var gv_Rol = document.getElementById('<%=gv_Rol.ClientID %>');
            if (gv_Rol != null) {
                for (var i = 1; i < gv_Rol.rows.length; i++) {
                    var rowElem = gv_Rol.rows[i];
                    var cont = 0;
                    for (var k = 1; k < gv_Rol.rows.length; k++) {
                        var rowElemX = gv_Rol.rows[k];
                        if (rowElemX.cells[1].children[0].value == rowElem.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert('Ha seleccionado el mismo rol [ ' + rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text + ' ].');
                            return false;
                        }
                    }
                }
            } else {
                alert('LA CUADRICULA DE ROL NO TIENE REGISTROS.');
                return false;
            }

            return confirm('DESEA CONTINUAR CON LA OPERACI�N.');
        }
        //
        function onclick_btn_AddPerfil() {
            var ddl_Perfil = document.getElementById('<%=ddl_Perfil.ClientID %>');
            var gv_Perfil = document.getElementById('<%=gv_Perfil.ClientID %>');
            if (gv_Perfil != null) {
                for (var i = 1; i < gv_Perfil.rows.length; i++) {
                    var rowElem = gv_Perfil.rows[i];
                    if (rowElem.cells[1].children[0].value == ddl_Perfil.value) {
                        alert('el elemento [ ' + ddl_Perfil.options[ddl_Perfil.selectedIndex].text + ' ] esta en la lista.');
                        return false;
                    }
                }
            }
        }
        //
        function onclick_btn_AddRol() {
            var ddl_Rol = document.getElementById('<%=ddl_Rol.ClientID %>');
            var gv_Rol = document.getElementById('<%=gv_Rol.ClientID %>');
            if (gv_Rol != null) {
                for (var i = 1; i < gv_Rol.rows.length; i++) {
                    var rowElem = gv_Rol.rows[i];
                    if (rowElem.cells[1].children[0].value == ddl_Rol.value) {
                        alert('el elemento [ ' + ddl_Rol.options[ddl_Rol.selectedIndex].text + ' ] esta en la lista.');
                        return false;
                    }
                }
            }
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            return false;
        }
        //
        function btn_guardar_x_Persona_Perfil() {
            var gv_Perfil0 = document.getElementById('<%=gv_Perfil0.ClientID %>');
            if (gv_Perfil0 != null) {
            } else {
                alert('LA CUADRICULA DE PERFIL - PERSONA NO TIENE REGISTROS.');
                return false;
            }
            return confirm('DESEA CONTINUAR CON LA OPERACI�N.');
        }
        //
        function btn_guardar_x_Persona_Rol() {
            var gv_Rol0 = document.getElementById('<%=gv_Rol0.ClientID %>');
            if (gv_Rol0 != null) {
            } else {
                alert('LA CUADRICULA DE ROL - PERSONA NO TIENE REGISTROS.');
                return false;
            }
            return confirm('DESEA CONTINUAR CON LA OPERACI�N.');

        }
        
    </script>

</asp:Content>

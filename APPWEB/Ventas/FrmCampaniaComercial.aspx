<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmCampaniaComercial.aspx.vb" Inherits="APPWEB.FrmCampaniaComercial" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" Width="80px" runat="server" Text="Nuevo" ToolTip="Nueva Campa�ia" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( valOnClick_btnGuardar()  );" Width="80px"
                                runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar" Width="80px" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="width: 100%" align="right">
                            <table>
                                <tr>
                                    <td class="Texto">
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                CAMPA�A COMERCIAL
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="70px" ToolTip="Buscar" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Descripci�n:
                        </td>
                        <td>
                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                ID="txtDescripcionCampania" Width="400px" Font-Bold="true" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Inicio:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                        Fin:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Estado:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEstado" runat="server" Width="100%">
                                <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tablaTiendaRef" runat="server">
                    <tr>
                        <td class="SubTituloCelda">
                            Aplicar a las tiendas
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td class="Texto">
                                        Tienda:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTiendaRef" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnAddTiendaRef" runat="server" Text="Agregar" ToolTip="Agregar las tiendas a replicar la campa�a."
                                            OnClientClick=" return ( onclick_AddTiendaRef() ); " />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_CampaniaRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="btnQuitarTienda" runat="server" OnClick="btnQuitarTienda_Click">Quitar</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Height="25px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Campania_Detalle" runat="server">
                    <table width="100%">
                        <tr>
                            <td class="SubTituloCelda">
                                LISTA DE PRODUCTOS EN CAMPA�A
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Precios por:
                                        </td>
                                        <td colspan="3">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoPV" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnBuscarProducto1" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                                            OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='../../Imagenes/BuscarProducto_b.JPG';"
                                                            onmouseover="this.src='../../Imagenes/BuscarProducto_A.JPG';" Style="text-align: justify" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Descuento:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDescuento" runat="server" Font-Bold="true" onblur=" return( valBlurClear('0',event) ); "
                                                onFocus=" return( aceptarFoco(this) ); " onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                Width="70px" Text="0"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Aplicar como:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rdb_OpcionDcto" runat="server" CssClass="Texto" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="0">Porcentaje (%)</asp:ListItem>
                                                <asp:ListItem Value="1">Unidad Monetaria</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAceptar_Dcto" runat="server" Text="Aceptar" OnClientClick=" return( valOnClick_btnAceptar_Dcto() ); "
                                                ToolTip="Aplicar descuento" Width="70px" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Cantidad M�nima de Venta:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidadMinima_Venta" runat="server" Font-Bold="true" onblur=" return( valBlurClear('0',event) ); "
                                                onFocus=" return( aceptarFoco(this) ); " onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAceptar_CantidadMin" runat="server" OnClientClick=" return( valOnClick_btnAceptar_CantidadMin() ); "
                                                Text="Aceptar" ToolTip="Cantidad M�nima de Venta para acceder a la Campa�a" Width="70px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Campania_Detalle" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="true" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblCodigoProducto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CodigoProducto") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Producto" HeaderStyle-HorizontalAlign="Center" HeaderText="Producto"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            HeaderText="Precio Actual ( Comercial )">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMoneda_PrecioActual" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Moneda_PrecioActual") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblPrecioActual" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"PrecioActual","{0:F3}") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            HeaderText="U.M.">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboUnidadMedida" runat="server" DataValueField="Id" DataTextField="DescripcionCorto"
                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"getListaUnidadMedida") %>'>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow"
                                            HeaderStyle-ForeColor="Blue" ItemStyle-HorizontalAlign="Center" HeaderText="Precio">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboMoneda" runat="server" DataValueField="Id" DataTextField="Simbolo"
                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaMoneda") %>'>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrecioCampania" TabIndex="200" runat="server" Width="70px" onblur=" return( valBlurClear('0',event)  ); "
                                                                onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); " onFocus=" return( aceptarFoco(this)  ); "
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Precio","{0:F3}") %>'></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow"
                                            HeaderStyle-ForeColor="Blue" ItemStyle-HorizontalAlign="Center" HeaderText="Cantidad M�n.">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtCantidadMin" TabIndex="210" runat="server" Width="70px" onblur=" return( valBlurClear('0',event)  ); "
                                                                onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); " onFocus=" return( aceptarFoco(this)  ); "
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"CantidadMin","{0:F3}") %>'></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Grilla" runat="server">
                    <asp:GridView ID="GV_Campania_Find" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEditar" OnClick="valOnClick_btnEditar" runat="server">Editar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="per_NComercial" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="tie_Nombre" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:TemplateField HeaderText="Descripcion" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDescripcion" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"cam_Descripcion") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCampania") %>' />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="cam_FechaInicio" HeaderText="Fecha Inicio" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="cam_FechaFin" HeaderText="Fecha Fin" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:TemplateField HeaderText="Estado" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chb_Estado" Enabled="false" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"cam_Estado") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdCampania" runat="server" Value="" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    DataValueField="Id" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarGrilla_AddProd">                                            
                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" TabIndex="204"
                                                ID="txtDescripcionProd_AddProd" runat="server" Width="300px" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this) );"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td class="Texto">
                                            C�d.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProd">    
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this) );" TabIndex="205"></asp:TextBox>
                                                </asp:Panel>  
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                onmouseout="this.src='../../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../../Imagenes/Agregar_A.JPG';"
                                                OnClientClick="return(valAddProductos());" TabIndex="208" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                &nbsp;
                            </td>
                            <td>
                                <asp:CheckBox ID="chb_CargaMasiva" runat="server" CssClass="Texto" Font-Bold="true"
                                    Text="Agregar los productos contenidos en la < L�nea / Sub L�nea > seleccionada." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="20">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkcttvMasivo" runat="server" onclick="selectAll(this)" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chb_AddDetalle" runat="server" />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Codigo" HeaderText="C�digo" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultar_Campania" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_capaConsultar_Campania" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton15" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa2('capaConsultar_Campania')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_Campania_Cab" runat="server" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Campa�a" DataField="cam_Descripcion" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Inicio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblFechaInicio" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cam_FechaInicio","{0:dd/MM/yyyy}")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fin" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblFechaFin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cam_FechaFin","{0:dd/MM/yyyy}")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Empresa" DataField="per_NComercial" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Tienda" DataField="tie_Nombre" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnCerrar_capaConsultar_Campania" runat="server" Text="Cerrar" Width="70px"
                                OnClientClick="return(  offCapa2('capaConsultar_Campania')   );" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">

        function valKeyPressCodigoSL() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(obj) {
            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {


            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd() {
            if (event.keyCode == 13) {
                document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        //***************************************************** BUSQUEDA PERSONA

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }


        function valOnClick_btnAceptar_Dcto() {

            var rdbOpcionDcto = document.getElementById('<%=rdb_OpcionDcto.ClientID %>');
            var txtDescuento = document.getElementById('<%=txtDescuento.ClientID %>');

            var descuento = parseFloat(txtDescuento.value);
            if (isNaN(descuento)) { descuento = 0; }

            var opcionDcto = 0;
            if (rdbOpcionDcto.cells[0].children[0].status) {
                opcionDcto = 0;
            } else {
                opcionDcto = 1;
            }


            var grilla = document.getElementById('<%=GV_Campania_Detalle.ClientID %>');

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {

                    var rowElem = grilla.rows[i];

                    var lblPrecioActual = rowElem.cells[3].children[0].cells[1].children[0];
                    var precioActual = parseFloat(lblPrecioActual.innerHTML);
                    if (isNaN(precioActual)) { precioActual = 0; }

                    var txtPrecioCampania = rowElem.cells[5].children[0].cells[1].children[0];

                    var precioCampania = 0;

                    if (opcionDcto == 0) {
                        //************* PORCENTAJE
                        precioCampania = precioActual * (1 - descuento / 100);
                    } else {
                        //************** VALOR MONETARIO
                        precioCampania = precioActual - descuento;
                    }

                    //*********** PRINT
                    txtPrecioCampania.value = redondear(precioCampania, 3);

                }
            }


            return false;
        }

        function valOnClick_btnGuardar() {
            var txtDescripcion = document.getElementById('<%=txtDescripcionCampania.ClientID %>');
            var grilla = document.getElementById('<%=GV_Campania_Detalle.ClientID %>');

            if (txtDescripcion.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtDescripcion.select();
                txtDescripcion.focus();
                return false;
            }

            if (grilla == null) {
                alert('NO SE INGRESARON PRODUCTOS A LA CAMPA�A. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            return confirm('Desea continuar con la Operaci�n ?');
        }

        function valOnClick_btnAceptar_CantidadMin() {


            var txtCantidadMinima_Venta = document.getElementById('<%=txtCantidadMinima_Venta.ClientID %>');
            var cantidadMin = parseFloat(txtCantidadMinima_Venta.value);
            if (isNaN(cantidadMin)) { cantidadMin = 0; }

            var grilla = document.getElementById('<%=GV_Campania_Detalle.ClientID %>');

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {

                    var rowElem = grilla.rows[i];

                    var txtCantidadMin_GV = rowElem.cells[6].children[0].cells[0].children[0];

                    txtCantidadMin_GV.value = redondear(cantidadMin, 3);

                }
            }

            return false;
        }
        //
        function selectAll(invoker) {
            var DGV_AddProd = document.getElementById('<%=DGV_AddProd.ClientID %>');

            if (DGV_AddProd != null) {
                for (var i = 1; i < DGV_AddProd.rows.length; i++) {
                    var rowElem = DGV_AddProd.rows[i];
                    rowElem.cells[0].children[0].cells[0].children[0].checked = invoker.checked;
                }
            }
            return false;

        }
        //
        function onclick_AddTiendaRef() {
            var IdTienda = document.getElementById('<%=cboTienda.ClientID %>');
            var IdTiendaRef = document.getElementById('<%=cboTiendaRef.ClientID %>');
            if (IdTienda.value == IdTiendaRef.value) {
                return false;
            }
            return true;
        }
        
        
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>

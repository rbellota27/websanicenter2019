<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmOrdenDespacho.aspx.vb" Inherits="APPWEB.FrmOrdenDespacho" %>
<%--<%@ Register assembly="Enlasys.WebControls" namespace="Enlasys.Web.UI.WebControls" tagprefix="Enlasys" %>--%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <asp:UpdatePanel ID="UpdatePanel_Base" runat="server"><ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td class="TituloCelda" style="text-align: center">
                        <asp:Label ID="Label87" runat="server" CssClass="LabelBlanco" 
                            Text="Orden de Despacho"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_BotonesControl" runat="server">
                            <asp:ImageButton ID="btnGuardar" runat="server" CausesValidation="true" 
                                ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(validarSave());" 
                                onmouseout="this.src='/Imagenes/Guardar_B.JPG';" 
                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" />
                            <asp:ImageButton ID="btnBuscar" runat="server" CausesValidation="false" 
                                ImageUrl="~/Imagenes/Buscar_b.JPG"
                                onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                            <asp:ImageButton ID="btnEditar" ImageUrl="~/Imagenes/Editar_B.JPG"   runat="server" ToolTip="Editar"
                            onmouseout="this.src='/Imagenes/Editar_B.JPG';" 
                                onmouseover="this.src='/Imagenes/Editar_A.JPG';"/>
                            <asp:ImageButton ID="btnAnular" ImageUrl="~/Imagenes/Anular_B.JPG" runat="server"
                            OnClientClick="return(valAnular());"
                            onmouseout="this.src='/Imagenes/Anular_B.JPG';" 
                                onmouseover="this.src='/Imagenes/Anular_A.JPG';" ToolTip="Anular"
                            
                             />
                            <asp:ImageButton ID="btnImprimir" runat="server" 
                                ImageUrl="~/Imagenes/Imprimir_B.JPG" 
                                OnClientClick="return(     print()       );" 
                                onmouseout="this.src='/Imagenes/Imprimir_B.JPG';" 
                                onmouseover="this.src='/Imagenes/Imprimir_A.JPG';" />
                            <asp:ImageButton ID="btnAtras" runat="server" 
                                ImageUrl="~/Imagenes/Arriba_B.JPG" 
                                OnClientClick="return(confirm('Desea retroceder?'));" 
                                onmouseout="this.src='/Imagenes/Arriba_B.JPG';" 
                                onmouseover="this.src='/Imagenes/Arriba_A.JPG';" ToolTip="Atr�s" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Cabecera" runat="server" Enabled="true">
                        <table>
                            <tr>
                                <td style="text-align: right">
                                    <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Empresa:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="cmbEmpresa" Enabled="true" runat="server" DataTextField="NombreComercial" DataValueField="Id" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Tienda:"></asp:Label>
                                    <asp:DropDownList ID="cmbTienda" runat="server" DataTextField="Nombre"  
                                        DataValueField="Id" AutoPostBack="True" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <asp:Label ID="Label21" runat="server" CssClass="Label" Text="Tipo Documento:"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList Enabled="false" ID="cmbTipoDocumento" runat="server" AutoPostBack="true" >
                                    <asp:ListItem Value="21" Selected="True">Orden de Despacho</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label ID="Label77" runat="server" CssClass="Label" Text="N� Serie:"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True" 
                                        Font-Bold="True" Width="56px">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label35" runat="server" CssClass="Label" Text="N�"></asp:Label>
                                    <asp:TextBox ID="txtCodigoDocumento" runat="server" ReadOnly="true"  onKeypress="return(onKeyPressEsNumero('event'));"
                                        Font-Bold="True" Width="104px" CssClass="TextBox_ReadOnly"></asp:TextBox>
    <asp:ImageButton ID="btnBuscarDocumento" runat="server" ImageUrl="~/Imagenes/Busqueda_b.JPG"
    onmouseover="this.src='/Imagenes/Busqueda_A.JPG';"  OnClientClick="return(validarBuscarDoc());"
    onmouseout="this.src='/Imagenes/Busqueda_b.JPG';" style="width: 27px"/>
                         
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label54" runat="server" CssClass="Label" 
                                        Text="Estado del Documento"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="cboEstadoDocumento" Enabled="false" runat="server" CssClass="LabelRojo" >
                                    <asp:ListItem Selected="True" Value="1">V�lido</asp:ListItem>
                                    </asp:DropDownList>                                    
                                    <asp:Label ID="Label88" runat="server" CssClass="Label" Text="Fecha Emisi�n:"></asp:Label>
                                    <asp:TextBox CssClass="TextBox_Fecha" Font-Bold="true" Width="90px" ID="txtFechaEmision" Enabled="false" runat="server"  onblur="return(valFecha(this));"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" 
                                            TargetControlID="txtFechaEmision" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                             <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFechaEmision"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999" >
                             </cc1:MaskedEditExtender>                               
                                    
                                </td>
                            </tr>
                        </table>
                        </asp:Panel>   
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        Datos del Cliente</td>
                </tr>
                <tr>
                <td>
                    <asp:Panel ID="Panel_DatosCliente" runat="server">
                    <table>
                 <tr>
                    <td>
                        <asp:Label ID="Label78" runat="server" Text="Ap. y Nombres/Raz�n Social:" CssClass="Label"></asp:Label>
                        <asp:TextBox ReadOnly="True" CssClass="TextBoxReadOnly" ID="txtNombreCliente" Width="550px" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtIdCliente" ReadOnly="True" CssClass="TextBoxReadOnly" runat="server" Width="75px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>                  
                     
                    
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_DatosCliente" runat="server" 
                                                TargetControlID="SubPanel_DatosCliente" 
                                                CollapsedSize="0" 
                                                ExpandedSize="60" 
                                                Collapsed="true"
                                                ExpandControlID="Image_DatosCliente"
                                                CollapseControlID="Image_DatosCliente" 
                                                TextLabelID="Label_DatosCliente" 
                                                ImageControlID="Image_DatosCliente"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Datos del Cliente" 
                                                ExpandedText="Datos del Cliente"                               
                                                ExpandDirection="Vertical" SuppressPostBack="True">
                                                </cc1:CollapsiblePanelExtender>                                                                           
                                            <asp:Image ID="Image_DatosCliente" runat="server" />                                                             
                                            <asp:Label ID="Label_DatosCliente" runat="server" Text="Label" CssClass="Label"></asp:Label>
                        <asp:Panel ID="SubPanel_DatosCliente" runat="server">
                        <table>
                                    <tr>
                                        <td align="right">
                                            <asp:Label CssClass="Label" ID="Label79" runat="server" Text="R.U.C.:" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ReadOnly="True" CssClass="TextBoxReadOnly" ID="txtRUC" runat="server"></asp:TextBox>
                                            <asp:Label CssClass="Label" ID="Label81" runat="server" Text="D.N.I.:" ></asp:Label>
                                            <asp:TextBox ID="txtDNI" runat="server" ReadOnly="True" CssClass="TextBoxReadOnly"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="Label" ID="Label80" runat="server" Text="Direcci�n:" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDireccion" Width="600px" runat="server" ReadOnly="True" CssClass="TextBoxReadOnly"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                        </asp:Panel>
                    </td>
                </tr>
                </table>
                    </asp:Panel>
                </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        Detalle del Despacho</td>
                </tr>
                <tr>
                    <td>
                 
                        <asp:Panel ID="Panel_DetalleOD" runat="server">
                        <asp:GridView ID="DGVDetalle" runat="server" AutoGenerateColumns="False"
                                Width="100%">                                
                                <Columns>                                  
                                <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ButtonType="Link" />
                                <asp:BoundField DataField="IdProducto" HeaderText="Id" NullDisplayText="0" />
                                <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" NullDisplayText="---" />
                                <asp:BoundField DataField="UMedida" HeaderText="U.M." NullDisplayText="---" />
                                <asp:BoundField DataField="Cantidad" DataFormatString="{0:F2}" HeaderText="Cantidad Total" NullDisplayText="0" />
                                <asp:BoundField DataField="CantxAtender" DataFormatString="{0:F2}" HeaderText="Cant. Pendiente" NullDisplayText="0" />
                                  <asp:TemplateField HeaderText="Cantidad a Despachar">
                                                    <ItemTemplate>
                                                    
                                                    <table>
                                                    <tr>
                                                    <td><asp:TextBox Font-Bold="true" Width="100px" onblur="return(valGrillaBlur());" ID="txtCantidadADespachar" runat="server" 
                                                            onKeypress="return(validarNumeroPunto(event));"  onKeyup="return(valCantidad());" onfocus="return(aceptarFoco(this));"
                                                            Text='<%#DataBinder.Eval(Container.DataItem,"CantADespachar","{0:F2}")%>'></asp:TextBox></td>
                                                    <td>
                                                        <asp:HiddenField ID="hddCantidadOriginal" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"CantADespacharOriginal","{0:F2}")%>'/>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    
                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                <asp:BoundField DataField="IdAlmacen" HeaderText="" NullDisplayText="0" />
                                <asp:BoundField DataField="IdUnidadMedidaPrincipal" HeaderText="" NullDisplayText="0" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </asp:Panel>
                 
                    </td>
                </tr>
                <tr >
                    <td class="TituloLinks">                                                          
                    <asp:LinkButton ID="btnMostrarCapaBuscarDoc" OnClientClick="return(valMostrarCapaBuscar(this));"  runat="server">Buscar Despacho Pendientes</asp:LinkButton>                                         
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hddIdDocumento" Value="" runat="server" />
                        <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                  </td>
                </tr>
            </table>
       </ContentTemplate></asp:UpdatePanel>
   <div  id="capaBuscarP" style="border: 3px solid blue; padding: 8px; width:672px; height:auto; position:absolute; top:165px; left:53px; background-color:white; z-index:2; display :none;">
                        <asp:UpdatePanel ID="UpdatePanelBuscarP" runat="server">
                            <ContentTemplate>

                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="btnCerrarCapa_B" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaBuscarP'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Label ID="Label82" runat="server" CssClass="Label" Text="Empresa:"></asp:Label>
                                    <asp:DropDownList ID="cmbEmpresa_B" runat="server" AutoPostBack="True" 
                                        DataTextField="NombreComercial" DataValueField="Id" Enabled="true">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label83" runat="server" Text="Tienda:" CssClass="Label"></asp:Label>
                                    <asp:DropDownList ID="cmbTienda_B" runat="server" AutoPostBack="true"  DataTextField="Nombre" 
                                        DataValueField="Id" Enabled="false">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label84" runat="server" Text="Tipo de Documento:" CssClass="Label"></asp:Label>
                                    <asp:DropDownList ID="cmbTipoDocumento_B" runat="server" AutoPostBack="True">
                                    <asp:ListItem Enabled="true" Selected="True" Value="21">Orden de Despacho</asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Label ID="Label85" CssClass="Label" runat="server" Text="Serie:" ></asp:Label>
                                    <asp:DropDownList ID="cmbSerie_B" runat="server">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label86" runat="server" CssClass="Label" Text="C�digo:"></asp:Label>
                                    <asp:TextBox ID="txtCodigo_B" runat="server" style="width: 128px" onKeypress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                    <asp:ImageButton ID="btnAceptar_B" runat="server"  OnClientClick="return(valAceptar_B());"
                                        ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                                        onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                                        onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
                                    </td>
                                </tr>
                                                                    </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div> 



    
    <div  id="capaDocxDespachar" 
        style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; top:165px; left:24px; background-color:white; z-index:2; display :none;">
        <asp:UpdatePanel ID="UpdatePanel_CapaDocxDespachar" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
              <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="btnCerrarCapaDocxDespachar" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" OnClientClick="return(offCapa('capaDocxDespachar'));"
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  />                                
                                </td>
                                </tr>
                                <tr align="left">
                                <td>            
                                
                                    <asp:GridView ID="DGV_DocxDespachar" Width="100%" runat="server" AutoGenerateColumns="false"  PageSize="20" AllowPaging="true"  >
                                    <Columns>
                                    
                                    <asp:CommandField SelectText="Despachar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" />
                                    <asp:TemplateField HeaderText="Documento">
                                        <ItemTemplate>
                                        <table>
                                        <tr>
                                        <td>  
                                            <asp:HiddenField ID="hddIdDocumentoxDespachar" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />  </td>
                                        <td> 
                                            <asp:Label ForeColor="Red" ID="lblSerieDocxDesp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Serie")%>'></asp:Label>  </td>
                                            <td>-</td>
                                        <td> 
                                            <asp:Label ForeColor="Red" ID="lblCodigoDocxDesp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>  </td>
                                        </tr>
                                        </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisi�n" />
                                    <asp:BoundField DataField="NomTienda" HeaderText="Tienda" />
                                    <asp:BoundField DataField="NomAlmacen" HeaderText="Almacen" />
                                    <asp:BoundField DataField="getNombreAMostrar" HeaderText="Descripci�n" />
                                    <asp:BoundField DataField="RUC" HeaderText="R.U.C." />
                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." />
                                    <asp:BoundField DataField="NomEstadoEntrega" HeaderText="Estado Entrega" />
                                    
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                
                                                        
                                </td>
                                </tr>                              
                                </table>
        
        </ContentTemplate>        
        <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnMostrarCapaBuscarDoc" EventName="Click" />
        </Triggers>
        </asp:UpdatePanel>
    
    
                          
                            
                        
                        </div> 
    
   
    

<script language="javascript" type="text/javascript">
    function valMostrarCapaBuscar(boton) {
        if (boton.disabled==true){
            return false;
        }
        //********* onCapa('capaDocxDespachar');
        return true;
    }
    function valAnular() {        
        return (confirm("Desea anular la Orden de Despacho?"));
    }
        function buscarDocumento() {
            var caja = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var boton = document.getElementById('<%=btnBuscarDocumento.ClientID %>');
            //var cboTipoDoc = document.getElementById('<%=cmbTipoDocumento.ClientID %>');
            //cboTipoDoc.disabled = false;
            boton.disabled = false;
            caja.disabled = false;
            caja.style.backgroundColor = 'Yellow';
            caja.select();
            caja.focus();
            return false;
        }
        function validarBuscarDoc() {
            var caja = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (CajaEnBlanco(caja) || !esDecimal(caja.value)) {
                alert('Ingrese un c�digo.');
                caja.focus();
                return false;
            }
            return true;
        }
    
        function valAceptar_B() {
            var caja = document.getElementById('<%=txtCodigo_B.ClientID %>');
            if (CajaEnBlanco(caja)){
                alert('Ingrese un c�digo.');
                caja.focus();
                return false;
            }            
            return true;
        }
        function validarSave() {
            var grilla = document.getElementById("<%= DGVDetalle.ClientID %>");
            if (grilla == null) {
                alert('No se han encontrado registros.');
                return false;
            }
            var IdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>').value;
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];

                    var cantxEntregar = parseFloat(rowElem.cells[6].children[0].cells[0].children[0].value);
                    var cantPendiente = parseFloat(rowElem.cells[5].innerHTML);
                    if (IdDocumento.length == 0 || isNaN(IdDocumento)) {
                        //*********** NUEVO                        
                        if (cantxEntregar > cantPendiente) {
                            rowElem.cells[6].children[0].cells[0].children[0].select();
                            rowElem.cells[6].children[0].cells[0].children[0].focus();
                            alert('La cantidad a despachar no puede ser mayor a la cantidad pendiente.');
                            return false;
                        }
                    } else {
                        //************* EDITAR
                        var cantOriginal = parseFloat(rowElem.cells[6].children[0].cells[1].children[0].value);
                        if (cantxEntregar > (cantPendiente + cantOriginal)) {
                            rowElem.cells[6].children[0].cells[0].children[0].select();
                            rowElem.cells[6].children[0].cells[0].children[0].focus();
                            alert('La cantidad a despachar no puede ser mayor a ' + (cantPendiente + cantOriginal) + ' ' + rowElem.cells[3].innerHTML + '.');
                            return false;
                        }
                    }
                   
                   
                   
                    if (cantxEntregar ==0) {
                        rowElem.cells[6].children[0].cells[0].children[0].select();
                        rowElem.cells[6].children[0].cells[0].children[0].focus();
                        alert('La Cantidad a despachar debe ser un n�mero mayor a cero.');
                        return false;
                    }
                    
                }
                //return (confirm('Desea continuar con la operaci�n?'));
                return true;
        }
        function valCantidad() {
            var grilla = document.getElementById("<%= DGVDetalle.ClientID %>");
            if (grilla == null) {
                alert('No se han encontrado registros.');
                return false;
            }
            var IdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>').value;
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
               
                if (rowElem.cells[6].children[0].cells[0].children[0].id == event.srcElement.id) {
                    
                    var cantxEntregar = parseFloat(rowElem.cells[6].children[0].cells[0].children[0].value);
                    var cantPendiente = parseFloat(rowElem.cells[5].innerHTML);                    
                    if (IdDocumento.length == 0 || isNaN(IdDocumento)) {
                        //*********** NUEVO                        
                        if (cantxEntregar > cantPendiente) {
                            rowElem.cells[6].children[0].cells[0].children[0].select();
                            rowElem.cells[6].children[0].cells[0].children[0].focus();
                            alert('La cantidad a despachar no puede ser mayor a la cantidad pendiente.');
                            return false;
                        }                           
                    }else{
                    //************* EDITAR
                    var cantOriginal = parseFloat(rowElem.cells[6].children[0].cells[1].children[0].value);
                        if (cantxEntregar > (cantPendiente + cantOriginal)) {
                            rowElem.cells[6].children[0].cells[0].children[0].select();
                            rowElem.cells[6].children[0].cells[0].children[0].focus();
                            alert('La cantidad a despachar no puede ser mayor a ' + (cantPendiente + cantOriginal) + ' ' + rowElem.cells[3].innerHTML + '.');
                            return false;
                        }  
                    }
                    return false;  
                }
            }
        }      
        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {                    
                    return false;
                }
            }
        }
        function valGrillaBlur() {
            var id = event.srcElement.id;
            var caja = document.getElementById(id);
            if (CajaEnBlanco(caja)) {
                caja.focus();
                alert('Debe ingresar un valor.');
                return false;
            }
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no v�lido.');
                return false;
            }
        }


        function print() { 
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if(  isNaN(parseInt(hddIdDocumento.value)) ||  hddIdDocumento.value.length<=0  ){
                alert('No se tiene ning�n documento para Impresi�n.');
                return false;
            }
            window.open('../../Reportes/visor.aspx?iReporte=3&IdDoc=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=700,scrollbars=1', null);
            return false;
        }
        
        
    </script>    
    
                        
                        
                        
</asp:Content>

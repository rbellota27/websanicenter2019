﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Configuration
Partial Public Class FrmEmitirCotizacionV2
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaCatalogo As List(Of Entidades.Catalogo)
    Private listamaestro As List(Of Entidades.Catalogo)

    Private listaDetalleConcepto As List(Of Entidades.DetalleConcepto)
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Private ValidoRep As New Negocio.Util
    Private objDocMer As New Negocio.DocumentosMercantiles
    Private listaDocumentoRef As List(Of Entidades.Documento)

    Private Const _PrecioBaseDcto_Lista As String = "PL"
    Private Const _PrecioBaseDcto_Comercial As String = "PC"
    Private Const _IdMagnitud_Peso As Integer = 1
#Region "Cadena Conx"
    Private con As New SqlConnection("Data Source=192.168.2.220;Initial Catalog=SIGE_SANISERVER;User ID=sa;pwd=Sani1234")
#End Region
#Region "************************ MANEJO DE SESSION"

    Private Function getListaCatalogo() As List(Of Entidades.Catalogo)
        Return CType(Session.Item(CStr(ViewState("listaCatalogo"))), List(Of Entidades.Catalogo))
    End Function
    Private Sub setListaCatalogo(ByVal lista As List(Of Entidades.Catalogo))
        Session.Remove(CStr(ViewState("listaCatalogo")))
        Session.Add(CStr(ViewState("listaCatalogo")), lista)
    End Sub
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)

        Return CType(Session.Item(CStr(ViewState("DetalleDocumento"))), List(Of Entidades.DetalleDocumento))

    End Function

    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove(CStr(ViewState("DetalleDocumento")))
        Session.Add(CStr(ViewState("DetalleDocumento")), lista)
    End Sub
    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item(CStr(ViewState("listaDocumentoRef"))), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(CStr(ViewState("listaDocumentoRef")))
        Session.Add(CStr(ViewState("listaDocumentoRef")), lista)
    End Sub

    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("DetalleDocumento", "DetalleDocumentoC" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaDocumentoRef", "listaDocumentoRefC" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaCatalogo", "listaCatalogoC" + Aleatorio.Next(0, 100000).ToString)

    End Sub

#End Region
    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            onLoad_Session()
            verSeccionesFrm()
            inicializarFrm()
            ConfigurarDatos()
            ValidarPermisos()
            ClientexDefecto()
        End If
        visualizarMoneda()
        Me.GV_ComponenteKit.DataSource = Nothing
        Me.GV_ComponenteKit.DataBind()
        Me.GV_ComponenteKit_Find.DataSource = Nothing
        Me.GV_ComponenteKit_Find.DataBind()
        lbl_detalles.Visible = True
        btnVentasMes.Visible = True
    End Sub
    Private Sub verSeccionesFrm()
        Dim cad() As String = Nothing
        cad = Split(ValidoRep.fx_getValorParametroGeneralxIdparam("18,19,21,22,23"), ";", -1)
        ViewState.Add("ClientexDefecto", CInt(cad(0).ToString))
        verPanelMaestroObra(CInt(cad(1).ToString)) 'para mostrar seccion maestro obra
        verPanelPuntoPartidaPuntoLlegada(CInt(cad(2).ToString)) 'para mostrar seccion punto partida y punto llegada
        verpanelConceptos(CInt(cad(3).ToString)) 'para mostrar seccion conceptos
        ViewState.Add("SecCobBarra", CInt(cad(4).ToString))
        If (CInt(cad(4).ToString) = 0) Then
            Me.tdSecCodBarras.Visible = False
        Else
            Me.tdSecCodBarras.Visible = True
            cursorCodigoBarras()
        End If
    End Sub
    Private Sub ClientexDefecto()
        If (CInt(ViewState("ClientexDefecto")) > 0) Then 'para mostrar Cliente por defecto
            cargarPersona(0)
        End If
    End Sub
    Private Sub verPanelPuntoPartidaPuntoLlegada(ByVal valor As Integer)
        If valor = 1 Then
            TrTituloPuntoPartida.Visible = True
            TrTituloPuntoLlegada.Visible = True
            TrDetallePuntoLlegada.Visible = True
            TrDetallePuntoPartida.Visible = True

        Else
            TrTituloPuntoPartida.Visible = False
            TrTituloPuntoLlegada.Visible = False
            TrDetallePuntoLlegada.Visible = False
            TrDetallePuntoPartida.Visible = False

        End If
    End Sub
    Private Sub verpanelConceptos(ByVal valor As Integer)
        If valor = 1 Then
            TrTituloConcepto.Visible = True
            TrDetalleConcepto.Visible = True
        Else
            TrTituloConcepto.Visible = False
            TrDetalleConcepto.Visible = False
        End If
    End Sub
    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / NRO DIAS VIGENCIA / REGISTRAR CLIENTE / EDITAR CLIENTE / MODIFICAR OPCION DE REDONDEO
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {89, 90, 91, 97, 92, 93, 94, 95, 96, 195, 217})
        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If
        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If
        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If
        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* FECHA VCTO
            Me.txtFechaVcto.Enabled = True
        Else
            Me.txtFechaVcto.Enabled = False
        End If
        'If listaPermisos(6) > 0 Then  '********* DIAS VIGENCIA CREDITO
        '    Me.txtNroDiasVigenciaCredito.Enabled = True
        'Else
        '    Me.txtNroDiasVigenciaCredito.Enabled = False
        'End If

        If listaPermisos(7) > 0 Then  '********* REGISTRAR CLIENTE
            Me.btnNuevoCliente.Enabled = True
            Me.btnGuardarCliente.Enabled = True
        Else
            Me.btnNuevoCliente.Enabled = False
            Me.btnGuardarCliente.Enabled = False
        End If

        If listaPermisos(8) > 0 Then  '********* EDITAR CLIENTE
            Me.btnEditar_Cliente.Enabled = True
            Me.btnGuardar_EditarCliente.Enabled = True
        Else
            Me.btnEditar_Cliente.Enabled = False
            Me.btnGuardar_EditarCliente.Enabled = False
        End If

        If listaPermisos(9) > 0 Then  '********* EDITAR CLIENTE
            Me.rbl_UtilizarRedondeo.Enabled = True
        Else
            Me.rbl_UtilizarRedondeo.Enabled = False
        End If

        If listaPermisos(10) > 0 Then  '********* USUARIO - VENDEDOR
            Me.cboUsuarioComision.Enabled = True
        Else
            Me.cboUsuarioComision.Enabled = False
        End If

    End Sub
    Private Sub cursorCodigoBarras()
        If (CInt(ViewState("SecCobBarra")) > 0) Then
            Me.txtCodBarrasPrincipal.Focus()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
                '***GenerarCodigoDocumento()  ** EL CODIGO SE GENERA EN ACTUALIZAR BOTONES DE CONTROL
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(cboTipoDocumento, CInt(Me.hddIdTipoDocumento.Value), 2, False)
                .LlenarCboMoneda(Me.cboMoneda, False)
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), False)
                .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)
                .LlenarCboCondicionPago(Me.cboCondicionPago)
                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, CInt(Me.hddIdTipoDocumento.Value), CInt(Me.cboCondicionPago.SelectedValue))
                .LlenarCboTipoPV(Me.cboTipoPrecioV)
                .LlenarCboRolCliente(Me.cborolcliente)
                .LlenarCboTipoPV(Me.cboTipoPrecioPV_CambiarDetalle)
                .LlenarCboTipoAgente(Me.cboTipoAgente, True)
                .LlenarCboTipoAgente(Me.cmbTipoAgente_RegistrarCliente, True)
                .LlenarCboTipoAgente(Me.cboTipoAgente_EditarCliente, True)
                '************** PUNTO DE LLEGADA
                .LLenarCboDepartamento(Me.cboDepartamento)
                .LLenarCboProvincia(Me.cboProvincia, (Me.cboDepartamento.SelectedValue))
                .LLenarCboDistrito(Me.cboDistrito, (Me.cboDepartamento.SelectedValue), (Me.cboProvincia.SelectedValue))
                '**************** BUSQUEDA DE PRODUCTOS
                .llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)
                '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
                .llenarCboTipoExistencia(cboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
                '***********************  USUARIO COMISION
                .LLenarCboUsuario(Me.cboUsuarioComision, False)
                .LlenarCboTipoAlmacen(Me.ddlTipoAlmacen, False)
                '***********************  PESO
                .llenarCboUnidadMedidaxIdMagnitud(Me.cboPesoTotal, _IdMagnitud_Peso, False)
                If (Me.cboUsuarioComision.Items.FindByValue(CStr(Session("IdUsuario"))) IsNot Nothing) Then
                    Me.cboUsuarioComision.SelectedValue = CStr(Session("IdUsuario"))
                End If
                '***********************Tipo de Impresion
                hddTipoDiseno.Value = CStr(objDocMer.crpImprimir(14))
            End With

            Dim IdMedioPago_Default As Integer = (New Negocio.MedioPago).SelectIdMedioPagoPrincipal
            Me.hddIdMedioPago_Default.Value = CStr(IdMedioPago_Default)
            If (Me.cboMedioPago.Items.FindByValue(CStr(IdMedioPago_Default)) IsNot Nothing) Then
                Me.cboMedioPago.SelectedValue = CStr(IdMedioPago_Default)
            End If
            Me.lblPorcentIgv.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))
            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaAEntregar.Text = Me.txtFechaEmision.Text
            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaNac.Text = Me.txtFechaEmision.Text
            Me.hddIdTipoPVDefault.Value = CStr((New Negocio.TipoPrecioV).SelectIdTipoPrecioVDefault())
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
            actualizarOpcionesBusquedaDocRef(0, False)

            visualizarMoneda()
            actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue))
            verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub verPanelMaestroObra(ByVal valor As Integer)
        If valor = 1 Then
            pn_MaestroObra.Visible = True
        Else
            pn_MaestroObra.Visible = False
        End If
    End Sub
    Private Sub actualizarPuntoPartida(ByVal IdTienda As Integer, ByVal updateDepto As Boolean, ByVal updateProv As Boolean, ByVal updateDist As Boolean, Optional ByVal updateCliente As Boolean = False)
        Dim ubigeoTienda As String = (New Negocio.Tienda).SelectxId(IdTienda)(0).Ubigeo
        Dim direccion_Tienda As String = (New Negocio.Tienda).SelectxId(IdTienda)(0).Direccion
        '***************** PUNTO DE PARTIDA
        Dim nUbigeo As New Negocio.Ubigeo
        If (updateDepto) Then
            Dim listaDpto As List(Of Entidades.Ubigeo) = nUbigeo.SelectAllDepartamentos()
            Me.cboDepto_Partida.DataSource = listaDpto
            Me.cboDepto_Partida.DataBind()
            'objCbo.LLenarCboDepartamento(Me.cboDepto_Partida)
            Me.cboDepto_Partida.SelectedValue = ubigeoTienda.Substring(0, 2)
            If updateCliente Then
                Me.cboCliDepartamentoE.DataSource = listaDpto
                Me.cboCliDepartamentoN.DataSource = listaDpto
                Me.cboMODepartamentoN.DataSource = listaDpto
                Me.cboMODepartamentoE.DataSource = listaDpto
                Me.cboCliDepartamentoE.DataBind()
                Me.cboCliDepartamentoN.DataBind()
                Me.cboMODepartamentoN.DataBind()
                Me.cboMODepartamentoE.DataBind()
                Me.cboCliDepartamentoE.SelectedValue = ubigeoTienda.Substring(0, 2)
                Me.cboCliDepartamentoN.SelectedValue = ubigeoTienda.Substring(0, 2)
                Me.cboMODepartamentoN.SelectedValue = ubigeoTienda.Substring(0, 2)
                Me.cboMODepartamentoE.SelectedValue = ubigeoTienda.Substring(0, 2)
            End If
        End If
        If (updateProv) Then
            Dim listaProv As List(Of Entidades.Ubigeo) = nUbigeo.SelectAllProvinciasxCodDpto(Me.cboDepto_Partida.SelectedValue)
            Me.cboProvincia_Partida.DataSource = listaProv
            Me.cboProvincia_Partida.DataBind()
            'objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
            Me.cboProvincia_Partida.SelectedValue = ubigeoTienda.Substring(2, 2)
            If updateCliente Then
                Me.cboCliProvinciaE.DataSource = listaProv
                Me.cboCliProvinciaN.DataSource = listaProv
                Me.cboMOProvinciaN.DataSource = listaProv
                Me.cboMOProvinciaE.DataSource = listaProv
                Me.cboCliProvinciaE.DataBind()
                Me.cboCliProvinciaN.DataBind()
                Me.cboMOProvinciaN.DataBind()
                Me.cboMOProvinciaE.DataBind()
                Me.cboCliProvinciaE.SelectedValue = ubigeoTienda.Substring(2, 2)
                Me.cboCliProvinciaN.SelectedValue = ubigeoTienda.Substring(2, 2)
                Me.cboMOProvinciaN.SelectedValue = ubigeoTienda.Substring(2, 2)
                Me.cboMOProvinciaE.SelectedValue = ubigeoTienda.Substring(2, 2)
            End If
        End If
        If (updateDist) Then
            Dim listaDist As List(Of Entidades.Ubigeo) = nUbigeo.SelectAllDistritosxCodDptoxCodProv(Me.cboDepto_Partida.SelectedValue, Me.cboProvincia_Partida.SelectedValue)

            Me.cboDistrito_Partida.DataSource = listaDist
            Me.cboDistrito_Partida.DataBind()
            'objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, (Me.cboProvincia_Partida.SelectedValue))
            Me.cboDistrito_Partida.SelectedValue = ubigeoTienda.Substring(4, 2)
            If updateCliente Then
                Me.cboCliDistritoE.DataSource = listaDist
                Me.cboCliDistritoN.DataSource = listaDist
                Me.cboMODistritoN.DataSource = listaDist
                Me.cboMODistritoE.DataSource = listaDist
                Me.cboCliDistritoE.DataBind()
                Me.cboCliDistritoN.DataBind()
                Me.cboMODistritoN.DataBind()
                Me.cboMODistritoE.DataBind()
            End If
        End If
        Me.txtDireccion_Partida.Text = direccion_Tienda
    End Sub

    Private Sub visualizarMoneda()
        Me.lblMonedaTotal.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalGasto.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalPagar.Text = Me.cboMoneda.SelectedItem.ToString
    End Sub
    Private Sub GenerarCodigoDocumento()
        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If
    End Sub
    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)
            actualizarPuntoPartida(CInt(Me.cboTienda.SelectedValue), True, True, True)
            '************* Línea de Crédito
            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                Me.GV_LineaCredito.DataBind()
                lbl_detalles.Visible = True
                btnVentasMes.Visible = True
            End If
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)
            actualizarPuntoPartida(CInt(Me.cboTienda.SelectedValue), True, True, True)
            '************* Línea de Crédito
            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                Me.GV_LineaCredito.DataBind()
                lbl_detalles.Visible = True
                btnVentasMes.Visible = True
            End If
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartamento.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia, (Me.cboDepartamento.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito, (Me.cboDepartamento.SelectedValue), (Me.cboProvincia.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboProvincia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito, (Me.cboDepartamento.SelectedValue), (Me.cboProvincia.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, ByVal viewMoneda As Boolean, Optional ByVal actualizarPuntoPartida_Tienda As Boolean = False)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove(CStr(ViewState("DetalleDocumento")))
            Session.Remove(CStr(ViewState("listaDocumentoRef")))
            Session.Remove(CStr(ViewState("listaCatalogo")))
        End If

        If (initSession) Then
            Me.listaCatalogo = New List(Of Entidades.Catalogo)
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setListaCatalogo(Me.listaCatalogo)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then
            '******* FECHA DE VCTO
            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})
            Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, listaParametros(0), CDate(Me.txtFechaEmision.Text)), "dd/MM/yyyy")
            'Me.txtNroDiasVigenciaCredito.Text = CStr(Math.Round(listaParametros(1), 0))
        End If

        If (viewMoneda) Then
            visualizarMoneda()
        End If

        If (actualizarPuntoPartida_Tienda) Then
            Dim updateCliente As Boolean = False
            If Not IsPostBack Then
                updateCliente = True
            End If
            actualizarPuntoPartida(CInt(Me.cboTienda.SelectedValue), True, True, True, updateCliente)
        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()
        lbl_detalles.Visible = True
        btnVentasMes.Visible = True
    End Sub
    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaVcto.Text = ""
        Me.txtFechaAEntregar.Text = ""
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto
        Me.cboTipoOperacion.SelectedIndex = 0  '******* SIN TIPO DE OPERACION POR DEFECTO       
        If (Me.cboUsuarioComision.Items.FindByValue(CStr(Session("IdUsuario"))) IsNot Nothing) Then
            Me.cboUsuarioComision.SelectedValue = CStr(Session("IdUsuario"))
        End If
        Me.cboMotivoTraslado.SelectedIndex = 0
        '******************* PERSONA
        Me.cboTipoPersona_PanelCliente.SelectedIndex = 0
        Me.txtApPaterno_RazonSocial.Text = ""
        Me.txtApMaterno.Text = ""
        Me.txtNombres.Text = ""
        Me.txtCodigoCliente.Text = ""
        Me.txtIdCliente_BuscarxId.Text = ""
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""
        Me.cboTipoPrecioV.SelectedIndex = 0
        Me.cborolcliente.SelectedIndex = 0
        Me.txtDireccionCliente.Text = ""
        Me.cboTipoAgente.SelectedIndex = 0
        Me.txtTasaAgente.Text = "0"
        '************* MAESTRO DE OBRA
        Me.txtMaestro.Text = ""
        Me.txtDni_Maestro.Text = ""
        Me.txtRuc_Maestro.Text = ""
        txtIdMaestroObra_BuscarxId.Text = ""
        '********** LINEA DE CREDITO
        Me.GV_LineaCredito.DataSource = Nothing
        Me.GV_LineaCredito.DataBind()
        lbl_detalles.Visible = False
        btnVentasMes.Visible = False
        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()
        Me.txtDescuento.Text = "0"
        Me.txtSubTotal.Text = "0"
        Me.txtTotal.Text = "0"
        Me.txtPercepcion.Text = "0"
        Me.txtTotalAPagar.Text = "0"
        Me.txtTotalConcepto_PanelDetalle.Text = "0"
        Me.txtIGV.Text = "0"
        Me.txtImporteTotal.Text = "0"
        '*********** PUNTO PARTIDA
        Me.cboDepto_Partida.SelectedIndex = 0
        Me.cboProvincia_Partida.SelectedIndex = 0
        Me.cboDistrito_Partida.SelectedIndex = 0
        Me.txtDireccion_Partida.Text = ""

        '*********** PUNTO LLEGADA
        Me.cboDepartamento.SelectedIndex = 0
        Me.cboProvincia.SelectedIndex = 0
        Me.cboDistrito.SelectedIndex = 0
        Me.txtDireccion_Llegada.Text = ""

        '************* CONCEPTO
        Me.GV_Concepto.DataSource = Nothing
        Me.GV_Concepto.DataBind()
        Me.txtTotalConcepto.Text = "0"

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIdPersona.Value = ""
        Me.hddIndex_GV_Detalle.Value = ""
        Me.hddIndexCambiarTipoPV_GV_Detalle.Value = ""
        Me.hddIndexGlosa_Gv_Detalle.Value = ""
        Me.hddIdMaestroObra.Value = ""

        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_ComponenteKit.DataSource = Nothing
        Me.GV_ComponenteKit.DataBind()
        Me.GV_ComponenteKit_Find.DataSource = Nothing
        Me.GV_ComponenteKit_Find.DataBind()

        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

    End Sub

    Private Sub ActualizarBotonesControl()

        Me.btnBuscarDocumentoRef.Visible = False
        Me.Panel_DocRef.Enabled = False

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                btnImprimirResumido.Visible = False


                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = True
                Me.Panel_Obs.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Concepto.Enabled = False
                Me.Panel_MaestroObra.Enabled = False


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboMoneda.Enabled = True
                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                btnImprimirResumido.Visible = False



                Me.btnBuscarDocumentoRef.Visible = True
                Me.Panel_DocRef.Enabled = True

                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Cab.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_PuntoLlegada.Enabled = True
                Me.Panel_Concepto.Enabled = True

                Me.Panel_MaestroObra.Enabled = True


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

                '*********** PUNTO PARTIDA
                actualizarPuntoPartida(CInt(Me.cboTienda.SelectedValue), True, True, True)


            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                btnImprimirResumido.Visible = False

                Me.btnBuscarDocumentoRef.Visible = True
                Me.Panel_DocRef.Enabled = True

                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Cab.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_PuntoLlegada.Enabled = True
                Me.Panel_Concepto.Enabled = True

                Me.Panel_MaestroObra.Enabled = True


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                Me.cboMoneda.Enabled = True




            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnImprimirDetallado.Visible = False
                btnImprimirResumido.Visible = False



                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = True
                Me.Panel_Obs.Enabled = False

                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Concepto.Enabled = False

                Me.Panel_MaestroObra.Enabled = False



                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True



            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True
                Me.btnImprimirDetallado.Visible = True
                btnImprimirResumido.Visible = True




                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Concepto.Enabled = False

                Me.Panel_MaestroObra.Enabled = False


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnImprimir.Visible = True
                Me.btnImprimirDetallado.Visible = True
                btnImprimirResumido.Visible = True


                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Concepto.Enabled = False


                Me.Panel_MaestroObra.Enabled = False



                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False



            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnImprimirDetallado.Visible = True
                btnImprimirResumido.Visible = True

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Concepto.Enabled = False

                Me.Panel_MaestroObra.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False

                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub


#Region "************************** BUSQUEDA PRODUCTO"

    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
        Me.txtCodBarrasCapa.Text = ""
    End Sub
    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                        If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                            IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                        End If
                    End If

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)
                    ViewState.Add("IdMoneda_BuscarProducto", Me.cboMoneda.SelectedValue)
                    ViewState.Add("IdTipoPV_BuscarProducto", IdTipoPV)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("CodigoBarras", Me.txtCodBarrasCapa.Text)
                    ViewState.Add("IdTipoExistencia_BuscarProducto", Me.cboTipoExistencia.SelectedValue)

                Case 1
                    '******************* Cuando se cambia el almacen de busqueda
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)

                Case 2
                    '******************** cuando se cambia la tienda de precios
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMoneda As Integer, ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal codbarras As String, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim IdTipoOperacion As Integer = 0
        IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)



        '************ Esto ha sido comentado a pedido de cristhian panta 
        Dim IdMedioPago As Integer = 0
        'If (CInt(Me.cboCondicionPago.SelectedValue) = 1) Then  '************ CONTADO
        IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
        'End If



        Dim IdCliente As Integer = 0
        If (IsNumeric(Me.hddIdPersona.Value) And (Me.hddIdPersona.Value.Trim.Length) > 0) Then
            IdCliente = CInt(Me.hddIdPersona.Value)
        End If

        Dim codigoProducto As String = Me.txtCodigoProducto.Text.Trim
        Dim codigoProveedor As String = Me.txtCodProveedor.Text.Trim
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        Dim filtroProductoCampania As Boolean = Me.chb_FiltroProductoCampania.Checked

        'Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        ''MODIFICADO
        '' Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, codbarras, filtroProductoCampania, IdTipoExistencia, codigoProveedor, IdTipoOperacion)
        ''NUEVO:
        Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoV2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, codbarras, filtroProductoCampania, IdTipoExistencia, codigoProveedor, IdTipoOperacion)
        If Me.listaCatalogo.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            setListaCatalogo(Me.listaCatalogo)
            grilla.DataSource = Me.listaCatalogo
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
                Dim cboUM As DropDownList = CType(gvrow.FindControl("cmbUnidadMedida_AddProd"), DropDownList)
                Me.listaCatalogo(e.Row.RowIndex).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(Me.listaCatalogo(e.Row.RowIndex).IdProducto, CInt(Me.cboTienda.SelectedValue), Me.listaCatalogo(e.Row.RowIndex).IdTipoPV)
                cboUM.DataSource = Me.listaCatalogo(e.Row.RowIndex).ListaUM_Venta
                cboUM.DataBind()

                If (cboUM.Items.FindByValue(CStr(Me.listaCatalogo(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                    cboUM.SelectedValue = Me.listaCatalogo(e.Row.RowIndex).IdUnidadMedida.ToString
                End If


                setListaCatalogo(Me.listaCatalogo)

                If (Me.listaCatalogo(e.Row.RowIndex).Kit) Then  '********* KIT

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = False

                End If

                Dim btnVerCampania As ImageButton = CType(e.Row.FindControl("btnViewCampania"), ImageButton)
                If (Me.listaCatalogo(e.Row.RowIndex).ExisteCampania_Producto) Then
                    btnVerCampania.Visible = True
                Else
                    btnVerCampania.Visible = False
                End If


            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    'Protected Sub cboUM_SelectedValueChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Try

    '        ActualizarListaCatalogo()

    '        Me.listaCatalogo = Me.getListaCatalogo
    '        Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

    '        If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
    '            If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
    '                IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
    '            End If
    '        End If

    '        Dim cboUM As DropDownList = CType(sender, DropDownList)
    '        Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

    '        Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaCatalogo(i).IdUnidadMedida, Me.listaCatalogo(i).IdProducto, _
    '         IdTipoPV, Me.listaCatalogo(i).IdTienda, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text), _
    '         )

    '        If objCatalogo IsNot Nothing Then

    '            Me.listaCatalogo(i).PrecioSD = objCatalogo.PrecioSD
    '            Me.listaCatalogo(i).PrecioLista = objCatalogo.PrecioLista
    '            Me.listaCatalogo(i).StockDisponibleN = objCatalogo.StockDisponibleN
    '            Me.listaCatalogo(i).pvComercial = objCatalogo.pvComercial



    '        End If

    '        setListaCatalogo(Me.listaCatalogo)

    '        DGV_AddProd.DataSource = Me.listaCatalogo
    '        DGV_AddProd.DataBind()

    '        objScript.onCapa(Me, "capaBuscarProducto_AddProd")

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub


    Protected Sub cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            ActualizarListaCatalogo()

            Me.listaCatalogo = Me.getListaCatalogo
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaCatalogo(i).IdUnidadMedida, Me.listaCatalogo(i).IdProducto, _
             IdTipoPV, Me.listaCatalogo(i).IdTienda, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text), _
             )

            If objCatalogo IsNot Nothing Then

                Me.listaCatalogo(i).PrecioSD = objCatalogo.PrecioSD
                Me.listaCatalogo(i).PrecioLista = objCatalogo.PrecioLista
                Me.listaCatalogo(i).StockDisponibleN = objCatalogo.StockDisponibleN
                Me.listaCatalogo(i).pvComercial = objCatalogo.pvComercial



            End If

            setListaCatalogo(Me.listaCatalogo)

            DGV_AddProd.DataSource = Me.listaCatalogo
            DGV_AddProd.DataBind()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    ''se grego

    'Protected Sub cboUnidadMedida_Salida_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Try
    '        Dim varv1 As String = cboUnidadMedida_Ingreso.SelectedValue
    '        Dim varv2 As String = cboUnidadMedida_Salida.SelectedValue
    '        ActualizarListaCatalogo()

    '        Me.listaCatalogo = Me.getListaCatalogo
    '        Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

    '        If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
    '            If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
    '                IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
    '            End If
    '        End If

    '        Dim cboUM As DropDownList = CType(sender, DropDownList)
    '        Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

    '        Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaCatalogo(i).IdUnidadMedida, Me.listaCatalogo(i).IdProducto, _
    '         IdTipoPV, Me.listaCatalogo(i).IdTienda, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text), _
    '         )

    '        If objCatalogo IsNot Nothing Then

    '            Me.listaCatalogo(i).PrecioSD = objCatalogo.PrecioSD
    '            Me.listaCatalogo(i).PrecioLista = objCatalogo.PrecioLista
    '            Me.listaCatalogo(i).StockDisponibleN = objCatalogo.StockDisponibleN
    '            Me.listaCatalogo(i).pvComercial = objCatalogo.pvComercial



    '        End If

    '        setListaCatalogo(Me.listaCatalogo)

    '        DGV_AddProd.DataSource = Me.listaCatalogo
    '        DGV_AddProd.DataBind()

    '        objScript.onCapa(Me, "capaBuscarProducto_AddProd")

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try

    'End Sub
    Private Sub ActualizarListaCatalogo()

        Me.listaCatalogo = getListaCatalogo()

        For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

            If (CType(DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text.Trim.Length > 0) Then

                Me.listaCatalogo(i).IdUnidadMedida = CInt(CType(DGV_AddProd.Rows(i).Cells(3).FindControl("cmbUnidadMedida_AddProd"), DropDownList).SelectedValue)
                Me.listaCatalogo(i).Cantidad = CDec(CType(DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)

            End If

        Next

        setListaCatalogo(Me.listaCatalogo)

    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            Dim IdTipoPv As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If Me.ddlTipoAlmacen.Items.Count > 0 Then Me.ddlTipoAlmacen.SelectedIndex = 0
            Dim IdTipoAlmacen As Integer = 0

            If (IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0) Then
                If (CInt(Me.cboTipoPrecioV.SelectedValue) > 0) Then
                    IdTipoPv = CInt(Me.cboTipoPrecioV.SelectedValue)
                End If
            End If


            If IsNumeric(Me.ddlTipoAlmacen.SelectedValue) Then IdTipoAlmacen = CInt(Me.ddlTipoAlmacen.SelectedValue)

            hddIdProductoStockPrecio.Value = CStr(IdProducto)
            txtStockPrecioCant.Text = ""
            txtStockPrecioTotal.Text = ""


            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, IdTipoPv, CInt(Me.cboMoneda.SelectedValue), (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue), IdTipoAlmacen)
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd'); CalcularConsultarStockPrecioxProd(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub ddlTipoAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoAlmacen.SelectedIndexChanged
        onChange_TipoAlmacen()
    End Sub
    Private Sub onChange_TipoAlmacen()
        Dim IdProducto As Integer = CInt(hddIdProductoStockPrecio.Value)
        Dim IdTipoPv As Integer = CInt(Me.hddIdTipoPVDefault.Value)
        Dim IdTipoAlmacen As Integer = 0
        If (IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0) Then
            If (CInt(Me.cboTipoPrecioV.SelectedValue) > 0) Then
                IdTipoPv = CInt(Me.cboTipoPrecioV.SelectedValue)
            End If
        End If

        If IsNumeric(Me.ddlTipoAlmacen.SelectedValue) Then IdTipoAlmacen = CInt(Me.ddlTipoAlmacen.SelectedValue)

        txtStockPrecioCant.Text = ""
        txtStockPrecioTotal.Text = ""


        Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, IdTipoPv, CInt(Me.cboMoneda.SelectedValue), (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue), IdTipoAlmacen)
        Me.GV_ConsultarStockPrecioxProd.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd'); CalcularConsultarStockPrecioxProd(); ", True)

    End Sub

    Protected Sub mostrarCapaConsultarTipoPrecio(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProducto_ConsultarTipoPrecio.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Me.lblTienda_ConsultarTipoPrecio.Text = CStr(Me.cboTienda.SelectedItem.ToString)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            Dim IdTienda As Integer = CInt(Me.cboTienda.SelectedValue)

            Me.GV_ConsultarTipoPrecio.DataSource = (New Negocio.DocumentoCotizacion).DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, CInt(Me.cboMoneda.SelectedValue), CDate(Me.txtFechaEmision.Text))
            Me.GV_ConsultarTipoPrecio.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarTipoPrecio');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnMostrarComponenteKit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            ActualizarListaDetalleDocumento()
            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            'Select Case CInt(Me.hddFrmModo.Value)
            '    Case FrmModo.Nuevo

            '****************** MOSTRAMOS LOS COMPONENTES DEL KIT
            Me.GV_ComponenteKit.DataSource = (New Negocio.Kit).SelectComponentexIdKit(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_ComponenteKit.DataBind()

            '    Case FrmModo.Editar

            ''************* MOSTRAMOS LOS COMPONENTES REGISTRADOS EN EL DOCUMENTO
            'Me.GV_ComponenteKit.DataSource = (New Negocio.DetalleDocumento).SelectComponenteKitxIdKitxIdDocumento(Me.listaDetalleDocumento(index).IdProducto, CInt(Me.hddIdDocumento.Value))
            'Me.GV_ComponenteKit.DataBind()

            'End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub mostrarCapaConsultarTipoPrecio_CapaCambiarTipoPV(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim indexGrilla As Integer = CInt(Me.hddIndexCambiarTipoPV_GV_Detalle.Value)
            Me.lblProducto_ConsultarTipoPrecio.Text = HttpUtility.HtmlDecode(CType(Me.GV_Detalle.Rows(indexGrilla).FindControl("lblDescripcion"), Label).Text)
            Me.lblTienda_ConsultarTipoPrecio.Text = CStr(Me.cboTienda.SelectedItem.ToString)
            Dim IdProducto As Integer = CInt(CType(Me.GV_Detalle.Rows(indexGrilla).FindControl("hddIdProducto"), HiddenField).Value)
            Dim IdTienda As Integer = CInt(Me.cboTienda.SelectedValue)

            Me.GV_ConsultarTipoPrecio.DataSource = (New Negocio.DocumentoCotizacion).DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, CInt(Me.cboMoneda.SelectedValue), CDate(Me.txtFechaEmision.Text))
            Me.GV_ConsultarTipoPrecio.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarTipoPrecio');   onCapa('capaCambiarTipoPrecioPV');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "****************Buscar Personas Mantenimiento"
    Private Sub btnGuardar_EditarCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar_EditarCliente.Click
        editarCliente()
    End Sub

    Private Sub btnGuardar_EditarMO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar_EditarMO.Click
        editarMaestroObra()
    End Sub
    Private Sub editarMaestroObra()

        Try
            Dim objPersonaView As Entidades.PersonaView = obtenerMaestroObraView()
            If ((New Negocio.Cliente).PersonaUpdate_MO(objPersonaView)) Then
                ' cargarPersona(objPersonaView.IdPersona, False)


                Me.DGV_AddProd.DataSource = Nothing
                Me.DGV_AddProd.DataBind()

                Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersonaMaestrosObra(CInt(Me.txtIdMaestroObra_BuscarxId.Text))

                If (objPersona Is Nothing) Then

                    objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS DE MAESTRO RELACIONADO CON ESTE ID")
                Else

                    Me.txtMaestro.Text = objPersona.Descripcion
                    Me.txtRuc_Maestro.Text = objPersona.Ruc
                    Me.txtDni_Maestro.Text = objPersona.Dni
                    Me.hddIdMaestroObra.Value = CStr(objPersona.IdPersona)
                    Me.txtCodigoMaestro.Text = CStr(objPersona.IdPersona)


                    Dim appat As String = objPersona.ApPaterno
                    Dim apmat As String = objPersona.ApMaterno
                    Dim nom As String = objPersona.Nombres
                    Dim dnim As String = objPersona.Dni
                    Dim direccionm As String = objPersona.Direccion
                    Dim telefonom As String = objPersona.Telefeono
                    Dim mailm As String = objPersona.Correo
                    Dim rol As String = objPersona.Rol
                    Me.txtApPaterno_EditarMO.Text = appat
                    Me.txtApMaterno_EditarMO.Text = apmat
                    Me.txtDNI_EditarMO.Text = dnim
                    Me.txtTelefono_MaestroObra.Text = telefonom
                    Me.txtMail_MaestroObra.Text = mailm

                    Me.txtDireccion_EditarMO.Text = direccionm
                    Me.txtNombres_EditarMO.Text = nom

                    ''AGREGADO
                    'Me.cboMODepartamentoE.SelectedValue = objPersona.departamento
                    'Me.cboMODistritoE.SelectedValue = objPersona.distrito
                    'Me.cboMOProvinciaE.SelectedValue = objPersona.provincia

                    Me.hddBusquedaMaestro.Value = "0"

                End If

            Else
                Throw New Exception("Problemas en la Edición del Cliente.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub editarCliente()

        Try
            Dim objPersonaView As Entidades.PersonaView = obtenerPersonaView()
            If ((New Negocio.Cliente).PersonaUpdate_Ventas(objPersonaView)) Then
                cargarPersona(objPersonaView.IdPersona, False)
            Else
                Throw New Exception("Problemas en la Edición del Cliente.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerPersonaView() As Entidades.PersonaView

        Dim objPersonaView As New Entidades.PersonaView
        With objPersonaView

            .IdPersona = CInt(Me.txtCodigoCliente.Text.Trim)
            .RazonSocial = Me.txtRazonSocial_EditarCliente.Text.Trim
            .ApPaterno = Me.txtApPaterno_EditarCliente.Text.Trim
            .ApMaterno = Me.txtApMaterno_EditarCliente.Text.Trim
            .Nombres = Me.txtNombres_EditarCliente.Text.Trim
            .Ruc = Me.txtRUC_EditarCliente.Text.Trim
            ''************
            .Direccion = Me.txtDireccion_EditarCliente.Text.Trim()
            If Me.cboCliDistritoE.SelectedValue <> "00" Then
                .Ubigeo = Me.cboCliDepartamentoE.SelectedValue + Me.cboCliProvinciaE.SelectedValue + Me.cboCliDistritoE.SelectedValue
            End If
            .Dni = Me.txtDNI_EditarCliente.Text.Trim
            .IdTipoAgente = CInt(Me.cboTipoAgente_EditarCliente.SelectedValue)

            .TipoPersona = CInt(Me.cboTipoPersona_PanelCliente.SelectedValue)

            ''agregado
            .Telefeono = Me.txtfonoE.Text.Trim
            .Correo = Me.txtmailE.Text.Trim
            '***** 0: --------
            '***** 1: Natural
            '***** 2: Jurídica

        End With
        Return objPersonaView
    End Function

    Private Function obtenerMaestroObraView() As Entidades.PersonaView

        Dim objPersonaView As New Entidades.PersonaView
        With objPersonaView

            .IdPersona = CInt(Me.txtCodigoMaestro.Text.Trim)
            .RazonSocial = Me.txtRazonSocial_EditarMO.Text.Trim
            .ApPaterno = Me.txtApPaterno_EditarMO.Text.Trim
            .ApMaterno = Me.txtApMaterno_EditarMO.Text.Trim
            .Nombres = Me.txtNombres_EditarMO.Text.Trim
            .Ruc = Me.txtRUC_EditarMO.Text.Trim
            ''************
            .Direccion = Me.txtDireccion_EditarMO.Text.Trim()
            If Me.cboMODistritoE.SelectedValue <> "00" Then
                .Ubigeo = Me.cboMODepartamentoE.SelectedValue + Me.cboMOProvinciaE.SelectedValue + Me.cboMODistritoE.SelectedValue
            End If
            .Dni = Me.txtDNI_EditarMO.Text.Trim
            .IdTipoAgente = 0
            .TipoPersona = 1

            'Dim telefonom As String = objPersonaView.Telefeono
            'Dim mailm As String = objPersonaView.EMail

            'Me.txtTelefono_MaestroObra.Text = telefonom
            'Me.txtMail_MaestroObra.Text = mailm


            .Telefeono = Me.txtTelefono_EditarMO.Text.Trim
            .EMail = Me.txtemail_EditarMO.Text.Trim



            '***** 0: --------
            '***** 1: Natural
            '***** 2: Jurídica

        End With
        Return objPersonaView
    End Function
    Private Sub btnBuscarClientexIdCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarClientexIdCliente.Click
        '************ Limpiamos la grilla de productos
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()

        cargarPersona(CInt(txtIdCliente_BuscarxId.Text))
    End Sub

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO

            If (CInt(Me.hddBusquedaMaestro.Value.Trim) = 1) Then

                ViewState.Add("rol", 3)  '******************** MAESTRO DE OBRA

            Else

                ViewState.Add("rol", 0)  '******************** TODOS

            End If

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try
            '************ Limpiamos la grilla de productos
            Me.DGV_AddProd.DataSource = Nothing
            Me.DGV_AddProd.DataBind()

            If (CInt(Me.hddBusquedaMaestro.Value) = 1) Then  '***************** Búsqueda de Maestros

                Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersonaMaestrosObra(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
                Me.txtMaestro.Text = objPersona.Descripcion
                Me.txtRuc_Maestro.Text = objPersona.Ruc
                Me.txtDni_Maestro.Text = objPersona.Dni
                Me.txtCodigoMaestro.Text = CStr(objPersona.IdPersona)
                Me.txtIdMaestroObra_BuscarxId.Text = CStr(objPersona.IdPersona)



                Dim appat As String = objPersona.ApPaterno
                Dim apmat As String = objPersona.ApMaterno
                Dim nom As String = objPersona.Nombres
                Dim dnim As String = objPersona.Dni
                Dim direccionm As String = objPersona.Direccion
                Dim telefonom As String = objPersona.Telefeono
                Dim mailm As String = objPersona.Correo
                Dim rol As String = objPersona.Rol
                Me.txtApPaterno_EditarMO.Text = appat
                Me.txtApMaterno_EditarMO.Text = apmat
                Me.txtDNI_EditarMO.Text = dnim
                Me.txtTelefono_EditarMO.Text = telefonom
                Me.txtemail_EditarMO.Text = mailm
                Me.txtDireccion_EditarMO.Text = direccionm
                Me.txtNombres_EditarMO.Text = nom

                ''AGREGADO
                'Me.cboMOProvinciaE.SelectedValue = objPersona.provincia
                'Me.cboMODistritoE.SelectedValue = objPersona.distrito
                'Me.cboMODepartamentoE.SelectedValue = objPersona.departamento


                Me.hddIdMaestroObra.Value = CStr(objPersona.IdPersona)

                Me.hddBusquedaMaestro.Value = "0"

            Else
                cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    '********************* CARGAMOS DATOS AL FRM DE COTIZACION SEGUN PERSONA SELECCIONADA

    Protected Sub btnGuardarCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardarCliente.Click
        RegistrarCliente()
    End Sub

    Protected Sub btnGuardarMaestroObra_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardarMaestroObra.Click
        Me.RegistrarMaestroObra()

    End Sub



    Private Sub validar_RegistrarCliente(ByVal objPersonaView As Entidades.PersonaView)

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), (New Integer() {242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254}))

        Select Case objPersonaView.TipoPersona
            Case 0  '**************  NATURAL

                If (listaPermisos(5) > 0 And objPersonaView.ApPaterno.Trim.Length <= 0) Then
                    Throw New Exception("< Ap. Paterno > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(6) > 0 And objPersonaView.ApMaterno.Trim.Length <= 0) Then
                    Throw New Exception("< Ap. Materno > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(7) > 0 And objPersonaView.Nombres.Trim.Length <= 0) Then
                    Throw New Exception("< Nombres > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(8) > 0 And objPersonaView.Dni.Trim.Length <= 0) Then
                    Throw New Exception("< D.N.I. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(9) > 0 And objPersonaView.Ruc.Trim.Length <= 0) Then
                    Throw New Exception("< R.U.C. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(10) > 0 And objPersonaView.Direccion.Trim.Length <= 0) Then
                    Throw New Exception("< Dirección > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(11) > 0 And objPersonaView.Telefeono.Trim.Length <= 0) Then
                    Throw New Exception("< Teléfono > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(12) > 0 And objPersonaView.EMail.Trim.Length <= 0) Then
                    Throw New Exception("< E - Mail > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

            Case 1  '**************  JURIDICA

                If (listaPermisos(0) > 0 And objPersonaView.RazonSocial.Trim.Length <= 0) Then
                    Throw New Exception("< Razón Social > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(1) > 0 And objPersonaView.Telefeono.Trim.Length <= 0) Then
                    Throw New Exception("< Teléfono > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(2) > 0 And objPersonaView.Ruc.Trim.Length <= 0) Then
                    Throw New Exception("< R.U.C. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(3) > 0 And objPersonaView.Direccion.Trim.Length <= 0) Then
                    Throw New Exception("< Dirección > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(4) > 0 And objPersonaView.EMail.Trim.Length <= 0) Then
                    Throw New Exception("< E - Mail > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

        End Select


    End Sub


    Private Sub validar_RegistrarMaestroobra(ByVal objPersonaView As Entidades.PersonaView)

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), (New Integer() {242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254}))

        Select Case objPersonaView.TipoPersona
            Case 0  '**************  NATURAL

                If (listaPermisos(5) > 0 And objPersonaView.ApPaterno.Trim.Length <= 0) Then
                    Throw New Exception("< Ap. Paterno > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(6) > 0 And objPersonaView.ApMaterno.Trim.Length <= 0) Then
                    Throw New Exception("< Ap. Materno > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(7) > 0 And objPersonaView.Nombres.Trim.Length <= 0) Then
                    Throw New Exception("< Nombres > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(8) > 0 And objPersonaView.Dni.Trim.Length <= 0) Then
                    Throw New Exception("< D.N.I. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                'If (listaPermisos(9) > 0 And objPersonaView.Ruc.Trim.Length <= 0) Then
                '    Throw New Exception("< R.U.C. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                'End If

                If (listaPermisos(10) > 0 And objPersonaView.Direccion.Trim.Length <= 0) Then
                    Throw New Exception("< Dirección > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(11) > 0 And objPersonaView.Telefeono.Trim.Length <= 0) Then
                    Throw New Exception("< Teléfono > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                'If (listaPermisos(12) > 0 And objPersonaView.EMail.Trim.Length <= 0) Then
                '    Throw New Exception("< E - Mail > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                'End If

            Case 1  '**************  JURIDICA

                If (listaPermisos(0) > 0 And objPersonaView.RazonSocial.Trim.Length <= 0) Then
                    Throw New Exception("< Razón Social > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(1) > 0 And objPersonaView.Telefeono.Trim.Length <= 0) Then
                    Throw New Exception("< Teléfono > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(2) > 0 And objPersonaView.Ruc.Trim.Length <= 0) Then
                    Throw New Exception("< R.U.C. > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(3) > 0 And objPersonaView.Direccion.Trim.Length <= 0) Then
                    Throw New Exception("< Dirección > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

                If (listaPermisos(4) > 0 And objPersonaView.EMail.Trim.Length <= 0) Then
                    Throw New Exception("< E - Mail > ES UN CAMPO OBLIGATORIO. NO SE PERMITE LA OPERACIÓN.")
                End If

        End Select


    End Sub

    Private Sub RegistrarCliente()

        Try

            Dim objPersonaView As New Entidades.PersonaView
            With objPersonaView

                Select Case Me.cboTipoPersona_RegistrarCliente.SelectedValue
                    Case "N"
                        .TipoPersona = 0
                    Case "J"
                        .TipoPersona = 1
                End Select

                .IdTipoAgente = CInt(Me.cmbTipoAgente_RegistrarCliente.SelectedValue)
                .RazonSocial = Me.txtRazonSocial_Cliente.Text.Trim
                .ApPaterno = Me.txtApPaterno_Cliente.Text.Trim
                .ApMaterno = Me.txtApMaterno_Cliente.Text.Trim
                .Nombres = Me.txtNombres_Cliente.Text.Trim
                .Dni = Me.txtDNI_Cliente.Text.Trim
                .Ruc = Me.txtRUC_Cliente.Text.Trim
                .Direccion = Me.txtDireccion_Cliente.Text.Trim
                If Me.cboCliDistritoN.SelectedValue <> "00" Then
                    .Ubigeo = Me.cboCliDepartamentoN.SelectedValue + Me.cboCliProvinciaN.SelectedValue + Me.cboCliDistritoN.SelectedValue
                End If
                .Telefeono = Me.txtTelefono_Cliente.Text.Trim
                .EMail = Me.txtMail_Cliente.Text

                Select Case Me.cboTipoPersona_RegistrarCliente.SelectedValue
                    Case "N"
                        validar_RegistrarCliente(objPersonaView)
                        .IdPersona = (New Negocio.Persona).RegistrarCliente_Ventas(objPersonaView, Me.cboTipoPersona_RegistrarCliente.SelectedValue, CInt(Me.cboEmpresa.SelectedValue))
                    Case "J"
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Juridica", "jur_Rsocial", CStr(IIf(objPersonaView.RazonSocial Is Nothing, "xzxzxz", objPersonaView.RazonSocial))) <= 0 Then
                            validar_RegistrarCliente(objPersonaView)
                            .IdPersona = (New Negocio.Persona).RegistrarCliente_Ventas(objPersonaView, Me.cboTipoPersona_RegistrarCliente.SelectedValue, CInt(Me.cboEmpresa.SelectedValue))
                        Else
                            objScript.mostrarMsjAlerta(Me, "La Razon social ya existe")
                            objScript.offCapa(Me, "capaRegistrarCliente")
                        End If
                End Select

            End With

            If objPersonaView.IdPersona <> Nothing Then
                cargarPersona(objPersonaView.IdPersona)
                objScript.offCapa(Me, "capaRegistrarCliente")
                'Else
                '    Throw New Exception("Problemas en la Operación.")
            End If


        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('" + ex.Message.Replace(vbCrLf, " ").Replace(vbCr, " ") + "'); onCapa('capaRegistrarCliente'); activarPanel_V1(); ", True)
        End Try

    End Sub
    Private Sub RegistrarMaestroObra()

        Try

            Dim objPersonaView As New Entidades.PersonaView
            With objPersonaView

                Select Case Me.cboTipoPersona_MaestroObra.SelectedValue
                    Case "N"
                        .TipoPersona = 0
                    Case "J"
                        .TipoPersona = 1
                End Select

                .IdTipoAgente = 0 'CInt(Me.cmbTipoAgente_MaestroObra.SelectedValue)
                .RazonSocial = Me.txtRazonSocial_MaestroObra.Text.Trim
                .ApPaterno = Me.txtApPaterno_MaestroObra.Text.Trim
                .ApMaterno = Me.txtApMaterno_MaestroObra.Text.Trim
                .Nombres = Me.txtNombres_MaestroObra.Text.Trim
                .Dni = Me.txtDNI_MaestroObra.Text.Trim
                .Ruc = Me.txtRUC_MaestroObra.Text.Trim
                .Direccion = Me.txtDireccion_MaestroObra.Text.Trim
                If Me.cboMODistritoN.SelectedValue <> "00" Then
                    .Ubigeo = Me.cboMODepartamentoN.SelectedValue + Me.cboMOProvinciaN.SelectedValue + Me.cboMODistritoN.SelectedValue
                End If
                .Telefeono = Me.txtTelefono_MaestroObra.Text.Trim
                .EMail = Me.txtMail_MaestroObra.Text

                If (IsDate(Me.txtFechaNac.Text) And Me.txtFechaNac.Text.Trim.Length > 0) Then
                    .FechaNac = CDate(Me.txtFechaNac.Text)
                End If
                Select Case Me.cboTipoPersona_MaestroObra.SelectedValue
                    Case "N"
                        validar_RegistrarMaestroobra(objPersonaView)
                        .IdPersona = (New Negocio.Persona).Registrar_MaestroObra(objPersonaView, Me.cboTipoPersona_MaestroObra.SelectedValue, CInt(Me.cboEmpresa.SelectedValue))
                    Case "J"
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Juridica", "jur_Rsocial", CStr(IIf(objPersonaView.RazonSocial Is Nothing, "xzxzxz", objPersonaView.RazonSocial))) <= 0 Then
                            validar_RegistrarMaestroobra(objPersonaView)
                            .IdPersona = (New Negocio.Persona).Registrar_MaestroObra(objPersonaView, Me.cboTipoPersona_RegistrarCliente.SelectedValue, CInt(Me.cboEmpresa.SelectedValue))
                        Else
                            objScript.mostrarMsjAlerta(Me, "La Razon social ya existe")
                            objScript.offCapa(Me, "IngresoMaestroObra")
                        End If
                End Select

            End With

            If objPersonaView.IdPersona <> Nothing Then
                'cargarMaestroObra(objPersonaView.IdPersona)


                ' Me.txtRuc_Maestro.Text = objPersonaView.Ruc
                Me.txtIdMaestroObra_BuscarxId.Text = CStr(objPersonaView.IdPersona)
                Me.txtDni_Maestro.Text = objPersonaView.Dni
                Me.hddIdMaestroObra.Value = CStr(objPersonaView.IdPersona)

                Me.txtMaestro.Text = objPersonaView.ApPaterno + " " + objPersonaView.ApMaterno + " " + objPersonaView.Nombres

                Me.hddBusquedaMaestro.Value = "0"

                objScript.offCapa(Me, "IngresoMaestroObra")
                'Else
                '    Throw New Exception("Problemas en la Operación.")
            End If


        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('" + ex.Message.Replace(vbCrLf, " ").Replace(vbCr, " ") + "'); onCapa('IngresoMaestroObra'); activarPanel_V1(); ", True)
        End Try

    End Sub
    'Private Sub cargarMaestroObra(ByVal IdPersona As Integer, Optional ByVal valGv_Detalle As Boolean = True)



    'End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer, Optional ByVal valGv_Detalle As Boolean = True)

        Try

            Dim objPersona As Entidades.PersonaView
            Dim objPersona_Old As Entidades.PersonaView


            If (Me.GV_Detalle.Rows.Count > 0 And valGv_Detalle) Then
                If IsNumeric(Me.hddIdPersona.Value) Then

                    If CInt(Me.hddIdPersona.Value) > 0 And IdPersona <> CInt(Me.hddIdPersona.Value) Then
                        objPersona_Old = (New Negocio.Cliente).SelectxId_Ventas(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue))
                    End If

                End If
            End If

            objPersona = (New Negocio.Cliente).SelectxId_Ventas(IdPersona, CInt(Me.cboEmpresa.SelectedValue))

            If Not IsNothing(objPersona_Old) Then
                If objPersona.IdTipoAgente <> objPersona_Old.IdTipoAgente Or objPersona.IdTipoPV <> objPersona_Old.IdTipoPV Then
                    Throw New Exception("No puede cambiar el Cliente. No se permite la Operación.")
                End If
            End If



            '***************** CARGAMOS DATOS CLIENTE
            Me.txtApPaterno_RazonSocial.Text = objPersona.RazonSocial + objPersona.ApPaterno
            Me.txtApMaterno.Text = objPersona.ApMaterno
            Me.txtNombres.Text = objPersona.Nombres
            Me.txtCodigoCliente.Text = CStr(objPersona.IdPersona)
            Me.txtIdCliente_BuscarxId.Text = CStr(objPersona.IdPersona)
            Me.txtDni.Text = objPersona.Dni
            Me.txtRuc.Text = objPersona.Ruc
            Me.txtDireccionCliente.Text = objPersona.Direccion
            Me.cboTipoPrecioV.SelectedValue = CStr(objPersona.IdTipoPV)
            Me.cboTipoAgente.SelectedValue = CStr(objPersona.IdTipoAgente)
            Me.txtTasaAgente.Text = CStr(Math.Round(objPersona.PorcentTipoAgente, 3))
            Me.cboTipoPersona_PanelCliente.SelectedValue = CStr(objPersona.TipoPersona)
            Me.cborolcliente.SelectedValue = CStr(objPersona.IdRol)
            Me.txtfonoE.Text = objPersona.Telefeono
            Me.txtmailE.Text = objPersona.Correo

            '*************** HIDDEN
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

            '************** LINEA DE CREDITO
            Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectAllConsultaxIdPersona(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
            Me.GV_LineaCredito.DataBind()
            lbl_detalles.Visible = True

            btnVentasMes.Visible = True

            '**************** DATOS DE ENTREGAR POR DEFECTO
            Dim ubigeo As String = objPersona.Ubigeo
            If (ubigeo.Trim.Length <= 0) Then
                ubigeo = "000000"
            End If

            Dim codDepto As String = ubigeo.Substring(0, 2)
            Dim codProv As String = ubigeo.Substring(2, 2)
            Dim codDist As String = ubigeo.Substring(4, 2)
            Dim objCbo As New Combo
            ' *************************************************   UBIGEO PERSONA ---- PUNTO DE LLEGADA

            Me.cboDepartamento.SelectedValue = codDepto : objCbo.LLenarCboProvincia(Me.cboProvincia, codDepto)
            Me.cboProvincia.SelectedValue = codProv : objCbo.LLenarCboDistrito(Me.cboDistrito, codDepto, codProv)
            Me.cboDistrito.SelectedValue = codDist

            If ubigeo <> "000000" Then
                Me.cboCliDepartamentoE.SelectedValue = codDepto : objCbo.LLenarCboProvincia(Me.cboCliProvinciaE, codDepto)
                Me.cboCliProvinciaE.SelectedValue = codProv : objCbo.LLenarCboDistrito(Me.cboCliDistritoE, codDepto, codProv)
                Me.cboCliDistritoE.SelectedValue = codDist
            End If

            ' ************************************************    FIN UBIGEO PERSONA ---- PUNTO DE LLEGADA

            Me.txtDireccion_Llegada.Text = objPersona.Direccion

            Me.GV_BusquedaAvanzado.DataSource = Nothing
            Me.GV_BusquedaAvanzado.DataBind()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            objScript.offCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#End Region



    Protected Sub btnAddDetalleCobro_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDetalleCobro.Click
        addDetalleConcepto()
    End Sub
    Private Sub addDetalleConcepto(Optional ByVal IdConcepto As Integer = 0, Optional ByVal IdProducto As Integer = 0, Optional ByVal Descripcion As String = "", Optional ByVal MontoFlete As Decimal = Decimal.Zero)
        Try

            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(1, IdConcepto, IdProducto, Descripcion, MontoFlete)

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            For i As Integer = 0 To Me.GV_Concepto.Rows.Count - 1

                If CInt(CType(Me.GV_Concepto.Rows(i).FindControl("hddIdProducto"), HiddenField).Value) > 0 Then
                    Dim txt As TextBox = CType(Me.GV_Concepto.Rows(i).FindControl("txtDescripcionConcepto"), TextBox)
                    txt.Attributes.Add("onFocus", "onFocus_ReadOnly(this);")
                End If

            Next

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularTotalConcepto(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaDetalleConcepto(ByVal cantAdd As Integer, Optional ByVal IdConcepto As Integer = 0, Optional ByVal IdProducto As Integer = 0, Optional ByVal Descripcion As String = "", Optional ByVal MontoFlete As Decimal = Decimal.Zero) As List(Of Entidades.DetalleConcepto)
        Dim lista As New List(Of Entidades.DetalleConcepto)

        Dim listaConcepto As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))
        listaConcepto.Insert(0, (New Entidades.Concepto(0, "-----")))

        For i As Integer = 0 To Me.GV_Concepto.Rows.Count - 1
            Dim obj As New Entidades.DetalleConcepto
            With obj
                .IdConcepto = CInt(CType(Me.GV_Concepto.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                .Concepto = CStr(CType(Me.GV_Concepto.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text)
                .Monto = CDec(CType(Me.GV_Concepto.Rows(i).FindControl("txtMonto"), TextBox).Text)
                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                If IsNumeric(CType(Me.GV_Concepto.Rows(i).FindControl("hddIdProducto"), HiddenField).Value) Then
                    .IdProducto = CInt(CType(Me.GV_Concepto.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                End If
                .ListaConcepto = listaConcepto

            End With
            lista.Add(obj)
        Next
        For i As Integer = 0 To cantAdd - 1
            Dim obj As New Entidades.DetalleConcepto
            With obj
                .IdConcepto = 0
                .Descripcion = ""
                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .Monto = 0
                .ListaConcepto = listaConcepto
            End With

            If IdConcepto > 0 Then
                With obj
                    .IdConcepto = IdConcepto
                    .Concepto = Descripcion
                    .Monto = MontoFlete
                    .IdProducto = IdProducto
                End With
            End If


            lista.Add(obj)
        Next

        Return lista
    End Function

    Private Sub GV_Concepto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Concepto.RowDataBound
        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim cboConcepto As DropDownList = CType(e.Row.FindControl("cboConcepto"), DropDownList)
                cboConcepto.DataSource = Me.listaDetalleConcepto(e.Row.RowIndex).ListaConcepto
                cboConcepto.DataBind()
                If (Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto <> 0) Then
                    cboConcepto.SelectedValue = CStr(Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto)
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Concepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Concepto.SelectedIndexChanged
        quitarDetalleConcepto(Me.GV_Concepto.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDetalleConcepto(ByVal Index As Integer)
        Try

            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)
            Me.listaDetalleConcepto.RemoveAt(Index)

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularTotalConcepto(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub cboCondicionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCondicionPago.SelectedIndexChanged
        Try
            actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue))
            actualizarPorcentDctoMax_xArticulo_GV_Detalle()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub




    Private Sub actualizarControlesMedioPago(ByVal IdCondicionPago As Integer)

        Dim objCbo As New Combo
        objCbo.LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, CInt(Me.hddIdTipoDocumento.Value), CInt(Me.cboCondicionPago.SelectedValue))

        'Select Case IdCondicionPago
        '    Case 1  '**************** CONTADO
        '        Me.cboMedioPago.Visible = True
        '        Me.txtNroDiasVigenciaCredito.Visible = False
        '        Me.lblMedioPago_DiasCredito.Text = "Medio de Pago:"
        '    Case 2  '**************** CREDITO
        '        Me.cboMedioPago.Visible = False
        '        Me.txtNroDiasVigenciaCredito.Visible = True
        '        Me.lblMedioPago_DiasCredito.Text = "Vigencia (Días):"
        'End Select

    End Sub

    Private Sub btnAddProductos_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos_AddProd.Click
        addProducto_DetalleDocumento()
        Me.txtCodBarrasCapa.Text = ""
        cursorCodigoBarras()

    End Sub
    Private Sub ActualizarListaDetalleDocumento(Optional ByVal Index As Integer = -1)


        Me.listaDetalleDocumento = getListaDetalleDocumento()
        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            If Index = i Then
                Me.listaDetalleDocumento(i).IdUMold = Me.listaDetalleDocumento(i).IdUnidadMedida
            End If

            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUM"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUM"), DropDownList).SelectedItem.ToString)
            Me.listaDetalleDocumento(i).PrecioSD = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtPrecioVenta"), TextBox).Text)
            Me.listaDetalleDocumento(i).Descuento = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtDescuento_Valor"), TextBox).Text)
            Me.listaDetalleDocumento(i).PorcentDcto = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtDescuento_Porcent"), TextBox).Text)
            Me.listaDetalleDocumento(i).PrecioCD = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtPrecioVentaFinal"), TextBox).Text)
            Me.listaDetalleDocumento(i).Importe = (Me.listaDetalleDocumento(i).Cantidad * Me.listaDetalleDocumento(i).PrecioCD)
            Me.listaDetalleDocumento(i).DctoValor_MaximoxPVenta = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtDctoValor_MaximoxPVenta"), TextBox).Text)
            Me.listaDetalleDocumento(i).DctoPorcent_MaximoxPVenta = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtDctoPorcent_MaximoxPVenta"), TextBox).Text)
            Me.listaDetalleDocumento(i).PorcentDctoMaximo = Me.listaDetalleDocumento(i).DctoPorcent_MaximoxPVenta
            Me.listaDetalleDocumento(i).prod_CodigoBarras = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("hddcodBarraProductoDetDoc"), HiddenField).Value)


            Try : Me.listaDetalleDocumento(i).CantMin = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("hddCantMin"), HiddenField).Value) : Catch ex As Exception : End Try





        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub
    Dim valCant As Boolean
    Private Sub addProducto_DetalleDocumento(Optional ByVal IdProducto As Integer = 0, Optional ByVal Cantidad As Decimal = 0, Optional ByVal MontoFlete As Decimal = Decimal.Zero)
        Try
            Dim Index As Integer = -1
            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaCatalogo()
            ActualizarListaDetalleDocumento()

            Me.listaCatalogo = getListaCatalogo()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            If IdProducto > 0 Then '' Este es el caso de flete
                Index = Me.listaCatalogo.FindIndex(Function(k As Entidades.Catalogo) k.IdProducto = CInt(hddIdProductoStockPrecio.Value))
                Me.listaCatalogo(Index).Cantidad = Cantidad
                Me.listaCatalogo(Index).Flete1 = MontoFlete
            End If

            For i As Integer = 0 To Me.listaCatalogo.Count - 1

                If (Me.listaCatalogo(i).Cantidad > 0) Then
                    valCant = False
                    For k As Integer = 0 To listaDetalleDocumento.Count - 1
                        If listaDetalleDocumento(k).IdProducto = listaCatalogo(i).IdProducto And listaDetalleDocumento(k).IdUnidadMedida = listaCatalogo(i).IdUnidadMedida Then
                            listaDetalleDocumento(k).Cantidad = Me.listaCatalogo(i).Cantidad

                            valCant = True
                            Exit For

                        End If
                    Next
                    If valCant = False Then

                        If IdProducto = 0 Then
                            Me.listaDetalleDocumento.Add(add_objDetalleDocumentoXobjCatalogo(Me.listaCatalogo(i)))
                        ElseIf Me.listaCatalogo(i).IdProducto = IdProducto Then
                            Me.listaDetalleDocumento.Add(add_objDetalleDocumentoXobjCatalogo(Me.listaCatalogo(i)))
                        End If
                    End If

                End If
            Next

            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                Dim Peso As Decimal = 0
                Peso = (New Negocio.Util).fn_ProductoMedida(Me.listaDetalleDocumento(i).IdProducto, Me.listaDetalleDocumento(i).IdUnidadMedida, _IdMagnitud_Peso, CInt(Me.cboPesoTotal.SelectedValue), 1)
                Me.listaDetalleDocumento(i).Peso = Math.Round(Peso, 4, MidpointRounding.AwayFromZero)

            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);   onCapa('capaBuscarProducto_AddProd');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function add_objDetalleDocumentoXobjCatalogo(ByVal oCatalogo As Entidades.Catalogo) As Entidades.DetalleDocumento
        'idoperacionant = (cboTipoOperacion.SelectedValue)
        Dim objDescuentoDetalle As Entidades.Catalogo = New Entidades.Catalogo
        Dim objDetalle As New Entidades.DetalleDocumento
        Try
            Dim IdTipoOperacion As Integer = 0
            Dim IdMedioPago As Integer = 0
            Dim IdTipoPV As Integer = 0
            Dim IdCondicionPago As Integer = 0
            Dim IdUsuario As Integer = 0
            Dim IdProducto As Integer = 0

            IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            IdCondicionPago = CInt(cboCondicionPago.SelectedValue)
            IdUsuario = CInt(Session("IdUsuario"))
            IdProducto = oCatalogo.IdProducto
            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If
            If (oCatalogo.PorcentDctoMaximo = 100) Then
                objDescuentoDetalle = New Negocio.Catalogo().GetDescuentoxIdProducto(IdUsuario, IdCondicionPago, IdMedioPago, IdTipoPV, 1, IdProducto)
                With objDetalle
                    .IdProducto = oCatalogo.IdProducto
                    .NomProducto = oCatalogo.Descripcion
                    .Cantidad = oCatalogo.Cantidad
                    .ListaUM_Venta = oCatalogo.ListaUM_Venta
                    .IdUnidadMedida = oCatalogo.IdUnidadMedida
                    .StockDisponibleN = oCatalogo.StockDisponibleN
                    .PrecioLista = oCatalogo.PrecioLista
                    .PrecioSD = oCatalogo.PrecioSD
                    .Descuento = 0
                    .PorcentDcto = 0
                    .PrecioCD = oCatalogo.PrecioSD
                    .Importe = .Cantidad * .PrecioCD
                    .TasaPercepcion = oCatalogo.Percepcion
                    .PorcentDctoMaximo = oCatalogo.PorcentDctoMaximo
                    .DsctoOriginal = oCatalogo.PorcentDctoMaximo
                    .PrecioBaseDcto = objDescuentoDetalle.PrecioBaseDcto
                    .IdUsuarioSupervisor = 0
                    .IdTienda = oCatalogo.IdTienda
                    .IdTipoPV = oCatalogo.IdTipoPV
                    .pvComercial = oCatalogo.pvComercial
                    .CodigoProducto = oCatalogo.CodigoProducto
                    .Kit = oCatalogo.Kit
                    .ExisteCampania_Producto = oCatalogo.ExisteCampania_Producto
                    .prod_CodigoBarras = oCatalogo.Prod_CodigoBarras
                End With
            Else
                objDescuentoDetalle = New Negocio.Catalogo().GetDescuentoxIdProducto(IdUsuario, IdCondicionPago, IdMedioPago, IdTipoPV, 1, IdProducto)

                With objDetalle
                    .IdProducto = oCatalogo.IdProducto
                    .NomProducto = oCatalogo.Descripcion
                    .Cantidad = oCatalogo.Cantidad
                    .ListaUM_Venta = oCatalogo.ListaUM_Venta
                    .IdUnidadMedida = oCatalogo.IdUnidadMedida
                    .StockDisponibleN = oCatalogo.StockDisponibleN
                    .PrecioLista = oCatalogo.PrecioLista
                    .PrecioSD = oCatalogo.PrecioSD
                    .Descuento = 0
                    .PorcentDcto = 0
                    .PrecioCD = oCatalogo.PrecioSD
                    .Importe = .Cantidad * .PrecioCD
                    .TasaPercepcion = oCatalogo.Percepcion
                    .PorcentDctoMaximo = objDescuentoDetalle.PorcentDctoMaximo
                    .DsctoOriginal = objDescuentoDetalle.PorcentDctoMaximo
                    .PrecioBaseDcto = objDescuentoDetalle.PrecioBaseDcto
                    .IdUsuarioSupervisor = 0
                    .IdTienda = oCatalogo.IdTienda
                    .IdTipoPV = oCatalogo.IdTipoPV
                    .pvComercial = oCatalogo.pvComercial
                    .CodigoProducto = oCatalogo.CodigoProducto
                    .Kit = oCatalogo.Kit
                    .ExisteCampania_Producto = oCatalogo.ExisteCampania_Producto
                    .prod_CodigoBarras = oCatalogo.Prod_CodigoBarras
                End With
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        Return objDetalle
    End Function
    Private Sub Actualizargrilla()

        'Me.listaCatalogo = Me.getListaCatalogo
        'Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

        'If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
        '    If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
        '        IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
        '    End If
        'End If

        'Dim cboUM As DropDownList = CType(sender, DropDownList)
        'Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

        'Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaCatalogo(i).IdUnidadMedida, Me.listaCatalogo(i).IdProducto, _
        ' IdTipoPV, Me.listaCatalogo(i).IdTienda, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text), _
        ' )

        'If objCatalogo IsNot Nothing Then

        '    Me.listaCatalogo(i).PrecioSD = objCatalogo.PrecioSD
        '    Me.listaCatalogo(i).PrecioLista = objCatalogo.PrecioLista
        '    Me.listaCatalogo(i).StockDisponibleN = objCatalogo.StockDisponibleN
        '    Me.listaCatalogo(i).pvComercial = objCatalogo.pvComercial



        'End If

        'setListaCatalogo(Me.listaCatalogo)

        'DGV_AddProd.DataSource = Me.listaCatalogo
        'DGV_AddProd.DataBind()

        'objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        'Catch ex As Exception
        '    objScript.mostrarMsjAlerta(Me, ex.Message)
        'End Try
    End Sub
    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try
            hddTipoOperacion.Value = (cboTipoOperacion.SelectedValue)
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUM"), DropDownList)
                If Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta Is Nothing Then
                    Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(Me.listaDetalleDocumento(e.Row.RowIndex).IdProducto, CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoPrecioV.SelectedValue))
                    cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta
                    cboUM.DataBind()
                    If (cboUM.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                        cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString
                    End If
                Else

                    cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM_Venta
                    cboUM.DataBind()
                    ' ActualizarListaDetalleDocumento()
                    If (cboUM.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                        cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString
                    End If
                End If

                If (cboUM.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                    cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString
                End If

                If (Me.listaDetalleDocumento(e.Row.RowIndex).VolumenVentaMin > 0) Then
                    cboUM.Enabled = False
                End If

                If (Me.listaDetalleDocumento(e.Row.RowIndex).Kit) Then  '********* KIT
                    CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton).Visible = True
                Else
                    CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton).Visible = False
                End If

                Dim btnVerCampania As ImageButton = CType(e.Row.FindControl("btnViewCampania"), ImageButton)
                If (Me.listaDetalleDocumento(e.Row.RowIndex).ExisteCampania_Producto) And Me.listaDetalleDocumento(e.Row.RowIndex).PorcentDctoMaximo < 100 Then
                    btnVerCampania.Visible = True
                Else
                    btnVerCampania.Visible = False
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    'cboUnidadMedida_Salida_SelectedIndexChanged



    Private Sub quitarDetalleDocumento(ByVal index As Integer)

        Try

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim Idcampania As Integer = listaDetalleDocumento(index).IdCampania
            Dim IdCampaniaDetalle As Integer = listaDetalleDocumento(index).IdCampaniaDetalle
            Dim IdProducto As Integer = listaDetalleDocumento(index).IdProducto

            Me.listaDetalleDocumento.RemoveAt(index)

            Dim lista As List(Of Entidades.DetalleConcepto) = obtenerListaDetalleConcepto(0)

            Dim IndexConcepto As Integer = -1

            IndexConcepto = lista.FindIndex(Function(k As Entidades.DetalleConcepto) k.IdProducto = IdProducto)

            If IndexConcepto > -1 Then
                quitarDetalleConcepto(IndexConcepto)
            End If



            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    Me.listaDetalleDocumento.RemoveAll(Function(e As Entidades.DetalleDocumento) e.IdCampania = Idcampania And e.IdProductoAux = IdProducto And e.IdCampaniaDetalle = IdCampaniaDetalle)

                Case FrmModo.Editar

                    Me.listaDetalleDocumento.RemoveAll(Function(e As Entidades.DetalleDocumento) e.IdCampania = Idcampania And e.IdProductoAux = IdProducto)

            End Select



            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub



    Private Sub actualizarPorcentDctoMax_xArticulo_GV_Detalle()

        Dim IdUsuario As Integer = CInt(Session("IdUsuario"))
        Dim IdCondicionPago As Integer = CInt(Me.cboCondicionPago.SelectedValue)
        Dim IdMedioPago As Integer = 0
        'If (IdCondicionPago = 1) Then  '************* CONTADO
        '    IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
        'End If
        If IsNumeric(Me.cboMedioPago.SelectedValue) Then
            IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
        End If

        Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
        If IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
            If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
            End If
        End If

        ActualizarListaDetalleDocumento()
        '  Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)

        Dim index As Integer = 0

        Me.listaDetalleDocumento = getListaDetalleDocumento()


        Dim IdTipoOperacion As Integer = 0
        IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
        Dim CodigoProd As String = Me.listaDetalleDocumento(index).CodigoProducto





        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            If Me.listaDetalleDocumento(i).PorcentDctoMaximo < 100 Then

                If (Me.listaDetalleDocumento(i).IdUsuarioSupervisor <> Nothing) Then
                    ' Me.listaDetalleDocumento(i).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuento).getPorcentDctoMax(Me.listaDetalleDocumento(i).IdUsuarioSupervisor, IdTipoPV, IdCondicionPago, IdMedioPago)
                    Me.listaDetalleDocumento(i).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuentoAvanzado).getPorcentDctoMaxavz(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago, IdTipoOperacion, CodigoProd)
                    Me.listaDetalleDocumento(i).PrecioBaseDcto = (New Negocio.ConfiguracionDescuento).getPrecioBaseDcto(Me.listaDetalleDocumento(i).IdUsuarioSupervisor, IdTipoPV, IdCondicionPago, IdMedioPago)


                Else
                    ' Me.listaDetalleDocumento(i).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuento).getPorcentDctoMax(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
                    Me.listaDetalleDocumento(i).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuentoAvanzado).getPorcentDctoMaxavz(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago, IdTipoOperacion, CodigoProd)
                    Me.listaDetalleDocumento(i).PrecioBaseDcto = (New Negocio.ConfiguracionDescuento).getPrecioBaseDcto(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
                End If


                '  -------------------------------
                '     Me.listaDetalleDocumento(i).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuentoAvanzado).getPorcentDctoMaxavz(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago, IdTipoOperacion, CodigoProd)

                '    Me.listaDetalleDocumento(i).PrecioBaseDcto = (New Negocio.ConfiguracionDescuento).getPrecioBaseDcto(Me.listaDetalleDocumento(i).IdUsuarioSupervisor, IdTipoPV, IdCondicionPago, IdMedioPago)
                'Else
                '       Me.listaDetalleDocumento(i).PorcentDctoMaximo = (New Negocio.ConfiguracionDescuento).getPrecioBaseDcto(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
                'End If



                ' --------------------------------




            End If

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

    End Sub


    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged
        Try
            actualizarPorcentDctoMax_xArticulo_GV_Detalle()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    'Private Function obtener_Cad_IdProducto(ByVal lista As List(Of Entidades.DetalleDocumento)) As String

    '    Dim cad As String = ""
    '    For i As Integer = 0 To lista.Count - 1
    '        If (i = lista.Count - 1) Then
    '            cad = cad + CStr(lista(i).IdProducto)
    '        Else
    '            cad = cad + CStr(lista(i).IdProducto) + "/"
    '        End If
    '    Next
    '    Return cad
    'End Function

    'Private Function obtener_Cad_IdUnidadMedida(ByVal lista As List(Of Entidades.DetalleDocumento)) As String

    '    Dim cad As String = ""
    '    For i As Integer = 0 To lista.Count - 1
    '        If (i = lista.Count - 1) Then
    '            cad = cad + CStr(lista(i).IdUnidadMedida)
    '        Else
    '            cad = cad + CStr(lista(i).IdUnidadMedida) + "/"
    '        End If
    '    Next
    '    Return cad
    'End Function
    'Private Function obtener_Cad_Cantidad(ByVal lista As List(Of Entidades.DetalleDocumento)) As String

    '    Dim cad As String = ""
    '    For i As Integer = 0 To lista.Count - 1
    '        If (i = lista.Count - 1) Then
    '            cad = cad + CStr(lista(i).Cantidad)
    '        Else
    '            cad = cad + CStr(lista(i).Cantidad) + "/"
    '        End If
    '    Next
    '    Return cad
    'End Function
    Private Sub btnAceptar_AddDctoAdicional_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_AddDctoAdicional.Click
        If (Me.Chkmasivo.Checked = False) Then
            addDctoAdicionalxArticulo_GV_Detalle()
        End If

        If (Me.Chkmasivo.Checked = True) Then
            addDctoAdicionalxArticulo_GV_Detalle_Masivo()
        End If


    End Sub
    Private Sub addDctoAdicionalxArticulo_GV_Detalle()
        Try

            Dim IdUsuario As Integer = (New Negocio.Usuario).ValidarIdentidad(Me.txtLogin_AddDctoAdicional.Text.Trim, Me.txtClave_AddDctoAdicional.Text.Trim)
            Dim IdCondicionPago As Integer = CInt(Me.cboCondicionPago.SelectedValue)
            Dim IdMedioPago As Integer = 0
            'If (IdCondicionPago = 1) Then  '************* CONTADO
            IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            'End If

            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            If IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If



            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            Dim IdTipoOperacion As Integer = 0
            IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim CodigoProd As String = Me.listaDetalleDocumento(index).CodigoProducto
            Dim SetPorcentDctoMaximo As Decimal = CDec(Me.txtDescuentoAdicional.Text)
            Dim PorcentDctoMaximo As Decimal = (New Negocio.ConfiguracionDescuentoAvanzado).getPorcentDctoMaxavz(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago, IdTipoOperacion, CodigoProd)


            If IdUsuario = 0 Then
                Throw New Exception("EL DESCUENTO NO ES ACCESIBLE.")
            End If

            If SetPorcentDctoMaximo > PorcentDctoMaximo Then
                Throw New Exception("EL DESCUENTO NO ES ACCESIBLE.")
            End If

            Me.listaDetalleDocumento(index).PorcentDctoMaximo = SetPorcentDctoMaximo
            'Me.listaDetalleDocumento(index).PrecioBaseDcto = (New Negocio.ConfiguracionDescuento).getPrecioBaseDcto(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
            Me.listaDetalleDocumento(index).IdUsuarioSupervisor = IdUsuario

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub addDctoAdicionalxArticulo_GV_Detalle_Masivo()
        Try

            Dim IdUsuario As Integer = (New Negocio.Usuario).ValidarIdentidad(Me.txtLogin_AddDctoAdicional.Text.Trim, Me.txtClave_AddDctoAdicional.Text.Trim)
            Dim IdCondicionPago As Integer = CInt(Me.cboCondicionPago.SelectedValue)
            Dim IdMedioPago As Integer = 0

            IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)

            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            Dim IdTipoOperacion As Integer = 0

            IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            ActualizarListaDetalleDocumento()



            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim CodigoProd As String = Me.listaDetalleDocumento(index).CodigoProducto
            Dim SetPorcentDctoMaximo As Decimal = CDec(Me.txtDescuentoAdicional.Text)
            Dim PorcentDctoMaximo As Decimal = (New Negocio.ConfiguracionDescuentoAvanzado).getPorcentDctoMaxavz(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago, IdTipoOperacion, CodigoProd)

            If IdUsuario = 0 Then
                Throw New Exception("EL DESCUENTO NO ES ACCESIBLE.")
            End If

            If SetPorcentDctoMaximo > PorcentDctoMaximo Then
                Throw New Exception("EL DESCUENTO NO ES ACCESIBLE.")
            End If

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1
                If index = i Then


                End If
                Me.listaDetalleDocumento(i).PorcentDctoMaximo = SetPorcentDctoMaximo
                Me.listaDetalleDocumento(i).IdUsuarioSupervisor = IdUsuario



            Next




            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        Me.Chkmasivo.Checked = False


    End Sub

    Private Sub btnAddGlosa_CapaGlosa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddGlosa_CapaGlosa.Click

        addGlosaDetalle()

    End Sub
    Private Sub addGlosaDetalle()
        Try

            Dim glosa As String = Me.txtGlosa_CapaGlosa.Text.Trim

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim index As Integer = CInt(Me.hddIndexGlosa_Gv_Detalle.Value)
            Me.listaDetalleDocumento(index).DetalleGlosa = Me.txtGlosa_CapaGlosa.Text.Trim

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cboConcepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        addConcepto_Procesar(CType(sender, DropDownList))

    End Sub

    Private Sub addConcepto_Procesar(ByVal cboConcepto As DropDownList)
        Try

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim ubigeoOrigen As String = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue
            Dim ubigeoDestino As String = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue
            Dim pesoTotal As Decimal = obtenerPesoTotal(Me.listaDetalleDocumento)
            Dim importeTotal As Decimal = CDec(Me.txtTotal.Text.Trim)
            Dim valor As Decimal = (New Negocio.DocumentoCotizacion).SelectValorEstimadoConcepto(CInt(cboConcepto.SelectedValue), CInt(Me.hddIdTipoDocumento.Value), ubigeoOrigen, ubigeoDestino, CInt(Me.cboMoneda.SelectedValue), pesoTotal, importeTotal, CDate(Me.txtFechaEmision.Text))

            Dim index As Integer = CType(cboConcepto.NamingContainer, GridViewRow).RowIndex

            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)
            Me.listaDetalleConcepto(index).Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
            Me.listaDetalleConcepto(index).Monto = CDec(valor)
            Me.listaDetalleConcepto(index).IdMoneda = CInt(Me.cboMoneda.SelectedValue)

            Me.GV_Concepto.DataSource = Me.listaDetalleConcepto
            Me.GV_Concepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularTotalConcepto(); ", True)

        Catch ex As Exception

            cboConcepto.SelectedValue = "0"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularTotalConcepto();     alert('" + ex.Message + "');    ", True)

        End Try
    End Sub

    Private Function obtenerPesoTotal(ByVal listaDetalle As List(Of Entidades.DetalleDocumento)) As Decimal

        Dim pesoTotal As Decimal = 0

        For i As Integer = 0 To listaDetalle.Count - 1

            pesoTotal = pesoTotal + (New Negocio.ProductoMedida).getMagnitudProducto(listaDetalle(i).IdProducto, listaDetalle(i).IdUnidadMedida, listaDetalle(i).Cantidad, 1, 0)

        Next

        Return pesoTotal

    End Function

    Private Sub cboDepto_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepto_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboProvincia_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnConsultarVolumenVentaMin_Detalle_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)

        verVolumenVentaMinimo()

    End Sub

    Private Sub verVolumenVentaMinimo()

        Try

            Dim index As Integer = CInt(Me.hddIndexCambiarTipoPV_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim IdProducto As Integer = Me.listaDetalleDocumento(index).IdProducto
            Me.lblProducto_CambiarTipoPV_GV_Detalle.Text = Me.listaDetalleDocumento(index).NomProducto

            Me.GV_VolumenVentaMin.DataSource = (New Negocio.VolumenVenta).SelectxIdProductoxIdTipoPVxIdTienda(IdProducto, 0, CInt(Me.cboTienda.SelectedValue))
            Me.GV_VolumenVentaMin.DataBind()

            objScript.onCapa(Me, "capaCambiarTipoPrecioPV")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub valChange_TipoPrecioPV()
        Try
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim Cad_IdProducto, cad_IdUnidadMedida, cad_Cantidad As String
            Dim index As Integer = CInt(Me.hddIndexCambiarTipoPV_GV_Detalle.Value)
            Cad_IdProducto = CStr(Me.listaDetalleDocumento(index).IdProducto)
            cad_IdUnidadMedida = CStr(Me.listaDetalleDocumento(index).IdUnidadMedida)
            cad_Cantidad = CStr(Me.listaDetalleDocumento(index).Cantidad)

            Dim IdUsuario As Integer = CInt(Session("IdUsuario"))
            If (Me.listaDetalleDocumento(index).IdUsuarioSupervisor <> Nothing) Then
                IdUsuario = Me.listaDetalleDocumento(index).IdUsuarioSupervisor
            End If
            Dim IdMedioPago As Integer = 0
            If (CInt(Me.cboCondicionPago.SelectedValue) = 1) Then  '********** CONTADO
                IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            End If


            Dim IdTipoOperacion As Integer = 0
            IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)

            Dim objCatalogo As Entidades.Catalogo = ((New Negocio.DocumentoCotizacion).DocumentoCotizacion_ValSelectTipoPrecioPV(IdUsuario, CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, CDate(Me.txtFechaEmision.Text), CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdPersona.Value), CInt(Me.cboTipoPrecioPV_CambiarDetalle.SelectedValue), Cad_IdProducto, cad_IdUnidadMedida, cad_Cantidad, IdTipoOperacion))


            '********* ALMACENAMOS LOS VALORES
            Me.listaDetalleDocumento(index).PrecioLista = objCatalogo.PrecioLista
            Me.listaDetalleDocumento(index).pvComercial = objCatalogo.pvComercial
            Me.listaDetalleDocumento(index).PrecioCD = objCatalogo.PrecioLista
            Me.listaDetalleDocumento(index).PrecioSD = objCatalogo.PrecioLista
            Me.listaDetalleDocumento(index).PorcentDctoMaximo = objCatalogo.PorcentDctoMaximo
            Me.listaDetalleDocumento(index).DsctoOriginal = objCatalogo.PorcentDctoMaximo
            Me.listaDetalleDocumento(index).PrecioBaseDcto = objCatalogo.PrecioBaseDcto
            Me.listaDetalleDocumento(index).VolumenVentaMin = objCatalogo.VolumenVenta
            Me.listaDetalleDocumento(index).IdTipoPV = CInt(Me.cboTipoPrecioPV_CambiarDetalle.SelectedValue)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnCambiarTipoPrecioVenta_Detalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCambiarTipoPrecioVenta_Detalle.Click
        valChange_TipoPrecioPV()
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        registrarCotizacion()
    End Sub
    Private Sub validarFrm()

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {161})

        If (listaPermisos(0) <= 0) Then '************** DESCUENTO SOBRE MAESTRO DE OBRA

            '************* VALIDAMOS
            If (IsNumeric(Me.hddIdMaestroObra.Value) And Me.hddIdMaestroObra.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdMaestroObra.Value) > 0) Then

                    Me.listaDetalleDocumento = getListaDetalleDocumento()

                    For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                        If (Me.listaDetalleDocumento(i).PrecioCD < Me.listaDetalleDocumento(i).PrecioLista) Then
                            Throw New Exception("EL PRODUCTO < " + Me.listaDetalleDocumento(i).NomProducto + " > TIENE UN DESCUENTO POR DEBAJO DEL PRECIO DE LISTA. NO TIENE LOS PERMISOS NECESARIOS PARA DAR ESTE TIPO DE DESCUENTO. NO SE PERMITE LA OPERACIÓN < RESTRICCIÓN : MAESTROS >")
                        End If

                    Next

                End If
            End If
        End If

        If CInt(Me.cboTipoOperacion.SelectedValue) = 4 And Me.GV_LineaCredito.Rows.Count = 0 Then
            Throw New Exception("EL CLIENTE NO POSEE LINEA DE CRÉDITO. NO SE PERMITE LA OPERACIÓN < RESTRICCIÓN : TIPO DE OPERACIÓN >")
        End If


    End Sub
    Private Sub registrarCotizacion()

        Try

            '**************** VALIDAMOS LOS CONCEPTOS
            If (Me.GV_Concepto.Rows.Count > 0) Then

                validarListaConceptos()

            End If


            ActualizarListaDetalleDocumento()

            validarFrm()

            '********************** ACTUALIZAMOS EL DETALLE DOCUMENTO - ADD COMPONENTES AL DETALLE - KIT
            ActualizarListaDetalleDocumento_KIT()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim objAnexoDocumento As Entidades.Anexo_Documento = obtenerAnexoDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim objMontoRegimenPercepcion As Entidades.MontoRegimen = obtenerMontoRegimen_Percepcion()
            Dim objPuntoPartida As Entidades.PuntoPartida = obtenerPuntoPartida()
            Dim objPuntoLlegada As Entidades.PuntoLlegada = obtenerPuntoLlegada()
            Dim listaAnexoDetalleDocumento As List(Of Entidades.Anexo_DetalleDocumento) = obtenerListaAnexoDetalleDocumento()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    objDocumento.Id = (New Negocio.DocumentoCotizacion).registrarCotizacion(objDocumento, objAnexoDocumento, Me.listaDetalleDocumento, listaAnexoDetalleDocumento, Me.listaDetalleConcepto, objObservaciones, objPuntoPartida, objPuntoLlegada, objMontoRegimenPercepcion, listaRelacionDocumento)

                Case FrmModo.Editar

                    objDocumento.Id = (New Negocio.DocumentoCotizacion).updateCotizacion(objDocumento, objAnexoDocumento, Me.listaDetalleDocumento, listaAnexoDetalleDocumento, Me.listaDetalleConcepto, objObservaciones, objPuntoPartida, objPuntoLlegada, objMontoRegimenPercepcion, listaRelacionDocumento)

            End Select

            If (objDocumento.Id > 0) Then

                Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, True)
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim listaRelacionDocumento As New List(Of Entidades.RelacionDocumento)

        Me.listaDocumentoRef = getlistaDocumentoRef()
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1
            Dim objRelacionDocumento As New Entidades.RelacionDocumento

            With objRelacionDocumento
                .IdDocumento1 = Me.listaDocumentoRef(i).Id
            End With

            listaRelacionDocumento.Add(objRelacionDocumento)

        Next

        Return listaRelacionDocumento

    End Function

    Private Sub ActualizarListaDetalleDocumento_KIT()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            If (Me.listaDetalleDocumento(i).Kit) Then  '*********** KIT

                '****************** OBTENEMOS EL DETALLE DOCUMENTO
                Dim listaComponenteKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponenteDetallexParams(Me.listaDetalleDocumento(i).IdProducto, Me.listaDetalleDocumento(i).IdUnidadMedida, Me.listaDetalleDocumento(i).Cantidad, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoPrecioV.SelectedValue))

                For j As Integer = 0 To listaComponenteKit.Count - 1

                    Dim objDetalleDocumento As New Entidades.DetalleDocumento
                    With objDetalleDocumento

                        .ComponenteKit = True
                        .IdKit = Me.listaDetalleDocumento(i).IdProducto

                        .IdProducto = listaComponenteKit(j).IdComponente
                        .IdUnidadMedida = listaComponenteKit(j).IdUnidadMedida_Comp
                        .Cantidad = listaComponenteKit(j).CantidadTotal_Comp
                        .PrecioLista = listaComponenteKit(j).PrecioListaUnitEqKit_Comp
                        .PrecioCD = listaComponenteKit(j).PrecioUnitEqKit_Comp
                        .PrecioSD = listaComponenteKit(j).PrecioUnitEqKit_Comp
                        .Importe = .PrecioCD * .Cantidad
                        .UMedida = listaComponenteKit(j).UnidadMedida_Comp

                        .IdTipoPV = CInt(Me.cboTipoPrecioV.SelectedValue)

                    End With

                    '******************** AGREGAMOS EL COMPONENTE AL DETALLE
                    Me.listaDetalleDocumento.Add(objDetalleDocumento)

                Next

            End If

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub

    Private Sub validarListaConceptos()

        '************** EL ERROR ES GENERADO DESDE LA MISMA BASE DE DATOS

        Me.listaDetalleConcepto = obtenerListaDetalleConcepto(0)

        ActualizarListaDetalleDocumento()
        Me.listaDetalleDocumento = getListaDetalleDocumento()

        Dim ubigeoOrigen As String = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue
        Dim ubigeoDestino As String = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue
        Dim pesoTotal As Decimal = obtenerPesoTotal(Me.listaDetalleDocumento)
        Dim importeTotal As Decimal = CDec(Me.txtTotal.Text.Trim)

        Dim objDocumentoCotizacion As New Negocio.DocumentoCotizacion

        For i As Integer = 0 To Me.listaDetalleConcepto.Count - 1

            objDocumentoCotizacion.SelectValorEstimadoConcepto(Me.listaDetalleConcepto(i).IdConcepto, CInt(Me.hddIdTipoDocumento.Value), ubigeoOrigen, ubigeoDestino, CInt(Me.cboMoneda.SelectedValue), pesoTotal, importeTotal, CDate(Me.txtFechaEmision.Text))

        Next

    End Sub

    Private Function obtenerListaAnexoDetalleDocumento() As List(Of Entidades.Anexo_DetalleDocumento)
        Dim lista As New List(Of Entidades.Anexo_DetalleDocumento)

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            Dim obj As New Entidades.Anexo_DetalleDocumento
            With obj

                .IdDocumento = Me.listaDetalleDocumento(i).IdDocumento
                .IdDetalleDocumento = Me.listaDetalleDocumento(i).IdDetalleDocumento
                .IdTipoPV = Me.listaDetalleDocumento(i).IdTipoPV

                .ComponenteKit = Me.listaDetalleDocumento(i).ComponenteKit
                .IdKit = Me.listaDetalleDocumento(i).IdKit
                .IdCampania = Me.listaDetalleDocumento(i).IdCampania
                'If Me.listaDetalleDocumento(i).FactorTG <= 0 Then
                '    .IdCampania = 0
                'End If
                .IdProductoRef = Me.listaDetalleDocumento(i).IdProductoAux

            End With
            lista.Add(obj)

        Next

        Return lista

    End Function

    Private Function obtenerAnexoDocumento() As Entidades.Anexo_Documento

        Dim objAnexoDocumento As New Entidades.Anexo_Documento
        With objAnexoDocumento

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)

            'Select Case CInt(Me.cboCondicionPago.SelectedValue)
            '    Case 1 '********** CONTADO
            '        .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            '        .NroDiasVigencia = Nothing

            '    Case 2 '********** CREDITO
            '        '.IdMedioPago = Nothing
            '        '.NroDiasVigencia = CInt(Me.txtNroDiasVigenciaCredito.Text)
            'End Select

            .TotalConcepto = CDec(Me.txtTotalConcepto.Text)

            If (IsNumeric(Me.hddIdMaestroObra.Value.Trim) And (Me.hddIdMaestroObra.Value.Trim.Length > 0)) Then

                If (CInt(Me.hddIdMaestroObra.Value.Trim) > 0) Then

                    objAnexoDocumento.IdMaestroObra = CInt(Me.hddIdMaestroObra.Value)

                End If

            End If

        End With
        Return objAnexoDocumento
    End Function

    Private Function obtenerPuntoLlegada() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing
        If (Me.txtDireccion_Llegada.Text.Trim.Length > 0) Then
            objPuntoLlegada = New Entidades.PuntoLlegada
            With objPuntoLlegada



                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .pll_Direccion = Me.txtDireccion_Llegada.Text.Trim
                .IdAlmacen = Nothing
                .pll_Ubigeo = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue

            End With
        End If

        Return objPuntoLlegada

    End Function

    Private Function obtenerPuntoPartida() As Entidades.PuntoPartida

        Dim objPuntoPartida As Entidades.PuntoPartida = Nothing
        If (Me.txtDireccion_Partida.Text.Trim.Length > 0) Then

            objPuntoPartida = New Entidades.PuntoPartida

            With objPuntoPartida

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .Direccion = Me.txtDireccion_Partida.Text.Trim
                .IdAlmacen = Nothing
                .Ubigeo = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue

            End With
        End If

        Return objPuntoPartida

    End Function

    Private Function obtenerMontoRegimen_Percepcion() As Entidades.MontoRegimen

        Dim objMontoRegimen As Entidades.MontoRegimen = Nothing

        If (CDec(Me.txtPercepcion.Text.Trim) > 0) Then

            objMontoRegimen = New Entidades.MontoRegimen
            With objMontoRegimen

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .Monto = CDec(Me.txtPercepcion.Text.Trim)
                .ro_Id = 1 '***************** PERCEPCION

            End With

        End If

        Return objMontoRegimen

    End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdMotivoT = CInt(Me.cboMotivoTraslado.SelectedValue)
            .IdCondicionPago = CInt(Me.cboCondicionPago.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacenReferencia.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdUsuarioComision = CInt(Me.cboUsuarioComision.SelectedValue)

            Try
                .FechaVenc = CDate(Me.txtFechaVcto.Text)
            Catch ex As Exception
                .FechaVenc = Nothing
            End Try
            Try
                .FechaAEntregar = CDate(Me.txtFechaAEntregar.Text)
            Catch ex As Exception
                .FechaAEntregar = Nothing
            End Try

            .IdPersona = CInt(Me.hddIdPersona.Value)
            .IdDestinatario = CInt(Me.hddIdPersona.Value)
            .IdTipoPV = CInt(Me.cboTipoPrecioV.SelectedValue)


            .Descuento = CDec(Me.txtDescuento.Text)
            .Total = CDec(Me.txtTotal.Text)
            .SubTotal = CDec(Me.txtSubTotal.Text)
            .IGV = CDec(Me.txtIGV.Text)
            .ImporteTotal = CDec(Me.txtImporteTotal.Text)

            .TotalAPagar = CDec(Me.txtTotalAPagar.Text)
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

            .TotalLetras = (New Negocio.ALetras).Letras(CStr(Math.Round(CDec(Me.txtTotal.Text) + CDec(Me.txtTotalConcepto_PanelDetalle.Text), 2)))

            Select Case .IdMoneda
                Case 1
                    .TotalLetras = (.TotalLetras + "/100 SOLES").ToUpper
                Case 2
                    .TotalLetras = (.TotalLetras + "/100 DÓLARES AMERICANOS").ToUpper
            End Select

        End With

        Return objDocumento

    End Function

    Protected Sub cboUnidadMedida_GV_Detalle(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

            ActualizarListaDetalleDocumento(i)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaDetalleDocumento(i).IdUnidadMedida, Me.listaDetalleDocumento(i).IdProducto, _
              IdTipoPV, CInt(IIf(Me.listaDetalleDocumento(i).IdTienda = 0, CInt(cboTienda.SelectedValue), Me.listaDetalleDocumento(i).IdTienda)), CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text), _
              listaDetalleDocumento(i).Cantidad, listaDetalleDocumento(i).IdUMold)

            If objCatalogo IsNot Nothing Then

                If objCatalogo.Cantidad > 0 Then
                    Me.listaDetalleDocumento(i).Cantidad = objCatalogo.Cantidad
                    listaDetalleDocumento(i).IdUMold = 0
                End If
                Me.listaDetalleDocumento(i).PrecioSD = objCatalogo.PrecioSD
                Me.listaDetalleDocumento(i).PrecioLista = objCatalogo.PrecioLista
                Me.listaDetalleDocumento(i).pvComercial = objCatalogo.pvComercial
                Me.listaDetalleDocumento(i).PrecioCD = objCatalogo.PrecioLista
                Me.listaDetalleDocumento(i).StockDisponibleN = objCatalogo.StockDisponibleN
                Me.listaDetalleDocumento(i).Descuento = 0
                Me.listaDetalleDocumento(i).PorcentDcto = 0
            End If



            Dim Peso As Decimal = 0
            Peso = (New Negocio.Util).fn_ProductoMedida(Me.listaDetalleDocumento(i).IdProducto, Me.listaDetalleDocumento(i).IdUnidadMedida, _IdMagnitud_Peso, CInt(Me.cboPesoTotal.SelectedValue), 1)
            Me.listaDetalleDocumento(i).Peso = Math.Round(Peso, 4, MidpointRounding.AwayFromZero)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            GV_Detalle.DataSource = Me.listaDetalleDocumento
            GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub actualizadespuesdecambiocmb(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

            ActualizarListaDetalleDocumento(i)
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If
            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaDetalleDocumento(i).IdUnidadMedida, Me.listaDetalleDocumento(i).IdProducto, _
              IdTipoPV, CInt(IIf(Me.listaDetalleDocumento(i).IdTienda = 0, CInt(cboTienda.SelectedValue), Me.listaDetalleDocumento(i).IdTienda)), CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text), _
              listaDetalleDocumento(i).Cantidad, listaDetalleDocumento(i).IdUMold)
            If objCatalogo IsNot Nothing Then
                If objCatalogo.Cantidad > 0 Then
                    Me.listaDetalleDocumento(i).Cantidad = objCatalogo.Cantidad
                    listaDetalleDocumento(i).IdUMold = 0
                End If
                Me.listaDetalleDocumento(i).PrecioSD = objCatalogo.PrecioSD
                Me.listaDetalleDocumento(i).PrecioLista = objCatalogo.PrecioLista
                Me.listaDetalleDocumento(i).pvComercial = objCatalogo.pvComercial
                Me.listaDetalleDocumento(i).PrecioCD = objCatalogo.PrecioLista
                Me.listaDetalleDocumento(i).StockDisponibleN = objCatalogo.StockDisponibleN
                Me.listaDetalleDocumento(i).Descuento = 0
                Me.listaDetalleDocumento(i).PorcentDcto = 0
            End If
            Dim Peso As Decimal = 0
            Peso = (New Negocio.Util).fn_ProductoMedida(Me.listaDetalleDocumento(i).IdProducto, Me.listaDetalleDocumento(i).IdUnidadMedida, _IdMagnitud_Peso, CInt(Me.cboPesoTotal.SelectedValue), 1)
            Me.listaDetalleDocumento(i).Peso = Math.Round(Peso, 4, MidpointRounding.AwayFromZero)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            GV_Detalle.DataSource = Me.listaDetalleDocumento
            GV_Detalle.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        cargarDocumentoCotizacion(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value.Trim), 0)
    End Sub


    Private Function obtenerListaDetalleDocumento_Load(ByVal IdDocumento As Integer, ByVal IdTienda As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectDet(IdDocumento)

        '************* QUITAMOS EL DETALLE DE LOS COMPONENTES
        For i As Integer = (lista.Count - 1) To 0 Step -1

            If (lista(i).ComponenteKit) Then

                lista.RemoveAt(i)

            End If

        Next
        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(lista(i).IdProducto, IdTienda, lista(i).IdTipoPV)

            If lista(i).IdProductoAux > 0 Then


                If lista(i).ListaUM_Venta.Count <= 0 Then
                    Dim obj As New Entidades.UnidadMedida

                    obj.Id = lista(i).IdUnidadMedida
                    obj.DescripcionCorto = (New Negocio.UnidadMedida).SelectNombreCortoxIdUnidadMedida(lista(i).IdUnidadMedida)

                    lista(i).ListaUM_Venta.Add(obj)

                End If

            End If

            If lista(i).IdCampania > 0 And lista(i).ExisteCampania_Producto And lista(i).IdProductoAux <= 0 And lista(i).PorcentDctoMaximo < 100 Then
                ' PRODUCTO EN CAMPAÑA
                Dim dcto, porcentDcto As Decimal
                Dim IdCampania As Integer = lista(i).IdCampania
                Dim IdProducto As Integer = lista(i).IdProducto
                Dim IdUnidadMedida As Integer = lista(i).IdUnidadMedida

                Dim objCampania_Detalle As Entidades.Campania_Detalle = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente(IdCampania, IdProducto, IdUnidadMedida)

                With lista(i)
                    dcto = (lista(i).PrecioLista - objCampania_Detalle.Precio)

                    If (dcto < 0) Then
                        dcto = 0
                    Else
                        porcentDcto = Math.Round(((dcto) / lista(i).PrecioLista) * 100, 1)
                    End If
                    .VolumenVentaMin = objCampania_Detalle.CantidadMin
                    .PorcentDctoMaximo = porcentDcto

                End With

            End If

            If lista(i).IdCampania > 0 And lista(i).IdProductoAux > 0 Then
                ' PRODUCTO EN TRANSFERENCIA X CAMPAÑA
                Dim dcto, porcentDcto As Decimal

                With lista(i)
                    dcto = (lista(i).PrecioSD - lista(i).PrecioCD)

                    If (dcto < 0) Then
                        dcto = 0
                    Else
                        porcentDcto = Math.Round(((dcto) / lista(i).PrecioSD) * 100, 1)
                    End If

                    .PorcentDctoMaximo = porcentDcto

                End With

            End If

        Next

        Return lista

    End Function
    Private Function obtenerListaDetalleConcepto_Load(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As List(Of Entidades.DetalleConcepto) = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectDetalleConcepto(IdDocumento)


        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaConcepto = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))
            lista(i).ListaConcepto.Insert(0, (New Entidades.Concepto(0, "-----")))

        Next

        Return lista

    End Function

    Private Sub cargarDocumentoCotizacion(ByVal IdSerie As Integer, ByVal nroDocumento As Integer, ByVal IdDocumento As Integer)

        Try

            verFrm(FrmModo.Inicio, True, True, True, True, True)

            Dim objDocumentoCotizacion As Entidades.DocumentoCotizacion = (New Negocio.DocumentoCotizacion).DocumentoCotizacionSelectCab(IdSerie, nroDocumento, IdDocumento)

            Me.listaDetalleDocumento = obtenerListaDetalleDocumento_Load(objDocumentoCotizacion.Id, objDocumentoCotizacion.IdTienda)
            Me.listaDetalleConcepto = obtenerListaDetalleConcepto_Load(objDocumentoCotizacion.Id)

            Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumentoCotizacion.Id)
            Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumentoCotizacion.Id)
            Dim objObservacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumentoCotizacion.Id)

            Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumentoCotizacion.Id)

            '*************** GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            ' *************************** PESO
            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                Dim Peso As Decimal = 0
                Peso = (New Negocio.Util).fn_ProductoMedida(Me.listaDetalleDocumento(i).IdProducto, Me.listaDetalleDocumento(i).IdUnidadMedida, _IdMagnitud_Peso, CInt(Me.cboPesoTotal.SelectedValue), 1)
                Me.listaDetalleDocumento(i).Peso = Math.Round(Peso, 4, MidpointRounding.AwayFromZero)

            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            cargarDocumentoCotizacion_GUI(objDocumentoCotizacion, Me.listaDetalleDocumento, Me.listaDetalleConcepto, objPuntoPartida, objPuntoLlegada, objObservacion, Me.listaDocumentoRef)

            verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, True)
            visualizarMoneda()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Sub cargarDocumentoCotizacion_GUI(ByVal objDocumento As Entidades.DocumentoCotizacion, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservacion As Entidades.Observacion, ByVal listaDocumentoRef As List(Of Entidades.Documento))

        Dim objCbo As New Combo

        With objDocumento

            '******************* CARGAMOS LA CABECERA
            Me.lblPorcentIgv.Text = CStr(Math.Round(.getObjImpuesto.Tasa, 0))

            If CInt(Me.lblPorcentIgv.Text) <= 0 Then
                Me.lblPorcentIgv.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))
            End If

            Me.txtCodigoDocumento.Text = CStr(.Codigo)
            Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)

            If (Me.cboMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(.IdMoneda)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(.IdTipoOperacion)
            End If

            If (Me.cboMotivoTraslado.Items.FindByValue(CStr(.IdMotivoT)) IsNot Nothing) Then

                Me.cboMotivoTraslado.SelectedValue = CStr(.IdMotivoT)

            Else
                objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

                If (Me.cboMotivoTraslado.Items.FindByValue(CStr(.IdMotivoT)) IsNot Nothing) Then
                    Me.cboMotivoTraslado.SelectedValue = CStr(.IdMotivoT)
                End If

            End If

            If (Me.cboCondicionPago.Items.FindByValue(CStr(.IdCondicionPago)) IsNot Nothing) Then
                Me.cboCondicionPago.SelectedValue = CStr(.IdCondicionPago)
                objCbo.LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, CInt(Me.hddIdTipoDocumento.Value), CInt(Me.cboCondicionPago.SelectedValue))
            End If

            If (Me.cboMedioPago.Items.FindByValue(CStr(.getObjAnexoDocumento.IdMedioPago)) IsNot Nothing) Then
                Me.cboMedioPago.SelectedValue = CStr(.getObjAnexoDocumento.IdMedioPago)
            End If

            If (Me.cboAlmacenReferencia.Items.FindByValue(CStr(.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacenReferencia.SelectedValue = CStr(.IdAlmacen)
            End If

            If (Me.cboUsuarioComision.Items.FindByValue(CStr(.IdUsuarioComision)) IsNot Nothing) Then
                Me.cboUsuarioComision.SelectedValue = CStr(.IdUsuarioComision)
            End If

            '*************   Me.txtNroDiasVigenciaCredito.Text = CStr(Math.Round(.getObjAnexoDocumento.NroDiasVigencia, 0))

            If (.FechaVenc <> Nothing) Then
                Me.txtFechaVcto.Text = Format(.FechaVenc, "dd/MM/yyyy")
            End If

            If (.FechaAEntregar <> Nothing) Then
                Me.txtFechaAEntregar.Text = Format(.FechaAEntregar, "dd/MM/yyyy")
            End If

            Me.hddIdDocumento.Value = CStr(.Id)
            Me.hddCodigoDocumento.Value = CStr(.Codigo)

            '******************************* TOTALES
            Me.txtDescuento.Text = CStr(Math.Round(.Descuento, 2))
            Me.txtSubTotal.Text = CStr(Math.Round(.SubTotal, 2))
            Me.txtIGV.Text = CStr(Math.Round(.IGV, 2))
            Me.txtTotal.Text = CStr(Math.Round(.Total, 2))
            Me.txtPercepcion.Text = CStr(Math.Round(.Percepcion, 2))
            Me.txtTotalConcepto.Text = CStr(Math.Round(.getObjAnexoDocumento.TotalConcepto, 2))
            Me.txtTotalConcepto_PanelDetalle.Text = CStr(Math.Round(.getObjAnexoDocumento.TotalConcepto, 2))
            Me.txtTotalAPagar.Text = CStr(Math.Round(.TotalAPagar, 2, MidpointRounding.AwayFromZero))
            Me.txtImporteTotal.Text = CStr(Math.Round(.ImporteTotal, 2))

            '******************************** PERSONA
            If (.getObjPersonaView.ApPaterno <> Nothing) Then
                Me.txtApPaterno_RazonSocial.Text = .getObjPersonaView.ApPaterno
            Else
                Me.txtApPaterno_RazonSocial.Text = .getObjPersonaView.RazonSocial
            End If
            Me.cboTipoPersona_PanelCliente.SelectedValue = CStr(.getObjPersonaView.TipoPersona)
            'Me.cborolcliente.SelectedValue = .getObjPersonaView.Rol
            Me.txtApMaterno.Text = .getObjPersonaView.ApMaterno
            Me.txtNombres.Text = .getObjPersonaView.Nombres
            Me.txtDni.Text = .getObjPersonaView.Dni
            Me.txtRuc.Text = .getObjPersonaView.Ruc
            Me.txtDireccionCliente.Text = .getObjPersonaView.Direccion
            Me.txtCodigoCliente.Text = CStr(.IdPersona)
            Me.hddIdPersona.Value = CStr(.IdPersona)

            '************************** MAESTRO OBRA
            Me.txtMaestro.Text = .getObjMaestroObra.Descripcion
            Me.txtDni_Maestro.Text = .getObjMaestroObra.Dni
            Me.txtRuc_Maestro.Text = .getObjMaestroObra.Ruc
            Me.hddIdMaestroObra.Value = CStr(.getObjMaestroObra.IdPersona)


            '********************* TIPO CLIENTE
            If (Me.cborolcliente.Items.FindByValue(CStr(.getObjPersonaView.IdRol)) IsNot Nothing) Then
                Me.cborolcliente.SelectedValue = CStr(.getObjPersonaView.IdRol)
            End If

            '********************* TIPO AGENTE
            If (Me.cboTipoAgente.Items.FindByValue(CStr(.getObjTipoAgente.IdAgente)) IsNot Nothing) Then
                Me.cboTipoAgente.SelectedValue = CStr(.getObjTipoAgente.IdAgente)
            End If

            Me.txtTasaAgente.Text = CStr(Math.Round(.getObjTipoAgente.Tasa, 2))

            '************** TIPO PV
            If (Me.cboTipoPrecioV.Items.FindByValue(CStr(.IdTipoPV)) IsNot Nothing) Then
                Me.cboTipoPrecioV.SelectedValue = CStr(.IdTipoPV)
            End If

            '*************** LINEA DE CREDITO
            Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectAllConsultaxIdPersona(objDocumento.IdPersona, objDocumento.IdEmpresa, objDocumento.IdTienda)
            Me.GV_LineaCredito.DataBind()
            lbl_detalles.Visible = True

            btnVentasMes.Visible = True

        End With

        cargarPuntoPartida(objPuntoPartida)
        cargarPuntoLlegada(objPuntoLlegada)
        cargarObservaciones(objObservacion)

        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()


        Me.GV_Concepto.DataSource = listaDetalleConcepto
        Me.GV_Concepto.DataBind()

        Me.GV_DocumentoRef.DataSource = listaDocumentoRef
        Me.GV_DocumentoRef.DataBind()

        '**************** ACTUALIZAMOS LOS CONTROLES DE CONDICION PAGO
        actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue))

    End Sub

    Private Sub cargarObservaciones(ByVal objObservacion As Entidades.Observacion)

        If (objObservacion IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservacion.Observacion
        End If

    End Sub

    Private Sub cargarPuntoPartida(ByVal objPuntoPartida As Entidades.PuntoPartida)
        Dim objcbo As New Combo
        If (objPuntoPartida IsNot Nothing) Then

            If (Me.cboDepto_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepto_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(0, 2)
                objcbo.LLenarCboProvincia(Me.cboProvincia_Partida, Me.cboDepto_Partida.SelectedValue)
            End If

            If (Me.cboProvincia_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(2, 2)
                objcbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, Me.cboProvincia_Partida.SelectedValue)
            End If

            If (Me.cboDistrito_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Partida.Text = objPuntoPartida.Direccion

        End If
    End Sub

    Private Sub cargarPuntoLlegada(ByVal objPuntoLlegada As Entidades.PuntoLlegada)
        Dim objcbo As New Combo
        If (objPuntoLlegada IsNot Nothing) Then
            If (Me.cboDepartamento.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepartamento.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(0, 2)
                objcbo.LLenarCboProvincia(Me.cboProvincia, Me.cboDepartamento.SelectedValue)
            End If

            If (Me.cboProvincia.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(2, 2)
                objcbo.LLenarCboDistrito(Me.cboDistrito, Me.cboDepartamento.SelectedValue, Me.cboProvincia.SelectedValue)
            End If

            If (Me.cboDistrito.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Llegada.Text = objPuntoLlegada.pll_Direccion

        End If

    End Sub


    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click


        Try

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            verFrm(FrmModo.Editar, False, False, False, False, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try



    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumentoCotizacion()
    End Sub
    Private Sub anularDocumentoCotizacion()

        Try

            If ((New Negocio.DocumentoCotizacion).anularCotizacion(CInt(Me.hddIdDocumento.Value))) Then

                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, True)

                objScript.mostrarMsjAlerta(Me, "Documento anulado con éxito.")

            Else

                Throw New Exception("Problemas en la Operación.")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
        If (CInt(ViewState("ClientexDefecto")) > 0) Then
            cargarPersona(0)
        End If
        cursorCodigoBarras()
        Me.lblPorcentIgv.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))
    End Sub

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub
    Private Sub busquedaAvanzado()

        Try

            Dim IdCliente As Integer = 0
            Try
                IdCliente = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdCliente = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdCliente, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoCotizacion(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnBuscarMaestroObra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarMaestroObra.Click

        buscarMaestroObra()

    End Sub

    Private Sub buscarMaestroObra()

        Try

            Me.gvBuscar.DataSource = Nothing
            Me.gvBuscar.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  mostrarCapaPersona();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region
#Region "**************************    CALCULAR EQUIVALENCIAS"

    Protected Sub btnEquivalencia_PR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Try

            ActualizarListaDetalleDocumento()
            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)

            Me.listaDetalleDocumento = getListaDetalleDocumento()


            '******************** CARGAMOS LOS CONTROLES
            Me.cboUnidadMedida_Ingreso.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.cboUnidadMedida_Ingreso.DataBind()

            Me.cboUnidadMedida_Salida.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.cboUnidadMedida_Salida.DataBind()

            Me.GV_CalcularEquivalencia.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_CalcularEquivalencia.DataBind()

            Me.GV_ResuldoEQ.DataSource = Nothing
            Me.GV_ResuldoEQ.DataBind()

            Me.txtCantidad_Ingreso.Text = CStr(Me.listaDetalleDocumento(index).Cantidad)
            Me.txtCantidad_Salida.Text = ""

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaEquivalencia_PR();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Protected Sub btnCalcular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCalcular.Click
        calcularEquivalencia()
    End Sub
    Private Sub calcularEquivalencia()
        Try

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()


            Dim tableUM As DataTable = obtenerTablaUM(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_ResuldoEQ.DataSource = (New Negocio.Producto).Producto_ConsultaEquivalenciaProductoxParams(Me.listaDetalleDocumento(index).IdProducto, CInt(Me.cboUnidadMedida_Ingreso.SelectedValue), CInt(Me.cboUnidadMedida_Salida.SelectedValue), CDec(Me.txtCantidad_Ingreso.Text), tableUM, CInt(Me.rbl_UtilizarRedondeo.SelectedValue))
            Me.GV_ResuldoEQ.DataBind()

            Me.GV_ResuldoEQ.Rows(Me.GV_ResuldoEQ.Rows.Count - 1).BackColor = Drawing.Color.Yellow

            Dim cantidadEq As Decimal = CDec(Me.GV_ResuldoEQ.Rows(Me.GV_ResuldoEQ.Rows.Count - 1).Cells(2).Text)
            Me.txtCantidad_Salida.Text = CStr(cantidadEq)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaEquivalencia_PR();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerTablaUM(ByVal IdProducto As Integer) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        For i As Integer = 0 To Me.GV_CalcularEquivalencia.Rows.Count - 1

            If (CType(Me.GV_CalcularEquivalencia.Rows(i).FindControl("chb_UnidadMedida"), CheckBox).Checked) Then
                Dim IdUnidadMedida As Integer = CInt(CType(Me.GV_CalcularEquivalencia.Rows(i).FindControl("hddIdUnidadMedida"), HiddenField).Value)
                dt.Rows.Add(IdProducto, IdUnidadMedida)
            End If

        Next

        Return dt


    End Function
    Private Sub aceptarEquivalencia_PR()
        Try

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim cantidadNew As Decimal = CDec(Me.txtCantidad_Salida.Text)
            If listaDetalleDocumento(index).ListaUM_Venta.Find(Function(U As Entidades.UnidadMedida) U.Id = CInt(Me.cboUnidadMedida_Salida.SelectedValue)) IsNot Nothing Then
                Me.listaDetalleDocumento(index).Cantidad = cantidadNew
                Me.listaDetalleDocumento(index).IdUnidadMedida = CInt(Me.cboUnidadMedida_Salida.SelectedValue)
                Me.listaDetalleDocumento(index).UMedida = CStr(Me.cboUnidadMedida_Salida.SelectedItem.ToString)
            End If



            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);   offCapa('capaEquivalenciaProducto');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub btnAceptar_EquivalenciaPR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_EquivalenciaPR.Click

        aceptarEquivalencia_PR()

    End Sub
#End Region

    Protected Sub btnMostrarComponenteKit_Find_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Me.listaCatalogo = getListaCatalogo()

            '****************** MOSTRAMOS LOS COMPONENTES DEL KIT
            Me.GV_ComponenteKit_Find.DataSource = (New Negocio.Kit).SelectComponentexIdKit(Me.listaCatalogo(index).IdProducto)
            Me.GV_ComponenteKit_Find.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#Region "*********************************** CAMPAÑA"
    Protected Sub valOnClick_btnViewCampania(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        visualizarCampania_Producto_Find(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex)

    End Sub
    Private Sub visualizarCampania_Producto_Find(ByVal index As Integer)
        Try

            Me.listaCatalogo = getListaCatalogo()

            Dim IdProducto As Integer = Me.listaCatalogo(index).IdProducto

            Me.lblProducto_capaCampania_Producto.Text = Me.listaCatalogo(index).Descripcion
            Me.lblCodigoProducto_capaCampania_Producto.Text = Me.listaCatalogo(index).CodigoProducto

            Me.GV_Campania_Producto.DataSource = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente_SelectxParams_DT(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdProducto, Nothing, CDate(Me.txtFechaEmision.Text))
            Me.GV_Campania_Producto.DataBind()

            Me.hddIndex_Campania_Producto.Value = CStr(index)
            Me.hddIndex_GV_Detalle.Value = CStr(-1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "      onCapa('capaBuscarProducto_AddProd');   onCapa2('capaCampania_Producto');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Campania_Producto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Campania_Producto.SelectedIndexChanged
        valOnChange_GV_Campania_Producto(Me.GV_Campania_Producto.SelectedIndex)
    End Sub
    Private Sub valOnChange_GV_Campania_Producto(ByVal index As Integer)

        Try

            Dim IdCampania As Integer = CInt(CType(Me.GV_Campania_Producto.Rows(index).FindControl("hddIdCampania"), HiddenField).Value)
            Dim IdProducto As Integer = CInt(CType(Me.GV_Campania_Producto.Rows(index).FindControl("hddIdProducto"), HiddenField).Value)
            Dim IdUnidadMedida As Integer = CInt(CType(Me.GV_Campania_Producto.Rows(index).FindControl("hddIdUnidadMedida"), HiddenField).Value)

            Dim objCampania_Detalle As Entidades.Campania_Detalle = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente(IdCampania, IdProducto, IdUnidadMedida)

            If (objCampania_Detalle IsNot Nothing) Then

                Dim listaCampaniaTG As List(Of Entidades.Campania_DetalleTG) = (New Negocio.Campania_DetalleTG).Campania_DetalleTGxIdCampaniaDetalle(objCampania_Detalle.IdCampaniaDetalle)

                Dim opcionLista As Integer = 0
                Dim indexLista As Integer = 0
                '*********** LISTA CATALOGO = 0       LISTA DETALLE DOCUMENTO = 1

                If (CInt(Me.hddIndex_Campania_Producto.Value) < 0) Then
                    opcionLista = 1  '************** TRABAJA CON LISTA DETALLE
                    indexLista = CInt(Me.hddIndex_GV_Detalle.Value)
                Else
                    opcionLista = 0  '************** TRABAJA CON LISTA CATALOGO
                    indexLista = CInt(Me.hddIndex_Campania_Producto.Value)
                End If

                ActualizarListaDetalleDocumento()
                Me.listaDetalleDocumento = getListaDetalleDocumento()
                Dim dcto, porcentDcto As Decimal

                Select Case opcionLista

                    Case 0  '************** TRABAJA CON LISTA CATALOGO

                        Dim objDetalleDocumento As New Entidades.DetalleDocumento
                        Me.listaCatalogo = getListaCatalogo()
                        dcto = (Me.listaCatalogo(indexLista).PrecioLista - objCampania_Detalle.Precio)

                        If (dcto < 0) Then
                            dcto = 0
                        Else

                            porcentDcto = Math.Round(((dcto) / Me.listaCatalogo(indexLista).PrecioLista) * 100, 1)

                        End If

                        With objDetalleDocumento

                            .IdProducto = objCampania_Detalle.IdProducto
                            .NomProducto = objCampania_Detalle.Producto
                            .Cantidad = objCampania_Detalle.CantidadMin
                            .IdUnidadMedida = objCampania_Detalle.IdUnidadMedida
                            .PrecioCD = objCampania_Detalle.Precio
                            .Importe = .Cantidad * .PrecioCD
                            .PrecioBaseDcto = _PrecioBaseDcto_Lista
                            .VolumenVentaMin = objCampania_Detalle.CantidadMin
                            .IdCampania = objCampania_Detalle.IdCampania
                            .IdCampaniaDetalle = objCampania_Detalle.IdCampaniaDetalle
                            .ExisteCampania_Producto = True

                            .ListaUM_Venta = Me.listaCatalogo(indexLista).ListaUM_Venta
                            .StockDisponibleN = Me.listaCatalogo(indexLista).StockDisponibleN
                            .PrecioLista = Me.listaCatalogo(indexLista).PrecioLista
                            .PrecioSD = Me.listaCatalogo(indexLista).PrecioLista
                            .Descuento = dcto
                            .PorcentDcto = porcentDcto
                            .TasaPercepcion = Me.listaCatalogo(indexLista).Percepcion
                            .PorcentDctoMaximo = porcentDcto
                            .IdUsuarioSupervisor = 0
                            .IdTienda = Me.listaCatalogo(indexLista).IdTienda
                            .IdTipoPV = Me.listaCatalogo(indexLista).IdTipoPV
                            .pvComercial = Me.listaCatalogo(indexLista).pvComercial
                            .CodigoProducto = Me.listaCatalogo(indexLista).CodigoProducto
                            .Kit = Me.listaCatalogo(indexLista).Kit

                            Me.listaDetalleDocumento.Add(objDetalleDocumento)


                        End With

                        Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumentoTG(listaCampaniaTG, objCampania_Detalle.IdProducto, objCampania_Detalle.CantidadMin))

                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);   onCapa('capaBuscarProducto_AddProd');    ", True)

                    Case 1  '************** TRABAJA CON LISTA DETALLE

                        dcto = (Me.listaDetalleDocumento(indexLista).PrecioLista - objCampania_Detalle.Precio)

                        If (dcto < 0) Then
                            dcto = 0
                        Else

                            porcentDcto = Math.Round(((dcto) / Me.listaDetalleDocumento(indexLista).PrecioLista) * 100, 1)

                        End If

                        With Me.listaDetalleDocumento(indexLista)

                            If (.Cantidad < objCampania_Detalle.CantidadMin) Then
                                .Cantidad = objCampania_Detalle.CantidadMin
                            End If

                            .IdUnidadMedida = objCampania_Detalle.IdUnidadMedida
                            .PrecioSD = .PrecioLista
                            .PrecioCD = objCampania_Detalle.Precio
                            .Importe = .Cantidad * .PrecioCD
                            .PrecioBaseDcto = _PrecioBaseDcto_Lista
                            .VolumenVentaMin = objCampania_Detalle.CantidadMin
                            .IdCampania = objCampania_Detalle.IdCampania
                            .IdCampaniaDetalle = objCampania_Detalle.IdCampaniaDetalle
                            .ExisteCampania_Producto = True

                            .Descuento = dcto
                            .PorcentDcto = porcentDcto
                            .PorcentDctoMaximo = porcentDcto

                        End With

                        Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumentoTG(listaCampaniaTG, Me.listaDetalleDocumento(indexLista).IdProducto, objCampania_Detalle.CantidadMin))

                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);  ", True)

                End Select

                setListaDetalleDocumento(Me.listaDetalleDocumento)

                Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                Me.GV_Detalle.DataBind()

            Else
                Throw New Exception("NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaDetalleDocumentoTG(ByVal listaDetalleCampaniaTG As List(Of Entidades.Campania_DetalleTG), ByVal IdProductoCampania As Integer, ByVal CantMin As Decimal) As List(Of Entidades.DetalleDocumento)
        Dim objDetalleDocumentoTG As Entidades.DetalleDocumento
        Dim listaDetalleDocumentoTG As New List(Of Entidades.DetalleDocumento)

        For Each objCampaniaDetalleTG As Entidades.Campania_DetalleTG In listaDetalleCampaniaTG
            objDetalleDocumentoTG = New Entidades.DetalleDocumento
            With objDetalleDocumentoTG

                .IdCampania = objCampaniaDetalleTG.IdCampania
                .ExisteCampania_Producto = False
                .IdProducto = objCampaniaDetalleTG.IdProducto
                .NomProducto = objCampaniaDetalleTG.Producto
                .Cantidad = objCampaniaDetalleTG.cdtg_Cantidad
                .IdUnidadMedida = objCampaniaDetalleTG.IdUnidadMedida
                .PrecioBaseDcto = _PrecioBaseDcto_Lista
                .VolumenVentaMin = objCampaniaDetalleTG.cdtg_Cantidad

                .PrecioLista = (New Negocio.ProductoTipoPV).SelectValorCalculadoxParams(objCampaniaDetalleTG.IdProducto, CInt(cboTienda.SelectedValue), CInt(cboTipoPrecioV.SelectedValue), objCampaniaDetalleTG.IdUnidadMedida, CInt(cboMoneda.SelectedValue))

                .PrecioSD = objCampaniaDetalleTG.cdtg_Precio
                .PrecioCD = 0
                .Importe = 0

                .PorcentDcto = 100
                .PorcentDctoMaximo = 100
                .Descuento = objCampaniaDetalleTG.cdtg_Precio
                .DctoValor_MaximoxPVenta = objCampaniaDetalleTG.cdtg_Precio

                .ListaUM_Venta = objCampaniaDetalleTG.getListaUnidadMedida
                .StockDisponibleN = (New Negocio.Util).fx_StockDisponible(CInt(cboEmpresa.SelectedValue), CInt(cboAlmacenReferencia.SelectedValue), objCampaniaDetalleTG.IdProducto)

                .CodigoProducto = objCampaniaDetalleTG.CodigoProducto
                .Kit = False
                .IdCampania = objCampaniaDetalleTG.IdCampania
                .IdCampaniaDetalle = objCampaniaDetalleTG.IdCampaniaDetalle
                .IdProductoAux = IdProductoCampania
                ' verifica si se incrementa la transferencia deacuerdo a la cant de la campania
                If objCampaniaDetalleTG.cdtg_cantCampania Then
                    .CantidadDetalleAfecto = CantMin ' volumen de venta minima para la campania
                End If




                listaDetalleDocumentoTG.Add(objDetalleDocumentoTG)

            End With

        Next

        Return listaDetalleDocumentoTG
    End Function

    Protected Sub valOnClick_btnViewCampania_GV_Detalle(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        visualizarCampania_Producto_GV_Detalle(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex)

    End Sub
    Private Sub visualizarCampania_Producto_GV_Detalle(ByVal index As Integer)
        Try

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim IdProducto As Integer = Me.listaDetalleDocumento(index).IdProducto

            Me.lblProducto_capaCampania_Producto.Text = Me.listaDetalleDocumento(index).NomProducto
            Me.lblCodigoProducto_capaCampania_Producto.Text = Me.listaDetalleDocumento(index).CodigoProducto

            Me.GV_Campania_Producto.DataSource = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente_SelectxParams_DT(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdProducto, Nothing, CDate(Me.txtFechaEmision.Text))
            Me.GV_Campania_Producto.DataBind()

            Me.hddIndex_GV_Detalle.Value = CStr(index)
            Me.hddIndex_Campania_Producto.Value = CStr(-1)


            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa2('capaCampania_Producto');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnVerCampania_Consulta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerCampania_Consulta.Click
        valOnClick_btnVerCampania_Consulta()
    End Sub
    Private Sub valOnClick_btnVerCampania_Consulta()
        Try

            Dim fechaActual As Date = (New Negocio.FechaActual).SelectFechaActual

            Me.GV_Campania_Cab.DataSource = (New Negocio.Campania).Campania_SelectxParams(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), Nothing, fechaActual, fechaActual, True)
            Me.GV_Campania_Cab.DataBind()

            Me.GV_Campania_Det.DataSource = Nothing
            Me.GV_Campania_Det.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "      onCapa('capaBuscarProducto_AddProd');   onCapa2('capaConsultar_Campania');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Campania_Cab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Campania_Cab.SelectedIndexChanged
        valOnChange_GV_Campania_Cab(Me.GV_Campania_Cab.SelectedIndex)
    End Sub
    Private Sub valOnChange_GV_Campania_Cab(ByVal index As Integer)
        Try

            Dim IdCampania As Integer = CInt(CType(Me.GV_Campania_Cab.Rows(index).FindControl("hddIdCampania"), HiddenField).Value)

            Me.GV_Campania_Det.DataSource = (New Negocio.Campania_Detalle).Campania_Detalle_SelectxIdCampania(IdCampania)
            Me.GV_Campania_Det.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "      onCapa('capaBuscarProducto_AddProd');   onCapa2('capaConsultar_Campania');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Campania_Det_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_Campania_Det.PageIndexChanging

        Me.GV_Campania_Det.PageIndex = e.NewPageIndex
        valOnChange_GV_Campania_Cab(Me.GV_Campania_Cab.SelectedIndex)

    End Sub
#End Region
#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As String = ""
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CStr(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef2(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(Me.cboTipoDocumento.SelectedValue), fechaInicio, fechafin, False)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objObservacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(IdDocumentoRef)
            Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(IdDocumentoRef)
            Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(IdDocumentoRef)

            '*************** GUARDAMOS EN SESSION
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** 
            cargarDocumentoRef_GUI(IdDocumentoRef, objDocumento, Nothing, Nothing, objPuntoPartida, objPuntoLlegada, objObservacion)

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            actualizarControlesMedioPago(1) '************ CONTADO
            If (Me.cboMedioPago.Items.FindByValue(Me.hddIdMedioPago_Default.Value) IsNot Nothing) Then
                Me.cboMedioPago.SelectedValue = CStr(Me.hddIdMedioPago_Default.Value)
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);  ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function obtenerListaDetalleDocumento_Load_DocRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)

        ' Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoCotizacion).DocumentoVenta_SelectDetxIdDocumentoRef2(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoCotizacion).DocumentoVenta_SelectDetxIdDocumentoRef3(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)

        ' _DocumentoVenta_SelectDetxIdDocumentoRef3()
        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(lista(i).IdProducto, IdTienda, IdTipoPV)

        Next

        Return lista

    End Function

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function


    Private Sub cargarDocumentoRef_GUI(ByVal IdDocumentoRef As Integer, ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservaciones As Entidades.Observacion)

        '*************** PERSONA
        cargarPersona(objDocumento.IdPersona)

        '****************** DETALLE CONCEPTO
        Me.GV_Concepto.DataSource = Nothing
        Me.GV_Concepto.DataBind()

        cargarPuntoLlegada(objPuntoLlegada)
        cargarPuntoPartida(objPuntoPartida)

        Dim IdTipoPv As Integer = CInt(Me.hddIdTipoPVDefault.Value)
        If (IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Length > 0) Then
            If (CInt(Me.cboTipoPrecioV.SelectedValue) > 0) Then
                IdTipoPv = CInt(Me.cboTipoPrecioV.SelectedValue)
            End If
        End If

        Dim IdPersona As Integer = CInt(Me.hddIdPersona.Value)

        Me.listaDetalleDocumento = obtenerListaDetalleDocumento_Load_DocRef(IdDocumentoRef, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CDate(Me.txtFechaEmision.Text), IdTipoPv, IdPersona, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cboUsuarioComision.SelectedValue))
        setListaDetalleDocumento(Me.listaDetalleDocumento)

        '***************** DETALLE DOCUMENTO
        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()

    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region

    Private Sub btnAceptarCodBarras_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarCodBarras.Click

        'BuscarProductoCatalogoCodBarras(0)
        'Me.txtCodBarrasPrincipal.Text = ""
        'Me.txtCodBarrasPrincipal.Focus()

        If getListaDetalleDocumento() IsNot Nothing Then
            If BuscaProductoEnLista(Me.txtCodBarrasPrincipal.Text.Trim) = False Then
                BuscarProductoCatalogoCodBarras(0)
            End If
        Else
            BuscarProductoCatalogoCodBarras(0)
        End If

        Me.txtCodBarrasPrincipal.Text = ""
        Me.txtCodBarrasPrincipal.Focus()


    End Sub

    Private Function BuscaProductoEnLista(ByVal codBarraproducto As String) As Boolean
        Dim val As Boolean = False
        ActualizarListaCatalogo()
        ActualizarListaDetalleDocumento()

        Dim listaDet As New List(Of Entidades.DetalleDocumento)
        listaDet = getListaDetalleDocumento()

        For Each prodCodBarra As Entidades.DetalleDocumento In listaDet
            If prodCodBarra.prod_CodigoBarras = codBarraproducto Then
                prodCodBarra.Cantidad = prodCodBarra.Cantidad + 1

                setListaDetalleDocumento(listaDet)

                Me.GV_Detalle.DataSource = getListaDetalleDocumento()
                Me.GV_Detalle.DataBind()

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0); CursorCodBarras();  ", True)
                val = True
            End If
        Next

        Return val

    End Function
    Protected Sub BuscarProductoCatalogoCodBarras(ByVal opcion As Integer)
        Try
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                        If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                            IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                        End If
                    End If

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)
                    ViewState.Add("IdMoneda_BuscarProducto", Me.cboMoneda.SelectedValue)
                    ViewState.Add("IdTipoPV_BuscarProducto", IdTipoPV)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("CodigoBarras", Me.txtCodBarrasPrincipal.Text)
                    ViewState.Add("IdTipoExistencia", CInt(cboTipoExistencia.SelectedValue))

                Case 1
                    '******************* Cuando se cambia el almacen de busqueda
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)

                Case 2
                    '******************** cuando se cambia la tienda de precios
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)

            End Select

            cargarDatosProductoGrillaCodBarras(0, 0, "", "", _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarDatosProductoGrillaCodBarras(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                                   ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                                   ByVal IdAlmacen As Integer, ByVal IdTienda As Integer, _
                                                   ByVal IdTipoPV As Integer, ByVal IdMoneda As Integer, _
                                                   ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, _
                                                   ByVal codbarras As String, ByVal IdTipoExistencia As Integer)

        Dim IdMedioPago As Integer = 0
        If (CInt(Me.cboCondicionPago.SelectedValue) = 1) Then  '************ CONTADO
            IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
        End If

        Dim IdCliente As Integer = 0
        If (IsNumeric(Me.hddIdPersona.Value) And (Me.hddIdPersona.Value.Trim.Length) > 0) Then
            IdCliente = CInt(Me.hddIdPersona.Value)
        End If

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        Dim filtroProductoCampania As Boolean = Me.chb_FiltroProductoCampania.Checked
        Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Cotizacion_V2CodBarras(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, 1, 1, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, codbarras, filtroProductoCampania)
        If Me.listaCatalogo.Count = 0 Then
            objScript.mostrarMsjAlerta(Me, "No se Encontro el producto")
        Else
            'guardamos el producto que se busco por codigo de barras
            ListProdCodBarras = listaCatalogo
            addProducto_DetalleDocumentoCodBarras()
        End If
    End Sub

    Private Sub addProducto_DetalleDocumentoCodBarras()
        Try
            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaCatalogo()
            ActualizarListaDetalleDocumento()
            Me.listaCatalogo = ListProdCodBarras
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            For i As Integer = 0 To Me.listaCatalogo.Count - 1
                'If (Me.listaCatalogo(i).Cantidad > 0) Then
                valCant = False
                For k As Integer = 0 To listaDetalleDocumento.Count - 1
                    If listaDetalleDocumento(k).IdProducto = listaCatalogo(i).IdProducto And listaDetalleDocumento(k).IdUnidadMedida = listaCatalogo(i).IdUnidadMedida Then
                        listaDetalleDocumento(k).Cantidad = listaDetalleDocumento(k).Cantidad + 1  'Me.listaCatalogo(i).Cantidad
                        valCant = True
                        Exit For
                    End If
                Next

                If valCant = False Then
                    Dim objDetalle As New Entidades.DetalleDocumento
                    With objDetalle

                        .IdProducto = Me.listaCatalogo(i).IdProducto
                        .NomProducto = Me.listaCatalogo(i).Descripcion
                        .Cantidad = 1 'Me.listaCatalogo(i).Cantidad
                        .ListaUM_Venta = Me.listaCatalogo(i).ListaUM_Venta
                        .IdUnidadMedida = Me.listaCatalogo(i).IdUnidadMedida
                        .StockDisponibleN = Me.listaCatalogo(i).StockDisponibleN
                        .PrecioLista = Me.listaCatalogo(i).PrecioLista
                        .PrecioSD = Me.listaCatalogo(i).PrecioSD
                        .Descuento = 0
                        .PorcentDcto = 0
                        .PrecioCD = Me.listaCatalogo(i).PrecioSD
                        .Importe = .Cantidad * .PrecioCD
                        .TasaPercepcion = Me.listaCatalogo(i).Percepcion
                        .PorcentDctoMaximo = Me.listaCatalogo(i).PorcentDctoMaximo
                        .DsctoOriginal = Me.listaCatalogo(i).PorcentDctoMaximo
                        .PrecioBaseDcto = Me.listaCatalogo(i).PrecioBaseDcto
                        .IdUsuarioSupervisor = 0
                        .IdTienda = Me.listaCatalogo(i).IdTienda
                        .IdTipoPV = Me.listaCatalogo(i).IdTipoPV
                        .pvComercial = Me.listaCatalogo(i).pvComercial
                        .CodigoProducto = Me.listaCatalogo(i).CodigoProducto
                        .Kit = Me.listaCatalogo(i).Kit
                        .ExisteCampania_Producto = Me.listaCatalogo(i).ExisteCampania_Producto
                        .prod_CodigoBarras = listaCatalogo(i).Prod_CodigoBarras


                    End With
                    Me.listaDetalleDocumento.Add(objDetalle)

                End If
                'End If

            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0); CursorCodBarras(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Public Property ListProdCodBarras() As List(Of Entidades.Catalogo)
        Get
            Return CType(Session.Item("ProdCodBarras"), List(Of Entidades.Catalogo))
        End Get
        Set(ByVal value As List(Of Entidades.Catalogo))
            Session.Remove("ProdCodBarras")
            Session.Add("ProdCodBarras", value)
        End Set
    End Property



    Protected Sub cboAlmacenReferencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboAlmacenReferencia.SelectedIndexChanged

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        CollapsiblePanelExtender3.Collapsed = False
        CollapsiblePanelExtender3.ClientState = "False"
        txtCodigoDocumento.Focus()
    End Sub

    Private Sub btnVentasMes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnVentasMes.Click
        '************* Línea de Crédito
        'Me.GridView3.DataSource = (New Negocio.CuentaPersona).SelectxcomprasParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
        'Me.GridView3.DataBind()

        Me.GridView3.DataSource = (New Negocio.CuentaPersona).ULTIMAS_VENTASFUNCION(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
        Me.GridView3.DataBind()
        Me.GV_VentaMes.DataSource = (New Negocio.CuentaPersona).Mostrarventaxpersona(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
        Me.GV_VentaMes.DataBind()


        If GV_VentaMes.Rows.Count > 0 Then
            objScript.onCapa(Me, "capaVentaMes")
        Else
            If GV_VentaMes.Rows.Count < 0 Then
                lbl_detalles.Text = "VENTAS NO REALIZADAS EN EL ULTIMO MES"
                lbl_detalles.Visible = True


            End If
        End If


    End Sub

    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btnAddProducto_ConsultarStockPrecio_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProducto_ConsultarStockPrecio.Click
        Try
            If IsNumeric(hddIdProductoStockPrecio.Value) Then
                Dim IdConcepto As Integer = CInt((New Negocio.Util).fx_getValorParametroGeneralxIdparam("25"))

                If IdConcepto > 0 Then
                    addProducto_DetalleDocumento(CInt(hddIdProductoStockPrecio.Value), CDec(Me.txtStockPrecioCant.Text.Trim), CDec(Me.txtStockPrecioTotal.Text.Trim))

                    addDetalleConcepto(IdConcepto, CInt(hddIdProductoStockPrecio.Value), CStr(hddDescripcionFlete.Value), CDec(Me.txtStockPrecioTotal.Text.Trim))
                Else
                    objScript.mostrarMsjAlerta(Me, "No se ha configurado el valor de flete.")
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        limpiarDetalleDocumento()
    End Sub
    Private Sub limpiarDetalleDocumento()
        Try

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboCliDepartamentoE_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCliDepartamentoE.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboCliProvinciaE, (Me.cboCliDepartamentoE.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboCliDistritoE, (Me.cboCliDepartamentoE.SelectedValue), (Me.cboCliProvinciaE.SelectedValue))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     onCapa('capaEditarPersona');       ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboCliProvinciaE_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCliProvinciaE.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboCliDistritoE, (Me.cboCliDepartamentoE.SelectedValue), (Me.cboCliProvinciaE.SelectedValue))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     onCapa('capaEditarPersona');       ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboCliDepartamentoN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCliDepartamentoN.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboCliProvinciaN, (Me.cboCliDepartamentoN.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboCliDistritoN, (Me.cboCliDepartamentoN.SelectedValue), (Me.cboCliProvinciaN.SelectedValue))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     onCapa('capaRegistrarCliente');      ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboCliProvinciaN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCliProvinciaN.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboCliDistritoN, (Me.cboCliDepartamentoN.SelectedValue), (Me.cboCliProvinciaN.SelectedValue))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     onCapa('capaRegistrarCliente');       ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    ''AGREGAR

    Private Sub cboMODepartamentoN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMODepartamentoN.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboMOProvinciaN, (Me.cboMODepartamentoN.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboMODistritoN, (Me.cboMODepartamentoN.SelectedValue), (Me.cboMOProvinciaN.SelectedValue))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     onCapa('IngresoMaestroObra');      ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboMOProvinciaN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMOProvinciaN.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboMODistritoN, (Me.cboMODepartamentoN.SelectedValue), (Me.cboMOProvinciaN.SelectedValue))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     onCapa('IngresoMaestroObra');       ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboMODepartamentoE_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMODepartamentoE.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboMOProvinciaE, (Me.cboMODepartamentoE.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboMODistritoE, (Me.cboMODepartamentoE.SelectedValue), (Me.cboMOProvinciaE.SelectedValue))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     onCapa('capaEditarMaestroObra');      ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboMOProvinciaE_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMOProvinciaE.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboMODistritoE, (Me.cboMODepartamentoE.SelectedValue), (Me.cboMOProvinciaE.SelectedValue))
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     onCapa('capaEditarMaestroObra');       ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub




    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalleDocumento(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Dim idoperacion2 As String
    Public idoperacionant As String
    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        Try
            ' idoperacion2 = (cboTipoOperacion.SelectedValue)

            If (GV_Detalle.Rows.Count = 0) Then


                Dim objCbo As New Combo

                With objCbo
                    .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)
                End With


                Me.DGV_AddProd.DataSource = Nothing
                Me.DGV_AddProd.DataBind()
                Me.DGV_AddProd.DataSource = Nothing



            Else


                cboTipoOperacion.SelectedValue = CStr(hddTipoOperacion.Value)
                Throw New Exception("No es Posible la Operacion Debe quitar los productos.")


            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try





    End Sub

    Protected Sub btnConsultar_DsctoVolVtas_OnClick(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Consultar_DsctoVolVtas(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Private Sub Consultar_DsctoVolVtas(ByVal Index As Integer)


        Dim objDscto As New Entidades.DsctoVolVtas

        Try

            Me.ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = Me.getListaDetalleDocumento()

            Try

                With objDscto
                    .IdUsuario = CInt(Session("IdUsuario"))
                    .IdTienda = CInt(Me.cboTienda.SelectedValue)
                    .IdCondicionPago = CInt(Me.cboCondicionPago.SelectedValue)
                    .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                    .IdTipoPV = Me.listaDetalleDocumento(Index).IdTipoPV
                    .IdProducto = Me.listaDetalleDocumento(Index).IdProducto
                    .CantMin = Me.listaDetalleDocumento(Index).Cantidad
                    .FechaFin = CDate(Me.txtFechaEmision.Text)
                End With

                If objDscto.IdTipoPV = 0 Then
                    objDscto.IdTipoPV = CInt(Me.cboTipoPrecioV.SelectedValue)
                End If

                Dim Lista As List(Of Entidades.DsctoVolVtas) = (New Negocio.DsctoVolVtas).Facturacion(objDscto)

                If Lista.Count > 0 Then

                    Me.listaDetalleDocumento(Index).PorcentDctoMaximo = Lista(0).Dscto
                    Me.listaDetalleDocumento(Index).CantMin = Lista(0).CantMin
                    Me.listaDetalleDocumento(Index).PrecioBaseDcto = CStr(Math.Round(Me.listaDetalleDocumento(Index).PrecioLista * (Lista(0).Dscto / 100), 4))

                Else

                    Me.listaDetalleDocumento(Index).PorcentDctoMaximo = Me.listaDetalleDocumento(Index).DsctoOriginal
                    Me.listaDetalleDocumento(Index).CantMin = 0
                    Me.listaDetalleDocumento(Index).PrecioBaseDcto = CStr(Math.Round(Me.listaDetalleDocumento(Index).PrecioLista * (Me.listaDetalleDocumento(Index).DsctoOriginal / 100), 4))

                End If



            Catch ex As Exception

            End Try

            Me.setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     calcularTotales_GV_Detalle(0);       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub



    Protected Sub btnBuscarMaestroxId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarMaestroxId.Click

        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()

        Dim datatable1 As New DataTable()
        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersonaMaestrosObra(CInt(Me.txtIdMaestroObra_BuscarxId.Text))

        If (objPersona Is Nothing) Then

            objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS DE MAESTRO RELACIONADO CON ESTE ID")
        Else

            Me.txtMaestro.Text = objPersona.Descripcion
            Me.txtRuc_Maestro.Text = objPersona.Ruc
            Me.txtDni_Maestro.Text = objPersona.Dni
            Me.hddIdMaestroObra.Value = CStr(objPersona.IdPersona)
            Me.txtCodigoMaestro.Text = CStr(objPersona.IdPersona)


            Dim appat As String = objPersona.ApPaterno
            Dim apmat As String = objPersona.ApMaterno
            Dim nom As String = objPersona.Nombres
            Dim dnim As String = objPersona.Dni
            Dim direccionm As String = objPersona.Direccion
            Dim telefonom As String = objPersona.Telefeono
            Dim mailm As String = objPersona.Correo
            Dim rol As String = objPersona.Rol
            Me.txtApPaterno_EditarMO.Text = appat
            Me.txtApMaterno_EditarMO.Text = apmat
            Me.txtDNI_EditarMO.Text = dnim
            Me.txtTelefono_EditarMO.Text = telefonom
            Me.txtemail_EditarMO.Text = mailm
            Me.txtDireccion_EditarMO.Text = direccionm
            Me.txtNombres_EditarMO.Text = nom
            Me.hddBusquedaMaestro.Value = "0"

        End If

    End Sub

    Protected Sub btnBuscarMaestroObra_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Me.buscarMaestroObra()
    End Sub
End Class
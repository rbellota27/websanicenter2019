﻿Imports Entidades
Imports Negocio
Public Class frmAprobarCotizacion
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarFrmGeneral()
        End If
    End Sub

    Private Sub CargarGrillaCotizaciones()
        Dim idUsuario As Integer = Session("IdUsuario")
        Dim fechaInicio As Date = Me.txtFechaInicio.Text
        Dim fechaFin As Date = Me.txtFechaFin.Text
        Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.bl_negocio).listarAprobarCotizacion(CInt(Me.ddlEmpresa.SelectedValue), CInt(ddlTienda.SelectedValue), _
                                                                                                   idUsuario, fechaInicio, fechaFin, _
                                                                                                   Me.ddlAprobacion.SelectedValue)
        Me.gv_CotizacionAprobar.DataSource = lista
        Me.gv_CotizacionAprobar.DataBind()
    End Sub

    Private Sub CargarFrmGeneral()
        Dim fechaActual As DateTime = Date.Now
        Dim fechaInicio As Date = New Date(fechaActual.Year, fechaActual.Month, 1)
        Dim fechaFinal As Date = New Date(fechaActual.Year, IIf(fechaActual.Month + 1 = 13, 12, fechaActual.Month + 1), 1).AddDays(-1)
        Me.txtFechaInicio.Text = fechaInicio
        Me.txtFechaFin.Text = fechaFinal

        Dim idUsuario As Integer = Session("IdUsuario")
        Dim objetoGeneral As be_frmCargaPrincipal = Nothing
        objetoGeneral = (New bl_FrmGenerales).cls_frmCancelarProvisiones(idUsuario)
        llenarCombo(objetoGeneral.listaEmpresa, ddlEmpresa, False)
        llenarCombo(objetoGeneral.listaTienda, ddlTienda, False, objetoGeneral.idTiendaPrincipal)
    End Sub

    Private Sub llenarCombo(Of T)(ByVal lista As List(Of T), ByVal combo As DropDownList, Optional addnewItem As Boolean = False, Optional valorDefecto As String = "-1")
        combo.DataSource = lista
        combo.DataBind()
        If addnewItem Then            
            'Código para agregar priemr item
        End If
        combo.SelectedValue = valorDefecto
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
        CargarGrillaCotizaciones()
    End Sub

    Private Sub gv_CotizacionAprobar_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gv_CotizacionAprobar.RowCommand
        If e.CommandName = "btnAprobar" Then
            Dim indice As Integer = CInt(e.CommandArgument)
            Dim datakey As DataKey = Me.gv_CotizacionAprobar.DataKeys(indice)
            Dim idDocumento As Integer = datakey(0)
            If (New bl_negocio).aprobarCotizacion(idDocumento) Then
                objScript.mostrarMsjAlerta(Me, "Aprobado.")
            End If
        End If
    End Sub
End Class
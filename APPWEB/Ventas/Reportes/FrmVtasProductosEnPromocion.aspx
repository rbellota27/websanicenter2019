﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmVtasProductosEnPromocion.aspx.vb" Inherits="APPWEB.FrmVtasProductosEnPromocion" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%" class="style1">
        <tr>
            <td colspan="9" align="center " class="TituloCelda">
                Ventas de Productos en Promocion
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="udpDetalle" runat="server">
                    <ContentTemplate>
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Label">
                                                            Empresa:<asp:DropDownList ID="cmbEmpresa" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Label">
                                                            Tienda:<asp:DropDownList ID="cmbTienda" runat="server" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Label">
                                                            Documento:
                                                            <asp:DropDownList ID="cboTipoDocumento" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Label" style="width: 54px">
                                                            :
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:ImageButton ID="btnBusProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                                                            OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='../../Imagenes/BuscarProducto_b.JPG';"
                                                                            onmouseover="this.src='../../Imagenes/BuscarProducto_A.JPG';" />
                                                                        <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                                            onmouseout="this.src='../../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="Prod_Codigo" HeaderText="Código" HeaderStyle-Height="25px"
                                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                                                                    HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                                                <asp:BoundField DataField="UnidadMedida" HeaderText="UM Principal" HeaderStyle-Height="25px"
                                                                                    HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnEliminarProducto" runat="server" OnClick="btnEliminarProducto_Click">Quitar</asp:LinkButton>
                                                                                        <asp:HiddenField ID="hdfIdProductodt" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                                            <FooterStyle CssClass="GrillaFooter" />
                                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                                            <PagerStyle CssClass="GrillaPager" />
                                                                            <RowStyle CssClass="GrillaRow" />
                                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top">
                                                            <table>
                                                                <tr>
                                                                    <td class="Label" valign="top">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td valign="top">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" valign="top">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <fieldset class="FieldSetPanel">
                                        <legend>Considerar Filtros por Semana</legend>
                                        <table>
                                            <tr>
                                                <td class="Label_fsp">
                                                    <asp:CheckBox ID="chbFiltrarSemana" runat="server" Text="Filtrar por Semanas" />
                                                </td>
                                                <td class="Label_fsp">
                                                    DE:
                                                </td>
                                                <td class="Label_fsp">
                                                    Año:
                                                    <asp:DropDownList ID="idYearI" runat="Server">
                                                    </asp:DropDownList>
                                                    Semana:
                                                    <asp:DropDownList ID="idSemanaI" runat="Server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Label_fsp">
                                                    HASTA:
                                                </td>
                                                <td class="Label_fsp">
                                                    Año:
                                                    <asp:DropDownList ID="idYearF" runat="Server">
                                                    </asp:DropDownList>
                                                    Semana:
                                                    <asp:DropDownList ID="idSemanaF" runat="Server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DGV_AddProd" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                        </td>
                        <td class="Label_fsp">
                            Fecha Inicio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Label_fsp">
                            Fecha Fin:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="txtFechaFin">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Label">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
           <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel_Cliente" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td class="TituloCeldaLeft">
                                                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="PnlCliente"
                                                    CollapsedSize="0" ExpandedSize="80" Collapsed="true" ExpandControlID="ImgCliente"
                                                    CollapseControlID="ImgCliente" TextLabelID="lblCliente" ImageControlID="ImgCliente"
                                                    CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                    CollapsedText="Buscar Cliente" ExpandedText="Buscar Cliente" ExpandDirection="Vertical"
                                                    SuppressPostBack="True" />
                                                <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                                <asp:Label ID="lblCliente" runat="server" Text="Buscar Cliente" CssClass="LabelBlanco"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="PnlCliente" runat="server">
                                                    <table cellspacing="0" style="width: 100%">
                                                        <tr>

                                                            <td>
                                                                <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                    onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                                                    OnClientClick="return(mostrarCapaPersona());" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="btnLimpiarCliente" runat="server" ImageUrl="~/Imagenes/Limpiar_b.JPG"
                                                                    onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';" onmouseout="this.src='../../Imagenes/Limpiar_b.JPG';" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:GridView ID="gvwCliente" runat="server" AutoGenerateColumns="False" Width="100%">
                                                                    <Columns>

                                                                        <asp:BoundField DataField="RazonSocial" HeaderText="Descripción" HeaderStyle-Height="25px"
                                                                            HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                                        <asp:BoundField DataField="Ruc" HeaderText="RucDni" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                                            NullDisplayText="---" />
                                                                        <asp:BoundField DataField="Dni" HeaderText="Dni" HeaderStyle-Height="25px"
                                                                            HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                                        <asp:BoundField DataField="Direccion" HeaderText="Direccion" HeaderStyle-Height="25px"
                                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hdfIdpersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                                    <FooterStyle CssClass="GrillaFooter" />
                                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                                    <PagerStyle CssClass="GrillaPager" />
                                                                    <RowStyle CssClass="GrillaRow" />
                                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
        
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                            onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                            OnClientClick="return(mostrarReporteDetallado());" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddidproducto" runat="server" />
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddIdpersona" runat="server" />
                <br />
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="udpbProducto" runat="server">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                            DataTextField="Descripcion" DataValueField="Id">
                                        </asp:DropDownList>
                                        <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                        <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                            DataValueField="Id" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripción:"></asp:Label>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarGrilla_AddProd">
                                                    <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                        onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );"></asp:TextBox>
                                                        </asp:Panel>
                                                </td>
                                                <td class="Texto">
                                                    Cód.:
                                                </td>
                                                <td>
                                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProd">
                                                    <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                        onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );"
                                                        TabIndex="205"></asp:TextBox>
                                                        </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                        TabIndex="207" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        onmouseout="this.src='../../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../../Imagenes/Agregar_A.JPG';"
                                                        OnClientClick="return(valAddProductos());" TabIndex="208" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                        onmouseout="this.src='../../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';"
                                                        OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                                SuppressPostBack="true">
                            </cc1:CollapsiblePanelExtender>
                            <asp:Image ID="Image21_11" runat="server" Height="16px" />
                            <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada" CssClass="Label"></asp:Label>
                            <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                                <table width="100">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Atributo:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                Width="650px">
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                        HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="GrillaRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chbSelecionar" runat="server" />
                                            <asp:HiddenField ID="hdfIdproducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Codigo" HeaderText="Código" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                    <asp:BoundField DataField="UnidadMedida" HeaderText="UMP" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                                Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                            <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                                Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                            <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                                CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                            <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                                Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProductos('2'));" />
                            <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='../../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../../Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr >
                        <td align="left">
                            <table>
                                <tr>
                                    <td class ="Label" >
                                         Persona
                                    </td>
                                    <td class ="Label" >
                                        <asp:RadioButtonList ID="rblpersona" runat="server"  RepeatDirection ="Horizontal"  >
                                        <asp:ListItem Value ="0" Text ="Juridica"  Selected ="True" ></asp:ListItem>
                                        <asp:ListItem Value ="1" Text ="Natural" ></asp:ListItem>
                                       </asp:RadioButtonList>
                                    </td>
                                    <td>
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LabelLeft" >
                                        RUC:<asp:TextBox ID ="txtBPruc" runat ="server"  onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                    </td>
                                    <td class="LabelLeft" >
                                        DNI:<asp:TextBox ID ="txtBPDni" runat ="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                   </td>
                                    <td class ="LabelLeft" >
                                        Razon Social/Nombre:
                                        <asp:TextBox ID ="txtBuscarPersona_Grilla" runat ="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                    </td>
                                    <td class ="LabelLeft">
                                        <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" CausesValidation="false"
                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                            onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Descripción" NullDisplayText="---" />
                                    <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                    <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                    <asp:BoundField DataField="Direccion" HeaderText="Dirección" NullDisplayText="---" />
                                </Columns> 
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                        
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                            <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                            <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox>
                            <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                ToolTip="Ir a la Página" OnClientClick="return(valNavegacionPersona('2'));" />
                            <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script language="javascript" type="text/javascript">

        function valKeyPressCodigoSL() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

//            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
//            var cont = 0;
//            if (grilla == null) {
//                alert('No se seleccionaron productos.');
//                return false;
//            }



            //            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
            //                var rowElem = grilla.rows[i];
            //                var txtCantidad = rowElem.cells[0].children[0];
            //                if (parseFloat(txtCantidad.value) > 0) {
            //                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
            //                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
            //                    //                        txtCantidad.select();
            //                    //                        txtCantidad.focus();
            //                    //     return false;
            //                    //  } else if (parseFloat(txtCantidad.value) > 0) {
            //                    cont = cont + 1;
            //                    //  }
            //                }
            //            }

            //            if (cont == 0) {
            //                alert('No se seleccionaron productos.');
            //                return false;
            //            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

                if (grillaDetalle.rows.length > 1) {

                    //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

                    if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd() {
            if (event.keyCode == 13) {
                document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        //***********************************

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }


        // Bussqueda de persona

        function onCapaCliente(s) {
            onCapa(s);
            return false;
        }
        function BuscarCliente() {

            return true;
        }


        function ValidarEnter() {
            return (!esEnter(event));
        }
        function ValidaEnter() {
            if (esEnter(event) == true) {
                alert('Tecla No permitida');
                caja.focus();
                return false;
            }
            return true;
        }



        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();

            return false;
        }
        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }


        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }




        function valNavegacionPersona(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarPersona_Grilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
    


        function mostrarReporteDetallado() {

            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            if (document.getElementById('<%= cmbEmpresa.ClientID%>' == null)) {
                IdEmpresa = 0
            }

            var IdTienda = document.getElementById('<%= cmbTienda.ClientID%>').value;
            if (document.getElementById('<%= cmbTienda.ClientID%>') == null) {
                IdTienda = 0
            }

            var IdTipoDocumento = document.getElementById('<%= cboTipoDocumento.ClientID%>').value;
            if (document.getElementById('<%= cboTipoDocumento.ClientID%>') == null) {
                IdTipoDocumento = 0 
            }

            var idpersona = document.getElementById('<%= hddIdpersona.ClientID%>').value;
            if (document.getElementById('<%= hddIdpersona.ClientID%>') == null) {
                idpersona = 0 
            }

            var yeari = document.getElementById('<%= idYearI.ClientID%>').value;
            if (document.getElementById('<%= idYearI.ClientID%>') == null) {
                yeari = 0
            }
            var semanai = document.getElementById('<%= idSemanaI.ClientID%>').value;
            if (document.getElementById('<%= idSemanaI.ClientID%>') == null) {
                semanai = 0
            }


            var yearf = document.getElementById('<%= idYearF.ClientID%>').value;
            if (document.getElementById('<%= idYearF.ClientID%>') == null) {
                yearf = 0
            }
            var semanaf = document.getElementById('<%= idSemanaF.ClientID%>').value;
            if (document.getElementById('<%= idSemanaF.ClientID%>') == null) {
                semanaf = 0
            }

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE  chbStockxTienda
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

//            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
//                var rowElem = grilla.rows[i];
//                var txtCantidad = rowElem.cells[0].children[0];
//                //if (parseFloat(txtCantidad.value) > 0) {
//                // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
//                //                        alert('La cantidad ingresada excede a la cantidad disponible.');
//                //                        txtCantidad.select();
//                //                        txtCantidad.focus();
//                //     return false;
//                //  } else if (parseFloat(txtCantidad.value) > 0) {
//                cont = cont + 1;
//                //}
//            }


            var grillaDetalle = document.getElementById('<%=GV_Detalle.ClientID%>');
            var cont = 0;


            if (grillaDetalle == null) {
                alert('Seleccione Productos.');
                return false;
            }

            if (grillaDetalle.rows.length < 2) {
                alert('Ingrese minimo un Producto');
                return false;
            }

            //            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
            //                var rowElem = grilla.rows[i];
            //                var txtCantidad = rowElem.cells[0].children[0];
            //                //if (parseFloat(txtCantidad.value) > 0) {
            //                // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
            //                //                        alert('La cantidad ingresada excede a la cantidad disponible.');
            //                //                        txtCantidad.select();
            //                //                        txtCantidad.focus();
            //                //     return false;
            //                //  } else if (parseFloat(txtCantidad.value) > 0) {
            //                cont = cont + 1;
            //                //}
            //            }


            var check = document.getElementById('<%= chbFiltrarSemana.ClientID%>')

            var filtrarsemana = 0;
            if (check.status == true) {
                var filtrarsemana = 1;
            }
            else {
                var filtrarsemana = 0;
            }   

            var fechaInicio;
            var fechaFin;
            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }
            frame1.location.href = 'visorVentas.aspx?iReporte=8&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&Idempresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&IdTipoDocumento=' + IdTipoDocumento + '&yeari=' + yeari + '&semanai=' + semanai + '&yearf=' + yearf + '&semanaf=' + semanaf + '&filtrarsemana=' + filtrarsemana + '&idpersona' + idpersona;
            return false;
        }
    </script>

</asp:Content>

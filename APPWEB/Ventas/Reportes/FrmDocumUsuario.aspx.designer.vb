﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmDocumUsuario

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''cmbEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmbEmpresa As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cmbtienda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmbtienda As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cmbTipoDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmbTipoDocumento As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtFechaInicio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFechaInicio As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFechaInicio_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFechaInicio_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtFechaFin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFechaFin As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFechaFin_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFechaFin_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''idtipos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents idtipos As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''AscDes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AscDes As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''btnMostrarReporte control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnMostrarReporte As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''hddFechaActual control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hddFechaActual As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hddRbtTipoReporte control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hddRbtTipoReporte As Global.System.Web.UI.WebControls.HiddenField
End Class

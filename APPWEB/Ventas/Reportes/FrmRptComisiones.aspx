<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmRptComisiones.aspx.vb" Inherits="APPWEB.FrmRptComisiones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%" width="100%">
        <tr>
            <td class="TituloCelda">
                COMISIONES
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            Empresa:
                            <asp:DropDownList ID="cmbEmpresa" runat="server" Width="110px">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            Tienda:
                            <asp:DropDownList ID="cmbTienda" runat="server" Width="110px">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            Perfil:
                            <asp:DropDownList ID="cmbPerfil" runat="server" Width="110px">
                            </asp:DropDownList>
                        </td>
                        <td class ="Label" >
                            <asp:CheckBox ID="ckbMaestroObra" runat="server"  Text ="Maestro Obra"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label_fsp">
                            Fecha Inicio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Label_fsp">
                            Fecha Fin:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="txtFechaFin">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_Cliente" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="TituloCeldaLeft">
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="PnlCliente"
                                        CollapsedSize="0" ExpandedSize="80" Collapsed="true" ExpandControlID="ImgCliente"
                                        CollapseControlID="ImgCliente" TextLabelID="lblCliente" ImageControlID="ImgCliente"
                                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                        CollapsedText="Buscar Persona" ExpandedText="Buscar Persona" ExpandDirection="Vertical"
                                        SuppressPostBack="True" />
                                    <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                    <asp:Label ID="lblCliente" runat="server" Text="Buscar Persona" CssClass="LabelBlanco"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PnlCliente" runat="server">
                                        <table cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                        onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                                        OnClientClick="return(mostrarCapaPersona());" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnLimpiarCliente" runat="server" ImageUrl="~/Imagenes/Limpiar_b.JPG"
                                                        onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';" onmouseout="this.src='../../Imagenes/Limpiar_b.JPG';" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:GridView ID="gvwCliente" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="RazonSocial" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                            <asp:BoundField DataField="Ruc" HeaderText="RucDni" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                                NullDisplayText="---" />
                                                            <asp:BoundField DataField="Dni" HeaderText="Dni" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                                NullDisplayText="---" />
                                                            <asp:BoundField DataField="Direccion" HeaderText="Direccion" HeaderStyle-Height="25px"
                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdfIdpersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hddIdpersona" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                    OnClientClick="return(MostrarReporte());" />
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddidproducto" runat="server" />
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <br />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='../../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../../Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table>
                                <tr>
                                    <td class="Label">
                                        Persona
                                    </td>
                                    <td class="Label">
                                        <asp:RadioButtonList ID="rblpersona" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0" Text="Juridica" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Natural"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LabelLeft">
                                        RUC:
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersona_Grilla">                                        
                                        <asp:TextBox ID="txtBPruc" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                        DNI:
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersona_Grilla"> 
                                        <asp:TextBox ID="txtBPDni" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                        Razon Social/Nombre:
                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarPersona_Grilla"> 
                                        <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                        <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" CausesValidation="false"
                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                            onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Descripci�n" NullDisplayText="---" />
                                    <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                    <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                    <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                            <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                            <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox>
                            <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                            <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">


        function onCapaCliente(s) {
            onCapa(s);
            return false;
        }
        function BuscarCliente() {

            return true;
        }


        function ValidarEnter() {
            return (!esEnter(event));
        }
        function ValidaEnter() {
            if (esEnter(event) == true) {
                alert('Tecla No permitida');
                caja.focus();
                return false;
            }
            return true;
        }



        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();

            return false;
        }
        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }


        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }




        function valNavegacionPersona(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarPersona_Grilla.ClientID %>');
                boton.focus();
                return true;
            }
        }

        function MostrarReporte() {
            var empresa = document.getElementById('<%= cmbempresa.ClientID%>').value;
            var tienda = document.getElementById('<%= cmbTienda.ClientID%>').value;
            var perfil = document.getElementById('<%= cmbPerfil.ClientID%>').value;

            var chk = document.getElementById('<%= ckbMaestroObra.ClientID%>');
            if (chk.checked == true) {
                var rol = 3;
            } else {
                var rol = 0;
            }  
            
            var usuario = document.getElementById('<%= hddIdpersona.ClientID%>').value;
            if (usuario == '') {
                usuario = 0;
            }

            var fechaInicio;
            var fechaFin;
            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }
            frame1.location.href = 'VisorGrillaVentas.aspx?iReporte=2&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&empresa=' + empresa + '&tienda=' + tienda + '&perfil=' + perfil + '&rol=' + rol + '&usuario=' + usuario;
            return false;
        }
        
        
    </script>

</asp:Content>

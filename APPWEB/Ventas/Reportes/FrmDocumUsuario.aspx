<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmDocumUsuario.aspx.vb" Inherits="APPWEB.FrmDocumUsuario" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
    <tr><td class="TituloCelda" > Documentos por Usuario </td> </tr>
   <tr>
            <td>
                <table>
                    <tr>
                    <td style="width: 870px" >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 936px">
                                            <table>
                                                <tr>
                                                    <td class="Label">
                                                        Empresa:<asp:DropDownList ID="cmbEmpresa" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Label">
                                                        Tienda:<asp:DropDownList ID="cmbtienda" runat="server" Enabled="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    
                                                    <td class="Label">
                                                        Documento:<asp:DropDownList ID="cmbTipoDocumento" runat="server" Enabled="true" 
                                                            Width="114px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td  colspan ="2" >
                                                        <table>
                                                            <tr >
                                                                <td class="Label">
                                                                    Fecha Inicio:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" 
                                                                        CssClass="TextBoxReadOnly" Width="63px" Height="18px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                                        TargetControlID="txtFechaInicio">
                                                                    </cc1:CalendarExtender>
                                                                </td>
                                                                <td class="Label">
                                                                    Fecha Fin:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" 
                                                                        CssClass="TextBoxReadOnly" Height="18px" Width="63px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                                        TargetControlID="txtFechaFin">
                                                                    </cc1:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>   
                                                        
                                                                                                        
                                                  </tr>
                                                <tr>
                                                   <td colspan ="3" class ="LabelLeft">
                                                   <asp:RadioButtonList ID="idtipos" runat ="server" RepeatDirection ="Horizontal">
                                                   <asp:ListItem Value ="1" Text ="Ordenado por Nro Documento" Selected ="True" ></asp:ListItem>
                                                   <asp:ListItem Value ="2" Text ="Ordenado por Fecha Registro"></asp:ListItem>
                                                   <asp:ListItem Value ="3" Text ="Ultimo Registro"></asp:ListItem>
                                                   
                                                   </asp:RadioButtonList>
                                                   </td> 
                                                   
                                                   <td class ="LabelLeft"> 
                                                    <asp:RadioButtonList ID="AscDes" runat ="server" >
                                                   <asp:ListItem Text ="Primero-Ultimo" Selected ="True"  Value ="1"></asp:ListItem>
                                                   <asp:ListItem Text ="Ultimo-Primero" Value ="2"></asp:ListItem>
                                                   </asp:RadioButtonList>
                                                   </td>

                                                                                                     
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                                        onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                                                                        OnClientClick="return(MostrarReporte());" Height="26px" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                     
                                                </tr>
                                        </td>
                                    </tr>
                                </table>
                                </td>
                            </tr>
                            <%--taable--%>
                                                          
                            </table>    
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    
                    </td>
                    </tr>



                </table>
            </td>
        </tr>
        
        <tr>
            <td>
            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                                               
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                <br />
            </td>
        </tr>
</table>
<script type ="text/javascript" language ="javascript" >

    function MostrarReporte() {
        var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
        var idtienda = document.getElementById('<%=cmbtienda.ClientID%>').value;
        var idtipodocumento = document.getElementById('<%=cmbTipoDocumento.ClientID%>').value;

        var rdbList = document.getElementById('<%=idtipos.ClientID%>');
        var radioTipos = rdbList.getElementsByTagName("INPUT");
        var tipo;
        if (radioTipos[0].checked == true) { 
            tipo = 1;
        } else if (radioTipos[1].checked == true) {
            tipo = 2;
        } else {
            tipo = 3;
        }


        var rblorden = document.getElementById('<%=AscDes.ClientID %>');
        var radioAsc = rblorden.getElementsByTagName("INPUT");
        var orden;
        if (radioAsc[0].checked == true) {
            orden = 1;}
        else{
            orden = 2;
            }
        
        
        if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
            fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
            fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
        } else {
            fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
            fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
        }
        frame1.location.href = 'visorVentas.aspx?iReporte=5&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + idempresa + '&IdTienda=' + idtienda + '&idtipodocumento=' + idtipodocumento + '&tipo=' + tipo + '&ascdes=' + orden; 
        return false;

    }
</script>
</asp:Content>

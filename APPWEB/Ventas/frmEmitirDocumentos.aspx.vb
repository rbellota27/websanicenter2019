'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.



Partial Public Class frmEmitirDocumentos
    Inherits System.Web.UI.Page

    Public objStringB As New StringBuilder
    Private objScript As New ScriptManagerClass
    Private mes() As String = {"", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"}
    Private listaCatalogoSelect As List(Of Entidades.Catalogo)
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumentoView)
    Private listaCancelacion As List(Of Entidades.PagoCaja)
    Private listaVuelto As List(Of Entidades.PagoCaja)
    Private IdUsuario As Integer = Nothing


    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6: OCULTAR TODOS LOS BOTONES
    '**********************************************

    Private Sub frmEmitirDocumentos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Session("IdUsuario") Is Nothing) Then
            objScript.mostrarMsjAlerta(Me, "No se tiene la variable <IdUsuario> en sessi�n. Reingrese al Sistema o cont�ctese con el Administrador del Sistema.")
            Return
        Else
            Me.IdUsuario = CInt(Session("IdUsuario"))
        End If

        If Not Me.IsPostBack Then

            inicializarFrm()

        End If

    End Sub

    Private Sub inicializarFrm()
        Try

            Me.lblFechaVencimiento.Visible = False
            Me.txtFechaVencimiento.Visible = False

            Dim cbo As New Combo
            '*********************** Llenamos el tipo Agente para el Registro de un Nuevo cliente
            cbo.LlenarCboTipoAgente(Me.cmbTipoAgente_RegistrarCliente)
            cbo.LlenarCboTipoAgente(Me.cboTipoAgente_EditarCliente)

            cbo.LlenarCboEmpresaxIdUsuario(Me.cboPropietario, CInt(Session("IdUsuario")), False)
            cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.CboTienda, CInt(Me.cboPropietario.SelectedValue), CInt(Session("IdUsuario")), False)
            cbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.CboTienda.SelectedValue), CInt(Me.IdUsuario), False)



            cbo.LlenarCboCondicionPago(Me.cboCondicionPago)
            '****************   cbo.LlenarCboMedioPago(Me.cboMedioPago)    LO MANEJAMOS POR ID
            cbo.LlenarCboBanco(Me.cboBanco, True)
            cbo.LlenarCboMoneda(Me.cboMoneda)

            cbo.LlenarCboCuentaBancaria(cboNCuenta, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboPropietario.SelectedValue), True)

            cbo.LLenarCboDepartamento(Me.cboDepartamento)
            cbo.LLenarCboProvincia(Me.cboProvincia, Me.cboDepartamento.SelectedValue)
            cbo.LLenarCboDistrito(Me.cboDistrito, Me.cboDepartamento.SelectedValue, Me.cboProvincia.SelectedValue)


            cbo.LlenarCboTipoPV1(Me.cboTipoPrecioV, True)
            cbo.LlenarCboTipoAgente(Me.cboTipoAgente)

            cbo.LLenarCboEstadoDocumento(Me.cboEstadoDocumento)
            cbo.LLenarCboEstadoCancelacion(Me.cboEstadoCancelacion, False)
            cbo.LLenarCboEstadoEntrega(Me.cboEstadoEntrega)

            Dim fecha As New Negocio.FechaActual
            Me.txtFechaEmision.Text = Format(fecha.SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaACobrar.Text = Me.txtFechaEmision.Text
            Me.txtFechaAEntregar.Text = Me.txtFechaEmision.Text
            Me.txtFechaVencimiento.Text = Me.txtFechaEmision.Text


            With New Negocio.Impuesto
                Me.hddIGV.Value = CStr(.SelectTasaIGV)
            End With

            Me.lblIgv.Text = CStr(CInt(CDec(Me.hddIGV.Value) * CDec(100)))

            'LLenar Serie
            cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboPropietario.SelectedValue), CInt(Me.CboTienda.SelectedValue) _
            , CInt(Me.cboTipoDocumento.SelectedValue))

            'Almacen x Tienda
            cbo.llenarCboAlmacenxIdTienda(Me.cmbAlmacen_AddProd, CInt(Me.CboTienda.SelectedValue))

            '********************** Llenamos el combo Moneda Cancelacion 
            cbo.LlenarCboMoneda(Me.cmbMonedaCancelacion_Efectivo)
            cbo.LlenarCboMoneda(Me.cmbMoneda_Vuelto)

            '********************** Llenamos los combos para la B�squeda de Productos
            cbo.LlenarCboLinea(Me.cmbLinea_AddProd, True)
            cbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)

            cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cmbTienda_AddProd, CInt(Me.cboPropietario.SelectedValue), Me.IdUsuario, False)

            cbo.llenarCboAlmacenxIdTienda(Me.cmbAlmacen_AddProd, CInt(Me.CboTienda.SelectedValue))


            hddIdAlmacen.Value = ""
            hddNomAlmacen.Value = ""






            '*********************** Cargamos las monedas
            lblMonto_Acumulado.Text = Me.cboMoneda.SelectedItem.ToString
            lblMonto_Saldo.Text = Me.cboMoneda.SelectedItem.ToString
            lblMonto_VueltoTotal.Text = Me.cboMoneda.SelectedItem.ToString
            lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString
            lblMontoFaltante_Vuelto.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblSimboloMoneda1.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblSimboloMoneda2.Text = Me.cboMoneda.SelectedItem.ToString

            Me.cmbMoneda_Vuelto.SelectedValue = Me.cboMoneda.SelectedValue
            Me.cmbMonedaCancelacion_Efectivo.SelectedValue = Me.cboMoneda.SelectedValue


            


            validarPermisos()

            verFrm("1", True, True, False, True, True, True, 1, 1)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / DESPACHAR / EMITIR GUIA REM / FECHA EMISION / COMPROMETER PERCEPCION / GENERAR DESPACHO AUTOMATICO
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(Me.IdUsuario, New Integer() {1, 2, 3, 17, 18, 19, 20, 29, 30, 32, 82})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumento.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumento.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* DESPACHAR
            Me.btnDespachar.Enabled = True
            Me.btnDespachar_capa.Enabled = True
        Else
            Me.btnDespachar.Enabled = False
            Me.btnDespachar_capa.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* REGISTRAR
            Me.LinkButton1.Enabled = True
            Me.lbtnEmitirGuiaRem_capa.Enabled = True
        Else
            Me.LinkButton1.Enabled = False
            Me.lbtnEmitirGuiaRem_capa.Enabled = False
        End If

        If listaPermisos(6) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(7) > 0 Then  '********* COMPROMETER PERCEPCION
            Me.chb_CompPercepcion.Enabled = True
            Me.chb_CompPercepcion.Checked = False
        Else
            Me.chb_CompPercepcion.Enabled = False
            Me.chb_CompPercepcion.Checked = False
        End If

        If listaPermisos(8) > 0 Then  '********* GENERAR DOC. COMP PERCEPCION
            Me.btnGenerarCompPercepcion.Enabled = True
            Me.btnGenerarCompPercepcion_Capa.Enabled = True
        Else
            Me.btnGenerarCompPercepcion.Enabled = False
            Me.btnGenerarCompPercepcion_Capa.Enabled = False
        End If

        Me.hddValAddDatoCancelacion.Value = CStr(listaPermisos(9)) '*********** Val Add Dato de Cancelaci�n

        If listaPermisos(10) > 0 Then  '********* GENERAR DOC. COMP PERCEPCION
            Me.chb_GenerarDespacho.Enabled = True
            Me.chb_GenerarDespacho.Enabled = True
        Else
            Me.chb_GenerarDespacho.Enabled = False
            Me.chb_GenerarDespacho.Enabled = False
        End If

    End Sub

    Private Sub llenarMontosMaximos()

        Dim objTipoDocumentoAE As Entidades.TipoDocumentoAE = (New Negocio.TipoDocumentoAE).SelectValoresxParams(CInt(Me.cboPropietario.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue), CInt(Me.cboMoneda.SelectedValue))

        If objTipoDocumentoAE IsNot Nothing Then

            hddMontoMaximoAfecto.Value = CStr(IIf(objTipoDocumentoAE.MontoMaximoAfecto = 0, 999999, objTipoDocumentoAE.MontoMaximoAfecto))
            hddMontoMaximoNOAfecto.Value = CStr(IIf(objTipoDocumentoAE.MontoMaximoNoAfecto = 0, 999999, objTipoDocumentoAE.MontoMaximoNoAfecto))

        End If
        

    End Sub

#Region "Busqueda del Cliente"
    Private Sub LLenarCliente(ByVal cliente As Entidades.PersonaView)

        Me.txtApPaterno_RazonSocial.Text = ""
        Me.txtApMaterno.Text = ""
        Me.txtNombres.Text = ""

        Me.txtCodigoCliente.Text = CStr(cliente.IdPersona)

        If cliente.RazonSocial <> "" Then
            Me.txtApPaterno_RazonSocial.Text = cliente.RazonSocial
            Me.cboTipoPersona.SelectedIndex = 2 'Jurirdica
        Else
            Me.txtApPaterno_RazonSocial.Text = cliente.ApPaterno
            Me.txtApMaterno.Text = cliente.ApMaterno
            Me.txtNombres.Text = cliente.Nombres
            Me.cboTipoPersona.SelectedIndex = 1 'Natural
        End If

        Me.txtDni.Text = cliente.Dni
        Me.txtRuc.Text = cliente.Ruc
        Me.txtDireccionCliente.Text = cliente.Direccion
        Me.txtTelefono.Text = cliente.Telefeono

        Me.cboTipoPrecioV.SelectedValue = CStr(cliente.IdTipoPV)

        '***************** Registro el IdTipoPV del Cliente
        ViewState.Add("IdTipoPV_Cliente", cliente.IdTipoPV)


        Dim NegPersonaTipoAgente As New Negocio.PersonaTipoAgente
        Dim TipoAgente As New Entidades.TipoAgente
        TipoAgente = NegPersonaTipoAgente.SelectIdAgenteTasaxIdPersona(cliente.IdPersona)

        Me.cboTipoAgente.SelectedValue = CStr(TipoAgente.IdAgente)
        Me.txtTasaAgente.Text = CStr(Math.Round((TipoAgente.Tasa * 100), 4))

        'Cargar la Linea de Cr�dito Disponible
        If Me.txtCodigoCliente.Text <> "" Then
            Dim NCuentaPersona As New Negocio.CuentaPersona
            Me.gvLineaCredito.DataSource = NCuentaPersona.SelectAllConsultaxIdPersona(CInt(Me.txtCodigoCliente.Text), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.CboTienda.SelectedValue))
            Me.gvLineaCredito.DataBind()
        End If
    End Sub



    Private Sub LimpiarBuscarCliente()
        Me.txtCodigoCliente.Text = ""
        Me.txtApPaterno_RazonSocial.Text = ""
        Me.txtApMaterno.Text = ""
        Me.txtNombres.Text = ""
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""
        Me.txtTelefono.Text = ""
        Me.txtDireccionCliente.Text = ""
    End Sub




#End Region
#Region "Consulta Productos"
    Protected Sub btnAgregar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar.Click
        'Me.panelCatalogo.Visible = True
        'Me.cboSubLinea.SelectedIndex = 0
        'Me.cboAlmacen.SelectedIndex = 0
        'Me.txtCodigoSubLinea.Text = ""
        'Me.txtNombreProducto.Text = ""
        'Me.gvCatalogo.DataSource = Nothing
        'Me.gvCatalogo.DataBind()
    End Sub


    Private Sub LlenarGrillaCatalogo()

        'Dim Lista As New List(Of Entidades.Catalogo)
        'Dim obj As New Negocio.Catalogo
        'Lista = obj.SelectxIdSublineaxIds(CInt(Me.cboPropietario.SelectedValue), CInt(Me.CboTiendaPrecios.SelectedValue), _
        '                                  CInt(Me.cboAlmacen.SelectedValue), CInt(Me.cboSubLinea.SelectedValue))
        'Session.Add("ListaCatalogo", Lista)
        'Me.gvCatalogo.DataSource = Lista
        'Me.gvCatalogo.DataBind()

    End Sub
    Private Sub LlenarGrillaCatalogoxNombre()

        'Dim Lista As New List(Of Entidades.Catalogo)
        'Dim obj As New Negocio.Catalogo

        'Lista = obj.SelectxIdSublineaxIdsxNombre(CInt(Me.cboPropietario.SelectedValue), CInt(Me.CboTiendaPrecios.SelectedValue), _
        '                                CInt(Me.cboAlmacen.SelectedValue), CInt(Me.cboSubLinea.SelectedValue), Me.txtNombreProducto.Text.Trim)

        'Session.Add("ListaCatalogo", Lista)
        'Me.gvCatalogo.DataSource = Lista
        'Me.gvCatalogo.DataBind()

    End Sub
  
    'Combo Unidad de la grilla DetalleDocumento
    Protected Sub cboUnidadMedida_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim listaOld As List(Of Entidades.DetalleDocumentoView) = Me.getListaDetalle
        Dim lista As New List(Of Entidades.DetalleDocumentoView)

        Dim cbo As DropDownList = CType(sender, DropDownList)

        For i As Integer = 0 To listaOld.Count - 1
            Dim cboUM As DropDownList = CType(gvDetalle.Rows(i).Cells(3).FindControl("cboUnidadMedida"), DropDownList)
            If cbo.ClientID = cboUM.ClientID Then

                Dim IdUM_Old As Integer = listaOld(i).IdUMedida
                Dim cant_Old As Decimal = 0
                Dim IdUM_New As Integer = 0

                ActualizarGrillaDetalle() 'para actualizar el valor que hemos elegido en el combo

                lista = Me.getListaDetalle
                IdUM_New = lista(i).IdUMedida
                cant_Old = lista(i).Cantidad



                Dim IdTipoPV As Integer = 1

                If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then

                    If CInt(cboTipoPrecioV.SelectedValue) > 0 Then

                        IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)

                    End If

                End If


                Dim addPorcentRetazo As Boolean = False

                If lista(i).Glosa IsNot Nothing Then
                    If lista(i).Glosa.Trim.Length > 0 Then
                        addPorcentRetazo = True
                    End If
                End If



                Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams(CInt(hddIdAlmacen.Value), CInt(Me.cboMoneda.SelectedValue), _
                    IdUM_New, lista(i).IdProducto, lista(i).IdTienda_PV, IdTipoPV, addPorcentRetazo)

                '************ lista(i).Cantidad = CDec(0)
                lista(i).UM = cboUM.SelectedItem.Text
                '*********** lista(i).IdUMedida = CInt(cboUM.SelectedValue)
                lista.Item(i).PrecioSD = objCatalogo.PrecioSD
                lista.Item(i).Descuento = CDec(0)
                lista.Item(i).PDescuento = CDec(0)
                lista.Item(i).PrecioCD = lista.Item(i).PrecioSD

                lista(i).StockDisponible = objCatalogo.StockDisponible

                '******************** actualizo la nueva cantidad

                If lista(i).Glosa IsNot Nothing Then

                    If lista(i).Glosa.Trim.Length > 0 Then

                        lista(i).Cantidad = (New Negocio.Producto).SelectEquivalenciaEntreUM(lista(i).IdProducto, IdUM_Old, IdUM_New, cant_Old)

                    End If

                End If



                Exit For
            End If
        Next
        Me.setListaDetalle(lista)
        Me.gvDetalle.DataSource = Me.getListaDetalle
        Me.gvDetalle.DataBind()

        ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)
        
    End Sub
    Protected Sub gvDetalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetalle.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim cboUMedida1 As DropDownList

            Dim gvrow As GridViewRow = CType(e.Row.Cells(4).NamingContainer, GridViewRow)
            cboUMedida1 = CType(gvrow.FindControl("cboUnidadMedida"), DropDownList)

            Dim ListaDetalle As New List(Of Entidades.DetalleDocumentoView)
            ListaDetalle = Me.getListaDetalle

            If ListaDetalle.Item(e.Row.RowIndex).IdUMedida <> 0 Then
                cboUMedida1.SelectedValue = CStr(ListaDetalle.Item(e.Row.RowIndex).IdUMedida)
            End If

        End If
    End Sub
    Private Sub ActualizarGrillaDetalle()
        Dim lista As New List(Of Entidades.DetalleDocumentoView)
        lista = Me.getListaDetalle
        For i As Integer = 0 To lista.Count - 1
            Dim TCantidad As TextBox = CType(Me.gvDetalle.Rows(i).Cells(2).FindControl("txtCantidad"), TextBox)
            lista.Item(i).Cantidad = CDec(TCantidad.Text)

            Dim cboUM As DropDownList = CType(gvDetalle.Rows(i).Cells(4).FindControl("cboUnidadMedida"), DropDownList)
            lista.Item(i).UM = CStr(IIf(cboUM.SelectedItem.Text = "", Nothing, cboUM.SelectedItem.Text))
            lista.Item(i).IdUMedida = CInt(IIf(cboUM.SelectedValue = "", Nothing, cboUM.SelectedValue))

            lista.Item(i).PrecioSD = CDec(gvDetalle.Rows(i).Cells(6).Text)

            Dim TDescuento As TextBox = CType(Me.gvDetalle.Rows(i).Cells(7).FindControl("txtDescuento"), TextBox)
            lista.Item(i).Descuento = CDec(TDescuento.Text)

            Dim TPrecioCD As TextBox = CType(Me.gvDetalle.Rows(i).Cells(9).FindControl("txtPrecioCD"), TextBox)
            lista.Item(i).PrecioCD = CDec(TPrecioCD.Text)

            Dim TImporte As TextBox = CType(Me.gvDetalle.Rows(i).Cells(10).FindControl("txtImporte"), TextBox)
            lista.Item(i).Importe = CDec(TImporte.Text)
        Next
        Me.setListaDetalle(lista)
    End Sub

#End Region

#Region "****************** LISTA CATALOGO DE PRECIOS"

    Private Function getListaCatalogo() As List(Of Entidades.Catalogo)
        Return CType(Session.Item("listaCatalogoSelect"), List(Of Entidades.Catalogo))
    End Function

    Private Sub setListaCatalogo(ByVal lista As List(Of Entidades.Catalogo))
        Session.Remove("listaCatalogoSelect")
        Session.Add("listaCatalogoSelect", lista)
    End Sub




    Private Function getListaCancelacion() As List(Of Entidades.PagoCaja)
        Return CType(Session.Item("listaCancelacion"), List(Of Entidades.PagoCaja))
    End Function

    Private Sub setListaCancelacion(ByVal lista As List(Of Entidades.PagoCaja))
        Session.Remove("listaCancelacion")
        Session.Add("listaCancelacion", lista)
    End Sub


    Private Function getListaVuelto() As List(Of Entidades.PagoCaja)
        Return CType(Session.Item("listaVuelto"), List(Of Entidades.PagoCaja))
    End Function

    Private Sub setListaVuelto(ByVal lista As List(Of Entidades.PagoCaja))
        Session.Remove("listaVuelto")
        Session.Add("listaVuelto", lista)
    End Sub

#End Region

#Region "Datos de Cabecera"
    Protected Sub cboPropietario_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboPropietario.SelectedIndexChanged
        Try

            Dim cbo As New Combo

            cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.CboTienda, CInt(Me.cboPropietario.SelectedValue), Me.IdUsuario, False)

            cbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.CboTienda.SelectedValue), Me.IdUsuario, False)

            cbo.LlenarCboCuentaBancaria(cboNCuenta, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboPropietario.SelectedValue), True)
            cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboPropietario.SelectedValue), CInt(Me.CboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            Me.txtCodigoDocumento.Text = ""
            llenarMontosMaximos()
            GenerarCodigo()

            cbo.llenarCboAlmacenxIdTienda(Me.cmbAlmacen_AddProd, CInt(Me.CboTienda.SelectedValue))
            cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cmbTienda_AddProd, CInt(Me.cboPropietario.SelectedValue), Me.IdUsuario, False)

            ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub CboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CboTienda.SelectedIndexChanged

        Try

            Dim cbo As New Combo

            cbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.CboTienda.SelectedValue), Me.IdUsuario, False)

            cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboPropietario.SelectedValue), CInt(Me.CboTienda.SelectedValue) _
               , CInt(Me.cboTipoDocumento.SelectedValue))

            Me.txtCodigoDocumento.Text = ""

            GenerarCodigo()

            cbo.llenarCboAlmacenxIdTienda(Me.cmbAlmacen_AddProd, CInt(Me.CboTienda.SelectedValue))

            Me.cmbTienda_AddProd.SelectedValue = Me.CboTienda.SelectedValue

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTipoDocumento.SelectedIndexChanged

        Dim cbo As New Combo
        cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboPropietario.SelectedValue), CInt(Me.CboTienda.SelectedValue) _
           , CInt(Me.cboTipoDocumento.SelectedValue))
        Me.txtCodigoDocumento.Text = ""
        GenerarCodigo()
        llenarMontosMaximos()
        ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True) ' Esto es porque en la boleta la percepcion se calcula diferente

    End Sub
    Private Sub GenerarCodigo()
        Dim obj As New Negocio.Serie
        Dim sc As New ScriptManagerClass
        Try


            If Me.cboSerie.Items.Count = 0 Then

                sc.mostrarMsjAlerta(Me, "No Existe Serie asignada para este Documento")
            Else
                Me.txtCodigoDocumento.Text = obj.GenerarCodigo(CInt(Me.cboSerie.SelectedValue))
            End If
        Catch ex As Exception
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSerie.SelectedIndexChanged


        Me.txtCodigoDocumento.Text = ""

        GenerarCodigo()


    End Sub

    Protected Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMoneda.SelectedIndexChanged

        Me.lblSimboloMoneda1.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblSimboloMoneda2.Text = Me.cboMoneda.SelectedItem.ToString

        lblMonto_Acumulado.Text = Me.cboMoneda.SelectedItem.ToString
        lblMonto_Saldo.Text = Me.cboMoneda.SelectedItem.ToString
        lblMonto_VueltoTotal.Text = Me.cboMoneda.SelectedItem.ToString


        lblMonedaCancelacion_TotalAPagar.Text = Me.cboMoneda.SelectedItem.ToString
        lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString
        lblMontoFaltante_Vuelto.Text = Me.cboMoneda.SelectedItem.ToString

        Actualizarlistapreciosdetalle()

        actualizarDatosListaCancelacion(0, DGV_Cancelacion)
        actualizarDatosListaCancelacion(1, DGV_Vuelto)

        Me.cmbMoneda_Vuelto.SelectedValue = Me.cboMoneda.SelectedValue
        Me.cmbMonedaCancelacion_Efectivo.SelectedValue = Me.cboMoneda.SelectedValue

        llenarMontosMaximos()

        DGV_AddProd.DataSource = Nothing
        DGV_AddProd.DataBind()

        ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)
    End Sub
#End Region
#Region "Datos del Detalle"



    Private Function getListaDetalle() As List(Of Entidades.DetalleDocumentoView)
        Return CType(Session.Item("listaD"), List(Of Entidades.DetalleDocumentoView))
    End Function

    Private Sub setListaDetalle(ByVal lista As List(Of Entidades.DetalleDocumentoView))

        Session.Add("listaD", lista)
    End Sub

    Private Function A�adidoProducto(ByVal IdProducto As Integer) As Boolean
        Dim lista As New List(Of Entidades.DetalleDocumentoView)
        Dim flag As Boolean = False
        lista = Me.getListaDetalle
        If lista.Count = 0 Then
            Return flag
        End If
        For i As Integer = 0 To lista.Count - 1
            If lista(i).IdProducto = IdProducto Then
                Return True
            End If
        Next
        Return flag
    End Function
    Private Sub EliminaFilaGrillaDetalle(ByVal index As Integer)
        Dim Lista As New List(Of Entidades.DetalleDocumentoView)
        Lista = Me.getListaDetalle
        If index >= 0 And index < Lista.Count Then
            'If Me.listaUMedida.Item(index).UnidadPrincipal Then
            '    M_chbUMPrincipal.Enabled = True
            '    M_chbUMPrincipal.Checked = True
            'End If
            Lista.RemoveAt(index)
            gvDetalle.DataSource = Lista
            gvDetalle.DataBind()
        End If
        Me.setListaDetalle(Lista)
    End Sub
    Protected Sub gvDetalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvDetalle.SelectedIndexChanged
        'Elimina un Item de la Grilla
        ActualizarGrillaDetalle()
        EliminaFilaGrillaDetalle(gvDetalle.SelectedIndex)
        If Me.gvDetalle.Rows.Count >= 0 Then
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)
        End If
    End Sub
#End Region
#Region "Guardar Cliente Nuevo"
    Protected Sub btnGuardarCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardarCliente.Click
        Me.AgregarCliente()
    End Sub
    Private Sub AgregarCliente()

        Try

            Dim objPersonaView As New Entidades.PersonaView
            With objPersonaView

                .IdTipoAgente = CInt(Me.cmbTipoAgente_RegistrarCliente.SelectedValue)
                .RazonSocial = Me.txtRazonSocial_Cliente.Text.Trim
                .ApPaterno = Me.txtApPaterno_Cliente.Text.Trim
                .ApMaterno = Me.txtApMaterno_Cliente.Text.Trim
                .Nombres = Me.txtNombres_Cliente.Text.Trim
                .Dni = Me.txtDNI_Cliente.Text.Trim
                .Ruc = Me.txtRUC_Cliente.Text.Trim
                .Direccion = Me.txtDireccion_Cliente.Text.Trim
                .Telefeono = Me.txtTelefono_Cliente.Text.Trim
                .IdPersona = (New Negocio.Persona).RegistrarCliente_Ventas(objPersonaView, cmbTipoPersona.SelectedValue, CInt(Me.cboPropietario.SelectedValue))

            End With

            If objPersonaView.IdPersona <> Nothing Then
                '**************** Me.GetClienteGuardado(objPersonaView.IdPersona)
                addPersona(objPersonaView.IdPersona)
            Else
                Throw New Exception("Problemas en la Operaci�n.")
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub GetClienteGuardado(ByVal IdCliente As Integer)
        Dim objCliente As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Cliente = objCliente.SelectxId(IdCliente)
        Me.LLenarCliente(Cliente)
    End Sub
#End Region
#Region "Botones del Formulario"
    Private Sub HabilitarWrite()

        Select Case CInt(hddModoFrm.Value)
            Case 0 '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                '**Me.btnCancelar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnDespachar.Visible = False
                Me.LinkButton1.Visible = False

                Me.cboPropietario.Enabled = False
                Me.CboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboTipoDocumento.Enabled = False

                Panel_FrmPrincipal.Enabled = False

                '****************** 
                Me.btnCanjearCotizacion.Visible = False
                Me.btnGenerarCompPercepcion.Visible = False


            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                '**    Me.btnCancelar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnDespachar.Visible = False
                Me.LinkButton1.Visible = False

                Panel_FrmPrincipal.Enabled = True

                Me.cboPropietario.Enabled = True
                Me.CboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboTipoDocumento.Enabled = True

                '****************** 
                Me.btnCanjearCotizacion.Visible = False
                Me.btnGenerarCompPercepcion.Visible = False

            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                '**    Me.btnCancelar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnDespachar.Visible = False
                Me.LinkButton1.Visible = False




                Panel_FrmPrincipal.Enabled = True

                Me.cboPropietario.Enabled = False
                Me.CboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboTipoDocumento.Enabled = False

                '****************** 
                Me.btnCanjearCotizacion.Visible = False
                Me.btnGenerarCompPercepcion.Visible = False

            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = True

                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()

                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                '**         Me.btnCancelar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnDespachar.Visible = False
                Me.LinkButton1.Visible = False

                Panel_FrmPrincipal.Enabled = True

                Me.cboPropietario.Enabled = True
                Me.CboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboTipoDocumento.Enabled = True

                '****************** 
                Me.btnCanjearCotizacion.Visible = False
                Me.btnGenerarCompPercepcion.Visible = False

            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                '**     Me.btnCancelar.Visible = True
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True
                Me.btnDespachar.Visible = True
                Me.LinkButton1.Visible = True

                Panel_FrmPrincipal.Enabled = False

                Me.cboPropietario.Enabled = False
                Me.CboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboTipoDocumento.Enabled = False
                '****************** 
                Me.btnCanjearCotizacion.Visible = True
                Me.btnGenerarCompPercepcion.Visible = True

            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                '**     Me.btnCancelar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnDespachar.Visible = True
                Me.LinkButton1.Visible = True

                Panel_FrmPrincipal.Enabled = False

                Me.cboPropietario.Enabled = False
                Me.CboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboTipoDocumento.Enabled = False
                '****************** 
                Me.btnCanjearCotizacion.Visible = True
                Me.btnGenerarCompPercepcion.Visible = True

            Case 6  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                '**     Me.btnCancelar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnDespachar.Visible = False
                Me.LinkButton1.Visible = False

                Panel_FrmPrincipal.Enabled = False


                Me.cboPropietario.Enabled = False
                Me.CboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboTipoDocumento.Enabled = False

                '****************** 
                Me.btnCanjearCotizacion.Visible = False
                Me.btnGenerarCompPercepcion.Visible = False

        End Select
    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click
        verFrm("2", False, False, False, False, False, False, 0, 0)

        '******** Calculamos los valores de vuelto
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontosCancelacion();", True)
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click


        Me.Agregar()

    End Sub
    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click

        Anular()

    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        verFrm("3", False, False, True, True, True, True, 1, 1)

    End Sub

#End Region
#Region "Procesos del Formulario"
    Private Sub LimpiarControles()
        Me.gvDetalle.DataSource = Nothing
        Me.gvDetalle.DataBind()
        Me.gvLineaCredito.DataSource = Nothing
        Me.gvLineaCredito.DataBind()

        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaACobrar.Text = Me.txtFechaEmision.Text
        Me.txtFechaAEntregar.Text = Me.txtFechaEmision.Text
        Me.txtFechaVencimiento.Text = Me.txtFechaEmision.Text

        hddIdDocumento.Value = ""
        Me.txtCodigoDocumento.Text = ""
        Me.cboMoneda.SelectedIndex = 0
        Me.cboEstadoDocumento.SelectedIndex = 0

        Me.txtDescuento.Text = "0"
        Me.txtSubTotal.Text = "0"
        Me.txtIGV.Text = "0"
        Me.txtTotal.Text = "0"
        Me.txtPercepcion.Text = "0"
        Me.txtValorReferencial.Text = "0"
        Me.txtDetraccion.Text = "0"
        Me.txtRetencion.Text = "0"
        Me.txtTotalAPagar.Text = "0"

        Me.txtApPaterno_RazonSocial.Text = ""
        Me.txtApMaterno.Text = ""
        Me.txtNombres.Text = ""
        Me.txtRuc.Text = ""
        Me.txtDni.Text = ""
        Me.txtCodigoCliente.Text = ""
        Me.txtTelefono.Text = ""
        Me.txtDireccionCliente.Text = ""


        Me.cboEstadoCancelacion.SelectedIndex = 0
        Me.cboCondicionPago.SelectedIndex = 0
        Me.cboMedioPago.SelectedIndex = 0

        Me.txtFechaVencimiento.Visible = False


        lblMonedaCancelacion_TotalAPagar.Text = "S/."
        lblMonedaVuelto.Text = "S/."

        Me.cboBanco.SelectedIndex = 0
        Me.txtNCheque.Text = ""
        Me.cboNCuenta.SelectedIndex = 0
        Me.txtNOperacion.Text = ""
        Me.hddIdCuentaPersona.Value = ""

        Me.cboBanco.Visible = False
        Me.txtNCheque.Visible = False
        Me.txtFechaACobrar.Visible = False
        Me.cboNCuenta.Visible = False
        Me.txtNOperacion.Visible = False

        Me.cboDepartamento.SelectedIndex = 0
        Me.txtDireccionEntrega.Text = ""





        Me.cboTipoPersona.SelectedIndex = 0
        Me.cboTipoPrecioV.SelectedIndex = 0
        Me.cboTipoAgente.SelectedIndex = 0
        Me.txtTasaAgente.Text = "0"

        Me.cboEstadoEntrega.SelectedIndex = 0
        Me.txtFechaAEntregar.Text = ""
        Me.cboDepartamento.SelectedIndex = 0
        Me.txtDireccionEntrega.Text = ""

        Me.DGV_Cancelacion.DataSource = Nothing
        Me.DGV_Cancelacion.DataBind()

        Me.DGV_Vuelto.DataSource = Nothing
        Me.DGV_Vuelto.DataBind()

        Me.txtMonto.Text = "0"
        Me.txtEfectivo.Text = "0"
        Me.txtMonto_Acumulado.Text = "0"
        Me.txtMonto_Saldo.Text = "0"
        Me.txtMonto_VueltoEfectivo.Text = "0"
        Me.txtMonto_VueltoTotal.Text = "0"
        Me.txtVueltoTotal.Text = "0"
        Me.txtMontoFaltante_Vuelto.Text = "0"


        Me.DGV_ImpresionDoc_Cab.DataSource = Nothing
        Me.DGV_ImpresionDoc_Cab.DataBind()
        Me.DGV_ImpresionDoc_Det.DataSource = Nothing
        Me.DGV_ImpresionDoc_Det.DataBind()

        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.txtCodigoSubLinea_AddProd.Text = ""
        Me.txtDescripcionProd_AddProd.Text = ""

        Me.txtIdCliente_BuscarxId.Text = ""

        hddIdAlmacen.Value = ""
        hddNomAlmacen.Value = ""

    End Sub

    Private Sub Anular()

        Try

            If ((New Negocio.Documento).AnulaDocumentoVentaT(CInt(Me.hddIdDocumento.Value))) Then

                Me.cboEstadoDocumento.SelectedValue = CStr(2) '************ Anulado
                hddModoFrm.Value = "6"
                HabilitarWrite()

                objScript.mostrarMsjAlerta(Me, "El Documento se Anul� Correctamente")



            Else
                Throw New Exception()
            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try






    End Sub
#End Region
#Region "Datos de Cancelaci�n"
    Protected Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboBanco.SelectedIndexChanged
        Dim cbo As New Combo
        cbo.LlenarCboCuentaBancaria(cboNCuenta, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboPropietario.SelectedValue), True)
    End Sub
    'Protected Sub cboMonedaCancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMonedaCancelacion.SelectedIndexChanged
    '    Select Case CInt(Me.cboMonedaCancelacion.SelectedValue)
    '        Case 1 ' Soles
    '            If Me.cboMonedaCancelacion.SelectedValue = Me.cboMoneda.SelectedValue Then
    '                Me.txtMonto.Text = Me.txtTotalAPagar.Text
    '            Else
    '                Dim cambio As Decimal = CDec(Me.txtMonto.Text) * CDec(Me.txtVentaOf.Text)
    '                Me.txtMonto.Text = CStr(Decimal.Round(cambio, 2))
    '            End If

    '        Case 2 ' Dolares
    '            If Me.cboMonedaCancelacion.SelectedValue = Me.cboMoneda.SelectedValue Then
    '                Me.txtMonto.Text = Me.txtTotalAPagar.Text
    '            Else
    '                Dim cambio As Decimal = CDec(Me.txtMonto.Text) / CDec(Me.hddCompraC.Value)
    '                Me.txtMonto.Text = CStr(Decimal.Round(cambio, 2))
    '            End If
    '    End Select
    '    Me.txtEfectivo.Text = "0"
    '    'Me.txtVuelto.Text = "0"
    'End Sub
    Private Sub actualizarControlesCondicionPago(ByVal IdCondicionPago As Integer)
        Try

            Select Case IdCondicionPago

                Case 1 '************* CONTADO

                    Me.cboMedioPago.Visible = True
                    Me.lblMedioPago.Visible = True

                    Me.lblFechaVencimiento.Visible = False
                    Me.txtFechaVencimiento.Visible = False

                    '********** Me.cboTipoPrecioV.SelectedValue = CStr(ViewState("IdTipoPV_Cliente")) '**** Precio por default de cliente



                Case 2 '************* CREDITO

                    Me.cboMedioPago.Visible = False
                    Me.lblMedioPago.Visible = False

                    Me.lblFechaVencimiento.Visible = True
                    Me.txtFechaVencimiento.Visible = True

                    '********* Me.cboTipoPrecioV.SelectedValue = CStr(5) ' Precios de Cr�dito



            End Select


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub cboCondicionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCondicionPago.SelectedIndexChanged

        actualizarControlesCondicionPago(CInt(Me.cboCondicionPago.SelectedValue))

        '****************** Actualizamos los precios del detale del documento
        '*** Actualizarlistapreciosdetalle()

        '***  ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)

    End Sub


    Private Sub actualizarControlesMedioPago(ByVal IdMedioPago As Integer)

        Try

            Me.btnViewCapaNotaCredito.Visible = False
            Me.btnAdd_MedioCancelacion.Visible = True

            Select Case CInt(Me.cboMedioPago.SelectedValue)
                Case 1 '"Efectivo"
                    Me.lblBanco.Visible = False
                    Me.cboBanco.Visible = False
                    Me.lblCheque.Visible = False
                    Me.txtNCheque.Visible = False
                    Me.lblFechaCobro.Visible = False
                    Me.txtFechaACobrar.Visible = False

                    Me.lblNOperacion.Visible = False
                    Me.txtNOperacion.Visible = False
                    Me.lblNCuenta.Visible = False
                    Me.cboNCuenta.Visible = False

                Case 2 '*************************** NOTA DE CREDITO

                    Me.lblBanco.Visible = False
                    Me.cboBanco.Visible = False
                    Me.lblCheque.Visible = False
                    Me.txtNCheque.Visible = False
                    Me.lblFechaCobro.Visible = False
                    Me.txtFechaACobrar.Visible = False
                    Me.btnAdd_MedioCancelacion.Visible = False
                    Me.btnViewCapaNotaCredito.Visible = True
                    Me.lblNOperacion.Visible = False
                    Me.txtNOperacion.Visible = False
                    Me.lblNCuenta.Visible = False
                    Me.cboNCuenta.Visible = False

                Case 3 ' "Transferencia"

                    Me.cboBanco.SelectedIndex = 0

                    Dim cbo As New Combo
                    cbo.LlenarCboCuentaBancaria(cboNCuenta, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboPropietario.SelectedValue), True)

                    Me.lblBanco.Visible = True
                    Me.cboBanco.Visible = True
                    Me.lblCheque.Visible = False
                    Me.txtNCheque.Visible = False
                    Me.lblFechaCobro.Visible = False
                    Me.txtFechaACobrar.Visible = False



                    Me.lblNOperacion.Text = "N� de Operaci�n"
                    Me.lblNOperacion.Visible = True
                    Me.txtNOperacion.Visible = True
                    Me.lblNCuenta.Visible = True
                    Me.cboNCuenta.Visible = True


                Case 7 '"Cheque"

                    Me.cboBanco.SelectedIndex = 0

                    Me.lblBanco.Visible = True
                    Me.cboBanco.Visible = True
                    Me.lblCheque.Visible = True
                    Me.txtNCheque.Visible = True
                    Me.lblFechaCobro.Visible = True
                    Me.txtFechaACobrar.Visible = True

                    Me.lblNOperacion.Visible = False
                    Me.txtNOperacion.Visible = False
                    Me.lblNCuenta.Visible = False
                    Me.cboNCuenta.Visible = False


                Case 8 '"Deposito"

                    Me.cboBanco.SelectedIndex = 0
                    Dim cbo As New Combo
                    cbo.LlenarCboCuentaBancaria(cboNCuenta, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboPropietario.SelectedValue), True)

                    Me.lblBanco.Visible = True
                    Me.cboBanco.Visible = True
                    Me.lblCheque.Visible = False
                    Me.txtNCheque.Visible = False
                    Me.lblFechaCobro.Visible = False
                    Me.txtFechaACobrar.Visible = False

                    Me.lblNOperacion.Text = "N� de Dep�sito"
                    Me.lblNOperacion.Visible = True
                    Me.txtNOperacion.Visible = True
                    Me.lblNCuenta.Visible = True
                    Me.cboNCuenta.Visible = True

                Case 9 '************* Comprobante Retenci�n

                    Me.lblBanco.Visible = False
                    Me.cboBanco.Visible = False
                    Me.lblCheque.Visible = False
                    Me.txtNCheque.Visible = False
                    Me.lblFechaCobro.Visible = False
                    Me.txtFechaACobrar.Visible = False

                    Me.lblNOperacion.Visible = True
                    Me.lblNOperacion.Text = "N� Comp. Retenci�n:"
                    Me.txtNOperacion.Visible = True
                    Me.lblNCuenta.Visible = False
                    Me.cboNCuenta.Visible = False

            End Select


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged

        actualizarControlesMedioPago(CInt(Me.cboMedioPago.SelectedValue))

    End Sub
#End Region
#Region "Datos de Entrega"
    Protected Sub cboDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboDepartamento.SelectedIndexChanged
        Dim cbo As New Combo
        cbo.LLenarCboProvincia(Me.cboProvincia, Me.cboDepartamento.SelectedValue)
        cbo.LLenarCboDistrito(Me.cboDistrito, Me.cboDepartamento.SelectedValue, Me.cboProvincia.SelectedValue)
        Me.cboProvincia.Focus()
    End Sub

    Protected Sub cboProvincia_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboProvincia.SelectedIndexChanged
        Dim cbo As New Combo
        cbo.LLenarCboDistrito(Me.cboDistrito, Me.cboDepartamento.SelectedValue, Me.cboProvincia.SelectedValue)
        Me.cboDistrito.Focus()
    End Sub
#End Region
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LinkButton1.Click
        'Response.Redirect("~/Almacen1/FrmGuiaRemision_1.aspx?IdDocumento=" + hddIdDocumento.Value)
    End Sub





    Private Sub DGV_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarPersona.SelectedIndexChanged
        addPersona(CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text))
    End Sub

    Private Sub validarDatoCancelacionNC()

        Me.listaCancelacion = getListaCancelacion()

        For i As Integer = 0 To Me.listaCancelacion.Count - 1

            If (Me.listaCancelacion(i).IdMedioPago = 2) Then  '*******  NOTA DE CREDITO
                Throw New Exception("EXISTEN DATOS DE CANCELACI�N CON MEDIO DE PAGO [ Nota de Cr�dito ]. NO SE PERMITE LA OPERACI�N.")
            End If

        Next

    End Sub

    Private Sub addPersona(ByVal IdPersona As Integer, Optional ByVal valDatoCancelacion As Boolean = True)
        Try

            If (valDatoCancelacion) Then
                validarDatoCancelacionNC()
            End If

            Dim objCliente As Entidades.PersonaView = (New Negocio.Cliente).SelectxId_Ventas(IdPersona, CInt(Me.cboPropietario.SelectedValue))

            Me.LLenarCliente(objCliente)

            '********* Para volver a calcular los totales para efectos si es agente de percepcion o Retenci�n.
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "addPersona_Venta();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub BuscarClientexId(ByVal IdCliente As Integer)
        Me.LimpiarBuscarCliente()
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Cliente = obj.SelectxId(IdCliente)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception

            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then



                Dim objProductoTipoPV As New Negocio.ProductoTipoPV

                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)

                Dim cboUM As DropDownList = CType(gvrow.FindControl("cmbUnidadMedida_AddProd"), DropDownList)

                Me.listaCatalogoSelect(e.Row.RowIndex).ListaUM_Venta = objProductoTipoPV.SelectUM_Venta(Me.listaCatalogoSelect(e.Row.RowIndex).IdProducto, CInt(Me.cmbTienda_AddProd.SelectedValue), Me.listaCatalogoSelect(e.Row.RowIndex).IdTipoPV)

                cboUM.DataSource = Me.listaCatalogoSelect(e.Row.RowIndex).ListaUM_Venta
                cboUM.DataBind()

                cboUM.SelectedValue = Me.listaCatalogoSelect(e.Row.RowIndex).IdUnidadMedida.ToString

                setListaCatalogo(Me.listaCatalogoSelect)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try

            ActualizarListaCatalogo()

            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Me.listaCatalogoSelect = Me.getListaCatalogo

            Dim IdTipoPV As Integer = 1

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            For i As Integer = 0 To DGV_AddProd.Rows.Count - 1

                If cboUM.ClientID = CType(DGV_AddProd.Rows(i).Cells(3).FindControl("cmbUnidadMedida_AddProd"), DropDownList).ClientID Then


                    Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams(CInt(Me.cmbAlmacen_AddProd.SelectedValue), CInt(Me.cboMoneda.SelectedValue), _
                    CInt(cboUM.SelectedValue), Me.listaCatalogoSelect(i).IdProducto, Me.listaCatalogoSelect(i).IdTienda, IdTipoPV)

                    If objCatalogo IsNot Nothing Then

                        Me.listaCatalogoSelect(i).PrecioSD = objCatalogo.PrecioSD
                        Me.listaCatalogoSelect(i).StockAReal = objCatalogo.StockAReal
                        Me.listaCatalogoSelect(i).StockComprometido = objCatalogo.StockComprometido

                    End If




                    setListaCatalogo(Me.listaCatalogoSelect)

                    DGV_AddProd.DataSource = Me.listaCatalogoSelect
                    DGV_AddProd.DataBind()

                    Return

                End If

            Next


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub ActualizarListaCatalogo()

        Try

            Me.listaCatalogoSelect = getListaCatalogo()

            For i As Integer = 0 To DGV_AddProd.Rows.Count - 1

                If (CType(DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text.Trim.Length > 0) Then
                    Me.listaCatalogoSelect(i).IdUnidadMedida = CInt(CType(DGV_AddProd.Rows(i).Cells(3).FindControl("cmbUnidadMedida_AddProd"), DropDownList).SelectedValue)
                    Me.listaCatalogoSelect(i).Cantidad = CDec(CType(DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)
                End If

            Next

            setListaCatalogo(Me.listaCatalogoSelect)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub


    Private Sub btnAddProductos_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos_AddProd.Click
        Try

            ActualizarListaCatalogo()
            ActualizarGrillaDetalle()

            '*************** Valido el Almac�n de Salida
            Me.listaDetalleDocumento = getListaDetalle()

            If Me.listaDetalleDocumento.Count <= 0 Then

                hddIdAlmacen.Value = cmbAlmacen_AddProd.SelectedValue
                hddNomAlmacen.Value = cmbAlmacen_AddProd.SelectedItem.ToString

            End If





            Dim ListaDetalle As List(Of Entidades.DetalleDocumentoView) = Me.getListaDetalle

            Me.listaCatalogoSelect = getListaCatalogo()


            For i As Integer = 0 To DGV_AddProd.Rows.Count - 1


                If (Me.listaCatalogoSelect(i).Cantidad > 0) Then

                    Dim Obj As New Entidades.DetalleDocumentoView
                    Obj.IdProducto = Me.listaCatalogoSelect(i).IdProducto
                    Obj.Cantidad = Me.listaCatalogoSelect(i).Cantidad
                    Obj.StockDisponible = Me.listaCatalogoSelect(i).StockDisponible

                    Obj.ListaUMedida = Me.listaCatalogoSelect(i).ListaUM_Venta

                    Obj.Descripcion = Me.listaCatalogoSelect(i).Descripcion
                    Obj.PrecioSD = Me.listaCatalogoSelect(i).PrecioSD
                    Obj.Descuento = 0
                    Obj.PDescuento = 0
                    Obj.PrecioCD = Me.listaCatalogoSelect(i).PrecioSD
                    Obj.Importe = 0
                    Obj.Percepcion = Me.listaCatalogoSelect(i).Percepcion
                    Obj.Detraccion = Me.listaCatalogoSelect(i).Detraccion

                    Obj.IdUMedida = Me.listaCatalogoSelect(i).IdUnidadMedida

                    Obj.IdTienda_PV = Me.listaCatalogoSelect(i).IdTienda

                    ListaDetalle.Add(Obj)
                End If

            Next

            Me.setListaDetalle(ListaDetalle)


            Me.gvDetalle.DataSource = ListaDetalle
            Me.gvDetalle.DataBind()

            '************ Calculamos los totales
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btnAceptar_CapaArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_CapaArea.Click
        Try

            '**************** Actualizo mi lista detalle
            ActualizarGrillaDetalle()

            '**************** Obtengo la equivalencia

            Dim objProductoNegocio As New Negocio.Producto

            Me.listaDetalleDocumento = Me.getListaDetalle

            Dim index As Integer = CInt(hddIndexDetalle_Area.Value)


            Me.listaDetalleDocumento(index).Cantidad = objProductoNegocio.SelectEquivalenciaEntreUM(Me.listaDetalleDocumento(index).IdProducto, 18, Me.listaDetalleDocumento(index).IdUMedida, (CDec(txtAncho.Text) * CDec(txtLargo.Text) * CDec(txtCantidadRetazo.Text)))

            Me.listaDetalleDocumento(index).Glosa = txtCantidadRetazo.Text + " de ( " + txtAncho.Text + " m. x " + txtLargo.Text + " m. )"


            '***************************** Actualizo su Precio de Venta           

            Dim IdTipoPV As Integer = 1

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams(CInt(hddIdAlmacen.Value), CInt(Me.cboMoneda.SelectedValue), _
                Me.listaDetalleDocumento(index).IdUMedida, Me.listaDetalleDocumento(index).IdProducto, Me.listaDetalleDocumento(index).IdTienda_PV, IdTipoPV, True)


            Me.listaDetalleDocumento(index).PrecioSD = objCatalogo.PrecioSD
            Me.listaDetalleDocumento(index).PrecioCD = objCatalogo.PrecioSD
            Me.listaDetalleDocumento(index).StockDisponible = objCatalogo.StockDisponible

            Me.listaDetalleDocumento(index).PDescuento = 0
            Me.listaDetalleDocumento(index).Descuento = 0



            '********************* Guardo la Lita Detalle

            Me.setListaDetalle(Me.listaDetalleDocumento)

            gvDetalle.DataSource = Me.listaDetalleDocumento
            gvDetalle.DataBind()

            ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub




    Private Sub btnAceptar_RetazoMetros_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_RetazoMetros.Click

        Try

            '**************** Actualizo mi lista detalle
            ActualizarGrillaDetalle()

            '**************** Obtengo la equivalencia

            Dim objProductoNegocio As New Negocio.Producto

            Me.listaDetalleDocumento = Me.getListaDetalle

            Dim index As Integer = CInt(hddIndexDetalle_Area.Value)


            Me.listaDetalleDocumento(index).Cantidad = objProductoNegocio.SelectEquivalenciaEntreUM(Me.listaDetalleDocumento(index).IdProducto, 15, Me.listaDetalleDocumento(index).IdUMedida, (CDec(txtLargo_Metros.Text) * CDec(txtCantidad_Metros.Text)))

            Me.listaDetalleDocumento(index).Glosa = txtCantidad_Metros.Text + " de ( " + txtLargo_Metros.Text + " m.)"


            '***************************** Actualizo su Precio de Venta           

            Dim IdTipoPV As Integer = 1

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If


            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams(CInt(hddIdAlmacen.Value), CInt(Me.cboMoneda.SelectedValue), _
                Me.listaDetalleDocumento(index).IdUMedida, Me.listaDetalleDocumento(index).IdProducto, Me.listaDetalleDocumento(index).IdTienda_PV, IdTipoPV, True)


            Me.listaDetalleDocumento(index).PrecioSD = objCatalogo.PrecioSD
            Me.listaDetalleDocumento(index).PrecioCD = objCatalogo.PrecioSD
            Me.listaDetalleDocumento(index).StockDisponible = objCatalogo.StockDisponible

            Me.listaDetalleDocumento(index).PDescuento = 0
            Me.listaDetalleDocumento(index).Descuento = 0



            '********************* Guardo la Lita Detalle

            Me.setListaDetalle(Me.listaDetalleDocumento)

            gvDetalle.DataSource = Me.listaDetalleDocumento
            gvDetalle.DataBind()

            ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try



    End Sub





    Private Sub Actualizarlistapreciosdetalle()
        Try

            ActualizarGrillaDetalle()

            Me.listaDetalleDocumento = Me.getListaDetalle

            '*************** Modificamos los precios        

            Me.txtEfectivo.Text = "0"

            Dim IdTipoPV As Integer = 1

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If


            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams(CInt(Me.hddIdAlmacen.Value), CInt(Me.cboMoneda.SelectedValue), _
                        Me.listaDetalleDocumento(i).IdUMedida, Me.listaDetalleDocumento(i).IdProducto, Me.listaDetalleDocumento(i).IdTienda_PV, IdTipoPV, Me.listaDetalleDocumento(i).HasGlosa)


                If objCatalogo IsNot Nothing Then

                    Me.listaDetalleDocumento(i).PrecioSD = objCatalogo.PrecioSD
                    Me.listaDetalleDocumento(i).PrecioCD = objCatalogo.PrecioSD

                    Me.listaDetalleDocumento(i).StockDisponible = objCatalogo.StockDisponible

                    Me.listaDetalleDocumento(i).Descuento = 0
                    Me.listaDetalleDocumento(i).PDescuento = 0

                End If

            Next

            Me.setListaDetalle(Me.listaDetalleDocumento)

            Me.gvDetalle.DataSource = Me.getListaDetalle
            Me.gvDetalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnAdd_MedioCancelacion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd_MedioCancelacion.Click
      
        calcularCancelacionDocumento()

    End Sub
    Private Sub calcularCancelacionDocumento()

        Try

            Me.listaCancelacion = getListaCancelacion()

            Dim objPagoCaja As New Entidades.PagoCaja

            With objPagoCaja

                .Efectivo = CDec(txtEfectivo.Text)
                .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                .IdMoneda = CInt(Me.cmbMonedaCancelacion_Efectivo.SelectedValue)
                .IdTipoMovimiento = 1   '******** Ingreso
                .Factor = 1

                .NomMedioPago = Me.cboMedioPago.SelectedItem.ToString
                .NomMoneda = Me.cmbMonedaCancelacion_Efectivo.SelectedItem.ToString

                '******************** Obtengo los valores de acuerdo a la moneda de emisi�n

                .NomMonedaDestino = cboMoneda.SelectedItem.ToString
                .IdMonedaDestino = CInt(cboMoneda.SelectedValue)
                .MontoEquivalenteDestino = (New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(.IdMoneda, .IdMonedaDestino, .Efectivo, "E")

                .EfectivoInicial = .Efectivo
            End With

            Select Case CInt(Me.cboMedioPago.SelectedValue)
                Case 1
                    '*************** efectivo                  
                Case 3
                    '****************** Transferencia
                    With objPagoCaja

                        .IdBanco = CInt(Me.cboBanco.SelectedValue)
                        .NomBanco = Me.cboBanco.SelectedItem.ToString
                        .NumeroCuenta = Me.cboNCuenta.SelectedItem.ToString
                        .NumeroOp = Me.txtNOperacion.Text
                        .IdCuentaBancaria = CInt(Me.cboNCuenta.SelectedValue)

                    End With

                Case 7 '******************* Cheque

                    With objPagoCaja

                        .IdBanco = CInt(Me.cboBanco.SelectedValue)
                        .NomBanco = Me.cboBanco.SelectedItem.ToString
                        .NumeroCheque = Me.txtNCheque.Text

                        Try
                            .FechaACobrar = CDate(Me.txtFechaACobrar.Text)
                        Catch ex As Exception
                            .FechaACobrar = Nothing
                        End Try



                        .FechaACobrar = CDate(IIf(IsDate(Me.txtFechaACobrar.Text) = True, Me.txtFechaACobrar.Text, Nothing))


                    End With

                Case 8
                    '****************** Deposito
                    With objPagoCaja


                        .IdBanco = CInt(Me.cboBanco.SelectedValue)
                        .NomBanco = Me.cboBanco.SelectedItem.ToString
                        .NumeroCuenta = Me.cboNCuenta.SelectedItem.ToString
                        .NumeroOp = Me.txtNOperacion.Text
                        .IdCuentaBancaria = CInt(Me.cboNCuenta.SelectedValue)

                    End With

                Case 9
                    '************** Comprobante de Retenci�n
                    With objPagoCaja
                        .NumeroOp = Me.txtNOperacion.Text
                    End With

            End Select

            Me.listaCancelacion.Add(objPagoCaja)

            setListaCancelacion(Me.listaCancelacion)

            DGV_Cancelacion.DataSource = Me.listaCancelacion
            DGV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontosCancelacion();", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub


    Private Sub DGV_Cancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_Cancelacion.SelectedIndexChanged
        EliminarRegistroListaPagoCaja(0, DGV_Cancelacion)
    End Sub



    Private Sub actualizarDatosListaCancelacion(ByVal opcion As Integer, ByVal grilla As GridView)
        Try

            Dim objUtil As New Negocio.Util

            Dim lista As List(Of Entidades.PagoCaja) = Nothing

            Select Case opcion

                Case 0
                    '************* Lista cancelaci�n
                    lista = getListaCancelacion()

                Case 1
                    '************* Lista vuelto
                    lista = getListaVuelto()

            End Select


            'Me.listaCancelacion = getListaCancelacion()

            For i As Integer = 0 To lista.Count - 1

                With lista(i)

                    .IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    .MontoEquivalenteDestino = objUtil.SelectValorxIdMonedaOxIdMonedaD(.IdMoneda, .IdMonedaDestino, .Monto, "E")
                    .NomMonedaDestino = Me.cboMoneda.SelectedItem.ToString

                End With

            Next

            Select Case opcion

                Case 0
                    '************* Lista cancelaci�n
                    setListaCancelacion(lista)

                Case 1
                    '************* Lista vuelto
                    setListaVuelto(lista)

            End Select

            grilla.DataSource = lista
            grilla.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Protected Sub btnAddVuelto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddVuelto.Click

        calcularVueltoDocumento()




    End Sub

    Private Sub DGV_Vuelto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_Vuelto.SelectedIndexChanged
        EliminarRegistroListaPagoCaja(1, DGV_Vuelto)
    End Sub

    Private Sub EliminarRegistroListaPagoCaja(ByVal opcion As Integer, ByVal grilla As GridView)

        Try
            '****************** eliminamos el registro

            Dim lista As List(Of Entidades.PagoCaja) = Nothing

            Select Case opcion

                Case 0
                    '************* Cancelacion
                    lista = getListaCancelacion()

                Case 1
                    '************* Vuelto
                    lista = getListaVuelto()

            End Select

            lista.RemoveAt(grilla.SelectedIndex)

            Select Case opcion

                Case 0
                    '************* Cancelacion
                    setListaCancelacion(lista)

                Case 1
                    '*************** Vuelto
                    setListaVuelto(lista)

            End Select

            grilla.DataSource = lista
            grilla.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontosCancelacion();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub calcularVueltoDocumento()
        Try

            Me.listaVuelto = getListaVuelto()

            Dim objPagoCaja As New Entidades.PagoCaja


            With objPagoCaja

                .Efectivo = CDec(txtMonto_VueltoEfectivo.Text)

                .IdMedioPago = 1  '**************** Efectivo

                .IdMoneda = CInt(Me.cmbMoneda_Vuelto.SelectedValue)

                .IdTipoMovimiento = 1   '******** Ingreso

                .Factor = 1

                .NomMedioPago = "Efectivo"
                .NomMoneda = Me.cmbMoneda_Vuelto.SelectedItem.ToString

                '******************** Obtengo los valores de acuerdo a la moneda de emisi�n

                .NomMonedaDestino = cboMoneda.SelectedItem.ToString
                .IdMonedaDestino = CInt(cboMoneda.SelectedValue)
                .MontoEquivalenteDestino = (New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(.IdMoneda, .IdMonedaDestino, .Efectivo, "E")

            End With


            Me.listaVuelto.Add(objPagoCaja)

            setListaVuelto(Me.listaVuelto)

            DGV_Vuelto.DataSource = Me.listaVuelto
            DGV_Vuelto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontosCancelacion();", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Function obtenerListaMovCuenta(ByVal IdCondicionPago As Integer) As List(Of Entidades.MovCuenta)

        Dim objMovCuenta As New List(Of Entidades.MovCuenta)

        Try

            Select Case IdCondicionPago


                Case 1
                    '************* Contado
                    '**************** Devolvemos NULO
                Case 2
                    '*************** Cr�dito
                    '**************** PENDIENTE
                    'objMovCuenta = New Entidades.MovCuenta

                    'With objMovCuenta

                    '    .IdMovCuenta = Nothing
                    '    .IdCuentaPersona = CInt(Me.hddIdCuentaPersona.Value)
                    '    .IdPersona = CInt(Me.txtCodigoCliente.Text)
                    '    .Monto = CDec(Me.txtMonto.Text)
                    '    .Factor = CInt(1) 'Por ser Cargo
                    '    .IdDocumento = Nothing
                    '    .IdDetalleRecibo = Nothing
                    '    .IdMovCuentaTipo = CInt(1) ' por ser del tipo Cargo
                    '    .Saldo = CDec(Me.txtMonto.Text)
                    '    .IdCargoCuenta = Nothing

                    'End With

            End Select

        Catch ex As Exception
            Throw ex
        End Try

        Return objMovCuenta
    End Function


    Private Function obtenerMovCaja(ByVal IdCondicionPago As Integer) As Entidades.MovCaja
        Dim objMovCaja As Entidades.MovCaja = Nothing

        Try

            Select Case IdCondicionPago

                Case 1
                    '********************* Contado
                    objMovCaja = New Entidades.MovCaja

                    With objMovCaja

                        .IdDocumento = Nothing
                        .IdCaja = CInt(cboCaja.SelectedValue)
                        .IdTienda = CInt(Me.CboTienda.SelectedValue)
                        .IdEmpresa = CInt(Me.cboPropietario.SelectedValue)
                        .IdPersona = CInt(Session("IdUsuario"))
                        .IdTipoMovimiento = 1   '************* Ingreso

                    End With
                Case 2
                    '********************* Cr�dito
                    '******** Devolvemos NULO
            End Select

        Catch ex As Exception
            Throw ex
        End Try

        Return objMovCaja
    End Function

    Private Function obtenerListaPagoCaja(ByVal IdCondicionPago As Integer) As List(Of Entidades.PagoCaja)

        Try

            Select Case IdCondicionPago

                Case 1
                    '*************** Contado

                    Me.listaCancelacion = getListaCancelacion()
                    Me.listaVuelto = getListaVuelto()

                    If Me.listaVuelto.Count = 0 Then

                        '**************** No se ha dado vuelto en diferentes monedas

                        For i As Integer = 0 To Me.listaCancelacion.Count - 1

                            If Me.listaCancelacion(i).IdMedioPago = 1 And Me.listaCancelacion(i).IdMoneda = CInt(Me.cboMoneda.SelectedValue) Then

                                '********** Si es efectivo y el IdMoneda es el mismo que el IdMonedaEmision
                                Me.listaCancelacion(i).Vuelto = CDec(txtMonto_VueltoTotal.Text)

                                Return Me.listaCancelacion

                            End If

                        Next

                        '**************** De no encontrarse, creo un nuevo objeto

                        Dim objPagoCaja As New Entidades.PagoCaja

                        With objPagoCaja

                            .Factor = 1
                            .IdMedioPago = 1
                            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                            .IdTipoMovimiento = 1
                            .Vuelto = CDec(txtMonto_VueltoTotal.Text)

                        End With

                        Me.listaCancelacion.Add(objPagoCaja)

                    Else

                        '******************** Se ha dado vuelto en otra y/o varias monedas

                        For i As Integer = 0 To Me.listaVuelto.Count - 1

                            Dim addPagoCaja As Boolean = True

                            For j As Integer = 0 To Me.listaCancelacion.Count - 1

                                If (Me.listaVuelto(i).IdMedioPago = Me.listaCancelacion(j).IdMedioPago) And (Me.listaVuelto(i).IdMoneda = Me.listaCancelacion(j).IdMoneda) Then

                                    Me.listaCancelacion(j).Vuelto = Me.listaVuelto(i).Efectivo
                                    addPagoCaja = False
                                    Exit For

                                End If

                            Next

                            If addPagoCaja Then

                                '******************* Si no existe, agrego un nuevo PagoCaja
                                Dim objPagoCaja As New Entidades.PagoCaja

                                With objPagoCaja

                                    .Factor = Me.listaVuelto(i).Factor
                                    .IdMedioPago = Me.listaVuelto(i).IdMedioPago
                                    .IdMoneda = Me.listaVuelto(i).IdMoneda
                                    .IdTipoMovimiento = Me.listaVuelto(i).IdTipoMovimiento
                                    .Vuelto = Me.listaVuelto(i).Efectivo

                                End With

                                Me.listaCancelacion.Add(objPagoCaja)

                            End If


                        Next

                    End If

                Case 2
                    '************** Cr�dito               
                    '**************** Devolvemos NULO
            End Select


        Catch ex As Exception
            Throw ex
        End Try

        Return Me.listaCancelacion

    End Function

    Private Sub mostrarVistaPrevia()


        Agregar(True)

    End Sub

    Private Sub Agregar(Optional ByVal vistaPrevia As Boolean = False)

        Try

            ActualizarGrillaDetalle()

            '************************ Obtengo el Documento - Cabecera
            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            '************************* Obtengo el Detalle
            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = obtenerListaDetalleDoc()
            '******************* Punto de Llegada
            Dim objPuntoLlegada As Entidades.PuntoLlegada = obtenerPuntoLlegada()

            '********************** Obtengo los datos de cancelaci�n
            Dim objMovCaja As Entidades.MovCaja = obtenerMovCaja(CInt(Me.cboCondicionPago.SelectedValue))
            Dim objMovCuenta As Entidades.MovCuenta = obtenerMovCuenta(CInt(Me.cboCondicionPago.SelectedValue))


            Me.listaCancelacion = getListaCancelacion()
            Me.listaVuelto = getListaVuelto()




            '********************** Si no se ha ingresado datos de cancelacion lo generamos
            If (Me.listaCancelacion.Count = 0) And (objMovCaja IsNot Nothing) And (Not vistaPrevia) Then

                Dim objCancelacion As New Entidades.PagoCaja
                With objCancelacion

                    .Efectivo = CDec(txtTotalAPagar.Text)

                    '****************** Si es AGENTE RETENEDOR
                    If objDocumento.IdTipoAgente = 2 Then .Efectivo = .Efectivo * (1 - (objDocumento.TasaAgente / 100))

                    .Factor = 1  '*********** Es un ingreso
                    .IdMedioPago = 1 '*********** efectivo
                    .IdMoneda = objDocumento.IdMoneda
                    .IdTipoMovimiento = objMovCaja.IdTipoMovimiento

                End With
                Me.listaCancelacion.Add(objCancelacion)




            End If



            '************* Validando agente RETENEDOR
            Dim separarAfectos_NoAfectos As Boolean = chb_SepararAfecto_NoAfecto.Checked
            If (CInt(cboTipoAgente.SelectedValue) = 2) Then separarAfectos_NoAfectos = False

            Dim saltar_correlativo As Boolean = chb_SaltarCorrelativo.Checked
            Dim separarxMontos As Boolean = chb_SepararxMontos.Checked
            Dim comprometerPercepcion As Boolean = Me.chb_CompPercepcion.Checked
            Dim generarDeshacho As Boolean = Me.chb_GenerarDespacho.Checked

            Select Case CInt(hddModoFrm.Value)
                Case 1 '**************** NUEVO
                    objDocumento.Id = Nothing
                Case 2 '**************** EDITAR
                    objDocumento.Id = CInt(hddIdDocumento.Value)
                    objDocumento.IdEstadoDoc = 1 '*********** ACTIVO
                    separarAfectos_NoAfectos = False
                    saltar_correlativo = False
                    separarxMontos = False
            End Select


            '******************* Inserto el documento
            Dim listaDocumento As List(Of Entidades.Documento) = Nothing
            Select Case CInt(Me.cboTipoDocumento.SelectedValue)
                Case 14  '********************* COTIZACION

                    listaDocumento = (New Negocio.Documento).registrarDocumentoVenta(objDocumento, Nothing, Nothing, listaDetalle, Nothing, Nothing, (New List(Of Entidades.PagoCaja)), (New List(Of Entidades.PagoCaja)), False, False, False, False, False, CInt(hddModoFrm.Value), vistaPrevia, False, False)

                Case Else

                    listaDocumento = (New Negocio.Documento).registrarDocumentoVenta(objDocumento, Nothing, objPuntoLlegada, listaDetalle, objMovCaja, objMovCuenta, Me.listaCancelacion, (New List(Of Entidades.PagoCaja)), chb_MoverAlmacen.Checked, chb_ComprometerStock.Checked, separarAfectos_NoAfectos, saltar_correlativo, separarxMontos, CInt(hddModoFrm.Value), vistaPrevia, comprometerPercepcion, generarDeshacho)

            End Select

            '******************** Obtengo la cabecera de cada documento guardado            

            If vistaPrevia Then

                DGV_VistaPreviaDoc.DataSource = listaDocumento
                DGV_VistaPreviaDoc.DataBind()

                lbl_Percepcion_VistaPrevia.Text = Me.cboMoneda.SelectedItem.ToString
                lbl_TotalAPagar_VistaPrevia.Text = Me.cboMoneda.SelectedItem.ToString

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "mostrarCapaVistaPrevia();", True)

            Else

                Dim listaDocumentoView As New List(Of Entidades.Documento)

                For i As Integer = 0 To listaDocumento.Count - 1

                    If listaDocumento(i).IdEstadoDoc = 1 Then

                        listaDocumentoView.Add((New Negocio.Documento).DocumentoCabSelectxId(listaDocumento(i).Id))

                    End If

                Next

                DGV_ImpresionDoc_Cab.DataSource = listaDocumentoView
                DGV_ImpresionDoc_Cab.DataBind()

                verFrm("5", False, False, False, False, False, False, 0, 0)

                lblPercepcion_DocSAVED.Text = Me.cboMoneda.SelectedItem.ToString
                lblTotalAPagar_DocSAVED.Text = Me.cboMoneda.SelectedItem.ToString

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaImpresionDocumento_SAVED();", True)
            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Sub DGV_ImpresionDoc_Cab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_ImpresionDoc_Cab.SelectedIndexChanged
        Try

            DGV_ImpresionDoc_Det.DataSource = (New Negocio.DetalleDocumento).DetalleDocumentoViewSelectxIdDocumento(CInt(DGV_ImpresionDoc_Cab.SelectedRow.Cells(1).Text))
            DGV_ImpresionDoc_Det.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")

        End Try
    End Sub

    Private Function obtenerMovCuenta(ByVal IdCondicionPago As Integer) As Entidades.MovCuenta

        Dim objMovCuenta As Entidades.MovCuenta = Nothing

        Select Case IdCondicionPago

            Case 1
                '************ Contado
                Return objMovCuenta

            Case 2
                '***********
                objMovCuenta = New Entidades.MovCuenta
                With objMovCuenta

                    .IdPersona = CInt(txtCodigoCliente.Text)
                    .Factor = 1 '************* Cargo
                    .IdCuentaPersona = CInt(hddIdCuentaPersona.Value)
                    .IdMovCuentaTipo = 1 '*********** Cargo

                End With

        End Select

        Return objMovCuenta

    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        mostrarVistaPrevia()
    End Sub

    Private Sub btnBuscarClientexIdCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarClientexIdCliente.Click
        addPersona(CInt(txtIdCliente_BuscarxId.Text))
    End Sub

    Private Sub cargarDatosPersonaGrilla(ByVal grilla As GridView, ByVal texto As String, ByVal opcion As Integer, ByVal tipoMov As Integer)
        Try
            '********** opcion 0: descripcion
            '********** opcion 1: ruc
            '********** opcion 2: dni
            Dim index As Integer = 0
            Select Case tipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) + 1
                Case 3 '***************** IR
                    index = (CInt(txtPageIndexGO_Persona.Text) - 1)
            End Select

            Dim listaPV As List(Of Entidades.PersonaView) = (New Negocio.PersonaView).SelectActivoxParam_Paginado(texto, opcion, grilla.PageSize, index)

            If listaPV.Count <= 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                grilla.DataSource = listaPV
                grilla.DataBind()
                txtPageIndex_Persona.Text = CStr(index + 1)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarPersona_Grilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersona_Grilla.Click
        ViewState.Add("TextoBuscarPersona", txtBuscarPersona_Grilla.Text)
        ViewState.Add("OpcionBuscarPersona", Me.cmbFiltro_BuscarPersona.SelectedValue)
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, CInt(Me.cmbFiltro_BuscarPersona.SelectedValue), 0)
        'txtPageIndex_Persona.Text = "1"
        'txtPageIndexGo_Persona.Text = "1"
    End Sub
    Private Sub btnAnterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 1)
    End Sub
    Private Sub btnPosterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 2)
    End Sub
    Private Sub btnIr_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 3)
    End Sub




#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub cmbAlmacen_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAlmacen_AddProd.SelectedIndexChanged

        BuscarProductoCatalogo(1)

    End Sub

    Private Sub cmbTienda_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTienda_AddProd.SelectedIndexChanged
        BuscarProductoCatalogo(2)
    End Sub
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub
    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try
            Dim codigo As Integer = 0
            Dim IdTipoPV As Integer = 1
            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                        If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                            IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                        End If
                    End If
                    If txtCodigoSubLinea_AddProd.Text.Trim.Length > 0 Then codigo = CInt(txtCodigoSubLinea_AddProd.Text)

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cmbAlmacen_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cmbAlmacen_AddProd.SelectedValue)
                    ViewState.Add("IdTienda_BuscarProducto", Me.cmbTienda_AddProd.SelectedValue)
                    ViewState.Add("IdMoneda_BuscarProducto", Me.cboMoneda.SelectedValue)
                    ViewState.Add("IdTipoPV_BuscarProducto", IdTipoPV)

                    If txtCodigoSubLinea_AddProd.Text.Trim.Length > 0 Then
                        ViewState.Add("CodigoSubLinea_BuscarProducto", txtCodigoSubLinea_AddProd.Text)
                    Else
                        ViewState.Add("CodigoSubLinea_BuscarProducto", 0)
                    End If

                Case 1
                    '******************* Cuando se cambia el almacen de busqueda
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cmbAlmacen_AddProd.SelectedValue)

                Case 2
                    '******************** cuando se cambia la tienda de precios
                    ViewState.Add("IdTienda_BuscarProducto", Me.cmbTienda_AddProd.SelectedValue)

            End Select

            If (opcion = 1 Or opcion = 2) And (DGV_AddProd.Rows.Count = 0) Then
                Return
            End If

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As Integer, ByVal nomProducto As String, ByVal IdAlmacen As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMoneda As Integer, ByVal tipoMov As Integer, ByVal IdEmpresa As Integer)
        Try
            Dim index As Integer = 0
            Select Case tipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
                Case 3 '**************** IR
                    index = (CInt(txtPageIndexGO_Productos.Text) - 1)
            End Select

            Me.listaCatalogoSelect = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index)

            If Me.listaCatalogoSelect.Count = 0 Then

                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")

            Else

                setListaCatalogo(Me.listaCatalogoSelect)

                grilla.DataSource = Me.listaCatalogoSelect
                grilla.DataBind()
                txtPageIndex_Productos.Text = CStr(index + 1)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 1, CInt(ViewState("IdEmpresa_BuscarProducto")))
    End Sub
    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 2, CInt(ViewState("IdEmpresa_BuscarProducto")))
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 3, CInt(ViewState("IdEmpresa_BuscarProducto")))
    End Sub
#End Region





    Private Sub btnAceptarBuscarProdxID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptarBuscarProdxID.Click

        obtenerProductoxId()

    End Sub

    Private Sub obtenerProductoxId()

        Try

            ActualizarGrillaDetalle()
            Me.listaDetalleDocumento = getListaDetalle()

            Dim IdTipoPv As Integer = 1  '********* Publico

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPv = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            '*************** Obtenemos el Catalogo del producto

            Dim objcatalogo As Entidades.Catalogo = (New Negocio.Catalogo).CatalogoProductoSelectxParams(CInt(txtBuscarProdxId.Text), _
                                                                        CInt(Me.cmbTienda_AddProd.SelectedValue), IdTipoPv, CInt(Me.cboMoneda.SelectedValue), CInt(Me.cmbAlmacen_AddProd.SelectedValue), True)
            If objcatalogo IsNot Nothing Then

                If objcatalogo.StockDisponible <= 0 Then

                    objScript.mostrarMsjAlerta(Me, "El Producto no posee Stock Disponible.")
                    Return

                End If


                If Me.listaDetalleDocumento.Count <= 0 Then

                    hddIdAlmacen.Value = cmbAlmacen_AddProd.SelectedValue
                    hddNomAlmacen.Value = cmbAlmacen_AddProd.SelectedItem.ToString

                End If


                '****************** Lo agregamos al detalle

                Dim objDD As New Entidades.DetalleDocumentoView

                With objDD

                    .IdProducto = objcatalogo.IdProducto
                    .Cantidad = 0
                    .StockDisponible = objcatalogo.StockDisponible

                    .ListaUMedida = objcatalogo.ListaUM_Venta

                    .Descripcion = objcatalogo.Descripcion
                    .PrecioSD = objcatalogo.PrecioSD
                    .Descuento = 0
                    .PDescuento = 0
                    .PrecioCD = objcatalogo.PrecioSD
                    .Importe = 0
                    .Percepcion = objcatalogo.Percepcion
                    .Detraccion = objcatalogo.Detraccion

                    .IdUMedida = objcatalogo.IdUnidadMedida

                    .IdTienda_PV = objcatalogo.IdTienda

                    .ListaUMedida = objcatalogo.ListaUM_Venta

                End With

                Me.listaDetalleDocumento.Add(objDD)

                setListaDetalle(Me.listaDetalleDocumento)

                gvDetalle.DataSource = Me.listaDetalleDocumento
                gvDetalle.DataBind()

            Else


                objScript.mostrarMsjAlerta(Me, "El Id ingresado no existe y/o no posee un precio de venta asignado.")

            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#Region "Buscar Documento"
    Protected Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocumento.Click
        buscarDocumento()
    End Sub

    Private Sub buscarDocumento()
        Try

            '*********** obtengo el IdDocumento
            Dim NegDocumento As New Negocio.Documento

            '*****Para obtener la Cabecera
            Dim Documento As New Entidades.Documento
            Documento = NegDocumento.SelectxIdSeriexCodigo(CInt(Me.cboSerie.SelectedValue), CInt(txtCodigoDocumento.Text))

            Me.hddPoseeAmortizaciones.Value = CStr(IIf(Documento.PoseeAmortizaciones = True, 1, 0))
            Me.hddPoseeOrdenDespacho.Value = CStr(IIf(Documento.PoseeOrdenDespacho = True, 1, 0))
            Me.hddPoseeCompPercepcion.Value = CStr(IIf(Documento.PoseeCompPercepcion = True, 1, 0))

            '********************** Verifico si el estado es del Correlativo SALTEADO
            If Documento.IdEstadoDoc = 3 Then

                With Documento


                    verFrm("4", False, True, True, True, False, False, 0, 0)

                    '************** Cargamos el Documento
                    Me.hddIdDocumento.Value = Documento.Id.ToString
                    Me.cboEstadoDocumento.SelectedValue = .IdEstadoDoc.ToString
                    Me.txtCodigoDocumento.Text = .Codigo

                End With

                Return

            End If

            '******************** CargarCliente
            Me.BuscarClientexId(Documento.IdPersona)

            With Documento
                Me.hddIdDocumento.Value = CStr(.Id)
                Me.txtFechaEmision.Text = CStr(.FechaEmision)
                Me.txtFechaAEntregar.Text = CStr(IIf(.FechaAEntregar = Nothing, "", .FechaAEntregar))
                Me.txtFechaVencimiento.Text = CStr(IIf(.FechaVenc = Nothing, "", .FechaVenc))
                Me.txtDescuento.Text = CStr(Decimal.Round(.Descuento, 2))
                Me.txtSubTotal.Text = CStr(Decimal.Round(.SubTotal, 2))
                Me.txtIGV.Text = CStr(Decimal.Round(.IGV, 2))
                Me.txtTotal.Text = CStr(Decimal.Round(.Total, 2))
                Me.txtTotalAPagar.Text = CStr(Decimal.Round(.TotalAPagar, 2))
                Me.txtMonto.Text = CStr(Math.Round(.TotalAPagar, 2))
                Me.txtValorReferencial.Text = CStr(Decimal.Round(.ValorReferencial, 2))
                Me.cboEstadoDocumento.SelectedValue = CStr(.IdEstadoDoc)



                Me.cboCondicionPago.SelectedValue = CStr(.IdCondicionPago)
                actualizarControlesCondicionPago(.IdCondicionPago)

                '********** Si es al contado mostramos los controles x defecto (EFECTIVO)
                If (.IdCondicionPago = 1) Then actualizarControlesMedioPago(1)



                Me.cboMoneda.SelectedValue = CStr(.IdMoneda)
                Me.cboEstadoCancelacion.SelectedValue = CStr(.IdEstadoCancelacion)
                Me.cboEstadoEntrega.SelectedValue = CStr(.IdEstadoEntrega)


                '********************* Pintamos los totales para la impresi�n
                Me.txtTotalAPagar_DocSAVED.Text = CStr(Math.Round(.TotalAPagar, 2))
                Me.txtPercepcion_DocSAVED.Text = CStr(Math.Round(.Percepcion, 2))


                hddIdAlmacen.Value = Documento.IdAlmacen.ToString
                hddNomAlmacen.Value = Documento.NomAlmacen


            End With
            '***************** Cargar Percepcion,Detraccion Retencion
            Dim NegMontoRegimen As New Negocio.MontoRegimen

            Me.txtPercepcion.Text = CStr(NegMontoRegimen.SelectPercepcionxIdDocumento(Documento.Id))
            Me.txtDetraccion.Text = CStr(NegMontoRegimen.SelectDetraccionxIdDocumento(Documento.Id))
            Me.txtRetencion.Text = CStr(NegMontoRegimen.SelectRetencionxIdDocumento(Documento.Id))



            '******************** obtengo el detalle
            Dim NegDetalleDocumento As New Negocio.DetalleDocumento
            Dim lista As New List(Of Entidades.DetalleDocumentoView)
            lista = NegDetalleDocumento.DetalleDocumentoViewSelectxIdDocumento(CInt(Me.hddIdDocumento.Value))

            '******************** agrego la lista obtenida al detalle
            Dim listaDetalle As New List(Of Entidades.DetalleDocumentoView)
            '********************** listaDetalle = Me.getListaDetalle

            Dim NegProductoUMView As New Negocio.ProductoUMView

            For i As Integer = 0 To lista.Count - 1
                lista(i).ListaUMedida = (New Negocio.ProductoTipoPV).SelectUM_Venta(lista(i).IdProducto, Documento.IdTienda, CInt(cboTipoPrecioV.SelectedValue))
                listaDetalle.Add(lista(i))
            Next
            Me.setListaDetalle(listaDetalle)

            '********************** lo agrego a la grilla
            gvDetalle.DataSource = listaDetalle
            gvDetalle.DataBind()

            '****************** Cargar Datos de Cancelaci�n

            Dim listaPagoCaja As List(Of Entidades.PagoCaja) = (New Negocio.PagoCaja).SelectListaxIdDocumento(Documento.Id)

            Me.listaCancelacion = obtenerListaCancelacion(listaPagoCaja)
            '***********  Me.listaVuelto = obtenerListaVuelto(listaPagoCaja)

            setListaCancelacion(Me.listaCancelacion)
            '************ setListaVuelto(Me.listaVuelto)

            Me.DGV_Cancelacion.DataSource = Me.listaCancelacion
            Me.DGV_Cancelacion.DataBind()

            '************* Me.DGV_Vuelto.DataSource = Me.listaVuelto
            '*********** Me.DGV_Vuelto.DataBind()

















            '***************************** Cargamos el punto de llegada
            Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(Documento.Id)

            If objPuntoLlegada IsNot Nothing Then
                Dim objCbo As New Combo

                txtDireccionEntrega.Text = objPuntoLlegada.pll_Direccion
                cboDepartamento.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(0, 2)

                objCbo.LLenarCboProvincia(Me.cboProvincia, objPuntoLlegada.pll_Ubigeo.Substring(0, 2))
                Me.cboProvincia.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(2, 2)

                objCbo.LLenarCboDistrito(Me.cboDistrito, objPuntoLlegada.pll_Ubigeo.Substring(0, 2), objPuntoLlegada.pll_Ubigeo.Substring(2, 2))
                Me.cboDistrito.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(4, 2)
            End If

            '************** Colocamos la Lista para la impresion
            Dim listaDocumento As New List(Of Entidades.Documento)
            listaDocumento.Add(Documento)
            DGV_ImpresionDoc_Cab.DataSource = listaDocumento
            DGV_ImpresionDoc_Cab.DataBind()

            verFrm("4", False, True, False, False, False, False, 0, 0)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontosCancelacion();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

    Private Function obtenerListaCancelacion(ByVal listaPagoCaja As List(Of Entidades.PagoCaja)) As List(Of Entidades.PagoCaja)

        Dim lista As New List(Of Entidades.PagoCaja)

        For i As Integer = 0 To listaPagoCaja.Count - 1

            If listaPagoCaja(i).Efectivo > 0 Then

                Dim obj As New Entidades.PagoCaja

                With obj

                    .Efectivo = listaPagoCaja(i).Efectivo
                    .IdMedioPago = listaPagoCaja(i).IdMedioPago
                    .IdMoneda = listaPagoCaja(i).IdMoneda
                    .IdTipoMovimiento = listaPagoCaja(i).IdTipoMovimiento
                    .Factor = listaPagoCaja(i).Factor
                    .NomMedioPago = listaPagoCaja(i).NomMedioPago
                    .NomMoneda = listaPagoCaja(i).NomMoneda
                    .NomMonedaDestino = listaPagoCaja(i).NomMonedaDestino
                    .IdMonedaDestino = listaPagoCaja(i).IdMonedaDestino
                    .MontoEquivalenteDestino = listaPagoCaja(i).MontoEquivalenteDestino
                    .EfectivoInicial = listaPagoCaja(i).EfectivoInicial

                    .NomBanco = listaPagoCaja(i).NomBanco
                    .NumeroCheque = listaPagoCaja(i).NumeroCheque
                    .NumeroOp = listaPagoCaja(i).NumeroOp
                    .FechaACobrar = listaPagoCaja(i).FechaACobrar

                End With

                lista.Add(obj)

            End If

        Next

        Return lista

    End Function

    Private Function obtenerListaVuelto(ByVal listaPagoCaja As List(Of Entidades.PagoCaja)) As List(Of Entidades.PagoCaja)

        Dim lista As New List(Of Entidades.PagoCaja)

        For i As Integer = 0 To listaPagoCaja.Count - 1

            If listaPagoCaja(i).Vuelto > 0 Then

                Dim obj As New Entidades.PagoCaja
                With obj

                    .Efectivo = listaPagoCaja(i).Vuelto
                    .IdMedioPago = listaPagoCaja(i).IdMedioPago
                    .IdMoneda = listaPagoCaja(i).IdMoneda
                    .IdTipoMovimiento = listaPagoCaja(i).IdTipoMovimiento
                    .Factor = listaPagoCaja(i).Factor
                    .NomMedioPago = listaPagoCaja(i).NomMedioPago
                    .NomMoneda = listaPagoCaja(i).NomMoneda
                    .NomMonedaDestino = listaPagoCaja(i).NomMonedaDestino
                    .IdMonedaDestino = listaPagoCaja(i).IdMonedaDestino
                    .MontoEquivalenteDestino = listaPagoCaja(i).MontoEquivalenteVuelto

                    lista.Add(obj)

                End With


            End If

        Next

        Return lista

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento
        Dim objDocumento As New Entidades.Documento
        With objDocumento

            .IdAlmacen = CInt(Me.hddIdAlmacen.Value)
            .IdTipoPV = CInt(Me.cboTipoPrecioV.SelectedValue)
            If (.IdTipoPV = Nothing) Then
                .IdTipoPV = 1 '******** P�blico x defecto
            End If
            .NomMoneda = Me.cboMoneda.SelectedItem.ToString
            .Codigo = txtCodigoDocumento.Text
            .Serie = Me.cboSerie.SelectedItem.ToString
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .IdArea = 1
            .IdAlmacen = CInt(hddIdAlmacen.Value)
            .FactorMov = -1
            .FechaEmision = CDate(Me.txtFechaEmision.Text)


            If (CInt(Me.cboCondicionPago.SelectedValue) = 2) Then  '******** CREDITO
                Try
                    .FechaVenc = CDate(Me.txtFechaVencimiento.Text)
                Catch ex As Exception
                    .FechaVenc = Nothing
                End Try
            End If

            .Vuelto = CDec(txtMonto_VueltoTotal.Text)
            .IdPersona = CInt(Me.txtCodigoCliente.Text)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdDestinatario = CInt(Me.txtCodigoCliente.Text)
            .IdEstadoDoc = CInt(Me.cboEstadoDocumento.SelectedValue)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdTipoOperacion = 1 '***************** Venta
            .IdTienda = CInt(Me.CboTienda.SelectedValue)

            .NroVoucherConta = "0"
            .IdEmpresa = CInt(Me.cboPropietario.SelectedValue)
            .IdTipoDocumento = CInt(Me.cboTipoDocumento.SelectedValue)
            .IdCondicionPago = CInt(Me.cboCondicionPago.SelectedValue)
            .IdCaja = CInt(Me.cboCaja.SelectedValue)


            '************** Estado de Cancelaci�n
            If CInt(Me.cboCondicionPago.SelectedValue) = 1 Then 'Si es Al contado
                .IdEstadoCancelacion = 2 ' Cancelado
            ElseIf CInt(Me.cboCondicionPago.SelectedValue) = 2 Then 'Si es al Cr�dito
                .IdEstadoCancelacion = 1 'Por Pagar
            End If

            '********** Datos de Entrega
            Try
                .FechaAEntregar = CDate(Me.txtFechaAEntregar.Text)
            Catch ex As Exception
                .FechaAEntregar = Nothing
            End Try


            .IdEstadoEntrega = CInt(Me.cboEstadoEntrega.SelectedValue)

            .IdTipoAgente = CInt(Me.cboTipoAgente.SelectedValue)
            .TasaAgente = CDec(txtTasaAgente.Text)

        End With

        Return objDocumento

    End Function
    Private Function obtenerListaDetalleDoc() As List(Of Entidades.DetalleDocumento)

        Me.listaDetalleDocumento = getListaDetalle()

        Dim lista As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            Dim objDetalleDocumento As New Entidades.DetalleDocumento

            With objDetalleDocumento


                .Descuento = Me.listaDetalleDocumento(i).Descuento
                .IdProducto = Me.listaDetalleDocumento(i).IdProducto
                .Cantidad = Me.listaDetalleDocumento(i).Cantidad

                .CantxAtender = Me.listaDetalleDocumento(i).Cantidad
                If CInt(Me.cboTipoDocumento.SelectedValue) = 14 Then .CantxAtender = 0 '****** COTIZACION

                .UMedida = Me.listaDetalleDocumento(i).UM
                .IdUnidadMedida = Me.listaDetalleDocumento(i).IdUMedida
                .PrecioSD = Me.listaDetalleDocumento(i).PrecioSD
                .Descuento = Me.listaDetalleDocumento(i).Descuento
                .PrecioCD = Me.listaDetalleDocumento(i).PrecioCD
                .Importe = Me.listaDetalleDocumento(i).Importe
                .IdDocumento = 0
                .Utilidad = 0
                .TasaDetraccion = Me.listaDetalleDocumento(i).Detraccion
                .TasaPercepcion = Me.listaDetalleDocumento(i).Percepcion
                .DetalleGlosa = Me.listaDetalleDocumento(i).Glosa

            End With

            lista.Add(objDetalleDocumento)

        Next

        Return lista
    End Function
    Private Function obtenerPuntoLlegada() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing

        If txtDireccionEntrega.Text.Trim.Length > 0 Then
            objPuntoLlegada = New Entidades.PuntoLlegada
            With objPuntoLlegada

                .pll_Direccion = CStr(IIf(Me.txtDireccionEntrega.Text = "", Nothing, Me.txtDireccionEntrega.Text))
                .pll_Ubigeo = Me.cboDepartamento.SelectedValue + Me.cboProvincia.SelectedValue + Me.cboDistrito.SelectedValue

            End With
        End If
        Return objPuntoLlegada
    End Function




    Protected Sub btnCanjearCotizac�n_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCanjearCotizacion.Click

        ViewState.Clear()

        Me.LimpiarControlesCanjearCotizacion()

        '****************** Llenamos el monto maximo
        llenarMontosMaximos()

        GenerarCodigo()

        '************** MODO FRM
        hddModoFrm.Value = "1"
        HabilitarWrite()

        actualizarControlesCondicionPago(1) '********** Contado
        actualizarControlesMedioPago(1) '******** Efectivo

    End Sub
    Private Sub LimpiarControlesCanjearCotizacion()

        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        hddIdDocumento.Value = ""
        Me.txtCodigoDocumento.Text = ""
        Me.cboEstadoDocumento.SelectedIndex = 0

        Me.cboEstadoCancelacion.SelectedIndex = 0

        Me.cboCondicionPago.SelectedIndex = 0
        actualizarControlesCondicionPago(CInt(Me.cboCondicionPago.SelectedValue))

        Me.cboMedioPago.SelectedIndex = 0
        actualizarControlesMedioPago(CInt(Me.cboMedioPago.SelectedValue))

        Me.txtFechaVencimiento.Text = ""
        Me.txtFechaVencimiento.Visible = False


        lblMonedaCancelacion_TotalAPagar.Text = Me.cboMoneda.SelectedItem.ToString
        lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString

        Me.cboBanco.SelectedIndex = 0
        Me.txtNCheque.Text = ""
        Me.cboNCuenta.SelectedIndex = 0
        Me.txtNOperacion.Text = ""
        Me.hddIdCuentaPersona.Value = ""

        Me.cboBanco.Visible = False
        Me.txtNCheque.Visible = False
        Me.txtFechaACobrar.Visible = False
        Me.cboNCuenta.Visible = False
        Me.txtNOperacion.Visible = False

        Me.cboDepartamento.SelectedIndex = 0
        Me.txtDireccionEntrega.Text = ""

        Me.txtFechaAEntregar.Text = ""
        Me.txtFechaACobrar.Text = ""

        Me.DGV_Cancelacion.DataSource = Nothing
        Me.DGV_Cancelacion.DataBind()

        Me.DGV_Vuelto.DataSource = Nothing
        Me.DGV_Vuelto.DataBind()


        Me.DGV_ImpresionDoc_Cab.DataSource = Nothing
        Me.DGV_ImpresionDoc_Cab.DataBind()
        Me.DGV_ImpresionDoc_Det.DataSource = Nothing
        Me.DGV_ImpresionDoc_Det.DataBind()

        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.txtCodigoSubLinea_AddProd.Text = ""
        Me.txtDescripcionProd_AddProd.Text = ""
        Me.txtIdCliente_BuscarxId.Text = ""

    End Sub

    Private Sub verFrm(ByVal modoFrm As String, ByVal generarCodigoDoc As Boolean, ByVal llenarMontosMaxxTipoDoc As Boolean, ByVal limpiarSessionViewState As Boolean, ByVal initListasSession As Boolean, ByVal limpiarControles As Boolean, ByVal initControlsCancelacion As Boolean, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer)

        If (limpiarControles) Then
            Me.LimpiarControles()
        End If


        If (limpiarSessionViewState) Then
            Session.Remove("listaD")
            Session.Remove("listaCancelacion")
            Session.Remove("listaVuelto")
            Session.Remove("listaCatalogoSelect")
            ViewState.Clear()
        End If


        If (initListasSession) Then
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumentoView)
            setListaDetalle(Me.listaDetalleDocumento)
            Me.listaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.listaCancelacion)
            Me.listaVuelto = New List(Of Entidades.PagoCaja)
            setListaVuelto(Me.listaVuelto)
            Me.listaCatalogoSelect = New List(Of Entidades.Catalogo)
            setListaCatalogo(Me.listaCatalogoSelect)
        End If



        If (llenarMontosMaxxTipoDoc) Then
            llenarMontosMaximos()
        End If

        If (generarCodigoDoc) Then
            GenerarCodigo()
        End If

        If (initControlsCancelacion) Then
            actualizarControlesCondicionPago(IdCondicionPago)
            actualizarControlesMedioPago(IdMedioPago)
        End If

        '************** MODO FRM
        hddModoFrm.Value = modoFrm
        HabilitarWrite()

    End Sub
    Private Sub imprimirDocumentoVenta(ByVal IdDocumento As Integer)

        Try
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).FacturaSelectCab_Impresion(IdDocumento)
            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.Documento).FacturaSelectDet_Impresion(IdDocumento)

            Dim texto As String = ""
            Dim aux As String = ""
            Dim objUtil As New Util

            If (objDocumento IsNot Nothing) Then
                With objStringB

                    '*********************************************** INICIO TABLA Cabecera - Detalle
                    .Append("<table style=""width:5000px;"">")

                    '************** Fila / 1 Columna [2]  ****** T�tulo de Comprobante de Percepci�n
                    .Append("<tr class=""Label_Print"">")
                    .Append("<td colspan=""2""  style=""vertical-align:bottom;height:140px"">")
                    If (objDocumento.Percepcion > 0) Then texto = "COMPROBANTE DE PERCEPCI�N VENTA INTERNA"
                    .Append(objUtil.completeText("", 108, "&nbsp;") + texto)
                    .Append("</td>")
                    .Append("</tr>")

                    '************** Fila / 1 Columna [2] ****** Fecha de Emisi�n - Nro Documento
                    .Append("<tr class=""Label_Print"">")
                    .Append("<td colspan=""2"" style=""vertical-align:bottom;height:40px"">")
                    .Append(objUtil.completeText("", 21, "&nbsp;") + objUtil.completeText(CStr(Day(objDocumento.FechaEmision)), 17, "&nbsp;") + objUtil.completeText(mes(DatePart(DateInterval.Month, objDocumento.FechaEmision)), 43, "&nbsp;") + objUtil.completeText(CStr(Year(objDocumento.FechaEmision)), 55, "&nbsp;") + objDocumento.Serie + "-" + objDocumento.Codigo)
                    .Append("</td>")
                    .Append("</tr>")

                    '************** Fila / 1 Columna [2]  ****** Cliente
                    .Append("<tr class=""Label_Print"">")
                    .Append("<td colspan=""2"" style=""vertical-align:bottom;height:21px"">")
                    .Append(objUtil.completeText("", 20, "&nbsp;") + objDocumento.DescripcionPersona)
                    .Append("</td>")
                    .Append("</tr>")

                    '************** Fila / 1 Columna [2]  ****** Direcci�n Cliente
                    .Append("<tr class=""Label_Print"">")
                    .Append("<td colspan=""2"" style=""vertical-align:bottom;height:21px"">")
                    .Append(objUtil.completeText("", 20, "&nbsp;") + objDocumento.Direccion)
                    .Append("</td>")
                    .Append("</tr>")

                    '************** Fila / 1 Columna  ****** RUC - Condici�n Pago
                    .Append("<tr class=""Label_Print"">")
                    '************** Columna DocumentoI
                    .Append("<td  style=""vertical-align:bottom;width:150px;height:38px"">")
                    .Append(objUtil.completeText("", 20, "&nbsp;") + objDocumento.DocumentoI)
                    .Append("</td>")
                    '************** Columna Condicion Pago
                    .Append("<td  style=""vertical-align:bottom;height:36px"">")
                    .Append(objDocumento.NomCondicionPago)
                    .Append("</td>")
                    .Append("</tr>")

                    '************** Fila / 1 Columna  CABECERA Detalle
                    .Append("<tr class=""Label_Print"">")
                    .Append("<td colspan=""2"" style=""vertical-align:bottom;height:22px"">")
                    .Append(objUtil.completeText("", 10, "&nbsp;"))
                    .Append("</td>")
                    .Append("</tr>")
                    .Append("</tr>")
                    .Append("</table>")

                    '************************** Saltamos una L�nea para la tabla del detalle
                    .Append("<table>")
                    For i As Integer = 0 To listaDetalle.Count - 1

                        '********* Fila
                        .Append("<tr class=""Label_Print_Detalle"">")

                        '******** Columna 1 : Cantidad
                        .Append("<td style=""height:15px;width:60px;vertical-align:top;text-align:right"">")
                        .Append(objUtil.Redondear(listaDetalle(i).Cantidad, 2))
                        .Append("</td>")

                        '******** Columna 2 : UMedida
                        .Append("<td style=""height:15px;width:35px;vertical-align:top;text-align:left"">")
                        .Append(objUtil.completeText("", 0, "&nbsp;") + listaDetalle(i).UMedida)
                        .Append("</td>")

                        '******** Columna 3 : Producto
                        .Append("<td style=""height:15px;width:510px;vertical-align:top;text-align:left"">")
                        .Append(listaDetalle(i).NomProducto)
                        .Append("</td>")

                        '******** Columna 4 : Precio CD
                        .Append("<td style=""height:15px;width:70px;vertical-align:top;text-align:right"">")
                        .Append(objUtil.Redondear(listaDetalle(i).PrecioCD, 2))
                        .Append("</td>")

                        '******** Columna 5 : Importe
                        .Append("<td style=""height:15px;width:67px;vertical-align:top;text-align:right"">")
                        .Append(objUtil.Redondear(listaDetalle(i).Importe, 2))
                        .Append("</td>")

                        .Append("</tr>")  '*** Fin Fila


                        If (listaDetalle(i).DetalleGlosa.Trim.Length > 0) Then  '******** Existe una Glosa

                            '********* Fila
                            .Append("<tr class=""Label_Print_DetalleGlosa"">")

                            '******** Columna 1
                            .Append("<td style=""height:5px;width:55px;vertical-align:top;text-align:right"">")
                            .Append("</td>")

                            '******** Columna 2 
                            .Append("<td style=""height:5px;width:35px;vertical-align:top;text-align:left"">")
                            .Append("</td>")

                            '******** Columna 3 [3] : GLOSA
                            .Append("<td colspan=""3"" style=""height:5px;vertical-align:top;text-align:left"">")
                            .Append(listaDetalle(i).DetalleGlosa)
                            .Append("</td>")

                            .Append("</tr>")  '*** Fin Fila                        
                        End If

                    Next
                    .Append("</table>") '******** Fin Tabla Cabecera - Detalle










                    '****************** CAPA TABLA Letras - Percepci�n
                    .Append(" <div id=""TABLA_Letras_Percepcion"" style=""z-index:100;position:absolute; top: 575px; left:16px; width: 700px; height: 65px;"">  ")

                    .Append("<table style=""z-index:20;position:absolute"">")

                    '**************** Fila 1  /  A Letras
                    .Append("<tr class=""Label_Print""   style=""height:15px""  >")

                    '***** Columna 1
                    .Append("<td  style=""width:120px;vertical-align:bottom"" >")
                    .Append("</td>")

                    '***** Columna 2  //  A Letras
                    .Append("<td colspan=""2"" style=""vertical-align:bottom;text-align:left"" >")
                    .Append("Son: " + objDocumento.TotalLetras.ToUpper)
                    .Append("</td>")

                    .Append("</tr>")

                    If (objDocumento.Percepcion > 0) Then  '******* Existe PERCEPCION

                        '**************** Fila 2  /  Title Percepci�n
                        .Append("<tr class=""Label_Print""   style=""height:15px""  >")

                        '***** Columna 1
                        .Append("<td  style=""width:100px;vertical-align:bottom"" >")
                        .Append("</td>")

                        '***** Columna 2  //  Title
                        .Append("<td colspan=""2"" style=""vertical-align:bottom;text-align:left"" >")
                        .Append("OPERACI�N SUJETA A PERCEPCI�N DEL I.G.V. ( " + CStr(objUtil.Redondear(objDocumento.PorcentPercepcion, 2)) + " % )")
                        .Append("</td>")

                        .Append("</tr>")

                        '**************** Fila 3  /  Importe de Percepci�n
                        .Append("<tr class=""Label_Print""   style=""height:15px""  >")

                        '***** Columna 1
                        .Append("<td  style=""width:100px;vertical-align:bottom"" >")
                        .Append("</td>")

                        '***** Columna 2
                        .Append("<td  style=""width:140px;vertical-align:bottom"" >")
                        .Append("Importe de Percepci�n:")
                        .Append("</td>")

                        '***** Columna 3 
                        .Append("<td style=""vertical-align:bottom;text-align:left"" >")
                        .Append(objDocumento.NomMoneda + objUtil.completeText("", 3, "&nbsp;") + CStr(objUtil.Redondear(objDocumento.Percepcion, 2)))
                        .Append("</td>")

                        .Append("</tr>")


                        '**************** Fila 4  /  Total a Pagar
                        .Append("<tr class=""Label_Print""   style=""height:15px""  >")

                        '***** Columna 1
                        .Append("<td  style=""width:100px;vertical-align:bottom"" >")
                        .Append("</td>")

                        '***** Columna 2
                        .Append("<td  style=""width:140px;vertical-align:bottom"" >")
                        .Append("Total a Pagar:")
                        .Append("</td>")

                        '***** Columna 3 
                        .Append("<td style=""vertical-align:bottom;text-align:left"" >")
                        .Append(objDocumento.NomMoneda + objUtil.completeText("", 3, "&nbsp;") + CStr(objUtil.Redondear(objDocumento.TotalAPagar, 2)))
                        .Append("</td>")

                        .Append("</tr>")



                    End If

                    .Append("</table>") '******** Fin Tabla Letras - Percepci�n
                    .Append("  </div> ")
















                    If (objDocumento.IdCondicionPago = 1) Then '********************* TABLA FECHA DE CANCELACI�N - CONTADO

                        '****************** CAPA condicion pago
                        .Append(" <div id=""TABLA_FECHA_CONTADO"" style=""z-index:200;position:absolute; top: 711px; left:355px; width: 350px; height: 25px;"">  ")

                        '******************** TABLA FECHA CONTADO
                        .Append("<table style=""z-index:40;position:absolute"">")

                        '**************** Fila 1  /  Sub Total
                        .Append("<tr class=""Label_Print"" style=""height:20px""  >")
                        '***** Columna 1 // DIA
                        .Append("<td  style=""width:25px;vertical-align:bottom;text-align:right"" >")
                        .Append(Day(objDocumento.FechaEmision))
                        .Append("</td>")
                        '***** Columna 2 // MES
                        .Append("<td  style=""width:195px;vertical-align:bottom;text-align:left"" >")
                        .Append(objUtil.completeText("", 6, "&nbsp;") + mes(Month(objDocumento.FechaEmision)))
                        .Append("</td>")
                        '***** Columna 3 // ANIO
                        .Append("<td  style=""width:60px;vertical-align:bottom;text-align:left"" >")
                        .Append(Year(objDocumento.FechaEmision))
                        .Append("</td>")
                        .Append("</tr>")
                        .Append("</table>")

                        .Append("  </div> ")
                    End If









                    '****************** CAPA TABLA TOTALES
                    .Append(" <div id=""TABLA_TOTALES"" style=""z-index:150;position:absolute; top: 686px; left:580px; width: 175px; height: 85px;"">  ")

                    '******************** TABLA TOTALES
                    .Append("<table style=""position:absolute"">")

                    '**************** Fila 1  /  Sub Total
                    .Append("<tr class=""Label_Print"" style=""height:25px""  >")
                    '***** Columna 1
                    .Append("<td  style=""width:100px;vertical-align:bottom;text-align:right"" >")
                    .Append("</td>")
                    '***** Columna 2
                    .Append("<td  style=""width:75px;vertical-align:bottom;text-align:right"" >")
                    .Append(objUtil.Redondear(objDocumento.SubTotal, 2))
                    .Append("</td>")
                    .Append("</tr>")

                    '**************** Fila 2  /  Sub Total
                    .Append("<tr class=""Label_Print"" style=""height:25px""  >")
                    '***** Columna 1
                    .Append("<td  style=""vertical-align:bottom;text-align:right"" >")
                    .Append(CInt(objDocumento.PorcentIGV))
                    .Append("</td>")
                    '***** Columna 2
                    .Append("<td  style=""vertical-align:bottom;text-align:right"" >")
                    .Append(objUtil.Redondear(objDocumento.IGV, 2))
                    .Append("</td>")
                    .Append("</tr>")

                    '**************** Fila 3  /  TOTAL
                    .Append("<tr class=""Label_Print"" style=""height:30px""  >")
                    '***** Columna 1
                    .Append("<td  style=""vertical-align:bottom;text-align:right"" >")
                    .Append(objDocumento.NomMoneda)
                    .Append("</td>")
                    '***** Columna 2
                    .Append("<td  style=""vertical-align:bottom;text-align:right"" >")
                    .Append(objUtil.Redondear(objDocumento.Total, 0))
                    .Append("</td>")
                    .Append("</tr>")

                    .Append("</table>") '******** Fin TOTALES
                    .Append("  </div> ")




                End With


            Else
                Throw New Exception("No se encontr� el Documento.")
            End If

            '************ Mandamos a Impresi�n
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "imprimirCapa();", True)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click

        printDocumento()

    End Sub
    Private Sub printDocumento()
        Try


            imprimirDocumentoVenta(CInt(Me.hddIdDocumentoPrint.Value))
            'imprimirDocumentoVenta(28352) '**+
            'imprimirDocumentoVenta(28349) '**+ siempre



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload


        '*********** Limpiamos los controles de Impresi�n
        objStringB = New StringBuilder
        hddIdDocumentoPrint.Value = ""


    End Sub

    Private Sub btnImprimir_JS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimir_JS.Click
        printDocumento()
    End Sub

    Private Sub cboTipoPrecioV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoPrecioV.SelectedIndexChanged
        '****************** Actualizamos los precios del detale del documento
        Actualizarlistapreciosdetalle()
        ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)
    End Sub

    Private Sub btnGuardar_EditarCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar_EditarCliente.Click
        editarCliente()
    End Sub
    Private Sub editarCliente()

        Try

            Dim objPersonaView As Entidades.PersonaView = obtenerPersonaView()

            If ((New Negocio.Cliente).PersonaUpdate_Ventas(objPersonaView)) Then
                addPersona(objPersonaView.IdPersona, False)
            Else
                Throw New Exception("Problemas en la Edici�n del Cliente.")
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
    Private Function obtenerPersonaView() As Entidades.PersonaView

        Dim objPersonaView As New Entidades.PersonaView
        With objPersonaView

            .IdPersona = CInt(Me.txtCodigoCliente.Text.Trim)
            .RazonSocial = Me.txtRazonSocial_EditarCliente.Text.Trim
            .ApPaterno = Me.txtApPaterno_EditarCliente.Text.Trim
            .ApMaterno = Me.txtApMaterno_EditarCliente.Text.Trim
            .Nombres = Me.txtNombres_EditarCliente.Text.Trim
            .Ruc = Me.txtRUC_EditarCliente.Text.Trim
            .Dni = Me.txtDNI_EditarCliente.Text.Trim
            .IdTipoAgente = CInt(Me.cboTipoAgente_EditarCliente.SelectedValue)

            .TipoPersona = CInt(Me.cboTipoPersona.SelectedValue)
            '***** 0: --------
            '***** 1: Natural
            '***** 2: Jur�dica

        End With
        Return objPersonaView
    End Function

    Private Sub btnNuevo_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm("1", True, True, False, True, True, True, 1, 1)
    End Sub

    Private Sub btnAddGlosa_CapaGlosa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddGlosa_CapaGlosa.Click
        Producto_AddGlosa()
    End Sub
    Private Sub Producto_AddGlosa()

        Try

            '**************** Actualizo mi lista detalle
            ActualizarGrillaDetalle()

            Dim index As Integer = CInt(hddIndexDetalle_Area.Value)

            Me.listaDetalleDocumento = getListaDetalle()

            Me.listaDetalleDocumento(index).Glosa = Me.txtGlosa_CapaGlosa.Text

            Me.setListaDetalle(Me.listaDetalleDocumento)

            gvDetalle.DataSource = Me.listaDetalleDocumento
            gvDetalle.DataBind()

            ScriptManager.RegisterStartupScript(Page, Page.GetType, "onLoad", "calcularImporteONLOAD('-1');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub



    Private Sub mostrarDocumentosNotaCredito()
        Try

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoSelectCabFind_Aplicacion(CInt(Me.txtCodigoCliente.Text), 0, 0)

            If (lista.Count > 0) Then

                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
                objScript.onCapa(Me, "capaDocumentosReferencia")

            Else

                Throw New Exception("No se hallaron registros.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    
    Private Sub btnViewCapaNotaCredito_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewCapaNotaCredito.Click
        mostrarDocumentosNotaCredito()
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged
        agregarDatoCancelacion_NC()
    End Sub
    Private Sub agregarDatoCancelacion_NC()
        Try

            '************** VALIDAMOS SALDO
            Dim saldo As Decimal = CDec(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblSaldo_Find"), Label).Text)
            Dim monto As Decimal = CDec(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("txtMontoRecibir_Find"), TextBox).Text)
            Dim moneda As String = CStr(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblMonedaMontoRecibir_Find"), Label).Text)
            Dim IdDocumento As Integer = CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value)
            Dim IdMonedaNC As Integer = CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value)
            Dim nroDocumento As String = CStr(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text)

            If (monto > saldo) Then
                Throw New Exception("El Monto Ingresado excede al saldo disponible [ " + moneda + " " + CStr(Math.Round(saldo, 2)) + " ].")
            Else
                '**************** Validamos el dato en Grilla de cancelaci�n
                Me.listaCancelacion = getListaCancelacion()
                For i As Integer = 0 To Me.listaCancelacion.Count - 1
                    If (CInt(Me.listaCancelacion(i).NumeroCheque) = IdDocumento) Then
                        Throw New Exception("La Nota de Cr�dito seleccionado ya se encuentra en [ Datos de Cancelaci�n ].")
                    End If
                Next

                '**************** Agregamos el dato de cancelaci�n
                Dim objPagoCaja As New Entidades.PagoCaja
                With objPagoCaja

                    .Efectivo = monto
                    .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                    .IdMoneda = CInt(IdMonedaNC)
                    .IdTipoMovimiento = 1   '******** Ingreso
                    .Factor = 1

                    .NomMedioPago = Me.cboMedioPago.SelectedItem.ToString
                    .NomMoneda = moneda

                    .NumeroCheque = CStr(IdDocumento)
                    .NumeroOp = "Nro. " + nroDocumento

                    .NomMonedaDestino = Me.cboMoneda.SelectedItem.ToString
                    .IdMonedaDestino = CInt(cboMoneda.SelectedValue)
                    .MontoEquivalenteDestino = (New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(.IdMoneda, .IdMonedaDestino, .Efectivo, "E")

                    .EfectivoInicial = .Efectivo


                End With
                Me.listaCancelacion.Add(objPagoCaja)
                setListaCancelacion(Me.listaCancelacion)

                '************* Mostramos la grilla
                Me.DGV_Cancelacion.DataSource = Me.listaCancelacion
                Me.DGV_Cancelacion.DataBind()

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " offCapa('capaDocumentosReferencia'); calcularImporteONLOAD('-1');", True)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

    End Sub
End Class
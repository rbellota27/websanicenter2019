﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="Foshan.aspx.vb" Inherits="APPWEB.Foshan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Reporte de Comercialización -Foshan02
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td align="center">
                            <fieldset>
                                <legend><span class="Texto"></span></legend>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Empresa:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Tienda:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTienda" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend><span class="Texto"></span></legend>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Tipo Existencia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Linea:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Sub-Linea:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboSubLinea" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend><span class="Texto">Atributo de Producto</span></legend>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnOpen_Atributo" runat="server" Text="Buscar" ToolTip="Buscar Atributos" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                Width="650px">
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                    <asp:TemplateField HeaderText="Atributo">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomTabla")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTablaValor")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Valor">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="GrillaRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend>
                                    <asp:RadioButtonList ID="rbtFechaOSemana" runat="server" RepeatDirection="Horizontal"
                                        CssClass="Texto">
                                        <asp:ListItem Value="0" Selected="True">Rango de Fechas</asp:ListItem>
                                        <asp:ListItem Value="1">Semanas</asp:ListItem>
                                    </asp:RadioButtonList>
                                </legend>
                                <table>
                                    <tr>
                                        <td align="center">
                                            <fieldset>
                                                <legend><span class="Texto">Fecha</span></legend>
                                                <table>
                                                    <tr>
                                                        <td class="Texto">
                                                            Inicio:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaInicio1" runat="server" Width="70px" CssClass="TextBox_Fecha"
                                                                onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio1">
                                                            </cc1:MaskedEditExtender>
                                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                TargetControlID="txtFechaInicio1">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                        <td class="Texto">
                                                            Fin:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaFin1" runat="server" Width="70px" CssClass="TextBox_Fecha"
                                                                onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin1">
                                                            </cc1:MaskedEditExtender>
                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                TargetControlID="txtFechaFin1">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                        <td align="center">
                                            <fieldset>
                                                <legend><span class="Texto">Semana</span></legend>
                                                <table>
                                                    <tr>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblDe" runat="server" Text="DE:"></asp:Label>
                                                        </td>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblanoi" runat="server" Text="Año:"></asp:Label>
                                                            <asp:DropDownList ID="idYearI" runat="Server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblSemanai" runat="server" Text="Semana:"></asp:Label>
                                                            <asp:DropDownList ID="idSemanaI" runat="Server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaInicioSemanaI" runat="server" Width="70px" onBlur=" return ( valFecha_Blank(this) ); "
                                                                CssClass="TextBox_Fecha"></asp:TextBox>
                                                        </td>
                                                        <td class="Texto">
                                                            -
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaFinSemanaI" runat="server" Width="70px" onBlur=" return ( valFecha_Blank(this) ); "
                                                                CssClass="TextBox_Fecha"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblHasta" runat="server" Text="HASTA:"></asp:Label>
                                                        </td>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblanof" runat="server" Text="Año:"></asp:Label>
                                                            <asp:DropDownList ID="idYearF" runat="Server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblSemanaf" runat="server" Text="Semana:"></asp:Label>
                                                            <asp:DropDownList ID="idSemanaF" runat="Server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaInicioSemanaFin" runat="server" Width="70px" onBlur=" return ( valFecha_Blank(this) ); "
                                                                CssClass="TextBox_Fecha"></asp:TextBox>
                                                        </td>
                                                        <td class="Texto">
                                                            -
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaFinSemanafin" runat="server" Width="70px" onBlur=" return ( valFecha_Blank(this) ); "
                                                                CssClass="TextBox_Fecha"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend><span class="Texto">Consultar Saldos</span></legend>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Tipo Almacén:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoAlmacen" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAddTipoAlmacen" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                OnClientClick="return ( onclick_AddTipoAlmacen() );" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:GridView ID="GV_TipoAlmacen" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                    <asp:TemplateField HeaderText="Atributo">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblNomTipoAlmacen" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Tipoalm_Nombre")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoAlmacen")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="GrillaRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="imgAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    OnClientClick=" return ( Aceptar() ); " />
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="iReportFrame" name="iReportFrame" width="100%" height="2000px" scrolling="no">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnOpen_TablaValor" runat="server" Text="" Width="1px" Height="1px" />
                        </td>
                        <td>
                            <asp:HiddenField ID="hddIdProducto" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <cc1:ModalPopupExtender ID="ModalPopup_Atributo" runat="server" TargetControlID="btnOpen_Atributo"
                                PopupControlID="Panel_Atributo" BackgroundCssClass="modalBackground" Enabled="true"
                                RepositionMode="None" CancelControlID="btnClose_Atributo" Y="50">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel_Atributo" runat="server" CssClass="modalPopup" Style="display: none;"
                                Width="750px">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Image ID="btnClose_Atributo" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend><span class="Texto">Clases de Atributos</span></legend>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Descripción:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtBuscarTabla" runat="server" MaxLength="50" Width="250px" onKeyPress=" return ( BuscarTabla() ); "></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="ImgBuscarTabla" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                            ToolTip="Buscar Tabla" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:GridView ID="GV_Tabla" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                PageSize="20">
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <Columns>
                                                                    <asp:CommandField ShowSelectButton="True" />
                                                                    <asp:TemplateField HeaderText="Descripción">
                                                                        <ItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lblTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Abv" HeaderText="Abv." />
                                                                </Columns>
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="btnAnteriorTabla" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                            ToolTip="Página Anterior" OnClientClick="return(valNavegacionTabla('0'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnSiguienteTabla" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                            ToolTip="Página Posterior" OnClientClick="return(valNavegacionTabla('1'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPageIndexTabla" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnIrTabla" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                                                            ToolTip="Ir a la Página" OnClientClick="return(valNavegacionTabla('2'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPageIndexGoTabla" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                            onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ModalPopupExtender ID="ModalPopup_TablaValor" runat="server" TargetControlID="btnOpen_TablaValor"
                                                PopupControlID="Panel_TablaValor" BackgroundCssClass="modalBackground" Enabled="true"
                                                RepositionMode="None" CancelControlID="ImgClose_TablaValor" Y="75">
                                            </cc1:ModalPopupExtender>
                                            <asp:Panel ID="Panel_TablaValor" runat="server" CssClass="modalPopup" Style="display: none;"
                                                Width="700px">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Image ID="ImgClose_TablaValor" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend><span class="Texto">Registros de </span>
                                                                    <asp:Label ID="lblNomTabla" runat="server" Font-Bold="true" CssClass="Texto" Text="Tabla"></asp:Label></legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="Texto">
                                                                                        Descripción:
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtBuscarTablaValor" runat="server" MaxLength="50" Width="250px"
                                                                                            onKeyPress=" return ( BuscarTablaValor() ); "></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="ImgBuscarTablaValor" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                                            ToolTip="Buscar Tabla Valor" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="ImgAgregarTablaValor" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                                                            OnClientClick=" return ( AgregarTablaValor() ); " ToolTip="Agregar Valor" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:GridView ID="GV_TablaValor" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                                PageSize="20">
                                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="ckTablaValor" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Código">
                                                                                        <ItemTemplate>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblTipoTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Descripción">
                                                                                        <ItemTemplate>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="Abv" HeaderText="Abv." />
                                                                                </Columns>
                                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                                <PagerStyle CssClass="GrillaPager" />
                                                                                <RowStyle CssClass="GrillaRow" />
                                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnAnteriorTablaValor" runat="server" Font-Bold="true" Width="50px"
                                                                                            Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionTablaValor('0'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnSiguienteTablaValor" runat="server" Font-Bold="true" Width="50px"
                                                                                            Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionTablaValor('1'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtPageIndexTablaValor" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnIrTablaValor" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                                                                            ToolTip="Ir a la Página" OnClientClick="return(valNavegacionTablaValor('2'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtPageIndexGoTablaValor" Width="50px" CssClass="TextBox_ReadOnly"
                                                                                            runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function valNavegacionTabla(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndexTabla.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valNavegacionTablaValor(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndexTablaValor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function BuscarTablaValor() {
            if (event.keyCode == 13) {
                document.getElementById('<%=ImgBuscarTablaValor.ClientID %>').click();
                return false;
            }
            return true;
        }

        function BuscarTabla() {
            if (event.keyCode == 13) {
                document.getElementById('<%=ImgBuscarTabla.ClientID %>').click();
                return false;
            }
            return true;
        }

        function AgregarTablaValor() {

            var GV_TablaValor = document.getElementById('<%=GV_TablaValor.ClientID %>');
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');

            if (GV_TablaValor != null) {

                for (var j = 1; j < GV_TablaValor.rows.length; j++) {
                    var rowElem2 = GV_TablaValor.rows[j];

                    if (rowElem2.cells[0].children[0].cells[0].children[0].checked == true) {

                        if (grilla != null) {
                            for (var i = 1; i < grilla.rows.length; i++) {
                                var rowElem = grilla.rows[i];

                                if (rowElem2.cells[1].children[0].cells[0].children[0].value == rowElem.cells[1].children[0].cells[1].children[0].value && rowElem2.cells[2].children[0].cells[0].children[0].value == rowElem.cells[1].children[0].cells[2].children[0].value) {
                                    alert('LA LISTA DE ATRIBUTOS YA CONTIENE LA TABLA < ' + rowElem.cells[1].children[0].cells[0].children[0].innerText + ' > CON VALOR < ' + rowElem.cells[2].children[0].cells[0].children[0].innerText + ' >. \n NO SE PERMITE LA OPERACIÓN.');
                                    return false;
                                }

                            }
                        }

                    } //end id
                }
            }

            return true;
        }

        // ****************
        function onclick_AddTipoAlmacen() {
            var cboTipoAlmacen = document.getElementById('<%=cboTipoAlmacen.ClientID %>');
            var grilla = document.getElementById('<%=GV_TipoAlmacen.ClientID %>');

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    rowElem = grilla.rows[i];

                    if (rowElem.cells[1].children[0].cells[1].children[0].value == cboTipoAlmacen.value) {
                        alert('LA LISTA TIPO ALMACÉN YA CONTIENE EL ELEMENTO < ' + rowElem.cells[1].children[0].cells[0].children[0].innerText + '. \n NO SE PERMITE LA OPERACIÓN.');
                        return false;
                    }
                }
            }

            return true;
        }


        function Aceptar() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
            var IdEmpresa = parseInt(cboEmpresa.value);

            var cboTienda = document.getElementById('<%=cboTienda.ClientID %>');
            var IdTienda = parseInt(cboTienda.value);

            var IdTipoExistencia = parseInt(document.getElementById('<%=cboTipoExistencia.ClientID %>').value);
            var IdLinea = parseInt(document.getElementById('<%=cboLinea.ClientID %>').value);
            var IdSubLinea = parseInt(document.getElementById('<%=cboSubLinea.ClientID %>').value);
            var IdProducto = parseInt(document.getElementById('<%=hddIdProducto.ClientID %>').value);


            var YearIni = document.getElementById('<%= idYearI.ClientID%>').value;
            if (document.getElementById('<%= idYearI.ClientID%>') == null) {
                YearIni = 0
            }
            var SemanaIni = document.getElementById('<%= idSemanaI.ClientID%>').value;
            if (document.getElementById('<%= idSemanaI.ClientID%>') == null) {
                SemanaIni = 0
            }

            var YearFin = document.getElementById('<%= idYearF.ClientID%>').value;
            if (document.getElementById('<%= idYearF.ClientID%>') == null) {
                YearFin = 0
            }
            var SemanaFin = document.getElementById('<%=idSemanaF.ClientID%>').value;
            if (document.getElementById('<%= idSemanaF.ClientID%>') == null) {
                SemanaFin = 0
            }

            var FechaIni = document.getElementById('<%=txtFechaInicio1.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaInicio1.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var FechaFin = document.getElementById('<%=txtFechaFin1.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaFin1.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            
            var rbtFechaOSemana = document.getElementById('<%=rbtFechaOSemana.ClientID %>');
            var ctrlFechaOSemana = rbtFechaOSemana.getElementsByTagName('input');
            var FiltrarSemana = 0;

            for (var i = 0; i < ctrlFechaOSemana.length; i++) {
                if (ctrlFechaOSemana[i].checked == true) {
                    FiltrarSemana = parseInt(ctrlFechaOSemana[i].value);                    
                }
            }
            
            iReportFrame.location.href = 'VisorGrilla.aspx?iReport=1&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + 
                                         '&IdTipoExistencia=' + IdTipoExistencia + '&IdLinea=' + IdLinea + '&IdSubLinea=' + IdSubLinea +
                                         '&FechaIni=' + FechaIni + '&FechaFin=' + FechaFin + '&YearIni=' + YearIni + '&SemanaIni=' + SemanaIni + '&YearFin=' + YearFin + '&SemanaFin=' + SemanaFin +
                                         '&FiltrarSemana=' + FiltrarSemana;

            return false;
        }
    
    </script>

</asp:Content>

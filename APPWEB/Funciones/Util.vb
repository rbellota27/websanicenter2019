﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports Microsoft.Office.Interop
Imports ADODB

Public Class Util

    Public Function decodificarTexto(ByVal texto As String) As String

        Return HttpUtility.HtmlDecode(texto)

    End Function

    Public Function encodeText(ByVal texto As String) As String

        Return HttpUtility.HtmlEncode(texto)

    End Function

    '******** Completa una cadena  de tamanio definido con un caracter o una cadena
    Public Function completeText(ByVal text As String, ByVal size As Integer, ByVal complete As String, Optional ByVal opcion As String = "R") As String
        Dim n As Integer = text.Length

        For i As Integer = 0 To (size - n - 1)

            Select Case opcion

                Case "L"  '*** Left

                    text = complete + text

                Case "R"  '*** Rigth
                    text = text + complete

            End Select


        Next

        Return text

    End Function

    Public Sub Mensaje(ByVal msj As String, ByVal pagina As Page)
        pagina.Response.Write("<script>alert('" + msj + "');</script>")
    End Sub
    Public Sub Salir(ByVal pagina As Page)
        pagina.Response.Write("<script>window.opener=top;window.close();</script>")
    End Sub
    Public Function Redondear(ByVal numero As Decimal, ByVal numDecimales As Integer) As Decimal
        Dim rpta As Decimal = Math.Round(numero, numDecimales)
        Return rpta
    End Function
    Sub ArregloJavaScript(ByVal pagina As Page, ByVal arreglo As String, ByVal ds As DataSet)
        pagina.Response.Write("<script language='javascript' type='text/javascript'>" + Chr(13))
        pagina.Response.Write(Trim(arreglo) + "=new Array();" + Chr(13))

        Dim dr As DataRow
        Dim indice As Integer = 0
        Dim i As Integer = 0

        For Each dr In ds.Tables(0).Rows
            pagina.Response.Write(Trim(arreglo) + "[" + indice.ToString() + "]=new Array();" + Chr(13))
            For i = 0 To ds.Tables(0).Columns.Count - 1
                pagina.Response.Write(Trim(arreglo) + "[" + indice.ToString() + "][" + i.ToString() + "]='" + dr(i).ToString().Replace("'", "") + "';" + Chr(13))
            Next
            indice += 1
        Next
        pagina.Response.Write("</script>" + Chr(13))
    End Sub
    'Sirve para obtener un datatable filtrado, segun parametros; de un datatable
    'por ejemplo traigo todas las personas, y luego quiero filtralas por edad, y ordenarlas por apellido
    Function SelectDataTable(ByVal dt As DataTable, ByVal filter As String, ByVal sort As String) As DataTable
        'Sirve para hacer filtros y ordenaciones a una tabla
        Dim rows As DataRow()
        Dim dtNew As DataTable
        ' copy table structure  
        dtNew = dt.Clone()
        ' sort and filter data  
        rows = dt.Select(filter, sort)
        ' fill dtNew with selected rows
        For Each dr As DataRow In rows
            dtNew.ImportRow(dr)
        Next
        ' return filtered dt  
        Return dtNew
    End Function
    Function ConvertToRecordset(ByVal inTable As DataTable) As ADODB.Recordset
        Dim result As New ADODB.Recordset()
        result.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        Dim resultFields As ADODB.Fields = result.Fields
        Dim inColumns As System.Data.DataColumnCollection = inTable.Columns
        Dim var As String = ""
        Try
            For Each inColumn As DataColumn In inColumns
                resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, CType(IIf(inColumn.AllowDBNull, ADODB.FieldAttributeEnum.adFldIsNullable, ADODB.FieldAttributeEnum.adFldUnspecified), FieldAttributeEnum), Nothing)
            Next

            result.Open(System.Reflection.Missing.Value, System.Reflection.Missing.Value, ADODB.CursorTypeEnum.adOpenStatic, _
                     ADODB.LockTypeEnum.adLockOptimistic, 0)

            For Each dr As DataRow In inTable.Rows
                result.AddNew(System.Reflection.Missing.Value, System.Reflection.Missing.Value)
                For columnIndex As Integer = 0 To inColumns.Count - 1
                    resultFields(columnIndex).Value = dr(columnIndex)
                    var = resultFields(columnIndex).Value.ToString
                Next
            Next

        Catch xErr As Exception
            'Dim result As ADODB.Recordset() = Nothing
            result = Nothing
            ' MsgBox(xErr.Message, MsgBoxStyle.Critical)
            ' Return result
        End Try
        Return result
    End Function

    Function TranslateType(ByVal columnType As Type) As ADODB.DataTypeEnum
        Select Case columnType.UnderlyingSystemType.ToString()
            Case "System.Boolean"
                Return ADODB.DataTypeEnum.adBoolean
            Case "System.Byte"
                Return ADODB.DataTypeEnum.adUnsignedTinyInt
            Case "System.Char"
                Return ADODB.DataTypeEnum.adChar
            Case "System.DateTime"
                Return ADODB.DataTypeEnum.adDate
            Case "System.TimeSpan"
                Return ADODB.DataTypeEnum.adDate
            Case "System.Decimal"
                Return ADODB.DataTypeEnum.adDouble

                'original
                'Case "System.Decimal"
                '    Return ADODB.DataTypeEnum.adCurrency

                'Case "System.Decimal"
                '    Return ADODB.DataTypeEnum.adVarChar
                'Case "System.Decimal"
                '    Return ADODB.DataTypeEnum.adDecimal

            Case "System.Double"
                Return ADODB.DataTypeEnum.adDouble
            Case "System.Int16"
                Return ADODB.DataTypeEnum.adSmallInt
            Case "System.Int32"
                Return ADODB.DataTypeEnum.adInteger
            Case "System.Int64"
                Return ADODB.DataTypeEnum.adBigInt
            Case "System.SByte"
                Return ADODB.DataTypeEnum.adTinyInt
            Case "System.Single"
                Return ADODB.DataTypeEnum.adSingle
            Case "System.UInt16"
                Return ADODB.DataTypeEnum.adUnsignedSmallInt
            Case "System.UInt32"
                Return ADODB.DataTypeEnum.adUnsignedInt
            Case "System.UInt64"
                Return ADODB.DataTypeEnum.adUnsignedBigInt
            Case "System.String"
                Return ADODB.DataTypeEnum.adVarChar
            Case "System.String"

            Case Else
                Return ADODB.DataTypeEnum.adVarChar
        End Select
    End Function


    Public Sub getOExcel(ByVal rcset As ADODB.Recordset, ByVal titulo As String, ByVal ruta As String)
        Dim oExcel As Excel.ApplicationClass
        Dim oBook As Excel.WorkbookClass
        Dim oBooks As Excel.Workbooks

        'Dim sDir As String = "\APPWEB\bin\Plantilla\VtasProductoTiendaRpt"

        Dim sCurDir As String = ""

        Try
            oExcel = CType(CreateObject("excel.application"), Excel.ApplicationClass)
            oExcel.Visible = True
            oBooks = oExcel.Workbooks
            sCurDir = ruta 'CurDir()
            'sCurDir = sCurDir + ruta

            oBook = CType(oBooks.Open(sCurDir), Excel.WorkbookClass)

            oExcel.Visible = True
            oExcel.DisplayAlerts = False

            oExcel.Run("reporte", rcset, titulo)

        Catch xErr As Exception
            'MsgBox(xErr.Message, MsgBoxStyle.Critical)
            'Throw xErr.Message
        End Try

    End Sub
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
Imports CrystalDecisions.CrystalReports.Engine

Partial Public Class FrmVisorCXP
    Inherits System.Web.UI.Page
    Dim reporte As New reportdocument
    Private errorStr As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        valOnLoad_Frm()

    End Sub
    Private Sub valOnLoad_Frm()
        Try

            If Not Me.IsPostBack Then
                Dim iReporte As String = Request.QueryString("iReporte")

                If iReporte Is Nothing Then
                    iReporte = ""
                End If

                ViewState.Add("iReporte", iReporte)

                Select Case iReporte
                    Case "1" '********** REPORTE CXP
                        ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                        ViewState.Add("IdPersona", Request.QueryString("IdPersona"))
                        ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                        ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                        ViewState.Add("OpcionDeudas", Request.QueryString("OpcionDeudas"))
                        ViewState.Add("Empresa", Request.QueryString("Empresa"))

                    Case Else
                        Throw New Exception("NO SE HALLARON REPORTES.")

                End Select
            End If

            mostrarReporte()

        Catch ex As Exception
            errorStr = ex.Message.Replace(vbCrLf, "").Replace("<", "").Replace(">", "")
            Response.Write("  <script>  alert('" + errorStr + "') </script> ")
        End Try
    End Sub
    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString
            Case "1"
                ReporteMovCuentaCXP()
        End Select
    End Sub
    Private Sub ReporteMovCuentaCXP()

        Dim fechaInicio As Date = Nothing
        Dim fechaFin As Date = Nothing
        If (IsDate(ViewState("FechaInicio")) And CStr(ViewState("FechaInicio")).Trim.Length > 0) Then
            fechaInicio = CDate(ViewState("FechaInicio"))
        End If
        If (IsDate(ViewState("FechaFin")) And CStr(ViewState("FechaFin")).Trim.Length > 0) Then
            fechaFin = CDate(ViewState("FechaFin"))
        End If

        reporte = New CR_MovCuentaCXP_V1
        reporte.SetDataSource((New Negocio.MovCuentaPorPagar).CR_MovCuenta_CXP_DT(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdPersona")), fechaInicio, fechaFin, CInt(ViewState("OpcionDeudas"))))
        reporte.SetParameterValue("@FechaInicio", ViewState("FechaInicio"))
        reporte.SetParameterValue("@FechaFin", ViewState("FechaFin"))
        reporte.SetParameterValue("@Empresa", ViewState("Empresa"))

        Me.CRV_Report.ReportSource = reporte
        Me.CRV_Report.DataBind()

    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class
﻿Imports general.librerias.serializador
Imports Entidades
Imports Negocio
Public Class frmCancelarProvisiones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Call ListaObjetosLoad()
        End If
    End Sub

    Public Sub ListaObjetosLoad()
        Dim fechaActual As Date = Date.Now
        'Dim fechaInicio As Date = New Date(fechaActual.Year, fechaActual.Month, 1)
        Dim fechaInicio As Date = Date.Now
        'Dim fechaFin As Date = New Date(fechaActual.Year, fechaActual.Month + 1, 1).AddDays(-1)
        Dim fechaFin As Date = Date.Now
        Me.txtDesde.Text = fechaInicio
        Me.txtHasta.Text = fechaFin

        Dim cadenaProveedores As String = ""

        Dim IdMovCtaPP As Integer
        Dim objeto As New be_frmCargaPrincipal
        objeto = (New bl_pagoProveedor).form_listarPagosProgramados(1, Me.txtDesde.Text)

        ''Lista Proveedores
        'Dim lista As New List(Of be_ComboGenerico)
        'lista = objeto.listaProveedores()
        'If lista IsNot Nothing And lista.Count > 0 Then
        '    cadenaProveedores = (New objeto).SerializarLista(lista, "|", ";", False, "", False)
        '    Session("cadenaProveedores") = cadenaProveedores
        'End If
        'Lista las cancelaciones
        Me.gv_programacionCancelacion.DataSource = objeto.listaCancelacionesProgramadas()
        Me.gv_programacionCancelacion.DataBind()
        'Lista Bancos
        llenarCombo(objeto.listaBancos, ddlBanco, True)
    End Sub

    Private Sub llenarCombo(Of T)(ByVal lista As List(Of T), ByVal combo As DropDownList, Optional addnewItem As Boolean = False)
        combo.DataSource = lista
        combo.DataBind()
        If addnewItem Then
            Dim listItem As New ListItem("FILTRAR POR BANCOS", "-1")
            listItem.Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "blue")
            listItem.Attributes.CssStyle.Add(HtmlTextWriterStyle.FontWeight, "bold")
            combo.Items.Add(listItem)
            combo.SelectedValue = "-1"
        End If
    End Sub

    Private Sub ListaSegunFiltro(ByVal flag As Integer)
        Dim lista As List(Of be_cancelacionDocumentos)
        lista = (New bl_pagoProveedor).filtrarListaProgramaciones(Me.txtDesde.Text, Me.txtHasta.Text, Me.ddlBanco.SelectedValue, Val(Me.txtSerie.Text), Val(Me.txtCodigo.Text), flag)
        Me.gv_programacionCancelacion.DataSource = lista
        Me.gv_programacionCancelacion.DataBind()
    End Sub

    Protected Sub btnBuscarProgramacionesxPagar_Click(sender As Object, e As System.EventArgs)
        Dim btn As Button = DirectCast(sender, Button)
        Dim cmdName As String = btn.CommandName.ToUpper()
        Select Case cmdName
            Case "BTNXPAGAR"
                Call ListaSegunFiltro(1)
            Case "BTNPAGADOS"
                Call ListaSegunFiltro(2)
        End Select
    End Sub

    Private Sub gv_programacionCancelacion_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gv_programacionCancelacion.RowCommand
        Dim nombreComando As String = e.CommandName.ToUpper()
        If nombreComando = "DETRACCION" Then
            Dim index As Integer = CInt(e.CommandArgument)
            Dim idproveedor As Integer = DirectCast(Me.gv_programacionCancelacion.Rows(index).FindControl("hddidProveedor"), HiddenField).Value            
            Dim idTienda As Integer = DirectCast(Me.gv_programacionCancelacion.Rows(index).FindControl("hddidTienda"), HiddenField).Value
            Dim idRequerimientoCadenas As String = DirectCast(Me.gv_programacionCancelacion.Rows(index).FindControl("hddidRequerimientosCadena"), HiddenField).Value
            Me.hddidproveedor.Value = idproveedor
            Me.hddidtienda.Value = idTienda
            Me.hddidRequerimiento.Value = idRequerimientoCadenas
            'Me.ddlCuentaDetracciones.DataSource = (New bl_pagoProveedor).llenarComboCuentaBancariaDetraccionPorProveedor(idproveedor)
            'Me.ddlCuentaDetracciones.DataBind()
            Me.lbSiguiente.Visible = True

            Dim objeto As New Negocio.bl_pagoProveedor
            Dim lista As List(Of Entidades.Documento) = objeto.BuscaDocumentoDetraccion(Me.hddidRequerimiento.Value)
            Me.gvDocRefDetracciones.DataSource = lista
            Me.gvDocRefDetracciones.DataBind()
            If lista.Count > 0 Then
                Me.lbSiguiente.Visible = False
                Me.lbCancelar.Visible = True
            Else
                Me.lbSiguiente.Visible = True
                Me.lbCancelar.Visible = False
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "onload", "mostrarCapaDetraccion();", True)
        End If
    End Sub

    Private Sub gv_programacionCancelacion_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_programacionCancelacion.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim button As Button = DirectCast(e.Row.FindControl("btnGenerarDetraccion"), Button)
            button.Attributes.Add("onclick", "setearDatosDetraccion(this);")
        End If
    End Sub

    Private Sub lbSiguiente_Click(sender As Object, e As System.EventArgs) Handles lbSiguiente.Click

        Dim objetoDocumento As New Entidades.Documento
        With objetoDocumento
            .NroDocumento = Me.txtNroConstancia.Text.Trim()
            .FechaEmision = Me.txtFechaPago.Text
            .Total = Me.txtmontoDeposito.Text
            .IdUsuario = Session("idUsuario")
            .IdPersona = Me.hddidproveedor.Value
            .IdTienda = Me.hddidtienda.Value
            '.IdCuentaBancaria = Me.ddlCuentaDetracciones.SelectedValue
            '.NroCuentaBancaria = Me.ddlCuentaDetracciones.SelectedItem.Text
            .strIdRequerimientoCadena = Me.hddidRequerimiento.Value
        End With
        Session("documentoDetraccion") = objetoDocumento
        Dim objeto As New bl_pagoProveedor()
        Dim lista As List(Of Entidades.Documento) = objeto.CrearDocumentoExternoDetraccion(Me.hddidRequerimiento.Value, True, objetoDocumento)
        Me.gvDocRefDetracciones.DataSource = lista
        Me.gvDocRefDetracciones.DataBind()
        If lista.Count > 0 Then
            Me.lbSiguiente.Visible = False
            Me.lbCancelar.Visible = True
        Else
            Me.lbSiguiente.Visible = True
            Me.lbCancelar.Visible = False
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "onload", "mostrarCapaDetraccion();alert('Se creó detracción.');", True)
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "onload", "return(validarDetracción(this));", True)
    End Sub

    Private Sub gvDocRefDetracciones_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDocRefDetracciones.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lb As LinkButton = DirectCast(e.Row.FindControl("lbEliminar"), LinkButton)
            lb.Attributes.Add("onclick", "return confirm('Se eliminará el documento');")
        End If
    End Sub

    Protected Sub lblEliminar_click(sender As Object, e As EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim hddidDocumento As HiddenField = DirectCast(Me.gvDocRefDetracciones.Rows(0).FindControl("hddidDocumento"), HiddenField)
        Dim objeto As New bl_pagoProveedor
        Try
            objeto.DeleteRegistroDetraccion(hddidDocumento.Value)
            Me.gvDocRefDetracciones.DataSource = Nothing
            Me.gvDocRefDetracciones.DataBind()
            If Me.gvDocRefDetracciones.Rows.Count = 0 Then
                Me.lbSiguiente.Visible = True
                Me.lbCancelar.Visible = False
            Else
                Me.lbSiguiente.Visible = False
                Me.lbCancelar.Visible = True
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "onload", "mostrarCapaDetraccion();", True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lbCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCancelar.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "onload", "abrirVentanaDetraccion();", True)
    End Sub
End Class
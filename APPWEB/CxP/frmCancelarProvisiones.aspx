﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmCancelarProvisiones.aspx.vb" Inherits="APPWEB.frmCancelarProvisiones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="max-width:100%;width:100%">
    <div class="TituloCelda" style="margin-top:3px"><span>CANCELAR PAGOS PROGRAMADOS</span></div>
    <div class="SubTituloCelda"><span>Panel de filtros</span></div>
    <div style="width:100%">
        <table>
            <tr>
                <td class="Texto"><span>Desde:</span></td>
                <td><asp:TextBox runat="server" ID="txtDesde" Text="" Width="80px" />
                    <cc1:MaskedEditExtender ID="txtDesde_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtDesde">
                                </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDesde" Format="dd/MM/yyyy" >
                    </cc1:CalendarExtender>
                </td>
                <td class="Texto"><span>Hasta:</span></td>
                <td><asp:TextBox runat="server" ID="txtHasta" Text="" Width="80px" />
                    <cc1:MaskedEditExtender ID="txtHasta_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtHasta">
                                </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtHasta" Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                </td>
                <td>&nbsp&nbsp</td>
                <td class="Texto"><span>Requerimiento</span></td>
                <td><asp:TextBox ID="txtSerie" runat="server" Text="" Width="30px"></asp:TextBox></td>
                <td><asp:TextBox ID="txtCodigo" runat="server" Text="" Width="50px"></asp:TextBox></td>
                <td>&nbsp&nbsp</td>
                <td class="Texto"><span>Banco</span></td>
                <td><asp:DropDownList ID="ddlBanco" runat="server" DataValueField="campo1" DataTextField="campo2"></asp:DropDownList></td>
                <td>&nbsp&nbsp</td>
                <td><asp:Button ID="btnBuscarProgramacionesxPagar" runat="server" Text="Buscar Doc. Por Cancelar" CssClass="btnBuscar" OnClientClick="this.value='Procesando...';disabled=true" UseSubmitBehavior="false" OnClick="btnBuscarProgramacionesxPagar_Click" CommandName="btnxPagar" /></td>
                <td><asp:Button ID="btnBuscarProgramacionesCanceladas" runat="server" Text="Buscar Doc. Cancelados" CssClass="btnBuscar" OnClientClick="this.value='Procesando...';disabled=true" UseSubmitBehavior="false" OnClick="btnBuscarProgramacionesxPagar_Click" CommandName="btnPagados" /></td>
            </tr>
        </table>
    </div>
        <div style="max-width:100%;width:100%;overflow:hidden">
            <asp:GridView Width="100%" ID="gv_programacionCancelacion" runat="server" AutoGenerateColumns="false" DataKeyNames="idMovCtaPP,
            idRequerimientosCadena">
            <RowStyle CssClass="GrillaRow" />
            <HeaderStyle CssClass="GrillaHeader" />
                <Columns>
                    <asp:BoundField DataField="nroRequerimientoCadena" HeaderText="Requerimiento" />
                    <asp:BoundField DataField="mp_Nombre" HeaderText="Medio Pago" />
                    <asp:BoundField DataField="ban_Nombre" HeaderText="Banco" />
                    <asp:BoundField DataField="cb_numero" HeaderText="Nro. Cuenta" />
                    <asp:BoundField DataField="NroDocumentoRef" HeaderText="Nro. Doc. Ref." ItemStyle-ForeColor="Red" />
                    <asp:BoundField DataField="tipocambio" HeaderText="T.C" />
                    <asp:TemplateField HeaderText="Detracción">
                        <ItemTemplate>
                        <asp:RadioButton ID="esDetraccion" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"pp_esDetraccion") %>' Enabled="false" />
                        <asp:HiddenField ID="hddIdTipoOperacion" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"idTipoOperacion") %>' />
                        <asp:HiddenField ID="hddidProveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"idProveedor") %>' />
                        <asp:HiddenField ID="hddidRequerimientosCadena" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"idRequerimientosCadena") %>' />
                        <asp:HiddenField ID="hddidMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago") %>' />
                        <asp:HiddenField ID="IdDocumentoRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef") %>' />
                        <asp:HiddenField ID="hddidMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda") %>' />
                        <asp:HiddenField ID="hddIdBanco" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdBanco") %>' />
                        <asp:HiddenField ID="hddIdCuentaBancaria" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>' />
                        <asp:HiddenField ID="hddidProgrmacion" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProgramacionPago") %>' />
                        <asp:HiddenField ID="hddpla_cCuentaContable" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"pla_cCuentaContable") %>' />
                        <asp:HiddenField ID="hddidTienda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"idTienda") %>' />
                        <asp:HiddenField ID="hddcadenaDocumentoReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"cadenaDocumentoReferencia") %>' />
                        <asp:HiddenField ID="hddtipocambio" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"tipocambio") %>' />
                        <asp:HiddenField ID="hddflagCancelacion" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"flagCancelacion") %>' />
                        <asp:HiddenField ID="hddMontoProgramado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"pp_MontoPagoProg") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Retención">
                        <ItemTemplate>
                            <asp:RadioButton ID="esRetencion" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"pp_esRetencion") %>' Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="pp_FechaPagoProg" HeaderText="Fec.Programación" />
                    <asp:BoundField DataField="mon_Simbolo" ItemStyle-ForeColor="Red"/>                    
                    <asp:TemplateField HeaderText="M.N" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblMonedaNacional" runat="server" Text='<%# Eval("monedaNacional","{0:F2}") %>' ForeColor="Red"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="M.E" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblMonedaExtranjera" runat="server" Text='<%# Eval("monedaExtranjera","{0:F2}") %>' ForeColor="Red"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnCancelar" runat="server" Text="Generar Cancelación" OnClientClick="return(valOnClick_btnCancelacion(this));"
                            Visible='<%# if(Eval("pp_esDetraccion") = false and Eval("pp_esRetencion") = false,true,false) %>' 
                            Enabled='<%# if(Eval("flagCancelacion") = false,true,false) %>' />
                            <asp:Button ID="btnGenerarDetraccion" runat="server" Text="Generar Detracción" Enabled='<%# if(Eval("flagCancelacion") = false,true,false) %>'
                            Visible='<%# if(Eval("pp_esDetraccion") = true,true,false) %>' CommandArgument='<%# Container.DataItemIndex %>' CommandName="detraccion" />
                            <asp:Button ID="btnGenerarRetencion" runat="server" Text="Generar Retención" OnClientClick="return(valOnClick_btnRetencion(this));"
                            Visible='<%# if(Eval("pp_esRetencion") = true,true,false) %>' Enabled='<%# if(Eval("flagCancelacion") = false,true,false) %>' />
                            <asp:Button ID="btnVerCancelacion" runat="server" Text="Ver Cancelación" OnClientClick="return(valOnClick_btnCancelacion(this));"
                            Visible='<%# if(Eval("flagCancelacion") = true,true,false) %>' Enabled='<%# if(Eval("flagCancelacion") = true,true,false) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div id="capaRegistrarDetraccion" style="width:500px; height:auto; position:absolute; display:none; top:100px; left:30%; background-color:White; border:1px solid blue;z-index:2">
        <table style="width:100%">
            <tr>
                <td colspan="2">
                    <div style="width:95%; float:left" class="TituloCelda">
                        <span>INGRESAR DATOS DE LA DETRACCIÓN</span>
                    </div>
                    <div style="width:5%; float:right; vertical-align:middle; text-align:right">
                        <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa('capaRegistrarDetraccion')   );" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Nro Constancia:
                </td>
                <td>
                    <asp:TextBox ID="txtNroConstancia" runat="server" Text=""></asp:TextBox>
                    <span id="spnNroConstancia" style="color:Red" title="Campo Obligatorio"></span>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Monto de Depósito:
                </td>
                <td>
                    <asp:TextBox ID="txtmontoDeposito" runat="server" Text=""></asp:TextBox>
                    <span id="spnmontoDeposito" style="color:Red" title="Campo Obligatorio"></span>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Fecha y Hora de Pago:
                </td>
                <td>
                    <asp:TextBox ID="txtFechaPago" runat="server" Text="" Width="80px"></asp:TextBox>
                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaPago">
                                </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtFechaPago" Format="dd/MM/yyyy" >
                    </cc1:CalendarExtender>
                    <asp:TextBox ID="txtHorapago" runat="server" Text="" Width="40px"></asp:TextBox>
                    <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99:99" TargetControlID="txtHorapago">
                                </cc1:MaskedEditExtender>
                    <span id="spntxtFechaPago" style="color:Red" title="Campo Obligatorio"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvDocRefDetracciones" runat="server" AutoGenerateColumns="false" Width="100%">
                        <RowStyle CssClass="GrillaRow" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbEliminar" runat="server" Text="Eliminar" ForeColor="Blue" OnClick="lblEliminar_click"></asp:LinkButton>
                                    <asp:HiddenField ID="hddidDocumento" runat="server" Value='<%# Eval("IdDocumento") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo Documento" />
                            <asp:TemplateField HeaderText="Nro Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNroDocumentoDetraccion" runat="server" Text='<%# Eval("NroDocumento") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Monto Pagado">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalPagoDetraccion" runat="server" Text='<%# Eval("TotalAPagar") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="lbSiguiente" runat="server" Text="Continuar.." ForeColor="Blue" Font-Bold="true" Font-Italic="true" Font-Underline="true"
                    OnClientClick="return(validarDetracción(this));"></asp:LinkButton>
                    <asp:LinkButton ID="lbCancelar" runat="server" Text="Cancelar.." ForeColor="Blue" Font-Bold="true" Font-Italic="true" Font-Underline="true"
                    OnClientClick="return(setearDatosDetraccion(this));"></asp:LinkButton>

                    <asp:HiddenField ID="hddidproveedor" runat="server" Value=0 />
                    <asp:HiddenField ID="hddidMoneda" runat="server" Value=0 />
                    <asp:HiddenField ID="hddidtienda" runat="server" Value=0 />
                    <asp:HiddenField ID="hddidEmpresa" runat="server" Value=0 />
                    <asp:HiddenField ID="hddidtipoDocumento" runat="server" Value=0 />
                    <asp:HiddenField ID="hddidRequerimiento" runat="server" Value=0 />
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        var idRequerimientosCadena = 0;
        var idtipoOperacion = 0;
        var IdUsuario = '<%= Session("IdUsuario") %>';
        var idProveedor = 0;
        var idMedioPago = 0;
        var idDocumentoReferencia = 0; //id del medio de pago
        var idMoneda = 0;
        var hddMontoProgramado = 0;
        var idbanco = 0;
        var esDetraccion = 0;
        var idProgrmacion = 0;
        var ctaContable;
        var IdCuentaBancaria=0;

        function validarCamposDetraccion(control) {
            if (esDetraccion == true) {
                idMoneda = 1;
                window.open('../CxP/frmCancelarBancosxProvisiones.aspx?IdRequerimiento=' + IdDocumento + '&IdUsuario=' + IdUsuario + '&idTipoOperacion=' + idtipoOperacion + '&idProveedor=' + idProveedor + '&idMedioPago=' + idMedioPago + '&idDocumentoReferencia=' + idDocumentoReferencia + '&idMoneda=' + idMoneda + '&idbanco=' + idbanco + '&IdCuentaBancaria=' + IdCuentaBancaria + '&esDetraccion=' + esDetraccion + '&idProgramacion=' + idProgramacion + '&ctaContable=' + ctaContable, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                return true;
            } else {
                window.open('../CxP/frmCancelarBancosxProvisiones.aspx?IdRequerimiento=' + IdDocumento + '&IdUsuario=' + IdUsuario + '&idTipoOperacion=' + idtipoOperacion + '&idProveedor=' + idProveedor + '&idMedioPago=' + idMedioPago + '&idDocumentoReferencia=' + idDocumentoReferencia + '&idMoneda=' + idMoneda + '&idbanco=' + idbanco + '&IdCuentaBancaria=' + IdCuentaBancaria + '&esDetraccion=' + esDetraccion + '&idProgramacion=' + idProgramacion + '&ctaContable=' + ctaContable, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                return true;
             }
        }

        function validarDetracción(control) {
            var spnNroConstancia = document.getElementById('spnNroConstancia');
            //var spnCuentaDetracciones = document.getElementById('spnCuentaDetracciones');
            var spnmontoDeposito = document.getElementById('spnmontoDeposito');
            var spntxtFechaPago = document.getElementById('spntxtFechaPago');

            var nroConstancia = document.getElementById('<%=txtNroConstancia.ClientID %>');
            var montoDepositado = document.getElementById('<%=txtmontoDeposito.ClientID %>');
            var txtFechaPago = document.getElementById('<%=txtFechaPago.ClientID %>');

            if (nroConstancia.value.length == 0) {
                spnNroConstancia.innerHTML = "*";
                return false;
            } else {spnNroConstancia.innerHTML = ""; }

            if (montoDepositado.value == "" || montoDepositado.value.length == 0) {
                spnmontoDeposito.innerHTML = "*";
                return false;
            } else { spnmontoDeposito.innerHTML = ""; }

        }
        function mostrarCapaDetraccion() {
            onCapa('capaRegistrarDetraccion');
            document.getElementById('<%=txtNroConstancia.ClientID%>').select();
            document.getElementById('<%=txtNroConstancia.ClientID%>').focus();
            return false;
        }
        function setearDatosDetraccion(control) {
            //setea variables 
            var grilla = document.getElementById('<%=gv_programacionCancelacion.ClientID%>');
            var grillaDetraccion = document.getElementById('<%=gvDocRefDetracciones.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[12].children[0];
                    //var hddIdDocumento = rowElem.cells[2].children[1];
                    if (control.id == boton.id) {
                        idDocumentoReferencia = rowElem.cells[6].children[5].value;
                        idMedioPago = rowElem.cells[6].children[4].value;
                        idRequerimientosCadena = rowElem.cells[6].children[3].value;
                        idtipoOperacion = rowElem.cells[6].children[1].value;
                        idProveedor = rowElem.cells[6].children[2].value;
                        //idMoneda = rowElem.cells[6].children[6].value;
                        idMoneda = 1
                        idbanco = rowElem.cells[6].children[7].value;
                        IdCuentaBancaria = rowElem.cells[6].children[8].value;
                        idProgramacion = rowElem.cells[6].children[9].value;
                        ctaContable = rowElem.cells[6].children[10].value;
                        //hddMontoProgramado = rowElem.cells[6].children[15].value;
                        esDetraccion = rowElem.cells[6].children[0].getElementsByTagName("input")[0].checked;
                        break;
                    }
                }
            }
            if (grillaDetraccion != null) {
                for (var j = 1; j < grillaDetraccion.rows.length; j++) {
                    var rowElem = grillaDetraccion.rows[j];
                    hddMontoProgramado = parseFloat(rowElem.cells[3].children[0].innerHTML);
                }
            }

            return true;
        }
        function abrirVentanaDetraccion() {
            window.open('../CxP/frmCancelarBancosxProvisiones.aspx?idCadenaRequerimiento=' + idRequerimientosCadena + '&IdUsuario=' + IdUsuario + '&idTipoOperacion=' + idtipoOperacion + '&idProveedor=' + idProveedor + '&idMedioPago=' + idMedioPago + '&idDocumentoReferencia=' + idDocumentoReferencia + '&idMoneda=' + idMoneda + '&idbanco=' + idbanco + '&IdCuentaBancaria=' + IdCuentaBancaria + '&esDetraccion=' + esDetraccion + '&idProgramacion=' + idProgramacion + '&ctaContable=' + ctaContable + '&hddMontoProgramado=' + hddMontoProgramado, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }

        function cadenaProveedores() {
            var sql = '<%=  Session("cadenaProveedores") %>';
            var filas = sql.split(";");
            return false;
        }

        function valOnClick_btnCancelacion(control) {

            var grilla = document.getElementById('<%=gv_programacionCancelacion.ClientID%>');
            var idCadenaRequerimiento = 0;
            var idtipoOperacion = 0;
            var IdUsuario = '<%= Session("IdUsuario") %>';
            var idProveedor = 0;
            var idMedioPago = 0;
            var idDocumentoReferencia = 0; //id del medio de pago
            var idMoneda = 0;
            var idbanco = 0;
            var esDetraccion = 0;
            var idProgrmacion = 0;
            var ctaContable;
            var hddMontoProgramado = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[12].children[0];
                    //var hddIdDocumento = rowElem.cells[2].children[1];
                    if (control.id == boton.id) {
                        idDocumentoReferencia = rowElem.cells[6].children[5].value;
                        idMedioPago = rowElem.cells[6].children[4].value;
                        idCadenaRequerimiento = rowElem.cells[6].children[3].value;
                        idtipoOperacion = rowElem.cells[6].children[1].value;
                        idProveedor = rowElem.cells[6].children[2].value;
                        idMoneda = rowElem.cells[6].children[6].value;
                        idbanco = rowElem.cells[6].children[7].value;
                        IdCuentaBancaria = rowElem.cells[6].children[8].value;
                        idProgramacion = rowElem.cells[6].children[9].value;
                        ctaContable = rowElem.cells[6].children[10].value;
                        hddMontoProgramado = rowElem.cells[6].children[15].value;
                        esDetraccion = rowElem.cells[6].children[0].getElementsByTagName("input")[0].checked; 
                            
                        break;
                    }
                }
            }
//            if (IdDocumento == 0 || isNaN(IdDocumento)) {
//                alert('El documento no tiene Documentos Referenciados. No procede la Operación.');
//                return false;
//            } else {
            window.open('../CxP/frmCancelarBancosxProvisiones.aspx?idCadenaRequerimiento=' + idCadenaRequerimiento + '&IdUsuario=' + IdUsuario + '&idTipoOperacion=' + idtipoOperacion + '&idProveedor=' + idProveedor + '&idMedioPago=' + idMedioPago + '&idDocumentoReferencia=' + idDocumentoReferencia + '&idMoneda=' + idMoneda + '&idbanco=' + idbanco + '&IdCuentaBancaria=' + IdCuentaBancaria + '&esDetraccion=' + esDetraccion + '&idProgramacion=' + idProgramacion + '&ctaContable=' + ctaContable + '&hddMontoProgramado=' + hddMontoProgramado, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                return false
                //return false;
           // }
        }

        function valOnClick_btnRetencion(control) {

            var grilla = document.getElementById('<%=gv_programacionCancelacion.ClientID%>');
            var idCadenaRequerimiento = 0;
            var idtipoOperacion = 0;
            var IdUsuario = '<%= Session("IdUsuario") %>';
            var idProveedor = 0;
            var idMedioPago = 0;
            var idDocumentoReferencia = 0; //id del medio de pago
            var idMoneda = 0;
            var idbanco = 0;
            var esDetraccion = 0;
            var idProgrmacion = 0;
            var ctaContable;
            var cadenaIdDocumentos = '';
            var tc = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[12].children[0];
                    //var hddIdDocumento = rowElem.cells[2].children[1];
                    if (control.id == boton.id) {
                        idDocumentoReferencia = rowElem.cells[6].children[5].value;
                        idMedioPago = rowElem.cells[6].children[4].value;
                        idCadenaRequerimiento = rowElem.cells[6].children[3].value;
                        idtipoOperacion = rowElem.cells[6].children[1].value;
                        idProveedor = rowElem.cells[6].children[2].value;
                        idMoneda = rowElem.cells[6].children[6].value;
                        idbanco = rowElem.cells[6].children[7].value;
                        IdCuentaBancaria = rowElem.cells[6].children[8].value;
                        idProgramacion = rowElem.cells[6].children[9].value;
                        ctaContable = rowElem.cells[6].children[10].value;
                        cadenaIdDocumentos = rowElem.cells[6].children[12].value;
                        tc = rowElem.cells[6].children[13].value;
                        esDetraccion = rowElem.cells[6].children[0].getElementsByTagName("input")[0].checked;
                        
                        break;
                    }
                }
            }
//            if (IdDocumento == 0 || isNaN(IdDocumento)) {
//                alert('El documento no tiene Documentos Referenciados. No procede la Operación.');
//                return false;
//            } else {
                window.open('../CxP/FrmComprobanteRetencion.aspx?idCadenaRequerimiento=' + idCadenaRequerimiento + '&IdUsuario=' + IdUsuario + '&idTipoOperacion=' + idtipoOperacion + '&idProveedor=' + idProveedor + '&idMedioPago=' + idMedioPago + '&idDocumentoReferencia=' + idDocumentoReferencia + '&idMoneda=' + idMoneda + '&idbanco=' + idbanco + '&IdCuentaBancaria=' + IdCuentaBancaria + '&esDetraccion=' + esDetraccion + '&idProgramacion=' + idProgramacion + '&ctaContable=' + ctaContable + '&cadenaIdDocumentos=' + cadenaIdDocumentos + '&tc=' + tc, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                return false;
            //}
        }
    </script>
</asp:Content>

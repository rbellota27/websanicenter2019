﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmCancelarBancosxProvisiones.aspx.vb" Inherits="APPWEB.frmCancelarBancosxProvisiones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" OnClientClick="return(   valOnClickNuevo()  );" Width="80px" Visible="false"
                                runat="server" Text="Nuevo" ToolTip="Generar un Doc. Cancelación de Bancos desde un Doc. Toma de Inventario." />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" OnClientClick="return(  valOnClickEditar()  );" Width="80px" Visible="false"
                                runat="server" Text="Editar" ToolTip="Editar" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );" Visible="false"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" OnClientClick="return(  valOnClickAnular()   );" Width="80px" Visible="false"
                                runat="server" Text="Anular" ToolTip="Anular Doc. Ajuste de Inventario" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( valSaveDocumento()  );" Width="80px"
                                runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" OnClientClick=" return(  valOnClickImprimir()  );  "
                                Width="80px" runat="server" Text="Imprimir" ToolTip="Imprimir Documento" />
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="width: 100%" align="right">
                            <table>
                                <tr>
                                    <td class="Texto">
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DOCUMENTO DE CANCELACIÓN ASOCIADO A BANCOS
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td class="Label">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Tienda:
                            </td>
                            <td style="width: 100px">
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return(  valOnKeyPressCodigoDoc(this)  );"
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Documento por [ Serie - Número ]." OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="Búsqueda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                Fecha Emisión:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="Label">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList CssClass="LabelRojo" ID="cboMoneda" Width="100%" runat="server"
                                    Enabled="true" onClick="return(  valOnClickMonedaEmision()  );">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Nro Voucher - (SOLUCONT):
                            </td>
                            <td>
                                <asp:TextBox ID="txtNroVoucher" runat="server" ></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="Label">
                                Caja:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboCaja" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>--%>
                        <tr>
                            <td class="Label">
                                Tipo Operación:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                BENEFICIARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripción:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" OnClientClick="return( valBuscarPersona()  );"
                                    runat="server" Text="Buscar" ToolTip="Buscar Cliente" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNI" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="SubTituloCelda">
            <td>
                DETALLE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table id="TablaDetalle" width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Documento de Cancelación por:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboMotivoDocCancelacionBancos" AutoPostBack="true" CssClass="LabelRojo"
                                                Font-Bold="true" runat="server" ToolTip="Seleccione un Motivo del Documento de Cancelación"
                                                onClick="return(  valSelectMotivoPago() );">
                                                <asp:ListItem Selected="True" Value="0">-- Seleccione un Motivo --</asp:ListItem>
                                                <asp:ListItem Value="1">Pago de deudas</asp:ListItem>
                                                <%--<asp:ListItem Value="2">Otros</asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto"> 
                                           A la fecha:
                                        </td>
                                        <td>  <asp:TextBox ID="txtFechaVcto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaVcto" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaVcto" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaVcto">
                                </cc1:CalendarExtender></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMessageHelp" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="**** Seleccione los documentos a los cuales ingresar un Abono."></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_Deudas" runat="server" AutoGenerateColumns="false" Width="100%" DataKeyNames="idDocumento">
                                                <Columns>
                                                    <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" />
                                                    <asp:TemplateField HeaderText="Nro. Documento">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumentoDeuda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"codigo")%>'></asp:Label>
                                                            <%--<asp:HiddenField ID="hddmontoProgramado" runat="server" Value='<%#Eval("montoProgramado") %>' />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FechaEmision" NullDisplayText="---" DataFormatString="{0:dd/MM/yyyy}"
                                                        HeaderText="Fecha Emisión" />
                                                    <asp:BoundField DataField="getFechaVctoText" NullDisplayText="---" HeaderText="Fecha Vcto."
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NroDiasMora" HeaderText="Días Vencidos" />
                                                    <asp:TemplateField HeaderText="Total Doc.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMonedaDocumentoDocumento" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            <asp:Label ID="lblMontoDocumento" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoTotal","{0:F3}")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdDocumentoDeuda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"idRequerimiento") %>' />
                                                            <asp:HiddenField ID="hddIdMovCtaPP" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"idMovCtaPP") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Retención">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMontoRetencion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            <asp:Label ID="lblMontoRetencion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Retencion","{0:F3}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Detracción">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMontoDetraccion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            <asp:Label ID="lblMontoDetraccion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Detraccion","{0:F3}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Pagar" ItemStyle-ForeColor="Red">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMontoTotal" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            <asp:Label ID="lblTotalAPagar" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                      
                        <tr>
                            <td>
                                <table style="width:100%">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnAddConcepto_Otros" Visible="false" Width="100px" runat="server"
                                                Text="Nuevo detalle" ToolTip="Agregar un nuevo detalle al documento." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_Otros" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ItemStyle-Width="65px"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="Concepto" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:DropDownList ID="cboConcepto" runat="server" DataTextField="Nombre" DataValueField="Id"
                                                                            DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaConcepto")%>'>
                                                                        </asp:DropDownList>
                                                                        <asp:TextBox ID="txtDescripcionConcepto" TabIndex="250" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                            onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" Width="400px"
                                                                            ToolTip="Ingrese una Descripción Adicional" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Concepto")%>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMoneda" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                                        <asp:TextBox ID="txtAbono" TabIndex="260" onKeypress="return(   validarNumeroPuntoPositivo(event)   );"
                                                                            onblur="return( valBlur(event)  );" onKeyup="return( calcularTotalAPagar()  );" onFocus="return(  aceptarFoco(this) );"
                                                                            Width="90px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F2}")%>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cancelacion" runat="server">
                    <table id="Cancelacion" width="100%">
                        <tr>
                            <td class="TituloCelda">
                                <asp:GridView ID="GV_NotaCredito" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center"
                                            SelectText="Seleccionar" ShowSelectButton="true" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMontoRecibir_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            <asp:TextBox ID="txtMontoRecibir_Find" runat="server" Font-Bold="true" onblur="return(  valBlur(event)   );"
                                                                onFocus="return(   aceptarFoco(this)   );" onKeypress="return(  validarNumeroPunto(event)  );"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>' Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Tipo" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Documento"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento_Find" runat="server" Font-Bold="true" ForeColor="Red"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                            <asp:HiddenField ID="hddIdMonedaDocRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                            <asp:HiddenField ID="hddIdEstadoCancelacion_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEstadoCancelacion")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Fec. Emisión" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Fec. Vcto." ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Contador" DataFormatString="{0:F0}" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Vigencia (Días)" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            <asp:Label ID="lblMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaSaldo_Find" runat="server" Font-Bold="true" ForeColor="Red"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            <asp:Label ID="lblSaldo_Find" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NomPropietario" HeaderStyle-HorizontalAlign="Center" HeaderText="Empresa"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomTienda" HeaderStyle-HorizontalAlign="Center" HeaderText="Tienda"
                                            ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td class="SubTituloCelda">
                                APLICACIÓN DE FACTURAS CONTRA DEUDAS PENDIENTES
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gv_AplicacionFacturasDeudasP" runat="server" AutoGenerateColumns="false">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:BoundField DataField="tipoDocumento" HeaderText="Tipo" />                                        
                                        <asp:TemplateField HeaderText="Nro. Documento">
                                           <ItemTemplate>
                                               <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#Eval("idDocumento") %>' />
                                               <asp:Label ID="nroDocumento" runat="server" Text='<%# Eval("doc_codigo") %>'></asp:Label>
                                           </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total aplicado">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMonedaAplicacion" runat="server" ForeColor="Red" Text='<%# Eval("nom_simbolo") %>'></asp:Label>
                                                <asp:Label ID="lblMontoAplicacion" runat="server" ForeColor="Red" Text='<%# Eval("doc_TotalAPagar","{0:F2}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCelda">
                                DATOS DE CANCELACIÓN
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="DatosCancelacionCab">
                                    <tr>
                                        <td class="Texto">
                                            Total a Pagar:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMonedaTotalAPagar" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="S/."></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalAPagar" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                            runat="server" CssClass="TextBox_ReadOnly" Width="100px" Text="0"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="SubTituloCelda">
                                CANCELACIÓN
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CP_Contado" runat="server" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblMedioPago_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Medio de Pago:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMedioPago" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                                AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblMonto_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Monto:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMoneda_DatoCancelacion" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtMonto_DatoCancelacion" onFocus=" return(  aceptarFoco(this)   ); "
                                                                runat="server" onKeypress="  return(  valOnKeyPress_txtMonto_DatoCancelacion(event)  ); "
                                                                Width="100px" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddDatoCancelacion" runat="server" Text="Agregar" Width="70px" Visible="false"
                                                                OnClientClick=" return(   valOnClick_btnAddDatoCancelacion()  ); " />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="Panel_DatosCancelacion_Add" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td style="text-align: right">
                                                                            <asp:Label ID="lblBanco_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Banco:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="true">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="text-align: right">
                                                                            <asp:Label ID="lblCuentaBancaria_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                                Text="Cuenta:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="cboCuentaBancaria" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: right">
                                                                            <asp:Label ID="lblNro_DC" runat="server" CssClass="Texto" Font-Bold="true" Text="Número:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtNro_DC" runat="server" onFocus=" return(  aceptarFoco(this)   ); "
                                                                                onKeypress=" return(    valOnKeyPress_txtNro_DC()    ); " Width="90%"></asp:TextBox>
                                                                        </td>
                                                                        <td style="text-align: right">
                                                                            <asp:Label ID="lblFechaACobrar_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                                Text="Fecha a Cobrar:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtFechaACobrar" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                                                                Width="100px"></asp:TextBox>
                                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" ClearMaskOnLostFocus="false"
                                                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaACobrar">
                                                                            </cc1:MaskedEditExtender>
                                                                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                                TargetControlID="txtFechaACobrar">
                                                                            </cc1:CalendarExtender>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnSeleccionarCheque" runat="server" Text="Seleccionar Cheque" ToolTip="Búsqueda de Cheques" Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_Cancelacion" runat="server" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ItemStyle-HorizontalAlign="Center"
                                                            ShowSelectButton="true" ItemStyle-Width="75px" />
                                                        <asp:TemplateField HeaderText="Monto" ItemStyle-Width="120px" HeaderStyle-Height="25px"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>                                                                
                                                                            <asp:Label ID="lblMonedaMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimboloDestino")%>'></asp:Label>

                                                                            <asp:Label ID="lblMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoEquivalenteDestino","{0:F2}")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago")%>' />

                                                                            <asp:HiddenField ID="hddIdMonedaOrigen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Descripcion" HeaderStyle-HorizontalAlign="Center" HeaderText="Descripción" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Total Recibido:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaTotalRecibido" Text="-" runat="server" CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTotalRecibido" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Faltante:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaFaltante" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFaltante" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Vuelto:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaVuelto" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtVuelto" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <asp:TextBox ID="txtObservaciones" Width="100%" Height="100px" TextMode="MultiLine"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                        runat="server"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" Value="51" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoPrincipal" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoInterfaz" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <div id="capaDocumento_NotaCredito" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; background-color: white; z-index: 2;
        display: none; top: 480px; left: 25px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton8" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumento_NotaCredito')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_NotaCredito" runat="server" ScrollBars="Horizontal" Width="100%">
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaAbonos" style="border: 3px solid blue; padding: 8px; width: 700px; height: auto;
        position: absolute; top: 298px; left: 98px; background-color: white; z-index: 2;
        display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaAbonos'));" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" class="TituloCelda">
                    Abonos
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="GV_Abonos" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" />
                            <asp:BoundField DataField="getNroDocumento" HeaderText="Nro. Documento" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisión" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:TemplateField HeaderText="Monto">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblMontoAbonado" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaCheques" style="border: 3px solid blue; padding: 8px; width: 700px;
        height: auto; position: absolute; top: 298px; left: 98px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
<%--                    <asp:ImageButton ID="ibtnCerrarCapaCheques" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaCheques'));" />--%>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" class="TituloCelda">
                    Cheques
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="GV_Cheques" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSeleccionarCheque" runat="server" OnClick="lbtnSeleccionarCheque_Click">Seleccionar</asp:LinkButton>
                                    <asp:HiddenField ID="hddId" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Serie" HeaderText="Serie" />
                            <asp:BoundField DataField="ChequeNumero" HeaderText="Nro." />
                            <asp:BoundField DataField="MonedaSimbolo" HeaderText="Mon." />
                            <asp:BoundField DataField="DescMonto" HeaderText="Monto" />
                            <asp:BoundField DataField="DescFechaACobrar" HeaderText="Fecha a Cobrar" />
                            <asp:BoundField DataField="CuentaBancaria" HeaderText="Cta. Bancaria" />
                            <asp:BoundField DataField="Banco" HeaderText="Banco" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaMovCPP" style="border: 3px solid blue; padding: 8px; width: 900px; height: auto;
        position: absolute; top: 231px; left: 21px; background-color: white; z-index: 2;
        display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_CapaMovCPP" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaMovCPP'));" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" class="TituloCelda">
                    Cuentas Por Pagar:
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Abono:
                </td>
                <td>
                    <asp:TextBox ID="txtAbonoMovCPP" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_MovCtaPP" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="20" Width="100%" BackColor="Silver">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddIdMovCtaPP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                    <asp:LinkButton ID="lbtnSeleccionar" runat="server">Seleccionar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" />
                            <asp:BoundField DataField="DescFecha" HeaderText="Fecha Emisión" />
                            <asp:BoundField DataField="DescFechaVenc" HeaderText="Fecha Vencimiento" />
                            <asp:BoundField DataField="MonedaSimbolo" HeaderText="Mon." />
                            <asp:BoundField DataField="DescMonto" HeaderText="Monto" />
                            <asp:BoundField DataField="DescSaldo" HeaderText="Saldo" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Jurídica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:DropDownList ID="cboRol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razón Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        R.U.C.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%" BackColor="Silver">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cód." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razón Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicio_BA" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                    CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicio_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin_BA" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisión"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de búsqueda realiza un filtrado de acuerdo al cliente seleccionado.
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        //*************************   DATOS DE CANCELACION

        //document.onkeypress = valOnKeyPress_txtMonto_DatoCancelacion;
        function valOnKeyPress_txtMonto_DatoCancelacion(elEvento) {
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);

            if (caracter == 13) { // Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }

        function valOnClick_btnAddDatoCancelacion() {
            var cboMoneda = document.getElementById('<%=cboMoneda_DatoCancelacion.ClientID%>');
            if (cboMoneda != null) {
                if (isNaN(parseInt(cboMoneda.value)) || parseInt(cboMoneda.value) <= 0 || cboMoneda.value.length <= 0) {
                    alert('Debe seleccionar una [ Moneda ].');
                    return false;
                }
            }
            var txtMonto = document.getElementById('<%=txtMonto_DatoCancelacion.ClientID%>');
            if (txtMonto != null) {
                if (isNaN(parseFloat(txtMonto.value)) || parseFloat(txtMonto.value) <= 0 || txtMonto.value.length <= 0) {
                    alert('Debe ingresar un valor válido.');
                    if (!txtMonto.disabled) {
                        txtMonto.select();
                        txtMonto.focus();
                    }
                    return false;
                }
            }


            var cboBanco = document.getElementById('<%=cboBanco.ClientID%>');
            if (cboBanco != null) {
                if (isNaN(parseInt(cboBanco.value)) || parseInt(cboBanco.value) <= 0 || cboBanco.value.length <= 0) {
                    alert('Debe seleccionar un [ Banco ].');
                    return false;
                }
            }
            var cboCuentaBancaria = document.getElementById('<%=cboCuentaBancaria.ClientID%>');
            if (cboCuentaBancaria != null) {
                if (isNaN(parseInt(cboCuentaBancaria.value)) || parseInt(cboCuentaBancaria.value) <= 0 || cboCuentaBancaria.value.length <= 0) {
                    alert('Debe seleccionar una [ Cuenta Bancaria ].');
                    return false;
                }
            }
            var txtNro = document.getElementById('<%=txtNro_DC.ClientID%>');
            if (txtNro != null) {
                if (txtNro.value.length <= 0) {
                    alert('Debe ingresar un valor válido.');
                    txtNro.select();
                    txtNro.focus();
                    return false;
                }
            }
            return true;
        }
        function valFocusCancelacion() {
            document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            return false;
        }
        function valOnKeyPress_txtNro_DC() {
            if (event.keyCode == 13) { //**************** Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            }
            return true;
        }
        function valFocusCancelacion() {
            document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            return false;
        }
        //******************* FIN DATOS DE CANCELACION





        function valSelectMotivoPago() {
            var IdPersona = document.getElementById('<%=hddIdPersona.ClientID %>').value;
            if (isNaN(IdPersona) || IdPersona.length <= 0) {
                alert('Seleccione una Persona.');
                return false;
            }
            return true;
        }
        function selectDeuda(e) {
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;
            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[0].children[0].children[0].id == event_element.id) {
                    rowElem.cells[8].children[1].value = '';
                    rowElem.cells[8].children[1].select();
                    rowElem.cells[8].children[1].focus();
                    return true;
                }
            }
            alert('Elemento no encontrado.');
            return false;
        }

        function valKeyPressAbono(e) {
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;

            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[8].children[1].id == event_element.id) {
                    var selectStatus = rowElem.cells[0].children[0].getElementsByTagName('INPUT')[0].checked
                    if (selectStatus) {
                        return validarNumeroPuntoPositivo(e);
                    } else {
                        alert('Debe seleccionar el Documento al cual hacer el Abono.');
                        return false;
                    }
                }
            }
            alert('Elemento no encontrado.');
            return false;
        }




        function calcularDatosCancelacion() {
            var totalAPagar = parseFloat(document.getElementById('<%=txtTotalAPagar.ClientID%>').value);
            var totalRecibido = 0;
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    totalRecibido = totalRecibido + parseFloat(rowElem.cells[1].children[1].innerHTML);
                }
            }
            var faltante = 0;
            var vuelto = 0;
            if ((totalAPagar - totalRecibido) > 0) {
                faltante = Math.abs((totalAPagar - totalRecibido));
            } else {
                vuelto = Math.abs((totalAPagar - totalRecibido));
            }

            //********** Imprimimos valores
            document.getElementById('<%=txtTotalRecibido.ClientID%>').value = redondear(totalRecibido, 2);
            document.getElementById('<%=txtFaltante.ClientID%>').value = redondear(faltante, 2);
            document.getElementById('<%=txtVuelto.ClientID%>').value = redondear(vuelto, 2);
            return false;
        }


        function valSelectDeudaAbono(e) {
            selectDeuda(e);
            calcularTotalAPagar();
            return true; //**** Retornamos true para que se tenga el check
        }


        function calcularTotalAPagar() {

            var cboMotivoDocCancelacionBancos = document.getElementById('<%=cboMotivoDocCancelacionBancos.ClientID%>');
            var totalAPagar = 0;
            switch (cboMotivoDocCancelacionBancos.value) {
                case '0':  //********* Seleccionar Item                                
                    break;
                case '1':  //********** Pago deudas
                    var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            var selectStatus = rowElem.cells[0].children[0].getElementsByTagName('INPUT')[0].checked;
                            if (selectStatus) {
                                var abono = parseFloat(rowElem.cells[8].children[1].value);
                                if (isNaN(abono)) {
                                    abono = 0;
                                }
                                totalAPagar = totalAPagar + abono;
                            }
                        }
                    }
                    break;
                case '2':  //********** Otros
                    var grilla = document.getElementById('<%=GV_Otros.ClientID%>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            var abono = parseFloat(rowElem.cells[2].children[1].value);
                            if (isNaN(abono)) {
                                abono = 0;
                            }
                            totalAPagar = totalAPagar + abono;
                        }
                    }
                    break;
            }

            //********** Imprimimos valores
            document.getElementById('<%=txtTotalAPagar.ClientID%>').value = redondear(totalAPagar, 2);

            //********* calculamos los datos de cancelación
            calcularDatosCancelacion();
            return false;
        }
        function valBuscarPersona() {
            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            if (grilla == null) {
                mostrarCapaPersona();
            } else {
                alert('No puede seleccionar otra persona. Motivo del Documento de Cancelación actual : < PAGO DE DEUDAS >. Caso contrario seleccione otro motivo del Documento de Cancelación.');
            }
            return false;
        }

        function valFocusPersona() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }

        function valSaveDocumento() {

            //******** Validamos la cabecera

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(cboEmpresa.value) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(cboTienda.value) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(cboSerie.value) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie');
                return false;
            }



            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(hddIdPersona.value) || hddIdPersona.value.length <= 0) {
                alert('Seleccione una Persona.');
                return false;
            }

            var grillaDeuda = document.getElementById('<%=GV_Deudas.ClientID%>');
            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID%>');
            if (grillaDeuda == null && grillaOtros == null) {
                alert('Debe añadir detalles al documento.');
                return false;
            }

            var observaciones = document.getElementById('<%=txtObservaciones.ClientID %>');
            if (observaciones.value.length == 0) {
                alert('Deberá ingresar una glosa.')
                return false;
            }

            //******* Validamos que el abono sea menor o igual al saldo
            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            var suma = 0;
            var montoProgramado = 0;
            
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    suma = suma + parseFloat(rowElem.cells[7].children[1].innerHTML);
                    //montoProgramado = parseFloat(rowElem.cells[1].children[1].value);
                    
                    //var selectStatus = rowElem.cells[0].children[0].getElementsByTagName('INPUT')[0].checked;
//                    if (selectStatus) {
//                        var abono = parseFloat(rowElem.cells[8].children[1].value);
//                        var saldo = parseFloat(rowElem.cells[7].children[1].innerHTML);
//                        var txtAbono = rowElem.cells[8].children[1];
//                        if (isNaN(txtAbono.value) || parseFloat(txtAbono.value) <= 0 || txtAbono.value.length <= 0) {
//                            alert('Debe ingresar un valor válido mayor a cero.');
//                            txtAbono.select();
//                            txtAbono.focus();
//                            return false;
//                        }
//                        if (abono > saldo) {
//                            alert('La cantidad ingresada supera el saldo pendiente.');
//                            rowElem.cells[8].children[1].select();
//                            rowElem.cells[8].children[1].focus();
//                            return false;
//                        }
//                    }
                }
//                if (suma != montoProgramado) {
//                    alert('El monto del cheque emitido no coincide con la suma del total de Documentos sustentados.');
//                    return false;
//                }
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(hddIdPersona.value) || hddIdPersona.value.length <= 0) {
                alert('Seleccione una Persona.');
                return false;
            }

            var txtTotalAPagar = document.getElementById('<%=txtTotalAPagar.ClientID%>');
            if (isNaN(txtTotalAPagar.value) || txtTotalAPagar.value.length <= 0 || parseFloat(txtTotalAPagar.value) <= 0) {
                alert('El Total a Pagar debe ser mayor a cero.');
                return false;
            }

            var hddIdMedioPagoPrincipal = document.getElementById('<%=hddIdMedioPagoPrincipal.ClientID%>');

            //********* Validamos que no Existe Vuelto si el medio de pago es fuera de EFECTIVO
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID %>');
            if (grilla != null) {
                var vuelto = parseFloat(document.getElementById('<%=txtVuelto.ClientID%>').value);
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    if (parseInt(rowElem.cells[1].children[2].value) != (parseInt(hddIdMedioPagoPrincipal.value)) && vuelto > 0) {
                        alert('EXISTE UNA DIFERENCIA QUE SE AJUSTARÁ COMO REDONDEO.');
//                        alert('SE HAN INGRESADO MEDIOS DE PAGO QUE NO DEBEN GENERAR VUELTO. NO SE PERMITE LA OPERACIÓN.');
//                        return false;
                    }
                    if (parseFloat(rowElem.cells[1].children[1].innerHTML) <= 0) {
                        alert('No puede añadirse una cancelación con monto Negativo y/o CERO. Quite el dato de cancelación.');
                        return false;
                    }
                }

                var txtTotalAPagar = document.getElementById('<%=txtTotalAPagar.ClientID%>');
                var txtTotalRecibido = document.getElementById('<%=txtTotalRecibido.ClientID%>');
                if (parseFloat(txtTotalRecibido.value) < parseFloat(txtTotalAPagar.value)) {
                    alert('El Total Recibido es menor al Total a Pagar. Se Ajustará como redondeo');
                }

            } else {
                alert('INGRESE UN DATO DE CANCELACIÓN.');
                return false;
            }

            return confirm('Desea continuar con la Operación ?');
        }

        function valOnClickNuevo() {

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 0 || parseInt(hddFrmModo.value) == 4 || parseInt(hddFrmModo.value) == 5 || parseInt(hddFrmModo.value) == 6) {
                return true;
            }

            var grillaDeudas = document.getElementById('<%=GV_Deudas.ClientID %>');
            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID %>');
            if (grillaDeudas == null && grillaOtros == null) {
                return true;
            }

            return (confirm('Está seguro que desea generar un NUEVO Documento ?'));
        }


        function valOnClickEditar() {
            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID %>');
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(cboEstadoDocumento.value) == 2 || parseInt(hddFrmModo.value) == 6) {
                alert('El Documento de Cancelación ha sido anulado. No puede habilitarse la edición del Documento.');
                return false;
            }

            return (confirm('Antes de editar un Documento de Cancelación, el Sistema revertirá todos los movimientos efectuados por el Documento. Está seguro que desea continuar con el Proceso de Edición ?'));
        }


        function valOnClickAnular() {
            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID %>');
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(cboEstadoDocumento.value) == 2 || parseInt(hddFrmModo.value) == 6) {
                alert('El Documento ya ha sido ANULADO. No procede la Anulación del Documento.');
                return false;
            }
            return (confirm('Desea continuar con el Proceso de Anulación del Documento Documento de Cancelación ?'));
        }


        function valOnClickMonedaEmision() {
            var grillaDeudas = document.getElementById('<%=GV_Deudas.ClientID %>');
            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID %>');
            var grillaCancelacion = document.getElementById('<%=GV_Cancelacion.ClientID %>');

            if (grillaDeudas != null || grillaOtros != null || grillaCancelacion != null) {
                alert('No puede seleccionar otra Moneda de Emisión ya que se ha ingresado Detalles de Documento y/o Datos de Cancelación, si desea seleccionar otra Moneda de Emisión quite el detalle y/o los datos de cancelación del Documento.');
                return false;
            }

            return true;
        }

        function valOnClickImprimir() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            if (isNaN(parseInt(hddIdDocumento.value)) || parseInt(hddIdDocumento.value) <= 0 || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ningún Documento para Impresión.');
                return false;
            }
            window.open('../../Caja/Reportes/Visor1.aspx?iReporte=51&IdDocumento=' + hddIdDocumento.value, 'DocCancelBancos', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }

        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la Búsqueda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la Búsqueda
                alert('Debe habilitar la Búsqueda del Documento haciendo clic en el botón [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento, elEvento) {
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);

            if (caracter == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }
        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la Búsqueda
                alert('Debe habilitar la Búsqueda del Documento haciendo clic en el botón [ Buscar ]');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }

            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor válido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        /*Busqueda Cheques ini*/
        function valBuscarCheques() {
            var grilla = document.getElementById('<%=GV_Cheques.ClientID%>');
            if (grilla == null) {
                mostrarCapaCheques();
            } else {
                alert('No hay cheques asociados a este Acreedor. Utilice otro Medio de Pago');
            }
            return false;
        }
        function mostrarCapaCheques() {
            onCapa('capaCheques');
            /*document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();*/
            return false;
        }
        /*Busqueda Cheques fin*/

        /*Busqueda Personas ini*/

        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }

        function validarCajaBusquedaNumero(elEvento) {
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(elEvento, obj) {
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        /*Busqueda Personas fin*/
        
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
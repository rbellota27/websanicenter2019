﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="ProgPagoDocExternoOC.aspx.vb" Inherits="APPWEB.ProgPagoDocExternoOC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width:100%;">
    <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
        <span>REQUERIMIENTO</span>
    </div>        
    <div style="width:100%;float:left"> 
    <asp:GridView ID="GV_MovCuenta_CXP" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="IdDocumento,IdProveedor"
                        PageSize="50">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:BoundField DataField="TipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                    <asp:HiddenField ID="hddIdMovCuenta_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCtaPP")%>' />
                                    <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />
                                        <asp:HiddenField ID="hddIdUsuario" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuario")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Proveedor" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:Label ID="lblProveedor" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Proveedor")%>'></asp:Label>
                                   <asp:HiddenField ID="hddIdProveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProveedor")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tienda" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                        <asp:Label ID="lblTienda" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                              <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                     <asp:Label ID="lblfechaEmision" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaEmision","{0:d}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Fecha Vcto." ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblfechaVencimiento" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaVenc","{0:d}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>             
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                     <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                                     <asp:Label ID="lblMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Monto","{0:F3}")%>'
                                                    Font-Bold="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Deuda" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblMoneda_Saldo" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                                    <asp:Label ID="lblSaldo" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Saldo","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
    </div><!--Aca termina la grilla requerimiento-->

    <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
        <span>CUENTA BANCARIA - PROVEEDOR </span>
    </div>

    <div style="width:100%">
        <asp:GridView ID="dgvCuentaBancaria" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataRowStyle-Font-Bold="true"
            EmptyDataText="Sin datos" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center" >
            <Columns>                  
                                                
            <asp:BoundField DataField="Empresa" HeaderText="Empresa" >
            </asp:BoundField>
            <asp:BoundField DataField="Banco" HeaderText="Banco" NullDisplayText="0" />
            <asp:BoundField DataField="MonSimbolo" HeaderText="Moneda" NullDisplayText="0" />
            <asp:BoundField DataField="NroCuentaBancaria" HeaderText="Numero" NullDisplayText="0" />
            <asp:BoundField DataField="esCuentaDetraccion" HeaderText="Cta. Detracción" NullDisplayText="---" />
            <asp:BoundField DataField="Estado" HeaderText="Estado" NullDisplayText="0" />
                                                  
            </Columns>

            <AlternatingRowStyle CssClass="GrillaRowAlternating" />    
            <FooterStyle CssClass="GrillaFooter" />
            <HeaderStyle CssClass="GrillaHeader" />
            <PagerStyle CssClass="GrillaPager" />
            <RowStyle CssClass="GrillaRow" />                            
       </asp:GridView>
    </div>

    <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
        <span>PROGRAMACIÓN DE PAGOS</span>
    </div>

    <div style="width:100%;float:left;margin-top:5px;margin-bottom:5px">
        <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" Width="70px" ToolTip="Nuevo" />
        <asp:Button ID="btnEditar" runat="server" Text="Editar" Width="70px" ToolTip="Editar Programación de Pago" />
        <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" Width="70px" ToolTip="Eliminar Programación de Pago"
                                    OnClientClick="return(  valOnClick_btnEliminar() );" />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="70px" ToolTip="Registrar Programación de Pago"
                                    OnClientClick="return(  valOnClick_btnGuardar() );" />
        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="70px" ToolTip="Cancelar" />
        <asp:Button ID="btnAgregarDocProvisionados" runat="server" Text="Doc. Provisionados" OnClientClick="return(mostrarProvisiones());"/>
        <span class="Texto">Tipo Agente : </span>
        <asp:DropDownList ID="ddlTipoAgente" AutoPostBack="true" runat="server" DataValueField="IdAgente" DataTextField="ag_Nombre"></asp:DropDownList>
        <img id="btnCrearCuentaDetraccion" src="../Imagenes/ctrlShiftP.png" alt="" title="Crear Cuenta Bancaria Detracción." onclick="return(mostrarCrearCuentaDetraccion());" runat="server" visible="false" />
    </div>
    
    <!--Formulario-->
    <div style="width:100%;float:left">
        <asp:Panel id="Panel_ProgramacionPago" runat="server" Enabled="false">
        <table>
            <tr>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Fecha de Pago:
                </td>
                <td>
                    <asp:TextBox ID="txtFecha_ProgPago" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                                Width="80px"></asp:TextBox>                    
                    <cc1:calendarextender ID="txtFecha_ProgPago_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFecha_ProgPago">
                    </cc1:calendarextender>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Medio de Pago:
                </td>
                <td>
                    <asp:DropDownList ID="cboMedioPago_Prog" runat="server" Width="170px"></asp:DropDownList>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Banco:
                </td>
                <td>
                    <asp:DropDownList ID="cboBanco_Prog" runat="server" AutoPostBack="true" Width="180px"></asp:DropDownList>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Cuenta Bancaria:
                </td>
                <td>
                    <asp:DropDownList ID="cboCuentaBancaria_Prog" runat="server" AutoPostBack="true"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Tipo Moneda:
                </td>
                <td>
                    <asp:DropDownList ID="cboTipoMoneda" runat="server" Enabled="false"></asp:DropDownList>
                </td>
                <td colspan="6">
                    <table>
                        <tr>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                            Monto
                            </td>
                            <td>
                            <asp:Label ID="lblMoneda_MontoProg" runat="server" Text="S/." CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                <asp:TextBox ID="txtMontoProg" Text="0" Font-Bold="true" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                                            onFocus=" return( aceptarFoco(this)  ); " onblur=" return( valBlur(event)  ); " runat="server"></asp:TextBox>
                            </td>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                                <asp:Label ID="lblTipoCambio" Text="Tipo de Cambio:" runat="server" Visible="false"> </asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTipoCambio" Visible="false"  runat="server" Text="0" Font-Bold="true" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); " ></asp:TextBox>
                            </td>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                                <asp:Label ID="Label2" runat="server" Text="Monto a Pagar:" Visible="false"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblResultadoDolares" runat="server" Text="US$." Font-Bold="true" Visible="false" Style="color:Red"></asp:Label>
                                <asp:TextBox ID="txtResultadoTipoCambio" runat="server" Text="0" AutoPostBack="true" Visible="false" ReadOnly ="true" ></asp:TextBox>
                                <asp:Button  ID="btncalcularTC" runat="server"  Text="Calcular Monto" AutoPostBack="true" Visible="false"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td colspan="8" style="width:100%">
                <asp:TextBox ID="txtObservaciones_Prog" runat="server" Text="" TextMode="MultiLine" Width="100%" Rows="3"></asp:TextBox>
            </td>
            </tr>
        </table>
        </asp:Panel>
    </div>
    <!--Aca termina el formulario-->


    <table style="width: 100%;">
        <tr>
            <td class="TituloCelda">
                DOCUMENTOS PROVISIONADOS
            </td>
        </tr> 
            <tr>
                <td>
                    <asp:Panel ID="Panel_ProgramacionPago_v2" runat="server" Visible="false">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table class="style1">
                                        <tr>
                                            <td>
                                                <table>                                                    
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" class="Texto" >
                                                            Fecha de Pago:
                                                        </td>
                                                        <td>
                                                            
                                                        </td>
                                                        <td>
                                                        <asp:Label ID="lblTituloDocumentos" runat="server" Text="Documento: " Visible="false" style="text-align: right;color:Red" ></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="lblDocumento" runat="server" Text=" " Visible="false" style="text-align: right;color:blue" ></asp:Label>
                                                         </td>
                                                         <td>  </td>
                                                         <td>  </td>
                                                           <td>
                                                        <asp:Label ID="lblTituloTotalDeuda" runat="server" Text="Monto Total: " style="text-align: right;color:Red"  Visible="false"></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="lblDeudaResult" runat="server" Text=" " Visible="false" style="text-align: right;color:blue"></asp:Label>
                                                         </td>
                                                         <td>  </td>
                                                         <td>  </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" class="Texto">
                                                            Medio de Pago:
                                                        </td>
                                                        <td colspan="3">
                                                            
                                                        </td>
                                                         <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                          <td>
                                                        <asp:Label ID="lblTituloProveedor" runat="server" Text="Proveedor: " Visible="false" style="text-align: right;color:Red"></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="lblProveedorResult" runat="server" Text=" " Visible="false" style="text-align: right;color:blue"  ></asp:Label>
                                                         </td>
                                                         <td>  </td>
                                                         <td>  </td>
                                                         <td>
                                                          <asp:Label ID="lblTituloDeudaRestante" runat="server" Text="Deuda Restante: " style="text-align: right;color:Red"  Visible="false"></asp:Label>
                                                         </td>
                                                         <td>
                                                          <asp:Label ID="lblmonedaResult" runat="server" Text=" " style="text-align: right;color:Blue"  Visible="false"></asp:Label>
                                                          <asp:Label ID="lblDeudaRestanteResult" runat="server" Text="0" style="text-align: right;color:Blue;background-color:Yellow   "  Visible="false"></asp:Label>
                                                            <asp:Label ID="lblValoreditarDeudaSave" runat="server" Text="0" style="text-align: right;color:Green"  Visible="false"></asp:Label>
                                                         </td>
                                                         
                                                         <td>  </td>
                                                         <td>  </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" class="Texto">
                                                            Banco:
                                                        </td>
                                                        <td colspan="3">
                                                        
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                        <asp:Label ID="lblTituloFechaEmision" runat="server" Text="Fecha Emisión: " Visible="false" style="text-align: right;color:Red"></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="lblfechaemisionResult" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                         </td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                         <td>
                                                        <asp:Label ID="lblTituloFechaVenc" runat="server" Text="Fecha Vencimiento: " Visible="false" style="text-align: right;color:Red"></asp:Label>
                                                        </td>
                                                         <td>
                                                         <asp:Label ID="lblfechaVenciResult" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                         </td>
                                                         <td>  </td>
                                                        <td>  </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" class="Texto">
                                                            Cuenta Bancaria:
                                                        </td>
                                                        <td colspan="3">
                                                        
                                                        </td>
                                                         <td>
                                                         <asp:Label ID="lblTituloCondicionomercia" runat="server" Text="Condicion Comercial: "  Visible="false" style="text-align: right;color:Red"   ></asp:Label>
                                                          </td>
                                                            <td>
                                                            <asp:Label ID="lblCondicionComerciaResult" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                            </td>
                                                         <td>  </td>
                                                         <td>  </td>
                                                        
                                                              <td>
                                                         <asp:Label ID="lblTituloCondicionPago" runat="server" Text="Condicion de Pago: "  Visible="false" style="text-align: right;color:Red"   ></asp:Label>
                                                          </td>
                                                            <td>
                                                            <asp:Label ID="lblCondicionPagoResult" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                            </td>
                                                           
                                                            <td>  </td>
                                                            <td>  </td>
                                                    </tr>
                                                    <tr>
                                                    <td style="text-align: right; font-weight: bold" class="Texto">
                                                    <asp:Label ID="lblTipoMoneda" runat="server" Text ="Tipo Moneda: " ></asp:Label>
                                                    </td>
                                                    <td colspan="3">     
                                                    
                                                    </td>
                                                    <td>
                                                     <asp:Label ID="lblTituloSaldoProgramado" runat="server" Text="Saldo Programado: "  Visible="false" style="text-align: right;color:Red"   ></asp:Label>
                                                    </td>
                                                         <td>
                                                         <asp:Label ID="lblSaldoProgramado" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                         </td>
                                                    </tr>
                                                    <tr>
                                                     <td class="Texto" style="font-weight: bold; text-align: right">
                                                            Monto:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                      <td style="text-align: right; font-weight: bold" class="Texto"> 
                                                      <asp:Label ID="lblTipoCambio2" Text="Tipo de Cambio:" runat="server" Visible="false"> </asp:Label>
                                                      </td>
                                                      
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <td class="Texto" style="text-align: right; font-weight: bold"> 
                                                    <asp:Label ID="lblResultadoTipoCambio" runat="server" Text="Monto a Pagar:" Visible="false"></asp:Label>
                                                                                                      </td>
                                                    <td colspan="3">
                                                    <table>
                                                    <tr>
                                                    <td>
                                                       
                                                    </td>
                                                    <td>
                                                       
                                                    </td>
                                                    <td></td>
                                                    
                                                       <td>
                                                    
                                                    </td>
                                                    </tr>
                                                    </table>
                                                 
                                                    </td>
                                                                                       
                                                                                                    
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                                            Observaciones:
                                                        </td>
                                                        <td colspan="3">
                                                            &nbsp;</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>                                      
                                                            <td>
                                                             <asp:Label ID="lblindex" runat="server" Text=" " Visible="false"></asp:Label>
                                                            </td>
                                                             <td></td>
                                                        <td></td> 
                                                   
                                                    </tr>
                                                    
                                                </table  >
                                                <table width="100%">
                                                <tr>
                                                  <td class="SubTituloCelda">
                                                      CUENTA BANCARIA - PROVEEDOR
                                                    </td></tr>
                                                </table>
                                                 <asp:Panel ID="Panel_MovCuenta_CxP" runat="server" Height="65px" ScrollBars="Vertical">
                                                <table width="100%" >
                                                 
                                                <tr>
                                                 <td align="left">
                                                        
                                                   
                                                        </td> 
                                                </tr>
                                                </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td>
                                        <table width="100%">
                                        <tr>
                                                    <td class="TituloCelda ">
                                                      DOCUMENTOS EXTERNOS RELACIONADOS
                                                    </td>
                                                </tr>
                                        <tr>
                                        <td> 
                                                        
                                                <asp:GridView ID="GV_DocumentoRelacionado" runat="server" AutoGenerateColumns="False" Width="100%">
                                                   <Columns>                  
                                                
                                                    <asp:BoundField DataField="NroDocumento" HeaderText="Documento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" 
                                                     ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" ItemStyle-ForeColor="Red" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="true"  >
                                                        </asp:BoundField>
                                                    <asp:BoundField DataField="DescripcionPersona" HeaderText="Beneficiario" NullDisplayText="0"
                                                    HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" ItemStyle-Font-Bold="true"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"/>
                                                    
                                                    <asp:BoundField DataField="FechaEmision" HeaderText="Fecha emisión" DataFormatString="{0:dd/MM/yyy}" 
                                                    HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px" 
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"/>
                                                     

                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaRefExt1" runat="server" Font-Bold="true" ForeColor="Red"   Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMontoRefExt" runat="server"  Font-Bold="true" ForeColor="Red"  Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                                                    </td>
                                                                     <td>
                                                                        <asp:HiddenField ID="hddIdDocumentoRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocRelacionado")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                       <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaRefExt2" runat="server" Font-Bold="true" ForeColor="Red"   Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMontoRefExt2" runat="server"  Font-Bold="true" ForeColor="Red"  Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F3}")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    
                                     
                                                    </Columns>
                                                      <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                  
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                </asp:GridView>
                                                </td>
                                        </tr>
                                        </table>
                                        </td>
                                        </tr>
                                    </table>
                                    
                                </td>
                            </tr>
                        </table>
                        
                              <table width="100%">
            <tr>
            <td class="SubTituloCelda" >
                DOCUMENTOS PROVISIONADOS AGREGADOS
                    </td>
            </tr>
             <tr>
                             <td>
                                      <asp:GridView ID="GV_DocumentosProvisionados" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" 
                                      EmptyDataText="0 documentos provisionados" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-Font-Bold="true"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                          <asp:TemplateField HeaderText="Opciones">
                                        <ItemTemplate>
                                            <table>
                                                <tr><td>
                                                        <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </td>

                                                    <td>
                                                        <asp:LinkButton ID="QuitarDocumento" runat="server" OnClick="QuitarDocumento_Click" >Quitar</asp:LinkButton>
                                                    </td>
  
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                                
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblTipoDocProvisionado" runat="server" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                            
                                             <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Nro.Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblNroDocProvisionado" runat="server" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                       
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMonedaProvisionado" runat="server" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMontoProvisionado" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocumentoProvisionado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMonedaSaldoProvisionado" runat="server" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSaldoProvisionado" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
 
                                         <%--    <asp:BoundField DataField="FechaEmision" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center"  DataFormatString="{0:dd/MM/yyyy}" /> --%>                                                   
            
                                           <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Beneficiario" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblBeneficiarioProvisionado" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                             </td>
                             </tr>
              </table>
              
                            <table>
                            <tr>
                            <td>
                                <asp:Button id="btnHabilitarProvisiones" runat="server" Text="<-----------------------MOSTRAR OPCIONES PARA AGREGAR DOCUMENTOS PROVISIONADOS----------------------->"/>
                            </td>
                            </tr>
                            
                            </table>
                            <asp:Panel ID="PanelProvisionesMenu" runat="server">
                           
                             <table width="100%">
                            <tr>
                             <td class="TituloCelda">
                                AGREGAR DOC.PROVISIONADOS DOC.EXTERNO
                            </td>
                             </tr>
                            
                            </table>
                             <table width="80%">
                            <tr>
                              <td style="text-align: right; font-weight: bold" class="Texto">
                                  <asp:Label ID="lblTipoBusqueda1" runat="server" Text="Tipo de Búsqueda:"></asp:Label> 
                              </td>
                                <td>
                                <asp:RadioButtonList runat="server" ID="rdTipoBusqueda" RepeatDirection="Horizontal"
                                CssClass="Texto" AutoPostBack="true" >
                                <asp:ListItem Value="0" Text="Por Fecha"  Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Por Documento" ></asp:ListItem>
                                </asp:RadioButtonList> 
                                </td>
                            </tr>
                             </table>
                             <table>
                        <tr>
                        <td class="Texto">
                        <asp:Label ID="lblSerieDocExt" runat="server" Text="Nro. Serie: "  ></asp:Label>
                         <asp:Label id="lblFechaIni" runat="server" Text="Fech.Inicio:"></asp:Label> 
                        </td>
                        <td><asp:TextBox ID="txtSerieDocExt" runat="server" Text=" "></asp:TextBox>
                        <asp:TextBox ID="txtFechaVcto_Inicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                         Width="100px">
                         </asp:TextBox>
                               <cc1:MaskedEditExtender ID="txtFechaVcto_Inicio_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto_Inicio">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="txtFechaVcto_Inicio_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaVcto_Inicio">
                                        </cc1:CalendarExtender>
                        </td>
                        <td class="Texto"><asp:Label ID="lblCodigoDocExt" runat="server" Text="Nro. Código: "></asp:Label> 
                          <asp:Label id="lblFechaFin" runat="server" Text="Fech.Fin:"></asp:Label> </td>
                        <td > <asp:TextBox ID="txtcodigoDocExt" runat="server" Text=" "></asp:TextBox>
                           <asp:TextBox ID="txtFechaVcto_Fin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtFechaVcto_Fin_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto_Fin">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="txtFechaVcto_Fin_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaVcto_Fin">
                                        </cc1:CalendarExtender>
                        
                        </td>
                        <td><asp:Button id="btnBuscarDocExt" runat="server" Text="Buscar Doc.Ext."/> </td>
                        </tr>
                        </table>
                             <table width="100%">
                     <tr>
                     <td>
                       <asp:Panel ID="PanelDocExterno" runat="server" Height="85px" ScrollBars="Vertical">
                        <asp:GridView ID="GV_DocExterno" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Seleccionar" ShowSelectButton="True" />
                                                
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblDocumentoTipoDocExterno" runat="server" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                            
                                             <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Nro.Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblNroDocumentoExterno" runat="server" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                       
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMonedaDocexterno" runat="server" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMontoDocExt" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocumentoDocExt" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMonedaSaldoDocexterno" runat="server" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSaldoDocExt" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
 
                                             <asp:BoundField DataField="FechaEmision" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center"  DataFormatString="{0:dd/MM/yyyy}" />                                                    
            
                                           <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Beneficiario" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblBeneficiarioDocExt" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                             </asp:Panel>
                                    </td>
                      </tr>
                   </table>
                             <table width="100%"> 
                        
                            <tr>
                            <td>
                            <table width="100%">
                            <tr>
                             <td class="TituloCelda">
                                AGREGAR DOC.PROVISIONADOS DOC.ORDEN DE COMPRA
                            </td>
                            </tr>
                        </table>
                       </td>
                        </tr>
                            </table>
                             <table width="80%">
                            <tr>
                              <td style="text-align: right; font-weight: bold" class="Texto">
                                  
                                     <asp:Label ID="lbltipobusqueda2" runat="server" Text="Tipo de Búsqueda:"></asp:Label> 
                              </td>
                                <td>
                                <asp:RadioButtonList runat="server" ID="rdbSeleccionOC" RepeatDirection="Horizontal"
                                CssClass="Texto" AutoPostBack="true" >
                                <asp:ListItem Value="0" Text="Por Fecha"  Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Por Documento" ></asp:ListItem>
                                </asp:RadioButtonList> 
                                </td>
                            </tr>
                             </table>
                             <table>
                        <tr>
                        <td class="Texto">
                        <asp:Label ID="lblNroSerieOC" runat="server" Text="Nro. Serie: "  ></asp:Label>
                         <asp:Label id="lblFechaIniOc" runat="server" Text="Fech.Inicio:"></asp:Label> 
                        </td>
                        <td><asp:TextBox ID="txtNroSerieOC" runat="server" Text=" "></asp:TextBox>
                        <asp:TextBox ID="txtFechaIniOC" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                         Width="100px">
                         </asp:TextBox>
                           <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server"
                            ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaIniOC">
                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaIniOC">
                                        </cc1:CalendarExtender>
                        </td>
                        <td class="Texto"><asp:Label ID="lblCodigoOC" runat="server" Text="Nro. Código: "></asp:Label> 
                          <asp:Label id="lblFechaFinOC" runat="server" Text="Fech.Fin:"></asp:Label> </td>
                        <td > <asp:TextBox ID="txtNroCodigoOC" runat="server" Text=" "></asp:TextBox>
                           <asp:TextBox ID="txtFechaFinOC" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFinOC">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaFinOC">
                                        </cc1:CalendarExtender>
                        
                        </td>
                        <td><asp:Button id="btnBuscarOC" runat="server" Text="Buscar OC."/> </td>
                        </tr>
                        </table>
                             <table width="100%">
                     <tr>
                     <td>
                      <asp:Panel ID="PanelOrdenCompra" runat="server" Height="85px" ScrollBars="Vertical">
                        <asp:GridView ID="GV_DocOrdenCompra" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Seleccionar" ShowSelectButton="True" />
                                                
                                                  <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                          <td>
                                                              <asp:Label ID="lblDocumentoOC" runat="server" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                               <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Nro.Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                          <td>
                                                              <asp:Label ID="lblNroDocumentoOC" runat="server" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                         
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                          <td>
                                                              <asp:Label ID="lblMonedaOC" runat="server" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMontoPOc" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocumentoOC" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                              <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                          <td>
                                                              <asp:Label ID="lblmonesasaldoOC" runat="server" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSaldoPOc" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                          
                                             <asp:BoundField DataField="FechaEmision" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center"  DataFormatString="{0:dd/MM/yyyy}" /> 
                                                                                                                                                                     
                                              <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Beneficiario" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblBeneficiarioOc" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                           </asp:Panel>
                                    </td>
                                    </tr>
                   </table>
                               
                                </asp:Panel>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <table width="100%">
         <tr>
            <td class="SubTituloCelda">
                PROGRAMACIONES REALIZADAS
            </td>
        </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_ProgramacionPago_Grilla" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_ProgramacionPago" runat="server" AutoGenerateColumns="False"
                                        Width="100%" DataKeyNames="idBanco,IdCuentaBancaria,IdProgramacionPago">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Seleccionar" ShowSelectButton="True" />
                                            <asp:BoundField DataField="FechaPagoProg" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha de Pago" ItemStyle-Font-Bold="true"
                                                ItemStyle-ForeColor="Red" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda_MontoProg" runat="server" Font-Bold="true" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMontoProg" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"MontoPagoProg","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProgramacionPago")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MedioPago" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Medio de Pago" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Banco" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Banco" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NroCuentaBancaria" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Cuenta Bancaria" ItemStyle-HorizontalAlign="Center" />
                                             <asp:BoundField DataField="MonedaNew" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo Moneda" ItemStyle-HorizontalAlign="Center" />
                                                 <asp:BoundField DataField="TipoCambio" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo de Cambio" ItemStyle-HorizontalAlign="Center" />
                                                
                                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento Ref." ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbldocreferencia" runat="server" Font-Bold="true" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumentoRef")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detracción" >
                                            <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:RadioButton Enabled="false" GroupName="detraccion" id="rbIdEsDetraccion" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"idEsDetraccion") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Retención">
                                            <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:RadioButton Enabled="false" GroupName="retencion" id="rbIdEsRetencion" runat="server"  Checked='<%#DataBinder.Eval(Container.DataItem,"idEsRetencion") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Emitir Cheque">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnCrearCheque" ImageUrl="~/Imagenes/Añadir.png" runat="server" Enabled="false" Visible="false"
                                                    ToolTip="Emitir Cheque." CommandName="crear_cheque" CommandArgument='<%# Container.DataItemIndex %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar Doc." CommandArgument="Cancelar" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                    <asp:HiddenField ID="hddIndiceProgramacion" runat="server" Value="-1" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdMovCuenta_CXP" runat="server" />
                <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                                <asp:HiddenField ID="hddSaldoRQ" runat="server" />
</div>

<div id="capaDocProvisionados" style="width:800px;border:2px solid blue;display:none;top:100px;position:fixed;background-color:White;left:30px;z-index:2">
    <div style="width:100%" align="right">
        <img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar" onclick="return(ocultarCapa());" />
    </div>
    <div style="width:100%">
        <table>
            <tr>
                <td>
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Nro. Documento</asp:ListItem>
                        <asp:ListItem Value="1">Por Fecha de Emisión</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr runat="server" id="trFiltroNroDocumento" visible="true">
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Serie: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtnroSerieProvision" runat="server" Text="" Width="80px"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Número: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtNumeroProvision" runat="server" Text="" Width="80px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarxNumeroProvision" runat="server" Text="Buscar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" id="trFiltroFecha" visible="false">
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Desde: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtDesde" runat="server" Text="" Width="80px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendartxtDesde" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtDesde">
                                        </cc1:CalendarExtender>
                            </td>
                            <td class="Texto">
                                Hasta: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtHasta" runat="server" Text="" Width="80px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendartxtHasta" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtHasta">
                                        </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarxFecha" runat="server" Text="Buscar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td>
                    <asp:GridView ID="gvProvisiones" runat="server" Width="100%" AutoGenerateColumns="false" EmptyDataText="No se encontraron documentos provisionados"
                    DataKeyNames="IdDocumento">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <RowStyle CssClass="GrillaRow" />
                    <EditRowStyle CssClass="Grilla_Seleccion" />
                    <Columns>
                    <asp:TemplateField ShowHeader="False">
                        
                                 <ItemTemplate>
                                 <input id="ckbSeleccionar" type="checkbox" onclick="return(SelectProvisiones());" />                                     
                                 </ItemTemplate>
                                 <HeaderStyle VerticalAlign="Middle" />
                                 <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                             </asp:TemplateField>                 
                    <asp:BoundField DataField="regNombre" HeaderText="Proveedor" />
                    <asp:BoundField DataField="Codigo" HeaderText="Número Doc." /> 
                    <asp:BoundField DataField="FechaEmision" HeaderText="Fec. Emision" />
                    <asp:BoundField DataField="NomTipoOperacion" HeaderText="Tipo. Ope." />
                    <asp:BoundField DataField="NomMoneda" HeaderText="Moneda" />
                    <asp:BoundField DataField="SubTotal" HeaderText="SubTotal" />
                    <asp:BoundField DataField="IGV" HeaderText="i.g.v" />
                    <asp:BoundField DataField="Total" HeaderText="Total" />
                    <asp:BoundField DataField="Detraccion" HeaderText="Detracción" />
                    <asp:BoundField DataField="TotalAPagar" HeaderText="Total a Pagar" />
                    </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="capaCrearCuentas" style="width:800px;display:none;background-color:White;top:150px;left:30px;position:absolute;border:3px solid blue;z-index:2">
<div style="width:100%">
    <div style="float:left">
        <asp:Button id="btnGuardarCuentaBancaria" runat="server" CssClass="btnGuardar" Text="Guardar"/>
    </div>
    <div style="float:right; height: 15px;">
        <img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar." style="float:right" onclick="return(ocultarCapaCuentaDetraccion());" />
    </div>
</div>
<div>
    <table>
        <tr>
            <td  class="Texto" style="font-weight:bold;text-align:right" >
                Banco:</td>
            <td>
            <asp:DropDownList ID="cboBanco" runat="server" >
            </asp:DropDownList>
            </td>
            
            <td class="Texto" style="font-weight:bold;text-align:right" >
                Moneda:</td>
            <td >
            <asp:DropDownList ID="cboMoneda" runat="server" >
            </asp:DropDownList>
            </td>
            <td class="Texto" style="font-weight:bold;text-align:right" >
                Nro. Cuenta Bancaria:</td>
            <td>
            <asp:TextBox ID="txtNumero" runat="server" Width="200px" ></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
         <td class="Texto" style="font-weight:bold;text-align:right" >
                Cuenta Contable:</td><td>
            <asp:TextBox ID="txtCtaContable" runat="server" Width="100%" ></asp:TextBox>
            </td >
            <td class="Texto" style="font-weight:bold;text-align:right">
                    Swift Bancario:
            </td>
            <td >
            <asp:TextBox ID="txtswiftbanca" runat="server" Text=""></asp:TextBox>
            </td>
            
            <td class="Texto" style="font-weight:bold;text-align:right">
             Nro. Cuenta Interbancaria:
            </td>
            
            <td> 
            <asp:TextBox ID="txtcuentaInter" runat="server"  Width="200px">
            </asp:TextBox></td>
        </tr>
    </table>
</div>
</div>
<div id="capaEmitirCheque" style="width:1000px;border:2px solid blue;background-color:White;z-index:3;position:absolute;display:none;top:180px;left:30px;border-radius:6px">
<div style="width:100%">
    <div style="float:right; height: 15px;">
        <img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar." style="float:right" onclick="return(ocultarcapaEmitirCheque());"/>
    </div>
</div>
<div style="width:100%" class="TituloCelda">
    <span>EMITIR CHEQUE</span>
</div>
<div>
    <table>
        <tr>
            <td class="Texto">
                Banco:
            </td>
            <td>
                <asp:DropDownList ID="ddlBancoEmitirCheque" runat="server" Width="150px" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">Cuenta Bancaria:</td>
            <td>
                <asp:DropDownList ID="ddlCuentaBancariaEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">
                Estado del Documento
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoDocEmitirCheque" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Serie / Numeración:
            </td>
            <td>
                <asp:DropDownList ID="ddlSerieNumeracionEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">Nro Cheque:</td>
            <td>
                <asp:TextBox ID="ddlNroChequeEmitirCheque" runat="server" ></asp:TextBox>
            </td>
            <td class="Texto">
                Estado de entrega:
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoEntregaEmitirCheque" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Monto: 
            </td>
            <td>
                <asp:TextBox ID="txtMontoEmitirCheque" runat="server" Text="0" Enabled="false"></asp:TextBox>
            </td>
            <td class="Texto">
                Nro voucher:
            </td>
            <td class="Texto">
                <asp:TextBox ID="txtNroVoucherEmitirCheque" runat="server" Text=""></asp:TextBox>
            </td>
            <td class="Texto">
                Estado de Cancelación:
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoCancelacionEmitirCheque" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Fecha de Cobro:
            </td>
            <td>
                <asp:TextBox ID="txtFEchaCobroEmitirCheque" runat="server"></asp:TextBox>
                <cc1:calendarextender ID="txtFEchaCobroEmitirCheque_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFEchaCobroEmitirCheque">
                    </cc1:calendarextender>
            </td>
            <td class="Texto">
                Fecha de Emisión:
            </td>
            <td>
                <asp:TextBox ID="txtFechaEmisionEmitirCheque" runat="server"></asp:TextBox>
                <cc1:calendarextender ID="txtFechaEmisionEmitirCheque_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFechaEmisionEmitirCheque">
                    </cc1:calendarextender>
            </td>
            <td colspan="2" align="center">
                <asp:Button ID="btnGuardarEmitirCheque" runat="server" Text="Guardar" CssClass="btnGuardar" />
            </td>
        </tr>
        </table>
        <table width="100%">
            <tr>
                <td>
                    <asp:GridView ID="gvEmisionCheques" runat="server" Width="100%" ShowHeader="true" AutoGenerateColumns="false">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <Columns>
                            <asp:BoundField HeaderText="Banco" DataField="banco_nombre" />
                            <asp:BoundField HeaderText="Cuenta Bancaria" DataField="ctaBancaria_nombre" />
                            <asp:BoundField HeaderText="Serie" ItemStyle-ForeColor="Red" DataField="Serie" />
                            <asp:BoundField HeaderText="Nro. Cheque" ItemStyle-ForeColor="Red" DataField="ChequeNumero" />
                            <asp:BoundField HeaderText="Mon." DataField="NomMoneda" />
                            <asp:BoundField HeaderText="Monto" DataField="Monto" />
                            <asp:BoundField HeaderText="Beneficiario" DataField="Beneficiario" />
                            <asp:BoundField HeaderText="Fecha a Cobrar" ItemStyle-ForeColor="Red" DataField="FechaACobrar" />
                            <asp:BoundField HeaderText="Fecha de Emisión" ItemStyle-ForeColor="Red" DataField="FechaEmision" />
                            <asp:BoundField HeaderText="Est. Documento" DataField="NomEstadoDocumento" />
                            <asp:BoundField HeaderText="Est. Entrega" DataField="NomEstadoEntregado" />
                            <asp:BoundField HeaderText="Est. Cancelación" DataField="NomEstadoCancelacion" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnImprimir" runat="server" Text="Imprimir"/>
                                    <asp:Button ID="btnVoucher" runat="server" Text="Voucher"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
</div>
</div>
    

                <script language="javascript" type="text/javascript">

                    function SelectProvisiones() {
                        var grillaOrigen = document.getElementById('<%=gvProvisiones.ClientID %>');
                        var grillaDestino = document.getElementById('<%=GV_DocumentosProvisionados.ClientID %>');
                        var seleccion = false;
                        for (var i = 1; i < grillaOrigen.rows.length; i++) {
                            if (grillaOrigen.rows[i].cells[0].getElementsByTagName('INPUT')[0].checked) {
                                var tbod = grillaDestino.rows[0].parentNode;
                                var newRow = grillaOrigen.rows[i].cloneNode(true);
                                tbod.appendChild(newRow);
                                grillaOrigen.rows[i].style.display = "none";
                                break;
                            }
                        }
                        return false;
                    }
                    function mostrarcapaEmitirCheque() {
                        xcapaBase = document.getElementById('capaBase');
                        xcapaBase.style.display = 'block';
                        xcapa = document.getElementById('capaEmitirCheque');
                        xcapa.style.display = 'block';
                        return false;
                    }
                    function ocultarcapaEmitirCheque() {
                        xcapaBase = document.getElementById('capaBase');
                        xcapaBase.style.display = 'none';
                        xcapa = document.getElementById('capaEmitirCheque');
                        xcapa.style.display = 'none';
                        return false;
                    }

                    function mostrarCrearCuentaDetraccion() {
                        xcapaBase = document.getElementById('capaBase');
                        xcapaBase.style.display = 'block';
                        xcapa = document.getElementById('capaCrearCuentas');
                        xcapa.style.display = 'block';
                        return false;
                    }
                    function ocultarCapaCuentaDetraccion() {
                        xcapaBase = document.getElementById('capaBase');
                        xcapaBase.style.display = 'none';
                        xcapa = document.getElementById('capaCrearCuentas');
                        xcapa.style.display = 'none';
                        return false;
                    }

                    function mostrarProvisiones() {
                        xcapaBase = document.getElementById('capaBase');
                        xcapaBase.style.display = 'block';
                        xcapa = document.getElementById('capaDocProvisionados');
                        xcapa.style.display = 'block';
                        return false;
                    }

                    function ocultarCapa() {
                        xcapaBase = document.getElementById('capaBase');
                        xcapaBase.style.display = 'none';
                        xcapa = document.getElementById('capaDocProvisionados');
                        xcapa.style.display = 'none';
                        return false;
                    }

                    function valOnClick_btnGuardar() {
                        var tipocambio = document.getElementById('<%=txtTipoCambio.ClientID %>');
                        var txtMonto = document.getElementById('<%=txtMontoProg.ClientID%>');
                        if (isNaN(parseFloat(txtMonto.value)) || txtMonto.value.length <= 0 || parseFloat(txtMonto.value) <= 0) {
                            alert('INGRESE UN VALOR VÁLIDO. NO SE PERMITE LA OPERACIÓN.');
                            txtMonto.select();
                            txtMonto.focus();
                            return false;
                        }
                        if (isNaN(parseFloat(tipocambio.value)) || parseFloat(tipocambio.value) <= 0) {
                            alert("El tipo de cambio no puede ser igual o menor a 0.");
                            tipocambio.select();
                            tipocambio.focus();
                            return false;
                        }
                        return confirm('Desea continuar con la Operación ?');
                    }

                    function valOnClick_btnEliminar() {
                        return confirm('Desea continuar con la Operación ?');
                    }
                </script>
</asp:Content>

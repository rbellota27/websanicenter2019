﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmProgramacionProvision.aspx.vb" Inherits="APPWEB.frmProgramacionProvision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
    <div style="width:100%;">        
    <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
        <span>REQUERIMIENTO</span>            
    </div>
    <div style="width:100%;float:left">
<asp:GridView ID="GV_MovCuenta_CXP" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="IdDocumento,IdProveedor"
                PageSize="50">
                <HeaderStyle CssClass="GrillaHeader" />
                <Columns>
                    <asp:BoundField DataField="TipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                        ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                            <asp:HiddenField ID="hddIdMovCuenta_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCtaPP")%>' />
                            <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />
                            <asp:HiddenField ID="hddIdUsuario" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuario")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Proveedor" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblProveedor" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Proveedor")%>'></asp:Label>
                            <asp:HiddenField ID="hddIdProveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProveedor")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Tienda" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                                <asp:Label ID="lblTienda" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                                <asp:Label ID="lblfechaEmision" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaEmision","{0:d}")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Fecha Vcto." ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblfechaVencimiento" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaVenc","{0:d}")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>             
                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                                <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                                <asp:Label ID="lblMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Monto","{0:F3}")%>'
                                Font-Bold="true"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Deuda" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblMoneda_Saldo" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                            <asp:Label ID="lblSaldo" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Saldo","{0:F3}")%>'></asp:Label>
                        </ItemTemplate>
                           <FooterTemplate>
                              <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                              <asp:Label ID="lblMontoTotalAgregados" runat="server" Text="0" ForeColor="White" Font-Bold="true" ></asp:Label>
                           </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="GrillaFooter" />
                <PagerStyle CssClass="GrillaPager" />
                <RowStyle CssClass="GrillaRow" />
                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                <EditRowStyle CssClass="GrillaEditRow" />
            </asp:GridView>
</div><!--Aca termina la grilla requerimiento-->
    <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
        <span>CUENTA BANCARIA - PROVEEDOR </span>
    </div>

    <div style="width:100%;float:left"> 
        <asp:GridView ID="dgvCuentaBancaria" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataRowStyle-Font-Bold="true"
            EmptyDataText="Sin datos" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center" >
            <Columns>                  
                                                
            <asp:BoundField DataField="Empresa" HeaderText="Empresa" >
            </asp:BoundField>
            <asp:BoundField DataField="Banco" HeaderText="Banco" NullDisplayText="0" />
            <asp:BoundField DataField="MonSimbolo" HeaderText="Moneda" NullDisplayText="0" />
            <asp:BoundField DataField="NroCuentaBancaria" HeaderText="Numero" NullDisplayText="0" />
            <asp:BoundField DataField="esCuentaDetraccion" HeaderText="Cta. Detracción" NullDisplayText="---" />
            <asp:BoundField DataField="Estado" HeaderText="Estado" NullDisplayText="0" />
                                                  
            </Columns>

            <AlternatingRowStyle CssClass="GrillaRowAlternating" />    
            <FooterStyle CssClass="GrillaFooter" />
            <HeaderStyle CssClass="GrillaHeader" />
            <PagerStyle CssClass="GrillaPager" />
            <RowStyle CssClass="GrillaRow" />                            
       </asp:GridView>
    </div><!--Aquin termina div cuenta bancaria - proveedor-->

    <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
        <span>PROGRAMACIÓN DE PAGOS</span>
    </div>

    <div style="width:100%;float:left;margin-top:5px;margin-bottom:5px">
    <table>
        <tr>
            <td>
                <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" Width="70px" ToolTip="Nuevo" />
            </td>
            <td>
                <asp:Button ID="btnEditar" runat="server" Text="Editar" Width="70px" ToolTip="Editar Programación de Pago" />
            </td>
            <td>
                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" Width="70px" ToolTip="Eliminar Programación de Pago" OnClientClick="return(  valOnClick_btnEliminar() );" />
            </td>
            <td>
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="70px" ToolTip="Registrar Programación de Pago" OnClientClick="return(  valOnClick_btnGuardar() );" />
            </td>
            <td>
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="70px" ToolTip="Cancelar" />
            </td>
            <td>
                <asp:Button ID="btnAgregarDocProvisionados" runat="server" Text="Doc. Provisionados" OnClientClick="return(mostrarProvisiones());" Visible="false"/>
            </td>
            <td>
                <span class="Texto">Tipo Agente : </span>
                <asp:DropDownList ID="ddlTipoAgente" AutoPostBack="true" runat="server" DataValueField="IdAgente" DataTextField="ag_Nombre"></asp:DropDownList>
            </td>
            <td>
                <img id="btnCrearCuentaDetraccion" src="../Imagenes/ctrlShiftP.png" alt="" title="Crear Cuenta Bancaria Detracción." onclick="return(mostrarCrearCuentaDetraccion());" runat="server" visible="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkEsAdelanto" runat="server" Text="Estoy programando un anticipo (<b>Proveedores</b>)." AutoPostBack="true"
                ToolTip="Habilitar esta opción solo si no se relacionará con un documento valor" /><br />

                <asp:CheckBox ID="chkEsCtaPorRendir" runat="server" Text="Estoy programando una cuenta por rendir (<b>Trabajadores</b>)." AutoPostBack="true"
                ToolTip="Habilitar esta opción solo si no se relacionará con un documento valor." />
            </td>
            <td>
                <asp:GridView ID="gvConceptoReferencia" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField HeaderText="Concepto">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hddidConcepto" runat="server" Value='<%# Eval("id") %>' />
                                        <asp:Label ID="lblNombreConcepto" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                            </Columns>
                 </asp:GridView>
            </td>
        </tr>
    </table>
    </div> <!--Aqui termina el div de botones de acciones sobre la programación-->
    <!--Formulario-->
    <div style="width:100%;float:left">
        <asp:Panel id="Panel_ProgramacionPago" runat="server" Enabled="false">
        <table>
            <tr>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Fecha de Pago:
                </td>
                <td>
                    <asp:TextBox ID="txtFecha_ProgPago" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );" AutoPostBack="true"
                                                                Width="80px"></asp:TextBox>                    
                    <cc1:calendarextender ID="txtFecha_ProgPago_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFecha_ProgPago">
                    </cc1:calendarextender>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Medio de Pago:
                </td>
                <td>
                    <asp:DropDownList ID="cboMedioPago_Prog" runat="server" Width="170px"></asp:DropDownList>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Banco:
                </td>
                <td>
                    <asp:DropDownList ID="cboBanco_Prog" runat="server" AutoPostBack="true" Width="180px"></asp:DropDownList>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Cuenta Bancaria:
                </td>
                <td>
                    <asp:DropDownList ID="cboCuentaBancaria_Prog" runat="server" AutoPostBack="true"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Tipo Moneda:
                </td>
                <td>
                    <asp:DropDownList ID="cboTipoMoneda" runat="server" Enabled="false"></asp:DropDownList>
                </td>
                <td colspan="6">
                    <table>
                        <tr>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                            Monto
                            </td>
                            <td>
                            <asp:Label ID="lblMoneda_MontoProg" runat="server" Text="S/." CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                <asp:TextBox ID="txtMontoProg" Text="0" Font-Bold="true" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                                            onFocus=" return( aceptarFoco(this)  ); " onblur=" return( valBlur(event)  ); " runat="server"></asp:TextBox>
                            </td>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                                <asp:Label ID="lblTipoCambio" Text="Tipo de Cambio:" runat="server" Visible="false"> </asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTipoCambio" Visible="false"  runat="server" Text="0" Font-Bold="true" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); " ></asp:TextBox>
                            </td>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                                <asp:Label ID="Label2" runat="server" Text="Monto a Pagar:" Visible="false"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblResultadoDolares" runat="server" Text="US$." Font-Bold="true" Visible="false" Style="color:Red"></asp:Label>

                                <asp:TextBox ID="txtResultadoTipoCambio" runat="server" Text="0" AutoPostBack="true" Visible="false" ReadOnly ="true" ></asp:TextBox>
                                <asp:Button  ID="btncalcularTC" runat="server"  Text="Calcular Monto" AutoPostBack="true" Visible="false"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td colspan="8" style="width:100%">
                <asp:TextBox ID="txtObservaciones_Prog" runat="server" Text="" TextMode="MultiLine" Width="100%" Rows="3"></asp:TextBox>
            </td>
            </tr>
        </table>
        </asp:Panel>
    </div>
    <!--Aca termina el formulario-->

    <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
        <span>DOCUMENTOS PROVISIONADOS AGREGADOS</span>
    </div>
    
    <div style="width:100%">
        <table style="width:100%">
            <tr>
                <td>
                    <asp:GridView ID="GV_DocumentosProvisionados" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="true" Width="100%">
          <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lkbtnQuitar" Text="Quitar" OnClick="evento_quitar" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                    <asp:HiddenField ID="hddidDocumento" runat="server" Value='<%# Eval("idDocumento") %>' />
                </ItemTemplate>
                <HeaderStyle VerticalAlign="Middle" />
                <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>                         
            <asp:BoundField DataField="regNombre" HeaderText="Proveedor" />
            <asp:BoundField DataField="Codigo" HeaderText="Número Doc." /> 
            <asp:BoundField DataField="FechaEmision" HeaderText="Fec. Emision" />
            <asp:BoundField DataField="NomTipoOperacion" HeaderText="Tipo. Ope." />
            <asp:BoundField DataField="NomMoneda" HeaderText="Moneda" />
            <asp:BoundField DataField="SubTotal" HeaderText="SubTotal" />
            <asp:BoundField DataField="IGV" HeaderText="i.g.v" />
            <asp:BoundField DataField="Total" HeaderText="Total" />
            <asp:BoundField DataField="Detraccion" HeaderText="Detracción" />
            <asp:TemplateField HeaderText="Total a Pagar">
                <ItemTemplate>
                    <asp:Label ID="lblMonedaPrincipal" runat="server" Text='<%# Eval("NomMoneda") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                    <asp:Label ID="lblTotalAPagar" runat="server" Text='<%# Eval("TotalAPagar") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="TotalAPagar" HeaderText="Total a Pagar" />--%>
        </Columns>
            <HeaderStyle CssClass="GrillaHeader" />
            <FooterStyle CssClass="GrillaFooter" />
            <PagerStyle CssClass="GrillaPager" />
            <RowStyle CssClass="GrillaRow" />
            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
            <SelectedRowStyle CssClass="GrillaSelectedRow" />
            <EditRowStyle CssClass="GrillaEditRow" />
        </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="float:right">
                    <asp:TextBox ID="txtTotalSaldo" runat="server" Text="0" Visible="false"></asp:TextBox>
                </td>
            </tr>
        </table>
       

    <!--Programaciones realizadas-->
    <table style="width:100%">
         <tr>
            <td class="SubTituloCelda">
                PROGRAMACIONES REALIZADAS
            </td>
        </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_ProgramacionPago_Grilla" runat="server">
                        <table style="white-space:100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_ProgramacionPago" runat="server" AutoGenerateColumns="False"
                                        Width="100%" DataKeyNames="idBanco,IdCuentaBancaria,IdProgramacionPago">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Seleccionar" ShowSelectButton="True" />
                                            <asp:BoundField DataField="FechaPagoProg" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha de Pago" ItemStyle-Font-Bold="true"
                                                ItemStyle-ForeColor="Red" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda_MontoProg" runat="server" Font-Bold="true" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMontoProg" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"MontoPagoProg","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProgramacionPago")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MedioPago" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Medio de Pago" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Banco" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Banco" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NroCuentaBancaria" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Cuenta Bancaria" ItemStyle-HorizontalAlign="Center" />
                                             <asp:BoundField DataField="MonedaNew" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo Moneda" ItemStyle-HorizontalAlign="Center" />
                                                 <asp:BoundField DataField="TipoCambio" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo de Cambio" ItemStyle-HorizontalAlign="Center" />
                                                
                                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento Ref." ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbldocreferencia" runat="server" Font-Bold="true" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumentoRef")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detracción" >
                                            <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:RadioButton Enabled="false" GroupName="detraccion" id="rbIdEsDetraccion" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"idEsDetraccion") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Retención">
                                            <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:RadioButton Enabled="false" GroupName="retencion" id="rbIdEsRetencion" runat="server"  Checked='<%#DataBinder.Eval(Container.DataItem,"idEsRetencion") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Emitir Cheque">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnCrearCheque" ImageUrl="~/Imagenes/Añadir.png" runat="server" Enabled="false" Visible="false"
                                                    ToolTip="Emitir Cheque." CommandName="crear_cheque" CommandArgument='<%# Container.DataItemIndex %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                    <asp:HiddenField ID="hddIndiceProgramacion" runat="server" Value="-1" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hddIdPersona" runat="server" />
        <asp:HiddenField ID="hddIdMovCuenta_CXP" runat="server" />
        <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" />
        <asp:HiddenField ID="hddFrmModo" runat="server" />
        <asp:HiddenField ID="hddIdDocumento" runat="server" />
        <asp:HiddenField ID="hddSaldoRQ" runat="server" />
    </div>
</div>
    <!--Capa Provisiones-->
    <div id="capaDocProvisionados" style="width:1000px;height:500px;border:2px solid blue;display:none;top:100px;position:absolute;background-color:White;
        left:30px;z-index:2;overflow:scroll" onclick="return capaDocProvisionados_onclick()">
    <div style="width:100%">
        <img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar" onclick="return(ocultarCapa());" />
    </div>
    <div style="width:100%">
        <table style="width:100%">
            <tr>
                <td>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="Texto" RepeatDirection="Horizontal" AutoPostBack="true" >
                    <asp:ListItem Text="Por Nro. Documento" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Por Fecha de Emisión" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Por Proveedor" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                    <%--<asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Nro. Documento</asp:ListItem>
                        <asp:ListItem Value="1">Por Fecha de Emisión</asp:ListItem>
                    </asp:RadioButtonList>--%>
                </td>
            </tr>
            <tr id="trFiltroNroDocumento" runat="server" style="display:block">
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Serie: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtnroSerieProvision" runat="server" Text="" Width="80px"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Número: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtNumeroProvision" runat="server" Text="" Width="80px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarxNumeroProvision" runat="server" Text="Buscar" OnClick="btnBuscarProvision_Click" 
                                OnClientClick="return(validarRucFiltro(this));" UseSubmitBehavior="false" />
                                <span id="spnMensajeFiltroNroDoc" style="color:Red;font-weight:bold"></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFiltroFecha" runat="server">
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Desde: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtDesde" runat="server" Text="" Width="80px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendartxtDesde" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtDesde">
                                        </cc1:CalendarExtender>
                            </td>
                            <td class="Texto">
                                Hasta: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtHasta" runat="server" Text="" Width="80px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendartxtHasta" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtHasta">
                                        </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarxFecha" runat="server" Text="Buscar" OnClick="btnBuscarProvision_Click" 
                                OnClientClick="return(validarRucFiltro(this));" UseSubmitBehavior="false" />
                                <span id="Span1" style="color:Red;font-weight:bold"></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFiltroProveedor" runat="server">
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtFiltroRuc" runat="server" placeholder="R.U.C"></asp:TextBox>
                                <asp:Button ID="btnBuscarPoProveedor" runat="server" Text="Buscar" CssClass="btnBuscar" OnClick="btnBuscarProvision_Click" 
                                OnClientClick="return(validarRucFiltro(this));" UseSubmitBehavior="false" />
                                <span id="spnFiltroRuc" style="color:Red;font-weight:bold"></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td>
                    <asp:GridView ID="gvProvisiones" runat="server" Width="100%" AutoGenerateColumns="false" EmptyDataText="No se encontraron documentos provisionados"
                    DataKeyNames="IdDocumento" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-Font-Bold="true" >
                    <HeaderStyle CssClass="GrillaHeader" />
                    <RowStyle CssClass="GrillaRow" />
                    <EditRowStyle CssClass="Grilla_Seleccion" />
                    <Columns>
                    <asp:TemplateField ShowHeader="False">
                                 <ItemTemplate>
                                 <asp:CheckBox ID="chkSeleccionar" runat="server" AutoPostBack="true" OnCheckedChanged="SeleccionarGuias" />
<%--                                 <input type="checkbox" name="sleccionar" onclick="return(SelectProvisiones());" />--%>
                                 </ItemTemplate>
                                 <HeaderStyle VerticalAlign="Middle" />
                                 <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                             </asp:TemplateField>                 
                    <asp:BoundField DataField="regNombre" HeaderText="Proveedor" />
                    <asp:BoundField DataField="Codigo" HeaderText="Número Doc." /> 
                    <asp:BoundField DataField="FechaEmision" HeaderText="Fec. Emision" />
                    <asp:BoundField DataField="NomTipoOperacion" HeaderText="Tipo. Ope." />
                    <asp:BoundField DataField="NomMoneda" HeaderText="Moneda" />
                    <asp:BoundField DataField="SubTotal" HeaderText="SubTotal" />
                    <asp:BoundField DataField="IGV" HeaderText="i.g.v" />
                    <asp:BoundField DataField="Detraccion" HeaderText="Detracción" />
                    <asp:BoundField DataField="Total" HeaderText="Total" />
                    <asp:BoundField DataField="TotalAPagar" HeaderText="Total a Pagar" />
                    </Columns>
                    </asp:GridView>
                </td>
            </tr>

        </table>
    </div>
</div>

    <!--Capa Crear cuentas Bancaria-->
    <div id="capaCrearCuentas" style="width:800px;display:none;background-color:White;top:150px;left:30px;position:absolute;border:3px solid blue;z-index:2">
<div style="width:100%">
    <div style="float:left">
        <asp:Button id="btnGuardarCuentaBancaria" runat="server" CssClass="btnGuardar" Text="Guardar"/>
    </div>
    <div style="float:right; height: 15px;">
        <img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar." style="float:right" onclick="return(ocultarCapaCuentaDetraccion());" />
    </div>
</div>
<div>
    <table>
        <tr>
            <td  class="Texto" style="font-weight:bold;text-align:right" >
                Banco:</td>
            <td>
            <asp:DropDownList ID="cboBanco" runat="server" >
            </asp:DropDownList>
            </td>
            
            <td class="Texto" style="font-weight:bold;text-align:right" >
                Moneda:</td>
            <td >
            <asp:DropDownList ID="cboMoneda" runat="server" >
            </asp:DropDownList>
            </td>
            <td class="Texto" style="font-weight:bold;text-align:right" >
                Nro. Cuenta Bancaria:</td>
            <td>
            <asp:TextBox ID="txtNumero" runat="server" Width="200px" ></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
         <td class="Texto" style="font-weight:bold;text-align:right" >
                Cuenta Contable:</td><td>
            <asp:TextBox ID="txtCtaContable" runat="server" Width="100%" ></asp:TextBox>
            </td >
            <td class="Texto" style="font-weight:bold;text-align:right">
                    Swift Bancario:
            </td>
            <td >
            <asp:TextBox ID="txtswiftbanca" runat="server" Text=""></asp:TextBox>
            </td>
            
            <td class="Texto" style="font-weight:bold;text-align:right">
             Nro. Cuenta Interbancaria:
            </td>
            
            <td> 
            <asp:TextBox ID="txtcuentaInter" runat="server"  Width="200px">
            </asp:TextBox></td>
        </tr>
    </table>
</div>
</div>

    <!--Capa Emitir Cheque-->
    <div id="capaEmitirCheque" style="width:1000px;border:2px solid blue;background-color:White;z-index:3;position:absolute;display:none;top:180px;left:30px;border-radius:6px">
<div style="width:100%">
    <div style="float:right; height: 15px;">
        <img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar." style="float:right" onclick="return(ocultarcapaEmitirCheque());"/>
    </div>
</div>
<div style="width:100%" class="TituloCelda">
    <span>EMITIR CHEQUE</span>
</div>
<div>
    <table>
        <tr>
            <td class="Texto">
                Banco:
            </td>
            <td>
                <asp:DropDownList ID="ddlBancoEmitirCheque" runat="server" Width="150px" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">Cuenta Bancaria:</td>
            <td>
                <asp:DropDownList ID="ddlCuentaBancariaEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">
                Estado del Documento
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoDocEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Serie / Numeración:
            </td>
            <td>
                <asp:DropDownList ID="ddlSerieNumeracionEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">Nro Cheque:</td>
            <td>
                <asp:TextBox ID="ddlNroChequeEmitirCheque" runat="server" ></asp:TextBox>
            </td>
            <td class="Texto">
                Estado de entrega:
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoEntregaEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Monto: 
            </td>
            <td>
                <asp:TextBox ID="txtMontoEmitirCheque" runat="server" Text="0" Enabled="false"></asp:TextBox>
            </td>
            <td class="Texto">
                Nro voucher:
            </td>
            <td class="Texto">
                <asp:TextBox ID="txtNroVoucherEmitirCheque" runat="server" Text=""></asp:TextBox>
            </td>
            <td class="Texto">
                Estado de Cancelación:
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoCancelacionEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Fecha de Cobro:
            </td>
            <td>
                <asp:TextBox ID="txtFEchaCobroEmitirCheque" runat="server"></asp:TextBox>
                <cc1:calendarextender ID="txtFEchaCobroEmitirCheque_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFEchaCobroEmitirCheque">
                    </cc1:calendarextender>
            </td>
            <td class="Texto">
                Fecha de Emisión:
            </td>
            <td>
                <asp:TextBox ID="txtFechaEmisionEmitirCheque" runat="server"></asp:TextBox>
                <cc1:calendarextender ID="txtFechaEmisionEmitirCheque_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFechaEmisionEmitirCheque">
                    </cc1:calendarextender>
            </td>
            <td colspan="2" align="center">
                <asp:Button ID="btnGuardarEmitirCheque" runat="server" Text="Guardar" CssClass="btnGuardar" CommandName="guardar_cheque" OnClientClick="this.disabled=true;this.value='Procesando'" UseSubmitBehavior="false" />
            </td>
        </tr>
        </table>
        <table width="100%">
            <tr>
                <td>
                    <asp:GridView ID="gvEmisionCheques" runat="server" Width="100%" ShowHeader="true" AutoGenerateColumns="false">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <Columns>
                            <asp:BoundField HeaderText="Banco" DataField="banco_nombre" />
                            <asp:BoundField HeaderText="Cuenta Bancaria" DataField="ctaBancaria_nombre" />
                            <asp:BoundField HeaderText="Serie" ItemStyle-ForeColor="Red" DataField="Serie" />
                            <asp:BoundField HeaderText="Nro. Cheque" ItemStyle-ForeColor="Red" DataField="ChequeNumero" />
                            <asp:BoundField HeaderText="Mon." DataField="NomMoneda" />
                            <asp:BoundField HeaderText="Monto" DataField="Monto" />
                            <asp:BoundField HeaderText="Beneficiario" DataField="Beneficiario" />
                            <asp:BoundField HeaderText="Fecha a Cobrar" ItemStyle-ForeColor="Red" DataField="FechaACobrar" />
                            <asp:BoundField HeaderText="Fecha de Emisión" ItemStyle-ForeColor="Red" DataField="FechaEmision" />
                            <asp:BoundField HeaderText="Est. Documento" DataField="NomEstadoDocumento" />
                            <asp:BoundField HeaderText="Est. Entrega" DataField="NomEstadoEntregado" />
                            <asp:BoundField HeaderText="Est. Cancelación" DataField="NomEstadoCancelacion" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" OnClientClick="return PrintPanel();"/>
                                    <asp:Button ID="btnVoucher" runat="server" Text="Voucher"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
</div>
</div>

    <div id="capaImprimirVoucher" style="width:700px;border:2px solid blue;background-color:White;z-index:4;position:absolute;display:none;top:190px;left:30px">
    <asp:Panel ID="pnlContents" runat="server">
        <table style="width:100%;border-top-width: 1px">
            <tr style="padding-top:1px"> 
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%" colspan="3"><span>&nbsp&nbsp 01/02/2016</span></td>
                <td style="width:5%" colspan="3"><span>05/02/2016</span></td>
                <td style="width:5%" colspan="4"><span>400.00</span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
            </tr>
            <tr>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%" colspan="13">ALAN SAN MARTIN FRANK NOMBRE NOMBRE<span>3</span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
            </tr>
            <tr>
                <td style="width:5%" colspan="15"><span>CINCUENTA Y CUATRO CON 78/100 SOLES</span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
            </tr>
        </table>
        </asp:Panel>
    </div>

    <!--Capa tipo de Anticipos-->
    <div id="capaAnticipos" style="width:200px;border:2px solid blue;background-color:White;z-index:4;position:absolute;display:none;top:120px;left:100px">
        <div style="width:100%">
            <table>
                <tr class="TituloCelda">
                    <td><asp:Label ID="lblSubtitulo" runat="server" Text="ELEGIR TIPO DE ANTICIPO"></asp:Label></td>
                    <td><img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar." style="float:right" onclick="return(ocultarCapaConcepto());" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gvTipoAnticipos" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>                                    
                                        <asp:LinkButton ID="lbSeleccionar" runat="server" Text="Seleccionar" OnClick="lbSeleccionar_click" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Concepto">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hddidConcepto" runat="server" Value='<%# Eval("id") %>' />
                                        <asp:Label ID="lblNombreConcepto" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--Capa Asociar documentos con la detracción-->
    <div id="capaAsociarDetracciones" style="width:700px;border:2px solid blue;background-color:White;z-index:3;position:absolute;display:none;top:180px;left:30px">
        <div style="float:right; height: 15px;">
            <%--<img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar." style="float:right" onclick="offcapa('capaAsociarDetracciones');" />--%>
            <asp:ImageButton ID="imgBtnCerrar" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
        </div>
        <div style="width:100%">
            <table>
                <tr>
                    <td colspan="10">
                        <asp:Label ID="lblMensaje" ForeColor="Red" runat="server" Text="* Deberá marcar los documentos que estan afecto a detracción." Font-Bold="true"></asp:Label>
                        <span style="font-weight:bold">|</span>
                        <asp:LinkButton ID="lbContinuarGrabando" runat="server" Text="Continar Grabando" ForeColor="Blue" Font-Bold="true" Font-Italic="true" OnClientClick="return confirm('Desea continuar con la Operación ?');"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="10">
                        <asp:GridView ID="gv_docAsociadosDetraccion" runat="server" AutoGenerateColumns="false">
                        <RowStyle CssClass="GrillaRow" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="checkDetraccion" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="regNombre" HeaderText="Proveedor" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="Fec. Emision" />
                            <asp:BoundField DataField="Codigo" HeaderText="Nro. Documento" />
                            <asp:TemplateField HeaderText="Total a Pagar">
                            <ItemTemplate>
                                <asp:Label ID="lblMonedaPrincipal" runat="server" Text='<%# Eval("NomMoneda") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblTotalAPagar" runat="server" Text='<%# Eval("TotalAPagar") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        var bPreguntar = true;
        window.onbeforeunload = preguntarAntesDeSalir;
        var variableSession;
        function preguntarAntesDeSalir() {
            if (bPreguntar)
            variableSession = '<%= Session("ListaProgramacion") %>';
            variableSession = "";
            return "¿Seguro que quieres salir?";
        }

        function validarRucFiltro(boton) {
            var spnMensajeFiltro = document.getElementById('spnMensajeFiltroNroDoc');
            var spnMensajeFiltroRuc = document.getElementById('spnFiltroRuc');

            var radioButton = document.getElementById('<%= RadioButtonList1.ClientID %>');
            var radio = radioButton.getElementsByTagName('INPUT');
            var radioValor;
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    radioValor = parseInt(radio[i].value);
                }
            }

            switch (radioValor) {
                case 1: //si es filtro por número de documento
                    var txtSerie = document.getElementById('<%= txtnroSerieProvision.ClientID %>');
                    var txtNumero = document.getElementById('<%= txtNumeroProvision.ClientID %>');
                    
                    if (txtSerie.value.length == 0) {
                        txtSerie.focus();
                        txtSerie.select();
                        spnMensajeFiltro.innerHTML = "Nro de Serie Invalido";
                        return false;
                    }
                    if (txtNumero.value.length == 0) {
                        txtNumero.focus();
                        txtNumero.select();
                        spnMensajeFiltro.innerHTML = "Nro de Documento Invalido";
                        return false;
                    }
                    spnMensajeFiltro.innerHTML = "";
                    break;
                case 2: //si es filtro por proveedor
                    var nroRuc = document.getElementById('<%= txtFiltroRuc.ClientID %>');
                    
                    if (nroRuc.value.length == 0) {
                        spnMensajeFiltroRuc.innerHTML = "Nro Ruc Incorrecto";
                        return false;
                    }
                    spnMensajeFiltroRuc.innerHTML = "";
                    break;
                    
                default:
                    return false;
            }
            
            
            boton.disabled = true;
            boton.value = 'Procesando...';
            return true;
        }
        function ocultarCapaConcepto() {
            var check = document.getElementById('<%= chkEsAdelanto.ClientID %>');
            var grilla = document.getElementById('<%= gvConceptoReferencia.ClientID %>');
            if (grilla == null) {
                grilla.checked = false;
            }
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'none';
            xcapa = document.getElementById('capaAnticipos');
            xcapa.style.display = 'none';
            return false;
        }

        function mostrarCapaAnticipos() {
            //var check = document.getElementById('<%= chkEsAdelanto.ClientID %>');

            //if (check.checked) {
                xcapaBase = document.getElementById('capaBase');
                xcapaBase.style.display = 'block';
                xcapa = document.getElementById('capaAnticipos');
                xcapa.style.display = 'block';
            //} else {
                return false;
            //}
            //return false;
        }

        function PrintPanel() {
            var panel = document.getElementById("<%=pnlContents.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
        function ImprimeDiv() {
            var divToPrint = document.getElementById('capaImprimirVoucher');
            var newWin = window.open('', 'Print-Window', 'width=100,height=100');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
            setTimeout(function () { newWin.close(); }, 10);
        }
   

        function SelectProvisiones() {
            var grillaOrigen = document.getElementById("<%=gvProvisiones.ClientID %>");

            var grillaDestino = document.getElementById("<%=GV_DocumentosProvisionados.ClientID %>");
            var totalSumaAPagar = document.getElementById("td_Total");
            var sumaTotal = 0
            var sumaParcial = 0
            

            var seleccion = false;
            for (var i = 1; i < grillaOrigen.rows.length; i++) {
                if (grillaOrigen.rows[i].cells[0].getElementsByTagName('INPUT')[0].checked) {
                    var tbod = grillaDestino.rows[0].parentNode;
                    var newRow = grillaOrigen.rows[i].cloneNode(true);
                    tbod.appendChild(newRow);
                    grillaOrigen.rows[i].style.display = "none";

//                    if (isNaN(parseFloat(totalSumaAPagar.innerHTML))) {
//                        sumaParcial = 0;
//                    } else {
//                        sumaParcial = parseFloat(totalSumaAPagar.innerHTML);
//                    }

//                    sumaTotal = sumaParcial + parseFloat(grillaOrigen.rows[i].cells[10].innerHTML);
//                    totalSumaAPagar.innerHTML = sumaTotal;
                    break;
                }
            }
            grillaFinal = document.getElementById("<%=GV_DocumentosProvisionados.ClientID %>");
            return false;
        }

        function mostrarcapaEmitirCheque() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'block';
            xcapa = document.getElementById('capaEmitirCheque');
            xcapa.style.display = 'block';
            return false;
        }
        function ocultarcapaEmitirCheque() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'none';
            xcapa = document.getElementById('capaEmitirCheque');
            xcapa.style.display = 'none';
            return false;
        }

        function mostrarCrearCuentaDetraccion() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'block';
            xcapa = document.getElementById('capaCrearCuentas');
            xcapa.style.display = 'block';
            return false;
        }
        function ocultarCapaCuentaDetraccion() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'none';
            xcapa = document.getElementById('capaCrearCuentas');
            xcapa.style.display = 'none';
            return false;
        }

        function mostrarProvisiones() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'block';
            xcapa = document.getElementById('capaDocProvisionados');
            xcapa.style.display = 'block';
            return false;
        }

        function ocultarCapa() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'none';
            xcapa = document.getElementById('capaDocProvisionados');
            xcapa.style.display = 'none';
            return false;
        }

        function valOnClick_btnGuardar() {
        var comboAgente = document.getElementById('<%=ddlTipoAgente.ClientID %>');
        var idcomboAgente = comboAgente.options[comboAgente.selectedIndex].value;
        var checkboxAdelanto = document.getElementById('<%=chkEsAdelanto.ClientID %>')            
        //Valido que la grilla tenga al menos una fila
            var grilla = document.getElementById('<%=GV_DocumentosProvisionados.ClientID %>');
            var checkBox = document.getElementById('<%=chkEsAdelanto.ClientID %>');
            
            
            var tipocambio = document.getElementById('<%=txtTipoCambio.ClientID %>');
            var txtMonto = document.getElementById('<%=txtMontoProg.ClientID%>');
            if (isNaN(parseFloat(txtMonto.value)) || txtMonto.value.length <= 0 || parseFloat(txtMonto.value) <= 0) {
                alert('INGRESE UN VALOR VÁLIDO. NO SE PERMITE LA OPERACIÓN.');
                txtMonto.select();
                txtMonto.focus();
                return false;
            }
            if (isNaN(parseFloat(tipocambio.value)) || parseFloat(tipocambio.value) <= 0) {
                alert("El tipo de cambio no puede ser igual o menor a 0.");
                tipocambio.select();
                tipocambio.focus();
                return false;
            }

            if (grilla.rows.length == 1 && checkBox.checked == false) {
                alert("Deberá agregar como mínimo 1 o más documentos provisionados.");
                return false;
            } else {
                if (grilla.rows.length > 1) {
                    if (idcomboAgente == 0 || idcomboAgente==2 || idcomboAgente == 3) {
                        return confirm('Desea continuar con la operación?.');
                    } else {return true;}
            } else {
                return confirm('No se está relacionando documentos provisionados, esta programación se tomará como Adelanto. Desea continuar con la Operación ?');
            }
            }
//            return confirm('Desea continuar con la Operación ?');
        }

        function valOnClick_btnEliminar() {
            return confirm('Desea continuar con la Operación ?');
        }

    </script> 
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmVisorProvisiones.aspx.vb" Inherits="APPWEB.frmVisorProvisiones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="width:100%">
    <div>
        <table>
            <tr class="TituloCelda">
                <td colspan="4">
                    <span>BUSQUEDA DE DOCUMENTOS PROGRAMADOS - CANCELADOS</span>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Fecha Emisión:
                </td>
                <td>
                    <asp:TextBox ID="txtFechaInicio" runat="server"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtFechaVcto_Inicio_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                        </cc1:CalendarExtender>
                </td>
                 <td class="Texto">
                    Fecha Final:
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="TextBox1">
                                        </cc1:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td class="Texto" colspan="4">
                    <asp:RadioButtonList ID="rbEstadoDocumento" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Pendiente" Value="2" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Cancelado" Value="1"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtProveedorFiltro" runat="server" placeholder="PROVEEDOR" Width="100%"></asp:TextBox>
                </td> 
                <td>
                    <asp:TextBox ID="txtNroDocumentoFiltro" runat="server" placeholder="NRO DOCUMENTO" Width="100%"></asp:TextBox>
                </td> 
                <td align="right">
                    <asp:Button ID="btnbuscar" runat="server" Text="Buscar" CssClass="btnBuscar" />
                </td>
            </tr>

        </table>
    </div>
    <asp:GridView ID="gv_visorProvisiones" runat="server" 
        AutoGenerateColumns="False">
        <HeaderStyle CssClass="GrillaCabecera" />
        <RowStyle CssClass="GrillaRow" />
        <Columns>            
            <asp:BoundField HeaderText="Fecha Emisión" DataField="docFechaEmision" />            
            <asp:BoundField HeaderText="Código" DataField="codigo"/>
            <asp:BoundField HeaderText="Proveedor" DataField="proveedor"/>
            <asp:TemplateField HeaderText="Total Documento" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Bold="true" Text='<%# eval("totalFactura") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Nro Dias" DataField="nroDiasPlazo"/>
            <asp:BoundField HeaderText="Fecha Vencimiento" DataField="doc_fechaVEncimiento"/>
            <asp:BoundField HeaderText="Medio Pago" DataField="medioPago"/>
            <asp:BoundField HeaderText="Fecha Cancelación" DataField="doc_fechaCancelacion"/>
            <asp:BoundField HeaderText="Banco" DataField="banco"/>
            <asp:BoundField HeaderText="Nro Operación" DataField="nroOperacion"/>
            <asp:BoundField HeaderText="Retención (-)" DataField="retencion"/>
            <asp:TemplateField HeaderText="Monto">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Font-Bold="true" Text='<%# Eval("montoCancelado") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Nro Operación (DET)" DataField="nroOperacionDET"/>
            <asp:BoundField HeaderText="Monto Detraido" DataField="montoCanceladoDET"/>
            <asp:BoundField HeaderText="NroREquerimiento" DataField="nroRequerimiento"/>            
            <asp:BoundField HeaderText="Estado" DataField="estado"/>       
        </Columns>
    </asp:GridView>

</div>
</asp:Content>

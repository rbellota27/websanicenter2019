﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmConfiguracionParametros.aspx.vb" Inherits="APPWEB.frmConfiguracionParametros" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
        function valSave() {

            var grillactd = document.getElementById('<%=DGV_Parametros.ClientID %>');
            if (grillactd != null) {
                for (var i = 1; i < grillactd.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grillactd.rows[i];

                    if (rowElem.cells[2].children[0].value == '') {
                        alert('Ingrese un valor');
                        rowElem.cells[2].children[0].select();
                        rowElem.cells[2].children[0].focus();
                        return false;
                    }

                    if (isNaN(rowElem.cells[2].children[0].value) || parseFloat(rowElem.cells[3].children[0].value) <= 0) {
                        alert('Ingrese una valor válido mayor a cero.');
                        rowElem.cells[2].children[0].select();
                        rowElem.cells[2].children[0].focus();
                        return false;
                    }
                    if (isNaN(rowElem.cells[2].children[0].value)) {
                        alert('El valor ingresado no es correcto');
                        rowElem.cells[2].children[0].select();
                        rowElem.cells[2].children[0].focus();
                        return false;
                    }
                }
            }

        }
    </script>

    <asp:Panel ID="Panel_Parametros" runat="server" Width="994px">
        <table style="width: 100%">
            <tr>
                <td class="TituloCelda" style="height: 21px; width: 100%;">
                    Parametros Generales
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <asp:ImageButton ID="btnGuardar" runat="server" OnClientClick="return(valSave());"
                        ImageUrl="~/Imagenes/Guardar_B.JPG" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';" Enabled="False" />
                </td>
            </tr>
            <tr>
                <td class="LabelTdLeft">
                    &nbsp;&nbsp;&Aacute;rea: &nbsp;&nbsp;<asp:DropDownList ID="cboArea" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnAddParametro" runat="server" Text="Agregar Parametro" ToolTip="Agregar Nuevo Parametro"
                        Visible="False" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="DGV_Parametros" runat="server" AutoGenerateColumns="False" CellPadding="0"
                        RowStyle-VerticalAlign="Middle" Width="99%">
                        <Columns>
                            <asp:BoundField DataField="IdParametro" HeaderText="Id" />
                            <asp:TemplateField HeaderText="Area">
                                <ItemTemplate>
                                    <asp:DropDownList ID="dgv_CboArea"  Font-Bold ="true"  runat="server" DataValueField="Id" DataTextField="DescripcionCorta"
                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objArea") %>'>
                                    </asp:DropDownList>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Valor">
                                <ItemTemplate>
                                    <asp:TextBox ID="dgv_txtvalor" runat="server" Width="50px" onfocus="return(aceptarFoco(this));"  Font-Bold ="true" DataFormatString="{0:F2}"  ></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Descripcion">
                                <ItemTemplate>
                                    <asp:TextBox ID="dgv_txtdescripcion" runat="server" onKeyDown="return false" Width="600px"
                                        TextMode="MultiLine" Height="35px" Font-Bold="true"  ></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                
                            </asp:TemplateField>
                                                       
                            <asp:TemplateField HeaderText="Activos" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:CheckBox ID="dgv_Chb_Estado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"par_Estado") %>' />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Font-Bold="True" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <RowStyle CssClass="GrillaRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hddId" runat="server" Value="0" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

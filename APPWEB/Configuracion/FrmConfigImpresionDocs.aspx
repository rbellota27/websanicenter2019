<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmConfigImpresionDocs.aspx.vb" Inherits="APPWEB.FrmConfigImpresionDocs" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Configuracion de Impresion de Documentos
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            Documento
                        </td>
                        <td>
                            <asp:DropDownList ID="cboDocs" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            Tipo Reporte
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTImpresion" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnMostrar" runat="server" Text="Mostrar" />
                        </td>
                        <td>
                            <asp:Button ID="btnAgregar" runat="server" Text="Agregar " OnClientClick="return (valGrillAdd());" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="dgvDocsImpresion" runat="server" AutoGenerateColumns="False" CellPadding="2"
                                HorizontalAlign="Center" EnableViewState="True">
                                <Columns>
                                    <asp:BoundField DataField="Docs" HeaderText="Documento" />
                                    <asp:BoundField DataField="Tp_Nombre" HeaderText="Impresion" />
                                    <asp:TemplateField HeaderText="Config">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chbConfig" runat="server" onClick="return(valChecked(this));" Checked='<%#DataBinder.Eval(Container.DataItem,"Timp")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
<%--                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtVistaPrevia" runat="server" OnClick="btnVistaPrevia_Click">Ver</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
--%>                                    
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="IdTipoDocs" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />
                                            <asp:HiddenField ID="idTipoImpresion" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdImpresion")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
     
        <tr>
            <td>
            </td>
        </tr>
      
    </table>
  <div id="CapaVistaPrevia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
            
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnCerrarCHC" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('CapaVistaPrevia'));" />
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCelda" >
                            <asp:Label ID="lbltitulo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                    <td>
                    
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                    <CR:CrystalReportViewer ID="crvDocsVistaPrev" runat="server" AutoDataBind="true" PrintMode="ActiveX" 
                                        HasCrystalLogo="False" HasToggleGroupTreeButton="False" ToolPanelView="None" />
                            </div>
                        </td>
                    </tr>

                </table>
            
    </div>
    <script language="javascript" type="text/javascript">
        function valOnClick_ADD() {
            var grilla = document.getElementById('<%=dgvDocsImpresion.ClientID %>');
            var idperfil = 0;
        }

        function valCheckPAT() {
            //revisar para que sirve esta funcion
            var grilla = document.getElementById('<%=dgvDocsImpresion.ClientID %>');
            var idperfil = 0;

            if (grilla != null) {
                //*********** obtengo el idPerfil elegido
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[5].children[0].id == event.srcElement.id) {
                        idperfil = parseFloat(rowElem.cells[0].children[1].value);  //revisar 
                        break;
                    }
                }
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[5].children[0].id != event.srcElement.id && parseFloat(rowElem.cells[0].children[1].value) == idperfil) {
                        rowElem.cells[5].children[0].status = false;
                    }
                }
            }
            return true;
        }

        function valChecked(obj) {
            var grilla = document.getElementById('<%=dgvDocsImpresion.ClientID%>');
            var index = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    if (obj.id == rowElem.cells[2].children[0].id) {
                        rowElem.cells[2].children[0].status = true;
                    } else {
                        rowElem.cells[2].children[0].status = false;
                    }
                }
            }
            return true;
        }


        function valGrillAdd() {
            var cboTipoDoc = document.getElementById('<%=cboDocs.ClientID%>');
            var cboTipoRpt = document.getElementById('<%=cboTImpresion.ClientID%>');
            var grilla = document.getElementById('<%=dgvDocsImpresion.ClientID%>');
            if (cboTipoRpt.value == 0) {
                alert('seleccione un tipo de reporte.');
                return false;
            }
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    if (rowElem.cells[3].children[1].value == cboTipoRpt.value) {

                        alert('El tipo de reporte ya ha sido seleccionado.');
                        return false;
                    }
                }
            }
            return true;
        }
        
    </script>

</asp:Content>

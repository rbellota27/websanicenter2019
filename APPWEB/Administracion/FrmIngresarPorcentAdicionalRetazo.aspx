﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmIngresarPorcentAdicionalRetazo.aspx.vb" Inherits="APPWEB.FrmIngresarPorcentAdicionalRetazo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript" >
    function cargarporcentMasivo() {
        var grilla = document.getElementById('<%= DGV_ListaProductos.ClientID %>');
        var porcent=parseFloat( document.getElementById('<%= txtPorcentajeAdicional.ClientID %>').value);
        if (!esDecimal(porcent) || isNaN(porcent) || porcent==null){
            document.getElementById('<%= txtPorcentajeAdicional.ClientID %>').select();
            document.getElementById('<%= txtPorcentajeAdicional.ClientID %>').focus();
            alert('Debe ingresar un valor válido.');
            return false;
        }
        if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                var rowElem = grilla.rows[i];
                if (rowElem.cells[8].children[0].status == false) {
                    rowElem.cells[7].children[0].value = porcent;
                }
            }
        } else {
        alert('No se hallaron registros.');
        return false;
        }
        return false;
    }
    function valGrillaBlur(e) {
        //obtiene srcElement.Id
        var targ;
        if (!e) var e = window.event;
        if (e.target) targ = e.target;
        else if (e.srcElement) targ = e.srcElement;
        //termina
        var id = e.id;
        var caja = document.getElementById(id);
        if (CajaEnBlanco(caja)) {
            caja.select();
            caja.focus();
            alert('Debe ingresar un valor.');
            return true;
        }
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();            
            alert('Valor no válido.');
            return true;
        }
    }
    function validarNumeroPunto(elEvento) {
        //obtener caracter pulsado en todos los exploradores
        var evento = elEvento || window.event;
        var caracter = evento.charCode || evento.keyCode;
        //alert("El carácter pulsado es: " + caracter);
        if (caracter == 46 || key == 45) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                return false;
            }
        }
    }    
</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <table width="100%">
    <tr>
    <td class="TituloCelda">INGRESO DE PORCENTAJES ADICIONALES DE VENTA - RETAZOS</td>
    </tr>
    <tr>
    <td>
        <asp:Panel ID="Panel_Cabecera" runat="server">
            <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
            <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="true" 
                DataTextField="Descripcion" DataValueField="Id">
            </asp:DropDownList>
            <asp:Label ID="Label3" runat="server" CssClass="Label" Text="SubLínea:"></asp:Label>
            <asp:DropDownList ID="cmbSubLinea" runat="server" DataTextField="Nombre" 
                DataValueField="Id">
            </asp:DropDownList>
            <asp:ImageButton ID="btnAceptar_SubLinea_Cab" runat="server" 
                ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
        </asp:Panel>
        </td>
    </tr>
    <tr>
    <td>
        <asp:Panel ID="Panel_detalle" runat="server">
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" CssClass="Label" 
                            Text="Porcentaje (%):"></asp:Label>
                        <asp:TextBox ID="txtPorcentajeAdicional" runat="server"  onblur="return(valGrillaBlur(event));"
                            onKeypress="return(validarNumeroPunto(event));" Width="60px"></asp:TextBox>
                        <asp:ImageButton ID="btnAceptar_SublineaDet" runat="server" 
                            ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                            OnClientClick="return(cargarporcentMasivo());" 
                            onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
                        <asp:ImageButton ID="btnAtras" runat="server" 
                            ImageUrl="~/Imagenes/Arriba_B.JPG" OnClientClick="return(confirm('Desea retroceder?'));"
                            onmouseout="this.src='/Imagenes/Arriba_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Arriba_A.JPG';" ToolTip="Atrás" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ImageButton ID="btnGuardar" runat="server" 
                            ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(confirm('Desea continuar con la operación?'));"
                            onmouseout="this.src='/Imagenes/Guardar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Guardar_A.JPG';" />
                      
                      
                            <div id="capa1" style="border: 0px solid ; padding: 10px; 
                position:absolute; top:352px; left:447px; z-index:3;">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <asp:Image ID="Image4" runat="server" ImageUrl="~/Imagenes/ajax.gif" />                        
                    </ProgressTemplate>
                    
                </asp:UpdateProgress>
                </div>
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="DGV_ListaProductos" runat="server" 
                            AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="IdProducto" HeaderText="Id" NullDisplayText="---" />
                                <asp:BoundField DataField="NomProducto" HeaderText="Descripción" 
                                    NullDisplayText="---" />
                                <asp:BoundField DataField="NomUMPrincipal" HeaderText="U.M. Principal" 
                                    NullDisplayText="---" />    
                                    <asp:BoundField DataField="PrecioCompra" HeaderText="P. Compra" 
                                    NullDisplayText="---" />
                                    <asp:BoundField DataField="NomMoneda" HeaderText="Moneda" 
                                    NullDisplayText="---" />                                   
                                    <asp:BoundField DataField="IdUnidadMedida" HeaderText="Id Retazo" 
                                    NullDisplayText="---" />
                                    <asp:BoundField DataField="NomUnidadMedida" HeaderText="U.M. Retazo" 
                                    NullDisplayText="---" />
                                <asp:TemplateField HeaderText="Porcentaje Adicional (%)" ItemStyle-HorizontalAlign="Center" >
                                    <ItemTemplate >                                        
                                        <asp:TextBox ID="txtPorcentAdd" runat="server" onblur="return(valGrillaBlur(event));" 
                                            onKeypress="return(validarNumeroPunto(event));" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"PorcentAdicionalRetazo")%>' Width="80px"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No Afectar" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate> 
                                        <asp:CheckBox ID="chbAfectar" runat="server" AutoPostBack="false" />                               
                                    </ItemTemplate>                                    
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        </td>
    </tr>
    </table>
    </ContentTemplate>    
    </asp:UpdatePanel>
</asp:Content>

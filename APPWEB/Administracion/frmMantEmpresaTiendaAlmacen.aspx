﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMantEmpresaTiendaAlmacen.aspx.vb"
    Inherits="APPWEB.frmMantEmpresaTiendaAlmacen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Estilos/Controles.css" rel="stylesheet" type="text/css" />
    <link href="/Estilos/stlGeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True">
        <Scripts>
            <asp:ScriptReference Path="~/JS/Util.js" />
            <asp:ScriptReference Path="~/JS/JS_Fecha.js" />
            <asp:ScriptReference Path="~/JS/JS_Impresion.js" />
        </Scripts>
    </asp:ScriptManager>
    <table align="center" width="100%">
        <tr>
            <td>            
                <cc1:TabContainer ID="Tabs" runat="server" ActiveTabIndex="0" Height="300px" Width="100%">
                    <cc1:TabPanel runat="server" HeaderText="Tienda" ID="TabTienda">
                        <HeaderTemplate>
                            Tienda</HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updatePanel1" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:ImageButton ID="btnNuevoMantT" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" onmouseover="this.src='/Imagenes/Nuevo_A.JPG'" />&nbsp;
                                                            <asp:ImageButton ID="cmd_GuardarMantT" runat="server" AlternateText="Guardar Los Cambios Efectuados"
                                                                ImageUrl="~/Imagenes/Guardar_B.JPG" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" ToolTip="Grabar" OnClientClick="return(valAddMantenimientoTienda());" /><cc1:ConfirmButtonExtender
                                                                    ID="cmd_GuardarMantT_ConfirmButtonExtender" runat="server" ConfirmText="Esta Seguro De Grabar . . . ?"
                                                                    TargetControlID="cmd_GuardarMantT">
                                                                </cc1:ConfirmButtonExtender>
                                                            &#160;&nbsp;<asp:ImageButton ID="cmd_EditarT" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                                                onmouseout="this.src='/Imagenes/Editar_B.JPG';" onmouseover="this.src='/Imagenes/Editar_A.JPG';"
                                                                ToolTip="Editar" />&nbsp;
                                                            <asp:ImageButton ID="btnCancelarMantTienda" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Panel ID="PanelNuevoT" runat="server" Width="676px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label28" runat="server" CssClass="" ForeColor="#4277AD" Text="Nombre:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtNomT" runat="server" Width="229px"></asp:TextBox>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label29" runat="server" CssClass="" ForeColor="#4277AD" Text="Tipo:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboTipoT" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label30" runat="server" CssClass="" ForeColor="#4277AD" Text="Dirección:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtDireccionT" runat="server" Height="35px" TextMode="MultiLine" Width="232px"
                                                                    MaxLength="100"></asp:TextBox>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label31" runat="server" CssClass="" ForeColor="#4277AD" Text="Referencia:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtReferenciaT" runat="server" Height="37px" TextMode="MultiLine" Width="226px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label32" runat="server" CssClass="" ForeColor="#4277AD" Text="Departamento:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboDptoT" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label33" runat="server" CssClass="" ForeColor="#4277AD" Text="Provincia:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboProvT" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label34" runat="server" CssClass="" ForeColor="#4277AD" Text="Distrito:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboDistT" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="right">
                                                                <asp:Label ID="Label1" runat="server" CssClass="" ForeColor="#4277AD" Text="E-mail:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtMail" runat="server" Width="220px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label35" runat="server" CssClass="" ForeColor="#4277AD" Text="Telefono:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox ID="txtTelefonoT" runat="server" MaxLength="80" Width="225px"></asp:TextBox>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label36" runat="server" CssClass="" ForeColor="#4277AD" Text="Fax:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox ID="txtFaxT" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label10" runat="server" CssClass="" ForeColor="#4277AD" Text="Cta. Debe Venta:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox ID="tbCtaDebeVenta" MaxLength="20" runat="server" Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <asp:Label ID="Label12" runat="server" CssClass="" ForeColor="#4277AD" Text="Cta. Haber Venta:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox ID="tbCtaHaberVenta" MaxLength="20" runat="server" Width="100px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:CheckBox ID="chkEliminar" runat="server" AutoPostBack="True" CssClass="" ForeColor="Black"
                                                                    Text="Inactivar" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="PanelBuscarT" runat="server" Height="280px" Width="82%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="Panel4" runat="server" ScrollBars="Auto" Width="82%" Height="250px">
                                                                    <asp:GridView ID="dgvTiendaMant" runat="server" Width="624px" AutoGenerateColumns="False">
                                                                        <Columns>
                                                                            <asp:CommandField ShowSelectButton="True" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Tienda" />
                                                                            <asp:BoundField DataField="Dpto" HeaderText="Departamento" />
                                                                            <asp:BoundField DataField="Prov" HeaderText="Provincia" />
                                                                            <asp:BoundField DataField="Dist" HeaderText="Distrito" />
                                                                            <asp:BoundField DataField="Direccion" HeaderText="Direccion" />
                                                                            <asp:BoundField DataField="Referencia" HeaderText="Referencia" />
                                                                            <asp:BoundField DataField="Telefono" HeaderText="Telefono" />
                                                                            <asp:BoundField DataField="Fax" HeaderText="Fax" />
                                                                            <asp:BoundField DataField="TipoTienda" HeaderText="Tipo Tienda" />
                                                                            <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                                                            <asp:BoundField DataField="Id" HeaderText="IdTienda" />
                                                                            <asp:BoundField DataField="Ubigeo" HeaderText="Ubigeo" />
                                                                            <asp:BoundField DataField="Email" HeaderText="E - Mail" />
                                                                            <asp:BoundField DataField="IdTipoTienda" HeaderText="IdTipoTienda" />
                                                                            <asp:BoundField DataField="CtaDebeVenta" HeaderText="Cta. Debe Venta" />
                                                                            <asp:BoundField DataField="CtaHaberVenta" HeaderText="Cta. Haber Venta" />
                                                                        </Columns>
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Tipo Almacen" ID="TabTAlmacen">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updatePanel5" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:ImageButton ID="ibtnNuevoTAlmacen" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" onmouseover="this.src='/Imagenes/Nuevo_A.JPG'" />&nbsp;
                                                            <asp:ImageButton ID="ibtnGuardartAlmacen" runat="server" AlternateText="Guardar Los Cambios Efectuados"
                                                                ImageUrl="~/Imagenes/Guardar_B.JPG" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" ToolTip="Grabar" OnClientClick="return(valAddMantenimientoTipoAlmacen());" /><cc1:ConfirmButtonExtender
                                                                    ID="ConfirmButtonExtenderTA" runat="server" ConfirmText="Esta Seguro De Grabar . . . ?"
                                                                    TargetControlID="ibtnGuardartAlmacen">
                                                                </cc1:ConfirmButtonExtender>
                                                            &#160;&nbsp;<asp:ImageButton ID="ibtnEditarTAlmacen" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                                                onmouseout="this.src='/Imagenes/Editar_B.JPG';" onmouseover="this.src='/Imagenes/Editar_A.JPG';"
                                                                ToolTip="Editar" />&nbsp;
                                                            <asp:ImageButton ID="ibtnCancelarTAlmacen" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Panel ID="PanelNuevoTipoAlmacen" runat="server" Height="120px" Width="500px">
                                                    <table style="width: 96%; height: 120px;">
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label5" runat="server" CssClass="" ForeColor="#4277AD" Text="Nombre:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtNombreTAlmacen" runat="server" Width="232px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label6" runat="server" CssClass="" ForeColor="#4277AD" Text="Nombre Corto:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtNombrecortoTAlmacen" runat="server" Width="232px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label7" runat="server" CssClass="" ForeColor="#4277AD" Text="Almacen Principal:"></asp:Label>
                                                            </td>
                                                            <td class="LabelLeft" align="left">
                                                                <asp:CheckBox ID="chkTAlmacenPrincipal" runat="server" Text="Almacen Principal" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblEstadoTA" runat="server" CssClass="Label_fsp" ForeColor="#4277AD"
                                                                    Text="Estado:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left" class="Label_fsp">
                                                                <asp:RadioButtonList ID="rbtTAEstado" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="1" Selected="True" Text="Activo"></asp:ListItem>
                                                                    <asp:ListItem Value="0" Text="Inactivo"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="panelBuscarTAlmacen" runat="server" Height="280px" Width="100%">
                                                    <table style="width: 10%">
                                                        <%--<tr>
       <td ><asp:Label ID="Label19" runat="server" CssClass="Label" Text="Nombre Area:"></asp:Label></td>
       <td><asp:TextBox ID="txtBuscarArea" runat="server" Width="296px"></asp:TextBox></td>
       <td><asp:ImageButton ID="btnBuscarArea" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';" ToolTip="Efectúa la Búsqueda del Cliente" Visible="true" /></td>
    </tr>--%><tr>
        <td>
            <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Width="100%" Height="250px">
                <asp:GridView ID="gvwTAlmacen" runat="server" Width="624px" AutoGenerateColumns="False">
                    <Columns>
                        <%--                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnMostrar" runat="server" OnClick="lbtnMostrar_Click">Seleccionar</asp:LinkButton>
                            
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="Tipoalm_Nombre" HeaderText="Tipo Almacen" />
                        <asp:BoundField DataField="Tipoalm_NombreCorto" HeaderText="Nombre Corto" />
                        <asp:BoundField DataField="strTipoalm_Principal" HeaderText="Almacen Principal" />
                        <asp:BoundField DataField="strTipoalm_Estado" HeaderText="Estado" />
                        <%--<asp:BoundField DataField="Id" HeaderText="Id" />--%>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdfIdTipoAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoAlmacen")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </asp:Panel>
        </td>
    </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Almacen" ID="TabAlmacen">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:ImageButton ID="btnMantAlmaNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" onmouseover="this.src='/Imagenes/Nuevo_A.JPG'" />&nbsp;
                                                            <asp:ImageButton ID="btnMantAlmaGraba" runat="server" AlternateText="Guardar Los Cambios Efectuados"
                                                                ImageUrl="~/Imagenes/Guardar_B.JPG" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" ToolTip="Grabar" OnClientClick="return(valAddMantenimientoAlmacen());" />
                                                            &#160;&nbsp;<asp:ImageButton ID="btnMantAlmaEditar" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                                                onmouseout="this.src='/Imagenes/Editar_B.JPG';" onmouseover="this.src='/Imagenes/Editar_A.JPG';"
                                                                ToolTip="Editar" />&nbsp;
                                                            <asp:ImageButton ID="btnCancelarMantAlmacen" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Panel ID="PanelNuevoAlmacen" runat="server" Height="210px" Width="676px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label11" runat="server" CssClass="" ForeColor="#4277AD" Text="Nombre:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtNomAlma" runat="server" Width="229px"></asp:TextBox>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label13" runat="server" CssClass="" ForeColor="#4277AD" Text="Area:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboAreaMantAlma" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label14" runat="server" CssClass="" ForeColor="#4277AD" Text="Dirección:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtDireccionAlma" runat="server" Height="35px" TextMode="MultiLine" Width="232px"
                                                                    MaxLength="100"></asp:TextBox>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label15" runat="server" CssClass="" ForeColor="#4277AD" Text="Referencia:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtReferenciaAlma" runat="server" Height="37px" TextMode="MultiLine" Width="226px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label8" runat="server" CssClass="" ForeColor="#4277AD" Text="Tipo Almacen:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboTipoAlmacen" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label9" runat="server" CssClass="" ForeColor="#4277AD" Text="Almacen  de Referencia:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboAlmacenRef" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label16" runat="server" CssClass="" ForeColor="#4277AD" Text="Departamento:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboDptoAlma" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label17" runat="server" CssClass="" ForeColor="#4277AD" Text="Provincia:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboProvAlma" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label18" runat="server" CssClass="" ForeColor="#4277AD" Text="Distrito:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboDistAlma" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="Label19" runat="server" CssClass="" ForeColor="#4277AD" Text="Centro de Distribuicion:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chbCentroDistribuicion" runat="server" Text=" " ForeColor="Black"
                                                                    AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:CheckBox ID="chkEliminaAlma" runat="server" Text="Inactivar" ForeColor="Black"
                                                                    AutoPostBack="true" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="PanelBuscaAlmacen" runat="server" Height="280px" Width="100%">
                                                    <table width="100%" style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="Panel8" runat="server" ScrollBars="Auto" Width="100%" Height="250px">
                                                                    <asp:GridView ID="dgvMantAlmacen" runat="server" Width="624px" AutoGenerateColumns="False">
                                                                        <Columns>
                                                                            <asp:CommandField ShowSelectButton="True" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Almacen" />
                                                                            <asp:BoundField DataField="Dpto" HeaderText="Departamento" />
                                                                            <asp:BoundField DataField="Prov" HeaderText="Provincia" />
                                                                            <asp:BoundField DataField="Dist" HeaderText="Distrito" />
                                                                            <asp:BoundField DataField="Direccion" HeaderText="Direccion" />
                                                                            <asp:BoundField DataField="Referencia" HeaderText="Referencia" />
                                                                            <asp:BoundField DataField="NomArea" HeaderText="Area" />
                                                                            <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                                                            <asp:BoundField DataField="Alm_NombreRef" HeaderText="Almacen Referencia" />
                                                                            <asp:BoundField DataField="Tipoalm_Nombre" HeaderText="Tipo Almacen" />
                                                                            <asp:BoundField DataField="CtroDistribuicion" HeaderText="Centro Distribuicion" />
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="hdfIdtipoAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdtipoAlmacen")%>' />
                                                                                    <asp:HiddenField ID="hdfIdAlmacenRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacenRef")%>' />
                                                                                    <asp:HiddenField ID="hdfTipoprincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Tipoalm_Principal")%>' />
                                                                                    <asp:HiddenField ID="hdfIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                                                    <asp:HiddenField ID="hdfUbigeo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Ubigeo")%>' />
                                                                                    <asp:HiddenField ID="hdfArea" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Area")%>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Caja" ID="TabCaja">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updatePanel4" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:ImageButton ID="btnMantCajaNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" onmouseover="this.src='/Imagenes/Nuevo_A.JPG'" />&nbsp;
                                                            <asp:ImageButton ID="btnMantCajaGrabar" runat="server" AlternateText="Guardar Los Cambios Efectuados"
                                                                ImageUrl="~/Imagenes/Guardar_B.JPG" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" ToolTip="Grabar" OnClientClick="return(valAddMantenimientoCaja());" /><cc1:ConfirmButtonExtender
                                                                    ID="ConfirmButtonExtender1" runat="server" ConfirmText="Esta Seguro De Grabar . . . ?"
                                                                    TargetControlID="btnMantCajaGrabar">
                                                                </cc1:ConfirmButtonExtender>
                                                            &#160;&nbsp;<asp:ImageButton ID="btnMantCajaCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Panel ID="PanelNuevaCaja" runat="server" Height="150px" Width="484px">
                                                    <table style="width: 96%; height: 74px;">
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="Label2" runat="server" CssClass="" ForeColor="#4277AD" Text="Nombre:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                    ID="txtNomCaja" runat="server" Width="232px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="Label3" runat="server" CssClass="" ForeColor="#4277AD" Text="Nro:"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtNroCaja" runat="server" Width="232px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="Label4" runat="server" CssClass="" ForeColor="#4277AD" Text="Tienda:"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:DropDownList ID="cboMantCajaTienda" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">                                                                
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:CheckBox ID="chkCajaChica" runat="server" Text="Caja Chica" ForeColor="Black"
                                                                    AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:CheckBox ID="chkInactivarCaja" runat="server" Text="Inactivar" ForeColor="Black"
                                                                    AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="PanelBuscaCaja" runat="server" Height="280px" Width="100%">
                                                    <table style="width: 10%">
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Width="100%" Height="250px">
                                                                    <asp:GridView ID="dgvCaja" runat="server" Width="624px" AutoGenerateColumns="False">
                                                                        <Columns>
                                                                            <asp:CommandField ShowSelectButton="True" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Caja" />
                                                                            <asp:BoundField DataField="CajaNumero" HeaderText="Nro" />
                                                                            <asp:BoundField DataField="NomTienda" HeaderText="Tienda" />
                                                                            <asp:BoundField DataField="DescEstado" HeaderText="Estado" />
                                                                            <asp:CheckBoxField DataField="caja_chica" HeaderText="Caja Chica"  ItemStyle-HorizontalAlign="Center"  />
                                                                            <asp:BoundField DataField="IdCaja" HeaderText="Id" />
                                                                            <asp:BoundField DataField="IdTienda" HeaderText="IdTienda" />
                                                                        </Columns>
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
    </form>
</body>
</html>

<script type="text/javascript" language="javascript">
    Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
    Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
        if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
            offsetX = Math.floor(offsetX);
            offsetY = Math.floor(offsetY);
        }
        this._origOnFormActiveElement(element, offsetX, offsetY);
    };
    function valAddMantenimientoTienda() {
        var cajaNombre = document.getElementById('<%=txtNomT.ClientID %>');
        var cboTipoTienda = document.getElementById('<%=cboTipoT.ClientID %>');
        var cajaDireccion = document.getElementById('<%=txtDireccionT.ClientID %>');
        var cboDptoT = document.getElementById('<%=cboDptoT.ClientID %>');
        var cboProvT = document.getElementById('<%=cboProvT.ClientID %>');
        var cboDistT = document.getElementById('<%=cboDistT.ClientID %>');
        if (cajaNombre.value.length == 0 || cajaNombre.value == 0) {
            alert('Ingrese el Nombre Tienda.');
            cajaNombre.focus();
            return false;
        }
        if (parseFloat(cboTipoTienda.value) == 0) {
            alert('Seleccione Tipo Tienda.');
            return false;
        }
        if (cajaDireccion.value.length == 0 || cajaDireccion.value == 0) {
            alert('Ingrese Dirección.');
            cajaDireccion.focus();
            return false;
        }
        if (parseFloat(cboDptoT.value) == 00) {
            alert('Seleccione Departamento.');
            return false;
        }
        if (parseFloat(cboProvT.value) == 00) {
            alert('Seleccione Provincia.');
            return false;
        }
        if (parseFloat(cboDistT.value) == 00) {
            alert('Seleccione Distrito.');
            return false;
        }

        return true;
    }


    function valAddMantenimientoTipoAlmacen() {
        var cajaNombre = document.getElementById('<%=txtNombreTAlmacen.ClientID %>');
        var cajaCorto = document.getElementById('<%=txtNombrecortoTAlmacen.ClientID %>');
        if (cajaNombre.value.length == 0 || cajaNombre.value == 0) {
            alert('Ingrese el Nombre del Tipo de Almacen.');
            cajaNombre.focus();
            return false;
        }
        if (cajaCorto.value.length == 0 || cajaCorto.value == 0) {
            alert('Ingrese el Nombre Corto.');
            cajaCorto.focus();
            return false;
        }
        return true;
    }



    function mostrarCapaAlmacen() {
        onCapa('capaMantAlmacen');
        return true;
    }
    function CerrarCapaAlmacen() {
        offCapa('capaMantAlmacen');
        return true;
    }

    function valAddMantenimientoAlmacen() {
        var cajaNombre = document.getElementById('<%=txtNomAlma.ClientID %>');
        var cboArea = document.getElementById('<%=cboAreaMantAlma.ClientID %>');
        var cajaDireccion = document.getElementById('<%=txtDireccionAlma.ClientID %>');
        var cboDptoA = document.getElementById('<%=cboDptoAlma.ClientID %>');
        var cboProvA = document.getElementById('<%=cboProvAlma.ClientID %>');
        var cboDistA = document.getElementById('<%=cboDistAlma.ClientID %>');

        var cboIdAlmacenRef = document.getElementById('<%=cboAlmacenRef.ClientID %>');
        var cboIdTipoAlmacen = document.getElementById('<%=cboTipoAlmacen.ClientID %>');


        if (cajaNombre.value.length == 0 || cajaNombre.value == 0) {
            alert('Ingrese el Nombre Almacen.');
            cajaNombre.focus();
            return false;
        }
        if (parseFloat(cboArea.value) == 0) {
            alert('Seleccione Area.');
            return false;
        }
        if (cajaDireccion.value.length == 0 || cajaDireccion.value == 0) {
            alert('Ingrese Dirección.');
            cajaDireccion.focus();
            return false;
        }
        if (parseFloat(cboDptoA.value) == 00) {
            alert('Seleccione Departamento.');
            return false;
        }
        if (parseFloat(cboProvA.value) == 00) {
            alert('Seleccione Provincia.');
            return false;
        }
        if (parseFloat(cboDistA.value) == 00) {
            alert('Seleccione Distrito.');
            return false;
        }

        //        if (cboIdAlmacenRef .value ==0){
        //            alert('Seleccione un Almacen de Referencia.');
        //            return false;
        //        }

        if (cboIdTipoAlmacen.value == 0) {
            alert('Seleccione un Tipo de Almacen .');
            return false;
        }

        // return confirm('Desea continuar con la operación...');
        return true;
    }

    function validarNumeroPunto(elEvento) {
        //obtener caracter pulsado en todos los exploradores
        var evento = elEvento || window.event;
        var caracter = evento.charCode || evento.keyCode;
        //alert("El carácter pulsado es: " + caracter);
        if (caracter == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                return false;
            }
        }
    }
    function valBlur(event) {
        var id = event.srcElement.id;
        var caja = document.getElementById(id);
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();
            alert('Valor no válido.');
        }
    }


    function valAddMantenimientoCaja() {
        var cajaNombre = document.getElementById('<%=txtNomCaja.ClientID %>');
        var cajaNro = document.getElementById('<%=txtNroCaja.ClientID %>');
        var cajaTienda = document.getElementById('<%=cboMantCajaTienda.ClientID %>');
        if (cajaNombre.value.length == 0 || cajaNombre.value == 0) {
            alert('Ingrese el Nombre Caja.');
            cajaNombre.focus();
            return false;
        }
        if (cajaNro.value.length == 0 || cajaNro.value == 0) {
            alert('Ingrese Nro Caja.');
            cajaNro.focus();
            return false;
        }
        if (parseFloat(cajaTienda.value) == 0) {
            alert('Seleccione Tienda.');
            return false;
        }
        return true;
    }    
</script>

<script language="javascript" type="text/javascript">
    var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;    
</script>


<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmRequerimientoGasto.aspx.vb" Inherits="APPWEB.frmRequerimientoGasto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panelbusqueda" runat="server">
        <table width="100%">
            <tr>
                <td class="TituloCelda">
                    Aprobaci&oacute;n de Requerimiento de Gasto
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="100%">
                        <tr>
                            <td class="LabelTab">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlempresabusqueda" DataTextField="NombreComercial" DataValueField="Id"
                                    runat="server" Font-Bold="true" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="LabelTab">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="dltiendabusqueda" DataTextField="Nombre" DataValueField="Id"
                                    runat="server" Font-Bold="true">
                                </asp:DropDownList>
                            </td>
                            <td class="LabelTab">
                                &Aacute;rea:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlareabusqueda" AutoPostBack="true" DataValueField="IdCompuesto"
                                    DataTextField="DescripcionCorta" runat="server" Font-Bold="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="ckAutorizadas" Text="Autorizadas" runat="server" />
                            </td>
                            <td>
                                <asp:ImageButton ID="imgBuscarAprobacion" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return(validarBuscarDocumentoAprobacion());" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8">
                                <asp:GridView ID="gvbusqueda" Width="100%" runat="server" GridLines="None" AutoGenerateColumns="False">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="ghdd_idserie" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdSerie") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Serie" HeaderText="Serie" />
                                        <asp:BoundField DataField="Codigo" HeaderText="Nro. Req. Gasto" />
                                        <asp:BoundField DataField="FechaEmision" HeaderText="Fecha" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="NombrePersona" HeaderText="Beneficiario" />
                                        <asp:BoundField DataField="Total" HeaderText="Monto" DataFormatString="{0:F2}" />
                                        <asp:TemplateField HeaderText="Autorizar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="gckaprobar" runat="server" Enabled="false" Checked='<%# DataBinder.Eval(Container.DataItem,"anex_Aprobar") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="nomSupervisor" HeaderText="Supervisor" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelForm" runat="server">
        <table style="width: 100%;">
            <tr>
                <td class="TituloCelda">
                    Requerimiento de Gasto
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button Width="85px" ID="btnuevo" runat="server" Text="Nuevo" />
                    <asp:Button Width="85px" ID="btguardar" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());" />
                    <asp:Button Width="85px" ID="btcancelar" runat="server" Text="Cancelar" OnClientClick="return(confirm('Esta seguro de cancelar la operacion ?'));" />
                    <asp:Button Width="85px" ID="btbuscar" runat="server" Text="Buscar" />
                    <asp:Button Width="85px" ID="bteditar" runat="server" Text="Editar" OnClientClick="return(validarEdicion());" />
                    <asp:Button Width="85px" ID="btanular" runat="server" Text="Anular" OnClientClick="return(confirm('Esta seguro de anular el documento ?'));" />
                    <asp:Button Width="85px" ID="btimprimir" runat="server" Text="Imprimir" />
                    <asp:Button Width="130px" ID="btgenerar" runat="server" Text="Generar Reg. Gasto" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlPrincipal" runat="server">
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="LabelTab">
                                                Empresa:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlempresa" runat="server" Font-Bold="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                Tienda:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dltienda" runat="server" Font-Bold="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                Serie:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlserie" runat="server" AutoPostBack="true" Width="100%" Font-Bold="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbcodigo" runat="server" CssClass="TextBox_ReadOnly" Width="80px"
                                                    onKeyDown="return(validarCod(event));" onKeyPress=" return(validarCodBuscar(event));"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btBuscarDocumento" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                                    OnClientClick="return(validarBuscarDocumento());" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Fecha Emisi�n:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbfechaemision" Width="100px" runat="server" CssClass="TextBox_Fecha"></asp:TextBox>
                                                <ajax:CalendarExtender ID="cefechaemision" runat="server" Format="d" TargetControlID="tbfechaemision">
                                                </ajax:CalendarExtender>
                                            </td>
                                            <td class="LabelTab">
                                                Medio Pago:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlmediopago" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                Estado:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlestado" runat="server" Enabled="false" Font-Bold="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Operaci&oacute;n:
                                            </td>
                                            <td colspan="5">
                                                <asp:DropDownList ID="dloperacion" runat="server" Width="100%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Moneda:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlmoneda" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>                                          
                                            <td class="LabelTab">
                                                &Aacute;rea:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlarea" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                E. Cancelaci�n:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlestadocancelacion" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                F. Cancelaci&oacute;n:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbfechacancelacion" onKeyDown="return(false)" runat="server" CssClass="TextBox_Fecha" Width="100px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="panelsupervisor" runat="server">
                                        <table>
                                            <tr>
                                                <td class="TituloCeldaLeft" colspan="4">
                                                    Autorizaci�n
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:RadioButtonList ID="rbsupervisor" onClick="return(validarRadioSupervisor(this));" runat="server" CssClass="Texto" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="1">SI</asp:ListItem>
                                                        <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td class="LabelTab">
                                                    &nbsp;Fecha de Aprobaci&oacute;n
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbfechaaprobacion" Width="100px" runat="server" CssClass="TextBox_Fecha"
                                                        onkeyPress="return(false);"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="ckenviarcuenta" onclick="return(validarCuenta());" runat="server"
                                                        CssClass="Label" Text="Enviar Cuentas Por Pagar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td class="TituloCeldaLeft" colspan="2">
                                                Datos del Beneficiario
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Raz&oacute;n Social / Nombres:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbbeneficiario" runat="server" Width="220px" Font-Bold="true" CssClass="TextBoxReadOnly" onKeyDown="return(false);"></asp:TextBox>
                                                &nbsp;<asp:Button ID="btbuscarbeneficiario" runat="server" Text="Buscar" OnClientClick="return(verCapaPersona());" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                D.N.I.:
                                            </td>
                                            <td class="LabelLeft">
                                                <asp:TextBox Width="80px" ID="tbdni" Font-Bold="true" runat="server" onKeyDown="return(false);" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                &nbsp;R.U.C.:
                                                <asp:TextBox Width="90px" ID="tbruc" Font-Bold="true" runat="server" onKeyDown="return(false);" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="TituloCeldaLeft" valign="bottom">
                                    &nbsp;Detalle del Requerimientos de Gasto (Concepto)
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btagregar" runat="server" Text="Agregar" /><br />
                                    <asp:GridView ID="gvdetalle" runat="server" AutoGenerateColumns="False" GridLines="None"
                                        Width="99%">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                            <asp:TemplateField HeaderText="Concepto">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="gdlconcepto" Width="160px" DataTextField="Nombre" DataValueField="Id"
                                                        runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objConcepto") %>'
                                                        OnSelectedIndexChanged="gdlconcepto_SelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Motivo">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="gdlmotivo" Width="170px" DataValueField="Id" DataTextField="Nombre_MG"
                                                        runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objMotivo") %>'>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Descripci�n">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="gtbdescripcion" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"descripcion") %>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tipo Documento">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="gdltipodocumento" Width="150px" DataValueField="Id" DataTextField="DescripcionCorto"
                                                        runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoDocumento") %>'>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="N� Documento">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="gtbnrodocumento" Width="90px" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"nroDocumento") %>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Moneda">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="gdlmoneda" Width="50px" Enabled="false" runat="server" DataTextField="Simbolo"
                                                        DataValueField="Id" DataSource='<%# DataBinder.Eval(Container.DataItem,"objMoneda") %>'>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Monto">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="gtbmonto" Width="90px" onfocus="return(aceptarFoco(this));" runat="server"
                                                        onKeyUp="javascript:CalcularMonto();" Text='<%# DataBinder.Eval(Container.DataItem,"Monto","{0:F2}") %>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaCabecera" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="LabelTab">
                                    Total:<asp:TextBox Width="90px" ID="tbtotal" Font-Bold="true" runat="server" onKeyPress="return(false);">0</asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="TituloCeldaLeft">
                                    &nbsp;Observaciones
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbobservaciones" TextMode="MultiLine" Width="100%" MaxLength="100"
                                        runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    <asp:HiddenField ID="hdd_idusuario" runat="server" Value="0" />
                    <asp:HiddenField ID="hdd_operativo" runat="server" />
                    <asp:HiddenField ID="hdd_idpersona" runat="server" Value="0" />
                    <asp:HiddenField ID="hdd_iddocumento" runat="server" Value="0" />
                    <asp:HiddenField ID="hdd_idObservacion" runat="server" Value="0" />
                    <asp:HiddenField ID="hdd_IdDocRelacionado" runat="server" Value="0" />
                </td>
            </tr>
        </table>
        <div id="capabeneficiario" style="border: 3px solid blue; padding: 8px; width: 750px;
            height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
            z-index: 2; display: none;">
            <table width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td colspan="5">
                                    <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                        AutoPostBack="false">
                                        <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                        <asp:ListItem Value="J">Juridica</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Texto">
                                    Razon Social / Nombres:
                                </td>
                                <td colspan="4">
                                    <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(event));" runat="server"
                                        Width="450px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Texto">
                                    D.N.I.:
                                </td>
                                <td>
                                    <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                        MaxLength="8" runat="server"></asp:TextBox>
                                </td>
                                <td class="Texto">
                                    Ruc:
                                </td>
                                <td>
                                    <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                        MaxLength="11"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            PageSize="20" Width="100%">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbselect" runat="server" OnClientClick="return(selectPersona(this));">Seleccionar</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                    <ItemStyle HorizontalAlign="Left" Width="500px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                            ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                        <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                            ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                        <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                            runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                        <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                            onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <script language="javascript" type="text/javascript">

        /* Ninguno = 0 - GuardarNuevo = 1 - Actualizar = 2  - Buscar = 3   */

        function validarGuardar() {
            var idarea = document.getElementById('<%=dlarea.ClientID %>');
            if (idarea.value == '0,0,0') {
                alert('Debe seleccionar un area');
                return false;
            }
            var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>');
            if (idpersona.value == '0') {
                alert('Debe ingresar al beneficiario');
                return false;
            }
            var grilla = document.getElementById('<%=gvdetalle.ClientID %>');
            if (grilla == null) {
                alert('Debe ingresar los detalles del documento');
                return false;
            } else {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var monto = redondear(parseFloat(rowElem.cells[7].children[0].value), 2);
                    if (isNaN(monto) || isNaN(rowElem.cells[7].children[0].value)) {
                        alert('Debe ingresar un numero');
                        rowElem.cells[7].children[0].focus();
                        return false;
                    } //end if
                    if (monto <= 0) {
                        alert('Debe ingresar un monto mayor a cero');
                        rowElem.cells[7].children[0].focus();
                        return false;
                    }
                } //next
            }
            return confirm('Desea continuar con la operacion?');
        }
        ///+++++++++++++
        function validarBuscarDocumento() {


        }
        ///++++++++++++++
        function verCapaPersona() {
            onCapa('capabeneficiario');
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            tb.focus();
            return false;
        }
        ///++++++++++++++
        function validarCodBuscar(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            var operativo = document.getElementById('<%=hdd_operativo.ClientID %>');
            var boton = document.getElementById('<%=btbuscardocumento.ClientID %>');
            var codigo = document.getElementById('<%=tbcodigo.ClientID %>');
            if (operativo.value == '3') {
                if (caracter == 13 && codigo.value.length > 0) {
                    boton.focus();
                    return true;
                }
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }

                return true;
            }
            return false;
        }
        function validarCod(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            var operativo = document.getElementById('<%=hdd_operativo.ClientID %>');
            if (operativo.value != '3') {
                if (caracter == 46 || caracter == 8) {  // 3 es buscar
                    return false;
                }
            }
            return true;
        }

        //+++++++++++++++++++++++Busqueda de personas
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        //++++++++++
        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        //++++++++++
        function validarCajaBusqueda(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        ///++++++
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        ///++++++++++++++++++++++++Termina la busqueda de personas
        function selectPersona(obj) {
            var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>');
            var beneficiario = document.getElementById('<%=tbbeneficiario.ClientID %>');
            var dni = document.getElementById('<%=tbdni.ClientID %>');
            var ruc = document.getElementById('<%=tbruc.CLientID %>');
            var grilla = document.getElementById('<%=gvBuscar.ClientID %>');

            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[0].children[0].id == obj.id) {
                    idpersona.value = rowElem.cells[1].innerText;
                    beneficiario.value = rowElem.cells[2].innerText;
                    ruc.value = rowElem.cells[3].innerText;
                    dni.value = rowElem.cells[4].innerText;
                    offCapa('capabeneficiario');
                    return false;
                }
            }
        }
        //++++++++++++++++++++++
        function CalcularMonto() {
            var grilla = document.getElementById('<%=gvdetalle.ClientID %>');
            if (grilla != null) {
                var montoTotal = 0;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var monto = redondear(parseFloat(rowElem.cells[7].children[0].value), 2);
                    if (isNaN(monto)) {
                        monto = 0;
                    } //end if
                    montoTotal = montoTotal + monto;
                } // next
                document.getElementById('<%=tbtotal.ClientID %>').value = redondear(montoTotal, 2);
            } else {  // end if
                document.getElementById('<%=tbtotal.ClientID %>').value = 0;
            }
        } // end function
        //+++++++++++++++++++++++++
        function validarEdicion() {
            var existeRegistro = document.getElementById('<%=hdd_IdDocRelacionado.ClientID %>');
            if (existeRegistro.value != '0') {
                alert('El documento ha sido generado a registro de gasto');
                return false;
            }
            var estadocancelacion = document.getElementById('<%=dlestadocancelacion.ClientID %>');
            if (estadocancelacion.value == '2') {   //cancelado
                alert('El documento tiene el estado de cancelado');
                return false;
            }
            return true;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        function validarBuscarDocumentoAprobacion() {
            var areabusqueda = document.getElementById('<%=dlareaBusqueda.ClientID %>');
            if (areabusqueda.value == '0,0,0') {
                alert('Debe seleccionar una area');
                return false;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        function validarCuenta() {
            var radio = document.getElementById('<%= rbsupervisor.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[0].checked == false) {
                alert('Debe autorizar el documento para poder enviar a cuentas por pagar');
                return false;
            }
            return true;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////
        function validarRadioSupervisor(obj) {
            var control = obj.getElementsByTagName('input');
            var ck = document.getElementById('<%=ckenviarcuenta.ClientID%>');
            if (control[0].checked == false) {
                ck.checked = false;
            }
            return true;
        }
    </script>

</asp:Content>

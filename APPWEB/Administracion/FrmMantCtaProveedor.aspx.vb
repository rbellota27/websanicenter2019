﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmMantCtaProveedor
    Inherits System.Web.UI.Page
#Region "Atributos"
    Dim Combo As New Combo
    Private objScript As New ScriptManagerClass
    Private objctaProveedor As New Negocio.CuentaProveedor
    Private IdTienda As Integer
    Private CuentaEmpTienda As New Entidades.CuentaEmpresaTienda
    'Dim CuentaEmpresa As New Entidades.CuentaEmpresaTienda

    'SListaCxP Sesion ListaCuentasxPersona
    Public Property SListaCxP() As List(Of Entidades.CuentaProveedor)
        Get
            Return CType(Session("ListaCxP"), List(Of Entidades.CuentaProveedor))
        End Get
        Set(ByVal value As List(Of Entidades.CuentaProveedor))
            Session.Remove("ListaCxP")
            Session.Add("ListaCxP", value)
        End Set
    End Property

    'pIdPersona p: persistente, se refiere a que esta guardado
    Public Property pIdPersona() As Integer
        Get
            Return CInt(ViewState.Item("IdPersona"))
        End Get

        Set(ByVal value As Integer)
            ViewState.Remove("IdPersona")
            ViewState.Add("IdPersona", value)
        End Set
    End Property
    'gIdEmpresa g: grafico, se refiere a la interfaz de usuario
    Public Property gIdEmpresa() As Integer
        Get
            Return CInt(cboEmpresa.SelectedValue)
        End Get
        Set(ByVal value As Integer)
            cboEmpresa.SelectedValue = CStr(value)
        End Set
    End Property

    Public Property pSaldo() As Decimal
        Get
            Return CInt(ViewState.Item("Saldo"))
        End Get

        Set(ByVal value As Decimal)
            ViewState.Remove("Saldo")
            ViewState.Add("Saldo", value)
        End Set
    End Property


    Public Property VSCuentasDisponibles() As Integer
        Get
            Return CInt(ViewState.Item("CuentasDisponibles"))
        End Get

        Set(ByVal value As Integer)
            ViewState.Remove("CuentasDisponibles")
            ViewState.Add("CuentasDisponibles", value)
        End Set
    End Property

    Public Property VSFontColor() As System.Drawing.Color
        Get
            Return CType(ViewState.Item("FontColor"), System.Drawing.Color)
        End Get

        Set(ByVal value As System.Drawing.Color)
            ViewState.Remove("FontColor")
            ViewState.Add("FontColor", value)
        End Set
    End Property
    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session("IdUsuario"), Integer)
        End Get
    End Property
    Public Property VSPermisoRegistroyEdicion() As Boolean
        Get
            Return CType(ViewState("permisoRegistroyEdicion"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.Remove("permisoRegistroyEdicion")
            ViewState("permisoRegistroyEdicion") = value
        End Set
    End Property
    Public Property VSPermisoEdicion() As Boolean
        Get
            Return CType(ViewState("permisoEdicion"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.Remove("permisoEdicion")
            ViewState("permisoEdicion") = value
        End Set
    End Property
    Public Property VSPermisoEdicionSaldo() As Boolean
        Get
            Return CType(ViewState("permisoEdicionSaldo"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.Remove("permisoEdicionSaldo")
            ViewState("permisoEdicionSaldo") = value
        End Set
    End Property

#End Region
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ConfigurarDatos()
            inicializar()
        End If
    End Sub
    Private Sub inicializar()
        Combo.LlenarCboMoneda(cboMoneda)
        Combo.LlenarCboPropietario(cboEmpresa, False)
    End Sub

#Region "Procedimientos auxiliares"

    Public Sub HabilitarEdicion(ByVal flag As Boolean)
        If VSPermisoRegistroyEdicion Or VSPermisoEdicion Then
            'pnlInformacionCuentas.Enabled = flag
            'pnlInfoCtaTienda.Visible = flag
        Else
            'pnlInformacionCuentas.Visible = False
            'pnlInfoCtaTienda.Visible = False
        End If
        'pnlGrillaCuentaxPersona.Enabled = flag
    End Sub

#End Region

#Region "Mostrar Documento"
    Private Function mostrardocumento(ByVal idCtaProv As Integer) As List(Of Entidades.CxCResumenView)
        Dim listactaperona As List(Of Entidades.CxCResumenView) = Nothing
        Me.lblMensajeDocumento.Text = ""
        listactaperona = Me.objctaProveedor.DocAsociadosCtaProvxPag(idCtaProv)
        If listactaperona.Count = 0 Then
            Me.lblMensajeDocumento.Text = "No Tiene Documento"
        End If
        Return listactaperona
    End Function
    Protected Sub lbtnMostrar_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Index As Integer = CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex

        Me.gvwCxCxDocumento.DataSource = Me.mostrardocumento(CInt(CType(Me.dgvCuentaxPersona.Rows(Index).FindControl("hdfidCtaProveedor"), HiddenField).Value))
        Me.gvwCxCxDocumento.DataBind()

    End Sub
    Protected Sub btnVerHistCredito_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'muestra el historial en los creditos asignadas

            Dim lbtnMostrar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnMostrar.NamingContainer, GridViewRow)

            Dim objnegcp As New Negocio.CuentaProveedor
            Dim LProv As List(Of Entidades.CuentaProveedor) = objnegcp.SelectAllConsultaxHistCtaProv(CInt(CType(fila.Cells(0).FindControl("hdfidCtaProveedor"), HiddenField).Value.Trim()))
            Me.gvwHistorialCreditos.DataSource = LProv
            gvwHistorialCreditos.DataBind()

            objScript.onCapa(Me, "CapaHistorialCredito")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "No Tiene Detalle.")
        End Try

    End Sub

    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbtnQuitar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
        Dim fila As GridViewRow
        fila = CType(lbtnQuitar.NamingContainer, GridViewRow)
        Me.quitarRegistroCuentaPersona(fila.Cells(0).Text, fila.RowIndex)

    End Sub
    Private Sub quitarRegistroCuentaPersona(ByVal cuenta As String, ByVal DelFila As Integer)
        Dim objUtil As New Negocio.Util
        Try
            '****************** validamos que no tenga registros relacionados
            If objUtil.ValidarxDosParametros("MovCuenta", "IdPersona", hdfidproveedor.Value, "IdCuentaPersona", cuenta) > 0 Then
                objScript.mostrarMsjAlerta(Me, "La cuenta no puede ser eliminada por que posee movimientos relacionados.")
                Return
            End If
            '**************** quitamos de la lista
            'Dim lista As List(Of Entidades.CuentaPersona) = ObtenerListaCuentaPersonaFrmGrilla()
            'lista.RemoveAt(DelFila)

            SListaCxP.RemoveAt(DelFila)
            'dgvCuentaxPersona.DataSource = lista

            dgvCuentaxPersona.DataSource = SListaCxP
            dgvCuentaxPersona.DataBind()

            'validarxIdTienda()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminación del registro.")
        End Try
    End Sub
#End Region
#Region "Mostrar Detalles Abono"


    Private Sub gvwCxCxDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvwCxCxDocumento.SelectedIndexChanged
        LevantaCapaDetalleAbono(CInt(gvwCxCxDocumento.SelectedRow.Cells(1).Text))
    End Sub

    Private Sub LevantaCapaDetalleAbono(ByVal iddocumento As Integer)
        Try
            'nroDias
            Dim objDetAbono As List(Of Entidades.CxCResumenView) = (New Negocio.CXCResumenView).GetDetalleAbonos(iddocumento)

            For Each _Row As Entidades.CxCResumenView In objDetAbono
                If (_Row.nroDias <= 0) Then
                    _Row.nroDias = 0
                End If
            Next
            Me.gvwDetalleAbonos.DataSource = objDetAbono
            Me.gvwDetalleAbonos.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapaDetalleAbonos();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "No Tiene Detalle.")
        End Try
    End Sub
#End Region


    Private Sub addCuentaPersonaGrilla()
        Try
            Dim lista As New List(Of Entidades.CuentaProveedor) '= ObtenerListaCuentaPersonaFrmGrilla()

            'agrega un registro a la lista (captura los valores y los asigna a la lista por medio del constructor y la lista add a la grilla)
            'Dim obj As New Entidades.CuentaProveedor(CInt(ViewState.Item("IdPersona")), CInt(Me.cboMoneda.SelectedValue), (Me.cboMoneda.SelectedItem.Text), CDec(Me.txtCargoMaximo.Text), "1", 0, chkCuentaFormal.Checked)
            Dim objFechaActual As New Negocio.FechaActual
            Dim fechaActual As String = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")

            Dim objProv As New Entidades.CuentaProveedor(pIdPersona, CInt(cboEmpresa.SelectedValue), CInt(Me.cboMoneda.SelectedValue), (Me.cboMoneda.SelectedItem.Text), CDec(Me.txtCargoMaximo.Text), CDec(Me.txtCargoMaximo.Text), "1", Me.txtObs.Text, fechaActual, 0)

            'objProv.IdPropietario = CInt(cboEmpresa.SelectedValue)

            Dim validacion As Integer
            validacion = 1 'Ok


            'If pSaldo < objProv.cprov_CargoMax Then
            '    validacion = -1
            'Else
            '    If VSCuentasDisponibles <= 0 Then
            '        validacion = -2
            '    End If
            'End If

            Select Case validacion
                Case 1
                    SListaCxP.Add(objProv)   ''''!!!!!!!!!Revisar
                    lista.Add(objProv)

                    dgvCuentaxPersona.DataSource = lista
                    'dgvCuentaxPersona.DataSource = SListaCxP
                    dgvCuentaxPersona.DataBind()

                    'validarxIdTienda()
                Case -1
                    objScript.mostrarMsjAlerta(Me, "El Cargo Máximo debe de ser menor o igual que el Saldo Disponible.")
                Case -2
                    objScript.mostrarMsjAlerta(Me, "No hay Cuentas disponibles.")

            End Select

        Catch ex As Exception
        Finally
        End Try
    End Sub



#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      mostrarCapaPersona();", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      mostrarCapaPersona();   alert('No se hallaron registros.');    ", True)
        End If



    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged
        Try
            'limpiar()
            'me.lblmensajedocumento.text = ""
            Me.gvwCxCxDocumento.DataSource = Nothing
            gvwCxCxDocumento.DataBind()
            'Me.gvwdetalleabonos.datasource = Nothing
            'gvwdetalleabonos.databind()
            'pIdPersona=

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            cargarDatosCuentaPersona(dgvCuentaxPersona)
            'informacion_cuentas_empresa_y_tienda()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)
        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")
        Me.txtNombreProveedor.Text = objPersona.Descripcion
        Me.hdfidproveedor.Value = CStr(objPersona.IdPersona)
        Me.txtDniProveedor.Text = objPersona.Dni
        Me.txtRucProveedor.Text = objPersona.Ruc
        'Me.hddIdPersona.Value = CStr(objPersona.IdPersona)
        pIdPersona = objPersona.IdPersona

        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region

    Public Sub limpiar()
        pIdPersona = 0
        txtNombreProveedor.Text = ""
        hdfidproveedor.Value = ""
        txtDniProveedor.Text = ""
        txtRucProveedor.Text = ""
        txtObs.Text = ""
        Me.txtCargoMaximo.Text = ""
        Me.dgvCuentaxPersona.DataBind()
        Me.gvwCxCxDocumento.DataBind()
        'Me.gvwDetalleAbonos.DataBind()
        SListaCxP = New List(Of Entidades.CuentaProveedor)
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        limpiar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        If pIdPersona > 0 Then
            GrabarCuentaPersona()
            'si graba los datos vuelve a obtener los datos grabado
            cargarDatosCuentaPersona(Me.dgvCuentaxPersona)
            'informacion_cuentas_empresa_y_tienda()
            'validarxIdTienda()
            'limpiar()
        Else
            objScript.mostrarMsjAlerta(Me, "No se puede grabar, no hay una persona seleccionada")
        End If
    End Sub
    Private Sub cargarDatosCuentaPersona(ByVal grilla As GridView)
        Dim objCtaProvedor As New Negocio.CuentaProveedor

        SListaCxP = objCtaProvedor.SelectxIdProveedor(pIdPersona)
        grilla.DataSource = SListaCxP
        grilla.DataBind()
        'validarxIdTienda() 
    End Sub

    Private Sub GrabarCuentaPersona()
        Try
            'Dim x As Integer = CInt(ViewState.Item("IdPersona"))
            'Dim x As Integer = pIdPersona

            '********** obtengo la lista de la grilla
            'Dim lista As List(Of Entidades.CuentaPersona) = ObtenerListaCuentaPersonaFrmGrilla()

            '************ obtengo la lista de cada Persona y va
            'Dim listaCuentaPersona As New List(Of Entidades.CuentaPersona)
            'For i As Integer = 0 To lista.Count - 1
            '    Dim obj As New Entidades.CuentaPersona(lista(i).IdPersona, lista(i).IdMoneda, lista(i).CargoMaximo, lista(i).Saldo, lista(i).Estado, lista(i).CuentaFormal)
            '    listaCuentaPersona.Add(obj)
            'Next

            '********** actualizo la lista con los datos de la grilla
            ActualizarListaCuentaPersonaFrmGrilla()
            '***************** registro la lista
            Dim objCuentaPersonaNegocio As New Negocio.CuentaProveedor
            'If objCuentaPersonaNegocio.GrabarCuentaPersona(lista, CInt(ViewState.Item("IdPersona"))) Then
            If CBool(objCuentaPersonaNegocio.GrabarCuentaProveedor(SListaCxP, pIdPersona)) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso Finalizó Con Éxito.")
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas En La Operación.")
        End Try
    End Sub
    Private Sub ActualizarListaCuentaPersonaFrmGrilla()
        'revizar las columnass

        Dim idCtaProveedor_grilla As Integer
        Dim idmoneda_grilla As Integer
        For i As Integer = 0 To dgvCuentaxPersona.Rows.Count - 1
            With dgvCuentaxPersona.Rows(i)
                idCtaProveedor_grilla = CInt(CType(.Cells(0).FindControl("hdfidCtaProveedor"), HiddenField).Value.Trim())
                idmoneda_grilla = CInt(CType(.Cells(0).FindControl("hdfidMoneda"), HiddenField).Value.Trim())
                For j As Integer = 0 To SListaCxP.Count - 1
                    If (SListaCxP(j).IdCuentaProv = idCtaProveedor_grilla And _
                        SListaCxP(j).IdMoneda = idmoneda_grilla) Then


                        SListaCxP(j).IdPropietario = CInt(CType(.Cells(0).FindControl("hdfidPropietario"), HiddenField).Value.Trim())
                        SListaCxP(j).cprov_CargoMax = CDec((CType(.Cells(2).FindControl("txtCargoMaxGvw"), TextBox)).Text)
                        'SListaCxP(j).cprov_Saldo = CDec((CType(.Cells(3).FindControl("txtSaldoGvw"), TextBox)).Text)
                        SListaCxP(j).cprov_Saldo = CDec((.Cells(3).Text))

                        SListaCxP(j).ObservProv = CStr((CType(.Cells(4).FindControl("txtObserGvw"), TextBox)).Text)
                        SListaCxP(j).Estado = CStr(IIf(CType(.Cells(5).FindControl("chbEstado"), CheckBox).Checked, "1", "0"))
                        SListaCxP(j).IdProveedor = pIdPersona

                        'SListaCxP(j).CuentaFormal = CType(.Cells(5).FindControl("chbCtaFormal"), CheckBox).Checked
                        'SListaCxP(j).IdCuentaPersona = CInt(dgvCuentaxPersona.Rows(i).Cells(8).Text)
                        'Esta funcion es llamada cuando se graba, asi que se debe guardar a quien edito las cuentas de esta persona
                        'SListaCxP(j).IdSuperv = SIdUsuario

                    End If
                Next
            End With
        Next
    End Sub


    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        limpiar()
    End Sub

    Private Sub btnAgregarCuentaPersona_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarCuentaPersona.Click
        addCuentaPersonaGrilla()

    End Sub

    Private Sub dgvCuentaxPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCuentaxPersona.SelectedIndexChanged
        'LevantaCapaHistorialCredito(CInt(CType(dgvCuentaxPersona.SelectedRow.Cells(0).FindControl("hdfidCtaProveedor"), HiddenField).Value.Trim()))
    End Sub

    Private Sub LevantaCapaHistorialCredito(ByVal idCtaProv As Integer)
        Try

            Dim objnegcp As New Negocio.CuentaProveedor
            Dim LProv As List(Of Entidades.CuentaProveedor) = objnegcp.SelectAllConsultaxHistCtaProv(idCtaProv)
            Me.gvwHistorialCreditos.DataSource = LProv
            gvwHistorialCreditos.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "CapaHistorialCredito();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "No Tiene Detalle.")
        End Try
    End Sub

    Private Sub btnBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 2) ' ***************** PROVEEDOR 

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
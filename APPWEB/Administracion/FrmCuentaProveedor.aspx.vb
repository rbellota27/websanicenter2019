﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
Partial Public Class FrmCuentaProveedor
    Inherits System.Web.UI.Page
    Dim Combo As New Combo
    Private objScript As New ScriptManagerClass
    Private listaCuentaProveedor As List(Of Entidades.CuentaProveedor)
    Private objctaProveedor As New Negocio.CuentaProveedor

#Region "VISTAS FORMULARIOS"
    Private Sub verFrmInicio()
        limpiarControles() 'limpia los controles de texto
        btBuscarPersonaGrilla.Visible = True
        btBuscarPersonaGrilla.Enabled = True
        Deshabilitar()
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.dgvProveedor.DataBind()
        Me.gvwCxCxDocumento.DataBind()
    End Sub
    Private Sub verFrmNuevo()
        Me.UpdatePanel.Visible = True
        btBuscarPersonaGrilla.Visible = True
        btBuscarPersonaGrilla.Enabled = True
        Me.cboMoneda.SelectedIndex = -1
        Habilitar()
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnNuevo.Visible = False
    End Sub
    Private Sub limpiarControles()
        Me.txtId.Text = ""
        Me.txtIdProveedor.Text = ""
        Me.txtPersona.Text = ""
        Me.txtRuc.Text = ""

        Me.txtCargoMaximo.Text = ""
        Me.txtSaldo.Text = ""
        Me.CboPropietario.SelectedIndex = -1
        Me.cboMoneda.SelectedIndex = -1
    End Sub
    Private Sub Habilitar()
        Me.CboPropietario.Enabled = True
        Me.cboMoneda.Enabled = True
        Me.txtCargoMaximo.Enabled = True
        Me.RblEstado.Enabled = True
    End Sub
    Private Sub Deshabilitar()
        Me.CboPropietario.Enabled = False
        Me.cboMoneda.Enabled = False
        Me.txtCargoMaximo.Enabled = False
        Me.RblEstado.Enabled = False
    End Sub
#End Region
#Region "CARGAR DATOS CONTROLES"
    Private Sub cargarDatosControles() '
        cargarDatosCboMoneda() 'Carga datos en el control combo moneda
        cargarDatosGrilla() 'carga datos en control grilla
    End Sub
    Private Sub cargarDatosCboMoneda() 'Carga los tipo moneda de la tabla y muestra en el combo
        Dim objMoneda As New Negocio.Moneda
        cboMoneda.DataSource = objMoneda.SelectCboNoBase 'lee los datos de la tabla
        cboMoneda.DataBind() 'muestra los datos de la tabla
    End Sub

#End Region
#Region "Busqueda de Personas"

    Private Sub Buscar(ByVal gv As GridView, ByVal ruc As String, _
                      ByVal RazonApe As String, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica("", ruc, RazonApe, 2, index, gv.PageSize, 2, 1)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capapersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capapersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btPersona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btPersona.Click
        Try
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbrazonape.Text.Trim)

            Buscar(gvBuscar, CStr(ViewState("ruc")), CStr(ViewState("pat")), 0)

        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try

            Buscar(gvBuscar, CStr(ViewState("ruc")), CStr(ViewState("pat")), 1)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("ruc")), CStr(ViewState("pat")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("ruc")), CStr(ViewState("pat")), 3)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

    Public Property VSIdProveedor() As Integer
        Get
            Return CInt(ViewState.Item("IdProveedor"))
        End Get

        Set(ByVal value As Integer)
            ViewState.Remove("IdProveedor")
            ViewState.Add("IdProveedor", value)
        End Set
    End Property

#Region "Mostrar Documento"

    Private Function mostrardocumento(ByVal idproveedor As Integer, ByVal idmoneda As Integer) As List(Of Entidades.CxPResumenView)
        Dim listactaproveedor As List(Of Entidades.CxPResumenView) = Nothing

        If (idproveedor > 0) Then
            Me.lblMensajeDocumento.Text = ""
            listactaproveedor = Me.objctaProveedor.SelectGrillaCxPResumenDocumento(idproveedor, idmoneda)
            If listactaproveedor.Count = 0 Then
                Me.lblMensajeDocumento.Text = "No Tiene Documento"
            End If
        Else
            objScript.mostrarMsjAlerta(Me, "La Persona no Existe.")
        End If
        Return listactaproveedor
    End Function
    Protected Sub lbtnMostrar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbtnMostrar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnMostrar.NamingContainer, GridViewRow)
            'mostrar detalle documento
            Me.gvwCxCxDocumento.DataSource = Me.mostrardocumento(CInt(Me.txtIdProveedor.Text), CInt(CType(fila.Cells(1).FindControl("hddIdMoneda"), HiddenField).Value.Trim()))
            gvwCxCxDocumento.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
            'Throw ex
        End Try
    End Sub
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            inicializarFrm()
            Combo.LlenarCboMoneda(Me.cboMoneda, True)
            Combo.LlenarCboPropietario(Me.CboPropietario, True)
            Me.Deshabilitar()
        End If
    End Sub
    Private Sub inicializarFrm()
        Dim objScript As New ScriptManagerClass
        Try
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub setDataSource(ByVal obj As Object)
        Session.Remove("DataSource") 'remueve sesion anterior
        Session.Add("DataSource", obj) 'agregar o actrualiza sesion
    End Sub
    Private Function getDataSource() As Object
        Return Session.Item("DataSource") 'recupera la sesion
    End Function

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        RegistrarCuentaProveedor()
        cargarDatosGrilla()


        If hddId.Value = "N" And CDec(Me.txtCargoMaximo.Text) <> 0 Then
            Me.btnNuevo.Visible = True
            Me.btnCancelar.Visible = False
            Me.btnGuardar.Visible = False
            Me.Deshabilitar()
        End If

    End Sub
    Private Sub RegistrarCuentaProveedor()
        Dim objScript As New ScriptManagerClass
        Dim obj As New Entidades.CuentaProveedor

        'obj.IdCuentaProv = CInt(Me.txtId.Text)
        obj.IdCuentaProv = 0
        obj.IdProveedor = CInt(Me.txtIdProveedor.Text)
        obj.IdPropietario = CInt(Me.CboPropietario.SelectedValue)
        If cboMoneda.SelectedIndex = 0 Then
            objScript.mostrarMsjAlerta(Me, "Debe Seleccionar un tipo moneda")
            cboMoneda.Focus()
            Exit Sub
        End If
        obj.IdMoneda = CInt(Me.cboMoneda.SelectedValue)
        If txtCargoMaximo.Text = "" Or CDec(txtCargoMaximo.Text) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Debe Ingresar un Monto Valido")
            txtCargoMaximo.Focus()
            Exit Sub
        End If
        obj.cprov_CargoMax = CDec(Me.txtCargoMaximo.Text)
        obj.cpro_Estado = Me.RblEstado.SelectedValue

        If hddId.Value = "N" Then
            hddMonto.Value = CStr(CDec(Me.txtCargoMaximo.Text))
            Me.txtSaldo.Text = hddMonto.Value
            obj.cprov_Saldo = CDec(Me.txtSaldo.Text)
        Else
            Calcular_Monto()
            obj.cprov_Saldo = CDec(Me.txtSaldo.Text)
        End If

        Try
            Dim objCuentaProveedor As New Negocio.CuentaProveedor

            If hddId.Value = "N" Then
                obj.Id = 0
                objCuentaProveedor.InsertaCuentaProv(obj)
                objScript.mostrarMsjAlerta(Me, "El Credito fue registrado con éxito del Proveedor:" & txtPersona.Text)
            Else
                'obj.Id = CInt(hddIdPersona.Value)
                obj.Id = CInt(Me.txtId.Text)
                objCuentaProveedor.ActualizaCuentaProv(obj)
                objScript.mostrarMsjAlerta(Me, "El Credito fue Actualizado con éxito del Proveedor:" & txtPersona.Text)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        verFrmNuevo() 'llama al verFrmNuevo
        hddId.Value = "N" 'asigna letra N

        Me.txtCargoMaximo.Text = CStr(0)
        Me.txtSaldo.Text = ""
    End Sub

    Private Sub dgvProveedor_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvProveedor.PageIndexChanging
        Me.dgvProveedor.PageIndex = e.NewPageIndex 'indica el Nro de paginacion en la grilla
        Me.dgvProveedor.DataSource = Me.getDataSource 'aki recupera
        Me.dgvProveedor.DataBind() 'muestra datos en la paginacion
    End Sub
    'Selecciona un registro de la grilla a editar
    Private Sub dgvProveedor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvProveedor.SelectedIndexChanged
        Try
            cargarDatos() 'llama a la cargadatos
            Me.gvwCxCxDocumento.DataBind()
            Me.btnNuevo.Visible = False
            If txtId.Text <> "" Then
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
            End If

            If txtCargoMaximo.Text <> txtSaldo.Text Then
                Me.txtCargoMaximo.Enabled = True
                Me.cboMoneda.Enabled = False
                Me.CboPropietario.Enabled = False
                Me.RblEstado.Enabled = True
            Else
                Me.txtCargoMaximo.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.RblEstado.Enabled = True
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'muestra el dato en los controles del formulario cuando se selecciona un registro en la grilla
    Private Sub cargarDatos()
        Try
            hddMonto.Value = "" 'El monto lo pone en blanco 
            hddSaldo.Value = "" 'El saldo lo pone en blanco 

            Me.cboMoneda.SelectedValue = dgvProveedor.SelectedRow.Cells(1).Text
            Me.txtCargoMaximo.Text = dgvProveedor.SelectedRow.Cells(3).Text
            Me.txtSaldo.Text = dgvProveedor.SelectedRow.Cells(4).Text
            Dim estado As String = dgvProveedor.SelectedRow.Cells(5).Text
            Me.RblEstado.SelectedValue = CStr(IIf(estado = "Activo", "1", "0"))
            Me.txtId.Text = dgvProveedor.SelectedRow.Cells(6).Text

            hddMonto.Value = CStr(CDec(Me.txtCargoMaximo.Text)) 'guarda el monto de la grilla
            hddSaldo.Value = CStr(CDec(Me.txtSaldo.Text)) 'guarda el saldo de la grilla
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'carga los datos en la grilla de acuerdo al proveedor y propietario seleccionado 
    Private Sub cargarDatosGrilla()
        Dim objScript As New ScriptManagerClass
        Try
            Dim objCuentaProveedor As New Negocio.CuentaProveedor
            Dim listCuentaProveedor As List(Of Entidades.CuentaProveedor) = objCuentaProveedor.SelectxIdProv_IdPropCuentaProveedor(CInt(txtIdProveedor.Text), CInt(CboPropietario.SelectedValue()))

            Me.dgvProveedor.DataSource = listCuentaProveedor 'asigna los datos de la lista al data source
            Me.dgvProveedor.DataBind() 'muestra los de la lista en la grilla
            Me.setDataSource(dgvProveedor.DataSource) 'recupera los datos en la paginacion

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub CboPropietario_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CboPropietario.SelectedIndexChanged
        'selecciona un propietario y pone en blanco controles y muestra o esconde botones
        Me.txtCargoMaximo.Text = ""
        Me.txtSaldo.Text = ""
        Me.btnNuevo.Visible = True
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False

        If hddId.Value = "N" Then 'si es nuevo habilita controles
            Me.txtCargoMaximo.Enabled = True
            Me.btnGuardar.Visible = True
            Me.btnCancelar.Visible = True
            Me.btnNuevo.Visible = False
        End If

        cargarDatosGrilla() ' Luego llama al proceso cargar grilla 
        cargarDatosGrilla()
    End Sub
    'Muestra datos del proveedor seleccionado en los controles del formulario
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged
        Try
            hddId.Value = ""
            txtIdProveedor.Text = HttpUtility.HtmlDecode(Me.gvBuscar.SelectedRow.Cells(1).Text)
            txtPersona.Text = HttpUtility.HtmlDecode(Me.gvBuscar.SelectedRow.Cells(2).Text)
            txtRuc.Text = HttpUtility.HtmlDecode(Me.gvBuscar.SelectedRow.Cells(3).Text)
            Me.CboPropietario.Enabled = True
            Me.CboPropietario.SelectedIndex = -1
            Me.cboMoneda.Enabled = False
            Me.cboMoneda.SelectedIndex = -1
            Me.txtCargoMaximo.Text = ""
            Me.txtSaldo.Text = ""
            Me.btnGuardar.Visible = False
            Me.btnNuevo.Visible = False
            Me.btnCancelar.Visible = False
            Me.dgvProveedor.DataBind()
            Me.gvwCxCxDocumento.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'Proceso a sumar o restar monto del credito
    Private Sub Calcular_Monto()

        If CDec(Me.txtCargoMaximo.Text) > CDec(hddMonto.Value) Then
            hddSaldo.Value = CStr(CDec(Me.txtCargoMaximo.Text) - CDec(hddMonto.Value))

            Me.txtSaldo.Text = CStr(CDec(Me.txtSaldo.Text) + CDec(hddSaldo.Value))
        ElseIf CDec(Me.txtCargoMaximo.Text) < CDec(hddMonto.Value) Then
            hddSaldo.Value = CStr(CDbl(hddMonto.Value) - CDec(Me.txtCargoMaximo.Text))
            Me.txtSaldo.Text = CStr(CDec(Me.txtSaldo.Text) - CDec(hddSaldo.Value))
        Else
            Me.txtSaldo.Text = CStr(CDec(hddSaldo.Value))
        End If
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        verFrmInicio() 'muestra el formulario inicio
    End Sub
End Class
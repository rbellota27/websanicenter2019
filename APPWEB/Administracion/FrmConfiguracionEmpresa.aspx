<%@ Page Title="Empresa" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmConfiguracionEmpresa.aspx.vb" Inherits="APPWEB.FrmConfiguracionEmpresa" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <table width="100%" class="TituloCelda">
                            <tr>
                                <td>
                                    Configuraci�n Empresa
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="40%">
                            <tr>
                                <td style="text-align: right">
                                    <asp:Label ID="Label1" runat="server" Text="Empresa:" CssClass="Label"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: left">
                                    <asp:Button ID="cmd_GuardarA" runat="server" Text="Grabar" ToolTip="Grabar" />
                                    <cc1:ConfirmButtonExtender ID="Confirmbuttonextender1" runat="server" TargetControlID="cmd_GuardarA"
                                        ConfirmText="Esta Seguro De Grabar . . . ?">
                                    </cc1:ConfirmButtonExtender>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="50%">
                            <tr>
                                <td align="left" class="TituloCeldaLeft">
                                    <asp:Image ID="imgTiendaEmpresa" runat="server" />&nbsp;Tienda
                                    <cc1:CollapsiblePanelExtender ID="cpEmpTienda" runat="server" TargetControlID="pnlEmpTienda"
                                        ImageControlID="imgTiendaEmpresa" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                        Collapsed="true" CollapseControlID="imgTiendaEmpresa" ExpandControlID="imgTiendaEmpresa">
                                    </cc1:CollapsiblePanelExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Panel ID="pnlEmpTienda" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Tienda:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboTienda" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label38" runat="server" CssClass="Label" Text="Cuenta Contable:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCuentaContable" runat="server" Width="87px" onKeypress="return(validarNumeroPunto(event));"
                                                                    onblur="return(valBlur(event));"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label39" runat="server" CssClass="Label" Text="Centro Costo:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCentroCosto" runat="server" Width="89px" onKeypress="return(validarNumeroPunto(event));"
                                                                    onblur="return(valBlur(event));"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <table width="100%" style="width: 81%">
                                                                    <tr>
                                                                        <td style="text-align: center">
                                                                            <asp:Button ID="btnAgregar_TiendaA" runat="server" Text="Agregar" OnClientClick="return(valAddTienda());"
                                                                                Width="62px" />&nbsp;&nbsp;
                                                                            <%--<asp:Button ID="cmd_MantTiendaA" runat="server" Text="Nueva Tienda" OnClientClick="return(mostrarCapa());" Width="89px"/>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="PnlEmpresaTienda" runat="server" Height="169px" ScrollBars="Vertical">
                                                                    <asp:GridView ID="dgvTienda" runat="server" AutoGenerateColumns="False">
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <Columns>
                                                                           
                                                                            <asp:BoundField DataField="Id" HeaderText="Id" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Tienda" />
                                                                            <asp:BoundField DataField="CuentaContable" HeaderText="Cuenta Contable" />
                                                                            <asp:BoundField DataField="CentroCosto" HeaderText="Centro Costo" />
                                                                            <asp:TemplateField HeaderText="Activo">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkEstadoTienda" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="TituloCeldaLeft">
                                    <asp:Image ID="ImgAlmacenEmpresa" runat="server" />&nbsp;Almacen
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pnlEmpAlmacen"
                                        ImageControlID="ImgAlmacenEmpresa" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                        Collapsed="true" CollapseControlID="ImgAlmacenEmpresa" ExpandControlID="ImgAlmacenEmpresa">
                                    </cc1:CollapsiblePanelExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Panel ID="pnlEmpAlmacen" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label3" runat="server" Text="Almacen:" CssClass="Label"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboAlmacen" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAgregar_AlmacenA" runat="server" Text="Agregar" OnClientClick="return(valAddAlmacen());"
                                                                    Width="56px" />&nbsp;
                                                                <%--<asp:Button ID="cmd_MantAlmacenA" runat="server" Text="Nuevo Almacen" OnClientClick="return(mostrarCapaAlmacen());" Width="97px"/>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="PnlEmpresaAlmacen" runat="server" Height="250px" ScrollBars="Vertical">
                                                                    <asp:GridView ID="dgvAlmacen" runat="server" AutoGenerateColumns="False" Style="text-align: left">
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="Id" DataField="IdAlmacen"></asp:BoundField>
                                                                            <asp:BoundField HeaderText="Almacen" DataField="Nombre"></asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Activo">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkEstadoAlmacen" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"EstadoAI")%>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="TituloCeldaLeft">
                                    <asp:Image ID="ImgAreaEmpresa" runat="server" />&nbsp;Area
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" runat="server" TargetControlID="pnlEmpArea"
                                        ImageControlID="ImgAreaEmpresa" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                        Collapsed="true" CollapseControlID="ImgAreaEmpresa" ExpandControlID="ImgAreaEmpresa">
                                    </cc1:CollapsiblePanelExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Panel ID="pnlEmpArea" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label8" runat="server" Text="Area:" CssClass="Label"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboArea" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAgregar_AreaA" runat="server" Text="Agregar" OnClientClick="return(valAddArea());" />
                                                                <%--<asp:Button ID="cmd_MantAreaA" runat="server" Text="Nueva Area" OnClientClick="return(mostrarCapaArea());"/>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="PnlEmpresaArea" runat="server" Height="250px" ScrollBars="Vertical">
                                                                    <asp:GridView ID="dgvArea" runat="server" AutoGenerateColumns="False" Style="text-align: left">
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="Id" DataField="Id"></asp:BoundField>
                                                                            <asp:BoundField HeaderText="Area" DataField="DescripcionCorta"></asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Activo">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkEstadoArea" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"EstadoAE")%>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="TituloCeldaLeft">
                                    <asp:Image ID="ImgRolEmpresa" runat="server" />&nbsp;Rol
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="pnlEmpRol"
                                        ImageControlID="ImgRolEmpresa" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                        Collapsed="true" CollapseControlID="ImgRolEmpresa" ExpandControlID="ImgRolEmpresa">
                                    </cc1:CollapsiblePanelExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Panel ID="pnlEmpRol" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label4" runat="server" Text="Rol:" CssClass="Label"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboRol" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAgregar_RolA" runat="server" Text="Agregar" OnClientClick="return(valAddRol());" />
                                                                <%--<asp:Button ID="cmd_MantRolA" runat="server" Text="Nueva Rol" OnClientClick="return(mostrarCapaRol());"/>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="PnlEmpresaRol" runat="server" Height="250px" ScrollBars="Vertical">
                                                                    <asp:GridView ID="dgvRol" runat="server" AutoGenerateColumns="False" Style="text-align: left">
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="Id" DataField="IdRol"></asp:BoundField>
                                                                            <asp:BoundField HeaderText="Rol" DataField="Rol"></asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Activo">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkEstadoRol" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" class="TituloCelda">
                            <tr>
                                <td>
                                    Configuraci�n Tienda
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="40%">
                            <tr>
                                <td style="text-align: right">
                                    <asp:Label ID="Label5" runat="server" Text="Tienda:" CssClass="Label"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="cboTiendaT" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: left">
                                    <asp:Button ID="cmd_GuardarTB" runat="server" Text="Grabar" />
                                    <cc1:ConfirmButtonExtender ID="Confirmbuttonextender2" runat="server" TargetControlID="cmd_GuardarTB"
                                        ConfirmText="Esta Seguro De Grabar . . . ?">
                                    </cc1:ConfirmButtonExtender>
                                    <%--<asp:Button ID="cmb_MantTiendaTB" runat="server" Text="Nueva Tienda" OnClientClick="return(mostrarCapa());" />--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="50%">
                            <tr>
                                <td class="TituloCeldaLeft">
                                    <asp:Image ID="imgTiendaArea" runat="server" />&nbsp;Area - Centro de costo
                                     <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender5" runat="server" TargetControlID="pnlTienArea"
                                        ImageControlID="imgTiendaArea" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                        Collapsed="true" CollapseControlID="imgTiendaArea" ExpandControlID="imgTiendaArea">
                                    </cc1:CollapsiblePanelExtender>    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlTienArea" runat="server">
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <asp:Panel ID="PnlTiendaArea" runat="server" Height="250px">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label6" runat="server" CssClass="Label" Text="Area:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboAreaT" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>                                                              
                                                                <td>
                                                                    <asp:Button ID="btnAgregar_AreaTB" runat="server" Text="Agregar" OnClientClick="return(valAddAreaT());" />&nbsp;
                                                                    <%--<asp:Button ID="cmd_MantAreaTiendaB" runat="server" Text="Nueva Area" OnClientClick="return(mostrarCapaArea());"/>--%>
                                                                </td>
                                                            </tr>
                                                            <tr>                                                               
                                                                  <td colspan="3">
                                                                    <table cellpadding="0" cellspacing="0">   
                                                                        <tr>
                                                                            <td class="TituloCeldaLeft" colspan="3">
                                                                                &nbsp;Centro de Costo
                                                                            </td>
                                                                        </tr>                                                                                                                                             
                                                                        <tr>
                                                                            <td class="LabelTdLeft">
                                                                                &nbsp;Unidad Negocio:<br />
                                                                                <asp:DropDownList ID="dlunidadnegocio" runat="server"  AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                &nbsp;Dpto Funcional:<br />
                                                                                <asp:DropDownList ID="dldptofuncional" runat="server"  AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>                                                                        
                                                                        <tr>                                                                                
                                                                            <td class="LabelTdLeft">
                                                                                &nbsp;Sub area 1:<br />
                                                                                <asp:DropDownList ID="dlsubarea1" runat="server"  AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                &nbsp;Sub area 2:<br />
                                                                                <asp:DropDownList ID="dlsubarea2" runat="server"  AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </td>                                                                          
                                                                            <td class="LabelTdLeft">
                                                                                &nbsp;Sub area 3:<br />
                                                                                <asp:DropDownList ID="dlsubarea3" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>                                                           
                                                        </table>
                                                        <asp:Panel ID="Panel1" runat="server" Height="250px" ScrollBars="Vertical">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="dgvAreaT" runat="server" AutoGenerateColumns="False" Style="text-align: left">
                                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                                            <FooterStyle CssClass="GrillaFooter" />
                                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                                            <PagerStyle CssClass="GrillaPager" />
                                                                            <RowStyle CssClass="GrillaRow" />
                                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                            <Columns>
                                                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                                                <asp:BoundField DataField="DescripcionCorta" HeaderText="Area" />
                                                                                <asp:TemplateField HeaderText="Centro Costo" ItemStyle-HorizontalAlign ="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="imgCentroCosto" runat="server" ImageUrl="~/Imagenes/Ok_b.bmp"
                                                                                            ToolTip="Agregar [ Centro de Costo ]" OnClientClick="return(selectCentroCosto(this));" />
                                                                                        <asp:ImageButton ID="imgVerCentroCosto" runat="server" ImageUrl="~/Imagenes/Ok_b.bmp"
                                                                                            ToolTip="Ver [ Centro de Costo ]" OnClick="imgVerCentroCosto_Click" OnClientClick ="return(validarVerCentroCosto(this));" />
                                                                                        <asp:HiddenField ID="hdd_idcentrocosto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCentroCosto") %>' />
                                                                                        <asp:Label ID="Label40" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"IdCentroCosto") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Activo">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkEstadoTiendaArea" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"EstadoAE")%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td class="TituloCeldaLeft">
                                    <asp:Image ID="imgTiendaAlmacen" runat="server" />&nbsp;Almacen
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="pnlTienAlmacen"
                                        ImageControlID="imgTiendaAlmacen" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                        Collapsed="true" CollapseControlID="imgTiendaAlmacen" ExpandControlID="imgTiendaAlmacen">
                                    </cc1:CollapsiblePanelExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlTienAlmacen" runat="server">
                                        <table>
                                            <tr>
                                                <td valign="top">
                                                    <asp:Panel ID="PnlTiendaAlmacen" runat="server" Height="250px">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label7" runat="server" CssClass="Label" Text="Almacen:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboAlmacenT" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnAgregar_AlmacenTB" runat="server" Text="Agregar" OnClientClick="return(valAddAlmacenT());" />&nbsp;
                                                                    <%--<asp:Button ID="cmd_VerMantAlmacenB" runat="server" Text="Nuevo Almacen" OnClientClick="return(mostrarCapaAlmacen());" />--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Panel ID="Panel2" runat="server" Height="250px" ScrollBars="Vertical">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="dgvAlmacenT" runat="server" AutoGenerateColumns="False" Style="text-align: left">
                                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                                            <FooterStyle CssClass="GrillaFooter" />
                                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                                            <PagerStyle CssClass="GrillaPager" />
                                                                            <RowStyle CssClass="GrillaRow" />
                                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                            <Columns>
                                                                                <asp:BoundField DataField="IdAlmacen" HeaderText="Id" />
                                                                                <asp:BoundField DataField="Nombre" HeaderText="Almacen" />
                                                                                <asp:TemplateField HeaderText="Principal">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkTiendaAlmancenPrincipal" runat="server" onClick="return(valCheckPATAlmacen());"
                                                                                            Checked='<%#DataBinder.Eval(Container.DataItem,"TalPrincipal")%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Activo">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkTiendaAlmance2" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Seleccionado")%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <%-- <tr>
        <td class="TituloCeldaLeft">
        <asp:Image ID="imgTiendaCaja" runat="server" />&nbsp;Caja
        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" runat="server" TargetControlID="pnlTienCaja"
        ImageControlID="imgTiendaCaja" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
        Collapsed="true" CollapseControlID="imgTiendaCaja" ExpandControlID="imgTiendaCaja">
        </cc1:CollapsiblePanelExtender>
        </td>
     </tr> 
     <tr>
   <td>
     <asp:Panel ID="pnlTienCaja" runat="server">
     <table>
     <tr>
        <td valign="top">
      <asp:Panel ID="PnlTiendaCaja" runat="server" Height="250px">          
      <table>
      <tr>
         <td><asp:Label ID="Label8" runat="server" CssClass="Label" Text="Caja:"></asp:Label></td>
         <td>
            <asp:TextBox ID="txtNomCaja" runat="server"></asp:TextBox>
            <asp:DropDownList ID="cboCaja" runat="server" AutoPostBack="True"></asp:DropDownList>
         </td>
      </tr>
      <tr>
         <td><asp:Label ID="Label9" runat="server" CssClass="Label" Text="Nro:"></asp:Label></td>
         <td><asp:TextBox ID="txtNroCaja" runat="server"></asp:TextBox></td>
      </tr>
      <tr>
         <td colspan="2" align="center">
           <table  width="50%">
           <tr>
              <td><asp:Button ID="btnNuevoCajaB" runat="server" Text="Nuevo" /></td>
              <td><asp:Button ID="btnCancelarCajaB" runat="server" Text="Cancelar" /></td>
              <td><asp:Button ID="btnAgregar_CajaB" runat="server" Text="Agregar" OnClientClick="return(valAddCaja());" /></td>
           </tr>
           </table>
         </td>
      </tr>
      </table>
      <asp:Panel ID="Panel3" runat="server" ScrollBars="Vertical" Height="230px">           
      <table>
      <tr>
        <td>
           <asp:GridView ID="dgvCaja" runat="server" AutoGenerateColumns="False" style="text-align: left">
           <AlternatingRowStyle CssClass="GrillaRowAlternating" />
           <EditRowStyle CssClass="GrillaEditRow" />
           <FooterStyle CssClass="GrillaFooter" />
           <HeaderStyle CssClass="GrillaHeader" />
           <PagerStyle CssClass="GrillaPager" />
           <RowStyle CssClass="GrillaRow" />
           <EmptyDataTemplate>
              <asp:CheckBox ID="CheckBox1" runat="server" />
           </EmptyDataTemplate>
           <SelectedRowStyle CssClass="GrillaSelectedRow" />
           <Columns>
              <asp:BoundField DataField="IdCaja" HeaderText="Id" />
              <asp:BoundField DataField="Nombre" HeaderText="Caja" />
              <asp:BoundField DataField="CajaNumero" HeaderText="Numero" />
           <asp:TemplateField HeaderText="Activo">
           <ItemTemplate>
              <asp:CheckBox ID="chkTiendaCaja2" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"EstadoTC")%>'/>
           </ItemTemplate>
           </asp:TemplateField>
           </Columns>
           </asp:GridView>
        </td>
      </tr>
      </table>
      </asp:Panel>
      </asp:Panel>
   </td>
     </tr>
     </table>
     </asp:Panel>
   </td>
</tr>    --%>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" class="TituloCelda">
                <tr>
                    <td>
                        .
                    </td>
                </tr>
            </table>
            </td>
            <td>
            </td>
            </tr> </table> </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="capaMantTienda" style="border: 3px solid blue; padding: 8px; width: 672px;
        height: auto; position: absolute; top: 86px; left: 153px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanelMantTienda" runat="server">
            <ContentTemplate>
                <table width="102%" style="width: 101%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrarTienda" runat="server" ImageUrl="~/Imagenes/Cerrar.gif"
                                ToolTip="Cerrar mantenimiento Tienda." OnClientClick="return(CerrarCapa());" />
                        </td>
                    </tr>
                </table>
                <table width="102%" style="width: 101%">
                    <tr>
                        <td align="left">
                            <asp:ImageButton ID="btnNuevoMantT" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" onmouseover="this.src='/Imagenes/Nuevo_A.JPG'" />&nbsp;
                            <asp:ImageButton ID="cmd_GuardarMantT" runat="server" AlternateText="Guardar Los Cambios Efectuados"
                                ImageUrl="~/Imagenes/Guardar_B.JPG" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" ToolTip="Grabar" OnClientClick="return(valAddMantenimientoTienda());" />
                            <cc1:ConfirmButtonExtender ID="cmd_GuardarMantT_ConfirmButtonExtender" runat="server"
                                ConfirmText="Esta Seguro De Grabar . . . ?" TargetControlID="cmd_GuardarMantT">
                            </cc1:ConfirmButtonExtender>
                            &nbsp;
                            <asp:ImageButton ID="cmd_EditarT" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                onmouseout="this.src='/Imagenes/Editar_B.JPG';" onmouseover="this.src='/Imagenes/Editar_A.JPG';"
                                ToolTip="Editar" />&nbsp;
                            <asp:ImageButton ID="btnCancelarMantTienda" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="PanelNuevoT" runat="server" Height="182px" Width="676px">
                    <table width="102%" style="width: 101%">
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label28" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtNomT" runat="server" Width="229px"></asp:TextBox>
                            </td>
                            <td style="text-align: right">
                                <asp:Label ID="Label29" runat="server" CssClass="Label" Text="Tipo:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboTipoT" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label30" runat="server" CssClass="Label" Text="Direcci�n:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtDireccionT" runat="server" Height="35px" TextMode="MultiLine"
                                    Width="232px"></asp:TextBox>
                            </td>
                            <td style="text-align: right">
                                <asp:Label ID="Label31" runat="server" CssClass="Label" Text="Referencia:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtReferenciaT" runat="server" Height="37px" TextMode="MultiLine"
                                    Width="226px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label32" runat="server" CssClass="Label" Text="Departamento:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboDptoT" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td style="text-align: right">
                                <asp:Label ID="Label33" runat="server" CssClass="Label" Text="Provincia:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboProvT" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label34" runat="server" CssClass="Label" Text="Distrito:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboDistT" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" align="right">
                                E-mail:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMail" Width="220px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label35" runat="server" CssClass="Label" Text="Telefono:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtTelefonoT" runat="server"></asp:TextBox>
                            </td>
                            <td style="text-align: right">
                                <asp:Label ID="Label36" runat="server" CssClass="Label" Text="Fax:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtFaxT" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:CheckBox ID="chkEliminar" CssClass="Label" runat="server" Text="Inactivar" ForeColor="Black"
                                    AutoPostBack="True" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanelBuscarT" runat="server" Height="182px" Width="637px">
                    <table width="40%" style="width: 101%">
                        <tr>
                            <td>
                                <asp:Label ID="Label37" runat="server" CssClass="Label" Text="Nombre Tienda:"></asp:Label>
                            </td>
                            <td style="width: 271px">
                                <asp:TextBox ID="txtBusNomT" runat="server" Width="401px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="cmd_BuscaMantT" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                    ToolTip="Efect�a la B�squeda del Cliente" Visible="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel ID="Panel4" runat="server" ScrollBars="Auto" Width="640px" Height="134px">
                                    <asp:GridView ID="dgvTiendaMant" runat="server" Width="1300px" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:BoundField DataField="Nombre" HeaderText="Tienda" />
                                            <asp:BoundField DataField="Dpto" HeaderText="Departamento" />
                                            <asp:BoundField DataField="Prov" HeaderText="Provincia" />
                                            <asp:BoundField DataField="Dist" HeaderText="Distrito" />
                                            <asp:BoundField DataField="Direccion" HeaderText="Direccion" />
                                            <asp:BoundField DataField="Referencia" HeaderText="Referencia" />
                                            <asp:BoundField DataField="Telefono" HeaderText="Telefono" />
                                            <asp:BoundField DataField="Fax" HeaderText="Fax" />
                                            <asp:BoundField DataField="TipoTienda" HeaderText="Tipo Tienda" />
                                            <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                            <asp:BoundField DataField="Id" HeaderText="IdTienda" />
                                            <asp:BoundField DataField="Ubigeo" HeaderText="Ubigeo" />
                                            <asp:BoundField DataField="Email" HeaderText="E - Mail" />
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="capaMantArea" style="border: 3px solid blue; padding: 8px; width: 645px;
        height: auto; position: absolute; top: 136px; left: 194px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table width="102%" style="width: 101%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrarArea" runat="server" ImageUrl="~/Imagenes/Cerrar.gif"
                                ToolTip="Cerrar mantenimiento Area." OnClientClick="return(CerrarCapaArea());" />
                        </td>
                    </tr>
                </table>
                <table width="102%" style="width: 101%">
                    <tr>
                        <td align="left">
                            <asp:ImageButton ID="btnNuevoArea" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" onmouseover="this.src='/Imagenes/Nuevo_A.JPG'" />&nbsp;
                            <asp:ImageButton ID="btnGrabarArea" runat="server" AlternateText="Guardar Los Cambios Efectuados"
                                ImageUrl="~/Imagenes/Guardar_B.JPG" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" ToolTip="Grabar" OnClientClick="return(valAddMantenimientoArea());" />
                            <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" ConfirmText="Esta Seguro De Grabar . . . ?"
                                TargetControlID="btnGrabarArea">
                            </cc1:ConfirmButtonExtender>
                            &nbsp;
                            <asp:ImageButton ID="btnEditarArea" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                onmouseout="this.src='/Imagenes/Editar_B.JPG';" onmouseover="this.src='/Imagenes/Editar_A.JPG';"
                                ToolTip="Editar" />&nbsp;
                            <asp:ImageButton ID="btnCancelarMantArea" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="PanelNuevoArea" runat="server" Height="85px" Width="484px">
                    <table style="width: 96%; height: 74px;">
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label10" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtNombreArea" runat="server" Width="232px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label12" runat="server" CssClass="Label" Text="Nombre Corto:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtNomCortoArea" runat="server" Width="232px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:CheckBox ID="chkEliminarArea" runat="server" Text="Eliminar" ForeColor="Black"
                                    AutoPostBack="true" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanelBuscarArea" runat="server" Height="182px" Width="638px">
                    <table style="width: 10%">
                        <tr>
                            <td>
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Nombre Area:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtBuscarArea" runat="server" Width="296px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarArea" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                    ToolTip="Efect�a la B�squeda del Cliente" Visible="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel ID="Panel7" runat="server" ScrollBars="Auto" Width="614px" Height="134px">
                                    <asp:GridView ID="dgvMantArea" runat="server" Width="624px" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:BoundField DataField="Descripcion" HeaderText="Area" />
                                            <asp:BoundField DataField="DescripcionCorta" HeaderText="Nombre Corto" />
                                            <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                            <asp:BoundField DataField="Id" HeaderText="Id" />
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="capaMantAlmacen" style="border: 3px solid blue; padding: 8px; width: 672px;
        height: auto; position: absolute; top: 261px; left: 166px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <table width="102%" style="width: 101%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrarMantAlma" runat="server" ImageUrl="~/Imagenes/Cerrar.gif"
                                ToolTip="Cerrar mantenimiento Tienda." OnClientClick="return(CerrarCapaAlmacen());" />
                        </td>
                    </tr>
                </table>
                <table width="102%" style="width: 101%">
                    <tr>
                        <td align="left">
                            <asp:ImageButton ID="btnMantAlmaNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" onmouseover="this.src='/Imagenes/Nuevo_A.JPG'" />&nbsp;
                            <asp:ImageButton ID="btnMantAlmaGraba" runat="server" AlternateText="Guardar Los Cambios Efectuados"
                                ImageUrl="~/Imagenes/Guardar_B.JPG" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" ToolTip="Grabar" OnClientClick="return(valAddMantenimientoAlmacen());" />
                            <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" ConfirmText="Esta Seguro De Grabar . . . ?"
                                TargetControlID="btnMantAlmaGraba">
                            </cc1:ConfirmButtonExtender>
                            &nbsp;
                            <asp:ImageButton ID="btnMantAlmaEditar" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                onmouseout="this.src='/Imagenes/Editar_B.JPG';" onmouseover="this.src='/Imagenes/Editar_A.JPG';"
                                ToolTip="Editar" />&nbsp;
                            <asp:ImageButton ID="btnCancelarMantAlmacen" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="PanelNuevoAlmacen" runat="server" Height="148px" Width="676px">
                    <table width="102%" style="width: 101%">
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label11" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtNomAlma" runat="server" Width="229px"></asp:TextBox>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Label13" runat="server" CssClass="Label" Text="Area:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboAreaMantAlma" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label14" runat="server" CssClass="Label" Text="Direcci�n:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtDireccionAlma" runat="server" Height="35px" TextMode="MultiLine"
                                    Width="232px"></asp:TextBox>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Label15" runat="server" CssClass="Label" Text="Referencia:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtReferenciaAlma" runat="server" Height="37px" TextMode="MultiLine"
                                    Width="226px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Departamento:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboDptoAlma" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Provincia:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboProvAlma" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label18" runat="server" CssClass="Label" Text="Distrito:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboDistAlma" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:CheckBox ID="chkEliminaAlma" runat="server" Text="Eliminar" ForeColor="Black"
                                    AutoPostBack="true" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanelBuscaAlmacen" runat="server" Height="182px" Width="637px">
                    <table width="40%" style="width: 101%">
                        <tr>
                            <td>
                                <asp:Label ID="Label22" runat="server" CssClass="Label" Text="Nombre Tienda:"></asp:Label>
                            </td>
                            <td style="width: 271px">
                                <asp:TextBox ID="txtBuscaNomMantAlmacen" runat="server" Width="401px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarMantAlmacen" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                    ToolTip="Efect�a la B�squeda del Cliente" Visible="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel ID="Panel8" runat="server" ScrollBars="Auto" Width="640px" Height="134px">
                                    <asp:GridView ID="dgvMantAlmacen" runat="server" Width="624px" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:BoundField DataField="Nombre" HeaderText="Almacen" />
                                            <asp:BoundField DataField="Dpto" HeaderText="Departamento" />
                                            <asp:BoundField DataField="Prov" HeaderText="Provincia" />
                                            <asp:BoundField DataField="Dist" HeaderText="Distrito" />
                                            <asp:BoundField DataField="Direccion" HeaderText="Direccion" />
                                            <asp:BoundField DataField="Referencia" HeaderText="Referencia" />
                                            <asp:BoundField DataField="NomArea" HeaderText="Area" />
                                            <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                            <asp:BoundField DataField="IdAlmacen" HeaderText="IdAlmacen" />
                                            <asp:BoundField DataField="Ubigeo" HeaderText="Ubigeo" />
                                            <asp:BoundField DataField="Area" HeaderText="IdArea" />
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript" language="javascript">
        function valAddTienda() {
            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
            var cboTienda = document.getElementById('<%=cboTienda.ClientID %>');
            var cajaCuentaContable = document.getElementById('<%=txtCuentaContable.ClientID %>');
            var cajaCentroCosto = document.getElementById('<%=txtCentroCosto.ClientID %>');
            if (parseFloat(cboEmpresa.value) == 0) {
                alert('Seleccione Empresa.');
                return false;
            }
            if (parseFloat(cboTienda.value) == 0) {
                alert('Seleccione Tienda.');
                return false;
            }
            if (cajaCuentaContable.value.length == 0 || cajaCuentaContable.value == 0) {
                alert('Ingrese Cuenta Contable.');
                cajaCuentaContable.focus();
                return false;
            }
            if (cajaCentroCosto.value.length == 0 || cajaCentroCosto.value == 0) {
                alert('Ingrese Centro Costo.');
                cajaCentroCosto.focus();
                return false;
            }
            var grilla = document.getElementById('<%=dgvTienda.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].innerHTML == cboTienda.value && rowElem.cells[0].innerHTML == cboTienda.value) {
                        alert('La Tienda Seleccionado Ya Ha Sido Ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valAddAlmacen() {
            var cboAlmacen = document.getElementById('<%=cboAlmacen.ClientID %>');
            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
            if (parseFloat(cboEmpresa.value) == 0) {
                alert('Seleccione Empresa.');
                return false;
            }
            if (parseFloat(cboAlmacen.value) == 0) {
                alert('Seleccione Almacen.');
                return false;
            }
            var grilla = document.getElementById('<%=dgvAlmacen.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].innerHTML == cboAlmacen.value && rowElem.cells[0].innerHTML == cboAlmacen.value) {
                        alert('El Almacen Seleccionado Ya Ha Sido Ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        function valAddArea() {
            var cboArea = document.getElementById('<%=cboArea.ClientID %>');
            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
            if (parseFloat(cboEmpresa.value) == 0) {
                alert('Seleccione Empresa.');
                return false;
            }
            if (parseFloat(cboArea.value) == 0) {
                alert('Seleccione Area.');
                return false;
            }
            var grilla = document.getElementById('<%=dgvArea.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].innerHTML == cboArea.value && rowElem.cells[0].innerHTML == cboArea.value) {
                        alert('El Area Seleccionado Ya Ha Sido Ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valAddRol() {
            var cboRol = document.getElementById('<%=cboRol.ClientID %>');
            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
            if (parseFloat(cboEmpresa.value) == 0) {
                alert('Seleccione Empresa.');
                return false;
            }
            if (parseFloat(cboRol.value) == 0) {
                alert('Seleccione Rol.');
                return false;
            }
            var grilla = document.getElementById('<%=dgvRol.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].innerHTML == cboRol.value && rowElem.cells[0].innerHTML == cboRol.value) {
                        alert('El Rol Seleccionado Ya Ha Sido Ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valAddAlmacenT() {
            var cboTiendaT = document.getElementById('<%=cboTiendaT.ClientID %>');
            var cboAlmacen = document.getElementById('<%=cboAlmacenT.ClientID %>');
            if (parseFloat(cboTiendaT.value) == 0) {
                alert('Seleccione Tienda.');
                return false;
            }
            if (parseFloat(cboAlmacen.value) == 0) {
                alert('Seleccione Almacen.');
                return false;
            }
            var grilla = document.getElementById('<%=dgvAlmacenT.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].innerHTML == cboAlmacen.value && rowElem.cells[0].innerHTML == cboAlmacen.value) {
                        alert('El Almacen Seleccionado Ya Ha Sido Ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valAddAreaT() {
            var cboArea = document.getElementById('<%=cboAreaT.ClientID %>');
            var cboTiendaT = document.getElementById('<%=cboTiendaT.ClientID %>');
            if (parseFloat(cboTiendaT.value) == 0) {
                alert('Seleccione Tienda.');
                return false;
            }
            if (parseFloat(cboArea.value) == 0) {
                alert('Seleccione Area.');
                return false;
            }
            var grilla = document.getElementById('<%=dgvAreaT.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].innerHTML == cboArea.value && rowElem.cells[0].innerHTML == cboArea.value) {
                        alert('El Area Seleccionado Ya Ha Sido Ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }


        function mostrarCapa() {
            onCapa('capaMantTienda');
            return true;
        }
        function CerrarCapa() {
            offCapa('capaMantTienda');
            return true;
        }



        function valAddMantenimientoTienda() {
            var cajaNombre = document.getElementById('<%=txtNomT.ClientID %>');
            var cboTipoTienda = document.getElementById('<%=cboTipoT.ClientID %>');
            var cajaDireccion = document.getElementById('<%=txtDireccionT.ClientID %>');
            var cboDptoT = document.getElementById('<%=cboDptoT.ClientID %>');
            var cboProvT = document.getElementById('<%=cboProvT.ClientID %>');
            var cboDistT = document.getElementById('<%=cboDistT.ClientID %>');
            if (cajaNombre.value.length == 0 || cajaNombre.value == 0) {
                alert('Ingrese el Nombre Tienda.');
                cajaNombre.focus();
                return false;
            }
            if (parseFloat(cboTipoTienda.value) == 0) {
                alert('Seleccione Tipo Tienda.');
                return false;
            }
            if (cajaDireccion.value.length == 0 || cajaDireccion.value == 0) {
                alert('Ingrese Direcci�n.');
                cajaDireccion.focus();
                return false;
            }
            if (parseFloat(cboDptoT.value) == 00) {
                alert('Seleccione Departamento.');
                return false;
            }
            if (parseFloat(cboProvT.value) == 00) {
                alert('Seleccione Provincia.');
                return false;
            }
            if (parseFloat(cboDistT.value) == 00) {
                alert('Seleccione Distrito.');
                return false;
            }

            return true;
        }

        function mostrarCapaArea() {
            onCapa('capaMantArea');
            return true;
        }
        function CerrarCapaArea() {
            offCapa('capaMantArea');
            return true;
        }

        function valAddMantenimientoArea() {
            var cajaNombre = document.getElementById('<%=txtNombreArea.ClientID %>');
            var cajaCorto = document.getElementById('<%=txtNomCortoArea.ClientID %>');
            if (cajaNombre.value.length == 0 || cajaNombre.value == 0) {
                alert('Ingrese el Nombre del Area.');
                cajaNombre.focus();
                return false;
            }
            if (cajaCorto.value.length == 0 || cajaCorto.value == 0) {
                alert('Ingrese Nombre Corto.');
                cajaCorto.focus();
                return false;
            }
            return true;
        }


        function mostrarCapaAlmacen() {
            onCapa('capaMantAlmacen');
            return true;
        }
        function CerrarCapaAlmacen() {
            offCapa('capaMantAlmacen');
            return true;
        }

        function valAddMantenimientoAlmacen() {
            var cajaNombre = document.getElementById('<%=txtNomAlma.ClientID %>');
            var cboArea = document.getElementById('<%=cboAreaMantAlma.ClientID %>');
            var cajaDireccion = document.getElementById('<%=txtDireccionAlma.ClientID %>');
            var cboDptoA = document.getElementById('<%=cboDptoAlma.ClientID %>');
            var cboProvA = document.getElementById('<%=cboProvAlma.ClientID %>');
            var cboDistA = document.getElementById('<%=cboDistAlma.ClientID %>');
            if (cajaNombre.value.length == 0 || cajaNombre.value == 0) {
                alert('Ingrese el Nombre Almacen.');
                cajaNombre.focus();
                return false;
            }
            if (parseFloat(cboArea.value) == 0) {
                alert('Seleccione Area.');
                return false;
            }
            if (cajaDireccion.value.length == 0 || cajaDireccion.value == 0) {
                alert('Ingrese Direcci�n.');
                cajaDireccion.focus();
                return false;
            }
            if (parseFloat(cboDptoA.value) == 00) {
                alert('Seleccione Departamento.');
                return false;
            }
            if (parseFloat(cboProvA.value) == 00) {
                alert('Seleccione Provincia.');
                return false;
            }
            if (parseFloat(cboDistA.value) == 00) {
                alert('Seleccione Distrito.');
                return false;
            }

            return true;
        }

        function validarNumeroPunto(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function valBlur(event) {
            var id = event.srcElement.id;
            var caja = document.getElementById(id);
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no v�lido.');
            }
        }

        function valCheckPATAlmacen() {
            var grilla = document.getElementById('<%=dgvAlmacenT.ClientID %>');
            var idperfil = 0;

            if (grilla != null) {
                //*********** obtengo el idPerfil elegido
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[2].children[0].id == event.srcElement.id) {
                        idperfil = parseFloat(rowElem.cells[0].innerHTML);
                        break;
                    }
                }
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[2].children[0].id != event.srcElement.id && parseFloat(rowElem.cells[0].innerHTML) == idperfil) {
                        rowElem.cells[2].children[0].status = false;
                    }
                }
            }
            return true;
        }
        //++++++++++++++++++++++++++++++++
        function selectCentroCosto(obj) {
           var unidadnegocio = document.getElementById('<%=dlunidadnegocio.ClientID %>');
           var dptofuncional = document.getElementById('<%=dldptofuncional.ClientID %>');
           var subarea1 = document.getElementById('<%=dlsubarea1.ClientID %>');
           var subarea2 = document.getElementById('<%=dlsubarea2.ClientID %>');
           var subarea3 = document.getElementById('<%=dlsubarea3.ClientID %>');
           var grilla = document.getElementById('<%=dgvAreaT.ClientID %>');
         
           
           var idcentrocosto = unidadnegocio.value + dptofuncional.value + subarea1.value + subarea2.value + subarea3.value
           
           if (grilla != null) {
               for (var i = 1; i < grilla.rows.length; i++) {
                   var rowElem = grilla.rows[i];
                   if (rowElem.cells[2].children[0].id == obj.id) {
                       if (unidadnegocio.value == '00') {
                           rowElem.cells[2].children[2].value = '';
                           rowElem.cells[2].children[3].innerText = '';
                           alert('Seleccione una unidad de negocio');
                           return false;
                       }
                       rowElem.cells[2].children[2].value = idcentrocosto;
                       rowElem.cells[2].children[3].innerText = idcentrocosto;
                       return false;
                   }
               }            
           }
           return false;
       }
       //++++++++++++++++++++++++
       function setText() {
           var grilla = document.getElementById('<%=dgvAreaT.ClientID %>');
           if (grilla != null) {
               for (var i = 1; i < grilla.rows.length; i++) {
                   var rowElem = grilla.rows[i];
                   rowElem.cells[2].children[3].innerText = rowElem.cells[2].children[2].value;
               }
           }
       }
       //++++++++++++++++++++++++
       function validarVerCentroCosto(obj) {
           var grilla = document.getElementById('<%=dgvAreaT.ClientID %>');
           if (grilla != null) {
               for (var i = 1; i < grilla.rows.length; i++) {
                   var rowElem = grilla.rows[i];
                     if (rowElem.cells[2].children[1].id == obj.id) {
                         if (rowElem.cells[2].children[3].innerText == '' || rowElem.cells[2].children[2].value == '') {
                             alert('El �rea de [ ' + rowElem.cells[1].innerText + ' ] no tiene [ Centro de Costo ].');
                             return false;
                         }
                     }                   
               }
           }
       }
    </script>

</asp:Content>

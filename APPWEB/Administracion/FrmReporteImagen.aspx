﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmReporteImagen.aspx.vb" Inherits="APPWEB.FrmReporteImagen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link href="/Estilos/Controles.css" rel="stylesheet" type="text/css" />
    <link href="/Estilos/stlGeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    
    <table align="center">
        <tr>
            <td class="TituloCelda" colspan="2"> Asignar una Imagen al Reporte</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="Label1" runat="server" Text="Ruta de la Imagen:" CssClass="Label"></asp:Label>
            </td>
            <td align="left">
                <input id="Upload" type="file" runat="server"/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnGrabar" runat="server" Text="Grabar" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Image ID="ImgVerRuta" runat="server" />
            </td>
        </tr>
    </table>    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True">
                <Scripts>
                <asp:ScriptReference Path="~/JS/Util.js" />
                <asp:ScriptReference Path="~/JS/JS_Fecha.js" />
                <asp:ScriptReference Path="~/JS/JS_Impresion.js" />
                </Scripts>
    </asp:ScriptManager>
    </form>
</body>
</html>

<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmIngresoTipoCambio.aspx.vb" Inherits="APPWEB.FrmIngresoTipoCambio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
		

        function valSave() {
            var caja = document.getElementById('<%=txtCompraOficial.ClientID %>');
            if (caja.value.length == 0 || !esDecimal(caja.value) || parseFloat(caja.value) <= 0) {
                caja.focus();
                caja.select();
                alert('Debe ingresar un valor.');
                return false;
            }
            caja = document.getElementById('<%=txtVentaOficial.ClientID%>');
            if (caja.value.length == 0 || !esDecimal(caja.value) || parseFloat(caja.value) <= 0) {
                caja.focus();
                caja.select();
                alert('Debe ingresar un valor.');
                return false;
            }
            caja = document.getElementById('<%=txtCompraComercial.ClientID %>');
            if (caja.value.length == 0 || !esDecimal(caja.value) || parseFloat(caja.value) <= 0) {
                caja.focus();
                caja.select();
                alert('Debe ingresar un valor.');
                return false;
            }
            caja = document.getElementById('<%=txtVentaComercial.ClientID %>');
            if (caja.value.length == 0 || !esDecimal(caja.value) || parseFloat(caja.value) <= 0) {
                caja.focus();
                caja.select();
                alert('Debe ingresar un valor.');
                return false;
            }

            //Fecha = document.getElementById('<%=txtfecha.ClientID %>');
            //return(valFecha(Fecha))

            return (confirm('Desea continuar con la operaci�n?'));
        }
        function validarNumeroPunto(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function valCajaBlur() {
            var id = event.srcElement.id;
            var caja = document.getElementById(id);
            if (CajaEnBlanco(caja)) {
                caja.focus();
                alert('Debe ingresar un valor.');
            }
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no v�lido.');
            }
        }
    
    
    </script>

    <table style="width: 100%">
        <tr>
            <td class="TituloCelda">
                TIPO DE CAMBIO
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Panel ID="Panel_Cabecera" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:ImageButton ID="btNuevo" runat="server" CausesValidation="False" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                    onmouseout="this.src='../Imagenes/Nuevo_b.JPG';" onmouseover="this.src='../Imagenes/Nuevo_A.JPG';"
                                    TabIndex="5000" />
                                <asp:ImageButton ID="btnBuscarTC" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseout="this.src='../Imagenes/Buscar_B.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';"
                                    ToolTip="Efect�a la B�squeda del Cliente" />
                                <asp:TextBox ID="txtFecha" runat="server" CssClass="TextBoxReadOnly" Height="18px"
                                    MaxLength="10" Width="82px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFecha">
                                </cc1:CalendarExtender>
                                <asp:Label ID="Label1" runat="server" CssClass="Label" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Moneda:"></asp:Label>
                                <asp:DropDownList ID="cmbTipoMoneda" runat="server" DataTextField="Simbolo" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:ImageButton ID="btnAceptar_TipoMoneda" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                    onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" onmouseover="this.src='/Imagenes/Aceptar_A.JPG';"
                                    Visible="False" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Principal" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td class="TituloCelda" style="width: 795px">
                                Tipo de Cambio - Oficial
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 795px;">
                                <asp:Label ID="Label3" runat="server" CssClass="Label" Text="Compra:"></asp:Label>
                                <asp:TextBox ID="txtCompraOficial" runat="server" onKeypress="return(validarNumeroPunto(event));"
                                    onblur="return(valCajaBlur());" Enabled="False"></asp:TextBox>
                                <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Venta:"></asp:Label>
                                <asp:TextBox ID="txtVentaOficial" runat="server" onKeypress="return(validarNumeroPunto(event));"
                                    onblur="return(valCajaBlur());" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCelda" style="width: 795px">
                                Tipo de Cambio - Comercial
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 795px;">
                                <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Compra:"></asp:Label>
                                <asp:TextBox ID="txtCompraComercial" runat="server" onKeypress="return(validarNumeroPunto(event));"
                                    onblur="return(valCajaBlur());"></asp:TextBox>
                                <asp:Label ID="Label6" runat="server" CssClass="Label" Text="Venta:"></asp:Label>
                                <asp:TextBox ID="txtVentaComercial" onKeypress="return(validarNumeroPunto(event));" onblur="return(valCajaBlur());"
                                    runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 795px;">
                                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                    OnClientClick="return(valSave());" onmouseout="this.src='../Imagenes/Guardar_B.JPG';"
                                    onmouseover="this.src='../Imagenes/Guardar_A.JPG';" />
                                <asp:ImageButton ID="btnEditar" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                    onmouseout="this.src='../Imagenes/Editar_B.JPG';" onmouseover="this.src='../Imagenes/Editar_A.JPG';"
                                    TabIndex="5001" Visible="False" />
                                <asp:ImageButton ID="btnAtras" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                    onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                    ToolTip="Atr�s" Visible="False" />
                                <asp:TextBox ID="txtId" runat="server" onblur="return(valCajaBlur());" onKeypress="return(validarNumeroPunto(event));"
                                    Width="50px" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnExportarExcel" runat="server" Text="Exportar Excel" ToolTip="Exportar Excel" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 795px">
                                <asp:GridView ID="DGV_SubLinea" runat="server" AutoGenerateColumns="False" ForeColor="#333333"
                                    Height="16px" Style="text-align: left" Width="100%" AllowPaging="True">
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <Columns>
                                        <asp:CommandField EditText="Seleccionar" SelectText="Seleccionar" ShowSelectButton="True" />
                                        <asp:BoundField DataField="Fecha" NullDisplayText="---" HeaderText="Fecha" />
                                        <asp:BoundField DataField="CompraOf" HeaderText="Compra - Of." NullDisplayText="---" />
                                        <asp:BoundField DataField="VentaOf" HeaderText="Venta - Of." NullDisplayText="---" />
                                        <asp:BoundField DataField="CompraC" HeaderText="Compra - Com." NullDisplayText="---" />
                                        <asp:BoundField DataField="VentaC" HeaderText="Venta - Com." NullDisplayText="---" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hddIdTipoCambio" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddId" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Table ID="tablaPrincipal" runat="server">
                </asp:Table>
            </td>
        </tr>
    </table>
</asp:Content>

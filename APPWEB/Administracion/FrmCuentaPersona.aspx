<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmCuentaPersona.aspx.vb" Inherits="APPWEB.FrmCuentaPersona" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
        function habilitarFrm(flag) {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').disabled = !(flag);
            document.getElementById('<%=txtCargoMaximo.ClientID%>').disabled = !(flag);
            document.getElementById('<%=btnAgregarCuentaPersona.ClientID%>').disabled = !(flag);
            document.getElementById('<%=cboMoneda.ClientID%>').disabled = !(flag);



            var grillaDoc = document.getElementById('<%=gvwCxCxDocumento.ClientID%>');
            if (grillaDoc != null) {
                grillaDoc.disabled = !(flag)
            }
            var grilla = document.getElementById('<%=dgvCuentaxPersona.ClientID%>');
            if (grilla != null) {
                grilla.disabled = !(flag)
            }
        }
        function verFrmNuevo() {
            document.getElementById('<%=btnNuevo.ClientID%>').disabled = true;
            document.getElementById('<%=btnGuardar.ClientID%>').disabled = false;
            document.getElementById('<%=btnCancelar.ClientID%>').disabled = false;
            habilitarFrm(true);
            return false;
        }
        function verFrmInicio() {
            if (confirm('Desea cancelar el proceso?')) {
                document.getElementById('<%=btnNuevo.ClientID%>').disabled = false;
                document.getElementById('<%=btnGuardar.ClientID%>').disabled = true;
                document.getElementById('<%=btnCancelar.ClientID%>').disabled = true;
                limpiarFrm();
                habilitarFrm(false);
            }
            return false;
        }
        function limpiarFrm() {
            document.getElementById('<%=txtCargoMaximo.ClientID%>').value = 0;
            document.getElementById('<%=txtIdPersona.ClientID%>').value = '';
            document.getElementById('<%=txtPersona.ClientID%>').value = '';
        }

        function validarNumeroPunto(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function valBlur(event) {
            var id = event.srcElement.id;
            var caja = document.getElementById(id);
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no v�lido.');
            }
        }
    </script>

    <asp:UpdatePanel ID="upBotones" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table align="left">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='../Imagenes/Nuevo_A.JPG';"
                                        onmouseout="this.src='../Imagenes/Nuevo_b.JPG';" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                        onmouseover="this.src='../Imagenes/Guardar_A.JPG';" onmouseout="this.src='../Imagenes/Guardar_B.JPG';"
                                        CausesValidation="true" OnClientClick="return(confirm('Desea continuar con la operaci�n?'));" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                        onmouseover="this.src='../Imagenes/Cancelar_A.JPG';" onmouseout="this.src='../Imagenes/Cancelar_B.JPG';" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td class="TituloCelda">
                                    Asignaci�n De Cuentas Corrientes
                                </td>
                            </tr>
                        </table>
                        <asp:Panel ID="pnlDatosPersona" runat="server">
                            <fieldset title="Busqueda de Personas" class="FieldSetPanel">
                                <legend>Persona</legend>
                                <table style="width: 100%">
                                    <tr>
                                        <td class="Label_fsp">
                                            <asp:Label ID="lblApellidosNombre" runat="server" Text="Apellidos y Nombres:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPersona" ReadOnly="true" CssClass="TextBoxReadOnly" runat="server"
                                                Width="333px"></asp:TextBox>
                                            <asp:TextBox ID="txtIdPersona" ReadOnly="true" CssClass="TextBoxReadOnly" Width="95px"
                                                runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBuscarPersona" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Buscar_B.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';"
                                                Visible="True" OnClientClick="return (mostrarCapaPersona());" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Label_fsp">
                                            <asp:Label ID="lblDNI" runat="server" Text="D.N.I.:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDNI" ReadOnly="true" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Label_fsp">
                                            <asp:Label ID="lblRUC" runat="server" Text="R.U.C.:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRUC" ReadOnly="true" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset></asp:Panel>
                        <asp:Panel ID="pnlInformacionCuentas" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 50%">
                                        <%-- <asp:Panel ID="pnlInfoCtaGral" runat="server">
                                            <fieldset title="Informaci�n Cuenta General" class="FieldSetPanel">
                                                <legend>
                                                    <label>
                                                        Cuenta General de la Empresa</label>
                                                </legend>
                                                <table class="Label_fsp">
                                                    <tr>
                                                        <td>
                                                            <label>
                                                                Disponible:</label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCtaGralDisponible" runat="server" Text="Disponible:"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>
                                                                Nro. Cuentas Disponibles:</label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCtaGralNroCtasDisponibles" runat="server" Text="Nro. Cuentas Disponibles:"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset></asp:Panel>--%>
                                        <asp:Panel ID="pnlCuenta" runat="server">
                                            <fieldset title="Cuentas" class="FieldSetPanel" style="height: 150">
                                                <legend>Cuentas</legend>
                                                <table>
                                                    <tr>
                                                        <td class="Label_fsp">
                                                            Empresa:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblTienda" runat="server" Text="Tienda:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblMonto" runat="server" Text="Monto M�ximo:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCargoMaximo" onKeypress="return(validarNumeroPunto(event));" onblur="return(valBlur(event));"
                                                                runat="server"></asp:TextBox>
                                                        </td>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblCuentaFormal" runat="server" Text="Cuenta Formal:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkCuentaFormal" runat="server" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblObs" runat="server" Text="Obs:"></asp:Label>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtObs" runat="server" Width="300px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Label_fsp">
                                                            <asp:Label ID="lblMoneda" runat="server" Text="Moneda:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMoneda" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnAgregarCuentaPersona" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                                onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                                OnClientClick="return(validaMonedaPersonaGrilla());" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset></asp:Panel>
                                    </td>
                                    <td>
                                        <asp:Panel ID="pnlInfoCtaTienda" runat="server">
                                            <fieldset title="Informaci�n Cuenta General" class="FieldSetPanel" style="height: 150">
                                                <legend>
                                                    <label>
                                                        Cuenta General de la Tienda</label>
                                                </legend>
                                                <br />
                                                <table class="Label_fsp">
                                                    <tr>
                                                        <td>
                                                            <label>
                                                                Linea de Cr&eacute;dito:</label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCtaTiendaLineaCredito" runat="server" Text="Monto Max:"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>
                                                                Cargo Max. Ctas:</label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblTiendaCargoMaxCtas" runat="server" Text="Cargo Max Ctas:"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>
                                                                Disponible:</label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCtaTiendaDisponible" runat="server" Text="Disponible:"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>
                                                                Nro. Cuentas Disponibles:</label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCtaTiendaNroCtasDisponibles" runat="server" Text="Nro. Cuentas Disponibles:"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset></asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="height: 62px; text-align: left;">
                        <asp:Panel ID="pnlGrillaCuentaxPersona" runat="server">
                            <asp:GridView ID="dgvCuentaxPersona" runat="server" AutoGenerateColumns="False" Width="650px">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnMostrar" runat="server" OnClick="lbtnMostrar_Click">Detalle</asp:LinkButton>
                                            <asp:HiddenField ID="hddIdMoneda" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hddIdCuentaPersona" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaPersona") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="MonedaSimbolo" HeaderText="Moneda" NullDisplayText="" />
                                    <asp:TemplateField HeaderText="Monto Maximos">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCargoMaximo_grilla" runat="server" onKeypress="return(validarNumeroPunto(event));"
                                                onblur="return(valBlur(event));" Text='<%# Bind("CargoMaximo","{0:F}") %>' NullDisplayText="0"
                                                Width="100px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Saldo">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtSaldo_grilla" runat="server" onKeypress="return(validarNumeroPunto(event));"
                                                onblur="return(valBlur(event));" Text='<%# Bind("Saldo","{0:F}") %>' NullDisplayText="0"
                                                Width="100px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    --%>
                                    <asp:BoundField DataField="Saldo" HeaderText="Disponible" />
                                    <asp:TemplateField HeaderText="Cta. Formal">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chbCtaFormal" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"CuentaFormal")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Activo">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chbEstado" runat="server" onClick="return(valCheckPAT());" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observ">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtObserv_grilla" runat="server" Text='<%# Bind("cp_ObServ","{0:F}") %>'
                                                NullDisplayText="0" Width="300px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FechaAlta" HeaderText="FechaAlta" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnHistorial" runat="server" OnClick="btnHistorial_Click">Historial</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Quitar</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </asp:Panel>
                        <div id="capa1" style="border: 0px solid; padding: 10px; position: absolute; top: 352px;
                            left: 447px; z-index: 2;">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Imagenes/ajax.gif" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </td>
                </tr>
                <%--Seccion de Cuentas Por Cobrar Por Documentos--%>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="TituloCeldaLeft">
                                    Resumen Cuentas Por Cobrar Por Documentos
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="upDocumento" runat="server">
                                        <ContentTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td align="center">
                                                        <asp:GridView ID="gvwCxCxDocumento" runat="server" AutoGenerateColumns="False" RowStyle-Height="25px"
                                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" Width="100%">
                                                            <Columns>
                                                                <asp:CommandField ShowSelectButton="True" SelectText="Ver Mov" />
                                                                <asp:BoundField DataField="idDocumento" HeaderText="Id" />
                                                                <asp:BoundField DataField="Numero" HeaderText="N�mero" />
                                                                <asp:BoundField DataField="tdoc_NombreCorto" HeaderText="Documento" />
                                                                <asp:BoundField DataField="doc_FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n" />
                                                                <asp:BoundField DataField="doc_FechaVencimiento" DataFormatString="{0:dd/MM/yyyy}"
                                                                    HeaderText="FechaVencimiento" />
                                                                <asp:BoundField DataField="nroDias" HeaderText="D�as Vencidos" />
                                                                <asp:BoundField DataField="mon_simbolo" HeaderText="Moneda" ItemStyle-Font-Bold="true" />
                                                                <asp:BoundField DataField="doc_TotalPagar" DataFormatString="{0:F2}" HeaderText="Importe"
                                                                    ItemStyle-Font-Bold="true" />
                                                                <asp:BoundField DataField="abono" DataFormatString="{0:F2}" HeaderText="Total Abonos"
                                                                    ItemStyle-Font-Bold="true" />
                                                                <asp:BoundField DataField="saldo" DataFormatString="{0:F2}" HeaderText="Deuda" ItemStyle-ForeColor="Red"
                                                                    ItemStyle-Font-Bold="true" />
                                                            </Columns>
                                                            <FooterStyle CssClass="GrillaFooter" />
                                                            <PagerStyle CssClass="GrillaPager" />
                                                            <RowStyle CssClass="GrillaRow" />
                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMensajeDocumento" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="capaDetalleAbonos" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="upAbonos" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.gif"
                                OnClientClick="return(offCapa('capaDetalleAbonos'));" />
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCeldaLeft">
                            Detalle De Abonos Por Documento
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvwDetalleAbonos" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%" HeaderStyle-Height="25px" RowStyle-Height="25px">
                                <Columns>
                                    <asp:BoundField DataField="Numero" HeaderText="N�mero" />
                                    <asp:BoundField DataField="tdoc_NombreCorto" HeaderText="Documento" />
                                    <asp:BoundField DataField="doc_FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Movimiento" />
                                    <asp:BoundField DataField="mon_simbolo" HeaderText="Moneda" ItemStyle-Font-Bold="true" />
                                    <asp:BoundField DataField="doc_TotalPagar" DataFormatString="{0:F2}" HeaderText="Importe"
                                        ItemStyle-Font-Bold="true" />
                                    <asp:BoundField DataField="abono" DataFormatString="{0:F2}" HeaderText="Total Abonos"
                                        ItemStyle-Font-Bold="true" />
                                    <asp:BoundField DataField="Saldo" DataFormatString="{0:F2}" HeaderText="Deuda" ItemStyle-Font-Bold="true"
                                        ItemStyle-ForeColor="Red" />
                                    <asp:BoundField DataField="CuentaTipo" HeaderText="Cuenta Tipo" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvwCxCxDocumento" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 2px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Raz�n Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(event));" runat="server"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        R.U.C.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel4" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Filtrar Cta por selecci&oacute;n:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:DropDownList ID="ddl_CuentaTienda" runat="server">
                                                                            <asp:ListItem Text="------" Selected="True" Value="0"></asp:ListItem>
                                                                            <asp:ListItem Text="Empresa" Value="Empresa"></asp:ListItem>
                                                                            <asp:ListItem Text="Tienda" Value="Tienda"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%" GridLines="None" CellPadding="3" CellSpacing="1">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="C�d." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Raz�n Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="400px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="Tienda">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlTienda" onChange="return ( onclick_gvBuscar(this,'1') );"
                                                                                runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"getTienda") %>'>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Moneda">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlMoneda" runat="server" onChange="return ( onclick_gvBuscar(this,'2') );"
                                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"getMoneda") %>'>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="L�nea Cr�dito">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlCargoMax" runat="server" onChange="return ( onclick_gvBuscar(this,'3') );"
                                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"getCargoMaximo") %>'>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Disponible">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlDisponible" runat="server" onChange="return ( onclick_gvBuscar(this,'4') );"
                                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"getDisponible") %>'>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Deuda">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlSaldo" runat="server" onChange="return ( onclick_gvBuscar(this,'5') );"
                                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"getSaldo") %>'>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="CapaHistPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="upnHistPersona" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.gif"
                                OnClientClick="return(offCapa('CapaHistPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCeldaLeft">
                            Historial de Asigacion de Creditos
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 62px; text-align: center;">
                            <asp:Panel ID="Panel1" runat="server">
                                <asp:GridView ID="gvwCtaPersHistorico" runat="server" AutoGenerateColumns="False"
                                    Width="100%" HeaderStyle-Height="25px" RowStyle-Height="25px">
                                    <Columns>
                                        <asp:BoundField DataField="MonedaSimbolo" HeaderText="Moneda" NullDisplayText=""
                                            ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="CargoMaximo" HeaderText="CargoMaximo" NullDisplayText=""
                                            ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="Disponible" HeaderText="Saldo" NullDisplayText="" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="FechaAlta" HeaderText="FechaAlta" NullDisplayText="" />
                                        <asp:BoundField DataField="TiendaNom" HeaderText="Tienda" NullDisplayText="" />
                                        <asp:BoundField DataField="DesCtaFormal" HeaderText="Cta. Formal" NullDisplayText="" />
                                        <asp:BoundField DataField="DesEstado" HeaderText="Estado" NullDisplayText="" />
                                        <asp:BoundField DataField="cp_ObServ" HeaderText="Observ" NullDisplayText="" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvwCtaPersHistorico" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:HiddenField ID="hddOpcionBusquedaPersona" runat="server" Value="0" />
    </div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };

        function valCheckPAT() {
            //revisar para que sirve esta funcion
            var grilla = document.getElementById('<%=dgvCuentaxPersona.ClientID %>');
            var idperfil = 0;

            if (grilla != null) {
                //*********** obtengo el idPerfil elegido
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[5].children[0].id == event.srcElement.id) {
                        idperfil = parseFloat(rowElem.cells[0].children[1].value);  //revisar 
                        break;
                    }
                }
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[5].children[0].id != event.srcElement.id && parseFloat(rowElem.cells[0].children[1].value) == idperfil) {
                        rowElem.cells[5].children[0].status = false;
                    }
                }
            }
            return true;
        }

        function onCapaDetalleAbonos() {
            var grilla = document.getElementById('<%=gvwDetalleAbonos.ClientID %>');
            if (grilla == null) {
                alert("no tiene registros Para mostrar");
                return false;
            }

            onCapa("capaDetalleAbonos");
            return false;
        }
        function valMoneda() {
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID %>');
            if (parseFloat(cboMoneda.value) == 0) {
                alert('Seleccione Moneda.');
                return false;
            }
            return true;
        }

        function valGrillAdd() {
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            var grilla = document.getElementById('<%=dgvCuentaxPersona.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[1].value == cboMoneda.value) {

                        alert('La Moneda Seleccionada Ya Ha Sido Ingresada.');
                        return false;
                    }
                }
            }
            return true;
        }
        function validaMonedaPersonaGrilla() {
            {
                var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
                var grilla = document.getElementById('<%=dgvCuentaxPersona.ClientID%>');
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        if (rowElem.cells[0].children[1].value == cboMoneda.value) {
                            alert('La Moneda Seleccionada Ya Ha Sido Ingresada.');
                            var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');
                            cajaCargoMaximo = "";
                            return false;
                        }
                    }
                }
                var cboMoneda = document.getElementById('<%=cboMoneda.ClientID %>');
                if (parseFloat(cboMoneda.value) == 0) {
                    alert('Seleccione Moneda.');
                    return false;
                }
                var cajaIdPersona = document.getElementById('<%= txtIdPersona.ClientID%>');
                var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');
                if (CajaEnBlanco(cajaIdPersona)) {
                    alert('Debe seleccionar una persona antes de asignar una cuenta.');
                    return false;

                }
                if (CajaEnBlanco(cajaCargoMaximo)) {
                    alert('Ingrese un cargo m�ximo.');
                    return false;
                }
            }
            return true;

        }

        function limpiaCargo() {
            var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');
            cajaCargoMaximo = "";
            return true;
        }

        function valPersona() {

            var cajaIdPersona = document.getElementById('<%= txtIdPersona.ClientID%>');
            var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');

            if (CajaEnBlanco(cajaIdPersona)) {
                alert('Debe seleccionar una persona antes de asignar una cuenta.');
                return false;
            }

            if (CajaEnBlanco(cajaCargoMaximo)) {
                alert('Ingrese un cargo m�ximo.');
                return false;
            }
            return true;
        }

        function SetBodyHeightToContentHeight() {
            document.body.style.Height = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight) + "px";
        }
        SetBodyHeightToContentHeight();
        window.attachEvent('onresize', SetBodyHeightToContentHeight);


        /*Busqueda Personas ini*/

        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }

        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        /*Busqueda Personas fin*/
        function onclick_gvBuscar(obj, opc) {
            var index = -1;
            var gvBuscar = document.getElementById('<%=gvBuscar.ClientID %>');
            if (gvBuscar != null) {
                for (var i = 1; i < gvBuscar.rows.length; i++) {
                    var rowElem = gvBuscar.rows[i];

                    switch (opc) {
                        case '1':
                            if (rowElem.cells[5].children[0] != null) {
                                if (rowElem.cells[5].children[0].id = obj.id) {
                                    index = rowElem.cells[5].children[0].selectedIndex;
                                }
                            }
                            break;
                        case '2':
                            if (rowElem.cells[6].children[0] != null) {
                                if (rowElem.cells[6].children[0].id = obj.id) {
                                    index = rowElem.cells[6].children[0].selectedIndex;
                                }
                            }
                            break;
                        case '3':
                            if (rowElem.cells[7].children[0] != null) {
                                if (rowElem.cells[7].children[0].id = obj.id) {
                                    index = rowElem.cells[7].children[0].selectedIndex;
                                }
                            }
                            break;
                        case '4':
                            if (rowElem.cells[8].children[0] != null) {
                                if (rowElem.cells[8].children[0].id = obj.id) {
                                    index = rowElem.cells[8].children[0].selectedIndex;
                                }
                            }
                            break;
                        case '5':
                            if (rowElem.cells[9].children[0] != null) {
                                if (rowElem.cells[9].children[0].id = obj.id) {
                                    index = rowElem.cells[9].children[0].selectedIndex;
                                }
                            }
                            break;
                    }
                    if (index >= 0) {
                        if (rowElem.cells[5].children[0] != null) {
                            rowElem.cells[5].children[0].selectedIndex = index;
                        }
                        if (rowElem.cells[6].children[0] != null) {
                            rowElem.cells[6].children[0].selectedIndex = index;
                        }
                        if (rowElem.cells[7].children[0] != null) {
                            rowElem.cells[7].children[0].selectedIndex = index;
                        }
                        if (rowElem.cells[8].children[0] != null) {
                            rowElem.cells[8].children[0].selectedIndex = index;
                        }
                        if (rowElem.cells[9].children[0] != null) {
                            rowElem.cells[9].children[0].selectedIndex = index;
                        }
                        index = -1;
                    }

                }
            }
            return false;
        }
    </script>

</asp:Content>

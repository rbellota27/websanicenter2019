﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmAsignacionPrecios

    Inherits System.Web.UI.Page
    Private listaProdTipoPV As List(Of Entidades.ProductoTipoPV)
    Private listaProdTipoPVView As List(Of Entidades.ProductoTipoPVView) '******* Usado solo para consultas, no se guarda en sesion
    Private listaProductoView As List(Of Entidades.ProductoView)

    Private objScript As New ScriptManagerClass
    Private NegVolVenta As New Negocio.VolumenVenta
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Private List_Producto_VolVenta As List(Of Entidades.Producto_VolVenta)
    Private IdtipoExistenciaXsublinea, IdLinea, IdSublinea, IdTPV, IdTienda, UM As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFRM()
        End If
    End Sub
    Private Sub inicializarFRM()

        Dim objCbo As New Combo
        Try
            Me.listaProdTipoPV = New List(Of Entidades.ProductoTipoPV)
            Session.Add("listaProdTipoPV", Me.listaProdTipoPV)

            '********** Cargamos el tipo de Cambio
            txtTipoCambio.Text = CStr(Math.Round((New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(2, 1, 1, "I"), 3))

            cargarControles()
            objCbo.llenarCboTipoExistencia(cboTipoExistencia, False)
            objCbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
            objCbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            '--lineasXsublineas----
            objCbo.llenarCboTipoExistencia(CbotipoExistenciaXsublinea, True)
            objCbo.llenarCboLineaxTipoExistencia(cmbLinea, CInt(Me.CbotipoExistenciaXsublinea.SelectedValue), True)
            objCbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cmbSubLinea, CInt(cmbLinea.SelectedValue), CInt(CbotipoExistenciaXsublinea.SelectedValue), False)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.CboTipotabla_sublinea, CInt(Me.cmbSubLinea.SelectedValue), False)

            ' objCbo.LlenarCboLinea(Me.cmbLinea_AddProd, True)
            'objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            'objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub setListaProdTipoPV(ByVal lista As List(Of Entidades.ProductoTipoPV))
        Session.Remove("listaProdTipoPV")
        Session.Add("listaProdTipoPV", lista)
    End Sub
    Private Function getListaProdTipoPV() As List(Of Entidades.ProductoTipoPV)
        Return CType(Session.Item("listaProdTipoPV"), List(Of Entidades.ProductoTipoPV))
    End Function
#Region "CARGAR DATOS CONTROLES"
    Private Sub cargarControles()
        cargarDatosLinea(Me.cmbLinea)
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
        cargarDatosTienda(Me.cmbTienda)
        cargarDatosTienda(Me.cmbTienda_Prod)
        cargarDatosTipoPrecioPV(Me.cmbTipoPrecioVenta_SubLinea)
        cargarDatosTipoPrecioPV(Me.cmbTipoPrecioVenta_Prod)
    End Sub
    Private Sub cargarDatosUMedidaxIdProducto(ByVal cbo As DropDownList, ByVal IdProducto As Integer)
        Dim objProductoUMView As New Negocio.ProductoUMView
        cbo.DataSource = objProductoUMView.SelectCboxIdProducto(IdProducto)
        cbo.DataBind()
    End Sub
    Private Sub cargarDatosTienda(ByVal combo As DropDownList)
        Dim nTienda As New Negocio.Tienda
        combo.DataSource = nTienda.SelectCbo
        combo.DataBind()
    End Sub
    Private Sub cargarDatosTipoPrecioPV(ByVal combo As DropDownList)
        Dim nTipoPrecioV As New Negocio.TipoPrecioV
        combo.DataSource = nTipoPrecioV.SelectCbo1
        combo.DataBind()
    End Sub
    Private Sub cargarDatosLinea(ByVal combo As DropDownList, Optional ByVal opc As String = "")
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        If opc = "B" Then
            Dim obj As New Entidades.Linea
            obj.Id = 0
            obj.Descripcion = "-----"
            lista.Insert(0, obj)
        End If
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal combo As DropDownList, ByVal idLinea As Integer, Optional ByVal opc As String = "")
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        If opc = "B" Then
            Dim obj As New Entidades.SubLinea
            obj.Id = 0
            obj.Nombre = "-----"
            lista.Insert(0, obj)
        End If
        combo.DataSource = lista
        combo.DataBind()
    End Sub
#End Region
#Region "COMBO SELECTED INDEX CHANGED"
    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        Dim objcbo As New Combo
        objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), CInt(CbotipoExistenciaXsublinea.SelectedValue), True)
        objcbo.LlenarCboTipoTablaxIdSubLinea(Me.CboTipotabla_sublinea, CInt(Me.cmbSubLinea.SelectedValue), False)
    End Sub
#End Region
#Region "VISTAS FORMULARIOS"
    Private Sub verFrmInicio()
        btnGuardar.Visible = False
        btnCancelar.Visible = False
        Panel_AsignarPreciosPor.Visible = True
        Panel_PorSubLinea.Visible = False
        Panel_PorSubLinea_Cab.Visible = False
        Panel_PorSubLinea_Det.Visible = False
        Panel_PorUM_Det.Visible = False
        Panel_Prod_Cab_P.Visible = False
        Panel_ProductoDetalle.Visible = False
        Panel_SubLineaxRetazo.Visible = False
        Panel_AsignarPreciosPor.Enabled = True
        Panel_PorSubLinea.Enabled = True
        Panel_PorSubLinea_Cab.Enabled = True
        Panel_PorSubLinea_Det.Enabled = True
        Panel_PorUM_Det.Enabled = True
        Panel_Prod_Cab_P.Enabled = True
        Panel_ProductoDetalle.Enabled = True
        Panel_SubLineaxRetazo.Enabled = True
    End Sub
    Private Sub verFrmNuevoEditar()
        Panel_AsignarPreciosPor.Visible = True
        Panel_AsignarPreciosPor.Enabled = True
    End Sub
#End Region
#Region "CARGAR PRODUCTOS POR SUBLINEA - NUEVO"
    Protected Sub btnAceptar_SubLinea_Cab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_SubLinea_Cab.Click

        cb_CambiarxEquivalencia.Visible = False
        Select Case cmbAsignarPorUM.SelectedValue
            Case "UMP"
                cb_CambiarxEquivalencia.Visible = True
                cb_CambiarxEquivalencia.Checked = True
                cargarDGV_Productos_Nuevo()
            Case "UMR"
                cargarDGV_ProductosxRetazo()
            Case "UMO"
                cargarDGV_ProductosxUMO()

        End Select
    End Sub
    Private Sub cargarDGV_Productos_Nuevo()
        Dim objScript As New ScriptManagerClass
        Try

            Dim obj As New Negocio.ProductoTipoPVView
            Dim TABLA As DataTable = obtenerDataTable_TipoTablaValor_2()


            actualizar_atributos()


            Me.listaProdTipoPVView = obj.SelectxSubLineaxTiendaxTipoPV(CInt(Me.cmbSubLinea.SelectedValue), CInt(Me.cmbTienda.SelectedValue), CInt(Me.cmbTipoPrecioVenta_SubLinea.SelectedValue), CInt(CbotipoExistenciaXsublinea.SelectedValue), TABLA)




            If Me.listaProdTipoPVView.Count = 0 Then
                '************ If DGV_SubLinea.Rows.Count = 0 Then

                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")

            Else

                DGV_SL_UMP.DataSource = Me.listaProdTipoPVView
                DGV_SL_UMP.DataBind()

                txtPUtilFijoSubLinea.Text = "0"
                txtPorcentPVPublico.Text = "0"

                chbEstadoGeneral.Checked = True
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                Panel_PorSubLinea_Det.Visible = True
                Panel_PorSubLinea_Cab.Enabled = False
                '''''''''desabilitando panel de otras medidas''''''''
                Panel_PorUM_Det.Visible = False
                Panel_AsignarPreciosPor.Enabled = False
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onLoad_PrecioVenta_SL_UMP();", True)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarDGV_ProductosxRetazo()
        Try

            Dim obj As New Negocio.ProductoTipoPVRetazo
            'Dim TABLA As DataTable = obtenerDataTable_TipoTablaValor()
            'actualizar_atributos()
            DGV_SL_UMR.DataSource = obj.SelectRetazoxSubLineaxTiendaxTipoPV(CInt(Me.cmbSubLinea.SelectedValue), CInt(Me.cmbTienda.SelectedValue), CInt(Me.cmbTipoPrecioVenta_SubLinea.SelectedValue))
            DGV_SL_UMR.DataBind()
            If DGV_SL_UMR.Rows.Count = 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros. Para asignar precios de venta a Unidades de Retazo debe primero asignar precios de venta a las Unidades Principales y asignar un valor porcentual adicional para la venta del retazo.")
            Else
                Panel_SubLineaxRetazo.Visible = True
                Panel_PorSubLinea_Cab.Enabled = False
                Panel_AsignarPreciosPor.Enabled = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onLoad_calcularPrecioVenta_SL_UMR();", True)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarDGV_ProductosxUMO()
        Try

            Dim TABLA As DataTable = obtenerDataTable_TipoTablaValor_2()

            Dim obj As New Negocio.ProductoTipoPVView
            DGV_SL_UMO.DataSource = obj.SelectxSubLineaxTiendaxUnidadMedidaTipoPV(CInt(Me.cmbSubLinea.SelectedValue), CInt(Me.cmbTienda.SelectedValue), CInt(Me.cmbTipoPrecioVenta_SubLinea.SelectedValue), CInt(CbotipoExistenciaXsublinea.SelectedValue), TABLA)
            DGV_SL_UMO.DataBind()

            If DGV_SL_UMO.Rows.Count = 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
            Else
                Panel_PorUM_Det.Visible = True
                txtPorcentPVPublicoUMO.Text = "0"
                chbEstadoGeneralUMO.Checked = True
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                Panel_PorSubLinea_Det.Visible = False
                Panel_PorSubLinea_Cab.Enabled = False
                Panel_AsignarPreciosPor.Enabled = False
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onLoad_calcularPrecioVenta_SL_UMO();", True)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "NO ESTA CARGANDO BIEN")
        End Try
    End Sub

#End Region

    Protected Sub btnAceptarBuscarPor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptarBuscarPor.Click
        Select Case cmbAsignarPrecioPor.SelectedValue
            Case "SL"
                Panel_PorSubLinea.Visible = True
                Panel_PorSubLinea_Cab.Visible = True
                Panel_PorSubLinea_Det.Visible = False
                Panel_PorUM_Det.Visible = False
                Panel_AsignarPreciosPor.Enabled = False
            Case "PR"
                limpiarControlesProducto()
                Panel_Prod_Cab_P.Visible = True
                Panel_ProductoDetalle.Visible = False
                Panel_AsignarPreciosPor.Enabled = False
        End Select
    End Sub
    Private Sub limpiarControlesProducto()

        Me.txtProducto.Text = ""
        Me.txtNomUMPrincipal.Text = ""
        Me.txtPrecioCompra_prod.Text = ""
        Me.txtMoneda_PrecioCompra.Text = ""
        Me.txtCodigoProducto.Text = ""
        Me.hddIdProducto.Value = ""
        Me.hddIdMoneda_PC.Value = ""
        Me.hddIdUnidadMedida_Principal.Value = ""

    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Select Case cmbAsignarPrecioPor.SelectedValue
            Case "SL"
                Select Case cmbAsignarPorUM.SelectedValue
                    Case "UMP"
                        registrarAsigPreciosxSL()
                    Case "UMR"
                        registrarAsigPreciosxRetazo()
                    Case "UMO"
                        registrarAsigPreciosxUMO()

                End Select
            Case "PR"
                registrarAsigPreciosxPR()
        End Select
    End Sub
    Private Sub registrarAsigPreciosxRetazo()

        Dim lista As New List(Of Entidades.ProductoTipoPV)
        Try
            For i As Integer = 0 To DGV_SL_UMR.Rows.Count - 1
                Dim objProdTipoPV As New Entidades.ProductoTipoPV
                With objProdTipoPV
                    .Estado = CStr(IIf((CType(DGV_SL_UMR.Rows(i).Cells(11).FindControl("chbEstado_SL_UMR"), CheckBox).Checked) = True, "1", "0"))
                    .IdProducto = CInt(DGV_SL_UMR.Rows(i).Cells(0).Text)
                    .IdTienda = CInt(cmbTienda.SelectedValue)
                    .IdTipoPv = CInt(cmbTipoPrecioVenta_SubLinea.SelectedValue)
                    .IdUnidadMedida = CInt(CType(DGV_SL_UMR.Rows(i).Cells(6).FindControl("hddIdUMRetazo_SL_UMR"), HiddenField).Value)
                    .IdUsuario = CInt(Session("IdUsuario").ToString)
                    .PUtilFijo = CDec(CType(DGV_SL_UMR.Rows(i).Cells(8).FindControl("txtPorcentUtilidad_SL_UMR"), TextBox).Text)
                    .PUtilVariable = 0
                    .Utilidad = CDec(CType(DGV_SL_UMR.Rows(i).Cells(9).FindControl("txtUtilidad_SL_UMR"), TextBox).Text)
                    .Valor = CDec(CType(DGV_SL_UMR.Rows(i).Cells(10).FindControl("txtPVFinal_SL_UMR"), TextBox).Text)
                    .IdMoneda = CInt(CType(DGV_SL_UMR.Rows(i).Cells(10).FindControl("hddIdMonedaPV_SL_UMR"), HiddenField).Value)
                    .Retazo = True
                    .UnidadPrincipal = False
                End With
                lista.Add(objProdTipoPV)
            Next
            Dim objNegProdTipoPV As New Negocio.ProductoTipoPV
            If objNegProdTipoPV.InsertaProductoTipoPVT(lista) Then
                'verFrmInicio()
                objScript.mostrarMsjAlerta(Me, "La operación se realizó con éxito.")
            Else
                Throw New Exception("Ocurrieron problemas en la operación.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub registrarAsigPreciosxSL()

        Dim lista As New List(Of Entidades.ProductoTipoPV)
        Try
            For i As Integer = 0 To DGV_SL_UMP.Rows.Count - 1

                Dim objProdTipoPV As New Entidades.ProductoTipoPV
                With objProdTipoPV
                    .Estado = CStr(IIf((CType(DGV_SL_UMP.Rows(i).Cells(10).FindControl("chbEstado"), CheckBox).Checked) = True, "1", "0"))
                    .IdProducto = CInt(CType(DGV_SL_UMP.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                    .IdTienda = CInt(cmbTienda.SelectedValue)
                    .IdTipoPv = CInt(cmbTipoPrecioVenta_SubLinea.SelectedValue)
                    .IdUnidadMedida = CInt(CType(DGV_SL_UMP.Rows(i).Cells(2).FindControl("hddIdUMPrincipal_SL_UMP"), HiddenField).Value)
                    .IdUsuario = CInt(Session("IdUsuario").ToString)
                    .PUtilFijo = CDec(CType(DGV_SL_UMP.Rows(i).Cells(4).FindControl("txtPorcentUtilidad_SL_UMP"), TextBox).Text)
                    .PUtilVariable = 0
                    .Utilidad = CDec(CType(DGV_SL_UMP.Rows(i).Cells(5).FindControl("txtUtilidad_SL_UMP"), TextBox).Text)

                    .IdMoneda = CInt(CType(DGV_SL_UMP.Rows(i).Cells(8).FindControl("cboMonedaDestino_SL_UMP"), DropDownList).SelectedValue)

                    If .IdMoneda = 1 Then '********* SOLES

                        .Valor = CDec(CType(DGV_SL_UMP.Rows(i).Cells(7).FindControl("txtPVFinal_Soles_SL_UMP"), TextBox).Text)

                    ElseIf .IdMoneda = 2 Then '******** DOLARES

                        .Valor = CDec(CType(DGV_SL_UMP.Rows(i).Cells(6).FindControl("txtPVFinal_Dolares_SL_UMP"), TextBox).Text)

                    Else
                        Throw New Exception("PROBLEMAS EN LA LECTURA DE LA MONEDA DESTINO. MONEDAS PERMITIDAS ( S/. ) ( $ ).")
                    End If

                    .UnidadPrincipal = True
                    .Retazo = False

                End With

                lista.Add(objProdTipoPV)


            Next

            If (New Negocio.ProductoTipoPV).InsertaProductoTipoPVT(lista, Me.cb_CambiarxEquivalencia.Checked) Then
                objScript.mostrarMsjAlerta(Me, "La operación se realizó con éxito.")
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub registrarAsigPreciosxUMO()

        Dim lista As New List(Of Entidades.ProductoTipoPV)
        Try
            For i As Integer = 0 To DGV_SL_UMO.Rows.Count - 1

                Dim objProdTipoPV As New Entidades.ProductoTipoPV
                With objProdTipoPV
                    .Estado = CStr(IIf((CType(DGV_SL_UMO.Rows(i).Cells(8).FindControl("chbEstadoUMO"), CheckBox).Checked) = True, "1", "0"))
                    .IdProducto = CInt(CType(DGV_SL_UMO.Rows(i).FindControl("hddIdProducto1"), HiddenField).Value)
                    .IdTienda = CInt(cmbTienda.SelectedValue)
                    .IdTipoPv = CInt(cmbTipoPrecioVenta_SubLinea.SelectedValue)
                    .IdUnidadMedida = CInt(CType(DGV_SL_UMO.Rows(i).Cells(2).FindControl("hddIdUMPrincipal_SL_UMO"), HiddenField).Value)
                    .IdUsuario = CInt(Session("IdUsuario").ToString)
                    .IdMoneda = CInt(CType(DGV_SL_UMO.Rows(i).Cells(6).FindControl("cboMonedaDestino_SL_UMO"), DropDownList).SelectedValue)

                    If .IdMoneda = 1 Then '********* SOLES
                        .Valor = CDec(CType(DGV_SL_UMO.Rows(i).Cells(5).FindControl("txtPVFinal_Soles_SL_UMO"), TextBox).Text)
                    ElseIf .IdMoneda = 2 Then '******** DOLARES
                        .Valor = CDec(CType(DGV_SL_UMO.Rows(i).Cells(4).FindControl("txtPVFinal_Dolares_SL_UMO"), TextBox).Text)
                    Else
                        Throw New Exception("PROBLEMAS EN LA LECTURA DE LA MONEDA DESTINO. MONEDAS PERMITIDAS ( S/. ) ( $ ).")
                    End If

                    .UnidadPrincipal = True
                    .Retazo = False

                End With
                lista.Add(objProdTipoPV)


            Next

            If ((New Negocio.ProductoTipoPV).InsertaProductoTipoPVT(lista)) Then
                objScript.mostrarMsjAlerta(Me, "La operación se realizó con éxito.")
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub registrarAsigPreciosxPR()

        Try
            saveListaProdTipoPV()
            Me.listaProdTipoPV = getListaProdTipoPV()

            If ((New Negocio.ProductoTipoPV).InsertaProductoTipoPVTxProducto(Me.listaProdTipoPV, CInt(Me.hddIdProducto.Value), CInt(Me.cmbTienda_Prod.SelectedValue), CInt(Me.cmbTipoPrecioVenta_Prod.SelectedValue))) Then

                objScript.mostrarMsjAlerta(Me, "La operación se realizó con éxito.")

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        verFrmInicio()
    End Sub
    Protected Sub btnAtras_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras.Click
        Atras_SL_UMP()


    End Sub
    Private Sub Atras_SL_UMP()

        Panel_PorSubLinea_Det.Visible = False
        Panel_PorUM_Det.Visible = False
        Panel_PorSubLinea_Cab.Enabled = True
        btnGuardar.Visible = False
        btnCancelar.Visible = False

    End Sub
    Private Sub Atras_SL_UMO()

        Panel_PorSubLinea_Det.Visible = False
        Panel_PorUM_Det.Visible = False
        Panel_PorSubLinea_Cab.Enabled = True
        btnGuardar.Visible = False
        btnCancelar.Visible = False

    End Sub
    Protected Sub btnAtras_Buscarpor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras_Buscarpor.Click
        Panel_AsignarPreciosPor.Enabled = True
        Panel_PorSubLinea.Visible = False
    End Sub




    Private Sub cargarDatosProdTipoPVxIdProductoxIdTiendaxIdTipoPV(ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipoPV As Integer, ByVal grilla As GridView)

        Dim objProductoTipoPV As New Negocio.ProductoTipoPV

        Me.listaProdTipoPV = objProductoTipoPV.SelectxIdProductoxIdTiendaxIdTipoPV(idproducto, idtienda, idtipoPV)

        setListaProdTipoPV(Me.listaProdTipoPV)

        grilla.DataSource = Me.listaProdTipoPV
        grilla.DataBind()
    End Sub
    Private Sub btnAtras_ProdCabecera_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras_ProdCabecera.Click
        Panel_Prod_Cab_P.Visible = False
        Panel_AsignarPreciosPor.Enabled = True
    End Sub
    Protected Sub btnAceptar_ProdCabecera_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_ProdCabecera.Click
        Try


            cargarDatosUMedidaxIdProducto(Me.cmbUMedida_Prod, CInt(Me.hddIdProducto.Value))

            cargarDatosProdTipoPVxIdProductoxIdTiendaxIdTipoPV(CInt(Me.hddIdProducto.Value), CInt(cmbTienda_Prod.SelectedValue), CInt(cmbTipoPrecioVenta_Prod.SelectedValue), Me.DGV_PRODUCTO)
            Panel_Prod_Cab_P.Enabled = False
            Panel_ProductoDetalle.Visible = True
            btnGuardar.Visible = True
            btnCancelar.Visible = True


            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularPrecioVenta_PR('event','5','1','1');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnAtras_ProdDetalle_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras_ProdDetalle.Click
        Panel_ProductoDetalle.Visible = False
        Panel_Prod_Cab_P.Enabled = True
        btnGuardar.Visible = False
        btnCancelar.Visible = False
    End Sub
    Protected Sub btnAgregar_UMProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar_UMProd.Click



        '*********** Guardamos las ediciones hechas
        saveListaProdTipoPV()

        Try
            Me.listaProdTipoPV = Me.getListaProdTipoPV

            Dim obj As New Entidades.ProductoTipoPV

            Dim objProductoUM As Entidades.ProductoUM = (New Negocio.ProductoUM).SelectxIdProductoxIdUM(CInt(Me.hddIdProducto.Value), CInt(cmbUMedida_Prod.SelectedValue))

            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {27})

            Dim IdTiendaPrincipal As Integer = 0
            If IsNumeric(listaParametros(0)) Then IdTiendaPrincipal = CInt(listaParametros(0))

            obj.IdUnidadMedida = objProductoUM.IdUnidadMedida
            obj.NomUMedida = objProductoUM.NomUnidadMedida
            obj.PUtilFijo = 0
            obj.PUtilVariable = 0
            obj.Utilidad = 0
            obj.Valor = 0
            obj.PV_Soles = 0
            obj.PV_Dolares = 0

            obj.IdMoneda = 1  '********** SOLES por defecto
            obj.Estado = "1"

            obj.Equivalencia = objProductoUM.Equivalencia

            obj.PorcentRetazo = objProductoUM.PorcentRetazo

            obj.NomMonedaPC = HttpUtility.HtmlDecode(Me.txtMoneda_PrecioCompra.Text)

            obj.UnidadPrincipal = objProductoUM.UnidadPrincipal
            obj.Retazo = objProductoUM.Retazo

            obj.IdProducto = CInt(Me.hddIdProducto.Value)
            obj.IdTienda = CInt(Me.cmbTienda_Prod.SelectedValue)
            obj.IdTipoPv = CInt(Me.cmbTipoPrecioVenta_Prod.SelectedValue)


            obj.IdUsuario = CInt(Session("IdUsuario").ToString)

            obj.IdTiendaPrincipal = IdTiendaPrincipal

            If obj.IdTiendaPrincipal <> 0 And obj.IdTiendaPrincipal <> obj.IdTienda Then

                obj.IdMonedaTiendaPrincipal = (New Negocio.Moneda).SelectCboMonedaBase(0).Id
                obj.PrecioTiendaPrincipal = (New Negocio.ProductoTipoPV).SelectPrecioxParams(obj.IdTiendaPrincipal, obj.IdProducto, obj.IdUnidadMedida, obj.IdTipoPv)

                If obj.PrecioTiendaPrincipal <= 0 Then
                    Throw New Exception("El producto no posee precio en la tienda principal. No se permite la operación.")
                End If

            End If

            Me.listaProdTipoPV.Add(obj)
            Me.setListaProdTipoPV(Me.listaProdTipoPV)

            DGV_PRODUCTO.DataSource = Me.listaProdTipoPV
            DGV_PRODUCTO.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub DGV_ProdUM_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_ProdUM.SelectedIndexChanged
        eliminarRegistroGrillaxProducto()
    End Sub
    Private Sub eliminarRegistroGrillaxProducto()
        Dim objScript As New ScriptManagerClass
        Try
            saveListaProdTipoPV()
            Me.listaProdTipoPV = getListaProdTipoPV()
            Me.listaProdTipoPV.RemoveAt(DGV_ProdUM.SelectedIndex)
            setListaProdTipoPV(Me.listaProdTipoPV)
            DGV_ProdUM.DataSource = Me.listaProdTipoPV
            DGV_ProdUM.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub saveListaProdTipoPV()
        Try
            Me.listaProdTipoPV = getListaProdTipoPV()
            For i As Integer = 0 To DGV_PRODUCTO.Rows.Count - 1
                With DGV_PRODUCTO

                    listaProdTipoPV(i).PUtilFijo = CDec(CType(.Rows(i).Cells(6).FindControl("txtPorcentUtilidad_PR"), TextBox).Text)
                    listaProdTipoPV(i).Utilidad = CDec(CType(.Rows(i).Cells(7).FindControl("txtUtilidad_PR"), TextBox).Text)

                    listaProdTipoPV(i).PV_Dolares = CDec(CType(.Rows(i).Cells(8).FindControl("txtPVFinal_Dolares_PR"), TextBox).Text)
                    listaProdTipoPV(i).PV_Soles = CDec(CType(.Rows(i).Cells(9).FindControl("txtPVFinal_Soles_PR"), TextBox).Text)

                    listaProdTipoPV(i).IdMoneda = CInt(CType(.Rows(i).Cells(10).FindControl("cboMonedaDestino_PR"), DropDownList).SelectedValue)

                    listaProdTipoPV.Item(i).Estado = CStr(IIf(CType(.Rows(i).Cells(11).FindControl("chbEstado_PR"), CheckBox).Checked = True, "1", "0"))

                    listaProdTipoPV.Item(i).IdUsuario = Session("IdUsuario")

                    If listaProdTipoPV(i).IdMoneda = 1 Then

                        '*********** SOLES
                        listaProdTipoPV(i).Valor = listaProdTipoPV(i).PV_Soles

                    ElseIf listaProdTipoPV(i).IdMoneda = 2 Then

                        '************ DOLARES
                        listaProdTipoPV(i).Valor = listaProdTipoPV(i).PV_Dolares

                    End If

                End With
            Next
            setListaProdTipoPV(Me.listaProdTipoPV)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAtras_SubLinea_retazo_Detalle_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras_SubLinea_retazo_Detalle.Click
        Panel_SubLineaxRetazo.Visible = False
        Panel_PorSubLinea_Cab.Enabled = True
        btnGuardar.Visible = False
        btnCancelar.Visible = False
    End Sub

    Private Sub DGV_SL_UMP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_SL_UMP.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboMonedaPV As DropDownList
                Dim gvrow As GridViewRow = CType(e.Row.Cells(8).NamingContainer, GridViewRow)
                cboMonedaPV = CType(gvrow.FindControl("cboMonedaDestino_SL_UMP"), DropDownList)
                cboMonedaPV.SelectedValue = CStr(Me.listaProdTipoPVView(e.Row.RowIndex).IdMonedaPV)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_PRODUCTO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_PRODUCTO.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboMonedaPV As DropDownList
                Dim gvrow As GridViewRow = CType(e.Row.Cells(10).NamingContainer, GridViewRow)
                cboMonedaPV = CType(gvrow.FindControl("cboMonedaDestino_PR"), DropDownList)
                cboMonedaPV.SelectedValue = CStr(Me.listaProdTipoPV(e.Row.RowIndex).IdMoneda)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub DGV_PRODUCTO_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_PRODUCTO.SelectedIndexChanged

        Try


            Me.listaProdTipoPV = getListaProdTipoPV()

            Me.listaProdTipoPV.RemoveAt(DGV_PRODUCTO.SelectedIndex)

            setListaProdTipoPV(Me.listaProdTipoPV)

            DGV_PRODUCTO.DataSource = Me.listaProdTipoPV
            DGV_PRODUCTO.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try


    End Sub

    Private Sub btnAtras_PVPublico_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras_PVPublico.Click
        Atras_SL_UMP()
    End Sub


#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", -1)
                    ViewState.Add("IdEmpresa_BuscarProducto", -1)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia_BuscarProducto", Me.cboTipoExistencia.SelectedValue)
            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                       ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                       ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, _
                                       ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto_Find.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_AddProd.SelectedIndexChanged
        Try
            Dim IdProducto As Integer = CInt(CType(Me.DGV_AddProd.SelectedRow.FindControl("hddIdProducto"), HiddenField).Value)
            cargarProducto_Frm(IdProducto)
            objScript.offCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarProducto_Frm(ByVal IdProducto As Integer)

        Dim objProducto As Entidades.ProductoView = (New Negocio.Producto).ProductoSelectxIdProducto(IdProducto)
        If (objProducto Is Nothing) Then
            Throw New Exception("EL PRODUCTO SELECCIONADO NO EXISTE. NO SE PERMITE LA OPERACIÓN.")
        End If
        With objProducto

            Me.txtProducto.Text = .Descripcion
            Me.txtCodigoProducto.Text = .Codigo
            Me.txtNomUMPrincipal.Text = .UnidadMedida
            Me.txtMoneda_PrecioCompra.Text = .Moneda
            Me.txtPrecioCompra_prod.Text = CStr(Math.Round(.PrecioCompra, 3))

            Me.hddIdProducto.Value = CStr(.Id)
            Me.hddIdMoneda_PC.Value = CStr(.IdMoneda)
            Me.hddIdUnidadMedida_Principal.Value = CStr(.IdUnidadMedida)

            Me.hddTipoCambio.Value = CStr((New Negocio.Util).CalcularValorxTipoCambio(1, .IdMoneda, 1, "E", (New Negocio.FechaActual).SelectFechaActual()))

        End With

        '************** CARGAMOS EL COMBO DE UNIDAD DE MEDIDA
        Me.cmbUMedida_Prod.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(IdProducto)
        Me.cmbUMedida_Prod.DataBind()

    End Sub
#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub

    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

    Protected Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    'Protected Sub cargar_listview()
    '    actualizar_atributos()
    '    Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
    '    List_Producto_VolVenta = NegVolVenta.SelectxLineaxSubLineaxTipoPVxTienda(IdLinea, IdSublinea, IdTPV, IdTienda, IdtipoExistenciaXsublinea, tableTipoTabla)
    '    'lvProductoVolVenta.DataSource = ListVolVenta.FindAll(AddressOf FiltrarListaxIDLineaxIdSubLineaxIdTPVxIdTienda)
    '    DGV_SL_UMP.DataSource = List_Producto_VolVenta
    '    DGV_SL_UMP.DataBind()
    'End Sub

    Private Sub actualizar_atributos()
        Try
            IdtipoExistenciaXsublinea = CInt(CbotipoExistenciaXsublinea.SelectedValue)
            IdLinea = CInt(Me.cmbLinea.SelectedValue)
            If (Not Me.cmbSubLinea.SelectedValue Is Nothing) And CStr(Me.cmbSubLinea.SelectedValue) <> "" Then
                IdSublinea = CInt(Me.cmbSubLinea.SelectedValue)
            Else
                IdSublinea = 0
            End If
            IdTPV = CInt(Me.cmbTipoPrecioVenta_SubLinea.SelectedValue)
            IdTienda = CInt(Me.cmbTienda.SelectedValue)
        Catch ex As Exception
        End Try
    End Sub

#Region "Busqueda AVANZADA Sublinea"
    Private Function obtenerDataTable_TipoTablaValor_2() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla_2()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function

    Private Sub cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubLinea.SelectedIndexChanged
        Dim obj As New Combo
        obj.LlenarCboTipoTablaxIdSubLinea(CboTipotabla_sublinea, CInt(cmbSubLinea.SelectedValue), False)
        Me.limpiar_BA_SubLinea()
    End Sub
    Private Sub CbotipoExistenciaXsublinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbotipoExistenciaXsublinea.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea, CInt(CbotipoExistenciaXsublinea.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), CInt(CbotipoExistenciaXsublinea.SelectedValue), True)
            objcbo.LlenarCboTipoTablaxIdSubLinea(Me.CboTipotabla_sublinea, CInt(Me.cmbSubLinea.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub limpiar_BA_SubLinea()
        Try

            Me.GvSublinea.DataSource = Nothing
            Me.GvSublinea.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        limpiar_BA_SubLinea()
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Try
            LLenarGrillaTipoTablaValorxsublinea()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub LLenarGrillaTipoTablaValorxsublinea()
        ListaSLTT = getListaTipoTablaValor_2()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea.SelectedValue), CInt(Me.cmbSubLinea.SelectedValue), CInt(Me.CboTipotabla_sublinea.SelectedValue))
            .Nombre = CStr(Me.CboTipotabla_sublinea.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.CboTipotabla_sublinea.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GvSublinea.DataSource = ListaSLTT
        Me.GvSublinea.DataBind()
    End Sub
    Private Function getListaTipoTablaValor_2() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GvSublinea.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTVsublinea"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function
    Private Sub GvSublinea_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvSublinea.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTVsublinea"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GvSublinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvSublinea.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor_2()
            ListaSLTT.RemoveAt(Me.GvSublinea.SelectedIndex)
            Me.GvSublinea.DataSource = ListaSLTT
            Me.GvSublinea.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla_2() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GvSublinea.Rows.Count - 1
            With Me.GvSublinea.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GvSublinea.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GvSublinea.Rows(i).Cells(2).FindControl("cboTTVsublinea"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

#End Region





    Private Sub cbotipotabla_sublinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipotabla_sublinea.SelectedIndexChanged

    End Sub




    Private Sub DGV_SL_UMO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_SL_UMO.RowDataBound
        'Try
        '    If e.Row.RowType = DataControlRowType.DataRow Then

        '        Dim cboMonedaPV As DropDownList
        '        Dim gvrow As GridViewRow = CType(e.Row.Cells(6).NamingContainer, GridViewRow)
        '        cboMonedaPV = CType(gvrow.FindControl("cboMonedaDestino_SL_UMO"), DropDownList)
        '        cboMonedaPV.SelectedValue = CStr(Me.listaProdTipoPVView(e.Row.RowIndex).IdMonedaPV)

        '    End If
        'Catch ex As Exception
        '    objScript.mostrarMsjAlerta(Me, "NO SE PUEDE")
        'End Try
    End Sub

    Private Sub btnAtras_PVUMO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras_PVUMO.Click
        Atras_SL_UMO()
    End Sub






End Class

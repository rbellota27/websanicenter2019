<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmAsignarCajaxUsuario.aspx.vb" Inherits="APPWEB.FrmAsignarCajaxUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td>
                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                    onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                    OnClientClick="return(valSave());" CausesValidation="true" />
                <asp:ImageButton ID="tbnAtras" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                    OnClientClick="return(confirm('Desea retroceder?.'));" onmouseout="this.src='/Imagenes/Arriba_B.JPG';"
                    onmouseover="this.src='/Imagenes/Arriba_A.JPG';" ToolTip="Atr�s" />
            </td>
        </tr>
        <tr class="TituloCelda">
            <td>
                Datos del Usuario
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Cabecera" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="Label1" runat="server" Text="Ap. y Nombres:" CssClass="Label"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNombre" onkeyDown="return (false);" CssClass="TextBox_ReadOnlyLeft"
                                                Width="600px" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtIdPersona" Width="100px" onkeyDown="return (false);" CssClass="TextBox_ReadOnlyLeft"
                                                runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBuscarPersona" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                                                onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                                Visible="True" OnClientClick="return(mostrarCapa());" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label2" CssClass="Label" runat="server" Text="D.N.I.:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDNI" onkeyDown="return (false);" CssClass="TextBox_ReadOnlyLeft"
                                                Width="150px" runat="server"></asp:TextBox>
                                            <asp:Label ID="Label3" CssClass="Label" runat="server" Text="Fecha Alta:"></asp:Label>
                                            <asp:Label ID="lblFechaAlta" CssClass="LabelRojo" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label29" runat="server" Text="Tipo de Asignaci�n:" CssClass="Label"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmbTipoAsignacion" runat="server" AutoPostBack="true" onClick="return( valOnClickCboTipoAsignacion()  );">
                                                <asp:ListItem Selected="True" Value="-1">[ Seleccione un Tipo de Asignaci�n ]</asp:ListItem>
                                                <asp:ListItem Value="0">Empresa - Tienda</asp:ListItem>
                                                <asp:ListItem Value="1">Caja - Usuario</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_AsignarEmpresaTienda" runat="server">
                    <table width="100%">
                        <tr>
                            <td class="TituloCelda">
                                Asignaci�n Empresa - Tienda
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Empresa:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Tienda:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTienda" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAddET" runat="server" OnClientClick="return(  valOnClickAddET()   );"
                                                ImageUrl="~/Imagenes/Agregar_B.JPG" onmouseout="this.src='/Imagenes/Agregar_B.JPG';"
                                                onmouseover="this.src='/Imagenes/Agregar_A.JPG';" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_EmpresaTienda" runat="server" AutoGenerateColumns="false" Width="700px">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ButtonType="Link" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Empresa" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                            <asp:Label ID="lblEmpresa" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"DescEmpresa")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tienda" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTienda")%>' />
                                                            <asp:Label ID="lblTienda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"DescTienda")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Principal" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:CheckBox ID="chbPrincipal_ET" onClick="return(  valOnClickPrincipal(this)   );"
                                                                runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Principal")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Estado" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:CheckBox ID="chbEstado_ET" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_AsignarCajas" runat="server">
                    <table width="100%">
                        <tr class="TituloCelda">
                            <td>
                                Asignaci�n Caja - Usuario
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label26" runat="server" Text="Tienda:" CssClass="Label"></asp:Label>
                                <asp:DropDownList ID="cmbTienda_usuario" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label27" runat="server" Text="Caja:" CssClass="Label"></asp:Label>
                                <asp:DropDownList ID="cmbCaja_User" runat="server">
                                </asp:DropDownList>
                                <asp:ImageButton ID="btnAddTiendaCaja" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                    onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';"
                                    OnClientClick="return(valAddTiendaCaja());" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="DGV_TiendaCajaUser" runat="server" AutoGenerateColumns="false"
                                    Width="700px">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ButtonType="Link" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" HeaderText="Tienda" DataField="NomTienda"
                                            NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" HeaderText="Caja" DataField="Nombre" NullDisplayText="---"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                        <asp:TemplateField HeaderText="Activo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbEstado_TiendaCajaUser" Checked='<%#DataBinder.Eval(Container.DataItem,"getEstadoBoolean")%>'
                                                    runat="server" onclick="return(valEstadoTiendaCaja(event));" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="" DataField="IdTienda" NullDisplayText="0" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" HeaderStyle-Width="0px" />
                                        <asp:BoundField HeaderText="" DataField="IdCaja" NullDisplayText="0" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" HeaderStyle-Width="0px" />
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
        </tr>
    </table>
    <div id="capaBuscarUsuario" style="border: 3px solid blue; padding: 7px; width: 790px;
        height: auto; position: absolute; top: 147px; left: 31px; background-color: white;
        z-index: 3; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaBuscarUsuario'));" />
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="Label28" runat="server" Text="Ap. y Nombres:" CssClass="Label"></asp:Label>
                    <asp:TextBox ID="txtApNombres_UserBuscar" runat="server" Width="450px" onKeyPress="return ( onKeyPressUsuario(this,event) );"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                    <asp:ImageButton ID="btnBuscarUsuarioxApNombre" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                        onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                        Visible="True" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="DGV_UserBuscar" HeaderStyle-Height="25px" RowStyle-Height="20px"
                        runat="server" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" SelectText="Seleccionar" />
                            <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="---" />
                            <asp:BoundField DataField="Nombre" HeaderText="Ap. y Nombres" NullDisplayText="---" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" />
                            <asp:BoundField DataField="Login" HeaderText="Login" NullDisplayText="---" />
                            <asp:BoundField DataField="DescFechaAlta" HeaderText="Fecha Alta" NullDisplayText="---" />
                            <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="---" />
                        </Columns>
                        <RowStyle CssClass="GrillaRow" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function valAceptarTipoAsignacion() {
            var cajaIdPersona = document.getElementById('<%=txtIdPersona.ClientID %>');
            if (cajaIdPersona.value == '' || cajaIdPersona.value.length == 0) {
                alert('No ha seleccionado ning�n usuario.');
                return false;
            }
            return true;
        }
        function valEstadoTiendaCaja(e) {
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;

            var grilla = document.getElementById('<%=DGV_TiendaCajaUser.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[0].id != event_element.id) {
                        rowElem.cells[3].getElementsByTagName('INPUT')[0].checked = false;
                    }
                }
            }
            return true;
        }

        function valAddTiendaCaja() {
            var cboTienda = document.getElementById('<%=cmbTienda_usuario.ClientID %>');
            var cboCaja = document.getElementById('<%=cmbCaja_User.ClientID %>');
            if (parseFloat(cboTienda.value) == 0) {
                alert('Seleccione una Tienda.');
                return false;
            }
            if (parseFloat(cboCaja.value) == 0) {
                alert('Seleccione una Caja.');
                return false;
            }
            //************** validando en la grilla
            var grilla = document.getElementById('<%=DGV_TiendaCajaUser.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[4].innerHTML == cboTienda.value && rowElem.cells[5].innerHTML == cboCaja.value) {
                        alert('La Tienda - Caja seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        function mostrarCapa() {
            onCapa('capaBuscarUsuario');
            document.getElementById('<%=txtApNombres_UserBuscar.ClientID %>').focus();
            document.getElementById('<%=txtApNombres_UserBuscar.ClientID %>').select();
            return false;
        }

        function valOnClickAddET() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');

            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa.');
                return false;
            }

            var cboTienda = document.getElementById('<%=cboTienda.ClientID %>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda.');
                return false;
            }


            var grilla = document.getElementById('<%=GV_EmpresaTienda.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].value) == parseInt(cboEmpresa.value) && parseInt(rowElem.cells[2].children[0].value) == parseInt(cboTienda.value)) {
                        alert('La Empresa / Tienda seleccionada ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valOnClickPrincipal(chbPrincipal) {
            var grilla = document.getElementById('<%=GV_EmpresaTienda.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[0].id != chbPrincipal.id) {
                        rowElem.cells[3].getElementsByTagName('INPUT')[0].checked = false;
                    }
                }
            }
            return true;
        }

        function valOnClickCboTipoAsignacion() {
            var txtIdPersona = document.getElementById('<%=txtIdPersona.ClientID %>');
            if (isNaN(parseInt(txtIdPersona.value)) || txtIdPersona.value.length <= 0) {
                alert('Debe seleccionar una Persona.');
                return false;
            }
            return true;
        }

        function valSave() {
            var cajaIdPersona = document.getElementById('<%=txtIdPersona.ClientID %>');
            if (cajaIdPersona.value == '' || cajaIdPersona.value.length == 0) {
                alert('Debe seleccionar un Usuario.');
                return false;
            }

            var cboTipoAsignacion = document.getElementById('<%=cmbTipoAsignacion.ClientID %>');

            switch (parseInt(cboTipoAsignacion.value)) {
                case -1:  //***** NO se ha seleccionado ninguna Opci�n.
                    alert('Debe seleccionar un Tipo de Configuraci�n.');
                    return false;
                    break;
                case 0:
                    break;
                case 1:
                    break;
            }
            return (confirm('Desea continuar con la operaci�n?'));
        }
        ///
        function onKeyPressUsuario(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {        
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscarUsuarioxApNombre.ClientID %>');
                boton.click();
                return true;
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>

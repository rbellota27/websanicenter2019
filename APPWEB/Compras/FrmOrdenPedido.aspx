<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmOrdenPedido.aspx.vb" Inherits="APPWEB.FrmOrdenPedido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Orden de Pedido
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btNuevo" Width="80px" OnClientClick="return(validarNuevo());" runat="server"
                    Text="Nuevo" />
                <asp:Button ID="btBuscar" Width="80px" runat="server" Text="Buscar" />
                <asp:Button ID="btAnular" Width="80px" runat="server" Text="Anular" />
                <asp:Button ID="btGuardar" Width="80px" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());" />
                <asp:Button ID="btCancelar" OnClientClick="return(confirm('Desea ir al modo Inicio?'));"
                    Width="80px" runat="server" Text="Cancelar" />
                <asp:Button ID="btEditar" Width="80px" runat="server" Text="Editar" OnClientClick="return(validarEditar());" />
                <asp:Button ID="btImprimir" Width="80px" OnClientClick="return(imprimirDocumento());"
                    runat="server" Text="Imprimir" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlDocumento" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlEmpresa" runat="server" Font-Bold="true" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlTienda" runat="server" Font-Bold="true" DataValueField="ID"
                                    DataTextField="Nombre" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlSerie" runat="server" Font-Bold="true" Width="100px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="tbCodigoDocumento" CssClass="TextBox_ReadOnly" onKeypress="return(validarCajaBusqueda());"
                                    runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumento" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Contacto" Style="height: 16px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi&oacute;n:
                            </td>
                            <td>
                                <asp:TextBox ID="tbFechaEmision" runat="server" Enabled="false" Width="100px" CssClass="TextBox_Fecha"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                Almacen:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlAlmacen" Width="100%" Font-Bold="true" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlEstadoDocumento" runat="server" Font-Bold="true" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tipo Operacion:
                            </td>
                            <td colspan="3">
                                <asp:DropDownList Width="100%" Enabled="false" Font-Bold="true" ID="dlOperacion"
                                    runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="panelPrincipal" runat="server">
                    <table width="100%">                                                                
                        <%--Datos adicionales--%>
                        <tr>
                            <td>
                                <asp:Panel ID="pnlProgPedido" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td class="TituloCelda" colspan="5">
                                                Programaci&oacute;n Semana
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTdLeft">
                                                Semana:
                                                <asp:DropDownList ID="dlweek" runat="server" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTdLeft">
                                                Mes:
                                                <asp:DropDownList ID="dlmonth" runat="server" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTdLeft">
                                                Fecha de Inicio:
                                                <asp:TextBox ID="tbfechaini" Enabled="false" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="LabelTdLeft">
                                                Fecha Fin:
                                                <asp:TextBox ID="tbfechafin" Enabled="false" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="LabelTdLeft">
                                                Dias Util:
                                                <asp:TextBox ID="tbutilday" runat="server" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlDocRef" runat="server">
                                    <table width="100%">                                        
                                        <tr>
                                            <td class="TituloCelda" colspan="5">
                                                Documento de Referencia
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="Texto">
                                                Comprometer Stock:
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="ckcomprometer" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="Texto">
                                                Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbserieref" Width="100px" onKeyPress="return(false);" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="right" class="Texto">
                                                Codigo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbcodigoref" Width="100px" onKeyPress="return(false);" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btbuscardocref" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                    OnClientClick="return(opendocref());" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                    onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="Texto">
                                                Tipo de Documento:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbdocumento" Width="100%" onKeyPress="return(false);" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="right" class="Texto">
                                                Cliente:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbclienteref" Width="100%" onKeyPress="return(false);" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                         <%--Datos del destinatario--%>
                        <tr>
                            <td class="TituloCelda">
                                <asp:Label ID="lbltipo" CssClass="TituloCeldaLeft" Font-Size="Small" runat="server"
                                    Text=""></asp:Label>
                            </td>
                        </tr>  
                          <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Tienda:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlTienda2" runat="server" DataValueField="ID" DataTextField="Nombre"
                                                onFocus="return(validarAlmacen2());" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" class="Texto">
                                            Almacen:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlAlmacen2" runat="server" Width="180px" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--Listado de productos--%>
                        <tr>
                            <td class="TituloCelda">
                                Listado de Productos
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                    OnClientClick="return(mostrarCapaListaProducto());" onmouseout="this.src='/Imagenes/BuscarProducto_b.JPG';"
                                    onmouseover="this.src='/Imagenes/BuscarProducto_A.JPG';" />
                            </td>
                        </tr>
                        <tr>
                            <td style="margin-left: 40px">
                                <asp:GridView ID="gvSucursal" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    GridLines="None" Style="margin-top: 0px" Width="95%">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True">
                                            <ItemStyle HorizontalAlign="right" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="IdProducto" HeaderText="Codigo">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NomProducto" HeaderText="Descripcion"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Stock Disponible Origen">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbstockorigen" runat="server" Enabled="false" Text='<%# DataBinder.Eval(Container.DataItem,"StockDisponibleN","{0:F2}") %>'
                                                    Width="75px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Stock Disponible Destino">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbstockdestino" runat="server" Enabled="false" Text='<%# DataBinder.Eval(Container.DataItem,"Stock","{0:F2}") %>'
                                                    Width="75px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CantxAtender" HeaderText="Pendiente" DataFormatString="{0:F2}">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UMedida" HeaderText="U. Medida" />
                                        <asp:TemplateField HeaderText="Cant. Solicitada" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtCant" runat="server" Height="20px" MaxLength="7" onblur="return(valBlur(event));"
                                                    onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"cantSolicitada","{0:F2}") %>' Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" Wrap="True" />
                                </asp:GridView>
                                <asp:GridView ID="gvProducto" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    GridLines="None" Style="margin-top: 0px" Width="95%">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True">
                                            <ItemStyle HorizontalAlign="right" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="IdProducto" HeaderText="Codigo">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="UM">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="dlum" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUMedida")%>'
                                                    DataTextField="DescripcionCorto" DataValueField="Id" Width="70px">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cant. Solicitada" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtCant" runat="server" Height="20px" MaxLength="7" onblur="return(valBlur(event));"
                                                    onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>' Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" Wrap="True" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <%--Observaciones--%>
                        <tr>
                            <td class="TituloCelda">
                                Observaciones
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtobservacion" runat="server" MaxLength="150" TextMode="MultiLine"
                                    Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="hiden" UpdateMode="Always" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hddidObs" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdd_operativo" runat="server" Value="0" />
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" Value="0" />
                                    <asp:HiddenField ID="hddIdAlmacen" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdd_estadoentrega" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdd_iddocumentoref" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdd_idtiendavalidar" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdd_idsemana" runat="server" Value = "0" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <%--Capa de busqueda de productos--%>
    <div id="verBusquedaProducto" style="border: 3px solid blue; padding: 8px; width: 750px;
        height: auto; position: absolute; top: 237px; left: 38px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="imgCerrarCapaProducto" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';" />
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td align="right" class="Texto">
                                Linea:
                                <asp:DropDownList ID="cmbLinea_AddProd" runat="server" AutoPostBack="True" DataTextField="Descripcion"
                                    DataValueField="Id" Width="180px">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Sub Linea:
                                <asp:DropDownList ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    DataValueField="Id" Width="180px">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Cod.:
                                <asp:TextBox ID="txtCodigoSubLinea_AddProd" runat="server" onKeypress="return(onKeyPressEsNumero('event'));"
                                    Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto" colspan="2">
                                Descripcion:
                                <asp:TextBox ID="txtDescripcionProd_AddProd" runat="server" Width="300px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseout="this.src='/Imagenes/Buscar_b.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                    onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';" />
                                <asp:ImageButton ID="btLimpiar" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                    OnClientClick="return(LimpiarGrillaProductos());" onmouseout="this.src='/Imagenes/Limpiar_B.JPG';"
                                    onmouseover="this.src='/Imagenes/Limpiar_A.JPG';" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="gvListaProducto" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    PageSize="20" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Cantidad">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbcantFiltro" runat="server" onblur="return(valBlur(event));" onfocus="return(aceptarFoco(this));"
                                                    onKeypress="return(validarNumeroPunto(event));" Text='<%# DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}") %>'
                                                    Width="70px"></asp:TextBox>
                                                <asp:HiddenField ID="ghdd_stock" runat="server" 
                                                    Value='<%# DataBinder.Eval(Container.DataItem,"StockAReal","{0:F2}") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="IdProducto" HeaderText="Codigo">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion">
                                            <ItemStyle HorizontalAlign="right" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="UM">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="dlmedida" runat="server" Width="60px" DataValueField="IdUnidadMedida"
                                                    DataTextField="NombreCortoUM" DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaUM") %>'>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                         <asp:BoundField DataField="StockDisponibleN" HeaderText="stock" DataFormatString="{0:F2}" >
                                         </asp:BoundField>
                                        <asp:BoundField DataField="NomLinea" HeaderText="Linea">
                                            <ItemStyle HorizontalAlign="right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NomSubLinea" HeaderText="SubLinea">
                                            <ItemStyle HorizontalAlign="right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Button ID="btnAnterior_Productos" runat="server" Font-Bold="true" OnClientClick="return(valNavegacionProductos('0'));"
                                    Text="&lt;" ToolTip="P�gina Anterior" Width="50px" />
                                <asp:Button ID="btnPosterior_Productos" runat="server" Font-Bold="true" OnClientClick="return(valNavegacionProductos('1'));"
                                    Text="&gt;" ToolTip="P�gina Posterior" Width="50px" />
                                <asp:TextBox ID="txtPageIndex_Productos" runat="server" Width="50px"></asp:TextBox>
                                <asp:Button ID="btnIr_Productos" runat="server" Font-Bold="true" OnClientClick="return(valNavegacionProductos('2'));"
                                    Text="Ir" ToolTip="Ir a la P�gina" Width="50px" />
                                <asp:TextBox ID="txtPageIndexGO_Productos" runat="server" CssClass="TextBox_ReadOnly"
                                    onKeyPress="return(onKeyPressEsNumero('event'));" Width="50px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divdocref" style="border: 3px solid blue; padding: 8px; width: 750px; height: auto;
        position: absolute; top: 237px; left: 38px; background-color: white; z-index: 2;
        display: none;">
        <table style="width: 100%;">
            <tr>
                <td colspan="3" align="right">
                    <asp:ImageButton ID="btcerrardocref" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        OnClientClick="return(offCapa('divdocref'));" onmouseout="this.src='/Imagenes/Cerrar_B.JPG';"
                        onmouseover="this.src='/Imagenes/Cerrar_A.JPG';" />
                </td>
            </tr>
            <tr>
                <td class="TituloCeldaLeft" colspan="3">
                    Busqueda de Documentos
                </td>
            </tr>
            <tr>
                <td align="right" class="Texto">
                    Serie:
                    <asp:TextBox ID="tbbuscarserieref" onKeyPress="return(irdocref());" MaxLength="4"
                        runat="server"></asp:TextBox>
                </td>
                <td align="right" class="Texto">
                    Codigo:
                    <asp:TextBox ID="tbbuscarcodigoref" onKeyPress="return(irdocref());" MaxLength="12"
                        runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:ImageButton ID="btEncontrarDocref" OnClientClick="return(validarDocref());"
                        ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:GridView ID="gvdocref" runat="server" Width="100%" EmptyDataText="No se hallaron registros"
                        GridLines="None" AutoGenerateColumns="False">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddiddocumentoref" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdDocumento") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Serie" HeaderText="Serie" />
                            <asp:BoundField DataField="Codigo" HeaderText="Codigo" />
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Documento" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="F. Emision" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="RazonSocial" HeaderText="Cliente" />
                            <asp:BoundField DataField="RUC" HeaderText="RUC / DNI" NullDisplayText="---" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgConsultar" runat="server" ToolTip="Consultar Orden de Pedido"
                                        ImageUrl="~/Imagenes/Ok_b.bmp" OnClick="imgConsultar_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Button ID="btanterior_docref" runat="server" Font-Bold="true" OnClientClick="return(valNavegaciondocref('0'));"
                        Text="&lt;" ToolTip="P�gina Anterior" Width="50px" />
                    <asp:Button ID="btposterior_docref" runat="server" Font-Bold="true" OnClientClick="return(valNavegaciondocref('1'));"
                        Text="&gt;" ToolTip="P�gina Posterior" Width="50px" />
                    <asp:TextBox ID="tbpageindex_docref" runat="server" Width="50px"></asp:TextBox>
                    <asp:Button ID="btir_docref" runat="server" Font-Bold="true" OnClientClick="return(valNavegaciondocref('2'));"
                        Text="Ir" ToolTip="Ir a la P�gina" Width="50px" />
                    <asp:TextBox ID="tbpageindexgo_docref" runat="server" CssClass="TextBox_ReadOnly"
                        onKeyPress="return(onKeyPressEsNumero('event'));" Width="50px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaRelacionDocumento" style="border: 3px solid blue; padding: 8px; width: 550px;
        height: auto; position:absolute; top: 250px; left: 45px; background-color: white;
        z-index: 6; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="imCerrar" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        OnClientClick="return(offCapa2('capaRelacionDocumento'));" onmouseout="this.src='/Imagenes/Cerrar_B.JPG';"
                        onmouseover="this.src='/Imagenes/Cerrar_A.JPG';" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Documento de Orden de pedido Relacionado
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvRelacionDocumento" runat="server" Width="100%" GridLines="None"
                        AutoGenerateColumns="False">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_iddocOrden" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Serie" HeaderText="Serie" />
                            <asp:BoundField DataField="Codigo" HeaderText="Codigo" />
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Documento" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="F. Emision" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="NomAlmacen" HeaderText="Almacen" />
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        function mostrarCapaListaProducto() {
            var tienda2 = document.getElementById('<%=dlAlmacen2.ClientID %>');
            if (tienda2.value == '' || tienda2.value == '0') {
                alert("No ha seleccionado un almacen");
                tienda2.focus();
                return false;
            }
            onCapa('verBusquedaProducto');
            return false;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function valNavegacionProductos(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++
        function valNavegaciondocref(tipoMov) {
            var index = parseInt(document.getElementById('<%=tbpageindex_docref.ClientID %>').value);

            if (isNaN(index) || index.length == 0 || index == null) {
                alert('Ingrese una P�gina de navegaci�n.');
                document.getElementById('<%=tbpageindexgo_docref.ClientID%>').select();
                document.getElementById('<%=tbpageindexgo_docref.ClientID%>').focus();
                return false;
            }

            switch (tipoMov) {
                case '0': //anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '2': //ir
                    index = parseInt(document.getElementById('<%=tbpageindexgo_docref.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbpageindexgo_docref.ClientID%>').select();
                        document.getElementById('<%=tbpageindexgo_docref.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbpageindexgo_docref.ClientID%>').select();
                        document.getElementById('<%=tbpageindexgo_docref.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++
        function setIdAlmacen(drop) {
            almacen = document.getElementById('<%=dlAlmacen.ClientID  %>');
            almacen2 = document.getElementById('<%=dlAlmacen2.ClientID  %>');
            if (almacen.value == almacen2.value) {
                almacen2.selectedIndex = 0;
                alert('No se puede seleccionar el mismo [ Almacen ]');
                return false;
            }

            document.getElementById('<%=hddIdAlmacen.ClientID %>').value = drop.value;
            return true;

        }

        function validarAlmacen2() {
            var idalmacen2 = document.getElementById('<%=hddIdAlmacen.ClientID %>');
            var operativo = document.getElementById('<%=hdd_operativo.ClientID %>');
            var tb = document.getElementById('<%=txtobservacion.ClientID %>');
            var productos = document.getElementById('<%=gvProducto.ClientID %>');

            if (productos != null) {
                if (idalmacen2.value != '0' && (operativo.value == '1' || operativo.value == '2')) {
                    var tbdesc = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID %>');
                    var almacen2 = document.getElementById('<%=dlalmacen2.ClientID %>');
                    alert("La Cuadricula de productos ya contiene consulta de productos del almacen " + almacen2.options[almacen2.selectedIndex].text);
                    tb.focus();
                    return false;
                }
            }
            if (operativo.value == '5') {
                alert("No se puede editar este control");
                tb.focus();
                return false;
            }
            return true;
        }
        //++++++++++++++++++++++++++
        function opendocref() {
            var almacen2 = document.getElementById('<%=dlalmacen2.ClientID %>');
            if (almacen2.value == '' || almacen2.value == '0') {
                alert('Debe seleccionar un almacen');
                almacen2.focus();
                return false;
            }
            onCapa('divdocref');
            document.getElementById('<%=tbbuscarserieref.ClientID %>').select();
            document.getElementById('<%=tbbuscarserieref.ClientID %>').focus();
            return false;
        }
        //++++++++++++++++++++++++++
        function validarGuardar() {
            var idserie = document.getElementById('<%=dlSerie.ClientID %>');
            if (idserie.value == '' || idserie.value == '0') {
                alert('No se ha generado la serie para el documento');
                return false;
            }

            var operacion = document.getElementById('<%=dlOperacion.ClientID %>');
            switch (operacion.value) {
                case '18':  //pedido entre sucursales
                    var iddocref = document.getElementById('<%=hdd_iddocumentoref.ClientID %>');
                    if (iddocref.value == '0') {
                        alert("No ha buscado el documento de referecia");
                        return false;
                    }
                    var estadoEnt = document.getElementById('<%=hdd_estadoentrega.ClientID %>');
                    if (estadoEnt.value == '2') {
                        alert('Los productos han sido entregados no se puede editar');
                    }

                    var grillax = document.getElementById('<%=gvSucursal.ClientID %>');
                    if (grillax != null) {
                        for (var i = 1; i < grillax.rows.length; i++) {
                            var rowElem = grillax.rows[i];
                            var stockDestino = parseFloat(rowElem.cells[4].children[0].value);
                            if (isNaN(stockDestino)) {
                                stockDestino = 0;
                            }

                            var cantidadSolicitante = parseFloat(rowElem.cells[7].children[0].value);
                            if (isNaN(cantidadSolicitante)) {
                                stockDestino = 0;
                            }

                            if (cantidadSolicitante == 0) {
                                alert('Debe Ingresar una cantidad mayor a cero');
                                return false;
                            }
                            if (cantidadSolicitante > stockDestino) {
                                alert('Debe la cantidad solicitante no puede ser mayor al stock disponible del destino');
                                return false;
                            }
                        }
                    } else {
                        alert('la cuadricula de productos esta vacia');
                        return false;
                    }

                    break;

                case '19':  //pedido al centro distribuidor
                    var grilla = document.getElementById('<%=gvProducto.ClientID %>');
                    if (grilla == null) {
                        alert("La cuadricula de productos esta vacia");
                        return false;
                    } else {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];

                            var cantidad = redondear(parseFloat(rowElem.cells[4].children[0].value), 2);
                            if (isNaN(cantidad)) {
                                cantidad = 0;
                            }
                            if (cantidad == 0) {
                                alert(' Ingrese una cantidad mayor a cero para el producto ' + rowElem.cells[2].innerHTML + '.');
                                rowElem.cells[4].children[0].select();
                                rowElem.cells[4].children[0].focus();
                                return false;
                            }

                        }
                    }
                    ////
                    var week = document.getElementById('<%=dlweek.ClientID %>');
                    if (week.value == '' || week.value == '0') {
                        alert('No ha seleccionado la semana');
                        week.focus();
                        return false;
                    }
                    break;
            }
            return confirm("Desea Continuar con la operacion");
        }

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function imprimirDocumento() {
            var idDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>').value;
            window.open('Reportes/visorCompras.aspx?iReporte=3&IdDoc=' + idDocumento, 'Pedido', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        //+++++++++++++++++++++++++++++
        function validarNuevo() {
            var operacion = document.getElementById('<%=dlOperacion.ClientID %>');
            if (operacion.value == '0') {
                alert('No ha seleccionado una operacion en el documento');
                operacion.focus();
                return false;
            }
            return true;
        }
        //+++++++++++++++++++++++++++++
        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarDocumento.ClientID %>');
                boton.focus();
                return true;
            }
        }
        //++++++++++++++++++++++++++++
        function irdocref() {
            var key = event.KeyCode;
            if (key == 13) {
                document.getElementById('<%=btEncontrarDocref.ClientID %>').focus();
                return true;
            }
        }
        //++++++++++++++++++++++++++
        function validarDocref() {
            var serie = document.getElementById('<%=tbbuscarserieref.ClientID %>');
            var codigo = document.getElementById('<%=tbbuscarcodigoref.ClientID %>');
            if (serie.value.length > 0) {
                if (isNaN(parseInt(serie.value))) {
                    alert('verifique la serie');
                    serie.select();
                    serie.focus()
                    return false;
                }
            }
            if (codigo.value.length > 0) {
                if (isNaN(parseInt(codigo.value))) {
                    alert('verifique el codigo');
                    codigo.select();
                    codigo.focus();
                    return false;
                }
            }
            return true
        }
        //++++++++++++++++++++++++++
        function LimpiarGrillaProductos() {
            var grilla = document.getElementById('<%=gvListaProducto.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    rowElem.cells[0].children[0].value = '0.00';
                } //next
            } //end if
            return false;
        } //end function
        //+++++++++++++++++++++++++++++++++
        function validarEditar() {
            var estado = document.getElementById('<%=dlEstadoDocumento.ClientID %>');
            if (estado.value == '2') {
                alert('El estado no permite la edici�n del documento');
                return false;
            }
            return true;
        }
    </script>

</asp:Content>

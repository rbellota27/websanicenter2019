Partial Public Class frmOrdenCompraLista
    Inherits System.Web.UI.Page

#Region "variables"
    Private objScript As New ScriptManagerClass
    Private drop As Combo
    Private ListaOrdenesCompra As List(Of Entidades.DocumentoView)

#End Region

#Region "Procedimientos"

    Private Sub CargarAlIniciar()
        drop = New Combo
        drop.LLenarCboEstadoEntrega(dlestEntrega, True)
        drop.LLenarCboEstadoCancelacion(dlestCancelacion, True)

        Me.txt_FechaIni.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txt_FechaFin.Text = Me.txt_FechaIni.Text

    End Sub

    Private Sub verOrdenesCompra(ByVal gv As GridView, ByVal descripcion As String, ByVal tipo As Integer, _
                                 ByVal estEntrega As Integer, ByVal estCancelacion As Integer, ByVal tipomov As Integer)

        Dim index% = 0
        Select Case tipomov
            Case 0
                index = 0
            Case 1
                index = (CInt(tbPageIndex.Text) - 1) - 1
            Case 2
                index = (CInt(tbPageIndex.Text) - 1) + 1
            Case 3
                index = (CInt(tbPageIndex.Text) - 1)

        End Select

        ListaOrdenesCompra = (New Negocio.OrdenCompra).listarOrdenesCompraxFiltro(descripcion, CInt(ddl_OrdenCompra.SelectedValue), CInt(dlestEntrega.SelectedValue), CInt(dlestCancelacion.SelectedValue), index, gv.PageSize)
        If ListaOrdenesCompra.Count > 0 Then
            gv.DataSource = ListaOrdenesCompra
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
        Else
            gv.DataSource = Nothing
            gv.DataBind()
            objScript.mostrarMsjAlerta(Me, "No se encontraron registros")
        End If

    End Sub

#End Region

#Region "Eventos Principales"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                CargarAlIniciar()
            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub ddl_OrdenCompra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddl_OrdenCompra.SelectedIndexChanged
        '***** Muestra el text para efectuar el filtro
        Select Case CInt(ddl_OrdenCompra.SelectedValue)
            Case 0 ' Todas las Ordenes de Compra 
                txt_FiltroOc.Text = ""
                txt_FechaFin.Visible = False
                txt_FiltroOc.Visible = False
                txt_FechaIni.Visible = False

            Case 1 ' R.U.C. Proveedor
                txt_FiltroOc.MaxLength = 11
                txt_FiltroOc.Text = ""
                txt_FiltroOc.Visible = True
                txt_FechaIni.Visible = False
                txt_FechaFin.Visible = False

            Case 2 ' Nombre/R. Social
                txt_FiltroOc.MaxLength = 35
                txt_FiltroOc.Text = ""
                txt_FiltroOc.Visible = True
                txt_FechaIni.Visible = False
                txt_FechaFin.Visible = False

            Case 3  ' Rango de fechas              
                txt_FiltroOc.Text = ""
                txt_FiltroOc.Visible = False
                txt_FechaIni.Visible = True
                txt_FechaFin.Visible = True

            Case 4 ' Estado
                txt_FiltroOc.Text = ""
                txt_FechaFin.Visible = False
                txt_FiltroOc.Visible = False
                txt_FechaIni.Visible = False
        End Select
    End Sub

#End Region

#Region "ver ordenes de compra"

    Private Sub cmd_AplicaFiltroOc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_AplicaFiltroOc.Click

        Try
            ViewState.Add("tipo", ddl_OrdenCompra.SelectedValue)
            Dim desc$
            If CInt(ddl_OrdenCompra.SelectedValue) = 3 Then
                desc = "CONVERT(datetime,'" + txt_FechaIni.Text + "',103) and CONVERT(datetime,'" + txt_FechaFin.Text + "',103)"
            Else
                desc = txt_FiltroOc.Text.Trim
            End If
            ViewState.Add("descripcion", desc)
            ViewState.Add("entrega", dlestEntrega.SelectedValue)
            ViewState.Add("cancelacion", dlestCancelacion.SelectedValue)

            verOrdenesCompra(gvOrdenCompra, CStr(ViewState("descripcion")), CInt(ViewState("tipo")), CInt(ViewState("entrega")), CInt(ViewState("cancelacion")), 0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            verOrdenesCompra(gvOrdenCompra, CStr(ViewState("descripcion")), CInt(ViewState("tipo")), CInt(ViewState("entrega")), CInt(ViewState("cancelacion")), 1)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            verOrdenesCompra(gvOrdenCompra, CStr(ViewState("descripcion")), CInt(ViewState("tipo")), CInt(ViewState("entrega")), CInt(ViewState("cancelacion")), 2)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btIr_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btIr.Click
        Try
            verOrdenesCompra(gvOrdenCompra, CStr(ViewState("descripcion")), CInt(ViewState("tipo")), CInt(ViewState("entrega")), CInt(ViewState("cancelacion")), 3)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="VisorGrilla.aspx.vb"
    Inherits="APPWEB.FrmdgvCompAnualCompras" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblTitulo" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="dgwComparativoAnualCompras" runat="server" AutoGenerateColumns="true"
                        HeaderStyle-BackColor="SkyBlue" Width="100%" CellPadding="0" CellSpacing="0"
                        AllowPaging="false" RowStyle-Height="17px">
                        <HeaderStyle Height="30" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

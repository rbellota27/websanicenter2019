﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmRankingCompras
    Inherits System.Web.UI.Page
    Private objNegUsu As New Negocio.UsuarioView
    Private objscript As New ScriptManagerClass
    Private objCombo As New Combo
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try

            objCombo.LlenarCboPropietario(Me.cmbEmpresa, True)
            'objCombo.LLenarCboTienda(Me.cmbtienda, True)
            'se addd
            If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 5) > 0 Then
                objCombo.LlenarCboTiendaxIdUsuario(Me.cmbtienda, CInt(Session("IdUsuario")), True)
            Else
                objCombo.LlenarCboTiendaxIdUsuario(Me.cmbtienda, CInt(Session("IdUsuario")), False)
            End If

            objCombo.LlenarCboLinea(Me.cboLinea, True)
            objCombo.LlenarCboSubLineaxIdLinea(Me.cboSublinea, CInt(Me.cboLinea.SelectedValue), True)
            objCombo.LlenarCboCondicionPago(Me.cmdTipoOperacion)
            objCombo.LlenarCboMoneda(Me.CmdMoneda, False)
            Dim objFechaActual As New Negocio.FechaActual
            txtFechaInicio.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            Me.hddFechaActual.Value = CStr(Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy"))

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        objCombo.LlenarCboSubLineaxIdLinea(Me.cboSublinea, CInt(Me.cboLinea.SelectedValue), True)
    End Sub
End Class
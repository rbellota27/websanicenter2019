﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Public Class FrmdgvCompAnualCompras
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            Me.lblTitulo.Width = 50
            lblTitulo.ForeColor = Drawing.Color.Black
            lblTitulo.Font.Size = 20
            lblTitulo.Height = 40
            lblTitulo.BackColor = Drawing.Color.SkyBlue


            If iReporte Is Nothing Then
                iReporte = ""
            End If

            ViewState.Add("iReporte", iReporte)
            Select Case iReporte


                Case "1" 'Comparativo anual - compra
                    ViewState.Add("idyear", Request.QueryString("idyear"))
                    ViewState.Add("idtipo", Request.QueryString("idtipo"))
                    ViewState.Add("idproveedor", Request.QueryString("idproveedor"))
                    ViewState.Add("idmoneda", Request.QueryString("idmoneda"))
                    ViewState.Add("idpropietario", Request.QueryString("idpropietario"))
                Case "2"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    'ViewState.Add("textEmpresa", Request.QueryString("textEmpresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    'ViewState.Add("textTienda", Request.QueryString("textTienda"))
                    ViewState.Add("idlinea", Request.QueryString("idlinea"))
                    ViewState.Add("idSublinea", Request.QueryString("idSublinea"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("yeari", Request.QueryString("yeari"))
                    ViewState.Add("semanai", Request.QueryString("semanai"))
                    ViewState.Add("yearf", Request.QueryString("yearf"))
                    ViewState.Add("semanaf", Request.QueryString("semanaf"))
                    ViewState.Add("filtrarsemana", Request.QueryString("filtrarsemana"))
                    ViewState.Add("stockxtienda", Request.QueryString("stockxtienda"))
                    ViewState.Add("nomTienda", Request.QueryString("nomTienda"))
            End Select
        End If

        mostrarReporte()
    End Sub
    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString

            Case "1"
                ReporteComprarativoCompra()
            Case "2"
                reporteVentasStock()

        End Select
    End Sub
    Private Sub ReporteComprarativoCompra()
        Try
            Dim ds As DataSet = (New Negocio.OrdenCompra).getDataSetComparativo(CInt(ViewState("idyear")), _
                                    CStr(ViewState("idtipo")), CInt(ViewState("idproveedor")), _
                                    CInt(ViewState("idmoneda")), CInt(ViewState("idpropietario")))


            If ds IsNot Nothing Then

                Me.lblTitulo.Text = "COMPARATIVO ANUAL DE COMPRAS"

                Me.dgwComparativoAnualCompras.DataSource = ds
                Me.dgwComparativoAnualCompras.DataBind()

                For Each row As GridViewRow In dgwComparativoAnualCompras.Rows
                    'total Sublinea
                    If row.Cells(0).Text = "999999" Then
                        row.BackColor = Drawing.Color.SkyBlue
                        row.Cells(0).Text = ""
                    End If

                Next

                Dim Nombre As String
                Nombre = "ComparativoAnualdeCompras.xls"
                Me.ExportaExcel(Me.dgwComparativoAnualCompras, Nombre)

                'Response.Clear()
                'Response.Buffer = True
                'Response.AddHeader("content-disposition", "attachment;filename=ComparativoAnualdeCompras.xls")
                'Response.Charset = ""
                'Response.ContentType = "application/vnd.ms-excel"

            End If
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message + "');</script>")
        End Try
    End Sub
    Private Sub reporteVentasStock()
        Try

            Dim vidempresa, vidtienda, vlinea, vsublinea As Integer
            Dim stockxtienda As Short = 0

            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If

            If CStr(ViewState.Item("idtienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("idtienda"))
            End If

            If CStr(ViewState.Item("idlinea")) = "" Then
                vlinea = 0
            Else
                vlinea = CInt(ViewState.Item("idlinea"))
            End If
            If CStr(ViewState.Item("idSublinea")) = "" Then
                vsublinea = 0
            Else
                vsublinea = CInt(ViewState.Item("idSublinea"))
            End If

            Dim yeari, yearf, semanai, semanaf, filtrarsemana As Integer

            If CStr(ViewState.Item("yeari")) = "" Then
                yeari = 1900
            Else
                yeari = CInt(ViewState.Item("yeari"))
            End If

            If CStr(ViewState.Item("semanai")) = "" Then
                semanai = 12
            Else
                semanai = CInt(ViewState.Item("semanai"))
            End If

            If CStr(ViewState.Item("yearf")) = "" Then
                yearf = 20100
            Else
                yearf = CInt(ViewState.Item("yearf"))
            End If

            If CStr(ViewState.Item("semanaf")) = "" Then
                semanaf = 12
            Else
                semanaf = CInt(ViewState.Item("semanaf"))
            End If

            If CStr(ViewState.Item("filtrarsemana")) = "" Then
                filtrarsemana = 0
            Else
                filtrarsemana = CInt(ViewState.Item("filtrarsemana"))
            End If

            If CStr(ViewState.Item("stockxtienda")) = "" Then
                stockxtienda = 0
            Else
                stockxtienda = CShort(ViewState.Item("stockxtienda"))
            End If

            Dim Tienda As String = CStr(ViewState.Item("nomTienda"))

            Dim listaProducto As New List(Of Entidades.ProductoView)
            Dim ListaTipoAlmacen As New List(Of Entidades.TipoAlmacen)

            listaProducto = CType(Session("listaProductoView"), Global.System.Collections.Generic.List(Of Global.Entidades.ProductoView))
            ListaTipoAlmacen = CType(Session("TipoAlmacen"), Global.System.Collections.Generic.List(Of Global.Entidades.TipoAlmacen))

            Dim ds As DataSet = (New Negocio.OrdenCompra).getReporteVentasStock(CInt(vidempresa), CInt(vidtienda), CInt(vlinea), CInt(vsublinea), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), yeari, semanai, yearf, semanaf, filtrarsemana, listaProducto, ListaTipoAlmacen, stockxtienda) '.Tables("_DT_VentasStock")

            If ds IsNot Nothing Then
                If Tienda = "" Or CInt(vidtienda) = 0 Then
                    Tienda = "TODAS LAS EMPRESAS"
                End If
                Tienda = UCase(Tienda)
                Me.lblTitulo.Text = "VENTAS MENSUALES EN " & Tienda & " Y STOCK POR ALMACEN"
                dgwComparativoAnualCompras.DataSource = ds.Tables(0)
                dgwComparativoAnualCompras.DataBind()

                dgwComparativoAnualCompras.AllowPaging = False

                For Each row As GridViewRow In dgwComparativoAnualCompras.Rows
                    'total Sublinea
                    If Right(row.Cells(0).Text, 5) = "33667" Then
                        row.BackColor = Drawing.Color.SkyBlue
                        row.Cells(0).Text = ""

                    End If
                Next

                'Dim nombre As String
                'nombre = "VentasYStockProductos"
                'ExportaExcel(Me.dgwComparativoAnualCompras, nombre)

                Response.Clear()
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=VentasyStock.xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.ms-excel"

                Response.ContentEncoding = System.Text.Encoding.UTF8

                Dim strWriter As System.IO.StringWriter
                strWriter = New System.IO.StringWriter
                Dim htmlTextWriter As System.Web.UI.HtmlTextWriter
                htmlTextWriter = New System.Web.UI.HtmlTextWriter(strWriter)

                dgwComparativoAnualCompras.RenderControl(htmlTextWriter)

                EnableViewState = False

                Response.Write(strWriter.ToString)
                dgwComparativoAnualCompras.Dispose()

                Response.End()


            End If
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message + "');</script>")
        End Try
    End Sub
    Public Sub ExportaExcel(ByVal grilla As GridView, ByVal FilleNameExt As String)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + FilleNameExt)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        Response.ContentEncoding = System.Text.Encoding.UTF8

        Dim strWriter As System.IO.StringWriter
        strWriter = New System.IO.StringWriter
        Dim htmlTextWriter As System.Web.UI.HtmlTextWriter
        htmlTextWriter = New System.Web.UI.HtmlTextWriter(strWriter)

        grilla.RenderControl(htmlTextWriter)

        EnableViewState = False

        Response.Write(strWriter.ToString)
        grilla.Dispose()

        Response.End()

    End Sub

    Private Sub ExportToExcel(ByVal strFileName As String, ByVal dg As GridView)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.Charset = ""
        Me.EnableViewState = False
        Dim oStringWriter As New System.IO.StringWriter
        Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)

        dgwComparativoAnualCompras.RenderControl(oHtmlTextWriter)

        Response.Write(oStringWriter.ToString())
        Response.[End]()

    End Sub

    '   Public Function HTML(ByVal grilla1 As GridView) As String

    '       Dim page1 As Page = New Page
    '       Dim form1 As HtmlForm = New HtmlForm

    '       grilla1.EnableViewState = False
    '       If (grilla1.DataSource IsNot Nothing) Then
    '           grilla1.DataBind()
    '       End If


    '       grilla1.EnableViewState = False
    '       page1.EnableViewState = False

    '       page1.Controls.Add(form1)
    '       form1.Controls.Add(grilla1)


    '       Dim builde1 As System.Text.StringBuilder = New System.Text.StringBuilder
    '       Dim writer1 As System.IO.StringWriter = New System.IO.StringWriter
    '       Dim writer2 As HtmlTextWriter = New HtmlTextWriter(writer1)


    'writer2.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n")

    '       writer2.Write("<img src=http://enlace/a/Imagen.gif>")

    '       writer2.Write("<table><tr><td></td><td></td><td><font face=Arial size=5><center>Título Principal</center></font></td></tr></table><br>")

    '       writer2.Write("<table>\n<tr>\n<td></td><td class=TD width=35%><b>Fecha  :</b></td><td width=65% align=left>" + lblTitulo.Text.Trim() + "</td>\n</tr>\n<tr>\n<td></td><td class=TD><b>Gerencia:</b></td><td>" + DDLGerencia.SelectedItem.ToString().Trim() + "</td>\n</tr>\n</table>\n<br><br>")


    '       page1.DesignerInitialize()
    '       page1.RenderControl(writer2)
    '       writer2.Write("\n</body>\n</html>")
    '       page1.Dispose()
    '       page1 = Nothing
    '       Return builder1.ToString()

    '   End Function


    Public Class GridViewExportUtil

        Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
            HttpContext.Current.Response.ContentType = "application/ms-excel"
            Dim sw As StringWriter = New StringWriter
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            '  Create a form to contain the grid
            Dim table As System.Web.UI.WebControls.Table = New System.Web.UI.WebControls.Table
            table.GridLines = gv.GridLines
            '  add the header row to the table
            If (Not (gv.HeaderRow) Is Nothing) Then
                GridViewExportUtil.PrepareControlForExport(gv.HeaderRow)
                table.Rows.Add(gv.HeaderRow)
            End If
            '  add each of the data rows to the table
            For Each row As GridViewRow In gv.Rows
                GridViewExportUtil.PrepareControlForExport(row)
                table.Rows.Add(row)
            Next
            '  add the footer row to the table
            If (Not (gv.FooterRow) Is Nothing) Then
                GridViewExportUtil.PrepareControlForExport(gv.FooterRow)
                table.Rows.Add(gv.FooterRow)
            End If
            '  render the table into the htmlwriter
            table.RenderControl(htw)
            '  render the htmlwriter into the response
            HttpContext.Current.Response.Write(sw.ToString)
            HttpContext.Current.Response.End()
        End Sub

        ' Replace any of the contained controls with literals
        Private Shared Sub PrepareControlForExport(ByVal control As Control)
            Dim i As Integer = 0
            Do While (i < control.Controls.Count)
                Dim current As Control = control.Controls(i)
                If (TypeOf current Is LinkButton) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
                ElseIf (TypeOf current Is ImageButton) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
                ElseIf (TypeOf current Is HyperLink) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
                ElseIf (TypeOf current Is DropDownList) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
                ElseIf (TypeOf current Is CheckBox) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CStr(CType(current, CheckBox).Checked)))
                    'TODO: Warning!!!, inline IF is not supported ?
                End If
                If current.HasControls Then
                    GridViewExportUtil.PrepareControlForExport(current)
                End If
                i = (i + 1)
            Loop
        End Sub
    End Class


End Class
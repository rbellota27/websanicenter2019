<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmComparativoxCompraAnual.aspx.vb" Inherits="APPWEB.frmComparativoxCompraAnual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td class="TituloCelda" colspan="5">
                Comparativo Anual de Compras
            </td>
        </tr>
        <tr>
            <td class="LabelTdLeft">
                &nbsp;Empresa:
            </td>
            <td>
                <asp:DropDownList ID="dlpropietario" Width="200px" runat="server">
                </asp:DropDownList>
            </td>
            <td class="LabelTdLeft" colspan="2">
                &nbsp;Proveedor:
                <asp:TextBox ID="tbproveedor" Width="220px" Enabled="false" Font-Bold="true" runat="server"></asp:TextBox>
                &nbsp; Ruc:
                <asp:TextBox ID="tbruc" Enabled="false" Width="90px" Font-Bold="true" runat="server"></asp:TextBox>&nbsp;
                <asp:HiddenField ID="hdd_Idproveedor" Value="0" runat="server" />
                <asp:Button ID="btBuscarProveedor" OnClientClick="return (mostrarCapaProveedor());"
                    runat="server" Text="Buscar" />&nbsp;
                <asp:Button ID="btLimpiarProveedor" runat="server" Text="Limpiar" />
            </td>
        </tr>
        <tr>
            <td class="LabelTdLeft">
                &nbsp;Ingrese A&ntilde;o:
            </td>
            <td>
                <asp:TextBox Font-Bold="true" Width="70px" ID="tbyear" MaxLength="4" onKeyPress="return(onKeyPressEsNumero('event'));"
                    runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList ID="rbtipo" onClick="return(validartipo(this));" runat="server"
                    CssClass="Label" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="dc_Cantidad">Unidades</asp:ListItem>
                    <asp:ListItem Value="dc_Importe">Valor</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="LabelTdLeft">
                &nbsp;Moneda:
                <asp:DropDownList ID="dlmoneda" OnChange="return(validarMoneda());" Width="70px"
                    runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" OnClientClick="return(validarSave());"
                    onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
            </td>
        </tr>
    </table>
    <%--Capa de BUSQUEDA DE PROVEEDORES--%>
    <iframe id="iframex" width="100%" scrolling="yes" height="1024px"></iframe>
    <div id="verBusquedaProveedor" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanelProveedor" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgCerraCapaProveedor" runat="server" ImageUrl="~/Imagenes/Cerrar.gif"
                                OnClientClick="return(offCapa('verBusquedaProveedor'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                            AutoPostBack="false">
                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Texto">
                                        Buscar Proveedor:
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rbcondicion" runat="server" RepeatDirection="Horizontal"
                                            CssClass="Texto" Style="cursor: hand;" onClick="return ( onChange_txtBuscarProveedorFocus() );">
                                            <asp:ListItem Value="1" Text="Nacional" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="Extranjero"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Texto">
                                        Razon Social / Nombres:
                                    </td>
                                    <td colspan="2">
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="Button1">                                        
                                        <asp:TextBox ID="txtBuscarProveedor" onkeyPress="return ( onKeyPressProveedor(this) );"
                                            runat="server" Width="400px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                            onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                            </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Texto">
                                        R.U.C.:
                                    </td>
                                    <td>
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="Button1">                                        
                                        <asp:TextBox ID="txtbuscarRucProveedor" runat="server" onkeyPress="return ( onKeyPressProveedor(this) );"
                                            MaxLength="11"></asp:TextBox>
                                            </asp:Panel>
                                    </td>
                                    <td>
                                        <asp:Button ID="Button1" runat="server" Text="Buscar" CssClass="btnBuscar" Style="cursor: hand;" UseSubmitBehavior="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                             <asp:GridView ID="gvProveedor" runat="server" AutoGenerateColumns="False" HeaderStyle-Height="25px"
                        Width="100%" RowStyle-Height="25px">
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="True">
                            </asp:CommandField>
                            <asp:BoundField DataField="idpersona" HeaderText="C�d." NullDisplayText="0"></asp:BoundField>
                            <asp:BoundField DataField="RazonSocial" HeaderText="Raz�n Social" NullDisplayText="---" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---" />
                            <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---">
                            </asp:BoundField>
                            <asp:BoundField DataField="Telefeono" HeaderText="Tel�fono" NullDisplayText="---" />
                            <asp:BoundField DataField="Correo" HeaderText="Correo" NullDisplayText="---">
                                <ItemStyle />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddidagente" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoAgente") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <asp:Button ID="btAnterior_Proveedor" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                OnClientClick="return(valNavegacionProveedor('0'));" Style="cursor: hand;" />
                            <asp:Button ID="btSiguiente_Proveedor" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                OnClientClick="return(valNavegacionProveedor('1'));" Style="cursor: hand;" />
                            <asp:TextBox ID="tbPageIndex_Proveedor" Width="50px" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox><asp:Button ID="btIr_Proveedor" runat="server" Width="50px"
                                    Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProveedor('2'));"
                                    Style="cursor: hand;" />
                            <asp:TextBox ID="tbPageIndexGO_Proveedor" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="gvProveedor" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">

        function validartipo(obj) {
            var control = obj.getElementsByTagName('input');
            var cbo = document.getElementById('<%=dlmoneda.ClientID %>');
            if (control[0].checked == true && cbo.value != '0') {
                cbo.selectedIndex = 0;
            }
            return true;
        }

        ///+++++++++++
        function validarMoneda() {
            var tipo = document.getElementById('<%=rbtipo.ClientID %>');
            var cbo = document.getElementById('<%=dlmoneda.ClientID %>');
            var control = tipo.getElementsByTagName('input');
            if (control[0].checked == true) {
                alert('El tipo de reporte es por unidades, no puede seleccionar una moneda.');
                cbo.selectedIndex = 0;
                return false;
            }
            return true;
        }

        function validarRucProveedor() {
            alert('Este proveedor no tiene numero de ruc');
            offCapa('verBusquedaProveedor');
            return false;
        }
        /////++++++++++++++++++++
        function mostrarCapaProveedor() {
            onCapa('verBusquedaProveedor');
            return false;
        }
        /////////++++++++++++
        function mostrarReporte() {
            var idpropietario = document.getElementById('<%=dlpropietario.ClientID %>').value;
            var idproveedor = document.getElementById('<%=hdd_Idproveedor.ClientID %>').value;
            var idmoneda = document.getElementById('<%=dlmoneda.ClientID %>').value;
            var idyear = document.getElementById('<%=tbyear.ClientID %>').value;
            var rb = document.getElementById('<%=rbtipo.ClientID %>');
            var control = rb.getElementsByTagName('input');
            var idtipo = '';
            for (var i = 0; i < control.length; i++) {
                if (control[i].checked == true) {
                    idtipo = control[i].value;
                }
            }
            iframex.location.href = 'VisorGrilla.aspx?iReporte=1&idyear=' + idyear + '&idtipo=' + idtipo + '&idproveedor=' + idproveedor + '&idmoneda=' + idmoneda + '&idpropietario=' + idpropietario;
        }
        //++
        function validarSave() {
            var year = document.getElementById('<%=tbyear.ClientID %>');
            if (year.value.length != 4) {
                alert('El formato de a�o es de 4 digitos.');
                return false;
            }

            var tipo = document.getElementById('<%=rbtipo.ClientID %>');
            var cbo = document.getElementById('<%=dlmoneda.ClientID %>');
            var control = tipo.getElementsByTagName('input');
            if (control[1].checked == true && cbo.value == '0') {
                alert('Selecione una moneda.');
                return false;
            }

            var tipo = document.getElementById('<%=rbtipo.ClientID %>');
            var cbo = document.getElementById('<%=dlmoneda.ClientID %>');
            var control = tipo.getElementsByTagName('input');
            if (control[0].checked == true && cbo.value != '0') {
                alert('NO selecionar una moneda.');
                return false;
            }
            var idpropietario = document.getElementById('<%=dlpropietario.ClientID %>').value;
            var idproveedor = document.getElementById('<%=hdd_Idproveedor.ClientID %>').value;
            var idmoneda = document.getElementById('<%=dlmoneda.ClientID %>').value;
            var idyear = document.getElementById('<%=tbyear.ClientID %>').value;
            var rb = document.getElementById('<%=rbtipo.ClientID %>');
            var control = rb.getElementsByTagName('input');
            var idtipo = '';
            for (var i = 0; i < control.length; i++) {
                if (control[i].checked == true) {
                    idtipo = control[i].value;
                }
            }
            iframex.location.href = 'VisorGrilla.aspx?iReporte=1&idyear=' + idyear + '&idtipo=' + idtipo + '&idproveedor=' + idproveedor + '&idmoneda=' + idmoneda + '&idpropietario=' + idpropietario;

            //            return confirm('Desea continuar con la operacion ?');

            return true;


        }
    </script>

</asp:Content>

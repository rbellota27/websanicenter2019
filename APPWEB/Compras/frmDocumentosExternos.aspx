﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmDocumentosExternos.aspx.vb" Inherits="APPWEB.frmDocumentosExternos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Registro de Documentos Externos
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button Width="85px" ID="btnuevo" runat="server" Text="Nuevo" />
                <asp:Button Width="85px" ID="btguardar" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());" />
                <asp:Button Width="85px" ID="btcancelar" runat="server" Text="Cancelar" OnClientClick="return(confirm('Está seguro de cancelar la operación.'));" />
                <asp:Button Width="85px" ID="btbuscar" runat="server" Text="Buscar" />
                <asp:Button Width="85px" ID="bteditar" runat="server" Text="Editar" OnClientClick="return(validarEditar());" />
                <asp:Button Width="85px" ID="btanular" runat="server" Text="Anular" OnClientClick="return(confirm('Está seguro de anular el documento.'));" />
                <asp:Button Width="85px" ID="btimprimir" runat="server" Text="Imprimir" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlprincipal" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="LabelTab">
                                            Tipo Documento:
                                        </td>
                                        <td>
                                            <asp:DropDownList Width="100%" ID="dlTipoDocumento" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="LabelTab">
                                            Serie:
                                        </td>
                                        <td>
                                            <asp:TextBox Width="100px" ID="tbserieExt" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="LabelTab">
                                            N&uacute;mero:
                                        </td>
                                        <td>
                                            <asp:TextBox Width="100px" ID="tbnumeroExt" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btBuscarDocumento" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                                ToolTip="Buscar Documento [ Serie - Número - Tipo Documento - Persona ]" OnClientClick="return(validarBuscarDocumento());" />
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lbAvanzado" ToolTip="Buscar [ Rango de fechas - Tipo Documento ] | [ Persona ]"
                                                runat="server" CssClass="Label" Text="Avanzado" OnClientClick="return(validarAvanzado());"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LabelTab">
                                            Fecha Emisi&oacute;n:
                                        </td>
                                        <td>
                                            <asp:TextBox Width="100px" ID="tbfechaemisionExt" CssClass="TextBox_Fecha" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="d" TargetControlID="tbfechaemisionExt">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="LabelTab">
                                            Fecha Cancelaci&oacute;n:
                                        </td>
                                        <td>
                                            <asp:TextBox Width="100px" ID="tbfechacanc" onKeyDown="return(false);" CssClass="TextBox_Fecha" runat="server"></asp:TextBox>                                            
                                        </td>
                                        <td class="LabelTab">
                                            Fecha Vencimiento:
                                        </td>
                                        <td>
                                            <asp:TextBox Width="100px" ID="tbfechavenc" CssClass="TextBox_Fecha" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="d" TargetControlID="tbfechavenc">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LabelTab">
                                            Estado:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlestado" runat="server" Enabled="false" Font-Bold="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                Datos de la Persona
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="LabelTab">
                                            Raz&oacute;n Social / Nombres:
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="tbPersona" CssClass="TextBoxReadOnly" runat="server" Width="235px"
                                                Font-Bold="true" onKeyDown="return(false);"></asp:TextBox>
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btBuscarPersona" runat="server" Text="Buscar" OnClientClick="return(validarPersona());" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LabelTab">
                                            D.N.I.:
                                        </td>
                                        <td class="LabelLeft">
                                            <asp:TextBox ID="tbdni" Width="90px" CssClass="TextBoxReadOnly" Font-Bold="true"
                                                runat="server" onKeyDown="return(false);"></asp:TextBox>
                                        </td>
                                        <td class="LabelTab">
                                            R.U.C.:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox Width="100px" ID="tbruc" Font-Bold="true" runat="server" CssClass="TextBoxReadOnly"
                                                onKeyDown="return(false);"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                Moneda y Condici&oacute;n Pago 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="LabelTab">
                                            Moneda:
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="dlmoneda" runat="server" Enabled="false" Font-Bold="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="LabelTab">
                                            &nbsp;Condici&oacute;n Pago:
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="dlcondicionpago" runat="server" Enabled="false" Font-Bold="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                Documento Relacionado
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom">
                                <table cellpadding="0" cellspacing="0">
                                     <tr>
                                        <td class="LabelTab">
                                            &nbsp;Empresa:
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="dlpropietario" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="LabelTab">
                                            &nbsp;Tienda:
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="dltienda" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="LabelTab">
                                            &nbsp;Tipo Documento:
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="dldocumento" runat="server" Enabled="false" Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;
                                            <asp:ImageButton ID="imgBuscarDocumentoRef" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                                ToolTip="[ Buscar Documento de referencia ]" 
                                                OnClientClick="return(verCapaDocumentoRef());" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LabelTab">
                                            &nbsp;Serie:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="tbserie" runat="server" onKeyDown="return(false);" 
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td class="LabelTab">
                                            &nbsp;N&uacute;mero:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="tbnumero" runat="server" onKeyDown="return(false);" 
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td class="LabelTab">
                                            Fecha Emision:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="tbfechaemision" runat="server" CssClass="TextBox_Fecha" 
                                                onKeyDown="return(false);" Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="LabelTab">
                                            &nbsp;Total
                                            <asp:Label CssClass="Label" ID="lblsimbolomoneda2" runat="server" Text=""></asp:Label>
                                            :
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="tbtotaldocumento" runat="server" onKeyDown="return(false);" 
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td class="LabelTab">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;</td>
                                        <td class="LabelTab">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                Detalles
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView Width="100%" ID="gvproductos" runat="server" AutoGenerateColumns="False"
                                    GridLines="None">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                        <asp:BoundField DataField="IdProducto" HeaderText="Código" />
                                        <asp:BoundField DataField="NomProducto" HeaderText="Descripción" />
                                        <asp:BoundField DataField="UMedida" HeaderText="U. Medida" />
                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbcantidad" runat="server" MaxLength="7" onfocus="return(aceptarFoco(this));"
                                                    onkeyUp="return(calcularmontoup('1'));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>' Width="45px"
                                                    Font-Bold="true"></asp:TextBox>
                                                <asp:HiddenField ID="hddidmedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cant. x Atender">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbcantidadxAtender" runat="server" MaxLength="7" Enabled="false"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"CantxAtender","{0:F2}")%>' Width="45px"
                                                    Font-Bold="true"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Precio">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbprecioCD" runat="server" Enabled="false" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD","{0:F2}")%>'
                                                    Width="105px" Font-Bold="true"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Importe">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbimporte" runat="server" Enabled="false" Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F2}")%>'
                                                    Width="105px" Font-Bold="true"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgBuscarDetalles" runat="server" OnClick="imgBuscarDetalles_Click"
                                                    ImageUrl="~/Caja/iconos/ok.gif" ToolTip="[ Consultar Guia de Remisión ]" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="Texto">
                                            Total:
                                        </td>
                                        <td valign="middle">
                                            <asp:Label Font-Size="Smaller" ID="lblSimboloMoneda" Text="" CssClass="Label" runat="server"></asp:Label>
                                            <asp:TextBox ID="tbtotal" runat="server" Font-Bold="true">0</asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCelda">
                                Observacion
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="tbobservacion" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                        </tr>                        
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                <asp:HiddenField ID="hdd_iddocumento" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_iddocRelacionado" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_idpersona" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_idobservacion" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_idusuario" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_idguias" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_operativo" runat="server" Value="0" />                
            </td>
        </tr>
    </table>
    <div id="capabusqueda" style="border: 3px solid blue; padding: 8px; width: 750px;
        height: auto; position: absolute; top: 160px; left: 38px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%" cellpadding="0">
            <tr>
                <td align="right">
                    <asp:Button ID="Button3" runat="server" Text="Cerrar" OnClientClick="return(offCapa('capabusqueda'));" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="LabelTab">
                                            Fecha Incio:
                                        </td>
                                        <td>
                                            <asp:TextBox Width="100px" ID="tbfechainibusqueda" CssClass="TextBoxReadOnly" onKeyPress="return(irdocref());"
                                                runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender6" runat="server" Format="d" TargetControlID="tbfechainibusqueda">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="LabelTab">
                                            Fecha Fin:
                                        </td>
                                        <td>
                                            <asp:TextBox Width="100px" ID="tbfechafinbusqueda" CssClass="TextBoxReadOnly" onKeyPress="return(irdocref());"
                                                runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender7" runat="server" Format="d" TargetControlID="tbfechafinbusqueda">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:Button ID="imgbusqueda" runat="server" Text="Buscar" OnClientClick="return(validarDocumentoBusqueda());" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvBusqueda" runat="server" ShowHeader="true" Width="100%" EmptyDataText="No se hallaron registros"
                        GridLines="None" AutoGenerateColumns="False" CellSpacing="1">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdDocumento") %>' />
                                    <asp:HiddenField ID="hddidtipodocumento" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoDocumento") %>' />
                                    <asp:HiddenField ID="hddidpersona" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPersona") %>' />
                                    <asp:HiddenField ID="hddIdEstadoDoc" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdEstadoDoc") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Documento" />
                            <asp:BoundField DataField="Serie" HeaderText="Serie" />
                            <asp:BoundField DataField="Codigo" HeaderText="Número" />
                            <asp:BoundField DataField="RazonSocial" HeaderText="Razón Social" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" />
                            <asp:BoundField DataField="TotalAPagar" HeaderText="Total" DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisión" DataFormatString="{0:d}" />
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capadocumentoref" style="border: 3px solid blue; padding: 8px; width: 750px;
        height: auto; position: absolute; top: 160px; left: 38px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%" cellpadding="0">
            <tr>
                <td align="right">
                    <asp:Button ID="Button1" runat="server" Text="Cerrar" OnClientClick="return(offCapa('capadocumentoref'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="LabelTab">
                                Tipo de Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="dltipodocumentoref" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="LabelTab">
                                Buscar Por:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtipo" runat="server" AutoPostBack="True" CssClass="Texto"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="2">Serie / N&uacute;mero</asp:ListItem>
                                    <asp:ListItem Value="1">Entre Fechas</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Panel ID="panelserienumero" runat="server">
                                    <table>
                                        <tr>
                                            <td class="LabelTab">
                                                Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox Width="100px" ID="tbbuscarserieref" MaxLength="4" runat="server" onKeyPress="onKeyPressEsNumero('event'); irdocref();"></asp:TextBox>
                                            </td>
                                            <td class="LabelTab">
                                                N&uacute;mero:
                                            </td>
                                            <td>
                                                <asp:TextBox Width="100px" ID="tbbuscarcodigoref" MaxLength="12" runat="server" onKeyPress="onKeyPressEsNumero('event'); irdocref();"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="panelfechas" runat="server">
                                    <table>
                                        <tr>
                                            <td class="LabelTab">
                                                Fecha Incio:
                                            </td>
                                            <td>
                                                <asp:TextBox Width="100px" ID="tbfechaini" CssClass="TextBoxReadOnly" onKeyPress="return(irdocref());"
                                                    runat="server"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="d" TargetControlID="tbfechaini">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="LabelTab">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox Width="100px" ID="tbfechafin" CssClass="TextBoxReadOnly" onKeyPress="return(irdocref());"
                                                    runat="server"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Format="d" TargetControlID="tbfechafin">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Button ID="btdocumentoref" runat="server" Text="Buscar" OnClientClick="return(validarDocumentoRef());" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvdocumentoref" runat="server" Width="100%" EmptyDataText="No se hallaron registros"
                        GridLines="None" AutoGenerateColumns="False" CellSpacing="1">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdDocumento") %>' />
                                    <asp:HiddenField ID="hddIdpersona" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPersona") %>' />
                                    <asp:HiddenField ID="hddidguias" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"NroDocumento") %>' />
                                    <asp:HiddenField ID="hddidmoneda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>' />
                                    <asp:HiddenField ID="hddidtipodocumento" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoDocumento") %>' />
                                    <asp:HiddenField ID="hddidcondicionpago" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCondicionPago") %>' />
                                    <asp:HiddenField ID="hddidtienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"idtienda") %>' />
                                    <asp:HiddenField ID="hddidempresa" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Documento" />
                            <asp:BoundField DataField="Serie" HeaderText="Serie" />
                            <asp:BoundField DataField="Codigo" HeaderText="Número" />
                            <asp:BoundField DataField="NomCondicionPago" HeaderText="Condición Pago" />
                            <asp:BoundField DataField="NomMoneda" HeaderText="" />
                            <asp:BoundField DataField="TotalAPagar" HeaderText="Total" DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisión" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="NomTienda" HeaderText="Tienda" />
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capapersona" style="border: 3px solid blue; padding: 8px; width: 750px;
        height: auto; position: absolute; background-color: white; top: 130px; z-index: 2;
        left: 40px; display: none;">
        <table width="100%">
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                    AutoPostBack="false">
                                    <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                    <asp:ListItem Value="J">Jur&iacute;dica</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="Texto">
                                Rol:
                            </td>
                            <td colspan="2" align="left">
                                <asp:DropDownList ID="dlrolpersona" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Raz&oacute;n Social / Nombres:
                            </td>
                            <td colspan="4" align="left">
                                <asp:TextBox ID="tbrazonape" onKeyPress="return(validarCajaBusqueda());" runat="server"
                                    Width="450px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                D.N.I.:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                    MaxLength="8" runat="server"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                R.U.C.:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                    MaxLength="11"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:Button ID="btPersona" runat="server" Text="Buscar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return(SelectPersona(this));">Seleccionar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="idpersona" HeaderText="Código" NullDisplayText="0" />
                            <asp:BoundField DataField="Nombre" HeaderText="Razón Social / Nombres" NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Left" Width="500px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                        ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                    <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                        ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                    <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                            Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                    <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaGuia" style="border: 3px solid blue; padding: 8px; width: 550px; height: auto;
        position: absolute; background-color: white; top: 130px; z-index: 2; left: 40px;
        display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="Button2" runat="server" Text="Cerrar" OnClientClick="return(offCapa('capaGuia'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblproducto" runat="server" Text="" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView Width="100%" ID="gvdetalleguia" runat="server" AutoGenerateColumns="false"
                        GridLines="None">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Documento" />
                            <asp:BoundField DataField="Serie" HeaderText="Serie" />
                            <asp:BoundField DataField="Codigo" HeaderText="Número" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisión" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="total" HeaderText="Cantidad" DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="NomTienda" HeaderText="Tienda" />
                            <asp:BoundField DataField="NomAlmacen" HeaderText="Almacén" />
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" language="javascript">
        function validarGuardar() {
            var serie = document.getElementById('<%=tbserieExt.ClientID %>');
            var codigo = document.getElementById('<%=tbnumeroExt.ClientID %>');
            if (serie.value.length == 0) {
                alert('Debe ingresar la serie del documento.');
                return false;
            }
            if (codigo.value.length == 0) {
                alert('Debe ingresar el número del documento.');
                return false;
            }
            var FechaEmision = document.getElementById('<%=tbfechaemisionExt.ClientID %>');
            if (valFecha(FechaEmision) == false) {
                return false;
            }
            var FechaVenc = document.getElementById('<%=tbfechavenc.ClientID %>');
            if (FechaVenc.value != '') {
                if (valFecha(FechaVenc) == false) {
                    return false;
                }
            } else {
            alert('Debe ingresar la fecha de vencimiento');
            return false;                
            }
            
            var FechaCancelacion = document.getElementById('<%=tbfechacanc.ClientID %>');
            if (FechaCancelacion.value != '') {
                if (valFecha(FechaCancelacion) == false) {
                    return false;
                }
            }
            var Total = document.getElementById('<%=tbtotal.ClientID %>');
            if (Total.value.length = 0 || isNaN(parseFloat(Total.value))) {
                alert('El total ingresado no es válido');

                return false;
            }
            var IdTipoDocumento = document.getElementById('<%= dlTipoDocumento.ClientID %>');
            if (IdTipoDocumento.value == '0') {
                alert('Debe Seleccionar un tipo de documento');
                return false;
            }
            var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>');
            if (idpersona.value == '0' || idpersona.value == '') {
                alert('Debe ingresar a una persona para el documento');
                return false;
            }
            return confirm('Desea continuar con la operación');
        }
        //////////////////////////////////////////////////////////////////
        function validarEditar() {
            fecha = document.getElementById('<%=tbfechacanc.ClientID %>');
            if (fecha.value != '') {
                alert('El documento esta enlazado a un documento de cancelación');
                return false;            
            }
            return true;
        }
        //////////////////////////////////////////////////////////////////////
        function verCapaDocumentoRef() {
            var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>')
            if (idpersona.value == '0') {
                alert('No ha ingresado a un proveedor');
                return false;
            }
            var tipodocumento = document.getElementById('<%=dlTipoDocumento.ClientID %>');
            if (tipodocumento.value == '0') {
                alert('Debe seleccionar un tipo de documento');
                return false;
            }
            onCapa('capadocumentoref');
            return false;
        }
        //////////////////////////////////////////////////////////////////////
        function irdocref() {
            var key = event.KeyCode;
            if (key == 13) {
                document.getElementById('<%=btdocumentoref.ClientID %>').focus();
                return true;
            }
        }
        /////////////////////////////////////////////////////////////////////
        function validarDocumentoRef() {
            var radio = document.getElementById('<%=rbtipo.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[0].checked == true) {  ///// serie y numero
                var serie = document.getElementById('<%=tbbuscarserieref.ClientID %>');
                var codigo = document.getElementById('<%=tbbuscarcodigoref.ClientID %>');
                if (codigo.value.length == 0 && serie.value.length == 0) {
                    alert('No ha ingresado datos');
                    return false;
                }
            }
            if (control[1].checked == true) {  ///// entre fechas
                var fechaini = document.getElementById('<%=tbfechaini.ClientID %>');
                var fechafin = document.getElementById('<%=tbfechafin.ClientID %>');
                if (valFecha(fechaini) == false) {
                    return false;
                }
                if (valFecha(fechafin) == false) {
                    return false;
                }
            }
            return true;
        }
        ////////////////////////////////////////////////////////////////////////
        function SelectPersona(obj) {
            var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>');
            var persona = document.getElementById('<%=tbPersona.ClientID %>');
            var ruc = document.getElementById('<%=tbruc.CLientID %>');
            var dni = document.getElementById('<%=tbdni.CLientID %>');
            var grilla = document.getElementById('<%=gvBuscar.ClientID %>');

            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[0].children[0].id == obj.id) {
                    idpersona.value = rowElem.cells[1].innerText;
                    persona.value = rowElem.cells[2].innerText;
                    ruc.value = rowElem.cells[3].innerText;
                    dni.value = rowElem.cells[4].innerText;
                    offCapa('capapersona');
                    return false;
                }
            }

        }
        //////////////////////////////////////////////////////////////////
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbrazonape.ClientID%>').select();
                document.getElementById('<%=tbrazonape.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   /////////////////////////// anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //////////////////////////// ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        /////////////////////////////////////////////////////////////////////////
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        //////////////////////////////////////////////////////////////////
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btPersona.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        /////////////////////////////////////////////////////////////////////////////
        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btPersona.ClientID %>');
                boton.focus();
                return true;
            }
        }
        /////////////////////////////////////////////////////////////////////////////
        function CalcularMonto() {
            var grilla = document.getElementById('<%= gvproductos.ClientID %>');
            var total = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    var importe = redondear(parseFloat(rowElem.cells[7].children[0].value), 2);
                    if (isNaN(importe)) {
                        importe = 0;
                    }

                    total = total + importe;
                }

                document.getElementById('<%=tbtotal.ClientID %>').value = redondear(parseFloat(total), 2);
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////
        function calcularmontoup(valor) {
            var grilla = document.getElementById('<%= gvproductos.ClientID %>');
            var total = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var monto = 0;
                    var rowElem = grilla.rows[i];

                    var cantidad = redondear(parseFloat(rowElem.cells[4].children[0].value), 2);
                    if (isNaN(cantidad)) {
                        cantidad = 0;
                    }
                    var precio = redondear(parseFloat(rowElem.cells[6].children[0].value), 2);
                    if (isNaN(precio)) {
                        precio = 0;
                    }
                    monto = cantidad * precio;
                    if (rowElem.cells[4].children[0].id == event.srcElement.id || rowElem.cells[6].children[0].id == event.srcElement.id) {
                        monto = 0;
                        switch (valor) {
                            case '1': //cantidad
                                monto = cantidad * precio;
                                break;
                        }
                        rowElem.cells[7].children[0].value = monto;
                    }

                    total = total + monto;
                }

                document.getElementById('<%=tbtotal.ClientID %>').value = redondear(parseFloat(total), 2);
            }
        }
        ////////////////////////////////////////////////////////////////////////
        function validarPersona() {
            var tipodocumento = document.getElementById('<%=dldocumento.ClientID %>');
            if (tipodocumento.value != 0) {
                alert('ha ingresado un documento relacionado a esta persona');
                return false;
            }
            onCapa('capapersona');
            return false;
        }
        //////////////////////////////////////////////////////////////////////
        function validarBuscarDocumento() {
            var tipodocumento = document.getElementById('<%=dlTipodocumento.ClientID %>');
            if (tipodocumento.value == '0') {
                alert('Debe seleccionar un tipo de documento');
                return false;
            }
            var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>');
            if (idpersona.value == '0') {
                alert('Debe buscar una persona');
                return false;
            }
            var serie = document.getElementById('<%=tbserieExt.ClientID %>');
            var codigo = document.getElementById('<%=tbnumeroExt.ClientID %>');
            
            if (serie.value.length == 0) {
                alert('Debe ingresar la serie del documento.');
                return false;
            }
            
            if (codigo.value.length == 0) {
                alert('Debe ingresar el número del documento.');
                return false;
            }
            
            return true;
        }
        function validarAvanzado() {
            var tipodocumento = document.getElementById('<%=dlTipodocumento.ClientID %>');
            if (tipodocumento.value == '0') {
                alert('Debe seleccionar un tipo de documento');
                return false;
            }
            onCapa('capabusqueda');
            return false;
        }

        function validarDocumentoBusqueda() {
            var fechaini = document.getElementById('<%=tbfechainibusqueda.ClientID %>');
            var fechafin = document.getElementById('<%=tbfechafinBusqueda.ClientID %>');
            if (valFecha(fechaini) == false) {
                return false;
            }
            if (valFecha(fechafin) == false) {
                return false;
            }
            return true;
        }
        
    </script>

</asp:Content>

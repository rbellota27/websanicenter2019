﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmConsultaOrdenCompra.aspx.vb" Inherits="APPWEB.FrmConsultaOrdenCompra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Consulta Orden Compra
            </td>
        </tr>
        <tr>
            <td align="center">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Texto">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEmpresa" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTienda" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Texto">
                            Raz&oacute;n Social:
                        </td>
                        <td>
                            <asp:TextBox ID="txtRazonSocial" Width="350px" CssClass="TextBoxReadOnly" onFocus=" return ( onFocus_ReadOnly(this) ); "
                                runat="server"></asp:TextBox>
                        </td>
                        <td class="Texto">
                            R.U.C.:
                        </td>
                        <td>
                            <asp:TextBox ID="txtRuc" CssClass="TextBoxReadOnly" onFocus=" return ( onFocus_ReadOnly(this) ); "
                                runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnBuscarProveedor" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                onmouseout="this.src='/Imagenes/Buscar_b.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                OnClientClick=" return( mostrarCapaProveedor() ); " />
                        </td>
                        <td>
                            <asp:ImageButton ID="btnLimpiarProveedor" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                OnClientClick=" return( onClick_LimpiarProveedor() ); " />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="Texto">
                                        Fecha Incio:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaIni" runat="server" CssClass="TextBox_Fecha" Width="80px"
                                            onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender_FechaIni" runat="server" TargetControlID="txtFechaIni"
                                            Format="dd/MM/yyyy">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td class="Texto">
                                        Fecha Fin:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" Width="80px"
                                            onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender_FechaFin" runat="server" TargetControlID="txtFechaFin"
                                            Format="dd/MM/yyyy">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbkConsiderarFecha" Text="Considerar Rango" CssClass="Texto" runat="server"
                                            Checked="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="Texto">
                                        Estado Entrega:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEstEntrega" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto">
                                        Estado Cancelaci&oacute;n:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEstCancelacion" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel_GV_Detalle" runat="server" Width="1000px" ScrollBars="Horizontal">
                    <asp:GridView ID="GV_OrdenCompra" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="20" HeaderStyle-Height="25px" RowStyle-Height="25px">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:BoundField DataField="NroDocumento" HeaderText="Numero" ItemStyle-Width="125px"
                                ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red">
                                <ItemStyle Font-Bold="True" ForeColor="Red" Width="125px"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaEmision" HeaderText="F. Emision" />
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Proveedor" ItemStyle-Font-Bold="true">
                                <ItemStyle Font-Bold="True"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." ItemStyle-Font-Bold="true">
                                <ItemStyle Font-Bold="True"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Importe">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" ForeColor="Red" Text='<%# DataBinder.Eval(Container.DataItem,"NomMoneda") %>'></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblTotal" runat="server" Font-Bold="true" ForeColor="Red" Text='<%# DataBinder.Eval(Container.DataItem,"Total","{0:F3}") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomCondicionPago" HeaderText="Cond. Pago" />
                            <asp:TemplateField HeaderText="S/. Importe">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblTotalAPagar" runat="server" Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomContactoProveedor" HeaderText="C. Proveedor" />
                            <asp:BoundField DataField="NomEmpleado" HeaderText="Usuario" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Est. Documento" />
                            <asp:BoundField DataField="NomEstadoCancelacion" HeaderText="Est. Cancelacion" />
                            <asp:BoundField DataField="NomEstadoEntregado" HeaderText="Est. Entregada" />
                            <asp:BoundField DataField="FechaEntrega" HeaderText="F. Entrega" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hhdIdDocumento" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id","{0:F3}") %>' />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnMostrarDetalleDocumentoRef" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                    OnClientClick=" return( valOnClick_btnMostrarDetalle(this)  ); " ToolTip="Visualizar Detalle"
                                                    Width="20px" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnAnteriorOC" runat="server" Font-Bold="true" Width="50px" Text="<"
                    ToolTip="Página Anterior" OnClientClick="return(valNavegacionOC('0'));" />
                <asp:Button ID="btnSiguienteOC" runat="server" Font-Bold="true" Width="50px" Text=">"
                    ToolTip="Página Posterior" OnClientClick="return(valNavegacionOC('1'));" />
                <asp:TextBox ID="tbPageIndexOC" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                    runat="server"></asp:TextBox><asp:Button ID="btIrOC" runat="server" Font-Bold="true"
                        Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionOC('2'));" />
                <asp:TextBox ID="tbPageIndexGOOC" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                    onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="SubTituloCelda">
                            Documento de Referencia
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TreeView ID="TreeViewDocumentoRef" runat="server" SelectedNodeStyle-BackColor="#FFCC99"
                                CssClass="LabelLeft">
                            </asp:TreeView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                <HeaderStyle CssClass="GrillaHeader" />
                                <Columns>
                                    <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" SelectText="Quitar" ShowSelectButton="True" />
                                    <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="DescripcionPersona" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Persona" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdPersona" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="verBusquedaProveedor" style="border: 3px solid blue; padding: 2px; width: 900px;
        height: auto; position: absolute; top: 200px; left: 25px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="imgCerraCapaProveedor" runat="server" ImageUrl="~/Imagenes/Cerrar.gif"
                        OnClientClick="return(offCapa('verBusquedaProveedor'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="N">Natural</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Buscar Proveedor:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbcondicion" runat="server" RepeatDirection="Horizontal"
                                    CssClass="Texto" Style="cursor: hand;" onClick="return ( onChange_txtBuscarProveedorFocus() );">
                                    <asp:ListItem Value="1" Text="Nacional" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Extranjero"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Razon Social / Nombres:
                            </td>
                            <td colspan="2">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btBuscarProveedor">                               
                                <asp:TextBox ID="txtBuscarProveedor" onkeyPress="return ( onKeyPressProveedor(this) );"
                                    runat="server" Width="400px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarProveedor">                               
                                <asp:TextBox ID="txtbuscarRucProveedor" runat="server" onkeyPress="return ( onKeyPressProveedor(this) );"
                                    MaxLength="11"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:Button ID="btBuscarProveedor" runat="server" Text="Buscar" Style="cursor: hand;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="gvProveedor" runat="server" AutoGenerateColumns="False" HeaderStyle-Height="25px"
                        Width="100%" RowStyle-Height="25px">
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="True">
                            </asp:CommandField>
                            <asp:BoundField DataField="idpersona" HeaderText="Cód." NullDisplayText="0"></asp:BoundField>
                            <asp:BoundField DataField="RazonSocial" HeaderText="Razón Social" NullDisplayText="---" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---" />
                            <asp:BoundField DataField="Direccion" HeaderText="Dirección" NullDisplayText="---">
                            </asp:BoundField>
                            <asp:BoundField DataField="Telefeono" HeaderText="Teléfono" NullDisplayText="---" />
                            <asp:BoundField DataField="Correo" HeaderText="Correo" NullDisplayText="---">
                                <ItemStyle />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddidagente" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoAgente") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Button ID="btAnterior_Proveedor" runat="server" Width="50px" Text="<" ToolTip="Página Anterior"
                        OnClientClick="return(valNavegacionProveedor('0'));" Style="cursor: hand;" />
                    <asp:Button ID="btSiguiente_Proveedor" runat="server" Width="50px" Text=">" ToolTip="Página Posterior"
                        OnClientClick="return(valNavegacionProveedor('1'));" Style="cursor: hand;" />
                    <asp:TextBox ID="tbPageIndex_Proveedor" Width="50px" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btIr_Proveedor" runat="server" Width="50px"
                            Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProveedor('2'));"
                            Style="cursor: hand;" />
                    <asp:TextBox ID="tbPageIndexGO_Proveedor" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;

        function onClick_LimpiarProveedor() {
            document.getElementById('<%=hddIdPersona.ClientID %>').value = '0';
            document.getElementById('<%=txtRazonSocial.ClientID %>').value = '';
            document.getElementById('<%=txtRuc.ClientID %>').value = '';
            return false;
        }
        //
        function mostrarCapaProveedor() {           
            onCapa('verBusquedaProveedor');
            var tb = document.getElementById('<%=txtBuscarProveedor.ClientID %>');
            tb.focus();
            tb.select();
            return false;
        }        
        //               
        function valNavegacionProveedor(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex_Proveedor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtBuscarProveedor.ClientID%>').select();
                document.getElementById('<%=txtBuscarProveedor.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }        
      
        function onFocus_ReadOnly(ocaja) {
            ocaja.readOnly = true;
            return false;
        }
        //
        function valNavegacionOC(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndexOC.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGOOC.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGOOC.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGOOC.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGOOC.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGOOC.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //
        function valOnClick_btnMostrarDetalle(control) {

            var grilla = document.getElementById('<%=GV_OrdenCompra.ClientID%>');
            var IdDocumento = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[14].children[0].cells[1].children[0];
                    var hddIdDocumento = rowElem.cells[14].children[0].cells[0].children[0];

                    if (control.id == boton.id) {
                        IdDocumento = parseInt(hddIdDocumento.value);
                        break;
                    }


                }
            }

            window.open('frmOrdenCompra1.aspx?IdDocumento=' + IdDocumento, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        //
        function onKeyPressProveedor(obj) {
            if (event.keyCode == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btBuscarProveedor.CLientID %>').click();
            }
            return true;
        }
        //
    </script>

</asp:Content>

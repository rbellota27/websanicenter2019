﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 22-Oct-2009
'Hora    : 12:22 pm
'*************************************************
'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 05-Oct-2009
'Hora    : 10:28 am
'*************************************************
Imports System.Data.SqlClient

Public Class Oficina
    Private obj As New DAO.DAOOficina
    Private objConexion As New DAO.Conexion
    Public Function SelectCboxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.Oficina)
        Return obj.SelectCboxIdBanco(IdBanco)
    End Function
    Public Function SelectxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.Oficina)
        Return obj.SelectxIdBanco(IdBanco)
    End Function
End Class

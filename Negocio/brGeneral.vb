﻿Imports System.Configuration

Public Class brGeneral

    Private _cadenaConexion As String

    Public Property CadenaConexion() As String
        Get
            Return _cadenaConexion
        End Get
        Set(ByVal value As String)
            _cadenaConexion = value
        End Set
    End Property

    Sub New()
        CadenaConexion = ConfigurationManager.ConnectionStrings("Conexion").ConnectionString

    End Sub

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ConfiguracionCorreo

    Dim objDaoConfiguracionCorreo As New DAO.DAOConfiguracionCorreo

    Public Function ConfiguracionCorreo_select(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal estado As Integer) As List(Of Entidades.ConfiguracionCorreo)
        Return objDaoConfiguracionCorreo.ConfiguracionCorreo_select(IdEmpresa, IdTienda, estado)
    End Function

    Public Function _ConfiguracionCorreoTransaction(ByVal obj As Entidades.ConfiguracionCorreo, Optional ByVal getList As Boolean = False) As List(Of Entidades.ConfiguracionCorreo)
        Return objDaoConfiguracionCorreo._ConfiguracionCorreoTransaction(obj, getList)
    End Function

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class PagoCaja
    Dim daoPagoCaja As New DAO.DAOPagoCaja
    Public Function SelectListaxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.PagoCaja)
        Return daoPagoCaja.SelectListaxIdDocumento(IdDocumento)
    End Function
    Public Function fnInsertaPagoCaja(ByVal pagocaja As Entidades.PagoCaja) As Integer
        Return daoPagoCaja.fnInsertaPagoCaja(pagocaja)
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.PagoCaja
        Return daoPagoCaja.SelectxIdDocumento(IdDocumento)
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class Empresa
    Dim obj As New DAO.DAOEmpresa
    Dim objConexion As New DAO.Conexion
    Public Function InsertaConfiguracionEmpresa(ByVal IdEmpresa As Integer, _
    ByVal lEmpresaTienda As List(Of Entidades.EmpresaTienda), _
    ByVal lEmpresaAlmacen As List(Of Entidades.EmpresaAlmacen), _
    ByVal lAreaEmpresa As List(Of Entidades.AreaEmpresa), _
    ByVal lRolEmpresa As List(Of Entidades.Rol_Empresa)) As Boolean

        Return obj.InsertaConfiguracionEmpresa(IdEmpresa, lEmpresaTienda, lEmpresaAlmacen, lAreaEmpresa, lRolEmpresa)
    End Function
End Class

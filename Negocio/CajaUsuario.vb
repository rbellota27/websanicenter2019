﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class CajaUsuario
    Private objConexion As New DAO.Conexion
    Private objDaoCajaUsuario As New DAO.DAOCajaUsuario

    Public Function SelectCboxIdTiendaxIdUsuario(ByVal IdUsuario As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Caja)
        Return objDaoCajaUsuario.SelectCboxIdTiendaxIdUsuario(IdUsuario, IdTienda)
    End Function


    Public Function SelectCboxIdTiendaxIdUsuario_Caja(ByVal IdTienda As Integer, ByVal IdUsuario As Integer) As List(Of Entidades.Caja)
        Return objDaoCajaUsuario.SelectCboxIdTiendaxIdUsuario_Caja(IdTienda, IdUsuario)
    End Function


    Public Function ValidarCajaActivoxIdPersonaxIdTiendaxIdCaja(ByVal idpersona As Integer, ByVal idtienda As Integer, ByVal idcaja As Integer) As Integer
        Return objDaoCajaUsuario.ValidarCajaActivoxIdPersonaxIdTiendaxIdCaja(idpersona, idtienda, idcaja)
    End Function

    Public Function registrarCajaUsuario(ByVal lista As List(Of Entidades.CajaUsuario), ByVal idPersona As Integer) As Boolean
        Dim hecho As Boolean = False

        '**************** obtengo la lista de registros a eliminar
        Dim listaOld As List(Of Entidades.CajaUsuario) = objDaoCajaUsuario.SelectAllxIdPersona(idPersona)
        Dim i As Integer = listaOld.Count - 1
        While i >= 0
            Dim j As Integer = lista.Count - 1
            While j >= 0
                If listaOld.Item(i).IdCaja = lista.Item(j).IdCaja Then
                    listaOld.RemoveAt(i)
                    Exit While
                End If
                j = j - 1
            End While
            i = i - 1
        End While

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************************ elimino los registros de caja usuario
            For i = 0 To listaOld.Count - 1
                objDaoCajaUsuario.DeletexIdCajaxIdPersona(listaOld(i).IdPersona, listaOld(i).IdCaja, cn, tr)
            Next

            '************************** inserto/actualizo los registros
            Dim objDaoUtil As New DAO.DAOUtil
            For i = 0 To lista.Count - 1
                If objDaoUtil.ValidarxDosParametros(cn, tr, "CajaUsuario", "IdCaja", lista(i).IdCaja.ToString, "IdPersona", lista(i).IdPersona.ToString) > 0 Then
                    '*********** actualizo
                    objDaoCajaUsuario.Update(lista(i), cn, tr)
                Else
                    '*********** inserto
                    objDaoCajaUsuario.Insert(lista(i), cn, tr)
                End If
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function SelectxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.Caja)
        Return objDaoCajaUsuario.SelectxIdPersona(idpersona)
    End Function
    Public Function SelectCountxIdTienda(ByVal idtienda As Integer) As Integer
        Return objDaoCajaUsuario.SelectCountxIdTienda(idtienda)
    End Function
    Public Function fnObtenerDeudasxIdCliente(ByVal _IdCliente As Integer) As List(Of Entidades.Cliente)
        Return objDaoCajaUsuario.fnObtenerDeudasxIdCliente(_IdCliente)
    End Function
    'Public Function UpdateCajaUsuario(ByVal lista As List(Of Entidades.CajaUsuario)) As Boolean
    '    Dim hecho As Boolean = False
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim tr As SqlTransaction = Nothing
    '    Try
    '        cn.Open()
    '        tr = cn.BeginTransaction(IsolationLevel.Serializable)
    '        Dim objDaoCajaUsuario As New DAO.DAOCajaUsuario
    '        For i As Integer = 0 To lista.Count - 1
    '            objDaoCajaUsuario.Insert(lista(i), cn, tr)
    '        Next
    '        tr.Commit()
    '        hecho = True
    '    Catch ex As Exception
    '        tr.Rollback()
    '        hecho = False
    '    Finally
    '        If cn.State = ConnectionState.Open Then cn.Close()
    '    End Try
    '    Return hecho
    'End Function

End Class

﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class blZonaAlmacenamiento
    Dim cn As SqlConnection

    Public Function listarZonaAlmacenamiento(ByVal IdTienda As Integer) As List(Of beZonaAlmacenamiento)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.beZonaAlmacenamiento)
            lista = (New DAOZonaAlmacenamiento).ListarZonaAlmacenamiento(cn, IdTienda)

            Return lista
        Catch ex As Exception
            Throw ex
            Throw ex
        End Try
    End Function




    Public Function listarComboZonaAlmacenamiento(ByVal IdTienda As Integer) As List(Of beZonaAlmacenamiento)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.beZonaAlmacenamiento)
            lista = (New DAOZonaAlmacenamiento).listarComboZonaAlmacenamiento(cn, IdTienda)

            Return lista
        Catch ex As Exception
            Throw ex
            Throw ex
        End Try
    End Function



End Class

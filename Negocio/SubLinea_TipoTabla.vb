﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class SubLinea_TipoTabla
    Dim DAO As New DAO.DAOSubLinea_TipoTabla
    Dim objConexion As DAO.Conexion

    Public Function TipoTablaSublineaConFig_Paginado(ByVal idtipoexist As Integer, ByVal idlinea As Integer, _
                                                     ByVal idsublinea As Integer, ByVal estado As Integer, _
                                                     ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Return DAO.TipoTablaSublineaConFig_Paginado(idtipoexist, idlinea, idsublinea, estado, PageIndex, PageSize)
    End Function

    Public Function GrabaSubLineaTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal lista As List(Of Entidades.SubLinea_TipoTabla)) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If lista.Count = 0 Then
                If Not DAO.AnularSubLineaTipoTabla(idlinea, idsublinea, cn, tr) Then
                    Throw New Exception("Fracasó el Proceso de Inserción")
                End If
            Else
                If Not DAO.AnularSubLineaTipoTabla(idlinea, lista.Item(0).IdSubLinea, cn, tr) Then
                    Throw New Exception("Fracasó el Proceso de Inserción")
                End If
            End If
            If Not DAO.GrabaSubLineaTipoTabla(lista, cn, tr) Then
                Throw New Exception("Fracasó el Proceso de Inserción")
            End If
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function SelectAllxTipoExistxIdLineaxIdSubLineaxEstado(ByVal idtipoexist As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal estado As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Return DAO.SelectAllxTipoExistxIdLineaxIdSubLineaxEstado(idtipoexist, idlinea, idsublinea, estado)
    End Function
    Public Function SelectxIdTipoExistenciaxIdLineaxIdSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Return DAO.SelectxIdTipoExistenciaxIdLineaxIdSubLinea(idlinea, idsublinea)
    End Function

    Public Function SelectxIdLineaxIdSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal tipouso As String) As List(Of Entidades.SubLinea_TipoTabla)
        Return DAO.SelectxIdLineaxIdSubLinea(idlinea, idsublinea, tipouso)
    End Function

    Public Function SelectAllTipoTablaxIdSubLinea(ByVal idsublinea As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Return DAO.SelectAllTipoTablaxIdSubLinea(idsublinea)
    End Function

    Public Function SelectxIdLineaxIdSubLineaxIdProducto(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal tipouso As String, ByVal idproducto As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Return DAO.SelectxIdLineaxIdSubLineaxIdProducto(idlinea, idsublinea, tipouso, idproducto)
    End Function
End Class

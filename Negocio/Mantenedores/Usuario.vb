﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Public Class Usuario
    Dim obj As New DAO.DAOUsuario
    Public Function ActualizaUsuarioAtencionPago(ByVal ObjUsuario As Entidades.Usuario) As Boolean
        Return obj.ActualizaUsuarioAtencionPago(ObjUsuario)
    End Function
    Public Function SelectCboAtencion() As List(Of Entidades.Usuario)
        Return obj.SelectCboAtencion
    End Function
    Public Function Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_Distinct(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPerfil As Integer) As DataTable
        Return obj.Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_Distinct(IdEmpresa, IdTienda, IdPerfil)
    End Function

    Public Function Usuario_SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.Comisionista
        Return obj.Usuario_SelectxIdPersona(IdPersona)
    End Function
    Public Function Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_DT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPerfil As Integer) As DataTable
        Return obj.Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_DT(IdEmpresa, IdTienda, IdPerfil)
    End Function
    Public Function ValidarIdentidad(ByVal Login As String, ByVal Clave As String) As Integer
        Return obj.ValidarIdentidad(Login, Clave)
    End Function

    Public Function ValidarIdentidadPerfil(ByVal Login As String, ByVal Clave As String) As Integer
        Return obj.ValidarIdentidadPerfil(Login, Clave)
    End Function

    Public Function ValidarIdentidadPerfilPermisoMasivo(ByVal IdUsuario As Integer) As Integer
        Return obj.ValidarIdentidadPerfilPermisoMasivo(IdUsuario)
    End Function

    Public Function InsertaUsuario(ByVal usuario As Entidades.Usuario, ByVal lista As List(Of Entidades.PerfilUsuario), ByVal lista_area As List(Of Entidades.Usuario_Area)) As Boolean
        Return obj.InsertaUsuario(usuario, lista, lista_area)
    End Function
    Public Function ActualizaUsuario(ByVal usuario As Entidades.Usuario, ByVal lista As List(Of Entidades.PerfilUsuario), ByVal lista_area As List(Of Entidades.Usuario_Area), ByVal UpdatePassword As Boolean) As Boolean
        Return obj.ActualizaUsuario(usuario, lista, lista_area, UpdatePassword)
    End Function
    Public Function SelectCbo() As List(Of Entidades.Natural)
        Return obj.SelectCbo
    End Function
    Public Function SelectUsuarioTiendaAreaxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.UsuarioTiendaArea)
        Return obj.SelectUsuarioTiendaAreaxIdPersona(IdPersona)
    End Function

    Public Function SelectTodosUsuario(ByVal IdPersona As Integer) As Entidades.Usuario
        Return obj.TodosUsuarios(IdPersona)
    End Function
End Class

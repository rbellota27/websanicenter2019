﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Pais
    Private objDaoPais As New DAO.DAOPais
    Public Function SelectAllActivoxIdProcedencia_Cbo(ByVal IdProcedencia As String) As List(Of Entidades.Pais)
        Return objDaoPais.SelectAllActivoxIdProcedencia_Cbo(IdProcedencia)
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.Pais)
        Return objDaoPais.SelectAllActivo()
    End Function



    Public Function SelectAllActivoTodos() As List(Of Entidades.Pais)
        Return objDaoPais.SelectAllActivoTodos()
    End Function

    Public Function SelectxIdProcedencia(ByVal IdPersona As Integer) As Entidades.Pais
        Return objDaoPais.SelectxIdProcedencia(IdPersona)
    End Function

    Public Function SelectxIdPais(ByVal IdPais As Integer) As Entidades.Pais
        Return objDaoPais.SelectxIdPais(IdPais)
    End Function

    Public Function InsertaUsuario(ByVal usuario As Entidades.Pais, ByVal lista As List(Of Entidades.Procedencia)) As Boolean
        Return objDaoPais.InsertaPais(usuario, lista)
    End Function
    Public Function ActualizaUsuario(ByVal usuario As Entidades.Pais, ByVal lista As List(Of Entidades.Procedencia)) As Boolean
        Return objDaoPais.ActualizaUsuario(usuario, lista)
    End Function

    Public Function SelectAllActivoxNomPais_Cbo(ByVal IdProcedencia As String) As List(Of Entidades.Pais)
        Return objDaoPais.SelectAllActivoxNoPais_Cbo(IdProcedencia)
    End Function

    Public Function SelectxEstadoxNombre(ByVal nombre As String, ByVal estado As String) As List(Of Entidades.Pais)
        Return objDaoPais.SelectxEstadoxNombre(nombre, estado)
    End Function

End Class

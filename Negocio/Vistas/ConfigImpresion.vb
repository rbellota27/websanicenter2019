﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports DAO.DAOConfigImpresion
Imports System.Data.SqlClient
Public Class ConfigImpresion
    Dim objImpresion As New DAO.DAOConfigImpresion
    Dim objconn As New DAO.Conexion

    Public Function SelectAll(ByVal idTipodocumento As Integer, ByVal idTipoimpresion As Integer) As List(Of Entidades.DocsImpresionView)
        Return objImpresion.SelectAll(idTipodocumento, idTipoimpresion)
    End Function
    Public Function llenaCbo() As List(Of Entidades.DocsImpresionView)
        Return objImpresion.llenaCbo
    End Function
    Public Function UpdateInsert(ByVal objimp As List(Of Entidades.DocsImpresionView)) As Boolean
        Dim cn As SqlConnection = objconn.ConexionSIGE()
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            For Each ob As Entidades.DocsImpresionView In objimp
                objImpresion.InsertConfigImpresion(ob)
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
End Class

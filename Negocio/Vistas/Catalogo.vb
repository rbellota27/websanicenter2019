﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*************************   MIERCOLES 02 06 2010


Public Class Catalogo
    Dim obj As New DAO.DAOCatalogo

    Public Function SelectxIdProductoDetalladoDescripcion(ByVal IdProducto As Integer) As Entidades.Catalogo
        Return obj.SelectxIdProductoDetalladoDescripcion(IdProducto)
    End Function
    Public Function SelectProductoxIdProducto(ByVal IdProducto As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer) As Entidades.Catalogo
        Return obj.SelectProductoxIdProducto(IdProducto, IdTienda, IdTipoPV)
    End Function

    Public Function SelectProductoxCodigoxDescripcion(ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal CodigoProducto As String, ByVal NombreProducto As String) As List(Of Entidades.Catalogo)
        Return obj.SelectProductoxCodigoxDescripcion(IdTienda, IdTipoPV, CodigoProducto, NombreProducto)
    End Function

    Public Function Producto_Consultar_Stock_Precio_Venta(ByVal IdProducto As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMonedaDestino As Integer, ByVal Fecha As Date, ByVal IdEmpresa As Integer, Optional ByVal IdTipoAlmacen As Integer = 0) As List(Of Entidades.Catalogo)
        Return obj.Producto_Consultar_Stock_Precio_Venta(IdProducto, IdTienda, IdTipoPV, IdMonedaDestino, Fecha, IdEmpresa, IdTipoAlmacen)
    End Function

    Public Function CatalogoProductoSelectxParams(ByVal IdProducto As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMonedaDestino As Integer, ByVal IdAlmacen As Integer, Optional ByVal addListaUM_venta As Boolean = False) As Entidades.Catalogo
        Return obj.CatalogoProductoSelectxParams(IdProducto, IdTienda, IdTipoPV, IdMonedaDestino, IdAlmacen, addListaUM_venta)
    End Function

    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2(ByVal TipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable, ByVal codigoProducto As String, Optional ByVal filtroProductoCampania As Boolean = False) As List(Of Entidades.Catalogo)
        Return obj.CatalogoProducto_PV_Paginado_Cotizacion_V2(TipoExistencia, IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania)
    End Function
    Public Function CatalogoProducto_PV_Cotizacion_V2CodBarras(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable, ByVal codigoProducto As String, ByVal codBarras As String, Optional ByVal filtroProductoCampania As Boolean = False) As List(Of Entidades.Catalogo)
        Return obj.CatalogoProducto_PV_Cotizacion_V2CodBarras(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codBarras)
    End Function
    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarras(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable, ByVal codigoProducto As String, ByVal codbarras As String, Optional ByVal filtroProductoCampania As Boolean = False, Optional ByVal IdTipoExistencia As Integer = 0, Optional ByVal codigoProveedor As String = "") As List(Of Entidades.Catalogo)
        Return obj.CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarras(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codbarras, IdTipoExistencia, codigoProveedor)
    End Function
    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable, ByVal codigoProducto As String, ByVal codbarras As String, Optional ByVal filtroProductoCampania As Boolean = False, Optional ByVal IdTipoExistencia As Integer = 0, Optional ByVal codigoProveedor As String = "", Optional ByVal IdTipoOperacion As Integer = 1) As List(Of Entidades.Catalogo)
        Return obj.CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codbarras, IdTipoExistencia, codigoProveedor, IdTipoOperacion)
    End Function
    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoV2(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable, ByVal codigoProducto As String, ByVal codbarras As String, Optional ByVal filtroProductoCampania As Boolean = False, Optional ByVal IdTipoExistencia As Integer = 0, Optional ByVal codigoProveedor As String = "", Optional ByVal IdTipoOperacion As Integer = 1) As List(Of Entidades.Catalogo)
        Return obj.CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoV2(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codbarras, IdTipoExistencia, codigoProveedor, IdTipoOperacion)
    End Function
    ''Agregando descuento al producto
    Public Function GetDescuentoxIdProducto(ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdTipoPV As Integer, ByVal IdTipoOperacion As Integer, ByVal IdProducto As Integer) As Entidades.Catalogo
        Return obj.GetDescuentoxIdProducto(IdUsuario, IdCondicionPago, IdMedioPago, IdTipoPV, IdTipoOperacion, IdProducto)
    End Function
    Public Function CatalogoProducto_PV_Paginado_Cotizacion(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal IdPais As Integer, ByVal IdFabricante As Integer, ByVal IdMarca As Integer, ByVal IdModelo As Integer, ByVal IdFormato As Integer, ByVal IdProveedor As Integer, ByVal IdEstilo As Integer, ByVal IdTransito As Integer, ByVal IdCalidad As Integer) As List(Of Entidades.Catalogo)
        Return obj.CatalogoProducto_PV_Paginado_Cotizacion(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, IdPais, IdFabricante, IdMarca, IdModelo, IdFormato, IdProveedor, IdEstilo, IdTransito, IdCalidad)
    End Function

    Public Function CatalogoProducto_PV_Paginado(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As Integer, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer) As List(Of Entidades.Catalogo)
        Return obj.CatalogoProducto_PV_Paginado(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber)
    End Function

    Public Function CatalogoProducto_PV(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As Integer, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoPV As Integer) As List(Of Entidades.Catalogo)
        Return obj.CatalogoProducto_PV(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdAlmacen, IdTipoPV)
    End Function

    Public Function SelectxIdSublineaxIds(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, _
    ByVal IdAlmacen As Integer, ByVal IdSublinea As Integer) As List(Of Entidades.Catalogo)
        Return obj.SelectxIdSublineaxIds(IdEmpresa, IdTienda, IdAlmacen, IdSublinea)
    End Function

    Public Function SelectxIdSublineaxIdsxNombre(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, _
                                                     ByVal IdAlmacen As Integer, ByVal IdSublinea As Integer, _
                                                     ByVal NombreProducto As String) As List(Of Entidades.Catalogo)
        Return obj.SelectxIdSublineaxIdsxNombre(IdEmpresa, IdTienda, IdAlmacen, IdSublinea, NombreProducto)
    End Function

    Public Function RptCatalogo(ByVal IdTienda As Integer, ByVal IdSubLinea As Integer) As DataSet
        Return obj.RptCatalogo(IdTienda, IdSubLinea)
    End Function
    Public Function SelectAll() As List(Of Entidades.Catalogo)
        Return obj.SelectAll
    End Function
    Public Function RptCatalogoProductoPVxParametros(ByVal IdTienda As Integer, ByVal idtipopv As Integer, ByVal Idlinea As Integer, ByVal idsublinea As Integer, ByVal umprincipal As Integer, ByVal umretazo As Integer) As DataSet
        Return obj.RptCatalogoProductoPVxParametros(IdTienda, idtipopv, Idlinea, idsublinea, umprincipal, umretazo)
    End Function
End Class

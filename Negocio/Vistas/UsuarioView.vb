﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'modifi 19nov2009 03:00

Imports System.Data.SqlClient

Public Class UsuarioView
    Private obj As New DAO.DAOUsuarioView

    Public Function ChangePassword(ByVal IdUsuario As Integer, ByVal ClaveOld As String, ByVal ClaveNew As String, ByVal ClaveNewConfirm As String) As Boolean

        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim hecho As Boolean = False

        Try

            cn.Open()
            hecho = (New DAO.DAOUsuario).ChangePassword(IdUsuario, ClaveOld, ClaveNew, ClaveNewConfirm, cn)

        Catch ex As Exception
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function SelectxEstadoxNombre(ByVal nombre As String, ByVal estado As String) As List(Of Entidades.UsuarioView)
        Return obj.SelectxEstadoxNombre(nombre, estado)
    End Function

    Public Function SelectxPersonaView(ByVal nombre As String) As List(Of Entidades.UsuarioView)
        Return obj.SelectxPersonaView(nombre)
    End Function

    Public Function SelectxEstadoxNombre_IdPerfil(ByVal nombre As String, ByVal estado As String, ByVal IdPerfil As Integer) As List(Of Entidades.UsuarioView)
        Return obj.SelectxEstadoxNombre_IdPerfil(nombre, estado, IdPerfil)
    End Function

    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.UsuarioView
        Return obj.SelectxIdPersona(IdPersona)
    End Function
    Public Function FunVerTodasTiendas(ByVal idusuario As Integer, ByVal idpermiso As Integer) As Integer
        Return obj.FunVerTodasTiendas(idusuario, idpermiso)
    End Function
    Public Function SelectDocumUsuario(ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal FechaInicio As String, ByVal fechafin As String) As DataSet
        Return obj.getDocumentosUsuario(idEmpresa, idTienda, FechaInicio, fechafin)
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class SerieChequeView
    Private obj As New DAO.DAOSerieChequeView

    Public Function SelectAll() As List(Of Entidades.SerieChequeView)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.SerieChequeView)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.SerieChequeView)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectCboxIdBancoxIdCuentaBancaria(ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As List(Of Entidades.SerieChequeView)
        Return obj.SelectCboxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
    End Function
    Public Function SelectCboxIdCuentaBancaria(ByVal IdCuentaBancaria As Integer) As List(Of Entidades.SerieChequeView)
        Return obj.SelectCboxIdCuentaBancaria(IdCuentaBancaria)
    End Function

    Public Function InsertT_GetID(ByVal x As Entidades.SerieChequeView) As Integer
        Return obj.InsertT_GetID(CType(x, Entidades.SerieCheque))
    End Function
    Public Function InsertT(ByVal x As Entidades.SerieChequeView) As Boolean
        Return obj.InsertT(CType(x, Entidades.SerieCheque))
    End Function
    Public Function UpdateT(ByVal x As Entidades.SerieChequeView) As Boolean
        Return obj.UpdateT(CType(x, Entidades.SerieCheque))
    End Function


    Private P As New Entidades.SerieChequeView
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.SerieChequeView) As Boolean
        'x va a ser el objeto a filtrar

        If (x.Id = P.Id Or P.Id = Nothing Or P.Id = 0) _
        And (x.IdBanco = P.IdBanco Or P.IdBanco = Nothing Or P.IdBanco = 0) _
        And (x.IdCuentaBancaria = P.IdCuentaBancaria Or P.IdCuentaBancaria = Nothing Or P.IdCuentaBancaria = 0) _
        And (P.Serie = "" Or x.Serie.ToLower.Trim.StartsWith(P.Serie.ToLower.Trim)) _
        And ((IsNothing(P.Estado)) Or x.Estado = P.Estado) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.SerieChequeView, _
    ByVal List As List(Of Entidades.SerieChequeView)) As List(Of Entidades.SerieChequeView)

        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    'Public Function FiltrarListaxRangoFechas(ByVal filtro As Entidades.SerieChequeView, _
    '   ByVal List As List(Of Entidades.SerieChequeView), _
    '   ByVal fecha_ini As Date, ByVal fecha_fin As Date) As List(Of Entidades.SerieChequeView)

    '    Dim L2, L3 As List(Of Entidades.SerieChequeView)
    '    L3 = New List(Of Entidades.SerieChequeView)
    '    Dim i As Integer

    '    P = filtro

    '    L2 = List.FindAll(AddressOf FiltrarEntidadxParametros)
    '    For i = 0 To L2.Count - 1
    '        If L2(i).FechaMov >= fecha_ini And L2(i).FechaMov <= fecha_fin Then
    '            L3.Add(L2(i))
    '        End If
    '    Next
    '    Return L3

    'End Function

    'Public Function ValidarEntidadxNombre(ByVal filtro As Entidades.SerieChequeView, ByVal List As List(Of Entidades.SerieChequeView)) As Boolean
    '    'X es el valor a verificar, 

    '    Dim x As New Entidades.SerieChequeView
    '    x.IdSerieCheque = filtro.IdSerieCheque
    '    x.NroOperacion = filtro.NroOperacion

    '    Dim List2 As List(Of Entidades.SerieChequeView)
    '    List2 = ValidarLista(x, List)

    '    If List2.Count > 0 Then 'Existen registros  
    '        Return False
    '    Else
    '        Return True
    '    End If

    'End Function

    'Private Function ValidarEntidadxParametros(ByVal x As Entidades.SerieChequeView) As Boolean
    '    'Se utiliza a la hora de insertar o eliminar
    '    'x va a ser el objeto a filtrar
    '    'En este caso no se cuenta el registro analizado
    '    If (x.IdSerieCheque <> P.IdSerieCheque) _
    '    And (x.NroOperacion.ToLower.Trim = P.NroOperacion.ToLower.Trim Or P.NroOperacion = "") Then
    '        'And (x.Estado = P.Estado Or (Not P.Estado.HasValue))
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function
    'Public Function ValidarLista(ByVal filtro As Entidades.SerieChequeView, ByVal List As List(Of Entidades.SerieChequeView)) As List(Of Entidades.SerieChequeView)
    '    P = filtro
    '    Return List.FindAll(AddressOf ValidarEntidadxParametros)

    'End Function

End Class

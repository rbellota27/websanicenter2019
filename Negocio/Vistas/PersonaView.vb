﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'08_febrero_2010
Public Class PersonaView
    Private obj As New DAO.DAOPersonaView


    Public Function SelectActivoxParam_PaginadoUsuario(ByVal texto As String, ByVal opcion As Integer, ByVal PageSize As Integer, ByVal PageNumber As Integer) As List(Of Entidades.PersonaView)
        Return obj.SelectActivoxParam_PaginadoUsuario(texto, opcion, PageSize, PageNumber)
    End Function

    Public Function SelectActivoxParam_Paginado(ByVal texto As String, ByVal opcion As Integer, ByVal PageSize As Integer, ByVal PageNumber As Integer) As List(Of Entidades.PersonaView)
        Return obj.SelectActivoxParam_Paginado(texto, opcion, PageSize, PageNumber)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.PersonaView)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectxIdPersona(ByVal idpersona As Integer) As Entidades.PersonaView
        Return obj.SelectxIdPersona(idpersona)
    End Function


    Public Function SelectxIdPersonaMaestrosObra(ByVal idpersona As Integer) As Entidades.PersonaView
        Return obj.SelectxIdPersonaMaestros(idpersona)
    End Function
    Public Function SelectActivoxParam(ByVal texto As String, ByVal opcion As Integer) As List(Of Entidades.PersonaView)
        Return obj.SelectActivoxParam(texto, opcion)
    End Function
    Public Function SelectActivoxPersonaRucDni(ByVal texto As String, ByVal ruc As String, ByVal dni As String, ByVal opcion As Integer, ByVal PageSize As Integer, ByVal PageNumber As Integer) As List(Of Entidades.PersonaView)
        Return obj.SelectActivoxPersonaRucDni(texto, ruc, dni, opcion, PageSize, PageNumber)
    End Function
    Public Function SelectActivoxPersonaRucDniDetDoc(ByVal texto As String, ByVal opcion As Integer, ByVal PageSize As Integer, ByVal PageNumber As Integer) As List(Of Entidades.PersonaView)
        Return obj.SelectActivoxPersonaRucDniDetDoc(texto, opcion, PageSize, PageNumber)
    End Function
End Class

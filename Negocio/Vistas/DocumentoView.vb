﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************************  '08_febrero_2010

Public Class DocumentoView
    Private obj As New DAO.DAODocumentoView
    Public Function DocumentoBuscarDocxSeriexCodigo(ByVal serie As Integer, ByVal codigo As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoBuscarDocxSeriexCodigo(serie, codigo, IdTipoOperacion)
    End Function
    Public Function DocumentoDetalle(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer, ByVal idtipodoc As Integer, ByVal idestadoDoc As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal persona As Integer, ByVal serie As Integer, ByVal codigo As Integer, ByVal rango As Integer, ByVal resumen As Integer, ByVal FiltrarSemana As Integer, ByVal YearIni As Integer, ByVal SemanaIni As Integer, ByVal YearFin As Integer, ByVal SemanaFin As Integer) As DataSet
        Return obj.DocumentoDetalle(idempresa, idtienda, idmoneda, idtipodoc, idestadoDoc, fechainicio, fechafin, persona, serie, codigo, rango, resumen, FiltrarSemana, YearIni, SemanaIni, YearFin, SemanaFin)
    End Function
    Public Function DocumentoDetallexMes(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer, ByVal idtipodoc As Integer, ByVal idestadoDoc As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal persona As Integer, ByVal serie As Integer, ByVal codigo As Integer, ByVal rango As Integer, ByVal resumen As Integer, ByVal FiltrarSemana As Integer, ByVal YearIni As Integer, ByVal SemanaIni As Integer, ByVal YearFin As Integer, ByVal SemanaFin As Integer) As DataSet
        Return obj.DocumentoDetallexMes(idempresa, idtienda, idmoneda, idtipodoc, idestadoDoc, fechainicio, fechafin, persona, serie, codigo, rango, resumen, FiltrarSemana, YearIni, SemanaIni, YearFin, SemanaFin)
    End Function
    Public Function SelectDocumUsuario(ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal FechaInicio As String, ByVal fechafin As String, ByVal idtipodocumento As Integer, ByVal tipo As Integer, ByVal ascdes As Int16) As DataSet
        Return obj.SelectDocumUsuario(idEmpresa, idTienda, FechaInicio, fechafin, idtipodocumento, tipo, ascdes)
    End Function
    Public Function SelectDocumMontDiferentes(ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal FechaInicio As String, ByVal fechafin As String) As DataSet
        Return obj.SelectDocumMontDiferentes(idEmpresa, idTienda, FechaInicio, fechafin)
    End Function
    Public Function SelectDocumentMediosPagos(ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal FechaInicio As String, ByVal fechafin As String) As DataSet
        Return obj.SelectDocumentMediosPagos(idEmpresa, idTienda, FechaInicio, fechafin)
    End Function
    Public Function SelectCantidadFilasDocumento(ByVal idEmpresa As Integer, ByVal idTipoDocumento As Integer) As Integer
        Return obj.SelectCantidadFilasDocumento(idEmpresa, idTipoDocumento)
    End Function
    Public Function SelectCantidadFilasDocumentoProducto(ByVal IdsProductos As String) As Integer
        Return obj.SelectCantidadFilasDocumentoProducto(IdsProductos)
    End Function

End Class

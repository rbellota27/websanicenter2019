﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'08_febrero_2010
Public Class CXCResumenView
    Dim obj As New DAO.DAOCxCREsumen
    Public Function SelectGrillaCxCResumen(ByVal idempresa As Integer, ByVal idtienda As Integer) As List(Of Entidades.CxCResumenView)
        Return obj.SelectGrillaCxCResumen(idempresa, idtienda)
    End Function

    Public Function SelectGrillaCxCResumenCliente(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idPersona As Integer) As List(Of Entidades.CxCResumenView)

        Return obj.SelectGrillaCxCResumenCliente(idempresa, idtienda, idPersona)
    End Function
    Public Function SelectGrillaCxCResumenDocumento(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer, ByVal idpersona As Integer) As List(Of Entidades.CxCResumenView)
        Return obj.SelectGrillaCxCResumenDocumento(idempresa, idtienda, idmoneda, idpersona)
    End Function
    Public Function GetDetalleAbonos(ByVal iddocumento As Integer) As List(Of Entidades.CxCResumenView)
        Return obj.GetDetalleAbonos(iddocumento)
    End Function
    Public Function getInventarioValorizado(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer) As DataSet
        Return obj.getInventarioValorizado(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea)
    End Function
    Public Function getGuiaRecepcionOrdendespacho(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idtipodocumento As Integer, ByVal fechainicio As String, ByVal fechafin As String) As DataSet
        Return obj.getGuiaRecepcionOrdendespacho(idempresa, idalmacen, idtipodocumento, fechainicio, fechafin)
    End Function
    Public Function getRankingProductos(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal nprimeros As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal IdVendedor As Integer) As DataSet
        Return obj.getRankingProductos(idempresa, idtienda, nprimeros, fechainicio, fechafin, idlinea, idsublinea, IdVendedor)
    End Function
    Public Function getUtilidadxProducto(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal idlinea As Integer, ByVal idsublinea As Integer) As DataSet
        Return obj.getUtilidadxProducto(idempresa, idtienda, fechainicio, fechafin, idlinea, idsublinea)

    End Function





    Public Function getKardexValorizado(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal Signo As String, ByVal Cantidad As Decimal) As DataSet
        Return obj.getKardexValorizado(IdEmpresa, IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, Signo, Cantidad)
    End Function

End Class

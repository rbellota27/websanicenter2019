﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Serie
    Dim daoSerie As New DAO.DAOSerie
    Public Function SelectCboxIdsEmpTienTDocNotinAsignacion(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.Serie)
        Return daoSerie.SelectCboxIdsEmpTienTDocNotinAsignacion(IdEmpresa, IdTienda, IdTipoDocumento)
    End Function
  

    Public Function SelectCboxIdsEmpTienTDoc(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.Serie)
        Return daoSerie.SelectCboxIdsEmpTienTDoc(IdEmpresa, IdTienda, IdTipoDocumento)
    End Function
    Public Function LLenarCboSeriexIdUsuarioValidacion(ByVal Idusuario As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Serie)
        Return daoSerie.LLenarCboSeriexIdUsuarioValidacion(Idusuario, IdTienda)
    End Function

    Public Function LLenarCboSeriexInforme(ByVal Idusuario As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Serie)
        Return daoSerie.LLenarCboSeriexInforme(Idusuario, IdTienda)
    End Function
    Public Function GenerarCodigo(ByVal IdSerie As Integer) As String
        Dim daoDocumento As New DAO.DAODocumento
        Dim Codigo As String = Nothing
        Try
            Dim serie As New Entidades.Serie
            serie = daoSerie.SelectxIdSerie(IdSerie)

            Dim CodigoUltimo As String = daoDocumento.SelectMaxCodxIdSerie(IdSerie)
            If CodigoUltimo = "" Then 'Esto es por que todavía no se ha grabado ningún documento con esta serie
                For i As Integer = 1 To Len(CStr(serie.Cantidad)) - Len(CStr(serie.Inicio))
                    Codigo += "0"
                Next
                Codigo += CStr(serie.Inicio)
            Else
                If CInt(CodigoUltimo) = serie.Cantidad Then
                    Throw New Exception("La Cantidad de la Serie ha llegado al límite")
                End If
                For i As Integer = 1 To Len(CStr(serie.Cantidad)) - Len(CStr(CInt(CodigoUltimo)))
                    Codigo += "0"
                Next
                Codigo += CStr(CInt(CodigoUltimo) + 1)
            End If
            Return Codigo
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function InsertaSerie(ByVal serie As Entidades.Serie) As Boolean
        Return daoSerie.InsertaSerie(serie)
    End Function
    Public Function ActualizaSerie(ByVal serie As Entidades.Serie) As Boolean
        Return daoSerie.ActualizaSerie(serie)
    End Function
    Public Function SelectxEmpresaxTiendaxTipoDocxEstado(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDoc As Integer, ByVal estado As String) As List(Of Entidades.Serie)
        Return daoSerie.SelectxEmpresaxTiendaxTipoDocxEstado(IdEmpresa, IdTienda, IdTipoDoc, estado)
    End Function
    Public Function SelectxIdSerie(ByVal IdSerie As Integer) As Entidades.Serie
        Return daoSerie.SelectxIdSerie(IdSerie)
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Calidad
    Private objDaoCalidad As New DAO.DAOCalidad
    Public Function SelectAllActivo() As List(Of Entidades.Calidad)
        Return objDaoCalidad.SelectAllActivo
    End Function
    Public Function SelectAll() As List(Of Entidades.Calidad)
        Return objDaoCalidad.SelectAll
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Calidad)
        Return objDaoCalidad.SelectAllInactivo
    End Function
    Public Function InsertaCalidad(ByVal Estilo As Entidades.Calidad) As Boolean
        Return objDaoCalidad.InsertaCalidad(Estilo)
    End Function
    Public Function ActualizaCalidad(ByVal estilo As Entidades.Calidad) As Boolean
        Return objDaoCalidad.ActualizaCalidad(estilo)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Calidad)
        Return objDaoCalidad.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Calidad)
        Return objDaoCalidad.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Calidad)
        Return objDaoCalidad.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Calidad)
        Return objDaoCalidad.SelectxId(id)
    End Function
End Class

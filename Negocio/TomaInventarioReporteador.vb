﻿
Public Class TomaInventarioReporteador
    Dim obj As New DAO.DAOTomaInventarioReporteador

    'Public Function ActualizaAlmacen(ByVal almacen As Entidades.TomaInventarioReporteador) As Boolean
    '    Return obj.ActualizaAlmacen(almacen)
    'End Function
    Public Function GetTomaInventario(ByVal idInventario As Integer) As Entidades.TomaInventarioReporteador
        Return obj.GetTomaInventario(idInventario)
    End Function

    Public Function Update(ByVal idInventario As Entidades.TomaInventarioReporteador) As Integer
        Return obj.UpdateTomaInventario(idInventario)
    End Function

    Public Function SelectAll(ByVal idTienda As Integer, ByVal fecha As Date) As List(Of Entidades.TomaInventarioReporteador)
        Return obj.SelectAll(idTienda, fecha)
    End Function
End Class

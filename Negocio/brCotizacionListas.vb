﻿Imports System.Data
Imports System.Data.SqlClient
Imports Entidades
Imports DAO
Imports System.Configuration

Public Class brCotizacionListas
    Inherits brGeneral

    Public Function obtenerListas(ByVal IdUsuario As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer,
                                  ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdCondicionPago As Integer,
                                  ByVal CodDep As String, ByVal CodProv As String, ByVal IdTipoExistencia As Integer,
                                  ByVal IdMangitud As Integer, ByRef Permisos As String, ByVal Parametros As String) As beCotizacionListas
        'ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, 

        Dim obeCotizacionListas As New beCotizacionListas

        Try
            Using con As New SqlConnection(CadenaConexion)
                con.Open()
                Dim obj As New DAO_CotizacionListas
                obeCotizacionListas = obj.obtenerListas(IdUsuario, IdEmpresa, IdTienda, IdTipoDocumento, IdTipoOperacion, IdCondicionPago, CodDep, CodProv, IdTipoExistencia, IdMangitud, Permisos, Parametros, con)
            End Using
        Catch ex As Exception

        End Try
        Return obeCotizacionListas
    End Function

    Public Function obtenerProductos(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer,
                                     ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer,
                                     ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer,
                                     ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable, ByVal codigoProducto As String, ByVal codbarras As String,
                                     Optional ByVal filtroProductoCampania As Boolean = False, Optional ByVal IdTipoExistencia As Integer = 0, Optional ByVal codigoProveedor As String = "",
                                     Optional ByVal IdTipoOperacion As Integer = 1) As List(Of Entidades.Catalogo)

        Dim lbeCotizacionListas As New List(Of Entidades.Catalogo)

        Try
            Using con As New SqlConnection(CadenaConexion)
                con.Open()
                Dim obj As New DAO_CotizacionListas
                lbeCotizacionListas = obj.obtenerProductos(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize,
                                                           pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania,
                                                           codbarras, IdTipoExistencia, codigoProveedor, IdTipoOperacion, con)
            End Using
        Catch ex As Exception

        End Try
        Return lbeCotizacionListas
    End Function

End Class

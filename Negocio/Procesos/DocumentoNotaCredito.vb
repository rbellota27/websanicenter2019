﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'********************  JUEVES 25 FEB 2010 HORA 10:37 AM










'******************* JUEVES 21 ENERO 2010 HORA 03_52 PM
Imports System.Data.SqlClient

Public Class DocumentoNotaCredito

    Private objDaoDocumentoNotaCredito As New DAO.DAODocumentoNotaCredito
    Private objConexion As New DAO.Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private tr As SqlTransaction

    Public Function DocumentoNotaCredito_AplicacionContraEfectivoCaja(ByVal objMovCaja As Entidades.MovCaja, ByVal objPagoCaja As Entidades.PagoCaja, ByVal opcionSaldoRestante As Integer) As Boolean

        Dim hecho As Boolean = False
        Try

            cn = objConexion.ConexionSIGE

            cn.Open()
            tr = cn.BeginTransaction

            If (objMovCaja Is Nothing Or objPagoCaja Is Nothing) Then
                Throw New Exception("NO SE HA DETERMINADO UN MOV. DE CAJA. VERIFIQUE LOS DATOS INGRESADOS. NO SE PERMITE LA OPERACIÓN.")
            End If

            '**************** APP CONTRA EFECTIVO DE CAJA
            objDaoDocumentoNotaCredito.DocumentoNotaCredito_AplicacionEfectivoCaja(objMovCaja.IdDocumento, objMovCaja.IdEmpresa, objMovCaja.IdTienda, objMovCaja.IdCaja, objMovCaja.IdPersona, objMovCaja.IdTipoMovimiento, objPagoCaja.IdMedioPago, objPagoCaja.IdMoneda, objPagoCaja.Factor, objPagoCaja.Monto, cn, tr)

            '******************** VALIDAMOS LA ACCION A TOMAR ( SALDO RESTANTE )
            objDaoDocumentoNotaCredito.DocumentoNotaCreditoAplicacion_ValSaldoRestante(objMovCaja.IdDocumento, opcionSaldoRestante, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function











    Public Function getDataSetDocumentoPrint(ByVal IdDocumento As Integer) As DataSet

        Dim ds As New DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try

            Dim cmd As New SqlCommand("_CR_DocumentoNotaCreditoCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoNotaCreditoCab")

            cmd = New SqlCommand("_CR_DocumentoNotaCreditoDetalle", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoNotaCreditoDetalle")

        Catch ex As Exception
            Throw ex
        End Try

        Return ds

    End Function

    Public Function DocumentoNotaCredito_UpdateSaldoFavorCliente(ByVal IdDocumento As Integer, ByVal monto As Decimal) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            objDaoDocumentoNotaCredito.DocumentoNotaCredito_UpdateSaldoFavorCliente(IdDocumento, monto, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function



    Public Function DocumentoNotaCreditoAplicacion_DeshacerAplicacion(ByVal IdDocumento As Integer, ByVal deshacer_AppDeudas As Boolean, ByVal deshacer_AppEfectivoCaja As Boolean) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            objDaoDocumentoNotaCredito.DocumentoNotaCreditoAplicacion_DeshacerAplicacion(IdDocumento, deshacer_AppDeudas, deshacer_AppEfectivoCaja, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function


    Public Function DocumentoNotaCredito_AplicacionDeudas(ByVal IdDocumentoNotaCredito As Integer, ByVal opcionSaldoRestante As Integer, ByVal lista As List(Of Entidades.MovCuenta)) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction()

            '******************************* Aplicacion de las Deudas
            For i As Integer = 0 To lista.Count - 1
                '******* En la Propiedad Monto almacenamos el valor del Abono
                objDaoDocumentoNotaCredito.DocumentoNotaCredito_AplicacionDeudas(IdDocumentoNotaCredito, lista(i).IdDocumento, lista(i).Monto, lista(i).IdMovCuenta, cn, tr)
            Next

            '******************** VALIDAMOS LA ACCION A TOMAR ( SALDO RESTANTE )
            objDaoDocumentoNotaCredito.DocumentoNotaCreditoAplicacion_ValSaldoRestante(IdDocumentoNotaCredito, opcionSaldoRestante, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function


    Public Function DocumentoNotaCreditoSelectCabFind_Aplicacion(ByVal IdPersona As Integer, ByVal IdTienda As Integer, ByVal Filtro As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDocumentoNotaCredito.DocumentoNotaCreditoSelectCabFind_Aplicacion(IdPersona, IdTienda, Filtro)
    End Function

    Public Function DocumentoNotaCreditoSelectCab(ByVal IdSerie As Integer, ByVal Codigo As Integer, Optional ByVal IdDocumento As Integer = 0) As Entidades.DocumentoNotaCredito
        Return objDaoDocumentoNotaCredito.DocumentoNotaCreditoSelectCab(IdSerie, Codigo, IdDocumento)
    End Function

    Public Function AnularDocumentoNotaCredito(ByVal IdDocumento As Integer) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '************************* Deshacemos los MOv
            objDaoDocumentoNotaCredito.DocumentoNotaCreditoDeshacerMov(IdDocumento, False, False, False, False, True, cn, tr)

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return IdDocumento

    End Function


    Public Function ActualizarDocumentoNotaCredito(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal objObservacion As Entidades.Observacion, ByVal ListaDetalleConcepto As List(Of Entidades.DetalleConcepto)) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '************************* Deshacemos los MOv
            objDaoDocumentoNotaCredito.DocumentoNotaCreditoDeshacerMov(objDocumento.Id, True, True, True, True, False, cn, tr)

            '********************** VALIDAMOS EL DOCUMENTO REF
            objDaoDocumentoNotaCredito.DocumentoNotaCredito_ValidarDocumentoNC_Rel(objRelacionDocumento.IdDocumento1, objDocumento.IdMoneda, objDocumento.TotalAPagar, cn, tr, objDocumento.IdTipoOperacion)

            '*************** Actualizamos la cabecera
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            ''************* Insertamos el detalle
            'Dim objDaoDetalleDoc As New DAO.DAODetalleDocumento
            'objDaoDetalleDoc.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalle, tr, objDocumento.Id, False, False, 0, 0)

            objDaoDocumentoNotaCredito.Registrar_DetalleDocumento(listaDetalle, objDocumento.Id, cn, tr)

            '************* Insertamos Relacion Documento
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            objRelacionDocumento.IdDocumento2 = objDocumento.Id
            objDaoRelacionDocumento.InsertaRelacionDocumento(objRelacionDocumento, cn, tr)

            '************* Actualizamos estado de doc relacionado solo si es anticipo
            Dim objDaoDocAnticipo As New DAO.DAODocAnticipo
            objDaoDocAnticipo.UpdateEstado(objRelacionDocumento.IdDocumento1, cn, tr)

            '************** Insertamos Observacion
            If objObservacion IsNot Nothing Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '************** Insertamos Detalle Concepto
            If (ListaDetalleConcepto IsNot Nothing) Then
                Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto

                For i As Integer = 0 To ListaDetalleConcepto.Count - 1
                    ListaDetalleConcepto(i).IdDocumento = objDocumento.Id
                    objDaoDetalleConcepto.DetalleConceptoInsert(ListaDetalleConcepto(i), cn, tr)
                Next

            End If
            Dim id As Integer = 0
            Dim obj As New DAO.DAODocumento
            id = objDocumento.Id
            tr.Commit()

            obj.crearFacturaElectronica(id, cn)
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function



    Public Function RegistrarDocumentoNotaCredito(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal objObservacion As Entidades.Observacion, ByVal ListaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objRelDocGuia As Entidades.RelacionDocumento) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim obj As New DAO.DAODocumento
        Try

            cn.Open()
            tr = cn.BeginTransaction

            '********************** VALIDAMOS EL DOCUMENTO REF
            '  
            'If (ListaDetalleConcepto.Count > 0) Then
            '    If (objDocumento.IdTipoOperacion = 25 And ListaDetalleConcepto.Item(0).IdConcepto = 65) Then
            '        obj.CambiarEstadoAnticipo(objRelacionDocumento.IdDocumento1, cn, tr)
            '    End If
            'End If
            objDaoDocumentoNotaCredito.DocumentoNotaCredito_ValidarDocumentoNC_Rel(objRelacionDocumento.IdDocumento1, objDocumento.IdMoneda, objDocumento.TotalAPagar, cn, tr, objDocumento.IdTipoOperacion)

            '*************** Insertamos la cabecera

            Dim objDaoDocumento As New DAO.DAODocumento
            objDocumento.Id = objDaoDocumento.InsertaDocumento(cn, objDocumento, tr)


            '************* Insertamos el detalle
            'Dim objDaoDetalleDoc As New DAO.DAODetalleDocumento
            'objDaoDetalleDoc.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalle, tr, objDocumento.Id, False, False, 0, 0)

            objDaoDocumentoNotaCredito.Registrar_DetalleDocumento(listaDetalle, objDocumento.Id, cn, tr)

            '************* Insertamos Relacion Documento
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            objRelacionDocumento.IdDocumento2 = objDocumento.Id
            objDaoRelacionDocumento.InsertaRelacionDocumento(objRelacionDocumento, cn, tr)

            '************* Actualizamos estado de doc relacionado solo si es anticipo
            Dim objDaoDocAnticipo As New DAO.DAODocAnticipo
            objDaoDocAnticipo.UpdateEstado(objRelacionDocumento.IdDocumento1, cn, tr)

            '************* Insertamos Relacion Documento GUIA
            If Not objRelDocGuia Is Nothing Then
                objRelDocGuia.IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(objRelDocGuia, cn, tr)
            End If

            '************** Insertamos Observacion
            If objObservacion IsNot Nothing Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '************** Insertamos Detalle Concepto
            If (ListaDetalleConcepto IsNot Nothing) Then
                Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto

                For i As Integer = 0 To ListaDetalleConcepto.Count - 1
                    ListaDetalleConcepto(i).IdDocumento = objDocumento.Id
                    objDaoDetalleConcepto.DetalleConceptoInsert(ListaDetalleConcepto(i), cn, tr)
                Next

            End If
            Dim id As Integer = 0

            id = objDocumento.Id
            tr.Commit()

            obj.crearFacturaElectronica(id, cn)
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function


    Public Function SelectDocReferenciaxParams(ByVal IdCliente As Integer, ByVal FechaI As Date, ByVal FechaF As Date, _
                                               ByVal IdTienda As Integer, ByVal serie As Integer, ByVal codigo As Integer, _
                                               ByVal opcion As Integer, ByVal ValDocRelacionado As Boolean, _
                                               ByVal idTipoDocumento As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDocumentoNotaCredito.SelectDocReferenciaxParams(IdCliente, FechaI, FechaF, IdTienda, serie, codigo, opcion, _
                                                                     ValDocRelacionado, idTipoDocumento)
    End Function



End Class



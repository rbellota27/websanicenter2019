﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DocumentoExterno

    Private obj As New DAO.DAODocumentoExterno
    Private cn As SqlConnection
    Private tr As SqlTransaction

    '**************************** VERSION 2

    Public Function registrarDocumento(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCuentaCxP As Entidades.MovCuentaPorPagar, ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal listaMontoRegimen As List(Of Entidades.MontoRegimen), ByVal listaMovCuentaCXP_Update As List(Of Entidades.MovCuentaPorPagar), ByVal ListaGastoCentroCosto As List(Of Entidades.GastoCentroCosto)) As Integer

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '************************* INSERTAMOS DOCUMENTO
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '************************* INSERTAMOS ANEXO DOCUMENTO
            If (objAnexoDocumento IsNot Nothing) Then
                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = objDocumento.Id
                objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, False)
            End If


          
            '********************** INSERTAMOS LISTA RELACION DOCUMENTO

            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '**********************  INSERTAMOS LISTA DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1
                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            Next

            '**********************  MOV CUENTA X PAGAR
            Dim objDaoMovCuentaCxP As New DAO.DAOMovCuentaPorPagar
            If (objMovCuentaCxP IsNot Nothing) Then

                objMovCuentaCxP.IdDocumento = objDocumento.Id
                objDaoMovCuentaCxP.Registrar(objMovCuentaCxP, cn, tr)

            End If


            If (listaMovCuentaCXP_Update.Count > 0) Then
                Dim dao As DAO.DAODocCancelacion = New DAO.DAODocCancelacion
                dao.UpdateDocumentoSustentoMovCuenta(listaMovCuentaCXP_Update, objDocumento.Id, cn, tr)
            End If




            '******************* ACTUALIZAMOS LA LISTA DE DOCUMENTOS DE REFERENCIA
            For i As Integer = 0 To listaMovCuentaCXP_Update.Count - 1
                objDaoMovCuentaCxP.UpdateSaldoxIdMovCuenta(listaMovCuentaCXP_Update(i).Id, listaMovCuentaCXP_Update(i).Saldo, cn, tr)
            Next

            '******************* ACTUALIZAMOS LA LISTA DE CENTRO DE COSTO
            Dim objDaoGastoCentroCosto As New DAO.DAOGastoCentroCosto
            For i As Integer = 0 To ListaGastoCentroCosto.Count - 1
                ListaGastoCentroCosto(i).IdDocumento = objDocumento.Id
                objDaoGastoCentroCosto.Insert(ListaGastoCentroCosto(i), cn, tr)
            Next

            '******************** OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then

                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            '*************************** INSERTAMOS MONTO REGIMEN
            Dim objDaoMontoRegimen As New DAO.DAOMontoRegimen
            For i As Integer = 0 To listaMontoRegimen.Count - 1

                listaMontoRegimen(i).IdDocumento = objDocumento.Id
                objDaoMontoRegimen.InsertaMontoRegimen(cn, listaMontoRegimen(i), tr)

            Next

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento.Id

    End Function
    Public Function actualizarDocumento(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCuentaCxP As Entidades.MovCuentaPorPagar, ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal listaMontoRegimen As List(Of Entidades.MontoRegimen), ByVal listaMovCuentaCXP_Update As List(Of Entidades.MovCuentaPorPagar), ByVal ListaGastoCentroCosto As List(Of Entidades.GastoCentroCosto)) As Integer

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '********************************** DESHACER MOV
            obj.DocumentoExterno_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, False, cn, tr)

            '************************* ACTUALIZAMOS DOCUMENTO
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************************* INSERTAMOS ANEXO DOCUMENTO
            If (objAnexoDocumento IsNot Nothing) Then
                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = objDocumento.Id
                objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, False)
            End If

            '********************** INSERTAMOS LISTA RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1

                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            '**********************  INSERTAMOS LISTA DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1
                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            Next

            '**********************  MOV CUENTA X PAGAR
            Dim objDaoMovCuentaCxP As New DAO.DAOMovCuentaPorPagar
            If (objMovCuentaCxP IsNot Nothing) Then

                objMovCuentaCxP.IdDocumento = objDocumento.Id
                objDaoMovCuentaCxP.InsertT_GetID(objMovCuentaCxP, cn, tr)

            End If

            If (listaMovCuentaCXP_Update.Count > 0) Then
                Dim dao As DAO.DAODocCancelacion = New DAO.DAODocCancelacion
                dao.UpdateDocumentoSustentoMovCuenta(listaMovCuentaCXP_Update, objDocumento.Id, cn, tr)
            End If

            '******************* ACTUALIZAMOS LA LISTA DE DOCUMENTOS DE REFERENCIA
            For i As Integer = 0 To listaMovCuentaCXP_Update.Count - 1
                objDaoMovCuentaCxP.UpdateSaldoxIdMovCuenta(listaMovCuentaCXP_Update(i).Id, listaMovCuentaCXP_Update(i).Saldo, cn, tr)
            Next


            '******************* ACTUALIZAMOS LA LISTA DE CENTRO DE COSTO
            Dim objDaoGastoCentroCosto As New DAO.DAOGastoCentroCosto
            objDaoGastoCentroCosto.DeletexIdDocumento(objDocumento.Id, cn, tr)
            For i As Integer = 0 To ListaGastoCentroCosto.Count - 1
                ListaGastoCentroCosto(i).IdDocumento = objDocumento.Id
                objDaoGastoCentroCosto.Insert(ListaGastoCentroCosto(i), cn, tr)
            Next

            '******************** OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then

                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            '*************************** INSERTAMOS MONTO REGIMEN
            Dim objDaoMontoRegimen As New DAO.DAOMontoRegimen
            For i As Integer = 0 To listaMontoRegimen.Count - 1

                listaMontoRegimen(i).IdDocumento = objDocumento.Id
                objDaoMontoRegimen.InsertaMontoRegimen(cn, listaMontoRegimen(i), tr)

            Next

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento.Id

    End Function
    Public Function anularDocumento(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction


            '**************** DESHACER MOV
            obj.DocumentoExterno_DeshacerMov(IdDocumento, False, False, False, False, True, False, True, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function











#Region "Reportes"

    Public Function getDataSet_Cuentas_Por_Pagar_Cargos(ByVal IdEmpresa As Integer, _
    ByVal IdMoneda As Integer, ByVal IdPersona As Integer, ByVal idTienda As Integer, ByVal fecha As String) As DataSet
        Return obj.getDataSet_Cuentas_Por_Pagar_Cargos(IdEmpresa, IdMoneda, IdPersona, idTienda, fecha)
    End Function

#End Region

#Region "Manteminiento"

    Public Function Documento_Externo_Insert(ByVal objDocumento As Entidades.Documento, _
                                             ByVal lista_detalle As List(Of Entidades.DetalleDocumento), _
                                             ByVal objObservacion As Entidades.Observacion) As Integer
        Return obj.Documento_Externo_Insert(objDocumento, lista_detalle, objObservacion)
    End Function

    Public Function Documento_Externo_Update(ByVal objDocumento As Entidades.Documento, _
                                           ByVal lista_detalle As List(Of Entidades.DetalleDocumento), _
                                           ByVal objObservacion As Entidades.Observacion) As Boolean
        Return obj.Documento_Externo_Update(objDocumento, lista_detalle, objObservacion)
    End Function

    Public Function DocumentoExterno_Busqueda(ByVal fechas As String, ByVal idpersona As Integer, ByVal idtipodocumento As Integer) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoExterno_Busqueda(fechas, idpersona, idtipodocumento)
    End Function

    Public Function DocumentoExterno_SerieNumero(ByVal serie As Integer, ByVal numero As Integer, _
                                                 ByVal idtipodocumento As Integer, ByVal idpersona As Integer) As Entidades.DocumentoView
        Return obj.DocumentoExterno_SerieNumero(serie, numero, idtipodocumento, idpersona)
    End Function

    Public Function DocumentoExterno_RelacionDoc(ByVal idDocumento As Integer) As Entidades.DocumentoView
        Return obj.DocumentoExterno_RelacionDoc(idDocumento)
    End Function

#End Region

#Region "Otros"
    Public Function DocumentoExterno_ConsultarGuia(ByVal cadIdDocumento As String, _
                                                    ByVal idProducto As Integer) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoExterno_ConsultarGuia(cadIdDocumento, idProducto)
    End Function

    Public Function DocumentoExterno_BuscarReferencia(ByVal tipo As Integer, _
                                         ByVal fechas As String, ByVal doc_Codigo As Integer, _
                                         ByVal doc_Serie As Integer, ByVal idpersona As Integer, _
                                         ByVal idtipodocumento As Integer) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoExterno_BuscarReferencia(tipo, fechas, doc_Codigo, doc_Serie, idpersona, idtipodocumento)
    End Function

    Public Function DocumentoExterno_Buscar(ByVal IdDocumento As Integer) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoExterno_Buscar(IdDocumento)
    End Function
#End Region

End Class

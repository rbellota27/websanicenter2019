﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'************************   JUEVES 15 ABRIL 01_09 PM







Public Class ProgramacionPedido

    Dim obj As New DAO.DAOProgramacionPedido

    Public Function ProgramacionPedidoSelectxIdYearxIdSemana(ByVal IdYear As Integer, ByVal IdSemana As Integer) As Entidades.ProgramacionPedido
        Return obj.ProgramacionPedidoSelectxIdYearxIdSemana(IdYear, IdSemana)
    End Function

    Public Function ProgramacionPedidoSelectxFecha(ByVal fecha As Date) As Entidades.ProgramacionPedido
        Return obj.ProgramacionPedidoSelectxFecha(fecha)
    End Function

    Public Function ProgramacionPedidoSelectSemanaxYear(ByVal IdYear As Integer) As List(Of Entidades.ProgramacionPedido)
        Return obj.ProgramacionPedidoSelectSemanaxYear(IdYear)
    End Function

    Public Function ProgramacionPedidoSelectYear() As List(Of Entidades.ProgramacionPedido)
        Return obj.ProgramacionPedidoSelectYear()
    End Function

    Public Function ListarProgramacionPedido(ByVal idyear As Integer) As String
        Return obj.ListarProgramacionPedido(idyear)
    End Function

    Public Function InsertProgramacionPedido(ByVal objE As Entidades.ProgramacionPedido) As Boolean
        Return obj.InsertProgramacionPedido(objE)
    End Function

    Public Function UpdateProgramacionPedido(ByVal objE As Entidades.ProgramacionPedido) As Boolean
        Return obj.UpdateProgramacionPedido(objE)
    End Function

    Public Function ListarIdSemanaxIdYear(ByVal idyear As Integer) As String
        Return obj.ListarIdSemanaxIdYear(idyear)
    End Function

    Public Function ListarProgramacionMantenimiento(ByVal idyear As Integer, ByVal idsemana As Integer) As Entidades.ProgramacionPedido
        Return obj.ListarProgramacionMantenimiento(idyear, idsemana)
    End Function

    Public Function ListarProgPedido_A_Documento(ByVal idyear As Integer, _
                    ByVal idTienda As Integer, ByVal idAlmacen As Integer) As String
        Return obj.ListarProgPedido_A_Documento(idyear, idTienda, idAlmacen)
    End Function

    Public Function ListarDocProgPedidoxIdDocumento(ByVal idDocumento As Integer) As Entidades.ProgramacionPedido
        Return obj.ListarDocProgPedidoxIdDocumento(idDocumento)
    End Function

    Public Function GetYear() As List(Of Entidades.ProgramacionPedido)
        Return obj.GetYear()
    End Function

    Public Function GetSemanaDYear() As List(Of Entidades.ProgramacionPedido)
        Return obj.GetSemanaDYear
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'*********************  JUEVES 08 ABRIL 2010 HORA 02_46 PM


Imports System.Data.SqlClient
Public Class DocGuiaRecepcion
    Private objDaoDocGuiaRecepcion As New DAO.DAODocGuiaRecepcion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private tr As SqlTransaction
    'Public Function ValidandoCantidadTransitoDocReferencia(ByVal IdDocumento As Integer) As Decimal
    '    Return objDaoDocGuiaRecepcion.ValidandoCantidadTransitoDocReferencia(IdProducto)
    'End Function
    Public Function DocumentoGuiaRecepcionSelectCostoProducto(ByVal IdProducto As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal Fecha As Date, ByVal IdDocumentoRef As Integer) As Decimal
        Return objDaoDocGuiaRecepcion.DocumentoGuiaRecepcionSelectCostoProducto(IdProducto, IdEmpresa, IdAlmacen, Fecha, IdDocumentoRef)
    End Function

    Public Function DocumentoGuiaRecepcion_BuscarDocumentoRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, _
                                                              ByVal FechaInicio As Date, _
                                                              ByVal FechaFin As Date, ByVal TipoDocumentoRef As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDocGuiaRecepcion.DocumentoGuiaRecepcion_BuscarDocumentoRef(IdEmpresa, IdTienda, IdAlmacen, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, TipoDocumentoRef, IdTipoOperacion)
    End Function
    Public Function DocumentoGuiaRecepcion_BuscarDocumentoRef2(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As String, _
                                                           ByVal FechaInicio As Date, _
                                                           ByVal FechaFin As Date, ByVal TipoDocumentoRef As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDocGuiaRecepcion.DocumentoGuiaRecepcion_BuscarDocumentoRef2(IdEmpresa, IdTienda, IdAlmacen, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, TipoDocumentoRef, IdTipoOperacion)
    End Function
    Public Function DocumentoGuiaRecepcionSelectCab(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer) As Entidades.DocGuiaRecepcion
        Return objDaoDocGuiaRecepcion.DocumentoGuiaRecepcionSelectCab(IdSerie, Codigo, IdDocumento)
    End Function

    Public Function SelectxIdDocumentoGuiarecepcion(ByVal IdDocumento As Integer) As Entidades.Documento
        Return objDaoDocGuiaRecepcion.SelectxIdDocumentoGuiarecepcion(IdDocumento)
    End Function

    Public Function registrarGuiaRecepcion(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal Factor As Integer, ByVal MoverStockFisico As Boolean) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction

            '************* CABECERA
            'objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)
            objDocumento.Id = (New DAO.DAODocGuiaRecepcion).InsertaDocumentoGuiaRecepcion(cn, objDocumento, tr)
            '************* DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, objDocumento.Id, MoverStockFisico, False, Factor, objDocumento.IdAlmacen)

            '************ PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '************ OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '***************** VERIFICAMOS LA ORDEN DE COMPRA
            objDaoDocGuiaRecepcion.DocumentoGuiaRecepcion_VerificarRegistro(objDocumento.Id, 1, cn, tr)

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function registrarSolicitudPago(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction
            '************* CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)
            objDocumento.IdDocumento = objDocumento.Id

            '************ DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1
                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            Next

            '************ OBSERVACIONES
            If (objObservaciones.Observacion IsNot Nothing Or objObservaciones.Id <> 0) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If
            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    Public Function anularSolicitudPago(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        cn = (New DAO.Conexion).ConexionSIGE
        Try

            '**** MoverStockFisico: MUEVE EL STOCK DE ALMACÉN

            cn.Open()
            tr = cn.BeginTransaction

            '******************** DESHACEMOS LOS MOV
            objDaoDocGuiaRecepcion.DocumentoSolicitudPago_DeshacerMov(IdDocumento, False, False, False, True, cn, tr)
            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function actualizarSolicitudPago(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try

            cn.Open()
            tr = cn.BeginTransaction

            '******************** DESHACEMOS LOS MOV
            objDaoDocGuiaRecepcion.DocumentoSolicitudPago_DeshacerMov(objDocumento.Id, True, True, True, False, cn, tr)

            '************* CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************ OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '**********************  INSERTAMOS LISTA DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1
                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function actualizarGuiaRecepcion(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal Factor As Integer, ByVal MoverStockFisico As Boolean) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try

            '**** MoverStockFisico: MUEVE EL STOCK DE ALMACÉN

            cn.Open()
            tr = cn.BeginTransaction

            '******************** DESHACEMOS LOS MOV
            objDaoDocGuiaRecepcion.DocumentoGuiaRecepcion_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, False, cn, tr)

            '************* CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************* DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, objDocumento.Id, MoverStockFisico, False, Factor, objDocumento.IdAlmacen)

            '************ PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '************ OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '***************** VERIFICAMOS LA ORDEN DE COMPRA/ STOCK TRANSITO
            objDaoDocGuiaRecepcion.DocumentoGuiaRecepcion_VerificarRegistro(objDocumento.Id, 1, cn, tr)

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function anularGuiaRecepcion(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        cn = (New DAO.Conexion).ConexionSIGE
        Try

            '**** MoverStockFisico: MUEVE EL STOCK DE ALMACÉN

            cn.Open()
            tr = cn.BeginTransaction

            '******************** DESHACEMOS LOS MOV
            objDaoDocGuiaRecepcion.DocumentoGuiaRecepcion_DeshacerMov(IdDocumento, False, True, True, True, False, False, False, True, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

End Class

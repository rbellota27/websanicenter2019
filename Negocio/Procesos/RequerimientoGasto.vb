﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************** LUNES 17 05 2010 HORA 06 00 PM


Imports System.Data.SqlClient

Public Class RequerimientoGasto

    Private obj As New DAO.DAORequerimientoGasto
    Private cn As SqlConnection
    Private tr As SqlTransaction

    '**************************** VERSION 2
    Public Function fx_getTotalSustentadoxIdDocumentoReqGasto(ByVal IdDocumento_RG As Integer) As Decimal
        Return obj.fx_getTotalSustentadoxIdDocumentoReqGasto(IdDocumento_RG)
    End Function
    Public Function registrarDocumento(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCuentaCxP As Entidades.MovCuentaPorPagar, ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer
        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '************************* INSERTAMOS DOCUMENTO
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '************************* INSERTAMOS ANEXO DOCUMENTO
            If (objAnexoDocumento IsNot Nothing) Then
                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = objDocumento.Id
                objDaoAnexoDocumento.InsertAnexo_Documento2(objAnexoDocumento, cn, tr, False)
            End If
            '************************ RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '**********************  INSERTAMOS LISTA DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1
                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            Next

            '**********************  MOV CUENTA X PAGAR
            If (objMovCuentaCxP IsNot Nothing) Then

                Dim objDaoMovCuentaCxP As New DAO.DAOMovCuentaPorPagar
                objMovCuentaCxP.IdDocumento = objDocumento.Id
                objDaoMovCuentaCxP.InsertT_GetID(objMovCuentaCxP, cn, tr)

            End If

            '******************** OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then

                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento.Id

    End Function
    Public Function actualizarDocumento(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCuentaCxP As Entidades.MovCuentaPorPagar, ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction


            '**************** DESHACER MOV
            obj.DeshacerDocumentoRequerimientoGasto(objDocumento.Id, True, True, True, True, False, True, cn, tr)

            '************************* INSERTAMOS DOCUMENTO
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************************* INSERTAMOS ANEXO DOCUMENTO
            If (objAnexoDocumento IsNot Nothing) Then
                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = objDocumento.Id
                objDaoAnexoDocumento.InsertAnexo_Documento2(objAnexoDocumento, cn, tr, False)
            End If

            '**********************  INSERTAMOS LISTA DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1
                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            Next

            '**********************  MOV CUENTA X PAGAR
            If (objMovCuentaCxP IsNot Nothing) Then

                Dim objDaoMovCuentaCxP As New DAO.DAOMovCuentaPorPagar
                objMovCuentaCxP.IdDocumento = objDocumento.Id
                objDaoMovCuentaCxP.InsertT_GetID(objMovCuentaCxP, cn, tr)

            End If
            '************************ RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next
            '******************** OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then

                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento.Id

    End Function
    Public Function anularDocumento(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction


            '**************** DESHACER MOV
            obj.DeshacerDocumentoRequerimientoGasto(IdDocumento, False, False, True, False, True, False, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function actualizarSustento_CentroCosto_Documento(ByVal IdDocumento As Integer, ByVal IdDetalleConcepto As Integer, ByVal listaDetalleConcepto_Anexo As List(Of Entidades.DetalleConcepto_Anexo), ByVal listaGastoCentroCosto As List(Of Entidades.GastoCentroCosto), ByVal objObservaciones As Entidades.Observacion) As Boolean

        Dim hecho As Boolean = False
        cn = (New DAO.Conexion).ConexionSIGE

        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim objDaoDCA As New DAO.DAODetalleConcepto_Anexo

            '************* ELIMINAMOS EL SUSTENTO
            objDaoDCA.DetalleDocumento_Anexo_DeletexIdDetalleConcepto(IdDetalleConcepto, cn, tr)

            '******************* INSERTAMOS
            For i As Integer = 0 To listaDetalleConcepto_Anexo.Count - 1

                objDaoDCA.DetalleConcepto_Anexo_Insert(listaDetalleConcepto_Anexo(i), cn, tr)

            Next

            '**************** ELIMINAMOS EL CENTRO COSTO
            Dim objDaoGastoCentroCosto As New DAO.DAOGastoCentroCosto
            objDaoGastoCentroCosto.DeletexIdDocumento(IdDocumento, cn, tr)
            If Not listaGastoCentroCosto Is Nothing Then
                For i As Integer = 0 To listaGastoCentroCosto.Count - 1
                    listaGastoCentroCosto(i).IdDocumento = IdDocumento
                    objDaoGastoCentroCosto.Insert(listaGastoCentroCosto(i), cn, tr)
                Next
            End If
            '***************** OBSERVACIONES
            Dim objDaoObservaciones As New DAO.DAOObservacion
            If (objObservaciones IsNot Nothing) Then

                objObservaciones.IdDocumento = IdDocumento
                objDaoObservaciones.Insert_Update(objObservaciones, cn, tr)

            Else

                objDaoObservaciones.DeletexIdDocumento(IdDocumento, cn, tr)

            End If


            obj.DocumentoRequerimientoGasto_UpdateMontoxSustentar(IdDocumento, cn, tr)

            obj.ValSustento_RelacionDocumento(IdDocumento, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho

    End Function
    Public Function DocumentoRequerimientoGasto_SelectAprobacion(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal MontoTot As Decimal) As DataTable
        Return obj.DocumentoRequerimientoGasto_SelectAprobacion(IdEmpresa, IdTienda, IdArea, Opcion_Aprobacion, FechaInicio, FechaFin, MontoTot)
    End Function

    Public Function aprobarDocumento(ByVal IdDocumento As Integer, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal objMovCuentaCXP As Entidades.MovCuentaPorPagar) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            tr = cn.BeginTransaction


            '****************  DESHACER MOV
            obj.DeshacerDocumentoRequerimientoGasto(IdDocumento, True, False, True, False, False, False, cn, tr)

            '****************  REGISTRAMOS LOS DATOS
            If (objAnexoDocumento IsNot Nothing) Then

                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = IdDocumento
                objDaoAnexoDocumento.InsertAnexo_Documento2(objAnexoDocumento, cn, tr, False)

            End If

            If (objMovCuentaCXP IsNot Nothing) Then

                Dim objDaoMovCuentaCXP As New DAO.DAOMovCuentaPorPagar
                objMovCuentaCXP.IdDocumento = IdDocumento
                objDaoMovCuentaCXP.InsertT(objMovCuentaCXP, cn, tr)

            End If


            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function InsertRequerimientoGasto(ByVal ObjEnt As Entidades.RequerimientoGasto.CabeceraRequerimiento, _
                                         ByVal Lista As List(Of Entidades.RequerimientoGasto), ByVal objObs As Entidades.Observacion) As String
        Return obj.InsertRequerimientoGasto(ObjEnt, Lista, objObs)
    End Function

    Public Function UpdateRequerimientoGasto(ByVal ObjEnt As Entidades.RequerimientoGasto.CabeceraRequerimiento, _
                                             ByVal Lista As List(Of Entidades.RequerimientoGasto), ByVal objObs As Entidades.Observacion) As String
        Return obj.UpdateRequerimientoGasto(ObjEnt, Lista, objObs)
    End Function

    Public Function selectRequerimientoGasto(ByVal idserie As Integer, ByVal codigo As Integer) As Entidades.RequerimientoGasto.CabeceraRequerimiento
        Return obj.selectRequerimientoGasto(idserie, codigo)
    End Function

    Public Function selectListaRequerimientoGasto(ByVal IdDocumento As Integer) As List(Of Entidades.RequerimientoGasto)
        Return obj.selectListaRequerimientoGasto(IdDocumento)
    End Function

    Public Function RequerimientoGasto_Select_Aprobar(ByVal idempresa As Integer, _
                                                     ByVal Idtienda As Integer, ByVal idarea As Integer, _
                                                     ByVal autorizadas As Boolean) As List(Of Entidades.RequerimientoGasto.CabeceraRequerimiento)
        Return obj.RequerimientoGasto_Select_Aprobar(idempresa, Idtienda, idarea, autorizadas)
    End Function
    ''AGREGADO PARA EL REPORTE DE REQUERIMIENTO DE GASTO IMPRESIÓN
    Public Function getDataSet_DocRequeGastoV2(ByVal IdDocumento As Integer) As DataSet

        Return obj.getDataSet_DocRequeGastoV2(IdDocumento)

    End Function
    Public Function getDataSet_DocRequeGastoV2Impre(ByVal IdDocumento As Integer) As DataSet

        Return obj.getDataSet_DocRequeGastoV2(IdDocumento)

    End Function

    Public Function selectxidval_req_aprob(ByVal objx As Entidades.Anexo_Documento) As Entidades.Anexo_Documento
        Dim Dao As DAO.DAODocumento = New DAO.DAODocumento
        Return Dao.selectxidval_req_aprob(objx)
    End Function
End Class

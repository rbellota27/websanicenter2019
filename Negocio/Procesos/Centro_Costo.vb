﻿Imports System.Data.SqlClient

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'viernes 12 de marzo
Public Class Centro_Costo

    Dim ObjDAO As New DAO.DAOCentro_Costo
    Private cn As SqlConnection
    Private tr As SqlTransaction
#Region "Marzo del 2010"
    Public Function CentroCosto_idTienda_IdUsuario_IdArea(ByVal idtienda As Integer, _
                                                          ByVal idusuario As Integer, ByVal idarea As Integer) As String
        Return ObjDAO.CentroCosto_idTienda_IdUsuario_IdArea(idtienda, idusuario, idarea)
    End Function

    Public Function GastoPorCentroCosto_Select_IdDocumento(ByVal idDocumento As Integer) As List(Of Entidades.Centro_costo)
        Return ObjDAO.GastoPorCentroCosto_Select_IdDocumento(idDocumento)
    End Function

#End Region

    Public Function ActualizarCentroCostos(ByVal ListaGastoCentroCosto As List(Of Entidades.GastoCentroCosto), ByVal IdDocumento As Integer) As Boolean
        Try
            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction
            Dim objDaoGastoCentroCosto As New DAO.DAOGastoCentroCosto
            objDaoGastoCentroCosto.EliminarxIdDocumento(IdDocumento)

            For i As Integer = 0 To ListaGastoCentroCosto.Count - 1
                ListaGastoCentroCosto(i).IdDocumento = IdDocumento
                objDaoGastoCentroCosto.Insert(ListaGastoCentroCosto(i), cn, tr)
            Next
            tr.Commit()
        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

    End Function
    Public Function EliminarxIdDocumento(ByVal IdDocumento As Integer) As Boolean
        Dim objDaoGastoCentroCosto As New DAO.DAOGastoCentroCosto
        Return objDaoGastoCentroCosto.EliminarxIdDocumento(IdDocumento)
    End Function
    Public Function BuscarAutogeneradoXCodSubArea3(ByVal CodUnidadNegocio As String, ByVal CodsubCodigo As String, ByVal CodSubArea1 As String, ByVal CodSubArea2 As String) As String
        Return ObjDAO.BuscarAutogeneradoXCodSubArea3(CodUnidadNegocio, CodsubCodigo, CodSubArea1, CodSubArea2)
    End Function
    Public Function BuscarAutogeneradoXCodSubArea2(ByVal CodUnidadNegocio As String, ByVal CodsubCodigo As String, ByVal CodSubArea1 As String) As String
        Return ObjDAO.BuscarAutogeneradoXCodSubArea2(CodUnidadNegocio, CodsubCodigo, CodSubArea1)
    End Function
    Public Function BuscarAutogeneradoXCodSubArea1(ByVal CodUnidadNegocio As String, ByVal CodsubCodigo As String) As String
        Return ObjDAO.BuscarAutogeneradoXCodSubArea1(CodUnidadNegocio, CodsubCodigo)
    End Function
    Public Function BuscarAutogeneradoXCodSubcodigo(ByVal CodUnidadNegocio As String) As String
        Return ObjDAO.BuscarAutogeneradoXCodSubcodigo(CodUnidadNegocio)
    End Function
    Public Function BuscarAutogeneradoXCodUniNegocio(ByVal CodUnidadNegocio As String) As String
        Return ObjDAO.BuscarAutogeneradoXCodUniNegocio(CodUnidadNegocio)
    End Function
    Public Function Insert_Centro_Costo(ByVal obj As Entidades.Centro_costo) As Boolean
        Return ObjDAO.Insert_Centro_Costo(obj)
    End Function
    Public Function Update_Centro_Costo(ByVal obj As Entidades.Centro_costo) As Boolean
        Return ObjDAO.Update_Centro_Costo(obj)
    End Function
    Public Function Listar_centro_costo(ByVal obj As Entidades.Centro_costo) As List(Of Entidades.Centro_costo)
        Return ObjDAO.Listar_centro_costo(obj)
    End Function

    Public Function Listar_Cod_UniNeg_Centro_Costo(ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Return ObjDAO.Listar_Cod_UniNeg_Centro_Costo(estado)
    End Function

    Public Function Listar_Cod_DepFunc_Centro_costo(ByVal Cod_UniNeg As String, ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Return ObjDAO.Listar_Cod_DepFunc_Centro_costo(Cod_UniNeg, estado)
    End Function

    Public Function Listar_Cod_SubCodigo2_Centro_costo(ByVal Cod_UniNeg As String, _
                                                       ByVal Cod_DepFunc As String, _
                                                       ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Return ObjDAO.Listar_Cod_SubCodigo2_Centro_costo(Cod_UniNeg, Cod_DepFunc, estado)
    End Function

    Public Function Listar_Cod_SubCodigo3_Centro_costo(ByVal Cod_UniNeg As String, _
                                                      ByVal Cod_DepFunc As String, _
                                                      ByVal Cod_SubCodigo2 As String, _
                                                      ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Return ObjDAO.Listar_Cod_SubCodigo3_Centro_costo(Cod_UniNeg, Cod_DepFunc, Cod_SubCodigo2, estado)
    End Function

    Public Function Listar_Cod_SubCodigo4_Centro_costo(ByVal Cod_UniNeg As String, _
                                                      ByVal Cod_DepFunc As String, _
                                                      ByVal Cod_SubCodigo2 As String, _
                                                      ByVal Cod_SubCodigo3 As String, _
                                                        ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Return ObjDAO.Listar_Cod_SubCodigo4_Centro_costo(Cod_UniNeg, Cod_DepFunc, Cod_SubCodigo2, Cod_SubCodigo3, estado)
    End Function

End Class

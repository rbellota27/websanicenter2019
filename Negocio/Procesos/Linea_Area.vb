﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Linea_Area

    Private obj As New DAO.DAOLinea_Area

    Public Function ListarLineaxArea(ByVal idArea As Integer) As List(Of Entidades.Linea)
        Return obj.ListarLineaxArea(idArea)
    End Function

    Public Function ListarTipoexistenciaxArea(ByVal idArea As Integer) As List(Of Entidades.TipoExistencia)
        Return obj.ListarTipoexistenciaxArea(idArea)
    End Function
    Public Function ListarLineaxTipoexistenciaxArea(ByVal idArea As Integer, ByVal idTipoExistencia As Integer) As List(Of Entidades.Linea)
        Return obj.ListarLineaxTipoexistenciaxArea(idArea, idTipoExistencia)
    End Function

    Public Function SelectLinea_Area(ByVal idLinea As Integer) As List(Of Entidades.Linea_Area)
        Return obj.SelectLinea_Area(idLinea)
    End Function

    Public Function SelectLinea_Area_TipoExist(ByVal idTipoExistencia As Integer) As List(Of Entidades.Linea_Area)
        Return obj.SelectLinea_Area_TipoExist(idTipoExistencia)
    End Function

End Class

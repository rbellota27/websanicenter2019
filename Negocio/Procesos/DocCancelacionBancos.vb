﻿Public Class DocCancelacionBancos

    Protected obj As New DAO.DAODocCancelacionBancos

    Public Function Insert(ByVal objDocumento As Entidades.Documento, _
    ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), _
    ByVal listaCancelacion As List(Of Entidades.DatosCancelacion), _
    ByVal objObservaciones As Entidades.Observacion, _
    ByVal listaMovBanco As List(Of Entidades.MovBancoView)) As Integer

        Return obj.Insert(objDocumento, listaDetalleConcepto, listaCancelacion, objObservaciones, listaMovBanco)

    End Function

    Public Function Update(ByVal objDocumento As Entidades.Documento, _
    ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), _
    ByVal listaCancelacion As List(Of Entidades.DatosCancelacion), _
    ByVal objObservaciones As Entidades.Observacion, _
    ByVal listaMovBanco As List(Of Entidades.MovBancoView)) As Integer

        Return obj.Update(objDocumento, listaDetalleConcepto, listaCancelacion, objObservaciones, listaMovBanco)

    End Function



End Class

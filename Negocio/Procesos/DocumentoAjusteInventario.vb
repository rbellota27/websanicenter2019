﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'*********************   MIERCOLES  24/03/2010 HORA 05_59 PM













Imports System.Data.SqlClient
Public Class DocumentoAjusteInventario

    Private objDaoDocumentoAjusteInv As New DAO.DAODocumentoAjusteInventario
    Private objConexion As New DAO.Conexion

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand

    Public Function DocumentoAjusteInventario_SelectDocRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal IdTipoDocumento As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDocumentoAjusteInv.DocumentoAjusteInventario_SelectDocRef(IdEmpresa, IdTienda, IdAlmacen, Serie, Codigo, FechaInicio, FechaFin, IdTipoDocumento)
    End Function
    Public Function DocumentoTomaInventario_GenerarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal listaDocumento_Persona As List(Of Entidades.Documento_Persona)) As Integer

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1

                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            Dim objDaoDocumento_Persona As New DAO.DAODocumento_Persona
            For i As Integer = 0 To listaDocumento_Persona.Count - 1

                listaDocumento_Persona(i).IdDocumento = objDocumento.Id
                objDaoDocumento_Persona.Documento_PersonaInsert(listaDocumento_Persona(i), cn, tr)

            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    Public Function DocumentoAjusteInventario_FiltroDetalle(ByVal IdDocumento As Integer, ByVal IdDocumentoTomaInv As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodigoSubLinea As String, ByVal Producto As String, ByVal CodigoProducto As String, ByVal IdtipoExistencia As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DetalleDocumentoTomaInventario)
        Return objDaoDocumentoAjusteInv.DocumentoAjusteInventario_FiltroDetalle(IdDocumento, IdDocumentoTomaInv, IdLinea, IdSubLinea, CodigoSubLinea, Producto, CodigoProducto, IdtipoExistencia, PageIndex, PageSize)
    End Function
    Public Function DocumentoAjusteInventario_UpdateDocumento(ByVal objDocumento As Entidades.Documento, ByVal factorSobrante As Integer, ByVal factorFaltante As Integer, ByVal listaDetalleDocumento_Sobrante As List(Of Entidades.DetalleDocumento), ByVal listaDetalleDocumento_Faltante As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal listaDocumento_Persona As List(Of Entidades.Documento_Persona)) As Integer

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction


            '************** DESHACER MOV
            objDaoDocumentoAjusteInv.DocumentoAjusteInventario_DeshacerMov(objDocumento.Id, False, False, True, False, True, False, cn, tr)

            '********************** ACTUALIZO LA CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '****************  INSERTO DOCUMENTO PERSONA
            Dim objDaoDocumento_Persona As New DAO.DAODocumento_Persona
            For i As Integer = 0 To listaDocumento_Persona.Count - 1

                listaDocumento_Persona(i).IdDocumento = objDocumento.Id
                objDaoDocumento_Persona.Documento_PersonaInsert(listaDocumento_Persona(i), cn, tr)

            Next

            '************************ INSERTO EL DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            '************** INSERTO FALTANTE
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento_Faltante, tr, objDocumento.Id, True, False, factorFaltante, objDocumento.IdAlmacen)
            '************** INSERTO SOBRANTE
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento_Sobrante, tr, objDocumento.Id, True, False, factorSobrante, objDocumento.IdAlmacen)

            '*********************** INSERTO OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then

                Dim objDaoObservaciones As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservaciones.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento.Id

    End Function
    Public Function DocumentoAjusteInventario_Anular(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDaoDocumentoAjusteInv.DocumentoAjusteInventario_DeshacerMov(IdDocumento, True, True, False, False, False, True, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function EliminarProcesoxIdProductoxIdDocumentoAjusteInv(ByVal IdDocAjusteInv As Integer, ByVal IdProducto As Integer) As Boolean
        Return objDaoDocumentoAjusteInv.EliminarProcesoxIdProductoxIdDocumentoAjusteInv(IdDocAjusteInv, IdProducto)
    End Function

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'*******************      VIERNES   19 MARZO 2010 HORA 10_22 AM


Imports System.Data.SqlClient
Public Class DocumentoCompPercepcion

    Private objDaoDocumentoCompPercepcion As New DAO.DAODocumentoCompPercepcion
    Private objConexion As New DAO.Conexion


    Public Function DocumentoPercepcionPrint(ByVal IdDocumento As Integer) As DataSet
        Return objDaoDocumentoCompPercepcion.DocumentoPercepcionPrint(IdDocumento)
    End Function

    Public Function DocumentoCompPercepcion_GenerarxIdDocumentoRef(ByVal IdDocumentoRef As Integer, ByVal IdMonedaDestino As Integer) As Entidades.DocumentoCompPercepcion
        Return objDaoDocumentoCompPercepcion.DocumentoCompPercepcion_GenerarxIdDocumentoRef(IdDocumentoRef, IdMonedaDestino)
    End Function


    Public Function DocumentoCompPercepcionSelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.DocumentoCompPercepcion)
        Return objDaoDocumentoCompPercepcion.DocumentoCompPercepcionSelectDetalle(IdDocumento)
    End Function

    Public Function DocumentoCompPercepcionSelectCab(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer) As Entidades.DocumentoCompPercepcion
        Return objDaoDocumentoCompPercepcion.DocumentoCompPercepcionSelectCab(IdSerie, Codigo, IdDocumento)
    End Function

    Public Function DocumentoCompPercepcionSelectDocReferencia_Find(ByVal IdPersona As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.DocumentoCompPercepcion)
        Return objDaoDocumentoCompPercepcion.DocumentoCompPercepcionSelectDocReferencia_Find(IdPersona, IdMonedaDestino)
    End Function

    Public Function DocumentoCompPercepcionAnticipo(ByVal IdDocumento As Integer, ByVal IdMonedaDestino As Integer, ByVal PorcentPercepcion As Decimal) As List(Of Entidades.DocumentoCompPercepcion)
        Return objDaoDocumentoCompPercepcion.DocumentoCompPercepcionAnticipo(IdDocumento, IdMonedaDestino, PorcentPercepcion)
    End Function
    ' AQUI CAMBIE A RAZON DE CRISTHIAN PANTA
    Public Function DocumentoCompPercepcion_NC(ByVal IdDocumento As Integer, ByVal IdMonedaDestino As Integer, ByVal PorcentPercepcion As Decimal) As List(Of Entidades.DocumentoCompPercepcion)
        Return objDaoDocumentoCompPercepcion.DocumentoCompPercepcion_NC(IdDocumento, IdMonedaDestino, PorcentPercepcion)
    End Function


    Public Function DocumentoCompPercepcion_Insert(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle_DocRef As List(Of Entidades.DocumentoCompPercepcion), _
                                                   ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), _
                                                   ByVal listaMovBanco As List(Of Entidades.MovBancoView), ByVal objMovCuenta As Entidades.MovCuenta, _
                                                   ByVal objObservacion As Entidades.Observacion, ByVal ListaMontoRegimenPercepcion As List(Of Entidades.MontoRegimen), _
                                                   Optional ByVal objAsientoContable As Entidades.be_asientoContable = Nothing, _
                                                   Optional ByVal idCadenaRequerimiento As String = "") As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************** INSERTAMOS LA CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '************* INSERTAMOS EL DETALLE / RELACION DOCUMENTO
            Dim objDaoDetalleDocumentoR As New DAO.DAODetalleDocumentoR
            For i As Integer = 0 To listaDetalle_DocRef.Count - 1

                '************* INSERTAMOS EL DETALLE
                With listaDetalle_DocRef(i)
                    objDaoDetalleDocumentoR.InsertaDetalleDocumentoR(objDocumento.Id, .Id, .IdMoneda, .FechaEmision, .Codigo, .Serie, .TotalAPagar, .PorcentPercepcion, .TipoCambio, .Percepcion, .ImporteTotal, cn, tr)
                End With

                '************ INSERTO RELACION DOCUMENTO
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                objDaoRelacionDocumento.InsertaRelacionDocumento((New Entidades.RelacionDocumento(listaDetalle_DocRef(i).Id, objDocumento.Id)), cn, tr)

            Next

            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            If (objMovCaja IsNot Nothing) Then

                '*********** Inserto Mov Caja
                Dim objDaoMovCaja As New DAO.DAOMovCaja
                objMovCaja.IdDocumento = objDocumento.Id
                objDaoMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                '*********** Inserto Lista Cancelacion
                Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                For i As Integer = 0 To listaCancelacion.Count - 1
                    listaCancelacion(i).IdDocumento = objDocumento.Id
                    objDaoPagoCaja.InsertaPagoCaja(cn, listaCancelacion(i), tr)
                Next

            End If

            '*********** Inserto Mov Cuenta
            If (objMovCuenta IsNot Nothing) Then

                Dim objDaoMovCuenta As New DAO.DAOMovCuenta
                objMovCuenta.IdDocumento = objDocumento.Id
                objDaoMovCuenta.InsertaMovCuenta(cn, objMovCuenta, tr)

            End If


            If Not ListaMontoRegimenPercepcion Is Nothing Then
                Dim dMontoRegimen As New DAO.DAOMontoRegimen

                For Each eMontoRegimen As Entidades.MontoRegimen In ListaMontoRegimenPercepcion
                    dMontoRegimen.InsertaMontoRegimen(cn, eMontoRegimen, tr)
                Next

            End If

            If objAsientoContable IsNot Nothing Then
                Dim dao As New DAO.DAODocCancelacion
                dao.insertarAsientoContable(cn, tr, objAsientoContable, idCadenaRequerimiento, objDocumento.Id)
            End If

            If (objObservacion IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If


            Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
            Dim objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
            For i As Integer = 0 To listaMovBanco.Count - 1
                listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                If (listaMovBanco(i).IdMovBanco <> Nothing) Then
                    objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdBanco), cn, tr)
                End If

            Next



            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            objDocumento.Id = -1
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function DocumentoCompPercepcion_Update(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle_DocRef As List(Of Entidades.DocumentoCompPercepcion), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal listaMovBanco As List(Of Entidades.MovBancoView), ByVal objMovCuenta As Entidades.MovCuenta, ByVal objObservacion As Entidades.Observacion, ByVal ListaMontoRegimenPercepcion As List(Of Entidades.MontoRegimen)) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '*****************   DESHACER MOV
            objDaoDocumentoCompPercepcion.DocumentoCompPercepcion_DeshacerMov(objDocumento.Id, True, True, True, True, True, False, True, True, cn, tr)

            '************** INSERTAMOS LA CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************* INSERTAMOS EL DETALLE / RELACION DOCUMENTO
            Dim objDaoDetalleDocumentoR As New DAO.DAODetalleDocumentoR
            For i As Integer = 0 To listaDetalle_DocRef.Count - 1

                '************* INSERTAMOS EL DETALLE
                With listaDetalle_DocRef(i)
                    objDaoDetalleDocumentoR.InsertaDetalleDocumentoR(objDocumento.Id, .Id, .IdMoneda, .FechaEmision, .Codigo, .Serie, .TotalAPagar, .PorcentPercepcion, .TipoCambio, .Percepcion, .ImporteTotal, cn, tr)
                End With

                '************ INSERTO RELACION DOCUMENTO
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                objDaoRelacionDocumento.InsertaRelacionDocumento((New Entidades.RelacionDocumento(listaDetalle_DocRef(i).Id, objDocumento.Id)), cn, tr)

            Next

            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            If (objMovCaja IsNot Nothing) Then

                '*********** Inserto Mov Caja
                Dim objDaoMovCaja As New DAO.DAOMovCaja
                objMovCaja.IdDocumento = objDocumento.Id
                objDaoMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                '*********** Inserto Lista Cancelacion
                Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                For i As Integer = 0 To listaCancelacion.Count - 1
                    listaCancelacion(i).IdDocumento = objDocumento.Id
                    objDaoPagoCaja.InsertaPagoCaja(cn, listaCancelacion(i), tr)
                Next

            End If

            '*********** Inserto Mov Cuenta
            If (objMovCuenta IsNot Nothing) Then

                Dim objDaoMovCuenta As New DAO.DAOMovCuenta
                objMovCuenta.IdDocumento = objDocumento.Id
                objDaoMovCuenta.InsertaMovCuenta(cn, objMovCuenta, tr)

            End If


            If Not ListaMontoRegimenPercepcion Is Nothing Then
                Dim dMontoRegimen As New DAO.DAOMontoRegimen

                For Each eMontoRegimen As Entidades.MontoRegimen In ListaMontoRegimenPercepcion
                    dMontoRegimen.InsertaMontoRegimen(cn, eMontoRegimen, tr)
                Next

            End If


            If (objObservacion IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If


            Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
            Dim objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
            For i As Integer = 0 To listaMovBanco.Count - 1
                listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                If (listaMovBanco(i).IdMovBanco <> Nothing) Then
                    objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdBanco), cn, tr)
                End If

            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            objDocumento.Id = -1
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id


    End Function

    Public Function DocumentoCompPercepcion_Anular(ByVal IdDocumento As Integer) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************* DESHACEMOS LOS MOVIMIENTOS
            objDaoDocumentoCompPercepcion.DocumentoCompPercepcion_DeshacerMov(IdDocumento, True, True, True, False, True, True, True, True, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
End Class

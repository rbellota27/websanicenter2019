﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Flete

    Dim obj As New DAO.DAOFlete

    Public Function deleteFlete(ByVal idflete As Integer) As Boolean
        Return obj.deleteFlete(idflete)
    End Function

    Public Function SelectFlete() As List(Of Entidades.Flete)
        Return obj.SelectFlete
    End Function

    Public Function InsertFlete(ByVal objEnt As Entidades.Flete) As Integer
        Return obj.InsertFlete(objEnt)
    End Function

    Public Function UpdateFlete(ByVal objEnt As Entidades.Flete) As Boolean
        Return obj.UpdateFlete(objEnt)
    End Function
End Class

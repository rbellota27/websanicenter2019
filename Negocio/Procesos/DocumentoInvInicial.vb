﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DocumentoInvInicial
    Private objDaoDocumentoInvInicial As New DAO.DAODocumentoInvInicial
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand

    Public Function DocumentoInventarioInicialSelectIdDocumentoxParams(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoDocumento As Integer) As Integer
        Return objDaoDocumentoInvInicial.DocumentoInventarioInicialSelectIdDocumentoxParams(IdEmpresa, IdAlmacen, IdTipoDocumento)
    End Function

    Public Function registrarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento)) As Integer

        Try

            '****************** VERIFICAMOS LA EXISTENCIA DEL DOCUMENTO
            objDocumento.Id = Me.DocumentoInventarioInicialSelectIdDocumentoxParams(objDocumento.IdEmpresa, objDocumento.IdAlmacen, objDocumento.IdTipoDocumento)

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            tr = cn.BeginTransaction

            Dim objDaoDocumento As New DAO.DAODocumento
            If (objDocumento.Id = Nothing) Then
                '*************** INSERTAMOS
                objDocumento.Id = objDaoDocumento.InsertaDocumento(cn, objDocumento, tr)
            Else
                objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)
            End If

            For i As Integer = 0 To listaDetalle.Count - 1

                listaDetalle(i).IdDocumento = objDocumento.Id
                objDaoDocumentoInvInicial.DocumentoInvInicial_RegistrarDetalle(listaDetalle(i), objDocumento.Id, True, 1, objDocumento.IdEmpresa, objDocumento.IdAlmacen, objDocumento.IdTipoOperacion, cn, tr)

            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

End Class

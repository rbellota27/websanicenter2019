﻿Imports System.Data.SqlClient
Public Class bl_Tonos
    Dim objConexion As New DAO.Conexion
    Dim objDaoNegocio As New DAO.DAO_Tonos
    Public Function SelectTonosxProductos_Nuevo(ByVal idProducto As Integer, ByVal idAlmacen As Integer, idDocumento As Integer) As List(Of Entidades.be_tonoXProducto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return objDaoNegocio.SelectTonosxProducto_Nuevo(idProducto, idAlmacen, idDocumento, cn)
    End Function

    Public Function SelectTonos(ByVal idProducto As Integer) As List(Of Entidades.Tono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return objDaoNegocio.SelectTono(idProducto, cn)
    End Function

    Public Function SelectTonos2(ByVal idProducto As Integer, ByVal idDocumento As Integer) As List(Of Entidades.Tono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return objDaoNegocio.SelectTono2(idProducto, idDocumento, cn)
    End Function
    Public Function SelectTonosxProductos_Editar(ByVal idDocumento As Integer, ByVal idDetalleDocumento As Integer) As List(Of Entidades.be_tonoXProducto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return objDaoNegocio.SelectTonosxProducto_Editar(idDocumento, idDetalleDocumento, cn)
    End Function

    Public Function SelectTonosxProducto(ByVal idProducto As Integer, ByVal iddocumentoRef As Integer, ByVal idUnidadMedida As Integer) As List(Of Entidades.be_tonoXProducto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return objDaoNegocio.DAO_ListarTono(cn, idProducto, iddocumentoRef, idUnidadMedida)
    End Function

    Public Function SelectTonosxProducto2(ByVal idProducto As Integer, ByVal nombre As String, ByVal desc As String, ByVal iddocumentoRef As Integer, ByVal idUnidadMedida As Integer) As List(Of Entidades.Tono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return objDaoNegocio.DAO_ListarTono2(cn, idProducto, nombre, desc, iddocumentoRef, idUnidadMedida)
    End Function

    Public Function SelectTonosxProductos_Editar_GuiaRecepcion(ByVal idProducto As Integer) As List(Of Entidades.be_tonoXProducto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return objDaoNegocio.SelectTonosxProducto_Editar_GuiaRecepcion(idProducto, cn)
    End Function

    Public Function ln_ListarTonosxProducto(ByVal idProducto As Integer) As List(Of Entidades.be_tonoXProducto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return (New DAO.DAO_Tonos).ListarTonosxProducto(idProducto, cn)
    End Function

    Public Function ln_ListarTonosxProducto_y_cantidades(ByVal idProducto As Integer, ByVal idAlmacen As Integer) As List(Of Entidades.be_tonoXProducto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Return (New DAO.DAO_Tonos).ListarTonosxProducto_y_cantidades(idProducto, idAlmacen, cn)
    End Function

    Public Function ln_SelectUnidadMedidaxProductos(ByVal prod_codigo As String) As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Return (New DAO.DAO_Tonos).SelectUnidadMedidaxProducto(prod_codigo, cn)
    End Function

    Public Function eliminarTono(ByVal idProducto As Integer, ByVal idTono As Integer, ByVal idAlmacen As Integer)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE        
        Return (New DAO.DAO_Tonos).eliminarTono(idProducto, idTono, idAlmacen, cn)
    End Function
End Class

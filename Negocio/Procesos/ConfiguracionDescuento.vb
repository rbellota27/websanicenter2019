﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
'*************************    JUEVES 08 ABRIL 2010 HORA 11_21 AM

Public Class ConfiguracionDescuento
    Dim obj As New DAO.DAOConfiguracionDescuento

    Public Function getPrecioBaseDcto(ByVal IdUsuario As Integer, ByVal IdTipoPV As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer) As String
        Return obj.getPrecioBaseDcto(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
    End Function

    Public Function getPorcentDctoMax(ByVal IdUsuario As Integer, ByVal IdTipoPV As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer) As Decimal
        Return obj.getPorcentDctoMax(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
    End Function


    Public Function InsertConfiguracionDescuento(ByVal objEnt As Entidades.ConfiguracionDescuento) As Integer
        Return obj.InsertConfiguracionDescuento(objEnt)
    End Function

    Public Function UpdateConfiguracionDescuento(ByVal objEnt As Entidades.ConfiguracionDescuento) As Boolean
        Return obj.UpdateConfiguracionDescuento(objEnt)
    End Function

    Public Function SelectConfiguracionDescuento(ByVal estado As Integer, ByVal idMedPago As Integer, ByVal idperfil As Integer, ByVal idTipoPv As Integer, ByVal idcondicionpago As Integer, ByVal desc_PrecioBaseDscto As String) As List(Of Entidades.ConfiguracionDescuento)
        Return obj.SelectConfiguracionDescuento(estado, idMedPago, idperfil, idTipoPv, idcondicionpago, desc_PrecioBaseDscto)
    End Function

End Class

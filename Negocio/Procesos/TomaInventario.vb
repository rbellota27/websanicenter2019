﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*********************   MIERCOLES  24/03/2010 HORA 05_59 PM


Imports System.Data.SqlClient
Public Class TomaInventario

    Private objConexion As New DAO.Conexion
    Private objDaoTomaInventario As New DAO.DAOTomaInventario
    Private cn As SqlConnection
    Private tr As SqlTransaction


    '****************************   VERSION II <  TOMA DE INVENTARIO  >
    Public Function DocumentoTomaInventario_SelectDocRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.DocumentoView)
        Return objDaoTomaInventario.DocumentoTomaInventario_SelectDocRef(IdEmpresa, IdTienda, IdAlmacen, Serie, Codigo, FechaInicio, FechaFin)
    End Function

    Public Function DocumentoTomaInventario_FiltroDetalle(ByVal IdDocumento As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                                          ByVal CodigoSubLinea As String, ByVal Producto As String, ByVal CodigoProducto As String, ByVal IdTipoexistencia As Integer, ByVal pageIndex As Integer, ByVal pageSize As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoTomaInventario.DocumentoTomaInventario_FiltroDetalle(IdDocumento, IdLinea, IdSubLinea, CodigoSubLinea, Producto, CodigoProducto, IdTipoexistencia, pageIndex, pageSize)
    End Function
    Public Function DocumentoTomaInventario_FiltroDetalle_Consolidado(ByVal IdDocumento As Integer, ByVal IdLinea As Integer, _
                                                                      ByVal IdSubLinea As Integer, ByVal CodigoSubLinea As String, ByVal Producto As String, ByVal CodigoProducto As String, ByVal IdtipoExistencia As Integer, ByVal pageIndex As Integer, ByVal PageSize As Integer) As DataSet
        Return objDaoTomaInventario.DocumentoTomaInventario_FiltroDetalle_Consolidado(IdDocumento, IdLinea, IdSubLinea, CodigoSubLinea, Producto, CodigoProducto, IdtipoExistencia, pageIndex, PageSize)
    End Function

    Public Function DocumentoTomaInventario_UpdateDetalle(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento)) As Integer

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '***************** VALIDAMOS EL DOCUMENTO
            objDaoTomaInventario.DocumentoTomaInventario_DeshacerMov(objDocumento.Id, False, False, False, False, False, cn, tr)

            '************************ INSERTO EL DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To listaDetalleDocumento.Count - 1

                listaDetalleDocumento(i).IdDocumento = objDocumento.Id
                objDaoTomaInventario.DocumentoTomaInventario_RegistrarDetalle(listaDetalleDocumento(i), cn, tr)

            Next

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento.Id

    End Function
    Public Function DocumentoTomaInventario_UpdateDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal listaDocumento_Persona As List(Of Entidades.Documento_Persona), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction


            '************** DESHACER MOV
            objDaoTomaInventario.DocumentoTomaInventario_DeshacerMov(objDocumento.Id, False, True, True, True, False, cn, tr)



            '********************** ACTUALIZO LA CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '****************** INSERTO RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento

            For i As Integer = 0 To listaRelacionDocumento.Count - 1

                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            '****************  INSERTO DOCUMENTO PERSONA
            Dim objDaoDocumento_Persona As New DAO.DAODocumento_Persona
            For i As Integer = 0 To listaDocumento_Persona.Count - 1

                listaDocumento_Persona(i).IdDocumento = objDocumento.Id
                objDaoDocumento_Persona.Documento_PersonaInsert(listaDocumento_Persona(i), cn, tr)

            Next

            '************************ INSERTO EL DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To listaDetalleDocumento.Count - 1

                listaDetalleDocumento(i).IdDocumento = objDocumento.Id
                objDaoTomaInventario.DocumentoTomaInventario_RegistrarDetalle(listaDetalleDocumento(i), cn, tr)

            Next

            '*********************** INSERTO OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then

                Dim objDaoObservaciones As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservaciones.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento.Id

    End Function
    Public Function DocumentoTomaInventario_AnularDocumento(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction


            '************** DESHACER MOV
            objDaoTomaInventario.DocumentoTomaInventario_DeshacerMov(IdDocumento, False, False, False, False, True, cn, tr)

            tr.Commit()

            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return hecho

    End Function



    Public Function DocumentoTomaInventario_GenerarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal listaDocumento_Persona As List(Of Entidades.Documento_Persona)) As Integer


        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1

                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            If (listaRelacionDocumento.Count > 0) Then

                objDaoTomaInventario.DocumentoTomaInventario_GenerarConsolidado_DetalleInsert(objDocumento.Id, cn, tr)

            End If

            Dim objDaoDocumento_Persona As New DAO.DAODocumento_Persona
            For i As Integer = 0 To listaDocumento_Persona.Count - 1

                listaDocumento_Persona(i).IdDocumento = objDocumento.Id
                objDaoDocumento_Persona.Documento_PersonaInsert(listaDocumento_Persona(i), cn, tr)

            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function DocumentoTomaInventario_CargarProductoDetallexParams(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal IdDocumento As Integer, ByVal FechaTomaInv As Date, ByVal PageIndex As Integer, ByVal pageSize As Integer, ByVal CodigoProducto As String) As List(Of Entidades.DetalleDocumento)

        Return objDaoTomaInventario.DocumentoTomaInventario_CargarProductoDetallexParams(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea, IdProducto, IdDocumento, FechaTomaInv, PageIndex, pageSize, CodigoProducto)

    End Function













    '***************************** VERSION I <  TOMA DE INVENTARIO  >

    Public Function SelectDetallexIdDocumentoxIdSubLinea_Ajuste(ByVal IdDocumentoTomaInv As Integer, ByVal IdSubLinea As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.DetalleDocumentoTomaInventario)
        Return objDaoTomaInventario.SelectDetallexIdDocumentoxIdSubLinea_Ajuste(IdDocumentoTomaInv, IdSubLinea, IdMonedaDestino)
    End Function


    Public Function RecalcularSaldosDocTomaInv(ByVal IdDocumento As Integer, ByVal FechaFin As Date, Optional ByVal NroDiasAdd As Integer = 0) As Boolean
        Return objDaoTomaInventario.RecalcularSaldosDocTomaInv(IdDocumento, FechaFin, NroDiasAdd)
    End Function

    Public Function getDataSet_DocTomaInvFormato(ByVal IdDocumento As Integer, ByVal opcion As String) As DataSet
        Return objDaoTomaInventario.getDataSet_DocTomaInvFormato(IdDocumento, opcion)
    End Function
    Public Function getDataSet_DocAjusteInventario(ByVal IdDocumento As Integer, ByVal opcion As Integer) As DataSet
        Return objDaoTomaInventario.getDataSet_DocAjusteInventario(IdDocumento, opcion)
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Documento
        Return objDaoTomaInventario.SelectxIdDocumento(IdDocumento)
    End Function

    Public Function SelectxIdSeriexCodigo(ByVal IdSerie As Integer, ByVal codigo As Integer) As Entidades.Documento
        Return objDaoTomaInventario.SelectxIdSeriexCodigo(IdSerie, codigo)
    End Function


    Public Function SelectLineaxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.Linea)
        Return objDaoTomaInventario.SelectLineaxIdDocumento(IdDocumento)
    End Function

    Public Function SelectSubLineaxIdDocumentoxIdLinea(ByVal IdDocumento As Integer, ByVal IdLinea As Integer) As List(Of Entidades.SubLinea)
        Return objDaoTomaInventario.SelectSubLineaxIdDocumentoxIdLinea(IdDocumento, IdLinea)
    End Function

    Public Function SelectDetallexIdDocumentoxIdSubLinea(ByVal IdDocumento As Integer, ByVal IdSubLinea As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoTomaInventario.SelectDetallexIdDocumentoxIdSubLinea(IdDocumento, IdSubLinea)
    End Function

    Public Function UpdateDocTomaInventario(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento)) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdDocumento As Integer = 0

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '************** Actualizo la cabecera
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '*************** Actualizo el detalle
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To listaDetalle.Count - 1

                If (Not objDaoDetalleDocumento.ActualizaDetalleDocumento(listaDetalle(i), cn, tr)) Then
                    Throw New Exception
                End If

            Next

            tr.Commit()
            IdDocumento = objDocumento.Id

        Catch ex As Exception

            tr.Rollback()
            IdDocumento = -1


        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try


        Return IdDocumento


    End Function

    Public Function GenerarDocTomaInventario(ByVal objDocumento As Entidades.Documento, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '********** Insertamos el Documento
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)
            If (objDocumento.Id <= 0) Then Throw New Exception

            '********* Generamos el detalle del documento de Toma de Inventarios
            '********* El IdDestinatario representa el Id de la Empresa(s) de cantidad saldo a calcular
            objDaoTomaInventario.GenerarDocTomaInventario(objDocumento.Id, objDocumento.IdDestinatario, objDocumento.IdAlmacen, IdLinea, IdSubLinea, objDocumento.FechaEntrega, cn, tr)


            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            objDocumento.Id = -1
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function registrarProcesoAjusteInventario(ByVal IdDocumentoTomaInv As Integer, ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento)) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim objDaoDocumento As New DAO.DAODocumento

        Try

            cn.Open()
            tr = cn.BeginTransaction

            'Select Case (existDocProcesoAjuste(IdDocumentoTomaInv, cn, tr))
            '    Case -1
            '        Throw New Exception("El Documento Toma Inv. ha sido anulado.")
            '    Case 0
            '        objDocumento.Id = objDaoDocumento.InsertaDocumento(cn, objDocumento, tr)
            '    Case 1
            'End Select

            If (objDocumento.Id = Nothing) Then
                '*********** El Documento aùn no ha sido generado

                '************** Inserto la cabecera
                objDocumento.Id = objDaoDocumento.InsertaDocumento(cn, objDocumento, tr)

                '************** Inserto el detalle y muevo Almacen de acuerdo al factor
                For i As Integer = 0 To listaDetalle.Count - 1

                    listaDetalle(i).IdDocumento = objDocumento.Id
                    objDaoTomaInventario.InsertaDetalleDocumentoAjusteInventario(cn, listaDetalle(i), tr)

                Next

                '************ Inserto relación documento
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                objDaoRelacionDocumento.InsertaRelacionDocumento(New Entidades.RelacionDocumento(IdDocumentoTomaInv, objDocumento.Id), cn, tr)

            Else

                '*************** Actualizo la cabecera
                objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

                '************** Inserto el detalle y muevo Almacen de acuerdo al factor
                For i As Integer = 0 To listaDetalle.Count - 1

                    listaDetalle(i).IdDocumento = objDocumento.Id
                    objDaoTomaInventario.InsertaDetalleDocumentoAjusteInventario(cn, listaDetalle(i), tr)

                Next

            End If







            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            objDocumento.Id = -1
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return objDocumento.Id
    End Function

    Private Function existDocProcesoAjuste(ByVal IdDocumentoTomaInv As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        '**** Valores de Retorno
        '**** -1: El Doc Toma Inv esta anulado, NO procede el Proceso de Ajuste
        '****  0: No existe un documento relacionado de Proceso de Ajuste Vàlido para este Doc Toma Inv. (INSERTAR UNO NUEVO)
        '****  1: EXISTE un documento relacionado de Proceso de Ajuste Vàlido para este Doc Toma Inv. (ACTUALIZAR EL EXISTENTE)

        Dim cmd As New SqlCommand("_DocumentoExistDocProcesoAjustexIdDocTomaInv", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocTomaInv", IdDocumentoTomaInv)

        Return (CInt(cmd.ExecuteScalar()))

    End Function

    Public Function SelectDocAjusteInventarioxIdDocTomaInv(ByVal IdDocumentoTomaInv As Integer) As Entidades.Documento

        Return objDaoTomaInventario.SelectDocAjusteInventarioxIdDocTomaInv(IdDocumentoTomaInv)

    End Function

    Public Function EliminarProcesoxIdProductoxIdDocumentoAjusteInv(ByVal IdDocAjusteInv As Integer, ByVal IdProducto As Integer) As Boolean
        Return objDaoTomaInventario.EliminarProcesoxIdProductoxIdDocumentoAjusteInv(IdDocAjusteInv, IdProducto)
    End Function

    Public Function SelectDocAjusteInventarioxIdSeriexCodigo(ByVal IdSerie As Integer, ByVal Codigo As Integer) As Entidades.Documento
        Return objDaoTomaInventario.SelectDocAjusteInventarioxIdSeriexCodigo(IdSerie, Codigo)
    End Function

    Public Function DocumentoAjusteInventarioAnularxIdDocumento(ByVal IdDocumento As Integer) As Boolean
        Return objDaoTomaInventario.DocumentoAjusteInventarioAnularxIdDocumento(IdDocumento)
    End Function
    Public Function DocumentoTomaInventarioSelectxIdAlmacen(ByVal IdAlmacen As Integer) As List(Of Entidades.Documento)
        Return objDaoTomaInventario.DocumentoTomaInventarioSelectxIdAlmacen(IdAlmacen)
    End Function

    Public Function DocumentTomaInventarioAnularxIdDocumento(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction

            objDaoTomaInventario.DocumentTomaInventarioAnularxIdDocumento(IdDocumento, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function
End Class

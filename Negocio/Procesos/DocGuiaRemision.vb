﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'******************   LUNES 31 MAYO 2010 HORA 03_23 PM


Imports System.Data.SqlClient

Public Class DocGuiaRemision

    Private objDaoDocGuiaRemision As New DAO.DAODocGuiaRemision
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private tr As SqlTransaction

    Public Function CostoFlete_AnalisisCostoFlete_PrecioVenta(ByVal Tabla_IdDocumento As DataTable, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal Producto As String, ByVal CodigoProducto As String, ByVal IdTiendaDestino As Integer, ByVal IdTipoPV As Integer, ByVal IdTiendaOrigen As Integer, ByVal IdMoneda As Integer) As DataTable
        Return objDaoDocGuiaRemision.CostoFlete_AnalisisCostoFlete_PrecioVenta(Tabla_IdDocumento, IdLinea, IdSubLinea, Producto, CodigoProducto, IdTiendaDestino, IdTipoPV, IdTiendaOrigen, IdMoneda)
    End Function


    Public Function DocumentoGuiaRemision_CostoFlete_SelectxParams(ByVal IdEmpresa As Integer, ByVal IdAlmacenOrigen As Integer, ByVal IdAlmacenDestino As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Serie As Integer, ByVal Codigo As Integer) As List(Of Entidades.Documento)
        Return objDaoDocGuiaRemision.DocumentoGuiaRemision_CostoFlete_SelectxParams(IdEmpresa, IdAlmacenOrigen, IdAlmacenDestino, FechaInicio, FechaFin, Serie, Codigo)
    End Function


    Public Function DocumentoGuiaRemisionSelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoDocGuiaRemision.DocumentoGuiaRemisionSelectDetalle(IdDocumento)
    End Function

    Public Function DocumentoGuiaRemisionSelectxParams(ByVal IdSerie As Integer, ByVal codigo As Integer) As Entidades.DocGuiaRemision
        Return objDaoDocGuiaRemision.DocumentoGuiaRemisionSelectxParams(IdSerie, codigo)
    End Function

    'Public Sub registrarTono(ByVal idDocumento As Integer, ByVal dt_tonos As DataTable)
    '    cn = (New DAO.Conexion).ConexionSIGE

    '    '*************DETALLE TONOS
    '    Try
    '        cn.Open()
    '        tr = cn.BeginTransaction
    '        objDaoDocGuiaRemision.DocumentoInsertaDatatableTonos(cn, tr, idDocumento, dt_tonos)
    '        tr.Commit()
    '    Catch ex As Exception
    '        tr.Rollback()
    '        Throw ex
    '    Finally
    '        If (cn.State = ConnectionState.Open) Then cn.Close()
    '    End Try
    'End Sub

    Public Function registrarGuiaRemision(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), _
                                          ByVal objObservaciones As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, _
                                          ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), _
                                          ByVal Factor As Integer, ByVal MoverStockFisico As Boolean, ByVal ValidarDespacho As Boolean) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try

            '**** MoverStockFisico: MUEVE EL STOCK DE ALMACÉN
            '**** ValidarDespacho: valida el despacho ( cantidad, etc ) y actualiza el stock x atender afecto

            cn.Open()
            tr = cn.BeginTransaction


            '************* CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '************* DETALLE
            Dim idDetalleDocumento As Integer = objDaoDocGuiaRemision.DocumentoGuiaRemision_DetalleInsert(cn, listaDetalleDocumento, tr, objDocumento.Id, Factor, MoverStockFisico, ValidarDespacho)

            '*************DETALLE TONOS
            'objDaoDocGuiaRemision.DocumentoInsertaDatatableTonos(cn, tr, objDocumento.Id, dt_tonos)

            '************ PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '************ PUNTO LLEGADA
            If (objPuntoLlegada IsNot Nothing) Then
                Dim objDaoPuntollegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objDaoPuntollegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            '************ OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function actualizarGuiaRemision(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), _
                                           ByVal objObservaciones As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, _
                                           ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), _
                                           ByVal Factor As Integer, ByVal MoverStockFisico As Boolean, ByVal ValidarDespacho As Boolean) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try

            '**** MoverStockFisico: MUEVE EL STOCK DE ALMACÉN
            '**** ValidarDespacho: valida el despacho ( cantidad, etc ) y actualiza el stock x atender afecto

            cn.Open()
            tr = cn.BeginTransaction

            '******************** DESHACEMOS MOV
            objDaoDocGuiaRemision.DocumentoGuiaRemision_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, False, cn, tr)

            '************* CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************* DETALLE
            objDaoDocGuiaRemision.DocumentoGuiaRemision_DetalleInsert(cn, listaDetalleDocumento, tr, objDocumento.Id, Factor, MoverStockFisico, ValidarDespacho)

            '************ PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '************ PUNTO LLEGADA
            If (objPuntoLlegada IsNot Nothing) Then
                Dim objDaoPuntollegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objDaoPuntollegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            '************ OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function anularGuiaRemision(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        Try
            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction
            objDaoDocGuiaRemision.DocumentoGuiaRemision_DeshacerMov(IdDocumento, False, True, False, False, False, False, True, True, cn, tr)
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function DocumentoSolicitudPagpSelectDocumentoRef(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)
        Return objDaoDocGuiaRemision.DocumentoSolicitudPagpSelectDocumentoRef(IdDocumento)
    End Function
    Public Function DocumentoGuiaRemisionSelectDocumentoRef(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)
        Return objDaoDocGuiaRemision.DocumentoGuiaRemisionSelectDocumentoRef(IdDocumento)
    End Function
End Class

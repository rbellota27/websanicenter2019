﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'*********************   VIERNES 14 MAYO 2010 HORA 04_38 PM








Imports System.Data.SqlClient
Public Class DocumentoCheque
    Private cn As SqlConnection
    Private objDaoDocumentoCheque As New DAO.DAODocumentoCheque
    Private objConexion As New DAO.Conexion

    'Public Function RegistrarDocumentoCheque(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal objObservacion As Entidades.Observacion, ByVal objMovCuenta As Entidades.MovCuenta, ByVal objAnexo As Entidades.Anexo_Documento) As Integer

    Public Function RegistrarDocumentoCheque(ByVal objDocumento As Entidades.Documento, ByVal objObservacion As Entidades.Observacion, _
                                             ByVal objMovBanco As Entidades.MovBanco, ByVal objAnexo As Entidades.Anexo_Documento, _
                                             ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), _
                                             ByVal listaRelacionDoc2Prog As List(Of Entidades.RelacionDocumento)) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try
            cn.Open()
            tr = cn.BeginTransaction

            '*************** Insertamos la cabecera
            Dim objDaoDocumento As New DAO.DAODocumento
            objDocumento.Id = objDaoDocumento.InsertaDocumento(cn, objDocumento, tr)

            ''************* Insertamos el detalle
            'Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            'For i As Integer = 0 To listaDetalleConcepto.Count - 1
            '    listaDetalleConcepto(i).IdDocumento = objDocumento.Id
            '    objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            'Next

            ''************* Insertamos Relacion Documento
            'Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            'For i As Integer = 0 To listaRelacionDocumento.Count - 1
            '    listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
            '    objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            'Next

            ''************** Insertamos Observacion
            If (objObservacion IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '*************** Insertamos Mov Banco
            If (objMovBanco IsNot Nothing) Then

                Dim objDaoMovBanco As New DAO.DAOMovBanco

                objMovBanco.IdMovBanco = objDaoMovBanco.InsertT_GetID(objMovBanco, cn, tr)


                '*************** Insertamos Anexo Mov Banco, este va a contener la relacion 
                'entre el mov banco realizado y el documento cheque
                'el anexo mov banco guarda el id del documento cheque
                With objMovBanco
                    If .IdMovBanco > 0 Then
                        Dim objDAOAnexo_MovBanco As New DAO.DAOAnexo_MovBanco
                        Dim objAnexo_MovBanco As New Entidades.Anexo_MovBanco(.IdMovBanco, _
                            .IdCuentaBancaria, Nothing, objDocumento.Id, .IdBanco)

                        objAnexo_MovBanco.IdAnexo_MovBanco = objDAOAnexo_MovBanco.Anexo_MovBancoInsert(objAnexo_MovBanco, cn, tr)
                    End If
                End With
            End If

            ''*************** Insertamos Anexo Documento
            If (objAnexo IsNot Nothing) Then
                Dim objDAOAnexo As New DAO.DAOAnexo_Documento
                objAnexo.IdDocumento = objDocumento.Id
                objDAOAnexo.InsertAnexo_Documento(objAnexo, cn, tr)
            End If

            '********************* INSERTAMOS RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '****************** INSERTAMOS EL SEGUNDO DOCUMENTO REFERENCIA PROGRAMACION PAGO CON CHEQUE
            Dim objDaoRelacionDocumento2 As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDoc2Prog.Count - 1
                listaRelacionDoc2Prog(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento2.InsertRelacionChequeProgramacionPago(listaRelacionDoc2Prog(i), cn, tr)
            Next
            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            objDocumento.Id = -1
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    'Public Function ActualizarDocumentoCheque(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal objObservacion As Entidades.Observacion, ByVal objMovCuenta As Entidades.MovCuenta, ByVal objAnexo As Entidades.Anexo_Documento) As Integer
    Public Function ActualizarDocumentoCheque(ByVal objDocumento As Entidades.Documento, ByVal objObservacion As Entidades.Observacion, ByVal objMovBanco As Entidades.MovBanco, ByVal objAnexo As Entidades.Anexo_Documento, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '***************** DESHACER MOV
            objDaoDocumentoCheque.DocumentoCheque_DeshacerMov(objDocumento.Id, True, cn, tr)

            '*************** Actualizamos la cabecera
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************* Insertamos el detalle
            'Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            'For i As Integer = 0 To listaDetalleConcepto.Count - 1
            '    listaDetalleConcepto(i).IdDocumento = objDocumento.Id
            '    objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            'Next

            '************* Insertamos Relacion Documento
            'Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            'For i As Integer = 0 To listaRelacionDocumento.Count - 1
            '    listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
            '    objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            'Next

            '************** Eliminamos y luego Insertamos Observacion
            If (objObservacion IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.DeletexIdDocumento(objDocumento.Id, cn, tr)
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)

            End If

            '*************** Actualizamos Mov Banco
            If (objMovBanco IsNot Nothing) Then
                Dim objDaoMovBanco As New DAO.DAOMovBanco
                objDaoMovBanco.UpdateT(objMovBanco, cn, tr)
            End If
            ''*************** Eliminamos y luego Insertamos Anexo Documento
            If (objAnexo IsNot Nothing) Then
                Dim objDAOAnexo As New DAO.DAOAnexo_Documento
                objAnexo.IdDocumento = objDocumento.Id

                objDAOAnexo.DeletexIdDocumento(objAnexo.IdDocumento, cn, tr)
                objDAOAnexo.InsertAnexo_Documento(objAnexo, cn, tr)
                'objDAOAnexo.ActualizaAnexoDocT(cn, objDocumento.Id, objAnexo, tr)
            End If

            '********************* INSERTAMOS RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento

            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                Dim valor As Integer = objDocumento.IdDocumento
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function SelectAll() As List(Of Entidades.DocumentoCheque)
        Return objDaoDocumentoCheque.SelectAll
    End Function

    Public Function SelectxId(ByVal id As Integer) As Entidades.DocumentoCheque
        Return objDaoDocumentoCheque.SelectxId(id)
    End Function

    Public Function SelectxEstadoCancelacion(ByVal Cancelado As Boolean) As List(Of Entidades.DocumentoCheque)
        Return objDaoDocumentoCheque.SelectxEstadoCancelacion(Cancelado)
    End Function
    Public Function SelectxIdBeneficiario(ByVal IdBeneficiario As Integer) As List(Of Entidades.DocumentoCheque)
        Return objDaoDocumentoCheque.SelectxIdBeneficiario(IdBeneficiario)
    End Function
    Public Function SelectxIdBeneficiarioxEstadoCancelacion(ByVal IdBeneficiario As Integer, ByVal Cancelado As Boolean) As List(Of Entidades.DocumentoCheque)
        Return objDaoDocumentoCheque.SelectxIdBeneficiarioxEstadoCancelacion(IdBeneficiario, Cancelado)
    End Function
    Public Function SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(ByVal IdBeneficiario As Integer, ByVal Cancelado As Boolean) As List(Of Entidades.DocumentoCheque)
        Return objDaoDocumentoCheque.SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(IdBeneficiario, Cancelado)
    End Function
    'Public Function AnularDocumentoCheque(ByVal IdDocumento As Integer) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim tr As SqlTransaction = Nothing
    '    Dim hecho As Boolean = False
    '    Try

    '        cn.Open()
    '        tr = cn.BeginTransaction

    '        '******************* DESHACEMOS LOS MOVIMIENTOS
    '        'objDaoDocumentoCheque.DocumentoCheque_DeshacerMov(IdDocumento, False, True, True, True, False, cn, tr)

    '        tr.Commit()
    '        hecho = True

    '    Catch ex As Exception
    '        tr.Rollback()
    '        hecho = False
    '        Throw ex
    '    Finally
    '        If (cn.State = ConnectionState.Open) Then cn.Close()
    '    End Try

    '    Return hecho
    'End Function

    Public Function GenerarDocCodigoChequexIdSerieCheque(ByVal IdSerieCheque As Integer) As String
        Return objDaoDocumentoCheque.GenerarDocCodigoChequexIdSerieCheque(IdSerieCheque)
    End Function

    Public Function ValidarNumeroCheque(ByVal IdDocumento As Integer, ByVal IdSerieCheque As Integer, ByVal ChequeNumero As String) As Boolean
        Return objDaoDocumentoCheque.ValidarNumeroCheque(IdDocumento, IdSerieCheque, ChequeNumero)
    End Function

#Region "Filtro para las busquedas"

    Private P As New Entidades.DocumentoCheque
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.DocumentoCheque) As Boolean
        'x va a ser el objeto a filtrar
        Try
            If (P.Id = 0 Or x.Id = P.Id Or P.Id = Nothing) _
            And (P.SerieCheque.IdBanco = 0 Or x.SerieCheque.IdBanco = P.SerieCheque.IdBanco) _
            And (P.SerieCheque.IdCuentaBancaria = 0 Or x.SerieCheque.IdCuentaBancaria = P.SerieCheque.IdCuentaBancaria) _
            And ((P.IdSerieCheque = 0) Or x.IdSerieCheque = P.IdSerieCheque) _
            And ((P.IdEstadoDoc = 0) Or x.IdEstadoDoc = P.IdEstadoDoc) _
            And ((P.IdEstadoEntrega = 0) Or x.IdEstadoEntrega = P.IdEstadoEntrega) _
            And ((P.IdEstadoCancelacion = 0) Or x.IdEstadoCancelacion = P.IdEstadoCancelacion) _
            And ((P.IdBeneficiario = 0) Or x.IdBeneficiario = P.IdBeneficiario) _
            And ((P.ChequeNumero = "") Or x.ChequeNumero.ToLower.Trim.Contains(P.ChequeNumero.ToLower.Trim)) Then
                'And (x.EstadoMov = P.EstadoMov Or (IsNothing(P.EstadoMov))) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.DocumentoCheque, _
    ByVal List As List(Of Entidades.DocumentoCheque)) As List(Of Entidades.DocumentoCheque)

        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function FiltrarListaxRangoFechas(ByVal filtro As Entidades.DocumentoCheque, _
       ByVal List As List(Of Entidades.DocumentoCheque), _
       ByVal fecha_ini As Date, ByVal fecha_fin As Date) As List(Of Entidades.DocumentoCheque)

        Dim L2, L3 As List(Of Entidades.DocumentoCheque)
        L3 = New List(Of Entidades.DocumentoCheque)
        Dim i As Integer

        P = filtro

        L2 = List.FindAll(AddressOf FiltrarEntidadxParametros)
        For i = 0 To L2.Count - 1
            If L2(i).FechaACobrar >= fecha_ini And L2(i).FechaACobrar <= fecha_fin Then
                L3.Add(L2(i))
            End If
        Next
        Return L3

    End Function

    Public Function LN_EmitirCheque(ByVal IdMovCtaPP As Integer) As List(Of Entidades.DocumentoCheque)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAODocumentoCheque).DAO_EmitirCheque(cn, IdMovCtaPP)
        cn.Close()
    End Function

    'Public Function ValidarEntidadxNombre(ByVal filtro As Entidades.DocumentoCheque, ByVal List As List(Of Entidades.DocumentoCheque)) As Boolean
    '    'X es el valor a verificar, 

    '    Dim x As New Entidades.DocumentoCheque
    '    x.Id = filtro.Id
    '    x.NroOperacion = filtro.NroOperacion

    '    Dim List2 As List(Of Entidades.DocumentoCheque)
    '    List2 = ValidarLista(x, List)

    '    If List2.Count > 0 Then 'Existen registros  
    '        Return False
    '    Else
    '        Return True
    '    End If

    'End Function

    'Private Function ValidarEntidadxParametros(ByVal x As Entidades.DocumentoCheque) As Boolean
    '    'Se utiliza a la hora de insertar o eliminar
    '    'x va a ser el objeto a filtrar
    '    'En este caso no se cuenta el registro analizado
    '    If (x.Id <> P.Id) _
    '    And (x.NroOperacion.ToLower.Trim = P.NroOperacion.ToLower.Trim Or P.NroOperacion = "") Then
    '        'And (x.Estado = P.Estado Or (Not P.Estado.HasValue))
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    'Public Function ValidarLista(ByVal filtro As Entidades.DocumentoCheque, ByVal List As List(Of Entidades.DocumentoCheque)) As List(Of Entidades.DocumentoCheque)
    '    P = filtro
    '    Return List.FindAll(AddressOf ValidarEntidadxParametros)

    'End Function
#End Region

End Class
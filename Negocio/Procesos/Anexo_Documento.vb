﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.








'************************   JUEVES 15 ABRIL 01_09 PM






Imports System.Data.SqlClient
Public Class Anexo_Documento
    Dim ObjConexion As New DAO.Conexion
    Dim HDAO As New DAO.HelperDAO
    Private objDaoAnexoD As New DAO.DAOAnexo_Documento

    Public Function Anexo_DocumentoSelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Anexo_Documento
        Return objDaoAnexoD.Anexo_DocumentoSelectxIdDocumento(IdDocumento)
    End Function

    Public Sub InsertAnexoDocumentoT(ByVal cn As SqlConnection, ByVal objAnexoDoc As Entidades.Anexo_Documento, Optional ByVal T As SqlTransaction = Nothing)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = objAnexoDoc.IdDocumento
        ArrayParametros(1) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
        ArrayParametros(1).Value = objAnexoDoc.doc_Importacion
        ArrayParametros(2) = New SqlParameter("@anex_PrecImportacion", SqlDbType.VarChar, 10)
        ArrayParametros(2).Value = objAnexoDoc.PrecImportacion
        ArrayParametros(3) = New SqlParameter("@anex_NroContenedores", SqlDbType.VarChar, 20)
        ArrayParametros(3).Value = objAnexoDoc.NroContenedores
        HDAO.InsertaT(cn, "_AnexoDocumentoInsert", ArrayParametros, T)
    End Sub
    Public Sub ActualizaAnexoDocumentoT(ByVal cn As SqlConnection, ByVal objAnexoDoc As Entidades.Anexo_Documento, Optional ByVal T As SqlTransaction = Nothing)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = objAnexoDoc.IdDocumento
        ArrayParametros(1) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
        ArrayParametros(1).Value = objAnexoDoc.doc_Importacion
        ArrayParametros(2) = New SqlParameter("@anex_PrecImportacion", SqlDbType.VarChar, 10)
        ArrayParametros(2).Value = objAnexoDoc.PrecImportacion
        ArrayParametros(3) = New SqlParameter("@anex_NroContenedores", SqlDbType.VarChar, 20)
        ArrayParametros(3).Value = objAnexoDoc.NroContenedores
        HDAO.InsertaT(cn, "_AnexoDocumentoUpdate", ArrayParametros, T)
    End Sub




End Class

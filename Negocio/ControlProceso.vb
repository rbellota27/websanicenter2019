﻿Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class ControlProceso
    Dim objDAOControl As New DAO.DAOControlProceso
    Dim objConexion As DAO.Conexion

    Public Function SelectControlProc(ByVal var As String) As List(Of Entidades.ControlProceso)
        Return objDAOControl.SelectControlProc(var)
    End Function

    Public Function SelectxIdControl(ByVal idControl As Integer) As List(Of Entidades.ControlProceso)
        Return objDAOControl.SelectxIdControl(idControl)
    End Function

    Public Function SelectxNombreControl(ByVal nombre As String)
        Return objDAOControl.SelectxNombreControl(nombre)
    End Function

    Public Function AgregarControlProc(ByVal objControl As Entidades.ControlProceso) As Boolean

        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim idproceso As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            idproceso = objDAOControl.AgregarControlProceso(objControl, cn, "i", tr)

            If idproceso = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If


            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho

    End Function



    Public Function ActualizaControlProc(ByVal objControl As Entidades.ControlProceso) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim idproceso As Integer = 0
        Dim hecho As Boolean = False

        Try
            cn.Open()
            tr = cn.BeginTransaction
            idproceso = objDAOControl.ActualizaControlProceso(objControl, cn, "a", tr)

            If idproceso = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If


            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function EliminarControlProceso(ByVal id As Integer) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If Not objDAOControl.EliminaControlProceso(id, cn, "e", tr) Then
                Throw New Exception("Fracasó el proceso de Eliminación.")
            End If
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

End Class

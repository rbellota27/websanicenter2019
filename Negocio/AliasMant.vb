﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports DAO
Imports Entidades
Public Class AliasMant
    Dim DAO As New DAO.DAOAlias
    Public Function InsertaAlias(ByVal obj As Entidades.AliasMant) As Boolean
        Return DAO.InsertaAlias(obj)
    End Function
    Public Function ActualizaAlias(ByVal obj As Entidades.AliasMant) As Boolean
        Return DAO.ActualizaAlias(obj)
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.AliasMant)
        Return DAO.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.AliasMant)
        Return DAO.SelectAllInactivo
    End Function
    Public Function SelectAll() As List(Of Entidades.AliasMant)
        Return DAO.SelectAll
    End Function
    Public Function SelectxId(ByVal Id As Integer) As List(Of Entidades.AliasMant)
        Return DAO.SelectxId(Id)
    End Function
    Public Function SelectAllxNombre(ByVal Persona As String) As List(Of Entidades.AliasMant)
        Return DAO.SelectAllxNombre(Persona)
    End Function
    Public Function SelectActivoxNombre(ByVal Persona As String) As List(Of Entidades.AliasMant)
        Return DAO.SelectActivoxNombre(Persona)
    End Function
    Public Function SelectInactivoxNombre(ByVal Persona As String) As List(Of Entidades.AliasMant)
        Return DAO.SelectInactivoxNombre(Persona)
    End Function
End Class

﻿Imports System.Data
Imports System.Data.SqlClient
Imports Entidades
Imports DAO
Imports System.Configuration

Public Class brCotizacion
	Inherits brGeneral
	Private objDaoDocumentoCotizacion As New DAO.DAODocumentoCotizacion
	Private objConexion As New DAO.Conexion
	Private cn As SqlConnection
	Private cmd As SqlCommand
	Private tr As SqlTransaction


	Public Function obtenerListas(ByVal IdUsuario As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer,
	   ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdCondicionPago As Integer,
	   ByVal CodDep As String, ByVal CodProv As String, ByVal IdTipoExistencia As Integer,
	   ByVal IdMangitud As Integer, ByRef Permisos As String, ByVal Parametros As String) As beCotizacionListas
		'ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, 

		Dim obeCotizacionListas As New beCotizacionListas

		Try
			Using con As New SqlConnection(CadenaConexion)
				con.Open()
				Dim obj As New daCotizacion
				obeCotizacionListas = obj.obtenerListas(IdUsuario, IdEmpresa, IdTienda, IdTipoDocumento, IdTipoOperacion, IdCondicionPago, CodDep, CodProv, IdTipoExistencia, IdMangitud, Permisos, Parametros, con)
			End Using
		Catch ex As Exception

		End Try
		Return obeCotizacionListas
	End Function

	Public Function obtenerProductos(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String,
	   ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer,
	   ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer,
	   ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable,
	   ByVal codigoProducto As String, ByVal codbarras As String, Optional ByVal filtroProductoCampania As Boolean = False,
	   Optional ByVal IdTipoExistencia As Integer = 0, Optional ByVal codigoProveedor As String = "", Optional ByVal IdTipoOperacion As Integer = 1) As List(Of Entidades.Catalogo)

		Dim lbeCotizacionListas As New List(Of Entidades.Catalogo)

		Try
			Using con As New SqlConnection(CadenaConexion)
				con.Open()
				Dim obj As New daCotizacion
				lbeCotizacionListas = obj.obtenerProductos(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize,
				 pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania,
				 codbarras, IdTipoExistencia, codigoProveedor, IdTipoOperacion, con)
			End Using
		Catch ex As Exception

		End Try
		Return lbeCotizacionListas
	End Function

	Public Function registrarCotizacion(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal lista_DetalleDocumento As String, ByVal lista_DetalleConcepto As String, ByVal objObservacion As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objMontoRegimenPercepcion As Entidades.MontoRegimen, ByVal lista_RelacionDocumento As String) As Integer
		Dim id As Integer = 0
		Try
			cn = objConexion.ConexionSIGE
			cn.Open()
			tr = cn.BeginTransaction

			Dim odaCotizacion As New daCotizacion
			id = odaCotizacion.adicionar(cn, objDocumento, objAnexoDocumento, lista_DetalleDocumento, lista_DetalleConcepto, objObservacion, objPuntoPartida, objPuntoLlegada, objMontoRegimenPercepcion, lista_RelacionDocumento, tr)

			tr.Commit()

		Catch ex As Exception
			tr.Rollback()
			Throw ex
		Finally
			If (cn.State = ConnectionState.Open) Then cn.Close()
		End Try

		Return id

	End Function

	Public Function updateCotizacion(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal lista_DetalleDocumento As String, ByVal lista_DetalleConcepto As String, ByVal objObservacion As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objMontoRegimenPercepcion As Entidades.MontoRegimen, ByVal lista_RelacionDocumento As String) As Integer

		Try
			Dim id As Integer = 0
			cn = objConexion.ConexionSIGE
			cn.Open()
			tr = cn.BeginTransaction

			'**************** SE DESHACEN LOS MOVIMIENTOS
			objDaoDocumentoCotizacion.DocumentoCotizacion_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, True, True, False, cn, tr)

			Dim odaCotizacion As New daCotizacion
			id = odaCotizacion.actualizar(cn, objDocumento, objAnexoDocumento, lista_DetalleDocumento, lista_DetalleConcepto, objObservacion, objPuntoPartida, objPuntoLlegada, objMontoRegimenPercepcion, lista_RelacionDocumento, tr)

			tr.Commit()
		Catch ex As Exception
			tr.Rollback()
			Throw ex
		Finally
			If (cn.State = ConnectionState.Open) Then cn.Close()
		End Try

		Return objDocumento.Id

	End Function

	Public Function buscarCotizacion(ByVal IdSerie As Integer, ByVal nroDocumento As Integer, ByVal IdDocumento As Integer, ByVal IdTipoDocumento As Integer, ByVal IdMagnitudPeso As Integer, ByVal PesoTotal As Integer, ByVal IdTipoDocumentoMP As Integer, Optional ByVal Consignacion As Boolean = False) As beCotizacionBuscar
		Dim obeCotizacionBuscar As beCotizacionBuscar = Nothing
		Using con As New SqlConnection(CadenaConexion)
			Try
				con.Open()
				Dim odaCotizacion As New daCotizacion
				obeCotizacionBuscar = odaCotizacion.buscarCotizacion(IdSerie, nroDocumento, IdDocumento, IdTipoDocumento, Consignacion, IdMagnitudPeso, PesoTotal, IdTipoDocumentoMP, con)
			Catch ex As Exception
                Throw ex
			End Try
		End Using
		Return obeCotizacionBuscar
	End Function

	Public Function NuevaCotizacion(ByVal IdTienda As Integer, ByVal IdSerie As Integer)
		Dim obeCotizacionNuevo As beCotizacionNuevo = Nothing
		Using con As New SqlConnection(CadenaConexion)
			Try
				con.Open()
				Dim odaCotizacion As New daCotizacion
				obeCotizacionNuevo = odaCotizacion.nuevaCotizacion(IdTienda, IdSerie, con)
			Catch ex As Exception

			End Try
		End Using
		Return obeCotizacionNuevo
	End Function

	Public Function cargarDocumentoReferencia(ByVal IdDocumentoReferencia As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer, ByVal IdTipoDocumento As Integer, ByVal CondicionPago As Integer)
		Dim obeCotizacionReferencia As beCotizacionReferencia = Nothing
		Using con As New SqlConnection(CadenaConexion)
			Try
				con.Open()
				Dim odaCotizacion As New daCotizacion
				obeCotizacionReferencia = odaCotizacion.cargarDocumentoReferencia(IdDocumentoReferencia, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdMoneda, IdVendedor, IdTipoDocumento, CondicionPago, con)
			Catch ex As Exception

			End Try
		End Using
		Return obeCotizacionReferencia
	End Function

End Class



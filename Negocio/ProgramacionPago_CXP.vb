﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM


Imports System.Data.SqlClient
Public Class ProgramacionPago_CXP
    Private objDaoProgPago_CXP As New DAO.DAOProgramacionPago_CXP
    Private obj As New DAO.DAORequerimientoGasto
    Private objanex As New DAO.DAOAnexo_Documento

    Private cn As SqlConnection
    Private tr As SqlTransaction

    Public Function SelectxIdProgramacionPago(ByVal IdProgramacionPago_CXP As Integer) As Entidades.ProgramacionPago_CXP
        Return objDaoProgPago_CXP.SelectxIdProgramacionPago(IdProgramacionPago_CXP)
    End Function


    Public Function MarcarProgramacionAprobacion(ByVal IdDocumento As Integer, ByVal estado As Integer) As Boolean
        'Dim hecho As Boolean = False

        'Try

        '    'cn = (New DAO.Conexion).ConexionSIGE
        '    'cn.Open()

        '    tr = cn.BeginTransaction

        '    objanex.MarcarProgramacionAprobacion(IdDocumento, estado)

        '    cn.Close()
        '    tr.Commit()
        '    hecho = True

        'Catch ex As Exception
        '    tr.Rollback()
        '    hecho = False
        '    Throw ex
        'Finally
        '    If (cn.State = ConnectionState.Open) Then cn.Close()
        'End Try

        'Return hecho

        Dim retornar As Boolean = False
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            retornar = objanex.MarcarProgramacionAprobacion(IdDocumento, estado, cn, tr)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return retornar
    End Function


    Public Function AprobarProgramacion(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            tr = cn.BeginTransaction

            objanex.UpdateProgramacionAprobacion(IdDocumento)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho
    End Function



    Public Sub Insert(ByVal cadenaDocumentoReferencia As String, ByVal idRequerimientoCadena As String, ByVal objProgramacionPago_CXP As Entidades.ProgramacionPago_CXP, _
                      ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal idEsDetraccion As Boolean, ByVal idEsRetencion As Boolean, _
                      ByVal idEsPercepcion As Boolean, ByVal modo As Integer, ByVal idRegimen As Integer, _
                      Optional ByVal listaDocumentosAplicacionesContraDeudas As List(Of Entidades.be_documentoAplicacionDeudasPendientes) = Nothing, _
                      Optional ByVal listaTipoAgente As List(Of Entidades.TipoAgente) = Nothing)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        tr = cn.BeginTransaction
        Try
            Dim porcent As Decimal = 0
            If (listaRelacionDocumento IsNot Nothing) Then
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                If Not IsNothing(listaTipoAgente) Then
                    If listaTipoAgente.Count > 0 Then
                        porcent = CDec(listaTipoAgente(0).Tasa)
                    End If
                End If
                '  qweqwe()
                For i As Integer = 0 To listaRelacionDocumento.Count - 1
                    objDaoRelacionDocumento.InsertaRelacionDocRegimen(listaRelacionDocumento(i), cn, tr, idRegimen, porcent, idRequerimientoCadena)
                Next
            End If
            If (listaDocumentosAplicacionesContraDeudas IsNot Nothing) Then
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                For i As Integer = 0 To listaDocumentosAplicacionesContraDeudas.Count - 1
                    objDaoRelacionDocumento.InsertaRelacionAplicacionContraDeudas(listaDocumentosAplicacionesContraDeudas(i), cn, tr)
                Next
            End If
            If modo = 1 Then
                objDaoProgPago_CXP.Insert(cadenaDocumentoReferencia, idRequerimientoCadena, objProgramacionPago_CXP, idEsDetraccion, idEsRetencion, _
                          idEsPercepcion, cn, tr)
            ElseIf modo = 2 Then
                'elimina las programaciones
                objDaoProgPago_CXP.ProgramacionAlModificar(idRequerimientoCadena, cn, tr, objProgramacionPago_CXP, cadenaDocumentoReferencia, idRequerimientoCadena)
            End If
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
    End Sub


    Public Sub InsertParcial(ByVal cadenaDocumentoReferencia As String, ByVal idRequerimientoCadena As String, ByVal objProgramacionPago_CXP As Entidades.ProgramacionPago_CXP, _
                      ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal listaRelacionDocumento_LQ As List(Of Entidades.RelacionDocumento), ByVal idEsDetraccion As Boolean, ByVal idEsRetencion As Boolean, _
                      ByVal idEsPercepcion As Boolean, ByVal modo As Integer, ByVal idRegimen As Integer, _
                      Optional ByVal listaDocumentosAplicacionesContraDeudas As List(Of Entidades.be_documentoAplicacionDeudasPendientes) = Nothing, _
                      Optional ByVal listaTipoAgente As List(Of Entidades.TipoAgente) = Nothing)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        tr = cn.BeginTransaction
        Try
            Dim porcent As Decimal = 0
            If (listaRelacionDocumento IsNot Nothing) Then
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                If Not IsNothing(listaTipoAgente) Then
                    If listaTipoAgente.Count > 0 Then
                        porcent = CDec(listaTipoAgente(0).Tasa)
                    End If
                End If
                '  qweqwe()
                For i As Integer = 0 To listaRelacionDocumento.Count - 1

                    objDaoRelacionDocumento.InsertaRelacionDocRegimenParcial(listaRelacionDocumento(i), cn, tr, idRegimen, porcent, idRequerimientoCadena)

                Next

                'modif LQ
                For i As Integer = 0 To listaRelacionDocumento_LQ.Count - 1

                    objDaoRelacionDocumento.InsertaRelacionDocRegimenParcial_LQ(listaRelacionDocumento_LQ(i), cn, tr, idRegimen, porcent, idRequerimientoCadena)

                Next


                'end modif LQ

            End If





            If (listaDocumentosAplicacionesContraDeudas IsNot Nothing) Then
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                For i As Integer = 0 To listaDocumentosAplicacionesContraDeudas.Count - 1
                    objDaoRelacionDocumento.InsertaRelacionAplicacionContraDeudas(listaDocumentosAplicacionesContraDeudas(i), cn, tr)
                Next
            End If
            If modo = 1 Then
                objDaoProgPago_CXP.InsertParcial(cadenaDocumentoReferencia, idRequerimientoCadena, objProgramacionPago_CXP, idEsDetraccion, idEsRetencion, _
                          idEsPercepcion, cn, tr)
            ElseIf modo = 2 Then
                'elimina las programaciones
                objDaoProgPago_CXP.ProgramacionAlModificar(idRequerimientoCadena, cn, tr, objProgramacionPago_CXP, cadenaDocumentoReferencia, idRequerimientoCadena)
            End If
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
    End Sub


    'Public Sub Insert(ByVal cadenaIdRequerimiento As String, ByVal IdUsuario As Integer, ByVal FechaPagoProg As String)As Entidades.be_programacionPagos
    '    cn = (New DAO.Conexion).ConexionSIGE
    '    cn.Open()
    '    tr = cn.BeginTransaction
    '    Try

    '    Catch ex As Exception
    '        tr.Rollback()
    '        Throw ex
    '    Finally
    '        If (cn.State = ConnectionState.Open) Then cn.Close()
    '    End Try
    'End Sub

    Public Sub Update(ByVal objProgramacionPago_CXP As Entidades.ProgramacionPago_CXP)
        objDaoProgPago_CXP.Update(objProgramacionPago_CXP)
    End Sub

    Public Function SelectxIdMovCuentaCXP(ByVal IdRequerimiento As Integer) As Entidades.be_programacionPagos
        Return objDaoProgPago_CXP.SelectxIdMovCuentaCXP(IdRequerimiento)
    End Function

    Public Function DocumentoProgramacion_SelectAprobacion(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal MontoTot As Decimal) As DataTable
        Return objDaoProgPago_CXP.DocumentoProgramacion_SelectAprobacion(IdEmpresa, IdTienda, IdArea, Opcion_Aprobacion, FechaInicio, FechaFin, MontoTot)
    End Function


    Public Function SelectxIdMovCuentaCXPTexto(ByVal IdRequerimiento As String) As Entidades.be_programacionPagos
        Return objDaoProgPago_CXP.SelectxIdMovCuentaCXPTexto(IdRequerimiento)
    End Function

    Public Function SelectxIdRetenciones(ByVal cadenaIdRequerimiento As String, ByVal IdUsuario As Integer, ByVal FechaPagoProg As String) As Entidades.be_programacionPagos
        Return objDaoProgPago_CXP.SelectxIdRetenciones(cadenaIdRequerimiento, IdUsuario, FechaPagoProg)
    End Function

    Public Function InsertDocumentosSolucont(ByVal IdUsuario As Integer, ByVal FechaPagoProg As String, ByVal cadenaIdRequerimiento As String) As Entidades.be_programacionPagos
        Return objDaoProgPago_CXP.InsertDocumentosSolucont(IdUsuario, FechaPagoProg, cadenaIdRequerimiento)
    End Function


    Public Function SelectxIdMovCuentaCXP2(ByVal IdMovCuentaCXP As Integer) As List(Of Entidades.ProgramacionPago_CXP)
        Return objDaoProgPago_CXP.SelectxIdMovCuentaCXP2(IdMovCuentaCXP)
    End Function

    Public Function DeletexIdProgramacionPagoCXP(ByVal cadenaIdRequerimiento As String) As Boolean
        Dim retornar As Boolean = False
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            retornar = objDaoProgPago_CXP.DeletexIdProgramacionPagoCXP(cadenaIdRequerimiento, cn, tr)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return retornar
    End Function

    Public Function buscarfacturasPorAplicar(ByVal objeto As Entidades.be_Requerimiento_x_pagar) As List(Of Entidades.be_Requerimiento_x_pagar)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAOProgramacionPago_CXP).buscarfacturasPorAplicar(cn, objeto)
    End Function

    Public Function crearAsientoContableProgramacion(ByVal idDocumento As Integer, ByVal idProgramacion As Integer, ByVal idUsuario As Integer) As String
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_Programaciones).crearAsientoContableProgramacion(cn, idDocumento, idProgramacion, idUsuario)
    End Function
End Class

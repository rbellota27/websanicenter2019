﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class PerfilTipoPV
    Dim obj As New DAO.DAOPerfilTipoPV
    'Edita la lista PerfilTipoPV
    Public Function _PerfilTipoPVselectxIdPerfil(ByVal idPerfil As Integer) As List(Of Entidades.PerfilTipoPV)
        Return obj._PerfilTipoPVEditarxIdPerfil(idPerfil)
    End Function
End Class
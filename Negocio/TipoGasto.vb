﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************  JUEVES 11 FEB 2010 HORA 10_00 AM




'*************** LUNES 18 ENERO 2010 HORA 11_51 AM



'********************************************************
'Autor      : Chang Carnero, Edgar
'Módulo     : Concepto
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 13-Enero-2010
'Hora       : 01:00:00 pm
'********************************************************
Public Class TipoGasto
    Private objDaoTipoGasto As New DAO.DAOTipoGasto
    Public Function SelectAllActivo() As List(Of Entidades.TipoGasto)
        Return objDaoTipoGasto.SelectAllActivo
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoGasto)
        Return objDaoTipoGasto.SelectAll
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoGasto)
        Return objDaoTipoGasto.SelectAllInactivo
    End Function
    Public Function InsertaTipoGasto(ByVal Estilo As Entidades.TipoGasto) As Boolean
        Return objDaoTipoGasto.InsertaTipoGasto(Estilo)
    End Function
    Public Function ActualizaTipoGasto(ByVal estilo As Entidades.TipoGasto) As Boolean
        Return objDaoTipoGasto.ActualizaTipoGasto(estilo)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoGasto)
        Return objDaoTipoGasto.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoGasto)
        Return objDaoTipoGasto.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoGasto)
        Return objDaoTipoGasto.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoGasto)
        Return objDaoTipoGasto.SelectxId(id)
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoGasto)
        Return objDaoTipoGasto.SelectCbo
    End Function
End Class

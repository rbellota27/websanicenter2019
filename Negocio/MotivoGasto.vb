﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************************************************
'Autor      : Chigne Bazan, Dany
'Módulo     : MotivoGasto
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 17-Febrero-2010
'Hora       : 06:00:00 pm
'********************************************************
Public Class MotivoGasto
    Private objDAOMotivoGasto As New DAO.DAOMotivoGasto
    Private obj As New DAO.DAOMotivoGasto
    'Private objConexion As New DAO.Conexion


    Public Function InsertaMotivoGasto(ByVal MotivoGasto As Entidades.MotivoGasto) As Boolean
        Return objDAOMotivoGasto.InsertaMotivoGasto(MotivoGasto)
    End Function
    Public Function ActualizaMotivoGasto(ByVal MotivoGasto As Entidades.MotivoGasto) As Boolean
        Return objDAOMotivoGasto.ActualizaMotivoGasto(MotivoGasto)
    End Function

    Public Function SelectxConceptoxMotivoGasto(ByVal IdConcepto As Integer) As List(Of Entidades.MotivoGasto)
        Return obj.SelectxConceptoxMotivoGasto(IdConcepto)
    End Function

    Public Function SelectAll() As List(Of Entidades.MotivoGasto)
        Return obj.SelectAllMotivoGasto
    End Function
    Public Function SelectAllActivoMotivoGasto() As List(Of Entidades.MotivoGasto)
        Return obj.SelectAllActivoMotivoGasto
    End Function
    Public Function SelectAllInactivoMotivoGasto() As List(Of Entidades.MotivoGasto)
        Return obj.SelectAllInactivoMotivoGasto
    End Function

    Public Function SelectAllxNombre(ByVal Nombre As String) As List(Of Entidades.MotivoGasto)
        Return obj.SelectAllxNombre(Nombre)
    End Function
    Public Function SelectAllxNombreInactivo(ByVal Nombre As String) As List(Of Entidades.MotivoGasto)
        Return obj.SelectAllxNombreInactivo(Nombre)
    End Function
    Public Function SelectAllxNombreActivo(ByVal Nombre As String) As List(Of Entidades.MotivoGasto)
        Return obj.SelectAllxNombreActivo(Nombre)
    End Function
    Public Function SelectxId(ByVal Codigo As Integer) As List(Of Entidades.MotivoGasto)
        Return obj.SelectxId(Codigo)
    End Function

End Class

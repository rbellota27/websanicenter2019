﻿Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class Modulo
    Dim objDAOModulo As New DAO.DAOModulo
    Dim objConexion As DAO.Conexion
    Public Function SelectModulo(ByVal var As String) As List(Of Entidades.Modulo)
        Return objDAOModulo.SelectModulo(var)
    End Function

    Public Function InsertaModulo(ByVal objModulo As Entidades.Modulo)
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdModulo As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            IdModulo = objDAOModulo.InsertaModulo(objModulo, cn, "i", tr)

            If IdModulo = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If
          

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function SelectxIdModulo(ByVal idModulo As Integer) As List(Of Entidades.Modulo)
        Return objDAOModulo.SelectxIdModulo(idModulo)
    End Function


    Public Function ActualizaModulo(ByVal objModulo As Entidades.Modulo) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If Not objDAOModulo.ActualizaModulo(objModulo, cn, "a", tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function


    Public Function EliminarIdModulo(ByVal idModulo As Integer) As List(Of Entidades.Modulo)
        Return objDAOModulo.EliminaIdModulo(idModulo)
    End Function
End Class

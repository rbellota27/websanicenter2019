﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*********************   VIERNES 23 ABRIL 2010

Public Class DetalleDocumento
    Private obj As New DAO.DAODetalleDocumento


    Public Function fn_TotalPag_DetalleDocumento(ByVal IdDocumento As Integer, ByVal PageSize As Integer) As Integer
        Return obj.fn_TotalPag_DetalleDocumento(IdDocumento, PageSize)
    End Function


    Public Function SelectxIdFact_NC(ByVal IdFactura As Integer, ByVal IdGuiaRecepcion As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.SelectxIdFact_NC(IdFactura, IdGuiaRecepcion)
    End Function

    Public Function SelectComponenteKitxIdKitxIdDocumento(ByVal IdKit As Integer, ByVal IdDocumento As Integer) As List(Of Entidades.Kit)
        Return obj.SelectComponenteKitxIdKitxIdDocumento(IdKit, IdDocumento)
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.SelectxIdDocumento(IdDocumento)
    End Function
    Public Function SelectxIdDocumentoValidacionTransito(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.SelectxIdDocumentoValidacionTransito(IdDocumento)
    End Function

    Public Function DetalleDocumentoRefxGuiaRecep(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.DetalleDocumentoRefxGuiaRecep(IdDocumento)
    End Function

    Public Function SelectCantxAtenderNoCeroxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.SelectCantxAtenderNoCeroxIdDocumento(IdDocumento)
    End Function
    Public Function SelectxIdSubLineaxIdDocumento(ByVal idsublinea As Integer, ByVal iddocumento As Integer, ByVal Prod_Codigo As String, _
                                                         ByVal Prod_Nombre As String, ByVal Tabla As DataTable) As List(Of Entidades.DetalleDocumentoView)
        Return obj.SelectxIdSubLineaxIdDocumento(idsublinea, iddocumento, Prod_Codigo, Prod_Nombre, Tabla)
    End Function
    Public Function DetalleDocumentoViewSelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)
        Return obj.DetalleDocumentoViewSelectxIdDocumento(IdDocumento)
    End Function

    Public Function DetalleDocumentoSelectDetallexAtenderxIdDocumento(ByVal IdDocumento As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.DetalleDocumentoSelectDetallexAtenderxIdDocumento(IdDocumento, IdTipoOperacion)
    End Function

    Public Function DetalleDocumentoSelectDetallexAtenderxIdDocumentoDetalle(ByVal IdDocumento As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.DetalleDocumentoSelectDetallexAtenderxIdDocumentoDetalle(IdDocumento, IdTipoOperacion)
    End Function

    Public Function BuscaProdcompuesto(ByVal IdDocumento As Integer) As Boolean

        'Return obj.BuscaProdcompuesto(IdDocumento)

    End Function
    Public Function CambiarProdcompuesto(ByVal IdDocumento As Integer) As Boolean

        Return obj.CambiarProdcompuesto(IdDocumento)

    End Function
    Public Function BuscaCheckNotCred(ByVal IdDocumento As Integer) As Boolean
        Return obj.BuscaCheckNotCred(IdDocumento)
    End Function
End Class

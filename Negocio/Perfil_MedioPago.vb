﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'********************  MIERCOLES 07 ABRIL 2010 HORA 11_13 AM


Public Class Perfil_MedioPago


    Dim obj As New DAO.DAOPerfil_MedioPago

    Public Function Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(ByVal IdUsuario As Integer, ByVal IdMedioPago As Integer) As Integer
        Return obj.Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(IdUsuario, IdMedioPago)
    End Function
    Public Function SelectxIdPerfil(ByVal IdPerfil As Integer) As List(Of Entidades.Perfil_MedioPago)
        Return obj.SelectxIdPerfil(IdPerfil)
    End Function

End Class
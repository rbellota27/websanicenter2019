﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class DocImportacion
    Private objConexion As DAO.Conexion
    Private obj As New DAO.DAODocImportacion
    Public Function OrdenCompraSelectNroOCxIdProveedor(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, _
                                                       ByVal Serie As Integer, ByVal Correlativo As Integer, _
                                                       ByVal IdProveedor As Integer, ByVal pageindex As Integer, _
                                                       ByVal pagesize As Integer) As List(Of Entidades.DocImportacion)
        Return obj.OrdenCompraSelectNroOCxIdProveedor(IdEmpresa, IdTienda, Serie, Correlativo, IdProveedor, pageindex, pagesize)
    End Function
    'Public Function OrdenCompraSelectIdOC(ByVal IdDoc As Integer) As List(Of Entidades.DocImportacion)
    '    Return obj.OrdenCompraSelectIdOC(IdDoc)
    'End Function
    Public Function SelectCostosSinPasarxIdDocumento(ByVal IdDocumento As Integer) As Integer
        Return obj.SelectCostosSinPasarxIdDocumento(IdDocumento)
    End Function
    Public Function DetalleGastoSelectxIdConcepto(ByVal IdConcepto As Integer) As List(Of Entidades.DocImportacion)
        Return obj.DetalleGastoSelectxIdConcepto(IdConcepto)
    End Function
    Public Function SelectAllTipoDocumento() As List(Of Entidades.DocImportacion)
        Return obj.SelectAllTipoDocumento()
    End Function
    Public Function InsertaConfiguracionImportacion(ByVal objimportacion As Entidades.DocImportacion, ByVal UpdPrecioOC As Boolean, ByVal ldetalleimportacion As List(Of Entidades.ImportacionDetalle), ByVal objAnexoDoc As Entidades.Anexo_Documento, ByVal lDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objRelacDoc As Entidades.RelacionDocumento) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdDocumento As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            IdDocumento = obj.InsertImportacionT(cn, objimportacion, tr)
            If IdDocumento = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If

            Dim daoDetalleImportacion As New DAO.DAOImportacionDetalle
            If Not daoDetalleImportacion.GrabarDetalleImportacionT(cn, IdDocumento, UpdPrecioOC, ldetalleimportacion, tr) Then
                'Throw New Exception("No se actualizó los precios de costeo de la Orden de Compra.")
            End If
            Dim daoAnexoDoc As New DAO.DAOAnexo_Documento
            If Not daoAnexoDoc.InsertAnexoDocT(cn, IdDocumento, objAnexoDoc, tr) Then
                'Throw New Exception("Fracasó el Proceso de Inserción")
            End If
            Dim daoDetalleConcepto As New DAO.DAODetalleConcepto
            If Not daoDetalleConcepto.GrabarDetalleConceptoT(cn, IdDocumento, lDetalleConcepto, tr) Then
                'Throw New Exception("Fracasó el Proceso de Inserción")
            End If
            Dim daoRelacDoc As New DAO.DAORelacionDocumento
            If Not daoRelacDoc.GrabaRelacionDocumentoT(cn, IdDocumento, objRelacDoc, tr) Then
                'Throw New Exception("Fracasó el Proceso de Inserción")
            End If

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function ActualizaConfiguracionImportacion(ByVal vIdDocumento As Integer, ByVal UpdPrecioOC As Boolean, ByVal objimportacion As Entidades.DocImportacion, ByVal ldetalleimportacion As List(Of Entidades.ImportacionDetalle), ByVal objAnexoDoc As Entidades.Anexo_Documento, ByVal lDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objRelacDoc As Entidades.RelacionDocumento) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdDocumento As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If Not obj.ActualizaImportacionT(objimportacion, cn, tr) Then
                'Throw New Exception("Fracasó el proceso de Inserción.")
            End If

            Dim daoDetalleImportacion As New DAO.DAOImportacionDetalle
            If Not daoDetalleImportacion.GrabarDetalleImportacionT(cn, vIdDocumento, UpdPrecioOC, ldetalleimportacion, tr) Then
                'Throw New Exception("Fracasó el Proceso de Inserción")
            End If
            Dim daoAnexoDoc As New DAO.DAOAnexo_Documento
            If Not daoAnexoDoc.ActualizaAnexoDocT(cn, vIdDocumento, objAnexoDoc, tr) Then
                'Throw New Exception("Fracasó el Proceso de Inserción")
            End If
            Dim daoDetalleConcepto As New DAO.DAODetalleConcepto
            If Not daoDetalleConcepto.GrabarDetalleConceptoT(cn, vIdDocumento, lDetalleConcepto, tr) Then
                'Throw New Exception("Fracasó el Proceso de Inserción")
            End If
            Dim daoRelacDoc As New DAO.DAORelacionDocumento
            If Not daoRelacDoc.GrabaRelacionDocumentoT(cn, vIdDocumento, objRelacDoc, tr) Then
                'Throw New Exception("Fracasó el Proceso de Inserción")
            End If

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    'Public Function ImportacionSelectId(ByVal NroImportacion As String) As Entidades.DocImportacion
    '    Return obj.ImportacionSelectId(NroImportacion)
    'End Function
    Public Function ImportacionSelectId(ByVal IdDocumento As Integer) As Entidades.DocImportacion
        Return obj.ImportacionSelectId(IdDocumento)
    End Function
    Public Function DetalleGastoImportacionSelectxId(ByVal IdDocumento As Integer) As List(Of Entidades.DocImportacion)
        Return obj.DetalleGastoImportacionSelectxId(IdDocumento)
    End Function
    Public Function AnularImportacion(ByVal objImportacion As Entidades.DocImportacion) As Boolean
        Return obj.AnularImportacion(objImportacion)
    End Function
    Public Function ImportacionSelectAllxSeriexCodigoxProveedor(ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdProveedor As Integer) As List(Of Entidades.DocImportacion)
        Return obj.ImportacionSelectAllxSeriexCodigoxProveedor(Serie, Codigo, IdProveedor)
    End Function

    Public Function ImportacionProyectadoSelectAllxSeriexCodigoxProveedor(ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdProveedor As Integer) As List(Of Entidades.DocImportacion)
        Return obj.ImportacionProyectadoSelectAllxSeriexCodigoxProveedor(Serie, Codigo, IdProveedor)
    End Function


End Class

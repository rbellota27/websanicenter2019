﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data
Imports System.Data.SqlClient
Public Class MagnitudUnidad
    Private obj As New DAO.DAOMagnitudUnidad

    Public Function SelectCboUM(ByVal IdMagnitud As Integer) As List(Of Entidades.UnidadMedida)
        Return obj.SelectCboUM(IdMagnitud)
    End Function
    Public Function ActualizaMagnitudUnidad(ByVal UpdateMU As List(Of Entidades.MagnitudUnidad)) As Boolean
        Return obj.ActualizaMagnitudUnidad(UpdateMU)
    End Function
    'Edita la lista Magnitud Unidades
    Public Function _MagnitudUnidadselectxIdMagnitud(ByVal idmagnitud As Integer) As List(Of Entidades.MagnitudUnidad)
        Return obj._MagnitudUnidadselectxIdMagnitud(idmagnitud)
    End Function
    Public Function DeletexIdMagnitud(ByVal IdMagnitud As Integer, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        'Public Function DeletexIdMagnitud(ByVal idMagnitud As Integer) As List(Of Entidades.MagnitudUnidad)
        Return obj.DeletexIdMagnitud(cn, tr, IdMagnitud)
    End Function
End Class

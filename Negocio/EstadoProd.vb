﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class EstadoProd
    Private objDaoEstadoP As New DAO.DAOEstadoProd
    Public Function InsertaEstadoProd(ByVal estadoprod As Entidades.EstadoProd) As Boolean
        Return objDaoEstadoP.InsertaEstadoProd(estadoprod)
    End Function
    Public Function ActualizaEstadoProd(ByVal estadoprod As Entidades.EstadoProd) As Boolean
        Return objDaoEstadoP.ActualizaEstadoProd(estadoprod)
    End Function
    Public Function SelectAll() As List(Of Entidades.EstadoProd)
        Return objDaoEstadoP.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.EstadoProd)
        Return objDaoEstadoP.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.EstadoProd)
        Return objDaoEstadoP.SelectAllInactivo
    End Function

    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.EstadoProd)
        Return objDaoEstadoP.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.EstadoProd)
        Return objDaoEstadoP.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.EstadoProd)
        Return objDaoEstadoP.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.EstadoProd)
        Return objDaoEstadoP.SelectxId(id)
    End Function

End Class

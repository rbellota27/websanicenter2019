﻿Imports Entidades
Imports DAO
Imports System.Data.SqlClient
Public Class bl_pagoProveedor
    Private cn As SqlConnection = Nothing
    Dim tr As SqlTransaction = Nothing
    Public Function form_listarPagosProgramados(ByVal IdMovCtaPP As Integer, ByVal fechaPagoProgramado As Date) As be_frmCargaPrincipal
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_Programaciones).form_listarPagosProgramados(cn, IdMovCtaPP, fechaPagoProgramado)
    End Function

    Public Function filtrarListaProgramaciones(ByVal fecInicio As Date, ByVal fecFin As Date, ByVal idBanco As Integer, _
                                               ByVal serie As Integer, ByVal codigo As Integer, ByVal flag As Integer) As List(Of be_cancelacionDocumentos)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Programaciones).filtrarListaProgramaciones(cn, fecInicio, fecFin, idBanco, serie, codigo, flag)
    End Function

    Public Function listarDeudasPendientesxCancelacion(ByVal idCadenaRequerimiento As String, ByVal esDetraccion As Boolean, ByVal idProgramacion As Integer) As Entidades.be_cancelacionDocumentos
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Programaciones).listarPagosxCancelar(cn, idCadenaRequerimiento, esDetraccion, idProgramacion)
    End Function

    Public Function listarMediodePagos(ByVal idDocumentopago As Integer) As List(Of Entidades.DocumentoCheque)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Programaciones).listarMediodePagos(cn, idDocumentopago)
    End Function

    Public Function llenarComboCuentaBancariaDetraccionPorProveedor(ByVal idProveedor As Integer) As List(Of be_ComboGenerico)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Programaciones).llenarComboCuentaBancariaDetraccionPorProveedor(cn, idProveedor)
    End Function

    Public Sub crearCuentaDetraccionxProveedor(idBanco As Integer, idPersona As Integer, cb_numero As String, cuentaContable As String,
                                              cb_cuentaInterbancaria As String, cb_swiftbancario As String, idMoneda As Integer)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Dim dato As New DAO_Programaciones
        dato.crearBancoDetraccionxProveedor(cn, idBanco, idPersona, cb_numero, cuentaContable, cb_cuentaInterbancaria, cb_swiftbancario, idMoneda)
    End Sub

    Public Function buscarTipoCambioCompra(ByVal fechaPago As Date)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Programaciones).buscarTipoCambioCompra(cn, fechaPago)
    End Function

    Public Function CrearDocumentoExternoDetraccion(ByVal idRequerimiento As String, ByVal esDetraccion As Boolean, ByVal documento As Entidades.Documento)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        tr = cn.BeginTransaction(IsolationLevel.Serializable)
        If esDetraccion Then
            documento.strIdRequerimientoCadena = idRequerimiento
            Dim lista As New List(Of Entidades.Documento)
            Try
                Dim objeto As New DAO.DAO_Programaciones
                lista = objeto.registraDetraccionComodocExterno(cn, documento, tr)
                tr.Commit()
            Catch ex As Exception
                tr.Rollback()
            End Try
            Return lista
        End If
    End Function

    Public Function BuscaDocumentoDetraccion(ByVal idRequerimiento As String)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction

        Try
            Return (New DAO_Programaciones).BuscaDetraccionxProgramacion(cn, tr, idRequerimiento)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            cn.Close()
        End Try
    End Function

    Public Sub DeleteRegistroDetraccion(ByVal idDocumento As Integer)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim objeto As New DAO_Programaciones
            objeto.DeleteRegistroDetraccion(cn, idDocumento)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function buscarRelacionConDocumentoRetencion(ByVal cadenaDocumentosRef As String, ByVal tc As Decimal) As List(Of be_cancelacionDocumentos)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim objeto As New DAO_Programaciones
            Return objeto.buscarRelacionConDocumentoRetencion(cn, cadenaDocumentosRef, tc)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function listarConceptoAdelantos(flag As String) As List(Of Entidades.Concepto)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Return (New DAO_Programaciones).ListarConceptoAdelantos(cn, flag)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListarcuentasPorRendir(flag As String) As List(Of Entidades.Concepto)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Return (New DAO_Programaciones).ListarcuentasPorRendir(cn, flag)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function listarPagoProveedor(flag As String) As List(Of Entidades.Concepto)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Return (New DAO_Programaciones).ListarPagoProveedor(cn, flag)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Sub CancelarProgramacion(ByVal idProgramacion As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
                                ByVal idbanco As Integer, ByVal fechaPago As Date, ByVal idPersona As Integer)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim obj As New DAO_Programaciones
            obj.CancelarProgramacion(cn, idProgramacion, nroOperacion, montoDeposito, idbanco, fechaPago, idPersona)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Public Sub CancelarProgramacionMasivos(ByVal fechainicio As Date, ByVal fechafin As Date, ByVal idProgramacion As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
    '                          ByVal idbanco As Integer, ByVal fechaPago As Date, ByVal idPersona As Integer, ByVal idUsuario As Integer)
    '    cn = (New Conexion).ConexionSIGE
    '    cn.Open()
    '    Try
    '        Dim obj As New DAO_Programaciones
    '        obj.CancelarProgramacionMasivos(cn, fechainicio, fechafin, idProgramacion, nroOperacion, montoDeposito, idbanco, fechaPago, idPersona, idUsuario)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub


    Public Function CancelarProgramacionMasivos(ByVal fechainicio As Date, ByVal fechafin As Date, ByVal idProgramacion As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
                              ByVal idbanco As Integer, ByVal fechaPago As Date, ByVal idPersona As Integer, ByVal idUsuario As Integer) As String
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_Programaciones).CancelarProgramacionMasivos(cn, fechainicio, fechafin, idProgramacion, nroOperacion, montoDeposito, idbanco, fechaPago, idPersona, idUsuario)

    End Function

   
    Public Function CancelarDetraccionesLotes(ByVal lote As Integer, ByVal Periodo As Integer, ByVal idbanco As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
                              ByVal FechaCancelacion As Date, ByVal idUsuario As Integer) As String

        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_Programaciones).CancelarDetraccionesLotes(cn, lote, Periodo, idbanco, nroOperacion, montoDeposito, FechaCancelacion, idUsuario)

    End Function

    Public Sub CancelarProgramacionDetraccion(ByVal idDocumento As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
                                ByVal idbanco As Integer, ByVal fechaPago As Date)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim obj As New DAO_Programaciones
            obj.CancelarProgramacionDetraccion(cn, idDocumento, nroOperacion, montoDeposito, idbanco, fechaPago)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function listarDeudasPendientes(ByVal fechaInicio As String, ByVal fechaFinal As String, ByVal estadoPRovision As Integer _
                                           , ByVal proveedor As String, ByVal nroDocumento As String) As List(Of Entidades.Programacion)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Return (New DAO_Programaciones).listarDeudasPendientes(cn, fechaInicio, fechaFinal, estadoPRovision, proveedor, nroDocumento)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function validaSiGuiaFueProgramada(ByVal idDocumento As Integer)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Programaciones).validaSiGuiaFueProgramada(cn, idDocumento)
    End Function
End Class

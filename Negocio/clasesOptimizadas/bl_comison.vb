﻿Imports Entidades
Imports DAO
Imports System.Data.SqlClient
Public Class bl_comison
    Dim cn As SqlConnection = Nothing
    Public Function comisionxSucursal(ByVal fechaInicio As String, ByVal fechaFinal As String) As List(Of be_comision)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Comision).comisionxSucursal(cn, fechaInicio, fechaFinal)
    End Function
End Class

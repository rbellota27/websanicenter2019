﻿Imports System.Data
Imports System.Data.SqlClient

Public Class MontoRegimenDet

    Dim daoMontoRegimenDet As New DAO.DAOMontoRegimenDet
    Dim objConexion As New DAO.Conexion

    Public Function InsertMontoRegimenDet(ByVal objMontoRegimenDet As Entidades.be_MontoRegimenDet) As Boolean
        '************ 
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            Dim objDaoCta As New DAO.DAOMontoRegimenDet
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '************** 
            objDaoCta.InsertaMontoRegimenDet(cn, objMontoRegimenDet, tr)

            'objDaoCta.InsertaCuentaBancariaDetraccion(objCta, cn, tr, esCuentaDetraccion)

            bool = True
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            bool = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function


End Class

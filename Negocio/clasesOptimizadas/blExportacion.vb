﻿
Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class blExportacion
    Dim cn As SqlConnection
    Public Function listarDetraccionesAprobadas(ByVal fechaInicio As String, ByVal fechaFinal As String, _
                                        ByVal anio As String) As List(Of be_Exportacion)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_Exportacion)
            lista = (New DAO_Exportacion).listarDetraccionesAprobadas(cn, fechaInicio, fechaFinal, anio)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

    Public Function listarDetraccionesxLote(ByVal lote As String) As List(Of be_Exportacion)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_Exportacion)
            lista = (New DAO_Exportacion).listarDetraccionesxLote(cn, lote)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

    Public Function CierreLoteDetracciones(ByVal fechaInicio As String, ByVal fechaFinal As String, _
                                    ByVal lote As String) As List(Of be_Exportacion)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_Exportacion)
            lista = (New DAO_Exportacion).CierreLoteDetracciones(cn, fechaInicio, fechaFinal, lote)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

    Public Function GeneraLoteDetracciones(ByVal fechaInicio As String, ByVal fechaFinal As String, _
                                     ByVal anio As String) As List(Of be_Exportacion)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_Exportacion)
            lista = (New DAO_Exportacion).GeneraLoteDetracciones(cn, fechaInicio, fechaFinal, anio)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

End Class

﻿Imports System.Data.SqlClient
Imports DAO
Imports Entidades
Public Class bl_SolucontPase
    Dim cn As SqlConnection = Nothing
    Dim tr As SqlTransaction = Nothing

    Public Function importarDocProvisionados(ByVal fechaInicio As String, ByVal fechaFinal As String, _
                                             ByVal idEmpresa As String, ByVal anio As String, ByVal moneda As String)
        cn = (New Conexion).ConexionSOLUCONT
        cn.Open()
        Try
            Return (New DAO_SolucontPase).listaDocumentos_provisionados(cn, fechaInicio, fechaFinal, idEmpresa, anio, moneda)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function listaDocProvisionadosCorrectamente(ByVal fechaInicio As String, ByVal fechaFinal As String, _
                                             ByVal anio As String) As List(Of be_provision)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        ' tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Dim lista As New List(Of Entidades.be_provision)
            lista = (New DAO_SolucontPase).listaDocumentosPasadosCorrectamente(cn, fechaInicio, fechaFinal, anio)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
        End Try
    End Function
End Class

﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_LetrasxCobrar
    Dim cn As SqlConnection
    Public Function listarLetrasxCobrar(ByVal IdTienda As Integer, ByVal IdEstado As Integer, ByVal fechaInicio As String, ByVal FechaFinal As String) As List(Of be_LetrasxCobrar)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_LetrasxCobrar)
            lista = (New DAO_LetrasxCobrar).listarLetrasxCobrar(cn, IdTienda, IdEstado, fechaInicio, FechaFinal)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function



    Public Function listarLetrasxExportar(ByVal IdTienda As Integer, ByVal IdEstado As Integer, ByVal fechaInicio As String, ByVal FechaFinal As String) As List(Of ExportaLetras)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.ExportaLetras)
            lista = (New DAO_LetrasxCobrar).listarLetrasxExportar(cn, IdTienda, IdEstado, fechaInicio, FechaFinal)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

    Public Function InsertUpdateLetras(ByVal IdDocumento As Integer, IdVendedor As Integer, FechaEntregaVend As String, FechaEntregaLFirm As String, FechaEnvioBanco As String, FechaCancelacion As String, FechaProtesto As String, NroUnico As String, IdBanco As Integer, FechaPRogramado As String, FechaRenovacion As String, Usuario As Integer, CondicionLetra As Integer)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim obj As New DAO_LetrasxCobrar
            obj.InsertUpdateLetras(cn, IdDocumento, IdVendedor, FechaEntregaVend, FechaEntregaLFirm, FechaEnvioBanco, FechaCancelacion, FechaProtesto, NroUnico, IdBanco, FechaPRogramado, FechaRenovacion, Usuario, CondicionLetra)
            'tr.Commit()
            'Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

End Class

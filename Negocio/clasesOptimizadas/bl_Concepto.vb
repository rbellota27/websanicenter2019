﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_Concepto
    Dim cn As SqlConnection

    Public Function listarConcepto(ByVal IdTipoGasto As Integer) As List(Of be_Concepto)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_Concepto)
            lista = (New DAO_Concepto).listarConcepto(cn, IdTipoGasto)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

End Class

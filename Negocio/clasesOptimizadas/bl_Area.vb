﻿
Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_Area
    Dim cn As SqlConnection

    Public Function selectArea(ByVal idEmpresa As Integer) As List(Of be_Area)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_Area)
            lista = (New DAO_Area).selectArea(cn, idEmpresa)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

End Class



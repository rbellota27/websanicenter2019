﻿Imports Entidades
Imports DAO
Imports System.Data.SqlClient
Public Class bl_negocio
    Dim cn As SqlConnection = Nothing
    Public Function listarAprobarCotizacion(ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal idPersona As Integer, _
                                      ByVal fechaInicio As Date, ByVal fechaFin As Date, idAprobarCoti As String) As List(Of Entidades.DocumentoView)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Dim lista As New List(Of Entidades.DocumentoView)
        lista = (New DAOCotizacion).CotizacionesXAprobar(cn, idEmpresa, idTienda, idPersona, fechaInicio, fechaFin, idAprobarCoti)
        Return lista
    End Function
    Public Function aprobarCotizacion(idDocumento As Integer) As Boolean
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAOCotizacion).aprobarCotizacion(cn, idDocumento)
    End Function
End Class

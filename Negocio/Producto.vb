﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class Producto
    Private objDAOProducto As New DAO.DAOProducto
    Private objConexion As DAO.Conexion
    Public Function SelectProductoxCodigoxDescripcion(ByVal CodigoProducto As String, ByVal NombreProducto As String) As List(Of Entidades.ProductoView)
        Return objDAOProducto.SelectProductoxCodigoxDescripcion(CodigoProducto, NombreProducto)
    End Function
    Public Function fx_Get_CodProd_Lin_SubLin_AutoGen(ByVal IdSublinea As Integer, ByVal Longitud As Integer) As String
        Return objDAOProducto.fx_Get_CodProd_Lin_SubLin_AutoGen(IdSublinea, Longitud)
    End Function

    Public Function BuscarProducto_Paginado(ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdEstadoProd As Integer, ByVal Nombre As String, ByVal Codigo As String, ByVal CodigoProveedor As String, ByVal Tb As List(Of Entidades.ProductoTipoTablaValor), ByVal pageIndex As Integer, ByVal pageSize As Integer) As List(Of Entidades.GrillaProducto_M)
        Return objDAOProducto.BuscarProducto_Paginado(IdTipoExistencia, IdLinea, IdSubLinea, IdEstadoProd, Nombre, Codigo, CodigoProveedor, Tb, pageIndex, pageSize)
    End Function
    Public Function ReporteAlmacen(ByVal Linea As Integer, ByVal SubLinea As Integer, ByVal Fecha As String, ByVal Tienda As Integer, ByVal IdAlmacen As Integer) As List(Of Entidades.ReporteAlmacen)
        Return objDAOProducto.ReporteAlmacen(Linea, SubLinea, Fecha, Tienda, IdAlmacen)
    End Function

    Public Function BuscarProducto_PaginadoConsulta(ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal Nombre As String, ByVal Codigo As String, ByVal Tb As List(Of Entidades.ProductoTipoTablaValor), ByVal pageIndex As Integer, ByVal pageSize As Integer) As List(Of Entidades.GrillaProducto_M)
        Return objDAOProducto.BuscarProducto_PaginadoConsulta(IdTipoExistencia, IdLinea, Nombre, Codigo, Tb, pageIndex, pageSize)
    End Function





    Public Function fx_getValorEquivalenteProducto(ByVal IdProducto As Integer, ByVal IdUnidadMedida_Origen As Integer, ByVal IdUnidadMedida_Destino As Integer, ByVal valor As Decimal) As Decimal
        Return objDAOProducto.fx_getValorEquivalenteProducto(IdProducto, IdUnidadMedida_Origen, IdUnidadMedida_Destino, valor)
    End Function
    Public Function Producto_ConsultaEquivalenciaProductoxParams(ByVal IdProducto As Integer, ByVal IdUnidadMedida_Entrada As Integer, ByVal IdUnidadMedida_Salida As Integer, ByVal Cantidad_Ingreso As Decimal, ByVal TablaUM As DataTable, ByVal utilizarRedondeoUp As Integer) As DataTable
        Return objDAOProducto.Producto_ConsultaEquivalenciaProductoxParams(IdProducto, IdUnidadMedida_Entrada, IdUnidadMedida_Salida, Cantidad_Ingreso, TablaUM, utilizarRedondeoUp)
    End Function
    Public Function ProductoSelectxIdProducto(ByVal IdProducto As Integer) As Entidades.ProductoView
        Return objDAOProducto.ProductoSelectxIdProducto(IdProducto)
    End Function
    Public Function InsertaProductoTipoMercaderia(idUsuario As Integer, ByVal objProd As Entidades.Producto, ByVal listaUM As List(Of Entidades.ProductoUMView), ByVal listaMagnitud As List(Of Entidades.ProductoMedida), ByVal listaProdTTV As List(Of Entidades.ProductoTipoTablaValor), ByVal listaKit As List(Of Entidades.Kit)) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdProducto As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            IdProducto = objDAOProducto.InsertaProductoTipoMercaderia(idUsuario, objProd, cn, tr)
            If IdProducto = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If


            Dim objDaoProdUM As New DAO.DAOProductoUM
            If Not objDaoProdUM.InsertaListaProductoUM(idUsuario, IdProducto, listaUM, cn, tr) Then
                Throw New Exception '("Fracasó el Proceso de Inserción")
            End If


            Dim objDaoProdMedida As New DAO.DAOProductoMedida
            Dim objProductoMedida As Entidades.ProductoMedida
            For Each objProductoMedida In listaMagnitud
                objProductoMedida.IdProducto = IdProducto
                objDaoProdMedida.InsertaProductoMedida(idUsuario, cn, tr, objProductoMedida)
            Next

            Dim objDaoProdTTV As New DAO.DAOProductoTipoTablaValor
            If Not objDaoProdTTV.DeleteProductoTipoTablaValor(IdProducto, cn, tr) Then
                Throw New Exception("Fracasó el Proceso de Inserción")
            End If

            If Not objDaoProdTTV.GrabaProductoTipoTablaValorT(IdProducto, cn, listaProdTTV, tr) Then
                Throw New Exception("Fracasó el Proceso de Inserción")
            End If

            Dim objDaoKit As New DAO.DAOKit
            For i As Integer = 0 To listaKit.Count - 1

                listaKit(i).IdKit = IdProducto
                objDaoKit.Kit_Registrar(listaKit(i), cn, tr)

            Next

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function ActualizaProductoTipoMercaderia(idusuario As Integer, ByVal objProd As Entidades.Producto, ByVal listaUM As List(Of Entidades.ProductoUMView), ByVal listaMagnitud As List(Of Entidades.ProductoMedida), ByVal listaProdTTV As List(Of Entidades.ProductoTipoTablaValor), ByVal listaKit As List(Of Entidades.Kit)) As Boolean

        '********************* ANÁLISIS DE KIT
        Dim listaKit_Aux As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponentexIdKit(objProd.Id)
        For x As Integer = listaKit_Aux.Count - 1 To 0 Step -1

            For y As Integer = listaKit.Count - 1 To 0 Step -1

                If (listaKit_Aux(x).IdComponente = listaKit(y).IdComponente) Then

                    listaKit_Aux.RemoveAt(x)
                    Exit For

                End If

            Next

        Next

        '*********** obtengo los item a eliminar
        Dim objProductoUMView As New Negocio.ProductoUMView
        Dim listaUM_Editar As List(Of Entidades.ProductoUMView) = objProductoUMView.SelectxIdProducto(objProd.Id)
        Dim i As Integer = listaUM_Editar.Count - 1
        While i >= 0
            Dim j As Integer = listaUM.Count - 1
            While j >= 0
                If listaUM_Editar.Item(i).IdUnidadMedida = listaUM.Item(j).IdUnidadMedida Then
                    listaUM_Editar.RemoveAt(i)
                    Exit While
                End If
                j = j - 1
            End While
            i = i - 1
        End While

        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            If Not objDAOProducto.ActualizaProductoTipoMercaderia(idusuario, objProd, cn, tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If
            Dim objDaoProdUM As New DAO.DAOProductoUM

            For x As Integer = 0 To listaUM_Editar.Count - 1
                With listaUM_Editar.Item(x)
                    objDaoProdUM.DeletexIdProductoxIdUnidadMedida(.IdProducto, .IdUnidadMedida, cn, tr)
                End With
            Next

            If Not objDaoProdUM.InsertaListaProductoUM(idusuario, objProd.Id, listaUM, cn, tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If


            Dim objDaoProdMedida As New DAO.DAOProductoMedida
            Dim objProductoMedida As Entidades.ProductoMedida
            If Not objDaoProdMedida.DeletexIdProducto(objProd.Id, cn, tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If
            For Each objProductoMedida In listaMagnitud
                objProductoMedida.IdProducto = objProd.Id
                objDaoProdMedida.InsertaProductoMedida(idusuario, cn, tr, objProductoMedida)
            Next

            Dim objDaoProdTTV As New DAO.DAOProductoTipoTablaValor
            If Not objDaoProdTTV.DeleteProductoTipoTablaValor(objProd.Id, cn, tr) Then
                Throw New Exception("Fracasó el Proceso de Inserción")
            End If

            If Not objDaoProdTTV.GrabaProductoTipoTablaValorT(objProd.Id, cn, listaProdTTV, tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If

            Dim objDaoKit As New DAO.DAOKit
            For x As Integer = 0 To listaKit.Count - 1
                listaKit(x).IdKit = objProd.Id
                objDaoKit.Kit_Registrar(listaKit(x), cn, tr)
            Next

            For y As Integer = 0 To listaKit_Aux.Count - 1
                objDaoKit.Kit_DeletexIdKitxIdComponente(objProd.Id, listaKit_Aux(y).IdComponente, cn, tr)
            Next

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function ProductoMercaderiaSelectxIdProducto(ByVal IdProducto As Integer) As Entidades.Producto
        Return objDAOProducto.ProductoMercaderiaSelectxIdProducto(IdProducto)
    End Function
    Public Function InsertaProducto(ByVal producto As Entidades.Producto) As Boolean
        Return objDAOProducto.InsertaProducto(producto)
    End Function
    Public Function ActualizaProducto(ByVal producto As Entidades.Producto) As Boolean
        Return objDAOProducto.ActualizaProducto(producto)
    End Function
    Public Function SelectAll() As List(Of Entidades.Producto)
        Return objDAOProducto.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Producto)
        Return objDAOProducto.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Producto)
        Return objDAOProducto.SelectAllInactivo
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectxId(id)
    End Function
    Public Function SelectAllxLinea(ByVal idLinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectAllxLinea(idLinea)
    End Function
    Public Function SelectActivoxLinea(ByVal idLinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectActivoxLinea(idLinea)
    End Function
    Public Function SelectInactivoxLinea(ByVal idLinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectInactivoxLinea(idLinea)
    End Function
    Public Function SelectAllxSubLinea(ByVal idSubLinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectAllxSubLinea(idSubLinea)
    End Function
    Public Function SelectActivoxSubLinea(ByVal idSublinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectActivoxSubLinea(idSublinea)
    End Function
    Public Function SelectInactivoxSubLinea(ByVal idSubLinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectInactivoxSubLinea(idSubLinea)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectAllxIdProveedor(ByVal idProveedor As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectAllxIdProveedor(idProveedor)
    End Function
    Public Function SelectActivoxIdProveedor(ByVal idProveedor As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectActivoxIdProveedor(idProveedor)
    End Function
    Public Function SelectInactivoxIdProveedor(ByVal idProveedor As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectInactivoxIdProveedor(idProveedor)
    End Function
    Public Function SelectGrillaProd_M(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                       ByVal IdProveedor As Integer, ByVal IdEstadoProd As Integer, _
                                       ByVal Nombre As String) As List(Of Entidades.GrillaProducto_M)
        Return objDAOProducto.SelectGrillaProd_M(IdLinea, IdSubLinea, IdProveedor, IdEstadoProd, Nombre)
    End Function
    '' NUEVO BUSQUEDA CON PARAMETROS ADICIONALES
    Public Function SelectGrillaProd_M(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdEstadoProd As Integer, ByVal Nombre As String, ByVal Codigo As String, ByVal Tb As List(Of Entidades.ProductoTipoTablaValor)) As List(Of Entidades.GrillaProducto_M)
        Return objDAOProducto.SelectGrillaProd_M(IdLinea, IdSubLinea, IdEstadoProd, Nombre, Codigo, Tb)
    End Function
    Public Function SelectGrillaProd_E(ByVal serie As String, ByVal nombre As String, ByVal idEstado As Integer) As List(Of Entidades.GrillaProducto_E)
        Return objDAOProducto.SelectGrillaProd_E(serie, nombre, idEstado)
    End Function
    Public Function SelectxSubLinea_Cbo(ByVal idSubLinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectxSubLinea_Cbo(idSubLinea)
    End Function
    Public Function ProductoCostoCompraSelectxIdSubLinea(ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, _
                                                         ByVal IdSubLinea As Integer, ByVal Prod_Codigo As String, _
                                                         ByVal Prod_Nombre As String, ByVal Tabla As DataTable) As List(Of Entidades.ProductoUMCostoCompraView)
        Return objDAOProducto.ProductoCostoCompraSelectxIdSubLinea(IdTipoExistencia, IdLinea, IdSubLinea, Prod_Codigo, Prod_Nombre, Tabla)
    End Function
    Public Function ActualizaProductoPrecioCompra(idUsuario As Integer, ByVal listaProductos As List(Of Entidades.Producto)) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(Data.IsolationLevel.Serializable)
            Dim objDaoProductoTipoPV As New DAO.DAOProductoTipoPV
            For i As Integer = 0 To listaProductos.Count - 1
                With listaProductos
                    If (Not objDAOProducto.ActualizaProductoPrecioCompra(idUsuario, cn, tr, .Item(i).Id, .Item(i).PrecioCompra, .Item(i).IdMoneda)) Then
                        Throw New Exception
                    End If
                End With

                ''***************************** actualizo los porcentajes de los tipo pv
                'Dim lista As List(Of Entidades.ProductoTipoPV) = objDaoProductoTipoPV.SelectxIdProducto(listaProductos(i).Id, cn, tr)

                ''************** creo un nuevo producto tipo pv
                'For x As Integer = 0 To lista.Count - 1

                '    Dim valorUtilidad As Decimal = lista(x).Valor - (lista(x).PrecioCompraxUMPrincipal * lista(x).Equivalencia)
                '    If lista(x).PUtilVariable <> 0 Then
                '        lista(x).PUtilFijo = 0
                '        If lista(x).PrecioCompraxUMPrincipal = 0 Then
                '            Select Case lista(x).Valor
                '                Case 0
                '                    lista(x).PUtilVariable = 0
                '                Case Is > 0
                '                    lista(x).PUtilVariable = 100
                '                Case Is < 0
                '                    lista(x).PUtilVariable = -100
                '            End Select
                '        Else
                '            lista(x).PUtilVariable = (valorUtilidad / (lista(x).PrecioCompraxUMPrincipal * lista(x).Equivalencia)) * 100
                '        End If
                '    Else
                '        lista(x).PUtilVariable = 0
                '        If lista(x).PrecioCompraxUMPrincipal = 0 Then
                '            Select Case lista(x).Valor
                '                Case 0
                '                    lista(x).PUtilFijo = 0
                '                Case Is > 0
                '                    lista(x).PUtilFijo = 100
                '                Case Is < 0
                '                    lista(x).PUtilFijo = -100
                '            End Select
                '        Else
                '            lista(x).PUtilFijo = (valorUtilidad / (lista(x).PrecioCompraxUMPrincipal * lista(x).Equivalencia)) * 100
                '        End If
                '    End If

                '    '************************** actualizo el precio de venta
                '    lista(x).Utilidad = valorUtilidad
                '    objDaoProductoTipoPV.ActualizaProductoTipoPV(lista(x), cn, tr)
                'Next
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function SelectUMPrincipalxIdSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal nomProducto As String) As List(Of Entidades.GrillaProducto_M)
        Return objDAOProducto.SelectUMPrincipalxIdSubLinea(idlinea, idsublinea, nomProducto)
    End Function
    Public Function SelectOrdenxIdSubLinea(ByVal idsublinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectOrdenxIdSubLinea(idsublinea)
    End Function
    Public Function UpdateOrdenProducto(ByVal lista As List(Of Entidades.Producto)) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            For i As Integer = 0 To lista.Count - 1
                With lista
                    objDAOProducto.UpdateOrdenxIdProducto(cn, tr, .Item(i).Id, .Item(i).Orden)
                End With
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function SelectPrecioCompra(ByVal IdProducto As Integer) As Decimal
        Return objDAOProducto.SelectPrecioCompra(IdProducto)
    End Function

    Public Function SelectEquivalenciaEntreUM(ByVal IdProducto As Integer, ByVal IdUM_Origen As Integer, ByVal IdUM_Destino As Integer, ByVal Escalar As Decimal) As Decimal
        Return objDAOProducto.SelectEquivalenciaEntreUM(IdProducto, IdUM_Origen, IdUM_Destino, Escalar)
    End Function

    Public Function SelectBusquedaProdxParams_Standar_Paginado(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal nomProducto As String, ByVal codSubLinea As Integer, ByVal pagenumber As Integer, ByVal pagesize As Integer) As List(Of Entidades.GrillaProducto_M)
        Return objDAOProducto.SelectBusquedaProdxParams_Standar_Paginado(idlinea, idsublinea, nomProducto, codSubLinea, pagenumber, pagesize)
    End Function

    Public Function SelectProductoxLineaSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer) As List(Of Entidades.Producto)
        Return objDAOProducto.SelectProductoxLineaSubLinea(idlinea, idsublinea)
    End Function

    Public Function DeleteDocumentoxIdVehiculo(ByVal producto As Integer) As Integer
        Return objDAOProducto.DeleteVehiculoxDocumento(producto)
    End Function
    Public Function InsertaVehiculoxEstadoProxSubLinea(ByVal producto As Entidades.Vehiculo) As Integer
        Return objDAOProducto.InsertaVehiculoxEstadoProxSubLinea(producto)
    End Function
    Public Function ActualizaVehiculoxEstadoProxSubLinea(ByVal producto As Entidades.Vehiculo) As Integer
        Return objDAOProducto.ActualizaVehiculoxEstadoProxSubLinea(producto)
    End Function
    Public Function SelectGrillaVehiculo_Buscar(ByVal Placa As String, ByVal Modelo As String, ByVal Constancia As String, ByVal EstadoProd As String) As List(Of Entidades.Vehiculo)
        Return objDAOProducto.SelectGrillaVehiculo_Buscar(Placa, Modelo, Constancia, EstadoProd)
    End Function
    Public Function ProductoValidadMovAlmacenIdProducto(ByVal IdProducto As Integer) As Entidades.Producto
        Return objDAOProducto.ProductoValidadMovAlmacenIdProducto(IdProducto)
    End Function

    '*********************   BUSQUEDA DE PRODUCTOS STANDAR
    Public Function Producto_ListarProductosxParams(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdPais As Integer, ByVal IdFabricante As Integer, ByVal IdMarca As Integer, ByVal IdModelo As Integer, ByVal IdFormato As Integer, ByVal IdProveedor As Integer, ByVal IdEstilo As Integer, ByVal IdTransito As Integer, ByVal IdCalidad As Integer) As List(Of Entidades.ProductoView)
        Return objDAOProducto.Producto_ListarProductosxParams(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdEmpresa, IdAlmacen, pagesize, pagenumber, IdPais, IdFabricante, IdMarca, IdModelo, IdFormato, IdProveedor, IdEstilo, IdTransito, IdCalidad)
    End Function
    Public Function Producto_ListarProductosxParams_V2(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, _
                                                       ByVal IdEmpresa As Integer, _
                                                       ByVal IdAlmacen As Integer, _
                                                       ByVal pagesize As Integer, _
                                                       ByVal pagenumber As Integer, _
                                                       ByVal codigoProducto As String, _
                                                       ByVal IdTipoExistencia As Integer, _
                                                        ByVal tableTablaTipoValor As DataTable) As List(Of Entidades.ProductoView)
        Return objDAOProducto.Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdEmpresa, IdAlmacen, pagesize, pagenumber, codigoProducto, IdTipoExistencia, tableTablaTipoValor)
    End Function
    Public Function Producto_ListarProductosxParamsReporte(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdEmpresa As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdPais As Integer, ByVal IdFabricante As Integer, ByVal IdMarca As Integer, ByVal IdModelo As Integer, ByVal IdFormato As Integer, ByVal IdProveedor As Integer, ByVal IdEstilo As Integer, ByVal IdTransito As Integer, ByVal IdCalidad As Integer) As List(Of Entidades.ProductoView)
        Return objDAOProducto.Producto_ListarProductosxParamsReporte(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdEmpresa, pagesize, pagenumber, IdPais, IdFabricante, IdMarca, IdModelo, IdFormato, IdProveedor, IdEstilo, IdTransito, IdCalidad)
    End Function
    Public Function ValidarAtributos(ByVal NoVisible As String, ByVal prod_Codigo As String, ByVal prod_Atributos As String) As Entidades.Producto
        Return objDAOProducto.ValidarAtributos(NoVisible, prod_Codigo, prod_Atributos)
    End Function
    Public Function Producto_ListarProductosxParams_V2reporte(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal codigoProducto As String, ByVal tableTablaTipoValor As DataTable) As List(Of Entidades.ProductoView)
        Return objDAOProducto.Producto_ListarProductosxParams_V2Reporte(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdEmpresa, IdAlmacen, pagesize, pagenumber, codigoProducto, tableTablaTipoValor)
    End Function
    Public Function LongitudProducto() As Entidades.Producto
        Return objDAOProducto.LongitudProducto()
    End Function
    Public Function EliminarxIdProducto(ByVal IdProducto As Integer) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            objDAOProducto.EliminarxIdProducto(cn, tr, IdProducto)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function productoAfectoSublineaforInsert(ByVal idsublinea As Integer) As Integer
        Return objDAOProducto.productoAfectoSublineaforInsert(idsublinea)
    End Function
    Public Function productoAfectoSublineaForUpdate(ByVal idsublinea As Integer, ByVal idproducto As Integer) As Integer
        Return objDAOProducto.productoAfectoSublineaForUpdate(idsublinea, idproducto)
    End Function

End Class

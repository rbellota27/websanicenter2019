﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Collections.Generic
Public Class CargoTarjeta
    Dim obj As New DAO.DAOCargoTarjeta
    Public Function InsertaCargoTarjeta(ByVal CargoTarjeta As Entidades.CargoTarjeta) As Boolean
        Return obj.InsertaCargoTarjeta(CargoTarjeta)
    End Function
    Public Function ActualizaCargoTarjeta(ByVal CargoTarjeta As Entidades.CargoTarjeta) As Boolean
        Return obj.ActualizaCargoTarjeta(CargoTarjeta)
    End Function
    'Public Function SelectAll() As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectAll
    'End Function
    'Public Function SelectAllActivo() As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectAllActivo
    'End Function
    'Public Function SelectAllInactivo() As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectAllInactivo
    'End Function
    'Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectAllxNombre(nombre)
    'End Function
    'Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectActivoxNombre(nombre)
    'End Function
    'Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectInactivoxNombre(nombre)
    'End Function
    'Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectxId(id)
    'End Function
    Public Function SelectxIdCargoTarjeta(ByVal idCargoTarjeta As Integer) As Entidades.CargoTarjeta
        Return obj.SelectxIdCargoTarjeta(idCargoTarjeta)
    End Function
    Public Function SelectCodigoxIdCargoTarjeta(ByVal IdCargoTarjeta As Integer) As String
        Return obj.SelectCodigoxIdCargoTarjeta(IdCargoTarjeta)
    End Function
    'Public Function SelectCbo() As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectCbo
    'End Function
    'Public Function SelectAllxValor(ByVal Valor As String) As List(Of Entidades.CargoTarjeta)
    '    Return obj.SelectAllxValor(Valor)
    'End Function

    Public Function SelectCargoTarjeta(ByVal estado As Integer) As List(Of Entidades.CargoTarjeta)
        Return obj.SelectCargoTarjeta(estado)
    End Function


    Public Function SelectxEstadoxNombre(ByVal nombre As String, ByVal estado As String) As List(Of Entidades.CargoTarjeta)
        Return obj.SelectxEstadoxNombre(nombre, estado)
    End Function

    Public Function SelectAllActivoxCargoTarjeta_Cbo(ByVal nombre As String) As List(Of Entidades.CargoTarjeta)
        Return obj.SelectAllActivoxNombre_Cbo(nombre)
    End Function




End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM

Public Class Concepto_TipoDocumento
    Private objDaoConceptoTipoDocumento As New DAO.DAOConcepto_TipoDocumento
    Public Function SelectCboxIdTipoDocumento(ByVal IdTipoDocumento As Integer) As List(Of Entidades.Concepto)
        Return objDaoConceptoTipoDocumento.SelectCboxIdTipoDocumento(IdTipoDocumento)
    End Function
    Public Function SelectConceptoTipoDocxIdTipoGastoxNomConcepto(ByVal IdTipoGasto As Integer, ByVal NomConcepto As String, _
                                                                  ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.Concepto_TipoDocumento)
        Return objDaoConceptoTipoDocumento.SelectConceptoTipoDocxIdTipoGastoxNomConcepto(IdTipoGasto, NomConcepto, pageindex, pagesize)
    End Function

    Public Function SelectConceptoDetraccionxIdTipoDocumento(ByVal IdTipoDocumento As Integer) As List(Of Entidades.Concepto)
        Return objDaoConceptoTipoDocumento.SelectConceptoDetraccionxIdTipoDocumento(IdTipoDocumento)
    End Function


End Class

﻿Imports System.Data.SqlClient
Public Class OrdenCompraPreliminar
    Dim Obj As New DAO.DAOOrdenCompraPreliminar
    Dim objConexion As New DAO.Conexion
    Dim objDoc As New DAO.DAODocumento

    Function InsertarOrdenCompra(ByVal objDocumento As Entidades.Documento, _
                                ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                ByVal obs As Entidades.Observacion, _
                                ByVal objMonto As Entidades.MontoRegimen, _
                                ByVal listaCondicionComercial As List(Of Entidades.CondicionComercial)) _
                                As String
        Return Obj.InsertarOrdenCompra(objDocumento, listaDetalle, obs, objMonto, listaCondicionComercial)
    End Function

   Public Function updateDocumento(ByVal objDocumento As Entidades.Documento, ByVal ListaDetalle As List(Of Entidades.DetalleDocumento), _
                                     ByVal objObs As Entidades.Observacion, ByVal objMonto As Entidades.MontoRegimen, ByVal listaCondicionComercial As List(Of Entidades.CondicionComercial)) As Boolean
        Return Obj.UpdateDocumento(objDocumento, ListaDetalle, objObs, objMonto, listaCondicionComercial)
    End Function


    Public Function ValidarIDCC(ByVal idTipo As Integer, ByVal nombre As String) As Integer
        Return Obj.ValidarIDCC(idTipo, nombre)
    End Function

    Public Function Relacion(ByVal IdDocumento1 As Integer, ByVal IdDocumento2 As Integer) As Boolean

        Return Obj.GrabaRelacionDocumentosNuevos(IdDocumento1, IdDocumento2)
        'Throw New Exception("Fracasó el Proceso de Inserción")
    End Function


    Public Function RegularizacionOC(ByVal Doc As Integer) As Boolean
        Return Obj.RegularizacionOC(Doc)
    End Function

    Public Function RegularizacionCI(ByVal Doc As Integer) As Boolean
        Return Obj.RegularizacionCI(Doc)
    End Function

    Public Function RegularizacionGR(ByVal Doc As Integer) As Boolean
        Return Obj.RegularizacionGR(Doc)
    End Function

    Public Function RegularizacionInforme(ByVal Doc As Integer) As Boolean
        Return Obj.RegularizacionInforme(Doc)
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Cargo
    Private objCargo As New DAO.DAOCargo
    Public Function InsertaCargo(ByVal cargo As Entidades.Cargo) As Boolean
        Return objCargo.InsertaCargo(cargo)
    End Function
    Public Function ActualizaCargo(ByVal cargo As Entidades.Cargo) As Boolean
        Return objCargo.ActualizaCargo(cargo)
    End Function
    Public Function SelectAll() As List(Of Entidades.Cargo)
        Return objCargo.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Cargo)
        Return objCargo.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Cargo)
        Return objCargo.SelectAllInactivo
    End Function
    Public Function SelectxId(ByVal IdCargo As Integer) As List(Of Entidades.Cargo)
        Return objCargo.SelectxId(IdCargo)
    End Function
    Public Function SelectAllxNombre(ByVal Nombre As String) As List(Of Entidades.Cargo)
        Return objCargo.SelectAllxNombre(Nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal Nombre As String) As List(Of Entidades.Cargo)
        Return objCargo.SelectActivoxNombre(Nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal Nombre As String) As List(Of Entidades.Cargo)
        Return objCargo.SelectInactivoxNombre(Nombre)
    End Function
    Public Function SelectCbo() As List(Of Entidades.Cargo)
        Return objCargo.SelectCbo
    End Function
End Class

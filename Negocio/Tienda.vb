﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Tienda
    Private objDaoTienda As New DAO.DAOTienda

    Public Function UpdateObjetivoxTienda(ByVal objtienda As Entidades.Tienda) As Boolean
        Return objDaoTienda.UpdateObjetivoxTienda(objtienda)
    End Function
    Public Function SelectxIdTiendaObjetivo(ByVal Obj As Entidades.Tienda) As Entidades.Tienda
        Return objDaoTienda.SelectxIdTiendaObjetivo(Obj)
    End Function
    Public Function SelectAllObjetivoxTienda(ByVal Obj As Entidades.Tienda) As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectAllObjetivoxTienda(Obj)
    End Function

    Public Function InsertObjxTienda(ByVal obj As Entidades.Tienda) As Boolean
        Return objDaoTienda.InsertObjxTienda(obj)
    End Function
    Public Function SelectCboxEmpresa(ByVal idEmpresa As Integer) As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectCboxEmpresa(idEmpresa)
    End Function
    Public Function SelectCbo() As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectCbo
    End Function
    Public Function SelectAll() As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectAllInactivo
    End Function
    Public Function InsertaTienda(ByVal tienda As Entidades.Tienda) As Boolean
        Return objDaoTienda.InsertaTienda(tienda)
    End Function
    Public Function ActualizaTienda(ByVal tienda As Entidades.Tienda) As Boolean
        Return objDaoTienda.ActualizaTienda(tienda)
    End Function
    Public Function SelectxId(ByVal IdTienda As Integer) As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectxId(IdTienda)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectInactivoxNombre(nombre)
    End Function
    Public Function InsertaConfiguracionTiendaT(ByVal IdTienda As Integer, ByVal lTiendaArea As List(Of Entidades.TiendaArea), ByVal lTiendaAlmacen As List(Of Entidades.TiendaAlmacen)) As Boolean
        Return objDaoTienda.InsertaConfiguracionTiendaT(IdTienda, lTiendaArea, lTiendaAlmacen)
    End Function
    Public Function BuscaAllxNombre(ByVal nombre As String) As List(Of Entidades.Tienda)
        Return objDaoTienda.BuscaAllxNombre(nombre)
    End Function
    Public Function BuscaAllxNombreActivoInactivo(ByVal nombre As String) As List(Of Entidades.Tienda)
        Return objDaoTienda.BuscaAllxNombreActivoInactivo(nombre)
    End Function

    Public Function SelectCboxIdEmpresaxIdUsuario_Caja(ByVal idEmpresa As Integer, ByVal IdUsuario As Integer) As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectCboxIdEmpresaxIdUsuario_Caja(idEmpresa, IdUsuario)
    End Function
    Public Function SelectCboxAlmacen(ByVal idEmpresa As Integer) As List(Of Entidades.Tienda)
        Return objDaoTienda.SelectCboxAlmacen(idEmpresa)
    End Function
End Class

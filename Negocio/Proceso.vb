﻿Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class Proceso
    Dim objDAOProceso As New DAO.DAOProceso
    Dim objConexion As DAO.Conexion
    Public Function SelectProceso(ByVal var As String) As List(Of Entidades.Proceso)
        Return objDAOProceso.SelectProceso(var)
    End Function

    Public Function AgregarProceso(ByVal objProceso As Entidades.Proceso) As Boolean

        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim idproceso As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            idproceso = objDAOProceso.AgregarProceso(objProceso, cn, "i", tr)

            If idproceso = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If


            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho

    End Function


    Public Function ActualizaProceso(ByVal objProceso As Entidades.Proceso) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If Not objDAOProceso.ActualizaProceso(objProceso, cn, "a", tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function


    Public Function SelectxIdProceso(ByVal idProceso As Integer) As List(Of Entidades.Proceso)
        Return objDAOProceso.SelectxIdProceso(idProceso)
    End Function



    Public Function EliminarProceso(ByVal id As Integer) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If Not objDAOProceso.EliminaIdProceso(id, cn, "e", tr) Then
                Throw New Exception("Fracasó el proceso de Eliminación.")
            End If
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

End Class

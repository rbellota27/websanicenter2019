﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class opcion

    Private objDaoOpcion As New DAO.DAOOpcion
    Private objConexion As New DAO.Conexion
    ' Private objNegOpcion As New Negocio.opcion

    Public Function SelectOpcionxDocumentoRef(ByVal IdDocumento As Integer) As List(Of Entidades.Opcion)
        Return objDaoOpcion.SelectOpcionxDocumentoRef(IdDocumento)
    End Function

    Public Function SelectxIdOpcion(ByVal IdOpcion As Integer) As Entidades.Opcion
        Return objDaoOpcion.SelectxIdOpcion(IdOpcion)
    End Function

    Public Function SelectAllOpcion() As List(Of Entidades.Opcion)
        Return objDaoOpcion.SelectAllOpcion
    End Function
    Public Function CargaNodoTreeView() As System.Data.DataTable
        Return objDaoOpcion.CargaNodoTreeView
    End Function
    Public Function SelectOpcionxUsuarioxPerfil(ByVal idperfil As Integer, ByVal idusuario As Integer) As List(Of Entidades.Opcion)
        Return objDaoOpcion.SelectOpcionxUsuarioxPerfil(idperfil, idusuario)
    End Function
    Public Function SelectOpcionxPerfil(ByVal idperfil As Integer) As List(Of Entidades.Opcion)
        Return objDaoOpcion.SelectOpcionxPerfil(idperfil)
    End Function

    Public Function SelectOpcionxUsuario(ByVal idusuario As Integer) As List(Of Entidades.Opcion)
        Return objDaoOpcion.SelectOpcionxUsuario(idusuario)
    End Function
    Public Function SelectOpcionMantenimiento() As List(Of Entidades.Opcion)
        Return objDaoOpcion.SelectOpcionMantenimiento()
    End Function
    Public Function SelectPerfilxUsuario(ByVal idusuario As Integer) As List(Of Entidades.Perfil)
        Return objDaoOpcion.SelectPerfilxUsuario(idusuario)
    End Function
    Public Function DeletePerfilOpcion(ByVal IdPerfil As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objDaoOpcion.DeletePerfilOpcion(IdPerfil, cn, tr)
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Function
    Public Function InsertaPerfilOpcion(ByVal listaOpcion As List(Of Entidades.Opcion), ByVal idperfil As Integer, ByVal aplicarAll As Boolean) As Boolean
        '************ 
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            Dim objDaoOpcion As New DAO.DAOOpcion
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            ''*************
            objDaoOpcion.DeletePerfilOpcion(idperfil, cn, tr)
            '************** 
            objDaoOpcion.InsertaPerfilOpcion(listaOpcion, cn, tr, idperfil)

            If (aplicarAll) Then
                objDaoOpcion.AplicarCambiosAllUsers(cn, tr, idperfil)
            End If


            bool = True
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function

    Public Function InsertaPerfilOpcionUsuario(ByVal listaOpcion As List(Of Entidades.Opcion), ByVal idperfil As Integer, ByVal idusuario As Integer) As Boolean
        '************ 
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            Dim objDaoOpcion As New DAO.DAOOpcion
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            ''*************
            objDaoOpcion.DeletePerfilOpcionUsuario(idperfil, cn, tr, idusuario)
            '************** 
            objDaoOpcion.InsertaPerfilOpcionUsuario(listaOpcion, cn, tr, idperfil, idusuario)
            bool = True
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function

    Public Function UpdateInsertaOpcion(ByVal Listaopcion As List(Of Entidades.Opcion)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objDaoOpcion.UpdateInsertaOpcion(Listaopcion, cn, tr)
            bool = True
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function
    'elimina 
    Public Function Elimina(ByVal idopcion As Integer, ByVal deleteAll As Boolean) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objDaoOpcion.Elimina(cn, tr, idopcion, deleteAll)
            tr.Commit()
            bool = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        End Try
        If cn.State = ConnectionState.Open Then cn.Close()
        Return bool
    End Function

    Public Function InsertaOpcion(ByVal opcion As Entidades.Opcion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objDaoOpcion.InsertaOpcion(opcion)
            tr.Commit()
            bool = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function
    Public Function ActualizaOpcion(ByVal opcion As Entidades.Opcion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objDaoOpcion.ActualizaOpcion(opcion)
            tr.Commit()
            bool = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        End Try

        Return bool
    End Function

End Class

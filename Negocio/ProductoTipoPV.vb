﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class ProductoTipoPV
    Dim obj As New DAO.DAOProductoTipoPV

    Public Function ProductoPrecioV(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, ByVal IdTipoPV_Origen As Integer, ByVal IdTipoPV_Destino As Integer, ByVal IdProducto As Integer, ByVal PorcentPV As Decimal, ByVal addCostoFletexPeso As Boolean) As List(Of Entidades.ProductoTipoPV)
        Return obj.ProductoPrecioV(IdTiendaOrigen, IdTiendaDestino, IdTipoPV_Origen, IdTipoPV_Destino, IdProducto, PorcentPV, addCostoFletexPeso)
    End Function


    Public Function ReplicarPrecioPV_IdProducto(ByVal lista As List(Of Entidades.ProductoTipoPV_Replica), ByVal ListaTienda As List(Of Entidades.Tienda)) As Boolean
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            For j As Integer = 0 To ListaTienda.Count - 1

                For i As Integer = 0 To lista.Count - 1

                    obj.ReplicarPrecioPV_IdProducto(lista(i).IdTienda_Origen, ListaTienda(j).Id, lista(i).IdProducto, lista(i).IdTipoPV_Origen, lista(i).IdTipoPV_Destino, lista(i).PorcentPV, lista(i).addCostoFletexPeso, cn, tr, lista(i).IdUsuario)

                Next

            Next


            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function


    Public Function SelectValor_Equivalencia_VentaxParams_Cotizacion(ByVal IdUnidadMedida As Integer, ByVal IdProducto As Integer, ByVal IdTipoPV As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, ByVal IdAlmacen As Integer, ByVal IdEmpresa As Integer, ByVal Fecha As Date, Optional ByVal cantidad As Decimal = 0, Optional ByVal IdUMold As Integer = 0) As Entidades.Catalogo
        Return obj.SelectValor_Equivalencia_VentaxParams_Cotizacion(IdUnidadMedida, IdProducto, IdTipoPV, IdTienda, IdMoneda, IdAlmacen, IdEmpresa, Fecha, cantidad, IdUMold)
    End Function
    Public Function ProductoTipoPV_ReplicarPrecioPV(ByVal lista As List(Of Entidades.ProductoTipoPV_Replica), ByVal DT_Atributos As DataTable) As Boolean
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            For i As Integer = 0 To lista.Count - 1

                obj.ProductoTipoPV_ReplicarPrecioPV(lista(i).IdTienda_Origen, lista(i).IdTienda_Destino, lista(i).IdLinea, lista(i).IdSubLinea, lista(i).IdTipoPV_Origen, lista(i).IdTipoPV_Destino, lista(i).PorcentPV, DT_Atributos, lista(i).addCostoFletexPeso, cn, tr, lista(i).IdUsuario)

            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function SelectValor_Equivalencia_VentaxParams(ByVal IdAlmacen As Integer, ByVal IdMoneda As Integer, ByVal IdUnidadMedida As Integer, ByVal IdProducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer, Optional ByVal addPorcentRetazoAlways As Boolean = False) As Entidades.Catalogo
        Return obj.SelectValor_Equivalencia_VentaxParams(IdAlmacen, IdMoneda, IdUnidadMedida, IdProducto, idtienda, idtipopv, addPorcentRetazoAlways)
    End Function
    Public Function SelectUM_Venta(ByVal IdProducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer) As List(Of Entidades.UnidadMedida)
        Return obj.SelectUM_Venta(IdProducto, idtienda, idtipopv)
    End Function
    Public Function SelectValorCalculadoxParams(ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer, ByVal idunidadmedida As Integer, ByVal idmoneda As Integer) As Decimal

        Return obj.SelectValorCalculadoxParams(idproducto, idtienda, idtipopv, idunidadmedida, idmoneda)

    End Function
    Public Function SelectAll() As List(Of Entidades.ProductoTipoPV)
        Return obj.SelectAll
    End Function
    Public Function InsertaProductoTipoPVT(ByVal lista As List(Of Entidades.ProductoTipoPV), Optional ByVal CambiarxEquivalencia As Boolean = False) As Boolean
        'INSERTA UNA LISTA DE PRECIOS DE VENTA PARA DIFERENTES PRODUCTOS
        'FORMA MASIVA
        Return obj.InsertaProductoTipoPVT(lista, CambiarxEquivalencia)
    End Function
    Public Function InsertaProductoTipoPVTxProducto(ByVal lista As List(Of Entidades.ProductoTipoPV), ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer) As Boolean
        'INSERTA TODOS LOS PRECIOS DE VENTA PARA UN PRODUCTO
        Return obj.InsertaProductoTipoPVTxProducto(lista, idproducto, idtienda, idtipopv)
    End Function
    'Public Function ActualizaProductoTipoPV(ByVal productotipopv As Entidades.ProductoTipoPV) As Boolean
    '    Return obj.ActualizaProductoTipoPV(productotipopv)

    'End Function
    Public Function SelectPrecioPublicoxParams(ByVal IdTienda As Integer, ByVal IdProducto As Integer, _
                                               ByVal IdUnidadMedida As Integer) As Decimal
        Return obj.SelectPrecioPublicoxParams(IdTienda, IdProducto, IdUnidadMedida)
    End Function
    Public Function SelectPrecioxParams(ByVal IdTienda As Integer, ByVal IdProducto As Integer, _
                                           ByVal IdUnidadMedida As Integer, ByVal IdTipoPV As Integer) As Decimal
        Return obj.SelectPrecioxParams(IdTienda, IdProducto, IdUnidadMedida, IdTipoPV)
    End Function
    Public Function SelectPpublicoUMPrincipalxParams(ByVal IdTienda As Integer, ByVal IdProducto As Integer) As Decimal
        Return obj.SelectPpublicoUMPrincipalxParams(IdTienda, IdProducto)
    End Function
    Public Function SelectxIdProductoxIdTiendaxIdTipoPV(ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer) As List(Of Entidades.ProductoTipoPV)
        Return obj.SelectxIdProductoxIdTiendaxIdTipoPV(idproducto, idtienda, idtipopv)
    End Function
End Class

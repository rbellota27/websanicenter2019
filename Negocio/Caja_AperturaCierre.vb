﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class Caja_AperturaCierre

    Dim dCaja_AperturaCierre As DAO.DAOCaja_AperturaCierre
    Dim cn As SqlConnection = Nothing
    Dim tr As SqlTransaction = Nothing

    Public Function Caja_AperturaCierre_SelectEstado(ByVal IdCaja As Integer, ByVal cac_FechaApertura As String) As String
        dCaja_AperturaCierre = New DAO.DAOCaja_AperturaCierre
        Return dCaja_AperturaCierre.Caja_AperturaCierre_SelectEstado(IdCaja, cac_FechaApertura)
    End Function

    Public Function Caja_AperturaCierre_Select(ByVal Id As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal IdUsuario As Integer, Optional ByVal cac_FechaApertura As String = Nothing, Optional ByVal cac_FechaCierre As String = Nothing) As List(Of Entidades.Caja_AperturaCierre)
        dCaja_AperturaCierre = New DAO.DAOCaja_AperturaCierre
        Return dCaja_AperturaCierre.Caja_AperturaCierre_Select(Id, IdEmpresa, IdTienda, IdCaja, IdUsuario, cac_FechaApertura, cac_FechaCierre)
    End Function

    Public Sub Caja_AperturaCierre_Transaction(ByVal ListaCajaAperturaCierre As List(Of Entidades.Caja_AperturaCierre))

        Try

            cn = (New DAO.Conexion).ConexionSIGE

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For i As Integer = 0 To ListaCajaAperturaCierre.Count - 1

                ListaCajaAperturaCierre(i).IdCaja_AperturaCierre = (New DAO.DAOCaja_AperturaCierre).Caja_AperturaCierre_Transaction(ListaCajaAperturaCierre(i), cn, tr)

            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Sub


End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class Persona

    Dim obj As New DAO.DAOPersona

    Public Function PersonaDato(ByVal ObjPersonaDato As Entidades.PersonaDato) As List(Of Entidades.PersonaDato)
        Return obj.PersonaDato(ObjPersonaDato)
    End Function
    Public Function ValidancionPerfilNuevoxIdUsuario(ByVal IdPersona As Integer) As Integer
        Return obj.ValidancionPerfilNuevoxIdUsuario(IdPersona)
    End Function

#Region "Otros"
   
    Public Function Registrar_MaestroObra(ByVal objPersonaView As Entidades.PersonaView, ByVal TipoPersona As String, ByVal IdEmpresa As Integer) As Integer

        Dim IdPersona As Integer = Nothing
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            IdPersona = obj.Registrar_MaestroObra(objPersonaView, TipoPersona, IdEmpresa, cn, tr)

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return IdPersona
    End Function

    Public Function RegistrarCliente_Ventas(ByVal objPersonaView As Entidades.PersonaView, ByVal TipoPersona As String, ByVal IdEmpresa As Integer) As Integer

        Dim IdPersona As Integer = Nothing
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            IdPersona = obj.RegistrarCliente_Ventas(objPersonaView, TipoPersona, IdEmpresa, cn, tr)

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return IdPersona
    End Function
    Public Function InsertaPersonaT(ByVal persona As Entidades.Persona, ByVal Nat As Boolean, ByVal natural As Entidades.Natural, _
                                   ByVal Jur As Boolean, ByVal Juridica As Entidades.Juridica, _
                                   ByVal rol As Boolean, ByVal RolPersona As Entidades.RolPersona, _
                                   Optional ByVal Dir As Boolean = Nothing, Optional ByVal LDir As List(Of Entidades.Direccion) = Nothing, _
                                   Optional ByVal tel As Boolean = Nothing, Optional ByVal LTel As List(Of Entidades.Telefono) = Nothing, _
                                   Optional ByVal doi As Boolean = Nothing, Optional ByVal LDoid As List(Of Entidades.DocumentoI) = Nothing, _
                                   Optional ByVal cor As Boolean = Nothing, Optional ByVal LCor As List(Of Entidades.Correo) = Nothing, _
                                   Optional ByVal Age As Boolean = Nothing, Optional ByVal Agente As Entidades.PersonaTipoAgente = Nothing, _
                                   Optional ByVal profesion As Boolean = Nothing, Optional ByVal ProfesionPersona As Entidades.ProfesionPersona = Nothing, _
                                   Optional ByVal ocupacion As Boolean = Nothing, Optional ByVal OcupacionPersona As Entidades.OcupacionPersona = Nothing) As Integer

        Return obj.InsertaPersonaT(persona, Nat, natural, Jur, Juridica, rol, RolPersona, Dir, LDir, tel, LTel, doi, LDoid, cor, LCor, Age, Agente, profesion, ProfesionPersona, ocupacion, OcupacionPersona)
    End Function
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.Persona
        Return obj.SelectxIdPersona(IdPersona)
    End Function
    Public Function Selectx_IdPersona(ByVal IdPersona As Integer) As Entidades.Persona
        Return obj.Selectx_IdPersona(IdPersona)
    End Function
    Public Function listarPersonaNoProveedor(ByVal nrodoc As String) As Entidades.Persona
        Return obj.listarPersonaNoProveedor(nrodoc)
    End Function
    Public Function listarPersonaFRMproveedor(ByVal nrodni As String) As Entidades.Natural
        Return obj.listarPersonaFRMproveedor(nrodni)
    End Function
    Public Function ActualizaPersonaT(ByVal persona As Entidades.Persona, _
                                   ByVal Nat As Boolean, ByVal natural As Entidades.Natural, _
                                   ByVal Jur As Boolean, ByVal Juridica As Entidades.Juridica, _
                                   ByVal rol As Boolean, ByVal RolPersona As Entidades.RolPersona, _
                                   Optional ByVal Dir As Boolean = Nothing, Optional ByVal LDir As List(Of Entidades.Direccion) = Nothing, _
                                   Optional ByVal tel As Boolean = Nothing, Optional ByVal LTel As List(Of Entidades.Telefono) = Nothing, _
                                   Optional ByVal doi As Boolean = Nothing, Optional ByVal LDoid As List(Of Entidades.DocumentoI) = Nothing, _
                                   Optional ByVal cor As Boolean = Nothing, Optional ByVal LCor As List(Of Entidades.Correo) = Nothing, _
                                   Optional ByVal Age As Boolean = Nothing, Optional ByVal Agente As Entidades.PersonaTipoAgente = Nothing, _
                                   Optional ByVal profesion As Boolean = Nothing, Optional ByVal ProfesionPersona As Entidades.ProfesionPersona = Nothing, _
                                   Optional ByVal ocupacion As Boolean = Nothing, Optional ByVal OcupacionPersona As Entidades.OcupacionPersona = Nothing) As Integer

        Return obj.ActualizaPersonaT(persona, Nat, natural, Jur, Juridica, rol, RolPersona, Dir, LDir, tel, LTel, doi, LDoid, cor, LCor, Age, Agente, profesion, ProfesionPersona, ocupacion, OcupacionPersona)
    End Function
    Public Function ActualizaPersonaEstado(ByVal persona As Entidades.Persona) As Integer
        Return obj.ActualizaPersonaEstado(persona)
    End Function
    Public Function getDescripcionPersona(ByVal IdPersona As Integer) As String
        Return obj.getDescripcionPersona(IdPersona)
    End Function
    Public Function getRUC(ByVal IdPersona As Integer) As String
        Return obj.getRUC(IdPersona)
    End Function
#End Region

#Region "Mantenimiento Persona"

    Public Function getDataSetConsultaPersona(ByVal nombres As String, ByVal numero As String, _
                                              ByVal tipo As Integer, ByVal estado As Integer, ByVal ordenar As Integer) As DataSet
        Return obj.getDataSetConsultaPersona(nombres, numero, tipo, estado, ordenar)
    End Function

    Public Function listarPersonaNaturalPersonaJuridica(ByVal dni As String, ByVal ruc As String, ByVal nombre As String, _
                                                        ByVal tipo As Integer, ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.PersonaView)
        Return obj.listarPersonaNaturalPersonaJuridica(dni, ruc, nombre, tipo, pageindex, pagesize)
    End Function

    Public Function listarxPersonaNaturalxPersonaJuridica(ByVal dni As String, ByVal ruc As String, ByVal RazonApe As String, _
                                                            ByVal tipo As Integer, ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.PersonaView)
        Return obj.listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, pageindex, pagesize)
    End Function


    Public Function InsertarNuevaPersona(ByVal tipoPersona As String, ByVal objPersona As Entidades.Persona, ByVal objNatural As Entidades.Natural, _
                                          ByVal objChofer As Entidades.Chofer, ByVal objEmpleado As Entidades.Empleado, ByVal objJuridica As Entidades.Juridica, _
                                          ByVal objListDireccion As List(Of Entidades.Direccion), ByVal objListTelefono As List(Of Entidades.TelefonoView), _
                                          ByVal objListCorreo As List(Of Entidades.CorreoView), ByVal objListaDocumentoI As List(Of Entidades.DocumentoI), _
                                          ByVal objListRolPersona As List(Of Entidades.RolPersona), ByVal objListProfesion As List(Of Entidades.ProfesionPersona), _
                                          ByVal objListOcupacion As List(Of Entidades.OcupacionPersona), ByVal ListaContactoEmpresa As List(Of Entidades.PersonaView), _
                                          ByVal List_PersonaTipoAgente As List(Of Entidades.PersonaTipoAgente)) As String

        Return obj.InsertarNuevaPersona(tipoPersona, objPersona, objNatural, objChofer, objEmpleado, objJuridica, _
                                        objListDireccion, objListTelefono, objListCorreo, objListaDocumentoI, _
                                        objListRolPersona, objListProfesion, objListOcupacion, ListaContactoEmpresa, List_PersonaTipoAgente)
    End Function

    Public Function ActualizarPersona(ByVal tipoPersona As String, ByVal objPersona As Entidades.Persona, ByVal objNatural As Entidades.Natural, _
                                        ByVal objChofer As Entidades.Chofer, ByVal objEmpleado As Entidades.Empleado, ByVal objJuridica As Entidades.Juridica, _
                                        ByVal objListDireccion As List(Of Entidades.Direccion), ByVal objListTelefono As List(Of Entidades.TelefonoView), _
                                        ByVal objListCorreo As List(Of Entidades.CorreoView), ByVal objListaDocumentoI As List(Of Entidades.DocumentoI), _
                                        ByVal objListRolPersona As List(Of Entidades.RolPersona), ByVal objListProfesion As List(Of Entidades.ProfesionPersona), _
                                        ByVal objListOcupacion As List(Of Entidades.OcupacionPersona), ByVal ListaContactoEmpresa As List(Of Entidades.PersonaView), _
                                        ByVal List_PersonaTipoAgente As List(Of Entidades.PersonaTipoAgente)) As String

        Return obj.ActualizarPersona(tipoPersona, objPersona, objNatural, objChofer, objEmpleado, objJuridica, _
                                        objListDireccion, objListTelefono, objListCorreo, objListaDocumentoI, _
                                        objListRolPersona, objListProfesion, objListOcupacion, ListaContactoEmpresa, List_PersonaTipoAgente)
    End Function

    Public Function ListarPersonaComplemento(ByVal IdPersona As Integer, ByVal TipoComplemento As String) As List(Of Entidades.PersonaView)
        Return obj.ListarPersonaComplemento(IdPersona, TipoComplemento)
    End Function
    Public Function listarxPersonaNaturalxPersonaJuridica(ByVal dni As String, ByVal ruc As String, ByVal RazonApe As String, _
                                                         ByVal tipo As Integer, ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                                         ByVal idrol As Integer, ByVal estado As Integer, Optional ByVal IdNacionalidad As Integer = Nothing) As List(Of Entidades.PersonaView)
        Return obj.listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, pageindex, pagesize, idrol, estado, IdNacionalidad)
    End Function

#End Region

End Class





﻿Imports System.Data.SqlClient
Imports DAO


Public Class DocInformeProducto
    Private objDaoInforme As New DAO.DAOInformeProducto
    Dim ObjConexion As New Conexion
    Dim HDAO As New DAO.HelperDAO
    Private Util As New DAO.DAOUtil
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private tr As SqlTransaction

    Public Function InsertarInformeproducto(ByVal ObjDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumentoView))

        Return objDaoInforme.Insert_Informe_Producto(ObjDocumento, listaDetalle)
    End Function

    Public Function SelectInforme(ByVal idserie As Integer, ByVal codigo As Integer) As Entidades.Documento
        Return objDaoInforme.SelectInformeProducto(idserie, codigo)
    End Function


    Public Function DetalleInformeProducto(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)
        Return objDaoInforme.DetalleInformeProducto(IdDocumento)
    End Function


    Public Function actualizarInforme(ByVal ObjDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumentoView))

        Return objDaoInforme.ActualizarInf(ObjDocumento, listaDetalle)
    End Function

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 22-Oct-2009
'Hora    : 10:02 am
'*************************************************

Imports System.Data.SqlClient
Public Class Magnitud
    Private obj As New DAO.DAOMagnitud
    Private objConexion As New DAO.Conexion

    Public Function InsertaMagnitud(ByVal magnitud As Entidades.Magnitud, ByVal lista As List(Of Entidades.MagnitudUnidad)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            '************ Insertar Magnitud
            Dim IdMagnitud As Integer = obj.InsertaMagnitud(magnitud, cn, tr)

            '************ Insertar Lista
            Dim objDaoMagnitudUnidad As New DAO.DAOMagnitudUnidad
            Dim objBEMagnitud As New Entidades.MagnitudUnidad
            For Each objBEMagnitud In lista
                If objBEMagnitud.Equivalencia = 0 Then
                    Throw New Exception("")
                End If
            Next
            objDaoMagnitudUnidad.InsertaMagnitudUnidad(cn, tr, lista, IdMagnitud)



            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho
    End Function


    Public Function ActualizaMagnitudUnidad(ByVal magnitud As Entidades.Magnitud, ByVal lista As List(Of Entidades.MagnitudUnidad)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ Actualizar Magnitud
            If Not (obj.ActualizaMagnitud(magnitud, cn, tr)) Then
                Throw New Exception
            End If


            '************* Eliminar magnitud unidad
            Dim objDaoMagnitudU As New DAO.DAOMagnitudUnidad
            If (Not objDaoMagnitudU.DeletexIdMagnitud(cn, tr, magnitud.Id)) Then
                Throw New Exception
            End If

            '************ Insertar Lista
            objDaoMagnitudU.InsertaMagnitudUnidad(cn, tr, lista, magnitud.Id)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho
    End Function
    'Public Function ActualizaMagnitud(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idMagnitud As Integer) As Boolean
    '    Return obj.ActualizaMagnitud(magnitud) 'AS entidades.magnitud,cn as system.data.sqlclient.sqlConnection,tr as system.data.sqlclient.sqltransaction) as boolean
    'End Function

    Public Function SelectAll() As List(Of Entidades.Magnitud)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Magnitud)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Magnitud)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Magnitud)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Magnitud)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Magnitud)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Magnitud)
        Return obj.SelectxId(id)
    End Function
    Public Function SelectCbo() As List(Of Entidades.Magnitud)
        Return obj.SelectCbo()
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoTarjeta
    Dim obj As New DAO.DAOTipoTarjeta


    Public Function SelectCboxIdCaja(ByVal IdCaja As Integer) As List(Of Entidades.TipoTarjeta)
        Return obj.SelectCboxIdCaja(IdCaja)
    End Function


    Public Function SelectAll() As List(Of Entidades.TipoTarjeta)
        Return obj.SelectAll()
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.TipoTarjeta)
        Return obj.SelectAllActivo()
    End Function

    Public Function SelectAllInactivo() As List(Of Entidades.TipoTarjeta)
        Return obj.SelectAllInactivo()
    End Function

    Public Function InsertT(ByVal x As Entidades.TipoTarjeta) As Boolean
        Return obj.InsertT(x)
    End Function
    Public Function UpdateT(ByVal x As Entidades.TipoTarjeta) As Boolean
        Return obj.UpdateT(x)
    End Function

    Private P As New Entidades.TipoTarjeta
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.TipoTarjeta) As Boolean
        'x va a ser el objeto a filtrar

        If (x.Id = P.Id Or P.Id = 0) _
        And (x.Nombre.ToLower.Trim.StartsWith(P.Nombre.ToLower.Trim) Or P.Nombre = "") _
        And (x.Estado = P.Estado Or (Not P.Estado.HasValue)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.TipoTarjeta, ByVal List As List(Of Entidades.TipoTarjeta)) As List(Of Entidades.TipoTarjeta)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function ValidarEntidadxNombre(ByVal filtro As Entidades.TipoTarjeta, ByVal List As List(Of Entidades.TipoTarjeta)) As Boolean
        'X es el valor a verificar, 

        Dim x As New Entidades.TipoTarjeta
        x.Id = filtro.Id
        x.Nombre = filtro.Nombre

        Dim List2 As List(Of Entidades.TipoTarjeta)
        List2 = ValidarLista(x, List)

        If List2.Count > 0 Then 'Existen registros  
            Return False
        Else
            Return True
        End If


    End Function
    Private Function ValidarEntidadxParametros(ByVal x As Entidades.TipoTarjeta) As Boolean
        'Se utiliza a la hora de insertar o eliminar
        'x va a ser el objeto a filtrar
        'En este caso no se cuenta el registro analizado
        If (x.Id <> P.Id) _
        And (x.Nombre.ToLower.Trim = P.Nombre.ToLower.Trim Or P.Nombre = "") Then
            'And (x.Estado = P.Estado Or (Not P.Estado.HasValue)) Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ValidarLista(ByVal filtro As Entidades.TipoTarjeta, ByVal List As List(Of Entidades.TipoTarjeta)) As List(Of Entidades.TipoTarjeta)
        P = filtro
        Return List.FindAll(AddressOf ValidarEntidadxParametros)

    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*********************   LUNES 24052010

Imports System.Data.SqlClient
Public Class MovCuentaPorPagar
    Private obj As New DAO.DAOMovCuentaPorPagar
    Public Function SelectDatosCancelacion(ByVal IdDocumento As String) As List(Of Entidades.MovCuentaPorPagar)
        Return obj.SelectDatosCancelacion(IdDocumento)
    End Function
    Public Function CR_MovCuenta_CXP_DT(ByVal IdEmpresa As Integer, ByVal IdPersona As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal OpcionDeudas As Integer) As DataTable
        Return obj.CR_MovCuenta_CXP_DT(IdEmpresa, IdPersona, FechaInicio, FechaFin, OpcionDeudas)
    End Function
    Public Function CR_MovCuenta_CXP_Lista(ByVal IdEmpresa As Integer, ByVal IdPersona As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal OpcionDeudas As Integer) As List(Of Entidades.MovCuentaPorPagar)
        Return obj.CR_MovCuenta_CXP_Lista(IdEmpresa, IdPersona, FechaInicio, FechaFin, OpcionDeudas)
    End Function
    Public Function ReporteCancelacionBancos(ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.Documento)
        Return obj.ReporteCancelacionBancos(FechaInicio, FechaFin)
    End Function
    Public Function ReporteProgramaciones(ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.ProgramacionReporte)
        Return obj.ReporteProgramaciones(FechaInicio, FechaFin)
    End Function

    Public Function MovCuentaPorPagar_SelectxParams_DataTable(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, ByVal IdPersona As Integer, ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date, ByVal Opcion_Programacion As Integer, ByVal Opcion_Deuda As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal IdTipoDocumento As Integer, ByVal Solicitud As Integer) As DataTable
        Return obj.MovCuentaPorPagar_SelectxParams_DataTable(IdEmpresa, IdTienda, IdMoneda, IdPersona, FechaVctoInicio, FechaVctoFin, Opcion_Programacion, Opcion_Deuda, PageIndex, PageSize, IdTipoDocumento, Solicitud)
    End Function
    ''nuevo metodo agregado 03/10/2014
    Public Function SelectProgramacionPagoRQxFechas(ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date) As DataTable
        Return obj.SelectProgramacionPagoRQxFechas(FechaVctoInicio, FechaVctoFin)
    End Function


    Public Function _MovCuentaPorPagarRQCheckes(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, ByVal IdPersona As Integer, ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date, ByVal serie As Integer, ByVal codigo As Integer, ByVal Opcion_Programacion As Integer, ByVal Opcion_Deuda As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal IdTipoDocumento As Integer) As DataTable
        Return obj._MovCuentaPorPagarRQCheckes(IdEmpresa, IdTienda, IdMoneda, IdPersona, FechaVctoInicio, FechaVctoFin, serie, codigo, Opcion_Programacion, Opcion_Deuda, PageIndex, PageSize, IdTipoDocumento)
    End Function
    Public Function SelectMovCuentaporPagarxIddocumento(ByVal IdDocumento As Integer) As Entidades.MovCuentaPorPagar
        Return obj.SelectMovCuentaporPagarxIddocumento(IdDocumento)
    End Function
    Public Function MovCuentaPorPagar_SelectxParams_DataTableReport(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, _
                                                                    ByVal IdPersona As Integer, ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date, _
                                                                    ByVal serie As Integer, ByVal codigo As Integer, ByVal Opcion_Programacion As Integer, _
                                                                    ByVal Opcion_Deuda As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, _
                                                                    ByVal IdTipoDocumento As Integer) As Entidades.be_Requerimiento_x_pagar
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAOMovCuentaPorPagar).MovCuentaPorPagar_SelectxParams_DataTableReport(IdEmpresa, IdTienda, IdMoneda, IdPersona, FechaVctoInicio, FechaVctoFin, serie, _
                                                                   codigo, Opcion_Programacion, Opcion_Deuda, PageIndex, PageSize, IdTipoDocumento, cn)
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.MovCuentaPorPagar)
        Return obj.SelectxIdDocumento(IdDocumento)
    End Function

    Public Function SelectDeudaxIdProveedor(ByVal IdProveedor As Integer) As List(Of Entidades.MovCuentaPorPagar)
        Return obj.SelectDeudaxIdProveedor(IdProveedor)
    End Function
    Public Function SelectDeudaxIdProveedorxIdMoneda(ByVal IdProveedor As Integer, ByVal IdMoneda As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)
        Return obj.SelectDeudaxIdProveedorxIdMoneda(IdProveedor, IdMoneda)
    End Function
    Public Function SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancel(ByVal IdProveedor As Integer, _
    ByVal IdMoneda As Integer, ByVal IdTipoDocCancel As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)
        Return obj.SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancel(IdProveedor, IdMoneda, IdTipoDocCancel)
    End Function
    Public Function SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelRC(ByVal IdProveedor As Integer, _
ByVal IdMoneda As Integer, ByVal IdTipoDocCancel As Integer, ByVal idTienda As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)
        Return obj.SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelRC(IdProveedor, IdMoneda, IdTipoDocCancel, idTienda)
    End Function
    Public Function SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelTwo(ByVal IdProveedor As Integer, _
    ByVal IdMoneda As Integer, ByVal IdTipoDocCancel As Integer, ByVal fecha As Date) As List(Of Entidades.Documento_MovCuentaPorPagar)
        Return obj.SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelTwo(IdProveedor, IdMoneda, IdTipoDocCancel, fecha)
    End Function
    Public Function SelectDocumentosCancelacionBancosxSustentar(ByVal IdProveedor As Integer, _
   ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal IdTienda As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)
        Return obj.SelectDocumentosCancelacionBancosxSustentar(IdProveedor, FechaInicio, FechaFin, IdTienda)
    End Function

    Public Function SelectAbonosxIdMovCtaPP(ByVal IdMovCtaPP As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)
        Return obj.SelectAbonosxIdMovCtaPP(IdMovCtaPP)
    End Function

End Class


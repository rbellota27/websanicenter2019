﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoConceptoBanco

    Dim obj As New DAO.DAOTipoConceptoBanco

    Public Function InsertaTipoConceptoBanco(ByVal TipoConceptoBanco As Entidades.TipoConceptoBanco) As Boolean
        Return obj.InsertaTipoConceptoBanco(TipoConceptoBanco)
    End Function
    Public Function ActualizaTipoConceptoBanco(ByVal TipoConceptoBanco As Entidades.TipoConceptoBanco) As Boolean
        Return obj.ActualizaTipoConceptoBanco(TipoConceptoBanco)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectAll()
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectAllActivo()
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectAllInactivo()
    End Function

    Public Function SelectxId(ByVal idTipoConceptoBanco As Integer) As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectxId(idTipoConceptoBanco)
    End Function

    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectInactivoxNombre(nombre)
    End Function

    Public Function SelectxFactor(ByVal Factor As Integer) As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectxFactor(Factor)
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoConceptoBanco)
        Return obj.SelectCbo()
    End Function

    Private P As New Entidades.TipoConceptoBanco
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.TipoConceptoBanco) As Boolean
        'x va a ser el objeto a filtrar

        If (x.Id = P.Id Or P.Id = 0) _
        And (x.Descripcion.ToLower.Trim.StartsWith(P.Descripcion.ToLower.Trim) Or P.Descripcion = "") _
        And (x.Factor = P.Factor Or P.Factor = 0) _
        And (x.Estado = P.Estado Or (Not P.Estado.HasValue)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.TipoConceptoBanco, ByVal List As List(Of Entidades.TipoConceptoBanco)) As List(Of Entidades.TipoConceptoBanco)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function ValidarEntidadxNombre(ByVal filtro As Entidades.TipoConceptoBanco, ByVal List As List(Of Entidades.TipoConceptoBanco)) As Boolean
        'X es el valor a verificar, 

        Dim x As New Entidades.TipoConceptoBanco
        x.Id = filtro.Id
        x.Nombre = filtro.Nombre

        Dim List2 As List(Of Entidades.TipoConceptoBanco)
        List2 = ValidarLista(x, List)

        If List2.Count > 0 Then 'Existen registros  
            Return False
        Else
            Return True
        End If

    End Function

    Private Function ValidarEntidadxParametros(ByVal x As Entidades.TipoConceptoBanco) As Boolean
        'Se utiliza a la hora de insertar o eliminar
        'x va a ser el objeto a filtrar
        'En este caso no se cuenta el registro analizado
        If (x.Id <> P.Id) _
        And (x.Nombre.ToLower.Trim = P.Nombre.ToLower.Trim Or P.Nombre = "") Then
            'And (x.Estado = P.Estado Or (Not P.Estado.HasValue))
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ValidarLista(ByVal filtro As Entidades.TipoConceptoBanco, ByVal List As List(Of Entidades.TipoConceptoBanco)) As List(Of Entidades.TipoConceptoBanco)
        P = filtro
        Return List.FindAll(AddressOf ValidarEntidadxParametros)

    End Function

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class RegimenOperacion
    Public obj As New DAO.DAORegimenOperacion
    Public Function InsertaRegimenOperacion(ByVal regimenoperacion As Entidades.RegimenOperacion) As Boolean
        Return obj.InsertaRegimenOperacion(regimenoperacion)
    End Function
    Public Function ActualizaRegimenOperacion(ByVal regimenoperacion As Entidades.RegimenOperacion) As Boolean
        Return obj.ActualizaRegimenOperacion(regimenoperacion)
    End Function
    Public Function SelectAll() As List(Of Entidades.RegimenOperacion)
        Return obj.SelectAll()
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.RegimenOperacion)
        Return obj.SelectAllActivo()
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.RegimenOperacion)
        Return obj.SelectAllInactivo()
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.RegimenOperacion)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.RegimenOperacion)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.RegimenOperacion)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxIdRegimen(ByVal ro_id As Integer) As List(Of Entidades.RegimenOperacion)
        Return obj.SelectxIdRegimen(ro_id)
    End Function
    Public Function SelectxIdTipoOperacion(ByVal idTipoOperacion As Integer) As List(Of Entidades.RegimenOperacion)
        Return obj.SelectxIdTipoOperacion(idTipoOperacion)
    End Function

    '-------------------------------------------------------------------------------------

    Private P As New Entidades.RegimenOperacion
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.RegimenOperacion) As Boolean
        'x va a ser el objeto a filtrar

        If (x.IdRegimen = P.IdRegimen Or P.IdRegimen = 0) _
        And (x.IdTipoOperacion = P.IdTipoOperacion Or P.IdTipoOperacion = 0) _
        And (x.CuentaContable.StartsWith(P.CuentaContable) Or P.CuentaContable = "") _
        And (x.Estado = P.Estado Or P.Estado = "2") Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.RegimenOperacion, ByVal List As List(Of Entidades.RegimenOperacion)) As List(Of Entidades.RegimenOperacion)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function ValidarEntidad(ByVal filtro As Entidades.RegimenOperacion, ByVal List As List(Of Entidades.RegimenOperacion)) As Boolean
        'X es el valor a verificar, se analizan sus llaves primarias, en este caso IdRegimen y IdTipoOperacion
        'Aqui se pone las otras variables en su forma general, para encontrar todos los resultados 
        Dim x As New Entidades.RegimenOperacion
        x.IdRegimen = filtro.IdRegimen
        x.IdTipoOperacion = filtro.IdTipoOperacion
        x.CuentaContable = ""
        x.Estado = "2" 'Todos los estados

        Dim List2 As List(Of Entidades.RegimenOperacion)
        List2 = FiltrarLista(x, List)
        If List2.Count > 0 Then 'Existen registros con esas llaves primarias 
            Return False
        Else
            Return True
        End If

    End Function

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 28-Set-2009
'Hora    : 04:27 pm
'*************************************************


Public Class TipoOperacion
    Private obj As New DAO.DAOTipoOperacion
    Public Function InsertaTipoOperacion(ByVal tipooperacion As Entidades.TipoOperacion) As Boolean
        Return obj.InsertaTipoOperacion(tipooperacion)
    End Function
    Public Function ActualizaTipoOperacion(ByVal tipooperacion As Entidades.TipoOperacion) As Boolean
        Return obj.ActualizaTipoOperacion(tipooperacion)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoOperacion)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoOperacion)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoOperacion)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoOperacion)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoOperacion)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoOperacion)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoOperacion)
        Return obj.SelectxId(id)
    End Function
    Public Function SelectxCodSunat(ByVal cod As String) As List(Of Entidades.TipoOperacion)
        Return obj.SelectxCodSunat(cod)
    End Function

    Public Function SelectCboxIdMotivoTraslado(ByVal IdMotivoTraslado As Integer) As List(Of Entidades.TipoOperacion)
        Return obj.SelectCboxIdMotivoTraslado(IdMotivoTraslado)
    End Function

    Public Function SelectCboxIdTipoDocumento(ByVal IdTipoDocumento As Integer) As List(Of Entidades.TipoOperacion)
        Return obj.SelectCboxIdTipoDocumento(IdTipoDocumento)
    End Function

    Public Function ObtenerAlmacen(ByVal IdTienda As Integer, ByVal IdTipoOperacion As Integer) As DataTable

        Return obj.OBTENER_ALMACEN(IdTienda, IdTipoOperacion)
    End Function
    Public Function ObtenerTipoOperacion(ByVal IdTienda As Integer, ByVal IdTipoOperacion As Integer) As DataTable

        Return obj.OBTENER_TIPOOPERACION(IdTienda, IdTipoOperacion)
    End Function
End Class

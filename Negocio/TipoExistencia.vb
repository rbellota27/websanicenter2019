﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'LUNES 01 DE FEBRERO
Public Class TipoExistencia
    Dim objDaoTipoEx As New DAO.DAOTipoExistencia

    Public Function UpdateTipoExistencia_Area(ByVal tipoexistencia As Entidades.TipoExistencia, _
                                    ByVal listaLinea_Area As List(Of Entidades.Linea_Area), _
                                    ByVal tieneRelacion As Integer) As Boolean
        Return objDaoTipoEx.UpdateTipoExistencia_Area(tipoexistencia, listaLinea_Area, tieneRelacion)
    End Function

    Public Function InsertaTipoExistencia(ByVal tipoexistencia As Entidades.TipoExistencia) As Boolean
        Return objDaoTipoEx.InsertaTipoExistencia(tipoexistencia)
    End Function
    Public Function ActualizaTipoExistencia(ByVal tipoexistencia As Entidades.TipoExistencia) As Boolean
        Return objDaoTipoEx.ActualizaTipoExistencia(tipoexistencia)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoExistencia)
        Return objDaoTipoEx.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoExistencia)
        Return objDaoTipoEx.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoExistencia)
        Return objDaoTipoEx.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoExistencia)
        Return objDaoTipoEx.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoExistencia)
        Return objDaoTipoEx.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoExistencia)
        Return objDaoTipoEx.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoExistencia)
        Return objDaoTipoEx.SelectxId(id)
    End Function
    Public Function SelectxCodSunat(ByVal codsunat As String) As List(Of Entidades.TipoExistencia)
        Return objDaoTipoEx.SelectxCodSunat(codsunat)
    End Function
End Class

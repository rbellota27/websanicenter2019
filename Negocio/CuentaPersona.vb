﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** martes03 agosto 2010 

Imports System.Data.SqlClient
Public Class CuentaPersona
    Private objConexion As New DAO.Conexion
    Private objCuentaPersona As New DAO.DAOCuentaPersona

    Public Function CuentaPersonaCtaEmpresaTienda(ByVal dni As String, ByVal ruc As String, _
                                      ByVal RazonApe As String, ByVal tipo As Integer, _
                                      ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                      ByVal idrol As Integer, ByVal estado As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.CuentaPersonaCtaEmpresaTienda(dni, ruc, RazonApe, tipo, pageindex, pagesize, idrol, estado, IdEmpresa, IdTienda)
    End Function

    Public Function SelectxParams(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.SelectxParams(IdPersona, IdEmpresa, IdTienda)
    End Function
    Public Function SelectxcomprasParams(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.SelectxcomprasParams(IdPersona, IdEmpresa, IdTienda)
    End Function
    Public Function ULTIMAS_VENTASFUNCION(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.ULTIMAS_VENTASFUNCION(IdPersona, IdEmpresa, IdTienda)
    End Function
    Public Function Mostrarventaxpersona(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.Mostrarventaxpersona(IdPersona, IdEmpresa, IdTienda)
    End Function

    Public Function Mostrarventaxpersonaxaño(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.Mostrarventaxpersonaxaño(IdPersona, IdEmpresa, IdTienda)
    End Function
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.SelectxIdPersona(IdPersona)
    End Function
    Public Function SelectxIdCtaPersona(ByVal IdCtaPersona As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.SelectxIdCtaPersona(IdCtaPersona)
    End Function
    Public Function SelectGrillaCxCResumenDocumento(ByVal idpersona As Integer, ByVal idmoneda As Integer) As List(Of Entidades.CxCResumenView)
        Return objCuentaPersona.SelectGrillaCxCResumenDocumento(idpersona, idmoneda)
    End Function

    Public Function SelectAllConsultaxIdPersona(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Return objCuentaPersona.SelectAllConsultaxIdPersona(IdPersona, IdEmpresa, IdTienda)
    End Function
    Public Function GrabarCuentaPersona(ByVal lista As List(Of Entidades.CuentaPersona), ByVal idPersona As Integer) As Boolean
        Dim hecho As Boolean = False
        '**************** obtengo la lista de registros a eliminar
        Dim listaOld As List(Of Entidades.CuentaPersona) = objCuentaPersona.SelectxIdPersona(idPersona)
        Dim i As Integer = listaOld.Count - 1
        While i >= 0
            Dim j As Integer = lista.Count - 1
            While j >= 0
                If listaOld.Item(i).IdCuentaPersona = lista.Item(j).IdCuentaPersona Then
                    listaOld.RemoveAt(i)
                    Exit While
                End If
                j = j - 1
            End While
            i = i - 1
        End While

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************************ elimino los registros de caja usuario
            For i = 0 To listaOld.Count - 1
                objCuentaPersona.DeletexIdCuentaPersona(listaOld(i).IdCuentaPersona, cn, tr)
            Next

            '************************** inserto/actualizo los registros
            Dim objDaoUtil As New DAO.DAOUtil
            For i = 0 To lista.Count - 1
                If objDaoUtil.ValidarExistenciaxTablax1Campo(cn, tr, "CuentaPersona", "IdCuentaPersona", lista(i).IdCuentaPersona.ToString) > 0 Then
                    '*********** actualizo
                    objCuentaPersona.UpdateCuentaPersonaT(lista(i), cn, tr)
                Else
                    '*********** inserto
                    objCuentaPersona.InsertaCuentaPersonaT(lista(i), cn, tr)
                End If
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function fnInsTblCuentaPersona(ByVal poBECuentaPersona As Entidades.CuentaPersona) As Integer
        Return objCuentaPersona.fnInsTblCuentaPersona(poBECuentaPersona)
    End Function
    Public Function fnUdpTblCuentaPersona(ByVal poBECuentaPersona As Entidades.CuentaPersona) As Integer
        Return objCuentaPersona.fnUdpTblCuentaPersona(poBECuentaPersona)
    End Function
End Class

﻿Imports System.Data.SqlClient

Public Class ValidDocRelacionado

    Private objDaoDocumentoNotaCredito As New DAO.DAOValidDocRelacionado
    Private objConexion As New DAO.Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private tr As SqlTransaction


    Public Function SelectDocRefValidNotaCredito(ByVal IdCliente As Integer, ByVal FechaI As Date, ByVal FechaF As Date, _
                                                 ByVal IdTienda As Integer, ByVal serie As Integer, ByVal codigo As Integer, _
                                                 ByVal opcion As Integer, ByVal ValDocRelacionado As Boolean,
                                                 ByVal idTipoDocumento As Integer) As List(Of Entidades.DocumentoValid)
        Return objDaoDocumentoNotaCredito.SelectDocRefValidNotaCredito(IdCliente, FechaI, FechaF, IdTienda, serie, codigo, opcion, _
                                                                       ValDocRelacionado, idTipoDocumento)
    End Function

End Class

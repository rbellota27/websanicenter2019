﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Talla
    Private objDaoTalla As New DAO.DAOTalla
    Public Function InsertaTalla(ByVal talla As Entidades.Talla) As Boolean
        Return objDaoTalla.InsertaTalla(talla)
    End Function
    Public Function ActualizaTalla(ByVal talla As Entidades.Talla) As Boolean
        Return objDaoTalla.ActualizaTalla(talla)
    End Function
    Public Function SelectAllActivo_Cbo() As List(Of Entidades.Talla)
        Return objDaoTalla.SelectAllActivo_Cbo
    End Function
    Public Function SelectAll() As List(Of Entidades.Talla)
        Return objDaoTalla.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Talla)
        Return objDaoTalla.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Talla)
        Return objDaoTalla.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Talla)
        Return objDaoTalla.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Talla)
        Return objDaoTalla.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Talla)
        Return objDaoTalla.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Talla)
        Return objDaoTalla.SelectxId(id)
    End Function
End Class

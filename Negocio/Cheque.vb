﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*********************   MARTES 15062010

Imports System.Data.SqlClient

Public Class Cheque

    Private objDaoCheque As New DAO.DAOCheque
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction

    Public Function Update_Estado_EstadoMovxIdCheque(ByVal IdCheque As Integer, ByVal Estado As Boolean, ByVal EstadoMov As String, ByVal Observacion As String) As Boolean
        Return objDaoCheque.Update_Estado_EstadoMovxIdCheque(IdCheque, Estado, EstadoMov, Observacion)
    End Function

    Public Function Cheque_ValSelect_AddDatoCancelacion_DT(ByVal IdCliente As Integer, ByVal FechaMov As Date, ByVal Val_DocumentoRelacionado As Boolean) As DataTable
        Return objDaoCheque.Cheque_ValSelect_AddDatoCancelacion_DT(IdCliente, FechaMov, Val_DocumentoRelacionado)
    End Function

    Public Function Registrar(ByVal listaCheque As List(Of Entidades.Cheque), ByVal listaCheque_DocumentoRef As List(Of Entidades.Cheque_DocumentoRef)) As Boolean

        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            For i As Integer = 0 To listaCheque.Count - 1

                listaCheque(i).IdCheque = objDaoCheque.Registrar(listaCheque(i), cn, tr)

            Next

            Dim objDaoCheque_DocumentoRef As New DAO.DAOCheque_DocumentoRef
            For i As Integer = 0 To listaCheque.Count - 1

                For x As Integer = 0 To listaCheque_DocumentoRef.Count - 1

                    listaCheque_DocumentoRef(x).IdCheque = listaCheque(i).IdCheque
                    objDaoCheque_DocumentoRef.Registrar(listaCheque_DocumentoRef(x), cn, tr)

                Next

            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function


    Public Function Cheque_SelectxParams_DT(ByVal IdCheque As Integer, ByVal IdCliente As Integer, ByVal IdBanco As Integer, ByVal IdUsuario As Integer, ByVal IdUsuarioSupervisor As Integer, ByVal FechaInicio_Find As Date, ByVal FechaFin_Find As Date, ByVal FechaCobrar_Inicio As Date, ByVal FechaCobrar_Fin As Date, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Estado As Boolean, ByVal EstadoMov As String) As DataTable
        Return objDaoCheque.Cheque_SelectxParams_DT(IdCheque, IdCliente, IdBanco, IdUsuario, IdUsuarioSupervisor, FechaInicio_Find, FechaFin_Find, FechaCobrar_Inicio, FechaCobrar_Fin, IdEmpresa, IdTienda, Estado, EstadoMov)
    End Function

    Public Function Cheque_SelectxParams_ValBanco(ByVal IdCheque As Integer, ByVal IdCliente As Integer, ByVal IdBanco As Integer, ByVal IdUsuario As Integer, ByVal IdUsuarioSupervisor As Integer, ByVal FechaInicio_Find As Date, ByVal FechaFin_Find As Date, ByVal FechaCobrar_Inicio As Date, ByVal FechaCobrar_Fin As Date, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Estado As Boolean, ByVal EstadoMov As String) As DataTable
        Return objDaoCheque.Cheque_SelectxParams_ValBanco(IdCheque, IdCliente, IdBanco, IdUsuario, IdUsuarioSupervisor, FechaInicio_Find, FechaFin_Find, FechaCobrar_Inicio, FechaCobrar_Fin, IdEmpresa, IdTienda, Estado, EstadoMov)
    End Function


    Public Function Cheque_SelectxIdCheque(ByVal IdCheque As Integer) As Entidades.Cheque
        Return objDaoCheque.Cheque_SelectxIdCheque(IdCheque)
    End Function

End Class

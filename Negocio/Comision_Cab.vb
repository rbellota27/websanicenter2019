﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM


Imports System.Data.SqlClient

Public Class Comision_Cab

    Private objDaoComisionCab As New DAO.DAOComision_Cab
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction


    Public Function Comision_Cab_cbo(ByVal IdTipoComision As Integer) As List(Of Entidades.Comision_Cab)
        Return objDaoComisionCab.Comision_Cab_cbo(IdTipoComision)
    End Function


    Public Function Comision_Cab_SelectxIdComisionCab(ByVal IdComisionCab As Integer) As Entidades.Comision_Cab
        Return objDaoComisionCab.Comision_Cab_SelectxIdComisionCab(IdComisionCab)
    End Function
    Public Function Comision_Cab_SelectxParams_DT(ByVal IdComisionCab As Integer) As DataTable
        Return objDaoComisionCab.Comision_Cab_SelectxParams_DT(IdComisionCab)
    End Function
    Public Function Registrar(ByVal objComisionCab As Entidades.Comision_Cab, ByVal listaComisionista As List(Of Entidades.Comisionista), ByVal listaComision_TipoPrecioV As List(Of Entidades.ComisionCab_TipoPrecioV), ByVal listaRestriccion_Cliente As List(Of Entidades.Restriccion_Cliente)) As Boolean

        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim listaComisionista_Aux As New List(Of Entidades.Comisionista)
            Dim listaTipoPv_Aux As New List(Of Entidades.ComisionCab_TipoPrecioV)
            Dim listaRestriccion_Cliente_Aux As New List(Of Entidades.Restriccion_Cliente)

            If (objComisionCab.IdComisionCab <> Nothing) Then

                '****************** PROCESO DE EDICIÓN

                '************** COMISIONISTA
                listaComisionista_Aux = (New DAO.DAOComisionista).Comisionista_SelectxParams(objComisionCab.IdComisionCab)

                For i As Integer = listaComisionista_Aux.Count - 1 To 0 Step -1
                    For j As Integer = listaComisionista.Count - 1 To 0 Step -1

                        If (listaComisionista_Aux(i).IdComisionista = listaComisionista(j).IdComisionista) Then
                            listaComisionista_Aux.RemoveAt(i)
                            Exit For
                        End If

                    Next
                Next

                '************** TIPO PV
                listaTipoPv_Aux = (New DAO.DAOComisionCab_TipoPrecioV).ComisionCab_TipoPrecioV_SelectxParams(objComisionCab.IdComisionCab)

                For i As Integer = listaTipoPv_Aux.Count - 1 To 0 Step -1
                    For j As Integer = listaComision_TipoPrecioV.Count - 1 To 0 Step -1

                        If (listaTipoPv_Aux(i).IdTipoPV = listaComision_TipoPrecioV(j).IdTipoPV) Then
                            listaTipoPv_Aux.RemoveAt(i)
                            Exit For
                        End If

                    Next
                Next

                '************** RESTRICCION CLIENTE
                listaRestriccion_Cliente_Aux = (New DAO.DAORestriccion_Cliente).RestriccionComision_Cliente_SelectxParams(objComisionCab.IdComisionCab)

                For i As Integer = listaRestriccion_Cliente_Aux.Count - 1 To 0 Step -1
                    For j As Integer = listaRestriccion_Cliente.Count - 1 To 0 Step -1

                        If (listaRestriccion_Cliente_Aux(i).IdCliente = listaRestriccion_Cliente(j).IdCliente) Then
                            listaRestriccion_Cliente_Aux.RemoveAt(i)
                            Exit For
                        End If

                    Next
                Next

            End If

            objComisionCab.IdComisionCab = objDaoComisionCab.Registrar(objComisionCab, cn, tr)

            Dim objDaoComisionista As New DAO.DAOComisionista
            For i As Integer = 0 To listaComisionista.Count - 1

                listaComisionista(i).IdComisionCab = objComisionCab.IdComisionCab
                objDaoComisionista.Registrar(listaComisionista(i), cn, tr)

            Next

            Dim objDaoComision_TipoPV As New DAO.DAOComisionCab_TipoPrecioV
            For i As Integer = 0 To listaComision_TipoPrecioV.Count - 1

                listaComision_TipoPrecioV(i).IdComisionCab = objComisionCab.IdComisionCab
                objDaoComision_TipoPV.Registrar(listaComision_TipoPrecioV(i), cn, tr)

            Next

            Dim objDaoRestriccion_Cliente As New DAO.DAORestriccion_Cliente
            For i As Integer = 0 To listaRestriccion_Cliente.Count - 1

                listaRestriccion_Cliente(i).IdComisionCab = objComisionCab.IdComisionCab
                objDaoRestriccion_Cliente.Registrar(listaRestriccion_Cliente(i), cn, tr)

            Next

            '*************************** ELIMINAMOS LOS DATOS
            For i As Integer = 0 To listaComisionista_Aux.Count - 1
                objDaoComisionista.DeletexIdComisionCabxIdPersona(listaComisionista_Aux(i).IdComisionCab, listaComisionista_Aux(i).IdComisionista, cn, tr)
            Next
            For i As Integer = 0 To listaTipoPv_Aux.Count - 1
                objDaoComision_TipoPV.DeletexIdComisionCabxIdTipoPv(listaTipoPv_Aux(i).IdComisionCab, listaTipoPv_Aux(i).IdTipoPV, cn, tr)
            Next
            For i As Integer = 0 To listaRestriccion_Cliente_Aux.Count - 1
                objDaoRestriccion_Cliente.DeletexIdComisionCabxIdPersona(listaRestriccion_Cliente_Aux(i).IdComisionCab, listaRestriccion_Cliente_Aux(i).IdCliente, cn, tr)
            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

End Class

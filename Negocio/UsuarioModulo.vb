﻿Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class UsuarioModulo

    Dim objDAOUsuario As New DAO.DAOUsuarioModulo
    Dim objConexion As DAO.Conexion
    Public Function SelectUsuarioModulo(ByVal var As String) As List(Of Entidades.UsuarioModulo)
        Return objDAOUsuario.SelectUsuarioModulo(var)
    End Function

    Public Function Textbox() As List(Of Entidades.Trabajador)
        Return objDAOUsuario.TextBox()
    End Function
    Public Function SelectxIdUsuario(ByVal IdUsuario As Integer) As List(Of Entidades.UsuarioModulo)
        Return objDAOUsuario.SelectxIdUsuario(IdUsuario)
    End Function

    Public Function AgregarUsuario(ByVal objUsuario As Entidades.UsuarioModulo) As Boolean

        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim idproceso As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            idproceso = objDAOUsuario.AgregarUsuarioModulo(objUsuario, cn, "i", tr)

            If idproceso = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If


            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho

    End Function

    Public Function SelectxNombreUsuario(ByVal nombre As String)
        Return objDAOUsuario.SelectxNombreUsuario(nombre)
    End Function

    Public Function ActualizaUsuario(ByVal objusuario As Entidades.UsuarioModulo) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If Not objDAOUsuario.ActualizaUsuario(objusuario, cn, "a", tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function EliminarUsuario(ByVal id As Integer) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If Not objDAOUsuario.EliminaIdUsuario(id, cn, "e", tr) Then
                Throw New Exception("Fracasó el proceso de Eliminación.")
            End If
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
End Class

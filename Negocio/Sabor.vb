﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Sabor
    Private objDaoSabor As New DAO.DAOSabor
    Public Function InsertaSabor(ByVal sabor As Entidades.Sabor) As Boolean
        Return objDaoSabor.InsertaSabor(sabor)
    End Function
    Public Function ActualizaSabor(ByVal sabor As Entidades.Sabor) As Boolean
        Return objDaoSabor.ActualizaSabor(sabor)
    End Function
    Public Function SaborSelectCbo() As List(Of Entidades.Sabor)
        Return objDaoSabor.SaborSelectCbo()
    End Function
    Public Function SelectAll() As List(Of Entidades.Sabor)
        Return objDaoSabor.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Sabor)
        Return objDaoSabor.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Sabor)
        Return objDaoSabor.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Sabor)
        Return objDaoSabor.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Sabor)
        Return objDaoSabor.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Sabor)
        Return objDaoSabor.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Sabor)
        Return objDaoSabor.SelectxId(id)
    End Function
End Class

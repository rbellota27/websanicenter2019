﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class MotivoTraslado
    Private obj As New DAO.DAOMotivoTraslado
    Private objconexion As New DAO.Conexion
    Dim objTraslado As New DAO.DAOMotivoTraslado
    'Public Function InsertaMotivoTraslado(ByVal motivotraslado As Entidades.MotivoTraslado) As Boolean
    '    Return obj.InsertaMotivoTraslado(motivotraslado)
    'End Function
    'InsertaMotivoTraslado_tipoOpera
    Public Function InsertaMotivoTraslado_tipoOpera(ByVal TipoOperacion As Entidades.MotivoTraslado, ByVal lista As List(Of Entidades.MotivoT_TipoOperacion)) As Boolean
        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            '************ Insertar Motivo Traslado
            Dim IdTraslado As Integer = obj.InsertaMotivoTraslado(TipoOperacion, cn, tr)

            '************ Insertar Lista
            Dim objDaoMoviT_TipoOperacion As New DAO.DAOMotivoT_TipoOperacion
            Dim objTraslado As New Entidades.MotivoT_TipoOperacion
            objDaoMoviT_TipoOperacion.InsertaMotivoT_TipoOperacion(cn, tr, lista, IdTraslado)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho
    End Function
    Public Function ActualizaMotivoT_TipoOperacion(ByVal traslado As Entidades.MotivoTraslado, ByVal lista As List(Of Entidades.MotivoT_TipoOperacion)) As Boolean
        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '*************  ActualizaMotivoT_TipoOperacion  ********************
            If Not (obj.ActualizaMotivoTraslado(traslado, cn, tr)) Then
                Throw New Exception
            End If

            '*************  Elimina ActualizaMotivoT_TipoOperacion ************************
            Dim obJDaoTipoOperacion As New DAO.DAOMotivoT_TipoOperacion
            If (Not obJDaoTipoOperacion.DeletexIdMotivoT(cn, tr, traslado.Id)) Then
                Throw New Exception
            End If
            '****************************** Insertar Lista  **************************************
            obJDaoTipoOperacion.InsertaMotivoT_TipoOperacion(cn, tr, lista, traslado.Id)
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    'Public Function ActualizaMotivoTraslado(ByVal motivotraslado As Entidades.MotivoTraslado) As Boolean
    '    Return obj.ActualizaMotivoTraslado(motivotraslado)
    'End Function
    Public Function ActualizaMotivoTraslado(ByVal traslado As Entidades.MotivoTraslado, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Return objTraslado.ActualizaMotivoTraslado(traslado, cn, tr)
    End Function
    'Public Function SelectCboTipoOperacion() As List(Of Entidades.MotivoT_TipoOperacion)
    '    Return obj.SelectCboTipoOperacion
    'End Function
    Public Function SelectAll() As List(Of Entidades.MotivoTraslado)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.MotivoTraslado)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.MotivoTraslado)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.MotivoTraslado)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.MotivoTraslado)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.MotivoTraslado)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.MotivoTraslado)
        Return obj.SelectxId(id)
    End Function
    Public Function SelectComprometedorxIdMotivoT(ByVal IdMotivoT As Integer) As Boolean
        Return obj.SelectComprometedorxIdMotivoT(IdMotivoT)
    End Function
End Class

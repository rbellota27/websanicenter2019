﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 30-Octubre-2009
'Hora    : 07:30 pm
'*************************************************
Imports System.Data.SqlClient

Public Class CondicionPago
    Private obj As New DAO.DAOCondicionPago
    Private objCC As New DAO.DAOCondicionComercial
    Dim objMedioP_CondicionP As New DAO.DAOMedioP_CondicionP
    Dim objconexion As New DAO.Conexion
    Dim objMedioPago As New DAO.DAOMedioPago

    Public Function SelectAllCondicionesComerciales(ByVal idDocumento As Integer, ByVal tipo As Integer) As List(Of Entidades.CondicionComercial)
        Return objCC.SelectAll(idDocumento, tipo)
    End Function

    Public Function InsertaCondicionPago(ByVal condicionpago As Entidades.CondicionPago) As Boolean
        Return obj.InsertaCondicionPago(condicionpago)
    End Function
    Public Function ActualizaCondicionPago(ByVal condicionpago As Entidades.CondicionPago) As Boolean
        Return obj.ActualizaCondicionPago(condicionpago)
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.CondicionPago)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAll() As List(Of Entidades.CondicionPago)
        Return obj.SelectAll
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.CondicionPago)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.CondicionPago)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.CondicionPago)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.CondicionPago)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.CondicionPago)
        Return obj.SelectxId(id)
    End Function
    Public Function InsertaMedioP_CondicionP(ByVal MedioP As Entidades.CondicionPago, ByVal lista As List(Of Entidades.MedioPago_CondicionPago)) As Boolean
        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            '************ Insertar Motivo Traslado
            Dim IdCondicionP As Integer = obj.InsertaMedioPago_CP(MedioP, cn, tr)

            '************ Insertar Lista
            Dim objDaoMedioPago_CondicionPago As New DAO.DAOMedioP_CondicionP
            Dim objCondicionP As New Entidades.MedioPago_CondicionPago
            objDaoMedioPago_CondicionPago.InsertaListaCondicionPago(cn, tr, lista, IdCondicionP)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho
    End Function
    Public Function ActualizaMedioPago_CondicionP(ByVal CondicionP As Entidades.CondicionPago, ByVal lista As List(Of Entidades.MedioPago_CondicionPago)) As Boolean
        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '*************   Actualiza Condición de Pago  ********************
            If Not (obj.ActualizaMedioPago_condicionP(CondicionP, cn, tr)) Then
                Throw New Exception
            End If

            '*************  Elimina ActualizaMotivoT_TipoOperacion ************************
            Dim obJDaoMedioPago As New DAO.DAOMedioP_CondicionP
            If (Not obJDaoMedioPago.DeletexCondicionPago_MedioPagoBorrar(cn, tr, CondicionP.Id)) Then
                Throw New Exception
            End If
            '****************************** Insertar Lista  **************************************
            obJDaoMedioPago.InsertaListaCondicionPago(cn, tr, lista, CondicionP.Id)
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
End Class

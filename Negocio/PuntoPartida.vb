﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class PuntoPartida
    Private obj As New DAO.DAOPuntoPartida


    Public Function SelectxIdDocumento(ByVal iddocumento As Integer) As Entidades.PuntoPartida

        Return obj.SelectxIdDocumento(iddocumento)

    End Function


    Public Function SelectxIdDocumentoDireccionP(ByVal iddocumento As Integer) As Entidades.PuntoPartida

        Return obj.SelectxIdDocumentoDireccionP(iddocumento)

    End Function
End Class

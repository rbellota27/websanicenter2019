﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Public Class ValorReferencia_Transporte

    Private objDaoValorRef_T As New DAO.DAOValorReferencia_T

    Public Function Registrar(ByVal objValorReferenciaT As Entidades.ValorReferencia_Transporte) As Boolean
        Return objDaoValorRef_T.Registrar(objValorReferenciaT)
    End Function

    Public Function SelectxParams_DT(ByVal IdValorReferenciaT As Integer, ByVal Estado As Boolean, Optional ByVal descripcion As String = Nothing) As DataTable
        Return objDaoValorRef_T.SelectxParams_DT(IdValorReferenciaT, Estado, descripcion)
    End Function

    Public Function SelectxIdValorReferenciaT(ByVal IdValorReferenciaT As Integer) As Entidades.ValorReferencia_Transporte
        Return objDaoValorRef_T.SelectxIdValorReferenciaT(IdValorReferenciaT)
    End Function

End Class

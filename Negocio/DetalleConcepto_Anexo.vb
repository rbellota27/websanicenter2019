﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM

Public Class DetalleConcepto_Anexo
    Private objDaoDCA As New DAO.DAODetalleConcepto_Anexo

    Public Function DetalleDocumento_Anexo_SelectDocumentoxIdDocumentoxIdDetalleConcepto(ByVal IdDocumento As Integer, ByVal IdDetalleConcepto As Integer) As List(Of Entidades.Documento)
        Return objDaoDCA.DetalleDocumento_Anexo_SelectDocumentoxIdDocumentoxIdDetalleConcepto(IdDocumento, IdDetalleConcepto)
    End Function

End Class

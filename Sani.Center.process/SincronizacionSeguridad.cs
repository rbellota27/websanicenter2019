﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Configuration;
using Sani.Service;
using System.Collections;


namespace Sani.Center.procces
{
    public class SincronizacionSeguridad
    {
        public int LogeoValidar(String usuario, string password)
        {
            string serverurl = System.Configuration.ConfigurationManager.AppSettings["ServicioSani"].ToString();
            string actionurl = serverurl+"proceso/svcSeguridad.svc";
            ServiceProxy proxy = new ServiceProxy(serverurl);
            var objRequest = new { usuario = usuario, password = password };
            string resultado1 = proxy.executeAction<Object>(actionurl, objRequest);
            string resultado = JsonConvert.DeserializeObject<string>(resultado1);
            return Convert.ToInt32(resultado);
        }
    }
}

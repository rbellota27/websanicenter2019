﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft;
using Newtonsoft.Json;
using System.Configuration;

namespace Sani.Service
{
    public class ServiceProxy
    {

        static HttpClientHandler authtHandler = new HttpClientHandler()
        {
            Credentials = CredentialCache.DefaultNetworkCredentials
        };

        HttpClient client = new HttpClient(authtHandler);

        string server = string.Empty;
        public ServiceProxy(string url)
        {
            this.server = url;
            client.BaseAddress = new Uri(server);
            // client.DefaultRequestHeaders.Accept.Clear();
            //  client.DefaultRequestHeaders.ExpectContinue = true;
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public string executeAction<T>(string action, T content)
        {
            string request = string.Empty;
            string responseString = string.Empty;

            try
            {
                request = JsonConvert.SerializeObject(content);
                var httpContent = new StringContent(request, Encoding.UTF8, "application/json");

                var response = client.PostAsync(action, httpContent).Result;
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;
                    // by calling .Result you are synchronously reading the result
                    responseString = responseContent.ReadAsStringAsync().Result;
                }
                else
                {
                    throw new Exception("No se pudo completar la invocación al servicio remoto");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return responseString;
        }

    }

}

﻿using Newtonsoft.Json;
using System.Text;

namespace Sani.Service
{
    public class WebUtils
    {
        public static byte[] WebResponse(object o)
        {
            var js = new System.Web.Script.Serialization.JavaScriptSerializer();
            string responseserialize = JsonConvert.SerializeObject(o, Newtonsoft.Json.Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,

                                });
            return Encoding.UTF8.GetBytes(responseserialize);
        }

    }
}


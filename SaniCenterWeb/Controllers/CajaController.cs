﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class CajaController : Controller
    {
        // GET: Caja
        public ActionResult Index()
        {
            int idpersona = Convert.ToInt32(Session["IdUsuario"]);
            if (idpersona == 0)
            {
                RedirectToAction("IniciarSesion", "Seguridad");
            }
            return View();
        }
        public ActionResult Facturación()
        {
          int idpersona = Convert.ToInt32(Session["IdUsuario"]);
          if (idpersona == 0)
            {
                RedirectToAction("IniciarSesion", "Seguridad");
            }
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Negocio;
using Entidades;


namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        Negocio.AliasMant objAliasMant = new Negocio.AliasMant();
        Negocio.Documento objDocumento = new Negocio.Documento();
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            List<Entidades.AliasMant> listadoalias = objAliasMant.SelectAllActivo();
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
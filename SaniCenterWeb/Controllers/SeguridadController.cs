﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Negocio;
using Entidades;
using Sani.Center.procces;
namespace WebApplication2.Controllers
{
    public class SeguridadController : Controller
    {
        public string idtienda;
        public int idcaja;
        public int idpersona;
        public int idperfil;

        SincronizacionSeguridad sincroseguridad = new SincronizacionSeguridad();

        // GET: Seguridad        
        public ActionResult Index()
        {            
            return View();
        }
        [HttpGet]
        public ActionResult IniciarSesion()
        {
            return View();
        }
        [HttpPost]
        public ActionResult IniciarSesionSanicenter()
        {
            Negocio.Usuario objusuario = new Negocio.Usuario();
            Util objutil = new Util();
            Negocio.Caja objcaja = new Negocio.Caja();
            Negocio.ParametroGeneral objparametro = new Negocio.ParametroGeneral();
            string usuario = Request.Form["user"].ToString();
            string password = Request.Form["password"].ToString();
          //  int idpersona = objusuario.ValidarIdentidad(usuario.Trim(), password.Trim());
           idpersona  = objusuario.ValidarIdentidad(usuario, password);
           idperfil = objusuario.ValidarIdentidadPerfil(usuario, password);
            if (idpersona!=0)
            {
                idtienda = objutil.ValidarTiendaxUsuario(idpersona);
                idcaja = objcaja.SelectIdCajaxIdPersona(idpersona);
                Session.Add("IdPersona", idpersona);
                Session.Add("IdUsuario", idpersona);
                Session.Add("IdCaja", idcaja);
                Session.Add("IdPerfil", idperfil);
                Session.Add("idTiendaUsuario", idtienda);
                //listaparametros = objparametro.getValorParametroGeneral();
            }
            else
            {
                return RedirectToAction("IniciarSesion", "Seguridad");
            }     
            
            return RedirectToAction("Index", "Home");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using Negocio;
using Entidades;
using Sani.Service;
using System.ServiceModel.Web;

namespace WcfService2.proceso
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "svcSeguridad" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione svcSeguridad.svc o svcSeguridad.svc.cs en el Explorador de soluciones e inicie la depuración.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class svcSeguridad : IsvcSeguridad
    {
        Negocio.Usuario objusuario = new Negocio.Usuario();
        public Stream Login(string usuario,string password)
        {
            int idpersona = objusuario.ValidarIdentidad(usuario, password);            
            return new MemoryStream(WebUtils.WebResponse(Convert.ToString(idpersona)));
        }
    }
    [ServiceContract(Namespace = "")]
    public interface IsvcSeguridad
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Stream Login(string usuario, string password);
    }
}

﻿Imports System.Data.SqlClient

Public Class DAOOrdenCompraPreliminar
    Dim ObjConexion As New Conexion
    Dim HDAO As New DAO.HelperDAO
    Dim objDaoDCC As DAO.DAODocumento_CondicionC
    Private Util As New DAO.DAOUtil

    Public Sub Anular(ByVal IdDocumento As Integer, ByVal DeleteDetalleDocumento As Boolean, ByVal DeletePuntoLlegada As Boolean, ByVal DeleteCondicionC As Boolean, ByVal DeleteObservacion As Boolean, ByVal DeleteMontoRegimen As Boolean, ByVal DeleteCuentaPorPagar As Boolean, ByVal Anular As Boolean, ByVal sqlcn As SqlConnection, ByVal sqltr As SqlTransaction)
        Dim cmd As New SqlCommand("_DocumentoOrdenCompra_DeshacerMov", sqlcn, sqltr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@DeleteDetalleDocumento", DeleteDetalleDocumento)
        cmd.Parameters.AddWithValue("@DeletePuntoLlegada", DeletePuntoLlegada)
        cmd.Parameters.AddWithValue("@DeleteCondicionC", DeleteCondicionC)
        cmd.Parameters.AddWithValue("@DeleteObservacion", DeleteObservacion)
        cmd.Parameters.AddWithValue("@DeleteMontoRegimen", DeleteMontoRegimen)
        cmd.Parameters.AddWithValue("@DeleteCuentaPorPagar", DeleteCuentaPorPagar)
        cmd.Parameters.AddWithValue("@Anular", Anular)

        cmd.ExecuteNonQuery()

    End Sub

    Public Function Validar_CosteoImportacion(ByVal IdDocumento As Integer) As Nullable(Of Integer)

        Dim IdDocumentoRef As Object
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoOrdenCompra_Val33", cn)
        cn.Open()
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        Try
            IdDocumentoRef = cmd.ExecuteScalar()
            If IsNumeric(IdDocumentoRef) Then
                Return CInt(IdDocumentoRef)
            End If
        Catch ex As Exception
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Nothing
    End Function


    Public Function InsertBD_OrdenCompra(ByVal obj As Entidades.Documento, ByVal cn As SqlConnection, _
                           ByVal tr As SqlTransaction) As String
        Dim parametro() As SqlParameter = New SqlParameter(30) {}

        parametro(0) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametro(0).Value = IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision)

        parametro(1) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        parametro(1).Value = IIf(obj.FechaAEntregar = Nothing, DBNull.Value, obj.FechaAEntregar)

        parametro(2) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        parametro(2).Value = IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc)

        parametro(3) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
        parametro(3).Value = IIf(obj.Descuento = Nothing, DBNull.Value, obj.Descuento)

        parametro(4) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
        parametro(4).Value = IIf(obj.SubTotal = Nothing, DBNull.Value, obj.SubTotal)

        parametro(5) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
        parametro(5).Value = IIf(obj.IGV = Nothing, DBNull.Value, obj.IGV)

        parametro(6) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        parametro(6).Value = IIf(obj.Total = Nothing, DBNull.Value, obj.Total)

        parametro(7) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        parametro(7).Value = IIf(obj.TotalLetras = Nothing, DBNull.Value, obj.TotalLetras)

        parametro(8) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
        parametro(8).Value = IIf(obj.TotalAPagar = Nothing, DBNull.Value, obj.TotalAPagar)

        parametro(9) = New SqlParameter("@IdPersona", SqlDbType.Int) '** Proveedor
        parametro(9).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)

        parametro(10) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        parametro(10).Value = IIf(obj.IdUsuario = Nothing, DBNull.Value, obj.IdUsuario)

        parametro(11) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        parametro(11).Value = IIf(obj.IdDestinatario = Nothing, DBNull.Value, obj.IdDestinatario)

        parametro(12) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        parametro(12).Value = IIf(obj.IdEstadoDoc = Nothing, DBNull.Value, obj.IdEstadoDoc)

        parametro(13) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametro(13).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)

        parametro(14) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
        parametro(14).Value = IIf(obj.LugarEntrega = Nothing, DBNull.Value, obj.LugarEntrega)

        parametro(15) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        parametro(15).Value = IIf(obj.IdTipoOperacion = Nothing, DBNull.Value, obj.IdTipoOperacion)

        parametro(16) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametro(16).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)

        parametro(17) = New SqlParameter("@IdSerie", SqlDbType.Int)
        parametro(17).Value = IIf(obj.IdSerie = Nothing, DBNull.Value, obj.IdSerie)

        parametro(18) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        parametro(18).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)

        parametro(19) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        parametro(19).Value = IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento)

        parametro(20) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
        parametro(20).Value = IIf(obj.IdEstadoCancelacion = Nothing, DBNull.Value, obj.IdEstadoCancelacion)

        parametro(21) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
        parametro(21).Value = IIf(obj.IdEstadoEntrega = Nothing, DBNull.Value, obj.IdEstadoEntrega)

        parametro(22) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
        parametro(22).Value = IIf(obj.doc_Importacion.ToString = "", DBNull.Value, CBool(obj.doc_Importacion))

        parametro(23) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        parametro(23).Value = IIf(obj.IdRemitente = Nothing, DBNull.Value, obj.IdRemitente)

        parametro(24) = New SqlParameter("@IdArea", SqlDbType.Int)
        parametro(24).Value = IIf(obj.IdArea = Nothing, DBNull.Value, obj.IdArea)

        parametro(25) = New SqlParameter("@anex_PrecImportacion", SqlDbType.Int)
        parametro(25).Value = IIf(obj.anex_PrecImportacion = Nothing, DBNull.Value, obj.anex_PrecImportacion)

        parametro(26) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.Date)
        parametro(26).Value = IIf(obj.FechaCancelacion = Nothing, DBNull.Value, obj.FechaCancelacion)

        parametro(27) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametro(27).Value = IIf(obj.IdCondicionPago = Nothing, DBNull.Value, obj.IdCondicionPago)

        parametro(28) = New SqlParameter("@idcentrocosto", SqlDbType.VarChar)
        parametro(28).Value = IIf(obj.Direccion = Nothing, DBNull.Value, obj.Direccion)

        parametro(29) = New SqlParameter("@enviarCuentaPorPagar", SqlDbType.Bit)
        parametro(29).Value = obj.EnviarTablaCuenta

        parametro(30) = New SqlParameter("@anex_igv", SqlDbType.Bit)
        parametro(30).Value = obj.anex_igv

        Dim cmd As New SqlCommand("_InsertOrdenCompra", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            Return cmd.ExecuteScalar().ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertarOrdenCompra(ByVal ObjDocumento As Entidades.Documento, _
                                       ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                       ByVal obs As Entidades.Observacion, _
                                       ByVal objMonto As Entidades.MontoRegimen, _
                                       ByVal listaCondicionComercial As List(Of Entidades.CondicionComercial)) _
                                       As String
        Dim cad As String = String.Empty
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)



        Try
            ' ****************** DOCUMENTO
            cad = Me.InsertBD_OrdenCompra(ObjDocumento, cn, tr)
            ObjDocumento.Id = CInt(cad.Split(CChar(","))(0))

            ' ****************** DETALLEDOCUMENTO
            Dim ObjDaoDetalleDocumento As New DAO.DAODetalleDocumento
            Dim ObjDAOAnexo_DetalleDocumento As New DAO.DAOAnexo_DetalleDocumento

            For i As Integer = 0 To listaDetalle.Count - 1
                With listaDetalle.Item(i)
                    .IdDocumento = ObjDocumento.Id
                    Dim iddetalle As Integer = ObjDaoDetalleDocumento.InsertaDetalleDocumentoTonos(cn, listaDetalle(i), ObjDocumento.Id, tr)
                    Dim objAnexo_DetalleDoc As New Entidades.Anexo_DetalleDocumento
                    With objAnexo_DetalleDoc
                        .IdDocumento = listaDetalle(i).IdDocumento
                        .IdKit = listaDetalle(i).IdKit
                        .ComponenteKit = listaDetalle(i).Kit
                        .IdDetalleDocumento = iddetalle
                    End With

                    ObjDAOAnexo_DetalleDocumento._Anexo_DetalleDocumentoInsert(objAnexo_DetalleDoc, cn, tr)

                End With
            Next


            Dim montoRegimen As New DAO.DAOMontoRegimen

            If ObjDocumento.CompPercepcion = True And objMonto.Monto > 0 Then
                objMonto.IdDocumento = ObjDocumento.Id
                montoRegimen.InsertaMontoRegimen(cn, objMonto, tr)
            End If



            updatePrecioCompraProducto(ObjDocumento.IdMoneda, listaDetalle, cn, tr)

            Dim objPuntoLLegada As New Entidades.PuntoLlegada
            Dim objDaoPuntoLLegada As New DAO.DAOPuntoLlegada

            With objPuntoLLegada
                .IdDocumento = ObjDocumento.Id
                .IdTienda = ObjDocumento.IdTienda
                .IdAlmacen = ObjDocumento.LugarEntrega
            End With

            objDaoPuntoLLegada.InsertaPuntoLlegadaT(cn, objPuntoLLegada, tr)

            For i As Integer = 0 To listaCondicionComercial.Count - 1

                listaCondicionComercial.Item(i).IdDocumento = ObjDocumento.Id
                listaCondicionComercial.Item(i).IdTipoDocumento = ObjDocumento.IdTipoDocumento

                objDaoDCC = New DAODocumento_CondicionC
                objDaoDCC.Documento_CondicionC_Insert(cn, tr, listaCondicionComercial(i))
            Next



            '******************** OBSERVACIONES
            If (obs IsNot Nothing) Then
                Dim objObservacion As New DAO.DAOObservacion

                obs.IdDocumento = ObjDocumento.Id
                objObservacion.InsertaObservacionT(cn, tr, obs)

            End If


            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function

    Public Sub ActualizarOrdenCompra(ByVal cn As SqlConnection, ByVal obj As Entidades.Documento, ByVal tr As SqlTransaction)

        Dim parametro() As SqlParameter = New SqlParameter(25) {}

        parametro(0) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametro(0).Value = IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision)

        parametro(1) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        parametro(1).Value = IIf(obj.FechaAEntregar = Nothing, DBNull.Value, obj.FechaAEntregar)

        parametro(2) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        parametro(2).Value = IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc)

        parametro(3) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
        parametro(3).Value = IIf(obj.Descuento = Nothing, DBNull.Value, obj.Descuento)

        parametro(4) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
        parametro(4).Value = IIf(obj.SubTotal = Nothing, DBNull.Value, obj.SubTotal)

        parametro(5) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
        parametro(5).Value = IIf(obj.IGV = Nothing, DBNull.Value, obj.IGV)

        parametro(6) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        parametro(6).Value = IIf(obj.Total = Nothing, DBNull.Value, obj.Total)

        parametro(7) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        parametro(7).Value = IIf(obj.TotalLetras = Nothing, DBNull.Value, obj.TotalLetras)

        parametro(8) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
        parametro(8).Value = IIf(obj.TotalAPagar = Nothing, DBNull.Value, obj.TotalAPagar)

        parametro(9) = New SqlParameter("@IdPersona", SqlDbType.Int) '** Proveedor
        parametro(9).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)

        parametro(10) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        parametro(10).Value = IIf(obj.IdUsuario = Nothing, DBNull.Value, obj.IdUsuario)

        parametro(11) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        parametro(11).Value = IIf(obj.IdDestinatario = Nothing, DBNull.Value, obj.IdDestinatario)

        parametro(12) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametro(12).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)

        parametro(13) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
        parametro(13).Value = IIf(obj.LugarEntrega = Nothing, DBNull.Value, obj.LugarEntrega)

        parametro(14) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametro(14).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)

        parametro(15) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        parametro(15).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)

        parametro(16) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
        parametro(16).Value = IIf(obj.doc_Importacion.ToString = "", DBNull.Value, CBool(obj.doc_Importacion))

        parametro(17) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        parametro(17).Value = IIf(obj.IdRemitente = Nothing, DBNull.Value, obj.IdRemitente)

        parametro(18) = New SqlParameter("@IdArea", SqlDbType.Int)
        parametro(18).Value = IIf(obj.IdArea = Nothing, DBNull.Value, obj.IdArea)

        parametro(19) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        parametro(19).Value = IIf(obj.Id = Nothing, DBNull.Value, obj.Id)

        parametro(20) = New SqlParameter("@anex_PrecImportacion", SqlDbType.Int)
        parametro(20).Value = IIf(obj.anex_PrecImportacion = Nothing, DBNull.Value, obj.anex_PrecImportacion)

        parametro(21) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.Date)
        parametro(21).Value = IIf(obj.FechaCancelacion = Nothing, DBNull.Value, obj.FechaCancelacion)

        parametro(22) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametro(22).Value = IIf(obj.IdCondicionPago = Nothing, DBNull.Value, obj.IdCondicionPago)

        parametro(23) = New SqlParameter("@idcentrocosto", SqlDbType.VarChar)
        parametro(23).Value = IIf(obj.Direccion = Nothing, DBNull.Value, obj.Direccion)

        parametro(24) = New SqlParameter("@enviarCuentaPorPagar", SqlDbType.Bit)
        parametro(24).Value = IIf(obj.EnviarTablaCuenta = Nothing, DBNull.Value, obj.EnviarTablaCuenta)

        parametro(25) = New SqlParameter("@anex_igv", SqlDbType.Bit)
        parametro(25).Value = obj.anex_igv

        Dim cmd As New SqlCommand("_ActualizarOrdenCompraPreliminar", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Function UpdateDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                   ByVal objObs As Entidades.Observacion, ByVal objMonto As Entidades.MontoRegimen, _
                                   ByVal listaCondicionComercial As List(Of Entidades.CondicionComercial)) As Boolean

        Dim ObjDaoDetalleDocumento As New DAO.DAODetalleDocumento

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
        Try


            Anular(objDocumento.Id, True, True, True, True, True, True, False, cn, tr)

            Me.ActualizarOrdenCompra(cn, objDocumento, tr)

            Dim ObjDAOAnexo_DetalleDocumento As New DAOAnexo_DetalleDocumento

            For i As Integer = 0 To listaDetalle.Count - 1
                With listaDetalle.Item(i)
                    .IdDocumento = objDocumento.Id
                    Dim iddetalle As Integer = ObjDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalle(i), tr)

                    Dim objAnexo_DetalleDoc As New Entidades.Anexo_DetalleDocumento
                    With objAnexo_DetalleDoc
                        .IdDocumento = listaDetalle(i).IdDocumento
                        .IdKit = listaDetalle(i).IdKit
                        .ComponenteKit = listaDetalle(i).Kit
                        .IdDetalleDocumento = iddetalle
                    End With

                    ObjDAOAnexo_DetalleDocumento._Anexo_DetalleDocumentoInsert(objAnexo_DetalleDoc, cn, tr)



                End With
            Next

            updatePrecioCompraProducto(objDocumento.IdMoneda, listaDetalle, cn, tr)

            If objDocumento.CompPercepcion = True And objMonto.Monto > 0 Then
                Dim objregimen As New DAO.DAOMontoRegimen
                objMonto.IdDocumento = objDocumento.Id
                objregimen.InsertaMontoRegimen(cn, objMonto, tr)
            End If

            Dim objPuntoLLegada As New Entidades.PuntoLlegada
            Dim objDaoPuntoLLegada As New DAO.DAOPuntoLlegada

            With objPuntoLLegada
                .IdDocumento = objDocumento.Id
                .IdTienda = objDocumento.IdTienda
                .IdAlmacen = objDocumento.LugarEntrega
            End With

            objDaoPuntoLLegada.InsertaPuntoLlegadaT(cn, objPuntoLLegada, tr)

            For i As Integer = 0 To listaCondicionComercial.Count - 1

                listaCondicionComercial.Item(i).IdDocumento = objDocumento.Id
                listaCondicionComercial.Item(i).IdTipoDocumento = objDocumento.IdTipoDocumento

                objDaoDCC = New DAODocumento_CondicionC
                objDaoDCC.Documento_CondicionC_Insert(cn, tr, listaCondicionComercial(i))

            Next

            '******************** OBSERVACIONES
            If (objObs IsNot Nothing) Then
                Dim objObservacion As New DAO.DAOObservacion

                objObs.IdDocumento = objDocumento.Id
                objObservacion.InsertaObservacionT(cn, tr, objObs)

            End If

            tr.Commit()
            Return True

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Sub updatePrecioCompraProducto(ByVal idmoneda As Integer, ByVal lista As List(Of Entidades.DetalleDocumento), ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Dim cmdx As SqlCommand
        For Each obj As Entidades.DetalleDocumento In lista
            cmdx = New SqlCommand("_updatePrecioCompraProducto", cnx, trx)
            cmdx.CommandType = CommandType.StoredProcedure
            cmdx.Parameters.AddWithValue("@idproducto", obj.IdProducto)
            cmdx.Parameters.AddWithValue("@precio", obj.CostoMovIngreso)
            cmdx.Parameters.AddWithValue("@idmoneda", idmoneda)

            cmdx.ExecuteNonQuery()
        Next
    End Sub

    'Validar y poder sacar el IDTipoDocumento
    Public Function ValidarIDCC(ByVal idtipo As Integer, ByVal nombre As String) As Integer
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_ID_CONDICIONC", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipo", idtipo)
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Tipo As Integer
                Do While lector.Read
                    Tipo = CInt(IIf(IsDBNull(lector.Item("IdCondicionCC")) = True, 0, lector.Item("IdCondicionCC")))
                Loop
                lector.Close()
                Return Tipo
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaRelacionDocumentosNuevos(ByVal IdDocumento1 As Integer, ByVal IdDocumento2 As Integer) As Boolean
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim T As SqlTransaction = Nothing

        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdDoc1", SqlDbType.Int)
            ArrayParametros(0).Value = IdDocumento1

            ArrayParametros(1) = New SqlParameter("@IdDoc2", SqlDbType.Int)
            ArrayParametros(1).Value = IdDocumento2

            HDAO.UpdateT(cn, "USP_RELACION_DOCUMENTOS", ArrayParametros, T)
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function


    Public Function RegularizacionOC(ByVal doc As Integer) As Boolean

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim Tr As SqlTransaction = Nothing

        Dim parametro() As SqlParameter = New SqlParameter(0) {}

        parametro(0) = New SqlParameter("@Doc", SqlDbType.Int)
        parametro(0).Value = doc

        Dim cmd As New SqlCommand("USP_REGULARIZACION_OC", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function


    Public Function RegularizacionCI(ByVal doc As Integer) As Boolean

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim Tr As SqlTransaction = Nothing

        Dim parametro() As SqlParameter = New SqlParameter(0) {}

        parametro(0) = New SqlParameter("@Doc", SqlDbType.Int)
        parametro(0).Value = doc

        Dim cmd As New SqlCommand("USP_REGULARIZACION_CI", cn, Tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function


    Public Function RegularizacionGR(ByVal doc As Integer) As Boolean

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim Tr As SqlTransaction = Nothing

        Dim parametro() As SqlParameter = New SqlParameter(0) {}

        parametro(0) = New SqlParameter("@Doc", SqlDbType.Int)
        parametro(0).Value = doc

        Dim cmd As New SqlCommand("USP_REGULARIZACION_GR", cn, Tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function



    Public Function RegularizacionInforme(ByVal doc As Integer) As Boolean

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim Tr As SqlTransaction = Nothing

        Dim parametro() As SqlParameter = New SqlParameter(0) {}

        parametro(0) = New SqlParameter("@Doc", SqlDbType.Int)
        parametro(0).Value = doc

        Dim cmd As New SqlCommand("USP_REGULARIZACION_INFORME", cn, Tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

End Class

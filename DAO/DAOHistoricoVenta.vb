﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'************************   MARTES 20 ABRIL 2010 HORA 05_51 PM









Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOHistoricoVenta

    Public Sub HistoricoVenta_GenerarCargaDatosxAnioMes(ByVal Anio As Integer, ByVal Mes As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)


        Dim p() As SqlParameter = New SqlParameter(5) {}
        p(0) = New SqlParameter("@Anio", SqlDbType.Int)
        p(0).Value = Anio
        p(1) = New SqlParameter("@Mes", SqlDbType.Int)
        p(1).Value = Mes
        p(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        p(2).Value = IdEmpresa
        p(3) = New SqlParameter("@IdTienda", SqlDbType.Int)
        p(3).Value = IdTienda
        p(4) = New SqlParameter("@IdLinea", SqlDbType.Int)
        p(4).Value = IdLinea
        p(5) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
        p(5).Value = IdSubLinea
        
        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_HistoricoVenta_GenerarCargaDatosxAnioMes", p)


    End Sub


End Class

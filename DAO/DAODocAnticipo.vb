﻿Imports System.Data.SqlClient

Public Class DAODocAnticipo
    Dim ObjConexion As New Conexion
    Dim HDAO As New DAO.HelperDAO
    Public Function UpdateEstado(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
            ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(IdDocumento = Nothing, DBNull.Value, IdDocumento)

            HDAO.UpdateT(cn, "_DocumentoAnticipo_UpdateEstado", ArrayParametros, tr)
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        Finally

        End Try
    End Function

End Class

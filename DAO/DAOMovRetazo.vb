'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOMovRetazo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaMovRetazo(ByVal movretazo As Entidades.MovRetazo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = movretazo.IdProducto
        ArrayParametros(1) = New SqlParameter("@IdRetazo", SqlDbType.Int)
        ArrayParametros(1).Value = movretazo.IdRetazo
        ArrayParametros(2) = New SqlParameter("@mr_Fecha", SqlDbType.DateTime)
        ArrayParametros(2).Value = movretazo.Fecha
        ArrayParametros(3) = New SqlParameter("@mr_Cantidad", SqlDbType.Decimal)
        ArrayParametros(3).Value = movretazo.Cantidad
        ArrayParametros(4) = New SqlParameter("@mr_Factor", SqlDbType.Int)
        ArrayParametros(4).Value = movretazo.Factor
        ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(5).Value = movretazo.IdTienda
        ArrayParametros(6) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(6).Value = movretazo.IdAlmacen
        ArrayParametros(7) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
        ArrayParametros(7).Value = movretazo.IdMovAlmacen
        ArrayParametros(8) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(8).Value = movretazo.IdEmpresa
        Return HDAO.Insert(cn, "_MovRetazoInsert", ArrayParametros)
    End Function
    Public Function ActualizaMovRetazo(ByVal movretazo As Entidades.MovRetazo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = movretazo.IdProducto
        ArrayParametros(1) = New SqlParameter("@IdRetazo", SqlDbType.Int)
        ArrayParametros(1).Value = movretazo.IdRetazo
        ArrayParametros(2) = New SqlParameter("@mr_Fecha", SqlDbType.DateTime)
        ArrayParametros(2).Value = movretazo.Fecha
        ArrayParametros(3) = New SqlParameter("@mr_Cantidad", SqlDbType.Decimal)
        ArrayParametros(3).Value = movretazo.Cantidad
        ArrayParametros(4) = New SqlParameter("@mr_Factor", SqlDbType.Int)
        ArrayParametros(4).Value = movretazo.Factor
        ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(5).Value = movretazo.IdTienda
        ArrayParametros(6) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(6).Value = movretazo.IdAlmacen
        ArrayParametros(7) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
        ArrayParametros(7).Value = movretazo.IdMovAlmacen
        ArrayParametros(8) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(8).Value = movretazo.IdEmpresa
        Return HDAO.Update(cn, "_MovRetazoUpdate", ArrayParametros)
    End Function
End Class

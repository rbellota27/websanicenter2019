﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTiendaArea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaTiendaAreaT(ByVal CN As SqlConnection, ByVal ltiendarea As List(Of Entidades.TiendaArea), ByVal T As SqlTransaction)
        Dim TiendaArea As Entidades.TiendaArea
        For Each TiendaArea In ltiendarea
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(0).Value = TiendaArea.IdTienda
            ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
            ArrayParametros(1).Value = TiendaArea.Id
            HDAO.InsertaT(CN, "_TiendaAreaInsert", ArrayParametros, T)
        Next
    End Sub
    Public Function DeletexIdTienda(ByVal cn As SqlConnection, ByVal IdTienda As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_TiendaAreaDeletexIdTienda", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Sub GrabaTiendaAreaT(ByVal CN As SqlConnection, ByVal ltiendarea As List(Of Entidades.TiendaArea), ByVal T As SqlTransaction)
        Dim TiendaArea As Entidades.TiendaArea
        For Each TiendaArea In ltiendarea
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(0).Value = TiendaArea.IdTienda
            ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
            ArrayParametros(1).Value = TiendaArea.Id
            ArrayParametros(2) = New SqlParameter("@Estado", SqlDbType.Bit)
            ArrayParametros(2).Value = TiendaArea.EstadoAE
            ArrayParametros(3) = New SqlParameter("@IdCentroCosto", SqlDbType.VarChar)
            ArrayParametros(3).Value = IIf(TiendaArea.IdCentroCosto = Nothing, DBNull.Value, TiendaArea.IdCentroCosto)
            HDAO.InsertaT(CN, "InsUpd_TiendaArea", ArrayParametros, T)
        Next
    End Sub
End Class

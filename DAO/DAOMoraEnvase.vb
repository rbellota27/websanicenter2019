'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOMoraEnvase
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaMoraEnvase(ByVal moraenvase As Entidades.MoraEnvase) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@moe_TasaDia", SqlDbType.Decimal)
        ArrayParametros(0).Value = moraenvase.TasaDia
        ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(1).Value = moraenvase.IdPersona
        ArrayParametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(2).Value = moraenvase.IdMoneda
        Return HDAO.Insert(cn, "_MoraEnvaseInsert", ArrayParametros)
    End Function
    Public Function ActualizaMoraEnvase(ByVal moraenvase As Entidades.MoraEnvase) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@moe_TasaDia", SqlDbType.Decimal)
        ArrayParametros(0).Value = moraenvase.TasaDia
        ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(1).Value = moraenvase.IdPersona
        ArrayParametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(2).Value = moraenvase.IdMoneda
        ArrayParametros(3) = New SqlParameter("@IdMoraEnvase", SqlDbType.Int)
        ArrayParametros(3).Value = moraenvase.Id
        Return HDAO.Update(cn, "_MoraEnvaseUpdate", ArrayParametros)
    End Function
End Class

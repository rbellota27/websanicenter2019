'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.




'*******************      VIERNES   19 MARZO 2010 HORA 10_22 AM










Imports System.Data.SqlClient

Public Class DAOEmpleado
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Function EmpleadoSelectActivoxParams_Find(ByVal dni As String, ByVal ruc As String, _
                                    ByVal RazonApe As String, ByVal tipo As Integer, _
                                    ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                    ByVal idrol As Integer, ByVal estado As Integer) As List(Of Entidades.PersonaView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim paramentros() As SqlParameter = New SqlParameter(7) {}
        paramentros(0) = New SqlParameter("@Dni", SqlDbType.VarChar)
        paramentros(0).Value = dni
        paramentros(1) = New SqlParameter("@Ruc", SqlDbType.VarChar)
        paramentros(1).Value = ruc
        paramentros(2) = New SqlParameter("@razonApe", SqlDbType.VarChar)
        paramentros(2).Value = RazonApe
        paramentros(3) = New SqlParameter("@tipo", SqlDbType.Int)
        paramentros(3).Value = tipo
        paramentros(4) = New SqlParameter("@pageindex", SqlDbType.Int)
        paramentros(4).Value = pageindex
        paramentros(5) = New SqlParameter("@pageSize", SqlDbType.Int)
        paramentros(5).Value = pagesize
        paramentros(6) = New SqlParameter("@idRol", SqlDbType.Int)
        paramentros(6).Value = idrol
        paramentros(7) = New SqlParameter("@estado", SqlDbType.Int)
        paramentros(7).Value = estado

        Dim cmd As New SqlCommand("_EmpleadoSelectActivoxParams_Find", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(paramentros)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                        .Ruc = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                        .Estado = CStr(IIf(IsDBNull(lector("estado")), "---", lector("estado")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Function InsertaEmpleadoTransact(ByVal empleado As Entidades.Empleado, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As String
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(empleado.IdPersona = Nothing, DBNull.Value, empleado.IdPersona)
        ArrayParametros(1) = New SqlParameter("@IdCatEmpleado", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(empleado.IdCatEmpleado = Nothing, DBNull.Value, empleado.IdCatEmpleado)
        ArrayParametros(2) = New SqlParameter("@emp_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = IIf(empleado.Estado = Nothing, DBNull.Value, empleado.Estado)

        Dim cmd As New SqlCommand("_InsertarEmpleado", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Return cmd.ExecuteScalar.ToString
    End Function

    Public Function InsertaEmpleado(ByVal empleado As Entidades.Empleado) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = empleado.IdPersona
        ArrayParametros(1) = New SqlParameter("@emp_Sueldo", SqlDbType.Decimal)
        ArrayParametros(1).Value = empleado.Sueldo
        ArrayParametros(2) = New SqlParameter("@IdCatEmpleado", SqlDbType.Int)
        ArrayParametros(2).Value = empleado.IdCatEmpleado
        ArrayParametros(3) = New SqlParameter("@emp_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(3).Value = empleado.FechaAlta
        ArrayParametros(4) = New SqlParameter("@emp_FechaBaja", SqlDbType.DateTime)
        ArrayParametros(4).Value = empleado.FechaBaja
        ArrayParametros(5) = New SqlParameter("@emp_Estado", SqlDbType.Char)
        ArrayParametros(5).Value = empleado.Estado
        ArrayParametros(6) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        ArrayParametros(6).Value = empleado.IdMotivoBaja
        Return HDAO.Insert(cn, "_EmpleadoInsert", ArrayParametros)
    End Function

    Public Function ActualizaEmpleadoTransact(ByVal empleado As Entidades.Empleado, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = empleado.IdPersona
        ArrayParametros(1) = New SqlParameter("@IdCatEmpleado", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(empleado.IdCatEmpleado = Nothing, DBNull.Value, empleado.IdCatEmpleado)
        ArrayParametros(2) = New SqlParameter("@emp_FechaBaja", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(empleado.FechaBaja = Nothing, DBNull.Value, empleado.FechaBaja)
        ArrayParametros(3) = New SqlParameter("@emp_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = IIf(empleado.Estado = Nothing, DBNull.Value, empleado.Estado)
        ArrayParametros(4) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(empleado.IdMotivoBaja = Nothing, DBNull.Value, empleado.IdMotivoBaja)
        Dim cmd As New SqlCommand("_UpdateEmpleado", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return True
    End Function

    Public Function ActualizaEmpleado(ByVal empleado As Entidades.Empleado) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = empleado.IdPersona
        ArrayParametros(1) = New SqlParameter("@emp_Sueldo", SqlDbType.Decimal)
        ArrayParametros(1).Value = empleado.Sueldo
        ArrayParametros(2) = New SqlParameter("@IdCatEmpleado", SqlDbType.Int)
        ArrayParametros(2).Value = empleado.IdCatEmpleado
        ArrayParametros(3) = New SqlParameter("@emp_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(3).Value = empleado.FechaAlta
        ArrayParametros(4) = New SqlParameter("@emp_FechaBaja", SqlDbType.DateTime)
        ArrayParametros(4).Value = empleado.FechaBaja
        ArrayParametros(5) = New SqlParameter("@emp_Estado", SqlDbType.Char)
        ArrayParametros(5).Value = empleado.Estado
        ArrayParametros(6) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        ArrayParametros(6).Value = empleado.IdMotivoBaja
        Return HDAO.Update(cn, "_EmpleadoUpdate", ArrayParametros)
    End Function

    Public Function SelectActivoxParams(ByVal opcion As Integer, ByVal textoBusqueda As String) As List(Of Entidades.Empleado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EmpleadoSelectActivoxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Opcion", opcion)
        cmd.Parameters.AddWithValue("@Texto", textoBusqueda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Empleado)
                Do While lector.Read
                    Dim objEmpleado As New Entidades.Empleado
                    With objEmpleado

                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")) = True, "", lector("Nombre")))
                        .NomCargo = CStr(IIf(IsDBNull(lector("car_Nombre")) = True, "", lector("car_Nombre")))
                        .DNI = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))

                    End With
                    Lista.Add(objEmpleado)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdEmpleado(ByVal IdPersona As Integer) As Entidades.Empleado
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_ListarEmpleadoxID", cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@idEmpleado", IdPersona)
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                Dim objEmpleado As Entidades.Empleado = Nothing

                If lector.Read Then
                    objEmpleado = New Entidades.Empleado
                    With objEmpleado
                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        .Sueldo = CDec(IIf(IsDBNull(lector("emp_Sueldo")) = True, 0, lector("emp_Sueldo")))
                        .IdCatEmpleado = CInt(IIf(IsDBNull(lector("IdCatEmpleado")) = True, 0, lector("IdCatEmpleado")))
                        .FechaAlta = CDate(IIf(IsDBNull(lector("emp_FechaAlta")) = True, Nothing, lector("emp_FechaAlta")))
                        .FechaBaja = CDate(IIf(IsDBNull(lector("emp_FechaBaja")) = True, Nothing, lector("emp_FechaBaja")))
                        .Estado = CStr(IIf(IsDBNull(lector("emp_Estado")) = True, "0", lector("emp_Estado")))
                        .IdMotivoBaja = CInt(IIf(IsDBNull(lector("IdMotivoBaja")) = True, 0, lector("IdMotivoBaja")))
                        .EsEmpleado = True
                    End With

                End If
                lector.Close()
                Return objEmpleado
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.Empleado
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_EmpleadoSelectxIdPersona", cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                Dim objEmpleado As Entidades.Empleado = Nothing

                If lector.Read Then
                    objEmpleado = New Entidades.Empleado
                    With objEmpleado

                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")) = True, "", lector("Nombre")))
                        .NomCargo = CStr(IIf(IsDBNull(lector("car_Nombre")) = True, "", lector("car_Nombre")))
                        .DNI = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))

                    End With

                End If
                lector.Close()
                Return objEmpleado
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

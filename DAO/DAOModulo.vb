﻿Imports System.Data.SqlClient
Public Class DAOModulo

    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader

    Public Function InsertaModulo(ByVal modulo As Entidades.Modulo, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Integer
      
        Try
            Dim cmd As New SqlCommand("USP_MANT_MODULO", cn, tr)
            Dim a As Integer
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@id", modulo.IdModulo)
            cmd.Parameters.AddWithValue("@nombre", modulo.nombre)
            cmd.Parameters.AddWithValue("@var", var)
            a = cmd.ExecuteNonQuery
            If a = 0 Then
                Return False
            End If
            Return a
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectModulo(ByVal var As String) As List(Of Entidades.Modulo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_MANT_MODULO", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@var", var)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Modulo)
                Do While lector.Read
                    Dim modulo As New Entidades.Modulo
                    modulo.IdModulo = CInt(IIf(IsDBNull(lector.Item("IdModulo")) = True, 0, lector.Item("IdModulo")))
                    modulo.nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    Lista.Add(modulo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdModulo(ByVal idmodulo As Integer) As List(Of Entidades.Modulo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModuloSelectxIdModulo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdModulo", idmodulo)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Modulo)
                Do While lector.Read
                    Dim modulo As New Entidades.Modulo
                    modulo.IdModulo = CInt(IIf(IsDBNull(lector.Item("IdModulo")) = True, 0, lector.Item("IdModulo")))
                    modulo.nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))

                    Lista.Add(modulo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ActualizaModulo(ByVal objModulo As Entidades.Modulo, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try
            Dim cmd As New SqlCommand("USP_MANT_MODULO", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Id", objModulo.IdModulo)
            cmd.Parameters.AddWithValue("@Nombre", objModulo.nombre)
            cmd.Parameters.AddWithValue("@var", var)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

 


    Public Function EliminaIdModulo(ByVal idmodulo As Integer) As List(Of Entidades.Modulo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_MANT_MODULO", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", idmodulo)
        cmd.Parameters.AddWithValue("@var", "e")
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Modulo)
                Do While lector.Read
                    Dim modulo As New Entidades.Modulo
                    modulo.IdModulo = CInt(IIf(IsDBNull(lector.Item("Id")) = True, 0, lector.Item("Id")))
                    modulo.nombre = CInt(IIf(IsDBNull(lector.Item("Nombre")) = True, 0, lector.Item("Nombre")))

                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


End Class

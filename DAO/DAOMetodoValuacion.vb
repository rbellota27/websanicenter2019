'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOMetodoValuacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaMetodoValuacion(ByVal metodovaluacion As Entidades.MetodoValuacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@mv_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(0).Value = metodovaluacion.Descripcion
        ArrayParametros(1) = New SqlParameter("@mv_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(1).Value = metodovaluacion.DescripcionCorto
        ArrayParametros(2) = New SqlParameter("@mv_Vigente", SqlDbType.Bit)
        ArrayParametros(2).Value = metodovaluacion.Vigente
        ArrayParametros(3) = New SqlParameter("@mv_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = metodovaluacion.Estado
        Return HDAO.Insert(cn, "_MetodoValuacionInsert", ArrayParametros)
    End Function
    Public Function ActualizaMetodoValuacion(ByVal metodovaluacion As Entidades.MetodoValuacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@mv_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(0).Value = metodovaluacion.Descripcion
        ArrayParametros(1) = New SqlParameter("@mv_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(1).Value = metodovaluacion.DescripcionCorto
        ArrayParametros(2) = New SqlParameter("@mv_Vigente", SqlDbType.Bit)
        ArrayParametros(2).Value = metodovaluacion.Vigente
        ArrayParametros(3) = New SqlParameter("@mv_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = metodovaluacion.Estado
        ArrayParametros(4) = New SqlParameter("@IdMetodoV", SqlDbType.Int)
        ArrayParametros(4).Value = metodovaluacion.Id
        Return HDAO.Update(cn, "_MetodoValuacionUpdate", ArrayParametros)
    End Function
    Public Function SelectIdMetValVigente() As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MetodoValuacionSelectIdMetValVigente", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Dim IdMetValVigente As Integer = 0
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.Read Then
                    IdMetValVigente = CInt(IIf(IsDBNull(lector.Item("IdMetodoV")) = True, 0, lector.Item("IdMetodoV")))
                Else
                    IdMetValVigente = 0
                End If
                lector.Close()
                Return IdMetValVigente
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

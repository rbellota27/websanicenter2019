﻿Imports System.Data.SqlClient
Public Class DAOProceso
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectProceso(ByVal var As String) As List(Of Entidades.Proceso)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_MANT_PROCESO", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@var", var)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Proceso)
                Do While lector.Read
                    Dim proceso As New Entidades.Proceso
                    proceso.id = CInt(IIf(IsDBNull(lector.Item("IdProceso")) = True, 0, lector.Item("IdProceso")))
                    proceso.codigo = CStr(IIf(IsDBNull(lector.Item("CodProceso")) = True, "", lector.Item("CodProceso")))
                    proceso.Descripcion = CStr(IIf(IsDBNull(lector.Item("Descripcion")) = True, "", lector.Item("Descripcion")))
                    proceso.nombre = CStr(IIf(IsDBNull(lector.Item("NomProceso")) = True, "", lector.Item("NomProceso")))
                    proceso.estado = CInt(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    proceso.modulo = CInt(IIf(IsDBNull(lector.Item("IdModulo")) = True, 0, lector.Item("IdModulo")))
                    Lista.Add(proceso)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdProceso(ByVal idproceso As Integer) As List(Of Entidades.Proceso)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProcesoSelectxIdproceso", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProceso", idproceso)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Proceso)
                Do While lector.Read
                    Dim proceso As New Entidades.Proceso
                    proceso.id = CInt(IIf(IsDBNull(lector.Item("Idproceso")) = True, 0, lector.Item("Idproceso")))
                    proceso.codigo = CStr(IIf(IsDBNull(lector.Item("CodProceso")) = True, "", lector.Item("CodProceso")))
                    proceso.Descripcion = CStr(IIf(IsDBNull(lector.Item("Descripcion")) = True, "", lector.Item("Descripcion")))
                    proceso.nombre = CStr(IIf(IsDBNull(lector.Item("NomProceso")) = True, "", lector.Item("NomProceso")))
                    proceso.estado = CStr(IIf(IsDBNull(lector.Item("Estado")) = True, "", lector.Item("Estado")))
                    proceso.modulo = CInt(IIf(IsDBNull(lector.Item("IdModulo")) = True, 0, lector.Item("IdModulo")))

                    Lista.Add(proceso)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function AgregarProceso(ByVal proceso As Entidades.Proceso, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Integer

    Try
            Dim cmd As New SqlCommand("USP_MANT_PROCESO", cn, tr)
            Dim a As Integer
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@codigo", proceso.codigo)
            cmd.Parameters.AddWithValue("@descripcion", proceso.Descripcion)
            cmd.Parameters.AddWithValue("@nombre", proceso.nombre)
            cmd.Parameters.AddWithValue("@estado", proceso.estado)
            cmd.Parameters.AddWithValue("@idmodulo", proceso.modulo)
            cmd.Parameters.AddWithValue("@var", var)
            a = cmd.ExecuteNonQuery
            If a = 0 Then
                Return False
            End If
            Return a
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function ActualizaProceso(ByVal objProceso As Entidades.Proceso, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try
            Dim cmd As New SqlCommand("USP_MANT_PROCESO", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@id", objProceso.id)
            cmd.Parameters.AddWithValue("@codigo", objProceso.codigo)
            cmd.Parameters.AddWithValue("@descripcion", objProceso.Descripcion)
            cmd.Parameters.AddWithValue("@nombre", objProceso.nombre)
            cmd.Parameters.AddWithValue("@estado", objProceso.estado)
            cmd.Parameters.AddWithValue("@idmodulo", objProceso.modulo)
            cmd.Parameters.AddWithValue("@var", var)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function



    Public Function EliminaIdProceso(ByVal id As Integer, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Boolean

        Try
            Dim cmd As New SqlCommand("USP_MANT_PROCESO", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@id", id)
            cmd.Parameters.AddWithValue("@var", var)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Function


End Class

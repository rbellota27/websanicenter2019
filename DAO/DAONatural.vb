'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Imports System.Data.SqlClient

Public Class DAONatural
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub InsertaNatural(ByVal cn As SqlConnection, ByVal natural As Entidades.Natural, ByVal T As SqlTransaction)

        Dim ArrayParametros() As SqlParameter = New SqlParameter(13) {}
        ArrayParametros(0) = New SqlParameter("@nat_Apepat", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(natural.ApellidoPaterno = Nothing, DBNull.Value, natural.ApellidoPaterno)
        ArrayParametros(1) = New SqlParameter("@nat_Apemat", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(natural.ApellidoMaterno = Nothing, DBNull.Value, natural.ApellidoMaterno)
        ArrayParametros(2) = New SqlParameter("@nat_Nombres", SqlDbType.VarChar)
        ArrayParametros(2).Value = natural.Nombres
        ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(natural.IdPersona = Nothing, DBNull.Value, natural.IdPersona)
        ArrayParametros(4) = New SqlParameter("@IdMadre", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(natural.IdMadre = Nothing, DBNull.Value, natural.IdMadre)
        ArrayParametros(5) = New SqlParameter("@IdPadre", SqlDbType.Int)
        ArrayParametros(5).Value = IIf(natural.IdPadre = Nothing, DBNull.Value, natural.IdPadre)
        ArrayParametros(6) = New SqlParameter("@nat_UbigeoNac", SqlDbType.Char)
        ArrayParametros(6).Value = IIf(natural.UbigeoNac = "", DBNull.Value, natural.UbigeoNac)
        ArrayParametros(7) = New SqlParameter("@IdConyuge", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(natural.IdConyuge = Nothing, DBNull.Value, natural.IdConyuge)
        ArrayParametros(8) = New SqlParameter("@nat_FechaNac", SqlDbType.DateTime)
        ArrayParametros(8).Value = IIf(natural.FechaNac = Nothing, DBNull.Value, natural.FechaNac)
        ArrayParametros(9) = New SqlParameter("@nat_FechaMat", SqlDbType.DateTime)
        ArrayParametros(9).Value = IIf(natural.FechaMat = Nothing, DBNull.Value, natural.FechaMat)
        ArrayParametros(10) = New SqlParameter("@IdCargo", SqlDbType.Int)
        ArrayParametros(10).Value = IIf(natural.IdCargo = Nothing, DBNull.Value, natural.IdCargo)
        ArrayParametros(11) = New SqlParameter("@IdEstadoCivil", SqlDbType.Int)
        ArrayParametros(11).Value = IIf(natural.IdEstadoCivil = Nothing, DBNull.Value, natural.IdEstadoCivil)
        ArrayParametros(12) = New SqlParameter("@IdCondTrabajo", SqlDbType.Int)
        ArrayParametros(12).Value = IIf(natural.IdCondTrabajo = Nothing, DBNull.Value, natural.IdCondTrabajo)

        ArrayParametros(13) = objDaoMantenedor.getParam(natural.Sexo, "@nat_Sexo", SqlDbType.VarChar)

        HDAO.InsertaT(cn, "_NaturalInsert", ArrayParametros, T)
    End Sub
    Public Function ActualizaNatural(ByVal cn As SqlConnection, ByVal natural As Entidades.Natural, ByVal t As SqlTransaction) As Integer
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(13) {}
            ArrayParametros(0) = New SqlParameter("@nat_Apepat", SqlDbType.VarChar)
            ArrayParametros(0).Value = IIf(natural.ApellidoPaterno = Nothing, DBNull.Value, natural.ApellidoPaterno)
            ArrayParametros(1) = New SqlParameter("@nat_Apemat", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(natural.ApellidoMaterno = Nothing, DBNull.Value, natural.ApellidoMaterno)
            ArrayParametros(2) = New SqlParameter("@nat_Nombres", SqlDbType.VarChar)
            ArrayParametros(2).Value = IIf(natural.Nombres = Nothing, DBNull.Value, natural.Nombres)
            ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(natural.IdPersona = Nothing, DBNull.Value, natural.IdPersona)
            ArrayParametros(4) = New SqlParameter("@IdMadre", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(natural.IdMadre = Nothing, DBNull.Value, natural.IdMadre)
            ArrayParametros(5) = New SqlParameter("@IdPadre", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(natural.IdPadre = Nothing, DBNull.Value, natural.IdPadre)
            ArrayParametros(6) = New SqlParameter("@nat_UbigeoNac", SqlDbType.Char)
            ArrayParametros(6).Value = IIf(natural.UbigeoNac = Nothing, DBNull.Value, natural.UbigeoNac)
            ArrayParametros(7) = New SqlParameter("@IdConyuge", SqlDbType.Int)
            ArrayParametros(7).Value = IIf(natural.IdConyuge = Nothing, DBNull.Value, natural.IdConyuge)
            ArrayParametros(8) = New SqlParameter("@nat_FechaNac", SqlDbType.DateTime)
            ArrayParametros(8).Value = IIf(natural.FechaNac = Nothing, DBNull.Value, natural.FechaNac)
            ArrayParametros(9) = New SqlParameter("@nat_FechaMat", SqlDbType.DateTime)
            ArrayParametros(9).Value = IIf(natural.FechaMat = Nothing, DBNull.Value, natural.FechaMat)
            ArrayParametros(10) = New SqlParameter("@IdCargo", SqlDbType.Int)
            ArrayParametros(10).Value = IIf(natural.IdCargo = Nothing, DBNull.Value, natural.IdCargo)
            ArrayParametros(11) = New SqlParameter("@IdEstadoCivil", SqlDbType.Int)
            ArrayParametros(11).Value = IIf(natural.IdEstadoCivil = Nothing, DBNull.Value, natural.IdEstadoCivil)
            ArrayParametros(12) = New SqlParameter("@IdCondTrabajo", SqlDbType.Int)
            ArrayParametros(12).Value = IIf(natural.IdCondTrabajo = Nothing, DBNull.Value, natural.IdCondTrabajo)

            ArrayParametros(13) = objDaoMantenedor.getParam(natural.Sexo, "@nat_Sexo", SqlDbType.VarChar)

            HDAO.UpdateT(cn, "_NaturalUpdate", ArrayParametros, t)
            Return 1
        Catch ex As Exception
            Return -1
        End Try
    End Function
    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.Natural
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NaturalSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objNatural As New Entidades.Natural
            If lector.Read Then
                objNatural = New Entidades.Natural
                objNatural.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                objNatural.ApellidoPaterno = CStr(IIf(IsDBNull(lector.Item("nat_Apepat")) = True, "", lector.Item("nat_Apepat")))
                objNatural.ApellidoMaterno = CStr(IIf(IsDBNull(lector.Item("nat_Apemat")) = True, "", lector.Item("nat_Apemat")))
                objNatural.Nombres = CStr(IIf(IsDBNull(lector.Item("nat_Nombres")) = True, Nothing, lector.Item("nat_Nombres")))
                objNatural.IdCargo = CInt(IIf(IsDBNull(lector.Item("IdCargo")) = True, Nothing, lector.Item("IdCargo")))
                objNatural.IdEstadoCivil = CInt(IIf(IsDBNull(lector.Item("IdEstadoCivil")) = True, Nothing, lector.Item("IdEstadoCivil")))
                objNatural.IdCondTrabajo = CInt(IIf(IsDBNull(lector.Item("IdCondTrabajo")) = True, Nothing, lector.Item("IdCondTrabajo")))
                objNatural.FechaNac = CDate(IIf(IsDBNull(lector.Item("nat_FechaNac")) = True, Nothing, lector.Item("nat_FechaNac")))
                objNatural.Sexo = objDaoMantenedor.UCStr(lector("nat_Sexo"))
            End If
            lector.Close()
            Return objNatural
        End Using
    End Function
End Class

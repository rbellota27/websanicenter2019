﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*********************   LUNES 24052010
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOCostoFletexPeso

    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private reader As SqlDataReader
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Dim objConexion As New DAO.Conexion

    Public Sub CostoFletexPeso_Delete(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = objDaoMantenedor.getParam(IdTiendaOrigen, "@IdTiendaOrigen", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(IdTiendaDestino, "@IdTiendaDestino", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_CostoFletexPeso_Delete", p)

    End Sub

    Public Function CostoFletexPeso_SelectxId_DT(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = objDaoMantenedor.getParam(IdTiendaOrigen, "@IdTiendaOrigen", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTiendaDestino, "@IdTiendaDestino", SqlDbType.Int)

            cmd = New SqlCommand("_CostoFletexPeso_SelectxId", (New DAO.Conexion).ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CostoFletexPeso")

        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return ds.Tables("DT_CostoFletexPeso")

    End Function


    Public Function CostoFletexPeso_SelectxId(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer) As Entidades.CostoFletexPeso

        Dim objCostoFletexPeso As Entidades.CostoFletexPeso = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = objDaoMantenedor.getParam(IdTiendaOrigen, "@IdTiendaOrigen", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTiendaDestino, "@IdTiendaDestino", SqlDbType.Int)

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_CostoFletexPeso_SelectxId", p)
            If (reader.Read) Then

                objCostoFletexPeso = New Entidades.CostoFletexPeso
                With objCostoFletexPeso

                    .IdTiendaOrigen = objDaoMantenedor.UCInt(reader("IdTiendaOrigen"))
                    .IdTiendaDestino = objDaoMantenedor.UCInt(reader("IdTiendaDestino"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .costoxPesoUnit = objDaoMantenedor.UCDec(reader("cfp_costoxPesoUnit"))
                    .IdUnidadMedida_Peso = objDaoMantenedor.UCInt(reader("IdUnidadMedida_Peso"))
                    .UnidadMedida = objDaoMantenedor.UCStr(reader("um_NombreCorto"))
                    .Moneda = objDaoMantenedor.UCStr(reader("mon_Simbolo"))

                End With

            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objCostoFletexPeso

    End Function

    Public Sub CostoFletexPeso_Registrar(ByVal objCostoFletexPeso As Entidades.CostoFletexPeso, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(4) {}
        With objCostoFletexPeso
            p(0) = objDaoMantenedor.getParam(.IdTiendaOrigen, "@IdTiendaOrigen", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(.IdTiendaDestino, "@IdTiendaDestino", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(.IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(.costoxPesoUnit, "@cfp_costoxPesoUnit", SqlDbType.Decimal)
            p(4) = objDaoMantenedor.getParam(.IdUnidadMedida_Peso, "@IdUnidadMedida_Peso", SqlDbType.Int)
        End With

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_CostoFletexPeso_Registrar", p)

    End Sub

End Class

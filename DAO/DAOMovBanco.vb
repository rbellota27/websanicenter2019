﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'''''''''''''MIERCOLES 10/02/2009   4:39 PM


''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 04/02/2010 10:26 am
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data


Public Class DAOMovBanco
    Inherits DAOMantenedor
    Dim HDAO As New DAO.HelperDAO
    'Protected objConexion As New Conexion
    'Protected Cn As SqlConnection '= objConexion.ConexionSIGE()
    'Protected Tr As SqlTransaction
    'Protected Prm() As SqlParameter

    'Enum modo_query
    '    Show = 0 'No se puede poner select
    '    Insert = 1
    '    Update = 2
    '    Delete = 3
    'End Enum
    Public Function SelectTiendaExcelImport(ByVal tie_Estado As Integer) As List(Of Entidades.Tienda)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(0) {}
        Prm(0) = New SqlParameter("@tie_Estado", SqlDbType.Int)
        Prm(0).Value = tie_Estado
        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()
                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "SelectTiendaExcelImport", Prm)
                Dim Lista As New List(Of Entidades.Tienda)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.Tienda
                        obj.IdTienda = CInt(.Item("IdTienda"))
                        obj.tie_Nombre = CStr(.Item("tie_Nombre"))
                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectInsercionExcel(ByVal IdentificadorExcel As Integer) As List(Of Entidades.MovBancoView)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(0) {}
        Prm(0) = New SqlParameter("@IdentificadorExport", SqlDbType.Int)
        Prm(0).Value = IdentificadorExcel
        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()
                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "SelectxIdentificadorMovBancarioTemp", Prm)
                Dim Lista As New List(Of Entidades.MovBancoView)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.MovBancoView
                        obj.IdMovBanco = CInt(.Item("IdMovBanco"))
                        obj.IdBanco = CInt(.Item("IdBanco"))
                        obj.IdCuentaBancaria = CInt(.Item("IdCuentaBancaria"))
                        obj.NroOperacion = CStr(.Item("mban_NroOperacion"))
                        obj.Observacion = CStr(.Item("mban_Observacion"))
                        obj.Factor = CInt(IIf(IsDBNull(.Item("mban_Factor")) = True, Nothing, .Item("mban_Factor")))
                        'obj.Factor = CInt(IIf(IsDBNull(.Item("mban_Factor")) = True, 0, .Item("mban_Factor")))
                        obj.Monto = CDec(.Item("mban_Monto"))
                        obj.FechaRegistro = CDate(IIf(IsDBNull(.Item("mban_FechaRegistro")) = True, Nothing, .Item("mban_FechaRegistro")))
                        obj.FechaMov = CDate(IIf(IsDBNull(.Item("mban_FechaMov")) = True, Nothing, .Item("mban_FechaMov")))
                        obj.FechaAprobacion = CDate(IIf(IsDBNull(.Item("mban_FechaAprobacion")) = True, Nothing, .Item("mban_FechaAprobacion")))
                        obj.EstadoAprobacion = CBool(.Item("mban_EstadoAprobacion"))
                        obj.EstadoMov = CBool(.Item("mban_EstadoMov"))
                        obj.IdUsuario = CInt(.Item("IdUsuario"))
                        obj.IdTienda = CInt(.Item("IdTienda"))
                        obj.Tienda = CStr(IIf(IsDBNull(.Item("Tienda")) = True, Nothing, .Item("Tienda")))
                        obj.Usuario = CStr(IIf(IsDBNull(.Item("Usuario")) = True, Nothing, .Item("Usuario")))
                        obj.IdentificadorExport = CInt(.Item("IdentificadorExport"))
                        obj.Referencia1 = CStr(IIf(IsDBNull(.Item("referencia1")) = True, Nothing, .Item("referencia1")))
                        obj.Referencia2 = CStr(IIf(IsDBNull(.Item("referencia2")) = True, Nothing, .Item("referencia2")))
                        obj.ConceptoMovBanco = CStr(IIf(IsDBNull(.Item("Concepto")) = True, Nothing, .Item("Concepto")))
                        Lista.Add(obj)
                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub InsertMovBancoExcel(ByVal x As Entidades.MovBancoView, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim Prm() As SqlParameter = New SqlParameter(21) {}

        Prm(0) = New SqlParameter("@IdBanco", SqlDbType.Int)
        Prm(0).Value = IIf(IsNothing(x.IdBanco2) Or x.IdBanco2 = 0, DBNull.Value, x.IdBanco2)

        Prm(1) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        Prm(1).Value = IIf(IsNothing(x.IdCuentaBancaria2) Or x.IdCuentaBancaria2 = 0, DBNull.Value, x.IdCuentaBancaria2)

        Prm(2) = New SqlParameter("@mban_NroOperacion", SqlDbType.VarChar)
        Prm(2).Value = IIf(String.IsNullOrEmpty(x.NroOperacion), DBNull.Value, x.NroOperacion)

        Prm(3) = New SqlParameter("@mban_Observacion", SqlDbType.VarChar)
        Prm(3).Value = IIf(String.IsNullOrEmpty(x.Observacion), DBNull.Value, x.Observacion)

        Prm(4) = New SqlParameter("@mban_Factor", SqlDbType.Int)
        Prm(4).Value = IIf(IsNothing(x.Factor) Or x.Factor = 0, DBNull.Value, x.Factor)

        Prm(5) = New SqlParameter("@mban_Monto", SqlDbType.Decimal)
        Prm(5).Value = IIf(x.Monto = Nothing, 0, x.Monto)

        Prm(6) = New SqlParameter("@mban_FechaRegistro", SqlDbType.DateTime)
        Prm(6).Value = IIf(x.FechaRegistro = Nothing, DBNull.Value, x.FechaRegistro)

        Prm(7) = New SqlParameter("@mban_FechaMov", SqlDbType.DateTime)
        Prm(7).Value = IIf(x.FechaMov = Nothing, DBNull.Value, x.FechaMov)

        Prm(8) = New SqlParameter("@mban_FechaAprobacion", SqlDbType.DateTime)
        Prm(8).Value = IIf(x.FechaAprobacion = Nothing, DBNull.Value, x.FechaAprobacion)

        Prm(9) = New SqlParameter("@mban_EstadoAprobacion", SqlDbType.Bit)
        Prm(9).Value = IIf(IsNothing(x.EstadoAprobacionNullable), DBNull.Value, x.EstadoAprobacion)

        Prm(10) = New SqlParameter("@mban_EstadoMov", SqlDbType.Bit)
        Prm(10).Value = IIf(IsNothing(x.EstadoMovNullable), DBNull.Value, x.EstadoMov)

        Prm(11) = New SqlParameter("@IdConceptoMovBanco", SqlDbType.Int)
        Prm(11).Value = IIf(IsNothing(x.IdConceptoMovBanco) Or x.IdConceptoMovBanco = 0, DBNull.Value, x.IdConceptoMovBanco)

        Prm(12) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        Prm(12).Value = IIf(IsNothing(x.IdUsuario) Or x.IdUsuario = 0, DBNull.Value, x.IdUsuario)

        Prm(13) = New SqlParameter("@IdSupervisor", SqlDbType.Int)
        Prm(13).Value = IIf(IsNothing(x.IdSupervisor) Or x.IdSupervisor = 0, DBNull.Value, x.IdSupervisor)

        Prm(14) = New SqlParameter("@IdTarjeta", SqlDbType.Int)
        Prm(14).Value = IIf(IsNothing(x.IdTarjeta) Or x.IdTarjeta = 0, DBNull.Value, x.IdTarjeta)

        Prm(15) = New SqlParameter("@IdPersonaRef", SqlDbType.Int)
        Prm(15).Value = IIf(IsNothing(x.IdPersonaRef) Or x.IdPersonaRef = 0, DBNull.Value, x.IdPersonaRef)

        Prm(16) = New SqlParameter("@IdCaja", SqlDbType.Int)
        Prm(16).Value = IIf(IsNothing(x.IdCaja) Or x.IdCaja = 0, DBNull.Value, x.IdCaja)

        Prm(17) = New SqlParameter("@IdTienda", SqlDbType.Int)
        Prm(17).Value = IIf(IsNothing(x.IdTienda) Or x.IdTienda = 0, DBNull.Value, x.IdTienda)

        Prm(18) = New SqlParameter("@IdPost", SqlDbType.Int)
        Prm(18).Value = IIf(IsNothing(x.IdPost) Or x.IdPost = 0, DBNull.Value, x.IdPost)

        Prm(19) = New SqlParameter("@IdentificadorExport", SqlDbType.Int)
        Prm(19).Value = IIf(IsNothing(x.IdentificadorExport) Or x.IdentificadorExport = 0, DBNull.Value, x.IdentificadorExport)

        Prm(20) = New SqlParameter("@Referencia1", SqlDbType.VarChar)
        Prm(20).Value = IIf(IsNothing(x.Referencia1) Or x.Referencia1 = "", DBNull.Value, x.Referencia1)

        Prm(21) = New SqlParameter("@Referencia2", SqlDbType.VarChar)
        Prm(21).Value = IIf(IsNothing(x.Referencia2) Or x.Referencia2 = "", DBNull.Value, x.Referencia2)

        'Prm(19) = New SqlParameter("@IdMovBanco", SqlDbType.Int)
        'Prm(19).Value = getParamValueInt(x.IdMovBanco)

        HDAO.InsertaT(cn, "_MovBancoTempInsert_CuentaBancariaUpdateSaldos", Prm, tr)

    End Sub


    Public Function InsertAll(ByVal x As Entidades.MovBanco, Optional ByVal L As List(Of Entidades.Anexo_MovBanco) = Nothing) As Boolean
        Try
            Cn = objConexion.ConexionSIGE()
            Cn.Open()
            Tr = Cn.BeginTransaction()

            Dim Obj As New DAOAnexo_MovBanco

            If InsertT(x, Cn, Tr) Then
                If Not IsNothing(L) Then
                    For i As Integer = 0 To L.Count - 1
                        Obj.Anexo_MovBancoInsert(L(i), Cn, Tr)
                    Next
                End If
            End If
            Tr.Commit()
            Return True
        Catch e As Exception
            Tr.Rollback()
            Throw (e)
            Return False
        Finally
            Cn.Close()
            Cn.Dispose()
        End Try
    End Function
    Public Function UpdateAll(ByVal x As Entidades.MovBanco, Optional ByVal L As List(Of Entidades.Anexo_MovBanco) = Nothing) As Boolean
        Try
            Cn = objConexion.ConexionSIGE()
            Cn.Open()
            Tr = Cn.BeginTransaction()

            Dim Obj As New DAOAnexo_MovBanco

            If UpdateT(x, Cn, Tr) Then
                If Obj.DeletexIdMovBanco(x.IdMovBanco, Cn, Tr) Then
                    If Not IsNothing(L) Then
                        For i As Integer = 0 To L.Count - 1
                            Obj.Anexo_MovBancoInsert(L(i), Cn, Tr)
                        Next
                    End If
                End If
            End If
            Tr.Commit()
            Return True
        Catch e As Exception
            Tr.Rollback()
            Throw (e)
            Return False
        Finally
            Cn.Close()
            Cn.Dispose()
        End Try
    End Function
    Public Function InsertT_GetID(ByVal x As Entidades.MovBanco, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Integer
        Try
            'Return QueryT(GetVectorParametros(x, False), modo_query.Insert, "_MovBancoInsertGetID", Cnx, Trx)
            Return QueryT(GetVectorParametros(x, modo_query.Insert), modo_query.Insert, "_MovBancoInsert_CuentaBancariaUpdateSaldos", Cnx, Trx)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Function
    Public Function InsertT(ByVal x As Entidades.MovBanco, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean
        If InsertT_GetID(x, Cnx, Trx) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function UpdateT(ByVal x As Entidades.MovBanco, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean
        Try
            'Return CBool(QueryT(GetVectorParametros(x), modo_query.Update, "_MovBancoUpdate", Cnx, Trx))
            Return CBool(QueryT(GetVectorParametros(x, modo_query.Update), modo_query.Update, "_MovBancoUpdate_CuentaBancariaUpdateSaldos", Cnx, Trx))
        Catch ex As Exception
            Throw (ex)
        End Try
    End Function

    Public Function GetVectorParametros(ByVal x As Entidades.MovBanco, ByVal op As modo_query) As SqlParameter()

        'Leer!!!
        'Se utiliza isNothing para saber si un atributo es Nothing,
        'excepto en fechas, porque las fechas han sido inicializadas asi:
        'fecha1=nothing, lo que automaticamente le da un valor aprox de 01/01/0001 12:000am
        'y eso es diferente a Nothing.
        'Para las fechas se evalua asi
        'if fecha1 =Nothing

        'if algo=Nothing, 
        'Esa evaluacion no funciona con booleanos
        'si un booleano es false
        'y se utiliza esa comparacion
        'entonces da verdadero -- false = nothing   ¿¿??
        'se debe usar para booleanos isNothing(booleano1)

        'Para String usar  String.IsNullOrEmpty(cadena1)

        Dim Prm() As SqlParameter

        If op <> modo_query.Insert Then
            Prm = New SqlParameter(21) {}
            'Id de la Tabla
            Prm(21) = New SqlParameter("@IdMovBanco", SqlDbType.Int)
            Prm(21).Value = getParamValueInt(x.IdMovBanco)
        Else
            Prm = New SqlParameter(20) {}
        End If

        Prm(0) = New SqlParameter("@IdBanco", SqlDbType.Int)
        Prm(0).Value = IIf(IsNothing(x.IdBanco) Or x.IdBanco = 0, DBNull.Value, x.IdBanco)

        Prm(1) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        Prm(1).Value = IIf(IsNothing(x.IdCuentaBancaria) Or x.IdCuentaBancaria = 0, DBNull.Value, x.IdCuentaBancaria)

        Prm(2) = New SqlParameter("@mban_NroOperacion", SqlDbType.VarChar)
        Prm(2).Value = IIf(String.IsNullOrEmpty(x.NroOperacion), DBNull.Value, x.NroOperacion)

        Prm(3) = New SqlParameter("@mban_Observacion", SqlDbType.VarChar)
        Prm(3).Value = IIf(String.IsNullOrEmpty(x.Observacion), DBNull.Value, x.Observacion)

        Prm(4) = New SqlParameter("@mban_Factor", SqlDbType.Int)
        Prm(4).Value = IIf(IsNothing(x.Factor) Or x.Factor = 0, DBNull.Value, x.Factor)

        Prm(5) = New SqlParameter("@mban_Monto", SqlDbType.Decimal)
        'Prm(5).Value = iif(isnothing(x.Monto) Or x.Monto = 0D, DBNull.Value, x.Monto)
        'Prm(5).Value = IIf(IsNothing(x.Monto), DBNull.Value, x.Monto)
        Prm(5).Value = IIf(x.Monto = Nothing, DBNull.Value, x.Monto)

        Prm(6) = New SqlParameter("@mban_FechaRegistro", SqlDbType.DateTime)
        Prm(6).Value = IIf(x.FechaRegistro = Nothing, DBNull.Value, x.FechaRegistro)

        Prm(7) = New SqlParameter("@mban_FechaMov", SqlDbType.DateTime)
        Prm(7).Value = IIf(x.FechaMov = Nothing, DBNull.Value, x.FechaMov)

        Prm(8) = New SqlParameter("@mban_FechaAprobacion", SqlDbType.DateTime)
        Prm(8).Value = IIf(x.FechaAprobacion = Nothing, DBNull.Value, x.FechaAprobacion)

        Prm(9) = New SqlParameter("@mban_EstadoAprobacion", SqlDbType.Bit)
        Prm(9).Value = IIf(IsNothing(x.EstadoAprobacionNullable), DBNull.Value, x.EstadoAprobacion)

        Prm(10) = New SqlParameter("@mban_EstadoMov", SqlDbType.Bit)
        Prm(10).Value = IIf(IsNothing(x.EstadoMovNullable), DBNull.Value, x.EstadoMov)

        Prm(11) = New SqlParameter("@IdConceptoMovBanco", SqlDbType.Int)
        Prm(11).Value = IIf(IsNothing(x.IdConceptoMovBanco) Or x.IdConceptoMovBanco = 0, DBNull.Value, x.IdConceptoMovBanco)

        Prm(12) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        Prm(12).Value = IIf(IsNothing(x.IdUsuario) Or x.IdUsuario = 0, DBNull.Value, x.IdUsuario)

        Prm(13) = New SqlParameter("@IdSupervisor", SqlDbType.Int)
        Prm(13).Value = IIf(IsNothing(x.IdSupervisor) Or x.IdSupervisor = 0, DBNull.Value, x.IdSupervisor)

        Prm(14) = New SqlParameter("@IdTarjeta", SqlDbType.Int)
        Prm(14).Value = IIf(IsNothing(x.IdTarjeta) Or x.IdTarjeta = 0, DBNull.Value, x.IdTarjeta)

        Prm(15) = New SqlParameter("@IdPersonaRef", SqlDbType.Int)
        Prm(15).Value = IIf(IsNothing(x.IdPersonaRef) Or x.IdPersonaRef = 0, DBNull.Value, x.IdPersonaRef)

        Prm(16) = New SqlParameter("@IdCaja", SqlDbType.Int)
        Prm(16).Value = IIf(IsNothing(x.IdCaja) Or x.IdCaja = 0, DBNull.Value, x.IdCaja)

        Prm(17) = New SqlParameter("@IdTienda", SqlDbType.Int)
        Prm(17).Value = IIf(IsNothing(x.IdTienda) Or x.IdTienda = 0, DBNull.Value, x.IdTienda)

        Prm(18) = New SqlParameter("@IdPost", SqlDbType.Int)
        Prm(18).Value = IIf(IsNothing(x.IdPost) Or x.IdPost = 0, DBNull.Value, x.IdPost)

        Prm(19) = New SqlParameter("@Referencia1", SqlDbType.VarChar)
        Prm(19).Value = IIf(IsNothing(x.Referencia1x) Or x.Referencia1x = "", DBNull.Value, x.Referencia1x)

        Prm(20) = New SqlParameter("@Referencia2", SqlDbType.VarChar)
        Prm(20).Value = IIf(IsNothing(x.Referencia2x) Or x.Referencia2x = "", DBNull.Value, x.Referencia2x)

        Return Prm
    End Function
End Class

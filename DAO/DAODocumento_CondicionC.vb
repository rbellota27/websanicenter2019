﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocumento_CondicionC

    Private cn As SqlConnection
    Private reader As SqlDataReader
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Dim objConexion As New DAO.Conexion

    Public Sub Documento_CondicionC_Insert(ByVal cnx As SqlConnection, ByVal trx As SqlTransaction, ByVal obj As Entidades.CondicionComercial)

        Dim cmd As New SqlCommand("_Documento_CondicionC_Insert", cnx, trx)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdDocumento", IIf(obj.IdDocumento = Nothing, DBNull.Value, obj.IdDocumento))
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento))
        cmd.Parameters.AddWithValue("@IdCondicionCC", IIf(obj.Id = Nothing, DBNull.Value, obj.Id))
        cmd.Parameters.AddWithValue("@dcc_fecha", IIf(obj.dcc_fecha = Nothing, DBNull.Value, obj.dcc_fecha))
        cmd.Parameters.AddWithValue("@dcc_fechabl", IIf(obj.dcc_fechabl = Nothing, DBNull.Value, obj.dcc_fechabl))
        Try

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub


    'Public Sub Documento_CondicionC_Update_Preliminar(ByVal cnx As SqlConnection, ByVal trx As SqlTransaction, ByVal obj As Entidades.CondicionComercial)

    '    Dim cmd As New SqlCommand("_Documento_CondicionC_Update", cnx, trx)
    '    cmd.CommandType = CommandType.StoredProcedure

    '    cmd.Parameters.AddWithValue("@IdDocumento", IIf(obj.IdDocumento = Nothing, DBNull.Value, obj.IdDocumento))
    '    cmd.Parameters.AddWithValue("@IdTipoDocumento", IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento))
    '    cmd.Parameters.AddWithValue("@IdCondicionCC", IIf(obj.Id = Nothing, DBNull.Value, obj.Id))
    '    cmd.Parameters.AddWithValue("@dcc_fecha", IIf(obj.dcc_fecha = Nothing, DBNull.Value, obj.dcc_fecha))
    '    cmd.Parameters.AddWithValue("@dcc_fechabl", IIf(obj.dcc_fechabl = Nothing, DBNull.Value, obj.dcc_fechabl))
    '    Try

    '        cmd.ExecuteNonQuery()

    '    Catch ex As Exception
    '        Throw ex
    '    Finally

    '    End Try
    'End Sub

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.CondicionComercial)

        Dim lista As New List(Of Entidades.CondicionComercial)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Documento_CondicionC_SelectxIdDocumento", p)
            While (reader.Read)

                Dim obj As New Entidades.CondicionComercial
                With obj
                    .Id = objDaoMantenedor.UCInt(reader("IdCondicionCC"))
                    .Descripcion = objDaoMantenedor.UCStr(reader("CondicionComercial"))
                    .dcc_fecha = objDaoMantenedor.UCStr(reader("dcc_Fecha"))
                    .dcc_fechabl = objDaoMantenedor.UCStr(reader("dcc_fechabl"))
                End With

                lista.Add(obj)

            End While
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function


End Class

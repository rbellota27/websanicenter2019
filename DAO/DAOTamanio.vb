'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTamanio
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTamanio(ByVal tamanio As Entidades.Tamanio) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@tam_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tamanio.Nombre
        ArrayParametros(1) = New SqlParameter("@tam_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tamanio.Estado
        Return HDAO.Insert(cn, "_TamanioInsert", ArrayParametros)
    End Function
    Public Function ActualizaTamanio(ByVal tamanio As Entidades.Tamanio) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tam_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tamanio.Nombre
        ArrayParametros(1) = New SqlParameter("@IdTamanio", SqlDbType.Int)
        ArrayParametros(1).Value = tamanio.Id
        ArrayParametros(2) = New SqlParameter("@tam_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = tamanio.Estado
        Return HDAO.Update(cn, "_TamanioUpdate", ArrayParametros)
    End Function
    Public Function SelectAllActivo_Cbo() As List(Of Entidades.Tamanio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TamanioSelectAllActivo_Cbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tamanio)
                Do While lector.Read
                    Dim objTam As New Entidades.Tamanio
                    objTam.Id = CInt(lector.Item("IdTamanio"))
                    objTam.Nombre = CStr(lector.Item("tam_Nombre"))
                    Lista.Add(objTam)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Tamanio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TamanioSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tamanio)
                Do While lector.Read
                    Dim obj As New Entidades.Tamanio
                    obj.Estado = CStr(lector.Item("tam_Estado"))
                    obj.Id = CInt(lector.Item("IdTamanio"))
                    obj.Nombre = CStr(lector.Item("tam_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Tamanio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TamanioSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tamanio)
                Do While lector.Read
                    Dim obj As New Entidades.Tamanio
                    obj.Estado = CStr(lector.Item("tam_Estado"))
                    obj.Id = CInt(lector.Item("IdTamanio"))
                    obj.Nombre = CStr(lector.Item("tam_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Tamanio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TamanioSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tamanio)
                Do While lector.Read
                    Dim obj As New Entidades.Tamanio
                    obj.Estado = CStr(lector.Item("tam_Estado"))
                    obj.Id = CInt(lector.Item("IdTamanio"))
                    obj.Nombre = CStr(lector.Item("tam_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Tamanio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TamanioSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tamanio)
                Do While lector.Read
                    Dim obj As New Entidades.Tamanio
                    obj.Estado = CStr(lector.Item("tam_Estado"))
                    obj.Id = CInt(lector.Item("IdTamanio"))
                    obj.Nombre = CStr(lector.Item("tam_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Tamanio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TamanioSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tamanio)
                Do While lector.Read
                    Dim obj As New Entidades.Tamanio
                    obj.Estado = CStr(lector.Item("tam_Estado"))
                    obj.Id = CInt(lector.Item("IdTamanio"))
                    obj.Nombre = CStr(lector.Item("tam_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Tamanio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TamanioSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tamanio)
                Do While lector.Read
                    Dim obj As New Entidades.Tamanio
                    obj.Estado = CStr(lector.Item("tam_Estado"))
                    obj.Id = CInt(lector.Item("IdTamanio"))
                    obj.Nombre = CStr(lector.Item("tam_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Tamanio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TamanioSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tamanio)
                Do While lector.Read
                    Dim obj As New Entidades.Tamanio
                    obj.Estado = CStr(lector.Item("tam_Estado"))
                    obj.Id = CInt(lector.Item("IdTamanio"))
                    obj.Nombre = CStr(lector.Item("tam_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

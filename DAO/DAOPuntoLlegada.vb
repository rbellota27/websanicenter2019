'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOPuntoLlegada
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub Registrar(ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(3) {}
        p(0) = objDaoMantenedor.getParam(objPuntoLlegada.IdAlmacen, "@IdAlmacen", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objPuntoLlegada.pll_Ubigeo, "@pll_Ubigeo", SqlDbType.VarChar)
        p(2) = objDaoMantenedor.getParam(objPuntoLlegada.pll_Direccion, "@pll_Direccion", SqlDbType.VarChar)
        p(3) = objDaoMantenedor.getParam(objPuntoLlegada.IdDocumento, "@IdDocumento", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_PuntoLlegada_Registrar", p)


    End Sub

    Public Sub InsertaPuntoLlegadaT(ByVal cn As SqlConnection, ByVal puntollegada As Entidades.PuntoLlegada, ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.Conexion
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(puntollegada.IdAlmacen = Nothing, DBNull.Value, puntollegada.IdAlmacen)
        ArrayParametros(1) = New SqlParameter("@pll_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = puntollegada.pll_Ubigeo
        ArrayParametros(2) = New SqlParameter("@pll_Direccion", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(puntollegada.pll_Direccion = Nothing, DBNull.Value, puntollegada.pll_Direccion)
        ArrayParametros(3) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(3).Value = puntollegada.IdDocumento
        ArrayParametros(4) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(puntollegada.IdTienda = Nothing, DBNull.Value, puntollegada.IdTienda)

        HDAO.InsertaT(cn, "_PuntoLlegadaInsert", ArrayParametros, T)

    End Sub
    Public Function ActualizaPuntoLlegada(ByVal puntollegada As Entidades.PuntoLlegada) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(0).Value = puntollegada.IdAlmacen
        ArrayParametros(1) = New SqlParameter("@pll_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = puntollegada.pll_Ubigeo
        ArrayParametros(2) = New SqlParameter("@pll_Direccion", SqlDbType.VarChar)
        ArrayParametros(2).Value = puntollegada.pll_Direccion
        ArrayParametros(3) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(3).Value = puntollegada.IdDocumento
        Return HDAO.Update(cn, "_PuntoLlegadaUpdate", ArrayParametros)
    End Function


    Public Function SelectxIdDocumento(ByVal iddocumento As Integer) As Entidades.PuntoLlegada
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PuntoLlegadaSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing
                If lector.Read Then

                    objPuntoLlegada = New Entidades.PuntoLlegada

                    With objPuntoLlegada

                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .pll_Direccion = CStr(IIf(IsDBNull(lector.Item("pll_Direccion")) = True, "", lector.Item("pll_Direccion")))
                        .pll_Ubigeo = CStr(IIf(IsDBNull(lector.Item("pll_Ubigeo")) = True, "000000", lector.Item("pll_Ubigeo")))

                        .IdTipoAlmacen = CInt(IIf(IsDBNull(lector.Item("IdTipoAlmacen")) = True, 0, lector.Item("IdTipoAlmacen")))

                    End With


                End If
                lector.Close()
                Return objPuntoLlegada
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub DeletexIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_PuntoLlegadaDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        cmd.ExecuteNonQuery()

    End Sub

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM


Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOComision_Cab
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private reader As SqlDataReader
    Dim objConexion As New DAO.Conexion

    Public Function Comision_Cab_cbo(ByVal IdTipoComision As Integer) As List(Of Entidades.Comision_Cab)
        Dim lista As New List(Of Entidades.Comision_Cab)
        Dim cmd As New SqlCommand("_Comision_Cab_cbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoComision", IIf(IdTipoComision = Nothing, DBNull.Value, IdTipoComision))
        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                If reader.HasRows = True Then
                    Dim obj As New Entidades.Comision_Cab
                    obj.IdComisionCab = CInt(reader("IdComisionCab"))
                    obj.Descripcion = CStr(reader("comc_Descripcion"))
                    lista.Add(obj)
                End If
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return lista
    End Function


    Public Function Comision_Cab_SelectxParams_DT(ByVal IdComisionCab As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)


            Dim cmd As New SqlCommand("_Comision_Cab_Select", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Comision_Cab")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Comision_Cab")

    End Function
    Public Function Comision_Cab_SelectxIdComisionCab(ByVal IdComisionCab As Integer) As Entidades.Comision_Cab

        Dim objComisioncab As Entidades.Comision_Cab = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Comision_Cab_Select", p)
            If (reader.Read) Then

                objComisioncab = New Entidades.Comision_Cab
                With objComisioncab

                    .IdComisionCab = objDaoMantenedor.UCInt(reader("IdComisionCab"))
                    .Descripcion = objDaoMantenedor.UCStr(reader("Descripcion"))
                    .Abv = objDaoMantenedor.UCStr(reader("Abv"))
                    .Estado = objDaoMantenedor.UCBool(reader("Estado"))
                    .Factura = objDaoMantenedor.UCBool(reader("Factura"))
                    .Boleta = objDaoMantenedor.UCBool(reader("Boleta"))
                    .IdTipoComision = objDaoMantenedor.UCInt(reader("IdTipoComision"))
                End With

            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objComisioncab

    End Function
    Public Function Registrar(ByVal objComisionCab As Entidades.Comision_Cab, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(6) {}
        p(0) = objDaoMantenedor.getParam(objComisionCab.IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objComisionCab.Descripcion, "@comc_Descripcion", SqlDbType.VarChar)
        p(2) = objDaoMantenedor.getParam(objComisionCab.Abv, "@comc_Abv", SqlDbType.VarChar)
        p(3) = objDaoMantenedor.getParam(objComisionCab.Estado, "@comc_Estado", SqlDbType.Bit)
        p(4) = objDaoMantenedor.getParam(objComisionCab.Factura, "@comc_Factura", SqlDbType.Bit)
        p(5) = objDaoMantenedor.getParam(objComisionCab.Boleta, "@comc_Boleta", SqlDbType.Bit)
        p(6) = objDaoMantenedor.getParam(objComisionCab.IdTipoComision, "@IdTipoComision", SqlDbType.Int)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Comision_Cab_Registrar", p))

    End Function

End Class

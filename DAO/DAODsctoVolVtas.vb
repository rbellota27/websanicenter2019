﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports DAO.DAOUtil

Public Class DAODsctoVolVtas
    Dim objConexion As New DAO.Conexion
    Public Function Facturacion(ByVal objDscto As Entidades.DsctoVolVtas) As List(Of Entidades.DsctoVolVtas)
        list_DsctoVolVtas = New List(Of Entidades.DsctoVolVtas)

        Dim cn As SqlConnection = (New Conexion).ConexionSIGE
        Dim reader As SqlDataReader = Nothing

        Dim cmd As New SqlCommand("[dbo].[_DsctoVolVtas_Facturacion]", cn)

        cmd.Parameters.AddWithValue("@IdUsuario", objDscto.IdUsuario)
        cmd.Parameters.AddWithValue("@IdTienda", objDscto.IdTienda)
        cmd.Parameters.AddWithValue("@IdCondicionPago", objDscto.IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", objDscto.IdMedioPago)
        cmd.Parameters.AddWithValue("@IdTipoPrecioV", objDscto.IdTipoPV)
        cmd.Parameters.AddWithValue("@IdProducto", objDscto.IdProducto)
        cmd.Parameters.AddWithValue("@Cantidad", objDscto.CantMin)
        cmd.Parameters.AddWithValue("@Fecha", Format(objDscto.FechaFin, "dd/MM/yyyy"))

        cmd.CommandType = CommandType.StoredProcedure

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_DsctoVolVtas = New Entidades.DsctoVolVtas
                With obj_DsctoVolVtas
                    .CantMin = CDec(IIf(IsDBNull(reader("CantMin")), Decimal.Zero, reader("CantMin")))
                    .Dscto = CDec(IIf(IsDBNull(reader("Dscto")), Decimal.Zero, reader("Dscto")))
                End With
                list_DsctoVolVtas.Add(obj_DsctoVolVtas)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return list_DsctoVolVtas


    End Function


    Public Function DsctoVolVtas_Transaction(ByVal objDsctoVolVtas As Entidades.DsctoVolVtas, ByVal sqlCN As SqlConnection, ByVal sqlTR As SqlTransaction) As Boolean

        Dim Parameter() As SqlParameter = New SqlParameter(19) {}

        Parameter(0) = New SqlParameter("@IdDsctoVolVtas", SqlDbType.Int)
        Parameter(0).Value = IIf(objDsctoVolVtas.IdDsctoVolVtas = Nothing, DBNull.Value, objDsctoVolVtas.IdDsctoVolVtas)

        Parameter(1) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        Parameter(1).Value = IIf(objDsctoVolVtas.IdEmpresa = Nothing, DBNull.Value, objDsctoVolVtas.IdEmpresa)

        Parameter(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        Parameter(2).Value = IIf(objDsctoVolVtas.IdTienda = Nothing, DBNull.Value, objDsctoVolVtas.IdTienda)

        Parameter(3) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        Parameter(3).Value = IIf(objDsctoVolVtas.IdPerfil = Nothing, DBNull.Value, objDsctoVolVtas.IdPerfil)

        Parameter(4) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        Parameter(4).Value = IIf(objDsctoVolVtas.IdCondicionPago = Nothing, DBNull.Value, objDsctoVolVtas.IdCondicionPago)

        Parameter(5) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        Parameter(5).Value = IIf(objDsctoVolVtas.IdMedioPago = Nothing, DBNull.Value, objDsctoVolVtas.IdMedioPago)

        Parameter(6) = New SqlParameter("@IdLinea", SqlDbType.Int)
        Parameter(6).Value = IIf(objDsctoVolVtas.IdLinea = Nothing, DBNull.Value, objDsctoVolVtas.IdLinea)

        Parameter(7) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
        Parameter(7).Value = IIf(objDsctoVolVtas.IdSubLinea = Nothing, DBNull.Value, objDsctoVolVtas.IdSubLinea)

        Parameter(8) = New SqlParameter("@IdProducto", SqlDbType.Int)
        Parameter(8).Value = IIf(objDsctoVolVtas.IdProducto = Nothing, DBNull.Value, objDsctoVolVtas.IdProducto)

        Parameter(9) = New SqlParameter("@dscto", SqlDbType.Decimal)
        Parameter(9).Value = IIf(objDsctoVolVtas.Dscto = Nothing, DBNull.Value, objDsctoVolVtas.Dscto)

        Parameter(10) = New SqlParameter("@CantMin", SqlDbType.Decimal)
        Parameter(10).Value = IIf(objDsctoVolVtas.CantMin = Nothing, DBNull.Value, objDsctoVolVtas.CantMin)

        Parameter(11) = New SqlParameter("@CantMax", SqlDbType.Decimal)
        Parameter(11).Value = IIf(objDsctoVolVtas.CantMax = Nothing, DBNull.Value, objDsctoVolVtas.CantMax)

        Parameter(12) = New SqlParameter("@FechaIni", SqlDbType.DateTime)
        Parameter(12).Value = IIf(objDsctoVolVtas.FechaIni = Nothing, DBNull.Value, objDsctoVolVtas.FechaIni)

        Parameter(13) = New SqlParameter("@FechaFin", SqlDbType.DateTime)
        Parameter(13).Value = IIf(objDsctoVolVtas.FechaFin = Nothing, DBNull.Value, objDsctoVolVtas.FechaFin)

        Parameter(14) = New SqlParameter("@Excepcion", SqlDbType.Bit)
        Parameter(14).Value = IIf(objDsctoVolVtas.Excepcion = Nothing, DBNull.Value, objDsctoVolVtas.Excepcion)

        Parameter(15) = New SqlParameter("@Estado", SqlDbType.Bit)
        Parameter(15).Value = IIf(objDsctoVolVtas.Estado = Nothing, DBNull.Value, objDsctoVolVtas.Estado)

        Parameter(16) = New SqlParameter("@IdTipoPrecioV", SqlDbType.Int)
        Parameter(16).Value = IIf(objDsctoVolVtas.IdTipoPV = Nothing, DBNull.Value, objDsctoVolVtas.IdTipoPV)

        Parameter(17) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        Parameter(17).Value = IIf(objDsctoVolVtas.IdUsuario = Nothing, DBNull.Value, objDsctoVolVtas.IdUsuario)

        Parameter(18) = New SqlParameter("@Cliente_IP", SqlDbType.VarChar)
        Parameter(18).Value = GetIPAddress()

        Parameter(19) = New SqlParameter("@Cliente_HostName", SqlDbType.VarChar)
        Parameter(19).Value = GetHostName()

        Dim sqlCMD As New SqlCommand("_DsctoVolVtas_Registrar", sqlCN, sqlTR)
        sqlCMD.CommandType = CommandType.StoredProcedure
        sqlCMD.Parameters.AddRange(Parameter)

        Try

            sqlCMD.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
        End Try

        Return True
    End Function

    Public Function DsctoVolVtas_UpdateEst(ByVal objDsctoVolVtas As Entidades.DsctoVolVtas, ByVal sqlCN As SqlConnection, ByVal sqlTR As SqlTransaction) As Boolean

        Dim Parameter() As SqlParameter = New SqlParameter(9) {}

        Parameter(0) = New SqlParameter("@IdDsctoVolVtas", SqlDbType.Int)
        Parameter(0).Value = IIf(objDsctoVolVtas.IdDsctoVolVtas = Nothing, DBNull.Value, objDsctoVolVtas.IdDsctoVolVtas)

        Parameter(1) = New SqlParameter("@Estado", SqlDbType.Bit)
        Parameter(1).Value = IIf(objDsctoVolVtas.Estado = False, 0, 1)

        Parameter(2) = New SqlParameter("@FechaIni", SqlDbType.DateTime)
        Parameter(2).Value = IIf(objDsctoVolVtas.FechaIni = Nothing, DBNull.Value, objDsctoVolVtas.FechaIni)

        Parameter(3) = New SqlParameter("@FechaFin", SqlDbType.DateTime)
        Parameter(3).Value = IIf(objDsctoVolVtas.FechaFin = Nothing, DBNull.Value, objDsctoVolVtas.FechaFin)

        Parameter(4) = New SqlParameter("@CantidadMin", SqlDbType.Decimal)
        Parameter(4).Value = IIf(objDsctoVolVtas.CantMin = Nothing, DBNull.Value, objDsctoVolVtas.CantMin)

        Parameter(5) = New SqlParameter("@CantidadMax", SqlDbType.Decimal)
        Parameter(5).Value = IIf(objDsctoVolVtas.CantMax = Nothing, DBNull.Value, objDsctoVolVtas.CantMax)

        Parameter(6) = New SqlParameter("@PorcentajeDscto", SqlDbType.Decimal)
        Parameter(6).Value = IIf(objDsctoVolVtas.Dscto = Nothing, DBNull.Value, objDsctoVolVtas.Dscto)

        Parameter(7) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        Parameter(7).Value = IIf(objDsctoVolVtas.IdUsuario = Nothing, DBNull.Value, objDsctoVolVtas.IdUsuario)

        Parameter(8) = New SqlParameter("@Cliente_IP", SqlDbType.VarChar)
        Parameter(8).Value = GetIPAddress()

        Parameter(9) = New SqlParameter("@Cliente_HostName", SqlDbType.VarChar)
        Parameter(9).Value = GetHostName()

        Dim sqlCMD As New SqlCommand("_DsctoVolVtas_UpdateEst", sqlCN, sqlTR)
        sqlCMD.CommandType = CommandType.StoredProcedure
        sqlCMD.Parameters.AddRange(Parameter)

        Try

            If CInt(sqlCMD.ExecuteScalar()) <= 0 Then
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return True

    End Function


    Public Function DsctoVolVtas_UpdateEstG(ByVal objDsctoVolVtas As Entidades.DsctoVolVtas, ByVal sqlCN As SqlConnection, ByVal sqlTR As SqlTransaction) As Boolean

        Dim Parameter() As SqlParameter = New SqlParameter(9) {}

        Parameter(0) = New SqlParameter("@IdDsctoVolVtas", SqlDbType.Int)
        Parameter(0).Value = IIf(objDsctoVolVtas.IdDsctoVolVtas = Nothing, DBNull.Value, objDsctoVolVtas.IdDsctoVolVtas)

        Parameter(1) = New SqlParameter("@Estado", SqlDbType.Bit)
        Parameter(1).Value = IIf(objDsctoVolVtas.Estado = False, 0, 1)

        Parameter(2) = New SqlParameter("@FechaIni", SqlDbType.DateTime)
        Parameter(2).Value = IIf(objDsctoVolVtas.FechaIni = Nothing, DBNull.Value, objDsctoVolVtas.FechaIni)

        Parameter(3) = New SqlParameter("@FechaFin", SqlDbType.DateTime)
        Parameter(3).Value = IIf(objDsctoVolVtas.FechaFin = Nothing, DBNull.Value, objDsctoVolVtas.FechaFin)

        Parameter(4) = New SqlParameter("@CantidadMin", SqlDbType.Decimal)
        Parameter(4).Value = IIf(objDsctoVolVtas.CantMin = Nothing, DBNull.Value, objDsctoVolVtas.CantMin)

        Parameter(5) = New SqlParameter("@CantidadMax", SqlDbType.Decimal)
        Parameter(5).Value = IIf(objDsctoVolVtas.CantMax = Nothing, DBNull.Value, objDsctoVolVtas.CantMax)

        Parameter(6) = New SqlParameter("@PorcentajeDscto", SqlDbType.Decimal)
        Parameter(6).Value = IIf(objDsctoVolVtas.Dscto = Nothing, DBNull.Value, objDsctoVolVtas.Dscto)

        Parameter(7) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        Parameter(7).Value = IIf(objDsctoVolVtas.IdUsuario = Nothing, DBNull.Value, objDsctoVolVtas.IdUsuario)

        Parameter(8) = New SqlParameter("@Cliente_IP", SqlDbType.VarChar)
        Parameter(8).Value = GetIPAddress()

        Parameter(9) = New SqlParameter("@Cliente_HostName", SqlDbType.VarChar)
        Parameter(9).Value = GetHostName()

        Dim sqlCMD As New SqlCommand("_DsctoVolVtas_UpdateEstG", sqlCN, sqlTR)
        sqlCMD.CommandType = CommandType.StoredProcedure
        sqlCMD.Parameters.AddRange(Parameter)

        Try

            If CInt(sqlCMD.ExecuteScalar()) <= 0 Then
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally        
        End Try

        Return True

    End Function

    Dim list_DsctoVolVtas As List(Of Entidades.DsctoVolVtas)
    Dim obj_DsctoVolVtas As Entidades.DsctoVolVtas

    Public Function DsctoVolVtas_SelectD(ByVal objDscto As Entidades.DsctoVolVtas, ByVal tabla As DataTable, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DsctoVolVtas)
        list_DsctoVolVtas = New List(Of Entidades.DsctoVolVtas)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim reader As SqlDataReader = Nothing

        Dim cmd As New SqlCommand("_DsctoVolVtas_SelectD", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", objDscto.IdTienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", objDscto.IdTipoPV)
        cmd.Parameters.AddWithValue("@IdPerfil", objDscto.IdPerfil)
        cmd.Parameters.AddWithValue("@IdCondicionPago", objDscto.IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", objDscto.IdMedioPago)
        cmd.Parameters.AddWithValue("@IdLinea", objDscto.IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", objDscto.IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProducto", objDscto.IdProducto)
        cmd.Parameters.AddWithValue("@CodigoProducto", objDscto.CodigoProducto)
        cmd.Parameters.AddWithValue("@Producto", objDscto.Producto)
        cmd.Parameters.AddWithValue("@Tabla_IdProducto", tabla)
        cmd.Parameters.AddWithValue("@dscto", objDscto.Dscto)
        cmd.Parameters.AddWithValue("@CantMin", objDscto.CantMin)
        cmd.Parameters.AddWithValue("@CantMax", objDscto.CantMax)
        cmd.Parameters.AddWithValue("@FechaIni", Format(objDscto.FechaIni, "dd/MM/yyyy"))
        cmd.Parameters.AddWithValue("@FechaFin", Format(objDscto.FechaFin, "dd/MM/yyyy"))
        cmd.Parameters.AddWithValue("@Estado", objDscto.Estado)
        cmd.Parameters.AddWithValue("@PageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_DsctoVolVtas = New Entidades.DsctoVolVtas
                With obj_DsctoVolVtas
                    .IdDsctoVolVtas = CInt(IIf(IsDBNull(reader("IdDsctoVolVtas")), 0, reader("IdDsctoVolVtas")))
                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")), 0, reader("IdProducto")))
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("prod_Codigo")), "", reader("prod_Codigo")))
                    .Producto = CStr(IIf(IsDBNull(reader("prod_Nombre")), "", reader("prod_Nombre")))
                    .ProductoUM = CStr(IIf(IsDBNull(reader("um_NombreCorto")), "", reader("um_NombreCorto")))
                    .ProductoPV = CDec(IIf(IsDBNull(reader("ppv_Valor")), Decimal.Zero, reader("ppv_Valor")))

                    .IdEmpresa = CInt(IIf(IsDBNull(reader("IdEmpresa")), 0, reader("IdEmpresa")))
                    .Empresa = CStr(IIf(IsDBNull(reader("Empresa")), "", reader("Empresa")))

                    .IdTienda = CInt(IIf(IsDBNull(reader("IdTienda")), 0, reader("IdTienda")))
                    .Tienda = CStr(IIf(IsDBNull(reader("Tienda")), "", reader("Tienda")))

                    .IdPerfil = CInt(IIf(IsDBNull(reader("IdPerfil")), 0, reader("IdPerfil")))
                    .Perfil = CStr(IIf(IsDBNull(reader("Perfil")), "", reader("Perfil")))

                    .IdCondicionPago = CInt(IIf(IsDBNull(reader("IdCondicionPago")), 0, reader("IdCondicionPago")))
                    .CondicionPago = CStr(IIf(IsDBNull(reader("CondicionPago")), "", reader("CondicionPago")))

                    .IdMedioPago = CInt(IIf(IsDBNull(reader("IdMedioPago")), 0, reader("IdMedioPago")))
                    .MedioPago = CStr(IIf(IsDBNull(reader("MedioPago")), "", reader("MedioPago")))

                    .IdTipoPV = CInt(IIf(IsDBNull(reader("IdTipoPrecioV")), 0, reader("IdTipoPrecioV")))
                    .TipoPV = CStr(IIf(IsDBNull(reader("TipoPV")), "", reader("TipoPV")))

                    .IdLinea = CInt(IIf(IsDBNull(reader("IdLinea")), 0, reader("IdLinea")))
                    .Linea = CStr(IIf(IsDBNull(reader("Linea")), "", reader("Linea")))

                    .IdSubLinea = CInt(IIf(IsDBNull(reader("IdSubLinea")), 0, reader("IdSubLinea")))
                    .SubLinea = CStr(IIf(IsDBNull(reader("sl_Nombre")), "", reader("sl_Nombre")))

                    .Dscto = CDec(IIf(IsDBNull(reader("dscto")), Decimal.Zero, reader("dscto")))
                    .CantMin = CDec(IIf(IsDBNull(reader("CantMin")), Decimal.Zero, reader("CantMin")))
                    .CantMax = CDec(IIf(IsDBNull(reader("CantMax")), Decimal.Zero, reader("CantMax")))
                    .FechaIni = CDate(IIf(IsDBNull(reader("FechaIni")), Nothing, reader("FechaIni")))
                    .FechaFin = CDate(IIf(IsDBNull(reader("FechaFin")), Nothing, reader("FechaFin")))
                    .Excepcion = CBool(IIf(IsDBNull(reader("Excepcion")), False, reader("Excepcion")))
                    .Estado = CBool(IIf(IsDBNull(reader("Estado")), False, reader("Estado")))

                    .PrecioDscto = .ProductoPV * (1 - (.Dscto / 100))

                End With
                list_DsctoVolVtas.Add(obj_DsctoVolVtas)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return list_DsctoVolVtas
    End Function

    Public Function DsctoVolVtas_SelectNew(ByVal fecInicio As Date, ByVal fecFinal As Date, ByVal cantMinima As Integer, ByVal cantMaxima As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal Dscto As Decimal, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal CodigoProducto As String, ByVal Producto As String, ByVal tabla As DataTable, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DsctoVolVtas)
        list_DsctoVolVtas = New List(Of Entidades.DsctoVolVtas)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim reader As SqlDataReader = Nothing

        Dim cmd As New SqlCommand("_DsctoVolVtas_SelectNew", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@CodigoProducto", CodigoProducto)
        cmd.Parameters.AddWithValue("@Producto", Producto)
        cmd.Parameters.AddWithValue("@Tabla_IdProducto", tabla)
        cmd.Parameters.AddWithValue("@PageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_DsctoVolVtas = New Entidades.DsctoVolVtas
                With obj_DsctoVolVtas
                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")), 0, reader("IdProducto")))
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("prod_Codigo")), "", reader("prod_Codigo")))
                    .Producto = CStr(IIf(IsDBNull(reader("prod_Nombre")), "", reader("prod_Nombre")))
                    .ProductoUM = CStr(IIf(IsDBNull(reader("um_NombreCorto")), "", reader("um_NombreCorto")))
                    .Moneda = CStr(IIf(IsDBNull(reader("mon_Simbolo")), "", reader("mon_Simbolo")))
                    .ProductoPV = CDec(IIf(IsDBNull(reader("ppv_Valor")), Decimal.Zero, reader("ppv_Valor")))
                    .Dscto = Dscto
                    .CantMin = cantMinima
                    .CantMax = cantMaxima
                    .FechaIni = fecInicio
                    .FechaFin = fecFinal
                    .PrecioDscto = .ProductoPV * (1 - (Dscto / 100))
                End With
                list_DsctoVolVtas.Add(obj_DsctoVolVtas)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return list_DsctoVolVtas
    End Function

    Public Function DsctoVolVtas_SelectG(ByVal objDscto As Entidades.DsctoVolVtas, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DsctoVolVtas)
        list_DsctoVolVtas = New List(Of Entidades.DsctoVolVtas)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim reader As SqlDataReader = Nothing

        Dim cmd As New SqlCommand("_DsctoVolVtas_SelectG", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", objDscto.IdTienda)
        cmd.Parameters.AddWithValue("@IdPerfil", objDscto.IdPerfil)
        cmd.Parameters.AddWithValue("@IdCondicionPago", objDscto.IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", objDscto.IdMedioPago)
        cmd.Parameters.AddWithValue("@IdTipoPV", objDscto.IdTipoPV)
        cmd.Parameters.AddWithValue("@IdLinea", objDscto.IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", objDscto.IdSubLinea)
        cmd.Parameters.AddWithValue("@dscto", objDscto.Dscto)
        cmd.Parameters.AddWithValue("@CantMin", objDscto.CantMin)
        cmd.Parameters.AddWithValue("@CantMax", objDscto.CantMax)
        cmd.Parameters.AddWithValue("@FechaIni", Format(objDscto.FechaIni, "dd/MM/yyyy"))
        cmd.Parameters.AddWithValue("@FechaFin", Format(objDscto.FechaFin, "dd/MM/yyyy"))
        cmd.Parameters.AddWithValue("@Estado", objDscto.Estado)
        cmd.Parameters.AddWithValue("@PageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_DsctoVolVtas = New Entidades.DsctoVolVtas
                With obj_DsctoVolVtas
                    .IdDsctoVolVtas = CInt(IIf(IsDBNull(reader("IdDsctoVolVtas")), 0, reader("IdDsctoVolVtas")))

                    .IdEmpresa = CInt(IIf(IsDBNull(reader("IdEmpresa")), 0, reader("IdEmpresa")))
                    .Empresa = CStr(IIf(IsDBNull(reader("Empresa")), "", reader("Empresa")))

                    .IdTienda = CInt(IIf(IsDBNull(reader("IdTienda")), 0, reader("IdTienda")))
                    .Tienda = CStr(IIf(IsDBNull(reader("Tienda")), "", reader("Tienda")))

                    .IdPerfil = CInt(IIf(IsDBNull(reader("IdPerfil")), 0, reader("IdPerfil")))
                    .Perfil = CStr(IIf(IsDBNull(reader("Perfil")), "", reader("Perfil")))

                    .IdCondicionPago = CInt(IIf(IsDBNull(reader("IdCondicionPago")), 0, reader("IdCondicionPago")))
                    .CondicionPago = CStr(IIf(IsDBNull(reader("CondicionPago")), "", reader("CondicionPago")))

                    .IdMedioPago = CInt(IIf(IsDBNull(reader("IdMedioPago")), 0, reader("IdMedioPago")))
                    .MedioPago = CStr(IIf(IsDBNull(reader("MedioPago")), "", reader("MedioPago")))

                    .IdTipoPV = CInt(IIf(IsDBNull(reader("IdTipoPrecioV")), 0, reader("IdTipoPrecioV")))
                    .TipoPV = CStr(IIf(IsDBNull(reader("TipoPV")), "", reader("TipoPV")))

                    .IdLinea = CInt(IIf(IsDBNull(reader("IdLinea")), 0, reader("IdLinea")))
                    .Linea = CStr(IIf(IsDBNull(reader("Linea")), "", reader("Linea")))

                    .IdSubLinea = CInt(IIf(IsDBNull(reader("IdSubLinea")), 0, reader("IdSubLinea")))
                    .SubLinea = CStr(IIf(IsDBNull(reader("sl_Nombre")), "", reader("sl_Nombre")))

                    .Dscto = CDec(IIf(IsDBNull(reader("dscto")), Decimal.Zero, reader("dscto")))
                    .CantMin = CDec(IIf(IsDBNull(reader("CantMin")), Decimal.Zero, reader("CantMin")))
                    .CantMax = CDec(IIf(IsDBNull(reader("CantMax")), Decimal.Zero, reader("CantMax")))
                    .FechaIni = CDate(IIf(IsDBNull(reader("FechaIni")), Nothing, reader("FechaIni")))
                    .FechaFin = CDate(IIf(IsDBNull(reader("FechaFin")), Nothing, reader("FechaFin")))
                    .Excepcion = CBool(IIf(IsDBNull(reader("Excepcion")), False, reader("Excepcion")))
                    .Estado = CBool(IIf(IsDBNull(reader("Estado")), False, reader("Estado")))

                End With
                list_DsctoVolVtas.Add(obj_DsctoVolVtas)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return list_DsctoVolVtas
    End Function

End Class

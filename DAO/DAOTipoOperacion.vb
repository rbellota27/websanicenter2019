'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 28-Set-2009
'Hora    : 04:27 pm
'*************************************************


Imports System.Data.SqlClient

Public Class DAOTipoOperacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTipoOperacion(ByVal tipooperacion As Entidades.TipoOperacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@top_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = tipooperacion.CodigoSunat
        ArrayParametros(1) = New SqlParameter("@top_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = tipooperacion.Nombre
        ArrayParametros(2) = New SqlParameter("@top_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = tipooperacion.Estado
        Return HDAO.Insert(cn, "_TipoOperacionInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoOperacion(ByVal tipooperacion As Entidades.TipoOperacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@top_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = tipooperacion.CodigoSunat
        ArrayParametros(1) = New SqlParameter("@top_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = tipooperacion.Nombre
        ArrayParametros(2) = New SqlParameter("@top_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = tipooperacion.Estado
        ArrayParametros(3) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(3).Value = tipooperacion.Id
        Return HDAO.Update(cn, "_TipoOperacionUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("top_Estado")) = True, "", lector.Item("top_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("top_Estado")) = True, "", lector.Item("top_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("top_Estado")) = True, "", lector.Item("top_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("top_Estado")) = True, "", lector.Item("top_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("top_Estado")) = True, "", lector.Item("top_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("top_Estado")) = True, "", lector.Item("top_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("top_Estado")) = True, "", lector.Item("top_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxCodSunat(ByVal cod As String) As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectxCodSunat", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod", cod)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("top_Estado")) = True, "", lector.Item("top_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCboxIdMotivoTraslado(ByVal IdMotivoTraslado As Integer) As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectCboxIdMotivoTraslado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMotivoTraslado", IdMotivoTraslado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCboxIdTipoDocumento(ByVal IdTipoDocumento As Integer) As List(Of Entidades.TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoOperacionSelectCboxIdTipoDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoOperacion
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function OBTENER_ALMACEN(ByVal IdTienda As Integer, ByVal IdTipoOperacion As Integer) As DataTable
        Try
            Dim dt As New DataTable
            Dim cn As SqlConnection = objConexion.ConexionSIGE
            Dim cmd As New SqlCommand("_DocumentoCotizacionSelectTipoOperacionAlmacen", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Idtienda", IdTienda)
            cmd.Parameters.AddWithValue("@idtipooperacion", IdTipoOperacion)
            Dim dao As New SqlDataAdapter(cmd)
            dao.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function






    Public Function OBTENER_TIPOOPERACION(ByVal IdTienda As Integer, ByVal IdTipoOperacion As Integer) As DataTable
        Try
            Dim dt As New DataTable
            Dim cn As SqlConnection = objConexion.ConexionSIGE
            Dim cmd As New SqlCommand("_DocumentoCotizacionSelectAlmacenTipoOperacion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Idtienda", IdTienda)
            cmd.Parameters.AddWithValue("@idtipooperacion", IdTipoOperacion)
            Dim dao As New SqlDataAdapter(cmd)
            dao.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOImportacionDetalle
    Dim ObjConexion As New Conexion
    Dim HDAO As New DAO.HelperDAO
    Public Function OrdenCompraSelectIdOC(ByVal IdDoC As Integer, ByVal TipoCambio As Decimal) As List(Of Entidades.ImportacionDetalle)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenCompraSelectIdOC", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdOC", IdDoC)
        cmd.Parameters.AddWithValue("@TipoCambio", TipoCambio)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ImportacionDetalle)
                Do While lector.Read
                    Dim obj As New Entidades.ImportacionDetalle
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .NroOrdenCompra = CStr(IIf(IsDBNull(lector.Item("NroOC")) = True, "", lector.Item("NroOC")))
                        .IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("RazonSocial")) = True, "", lector.Item("RazonSocial")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_direccion")) = True, "", lector.Item("dir_direccion")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDoc = CStr(IIf(IsDBNull(lector.Item("NomTipoDoc")) = True, "", lector.Item("NomTipoDoc")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("NomProducto")) = True, "", lector.Item("NomProducto")))
                        .IdUM = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomUM = CStr(IIf(IsDBNull(lector.Item("UM")) = True, "", lector.Item("UM")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .Peso = CDec(IIf(IsDBNull(lector.Item("Peso")) = True, 0, lector.Item("Peso")))
                        .AbvMagnitud = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                        .PrecioUnitario = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .Costo = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function GrabarDetalleImportacionT(ByVal cn As SqlConnection, ByVal vIdDocumento As Integer, ByVal UpdPrecioOC As Boolean, ByVal ldetalleimportacion As List(Of Entidades.ImportacionDetalle), ByVal T As SqlTransaction) As Boolean
        Try

            Dim DetalleImportacion As Entidades.ImportacionDetalle
            For Each DetalleImportacion In ldetalleimportacion
                Dim ArrayParametros() As SqlParameter = New SqlParameter(11) {}
                ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
                ArrayParametros(0).Value = IIf(vIdDocumento = Nothing, DBNull.Value, vIdDocumento)

                ArrayParametros(1) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
                ArrayParametros(1).Value = IIf(DetalleImportacion.IdDetalleDocumento = Nothing, DBNull.Value, DetalleImportacion.IdDetalleDocumento)

                ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
                ArrayParametros(2).Value = IIf(DetalleImportacion.IdProducto = Nothing, DBNull.Value, DetalleImportacion.IdProducto)

                ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
                ArrayParametros(3).Value = IIf(DetalleImportacion.IdUM = Nothing, DBNull.Value, DetalleImportacion.IdUM)

                ArrayParametros(4) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
                ArrayParametros(4).Value = IIf(DetalleImportacion.Cantidad = Nothing, DBNull.Value, DetalleImportacion.Cantidad)

                ArrayParametros(5) = New SqlParameter("@dc_Importe", SqlDbType.Decimal)
                ArrayParametros(5).Value = IIf(DetalleImportacion.dc_Importe = Nothing, DBNull.Value, DetalleImportacion.dc_Importe)

                ArrayParametros(6) = New SqlParameter("@dc_Peso", SqlDbType.Decimal)
                ArrayParametros(6).Value = IIf(DetalleImportacion.Peso = Nothing, DBNull.Value, DetalleImportacion.Peso)

                ArrayParametros(7) = New SqlParameter("@dc_Costo_Imp", SqlDbType.Decimal)
                ArrayParametros(7).Value = IIf(DetalleImportacion.Costo = Nothing, DBNull.Value, DetalleImportacion.Costo)

                ArrayParametros(8) = New SqlParameter("@dc_PrecioCD", SqlDbType.Decimal)
                ArrayParametros(8).Value = IIf(DetalleImportacion.PrecioUnitario = Nothing, DBNull.Value, DetalleImportacion.PrecioUnitario)

                ArrayParametros(9) = New SqlParameter("@IdDetalleAfecto", SqlDbType.Int)
                ArrayParametros(9).Value = IIf(DetalleImportacion.IdDetalleAfecto = Nothing, DBNull.Value, DetalleImportacion.IdDetalleAfecto)

                ArrayParametros(10) = New SqlParameter("@UpdPrecioOC", SqlDbType.Bit)
                ArrayParametros(10).Value = IIf(UpdPrecioOC = Nothing, DBNull.Value, UpdPrecioOC)

                ArrayParametros(11) = New SqlParameter("@Arancel", SqlDbType.Decimal)
                ArrayParametros(11).Value = IIf(DetalleImportacion.Arancel = Nothing, DBNull.Value, DetalleImportacion.Arancel)

                HDAO.InsertaT(cn, "InsUpd_DetalleImportacion", ArrayParametros, T)
            Next
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function DetalleImportacionSelectId(ByVal IdDocumento As Integer) As List(Of Entidades.ImportacionDetalle)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ImportacionDetalleSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ImportacionDetalle)
                Do While lector.Read
                    Dim obj As New Entidades.ImportacionDetalle
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("NomProducto")) = True, "", lector.Item("NomProducto")))
                        .IdUM = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomUM = CStr(IIf(IsDBNull(lector.Item("UM")) = True, "", lector.Item("UM")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .Peso = CDec(IIf(IsDBNull(lector.Item("Peso")) = True, 0, lector.Item("Peso")))
                        .AbvMagnitud = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                        .PrecioUnitario = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .Costo = CDec(IIf(IsDBNull(lector.Item("dc_Costo_Imp")) = True, 0, lector.Item("dc_Costo_Imp")))
                        .dc_Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .Arancel = CDec(IIf(IsDBNull(lector.Item("dc_Descuento1")) = True, 0, lector.Item("dc_Descuento1")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

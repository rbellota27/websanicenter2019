'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'********************************************************
'Autor   : Caro Freyre, verly
'M�dulo  : Caja-Documento
'Sistema : Indusfer
'Empresa : Digrafic SRL
'Fecha   : 01-Oct-2009
'********************************************************
Imports System.Data.SqlClient

Public Class DAODetalleDocumentoR
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaDetalleDocumentoR(ByVal IdDocumento As Integer, ByVal IdDocumentoRef As Integer, ByVal IdMonedaRef As Integer, ByVal fechaEmisionRef As Date, ByVal CodigoRef As String, ByVal SerieRef As String, ByVal TotalDocumentoRef As Decimal, ByVal PorcentPercepcion As Decimal, ByVal TipoCambio As Decimal, ByVal ImportePercepcion As Decimal, ByVal ImporteFinal As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(10) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IdDocumento

        p(1) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
        p(1).Value = IdDocumentoRef

        p(2) = New SqlParameter("@dcr_TotalDocumento", SqlDbType.Decimal)
        p(2).Value = IIf(TotalDocumentoRef = Nothing, DBNull.Value, TotalDocumentoRef)

        p(3) = New SqlParameter("@dcr_Serie", SqlDbType.VarChar)
        p(3).Value = IIf(SerieRef = Nothing, DBNull.Value, SerieRef)

        p(4) = New SqlParameter("@dcr_NumDocumento", SqlDbType.VarChar)
        p(4).Value = IIf(CodigoRef = Nothing, DBNull.Value, CodigoRef)

        p(5) = New SqlParameter("@dcr_FechaEmision", SqlDbType.DateTime)
        p(5).Value = IIf(fechaEmisionRef = Nothing, DBNull.Value, fechaEmisionRef)

        p(6) = New SqlParameter("@dcr_TipoCambio", SqlDbType.Decimal)
        p(6).Value = IIf(TipoCambio = Nothing, DBNull.Value, TipoCambio)

        p(7) = New SqlParameter("@dcr_Porcentaje", SqlDbType.Decimal)
        p(7).Value = IIf(PorcentPercepcion = Nothing, DBNull.Value, PorcentPercepcion)

        p(8) = New SqlParameter("@dcr_Importe", SqlDbType.Decimal)
        p(8).Value = IIf(ImportePercepcion = Nothing, DBNull.Value, ImportePercepcion)

        p(9) = New SqlParameter("@dcr_ImporteFinal", SqlDbType.Decimal)
        p(9).Value = IIf(ImporteFinal = Nothing, DBNull.Value, ImporteFinal)

        p(10) = New SqlParameter("@IdMonedaDocRef", SqlDbType.Int)
        p(10).Value = IIf(IdMonedaRef = Nothing, DBNull.Value, IdMonedaRef)

        HDAO.InsertaT(cn, "_DetalleDocumentoRInsert", p, tr)

    End Sub
    'Public Function ActualizaDetalleDocumentoR(ByVal detalledocumentor As Entidades.DetalleDocumentoR) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(10) {}
    '    ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
    '    ArrayParametros(0).Value = detalledocumentor.IdDocumento
    '    ArrayParametros(1) = New SqlParameter("@dcr_TotalDocumento", SqlDbType.Decimal)
    '    ArrayParametros(1).Value = detalledocumentor.TotalDocumento
    '    ArrayParametros(2) = New SqlParameter("@dcr_Serie", SqlDbType.VarChar)
    '    ArrayParametros(2).Value = detalledocumentor.Serie
    '    ArrayParametros(3) = New SqlParameter("@dcr_NumDocumento", SqlDbType.VarChar)
    '    ArrayParametros(3).Value = detalledocumentor.NumDocumento
    '    ArrayParametros(4) = New SqlParameter("@dcr_FechaEmision", SqlDbType.Decimal)
    '    ArrayParametros(4).Value = detalledocumentor.FechaEmision
    '    ArrayParametros(5) = New SqlParameter("@dcr_TipoCambio", SqlDbType.Decimal)
    '    ArrayParametros(5).Value = detalledocumentor.TipoCambio
    '    ArrayParametros(6) = New SqlParameter("@dcr_Porcentaje", SqlDbType.Decimal)
    '    ArrayParametros(6).Value = detalledocumentor.Porcentaje
    '    ArrayParametros(7) = New SqlParameter("@dcr_Importe", SqlDbType.Decimal)
    '    ArrayParametros(7).Value = detalledocumentor.Importe
    '    ArrayParametros(8) = New SqlParameter("@dcr_ImporteFinal", SqlDbType.Decimal)
    '    ArrayParametros(8).Value = detalledocumentor.ImporteFinal
    '    ArrayParametros(9) = New SqlParameter("@IdMoneda", SqlDbType.Int)
    '    ArrayParametros(9).Value = detalledocumentor.IdMoneda
    '    ArrayParametros(10) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
    '    ArrayParametros(10).Value = detalledocumentor.IdTipoDocumento
    '    Return HDAO.Update(cn, "_DetalleDocumentoRUpdate", ArrayParametros)
    'End Function
    
End Class

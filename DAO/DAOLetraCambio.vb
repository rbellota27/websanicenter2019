'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient


Public Class DAOLetraCambio
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion



    Public Sub LetraCambioInsert(ByVal objLetraCambio As Entidades.LetraCambio, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(10) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IIf(objLetraCambio.IdDocumento = Nothing, DBNull.Value, objLetraCambio.IdDocumento)
        p(1) = New SqlParameter("@lc_Ubigeo", SqlDbType.Char)
        p(1).Value = IIf(objLetraCambio.Ubigeo = Nothing, DBNull.Value, objLetraCambio.Ubigeo)
        p(2) = New SqlParameter("@IdAval", SqlDbType.Int)
        p(2).Value = IIf(objLetraCambio.IdAval = Nothing, DBNull.Value, objLetraCambio.IdAval)
        p(3) = New SqlParameter("@IdBanco", SqlDbType.Int)
        p(3).Value = IIf(objLetraCambio.IdBanco = Nothing, DBNull.Value, objLetraCambio.IdBanco)
        p(4) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        p(4).Value = IIf(objLetraCambio.IdCuentaBancaria = Nothing, DBNull.Value, objLetraCambio.IdCuentaBancaria)
        p(5) = New SqlParameter("@IdOficina", SqlDbType.Int)
        p(5).Value = IIf(objLetraCambio.IdOficina = Nothing, DBNull.Value, objLetraCambio.IdOficina)
        p(6) = New SqlParameter("@lc_DebitoCuenta", SqlDbType.Bit)
        p(6).Value = objLetraCambio.DebitoCuenta
        p(7) = New SqlParameter("@lc_Renegociado", SqlDbType.Bit)
        p(7).Value = objLetraCambio.Renegociado
        p(8) = New SqlParameter("@lc_Protestado", SqlDbType.Bit)
        p(8).Value = objLetraCambio.Protestado
        p(9) = New SqlParameter("@lc_EnCartera", SqlDbType.Bit)
        p(9).Value = objLetraCambio.lc_EnCartera
        p(10) = New SqlParameter("@lc_EnCobraza", SqlDbType.Bit)
        p(10).Value = objLetraCambio.lc_EnCobranza

        Dim cmd As New SqlCommand("_LetraCambioInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(p)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
        Finally

        End Try


    End Sub














    Public Function InsertaLetraCambio(ByVal letracambio As Entidades.LetraCambio) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = letracambio.IdDocumento
        ArrayParametros(1) = New SqlParameter("@lc_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = letracambio.Ubigeo
        ArrayParametros(2) = New SqlParameter("@IdAval", SqlDbType.Int)
        ArrayParametros(2).Value = letracambio.IdAval
        ArrayParametros(3) = New SqlParameter("@IdBanco", SqlDbType.Int)
        ArrayParametros(3).Value = letracambio.IdBanco
        ArrayParametros(4) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        ArrayParametros(4).Value = letracambio.IdCuentaBancaria
        ArrayParametros(5) = New SqlParameter("@IdOficina", SqlDbType.Int)
        ArrayParametros(5).Value = letracambio.IdOficina
        Return HDAO.Insert(cn, "_LetraCambioInsert", ArrayParametros)
    End Function
    Public Function ActualizaLetraCambio(ByVal letracambio As Entidades.LetraCambio) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = letracambio.IdDocumento
        ArrayParametros(1) = New SqlParameter("@lc_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = letracambio.Ubigeo
        ArrayParametros(2) = New SqlParameter("@IdAval", SqlDbType.Int)
        ArrayParametros(2).Value = letracambio.IdAval
        ArrayParametros(3) = New SqlParameter("@IdBanco", SqlDbType.Int)
        ArrayParametros(3).Value = letracambio.IdBanco
        ArrayParametros(4) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        ArrayParametros(4).Value = letracambio.IdCuentaBancaria
        ArrayParametros(5) = New SqlParameter("@IdOficina", SqlDbType.Int)
        ArrayParametros(5).Value = letracambio.IdOficina
        Return HDAO.Update(cn, "_LetraCambioUpdate", ArrayParametros)
    End Function
End Class

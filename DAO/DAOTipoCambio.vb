'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTipoCambio

    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function SelectVigentexIdMoneda(ByVal IdMoneda As Integer) As Entidades.TipoCambio
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCambioSelectVigentexIdMoneda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objTipoCambio As New Entidades.TipoCambio
                If lector.Read Then
                    With objTipoCambio
                        .CompraC = CDec(IIf(IsDBNull(lector.Item("tc_CompraC")) = True, 0, lector.Item("tc_CompraC")))
                        .CompraOf = CDec(IIf(IsDBNull(lector.Item("tc_CompraOf")) = True, 0, lector.Item("tc_CompraOf")))
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdTipoCambio")) = True, 0, lector.Item("IdTipoCambio")))

                        .VentaC = CDec(IIf(IsDBNull(lector.Item("tc_VentaC")) = True, 0, lector.Item("tc_VentaC")))
                        .VentaOf = CDec(IIf(IsDBNull(lector.Item("tc_VentaOf")) = True, 0, lector.Item("tc_VentaOf")))
                    End With
                End If
                lector.Close()
                Return objTipoCambio
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function InsertaTipoCambio(ByVal tipocambio As Entidades.TipoCambio) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@tc_CompraOf", SqlDbType.Decimal)
        ArrayParametros(0).Value = IIf(tipocambio.CompraOf = Nothing, DBNull.Value, tipocambio.CompraOf)
        ArrayParametros(1) = New SqlParameter("@tc_CompraC", SqlDbType.Decimal)
        ArrayParametros(1).Value = IIf(tipocambio.CompraC = Nothing, DBNull.Value, tipocambio.CompraC)
        'ArrayParametros(2) = New SqlParameter("@tc_Fecha", SqlDbType.DateTime)
        ArrayParametros(2) = New SqlParameter("@tc_Fecha", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(tipocambio.Fecha = Nothing, DBNull.Value, tipocambio.Fecha)
        ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(tipocambio.IdMoneda = Nothing, DBNull.Value, tipocambio.IdMoneda)
        ArrayParametros(4) = New SqlParameter("@tc_VentaOf", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(tipocambio.VentaOf = Nothing, DBNull.Value, tipocambio.VentaOf)
        ArrayParametros(5) = New SqlParameter("@tc_VentaC", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(tipocambio.VentaC = Nothing, DBNull.Value, tipocambio.VentaC)
        ArrayParametros(6) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(tipocambio.IdUsuario = Nothing, DBNull.Value, tipocambio.IdUsuario)

        Return HDAO.Insert(cn, "_TipoCambioInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoCambio(ByVal tipocambio As Entidades.TipoCambio) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
        ArrayParametros(0) = New SqlParameter("@tc_CompraOf", SqlDbType.Decimal)
        ArrayParametros(0).Value = IIf(tipocambio.CompraOf = Nothing, DBNull.Value, tipocambio.CompraOf)
        ArrayParametros(1) = New SqlParameter("@tc_CompraC", SqlDbType.Decimal)
        ArrayParametros(1).Value = IIf(tipocambio.CompraC = Nothing, DBNull.Value, tipocambio.CompraC)
        ArrayParametros(2) = New SqlParameter("@tc_Fecha", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(tipocambio.Fecha = Nothing, DBNull.Value, tipocambio.Fecha)
        ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(tipocambio.IdMoneda = Nothing, DBNull.Value, tipocambio.IdMoneda)
        ArrayParametros(4) = New SqlParameter("@tc_VentaOf", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(tipocambio.VentaOf = Nothing, DBNull.Value, tipocambio.VentaOf)
        ArrayParametros(5) = New SqlParameter("@tc_VentaC", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(tipocambio.VentaC = Nothing, DBNull.Value, tipocambio.VentaC)
        ArrayParametros(6) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(tipocambio.IdUsuario = Nothing, DBNull.Value, tipocambio.IdUsuario)
        ArrayParametros(7) = New SqlParameter("@IdTipoCambio", SqlDbType.Int)
        ArrayParametros(7).Value = tipocambio.Id
        Return HDAO.Update(cn, "_TipoCambioUpdate", ArrayParametros)
    End Function
    Public Function SelectOficial(ByVal IdMoneda As Integer) As Entidades.TipoCambio
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCambioSelectOficial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.HasRows = False Then
                    Throw New Exception("Falta Ingresar el Tipo de Cambio")
                End If
                lector.Read()
                Dim TipoCambio As New Entidades.TipoCambio
                TipoCambio.CompraOf = CDec(IIf(IsDBNull(lector.Item("tc_CompraOf")) = True, "0", lector.Item("tc_CompraOf")))
                TipoCambio.VentaOf = CDec(IIf(IsDBNull(lector.Item("tc_VentaOf")) = True, "0", lector.Item("tc_VentaOf")))
                Return TipoCambio
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectComercial(ByVal IdMoneda As Integer) As Entidades.TipoCambio
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCambioSelectComercial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.HasRows = False Then
                    Throw New Exception("Falta Ingresar el Tipo de Cambio")
                End If
                lector.Read()
                Dim TipoCambio As New Entidades.TipoCambio
                TipoCambio.CompraC = CDec(IIf(IsDBNull(lector.Item("tc_CompraC")) = True, "0", lector.Item("tc_CompraC")))
                TipoCambio.VentaC = CDec(IIf(IsDBNull(lector.Item("tc_VentaC")) = True, "0", lector.Item("tc_VentaC")))
                Return TipoCambio
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectTipoCambioActualxIdMoneda(ByVal IdMoneda As Integer) As Entidades.TipoCambio
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCambioSelectTipoCambioActualxIdMoneda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objTipoCambio As New Entidades.TipoCambio
                If lector.Read Then
                    With objTipoCambio
                        .CompraC = CDec(IIf(IsDBNull(lector.Item("tc_CompraC")) = True, 0, lector.Item("tc_CompraC")))
                        .CompraOf = CDec(IIf(IsDBNull(lector.Item("tc_CompraOf")) = True, 0, lector.Item("tc_CompraOf")))
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdTipoCambio")) = True, 0, lector.Item("IdTipoCambio")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                        .VentaC = CDec(IIf(IsDBNull(lector.Item("tc_VentaC")) = True, 0, lector.Item("tc_VentaC")))
                        .VentaOf = CDec(IIf(IsDBNull(lector.Item("tc_VentaOf")) = True, 0, lector.Item("tc_VentaOf")))
                    End With
                End If
                lector.Close()
                Return objTipoCambio
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdMoneda(ByVal IdMoneda As Integer) As List(Of Entidades.TipoCambio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCambioSelectxIdMoneda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TipoCambio)
                While lector.Read
                    Dim objtipocambio As New Entidades.TipoCambio
                    With objTipoCambio
                        .CompraC = CDec(IIf(IsDBNull(lector.Item("tc_CompraC")) = True, 0, lector.Item("tc_CompraC")))
                        .CompraOf = CDec(IIf(IsDBNull(lector.Item("tc_CompraOf")) = True, 0, lector.Item("tc_CompraOf")))
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdTipoCambio")) = True, 0, lector.Item("IdTipoCambio")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                        .VentaC = CDec(IIf(IsDBNull(lector.Item("tc_VentaC")) = True, 0, lector.Item("tc_VentaC")))
                        .VentaOf = CDec(IIf(IsDBNull(lector.Item("tc_VentaOf")) = True, 0, lector.Item("tc_VentaOf")))
                        '.Fecha = CStr(IIf(IsDBNull(lector.Item("tc_Fecha")) = True, Nothing, lector.Item("tc_Fecha")))
                        .Fecha = CStr(IIf(IsDBNull(lector.Item("tc_Fecha")) = True, Nothing, lector.Item("tc_Fecha")))
                    End With
                    lista.Add(objtipocambio)
                End While
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectTipoCambioBuscarFecha(ByVal IdMoneda As Integer, ByVal tc_Fecha As String) As List(Of Entidades.TipoCambio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCambioBuscarFecha", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        cmd.Parameters.AddWithValue("@tc_Fecha", tc_Fecha)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TipoCambio)
                While lector.Read
                    Dim objtipocambio As New Entidades.TipoCambio
                    With objtipocambio
                        .CompraC = CDec(IIf(IsDBNull(lector.Item("tc_CompraC")) = True, 0, lector.Item("tc_CompraC")))
                        .CompraOf = CDec(IIf(IsDBNull(lector.Item("tc_CompraOf")) = True, 0, lector.Item("tc_CompraOf")))
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdTipoCambio")) = True, 0, lector.Item("IdTipoCambio")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                        .VentaC = CDec(IIf(IsDBNull(lector.Item("tc_VentaC")) = True, 0, lector.Item("tc_VentaC")))
                        .VentaOf = CDec(IIf(IsDBNull(lector.Item("tc_VentaOf")) = True, 0, lector.Item("tc_VentaOf")))
                        .Fecha = CStr(IIf(IsDBNull(lector.Item("tc_Fecha")) = True, Nothing, lector.Item("tc_Fecha")))
                    End With
                    lista.Add(objtipocambio)
                End While
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


End Class


﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOEstadoEntrega
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectCbo() As List(Of Entidades.EstadoEntrega)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoEntregaSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoEntrega)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoEntrega
                    obj.Descripcion = CStr(lector.Item("eent_Nombre"))
                    obj.Id = CInt(lector.Item("IdEstadoEnt"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

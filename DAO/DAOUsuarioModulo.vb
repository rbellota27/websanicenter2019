﻿Imports System.Data.SqlClient
Public Class DAOUsuarioModulo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Function SelectUsuarioModulo(ByVal var As String) As List(Of Entidades.UsuarioModulo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_MANT_USU_MOD", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@var", var)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UsuarioModulo)
                Do While lector.Read
                    Dim UsuMod As New Entidades.UsuarioModulo
                    UsuMod.idusuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    UsuMod.usu = CStr(IIf(IsDBNull(lector.Item("Usuario")) = True, "", lector.Item("Usuario")))
                    UsuMod.mod1 = CStr(IIf(IsDBNull(lector.Item("Modulo")) = True, "", lector.Item("Modulo")))
                    Lista.Add(UsuMod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    Public Function SelectxIdUsuario(ByVal idsuario As Integer) As List(Of Entidades.UsuarioModulo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProcesoSelectxIdUsuario", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", idsuario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UsuarioModulo)
                Do While lector.Read
                    Dim usuario As New Entidades.UsuarioModulo
                    usuario.idusuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    usuario.usu = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    usuario.modulo = CInt(IIf(IsDBNull(lector.Item("IdModulo")) = True, 0, lector.Item("IdModulo")))
                    Lista.Add(usuario)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function TextBox() As List(Of Entidades.Trabajador)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_NOMBRE_PERSONA", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Trabajador)
                Do While lector.Read
                    Dim trabajador As New Entidades.Trabajador
                    trabajador.nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    Lista.Add(trabajador)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Public Function AgregarUsuarioModulo(ByVal usuario As Entidades.UsuarioModulo, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Integer

        Try
            Dim cmd As New SqlCommand("USP_MANT_USU_MOD", cn, tr)
            Dim a As Integer
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idPersona", usuario.usuario)
            cmd.Parameters.AddWithValue("@idModulo", usuario.modulo)
            cmd.Parameters.AddWithValue("@var", var)
            a = cmd.ExecuteNonQuery
            If a = 0 Then
                Return False
            End If
            Return a
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectxNombreUsuario(ByVal nombre As String)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UsuarioSelectxNombre ", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UsuarioModulo)
                Do While lector.Read
                    Dim usuario As New Entidades.UsuarioModulo
                    usuario.usuario = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    Lista.Add(usuario)
                Loop
                lector.Close()
                Return Lista.Item(0).usuario
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function ActualizaUsuario(ByVal objUsuario As Entidades.UsuarioModulo, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try
            Dim cmd As New SqlCommand("USP_MANT_USU_MOD", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@id", objUsuario.idusuario)
            cmd.Parameters.AddWithValue("@idModulo", objUsuario.modulo)
            cmd.Parameters.AddWithValue("@idPersona", objUsuario.usuario)
            cmd.Parameters.AddWithValue("@var", var)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function


    Public Function EliminaIdUsuario(ByVal id As Integer, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Boolean

        Try
            Dim cmd As New SqlCommand("USP_MANT_USU_MOD", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@id", id)
            cmd.Parameters.AddWithValue("@var", var)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Function
End Class

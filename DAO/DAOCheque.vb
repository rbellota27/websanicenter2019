﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*********************   MARTES 15062010

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOCheque

    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private reader As SqlDataReader
    Dim objConexion As New DAO.Conexion

    Public Function Update_Estado_EstadoMovxIdCheque(ByVal IdCheque As Integer, ByVal Estado As Boolean, ByVal EstadoMov As String, ByVal Observacion As String) As Boolean

        Dim hecho As Boolean = False
        Try

            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(IdCheque, "@IdCheque", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(Estado, "@Estado", SqlDbType.Bit)
            p(2) = objDaoMantenedor.getParam(EstadoMov, "@EstadoMov", SqlDbType.VarChar)
            p(3) = objDaoMantenedor.getParam(Observacion, "@Observacion", SqlDbType.VarChar)

            cn.Open()
            SqlHelper.ExecuteNonQuery(cn, CommandType.StoredProcedure, "_Cheque_Update_Estado_EstadoMovxIdCheque", p)
            hecho = True
        Catch ex As Exception
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function


    Public Function Cheque_ValSelect_AddDatoCancelacion_DT(ByVal IdCliente As Integer, ByVal FechaMov As Date, ByVal Val_DocumentoRelacionado As Boolean) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdCliente, "@IdCliente", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(FechaMov, "@FechaMov", SqlDbType.Date)
            p(2) = objDaoMantenedor.getParam(Val_DocumentoRelacionado, "@Val_DocumentoRelacionado", SqlDbType.Bit)

            Dim cmd As New SqlCommand("_Cheque_ValSelect_AddDatoCancelacion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Cheque")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Cheque")

    End Function
    Public Function Registrar(ByVal objCheque As Entidades.Cheque, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(16) {}
        p(0) = objDaoMantenedor.getParam(objCheque.IdCheque, "@IdCheque", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objCheque.IdMedioPago, "@IdMedioPago", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objCheque.IdMoneda, "@IdMoneda", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(objCheque.Monto, "@ch_Monto", SqlDbType.Decimal)
        p(4) = objDaoMantenedor.getParam(objCheque.IdBanco, "@IdBanco", SqlDbType.Int)
        p(5) = objDaoMantenedor.getParam(objCheque.Numero, "@ch_Numero", SqlDbType.VarChar)
        p(6) = objDaoMantenedor.getParam(objCheque.FechaCobrar, "@ch_FechaCobrar", SqlDbType.Date)
        p(7) = objDaoMantenedor.getParam(objCheque.EstadoMov, "@ch_EstadoMov", SqlDbType.VarChar)
        p(8) = objDaoMantenedor.getParam(objCheque.Estado, "@ch_Estado", SqlDbType.Bit)
        p(9) = objDaoMantenedor.getParam(objCheque.IdTipoMovimiento, "@IdTipoMovimiento", SqlDbType.Int)
        p(10) = objDaoMantenedor.getParam(objCheque.Observacion, "@ch_Observacion", SqlDbType.VarChar)
        p(11) = objDaoMantenedor.getParam(objCheque.IdCliente, "@IdCliente", SqlDbType.Int)
        p(12) = objDaoMantenedor.getParam(objCheque.IdUsuario, "@IdUsuario", SqlDbType.Int)
        p(13) = objDaoMantenedor.getParam(objCheque.IdUsuarioSupervisor, "@IdUsuarioSupervisor", SqlDbType.Int)
        p(14) = objDaoMantenedor.getParam(objCheque.FechaMov, "@ch_FechaMov", SqlDbType.Date)
        p(15) = objDaoMantenedor.getParam(objCheque.IdEmpresa, "@IdEmpresa", SqlDbType.Int)
        p(16) = objDaoMantenedor.getParam(objCheque.IdTienda, "@IdTienda", SqlDbType.Int)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Cheque_Registrar", p))

    End Function

    Public Function Cheque_SelectxParams_DT(ByVal IdCheque As Integer, ByVal IdCliente As Integer, ByVal IdBanco As Integer, ByVal IdUsuario As Integer, ByVal IdUsuarioSupervisor As Integer, ByVal FechaInicio_Find As Date, ByVal FechaFin_Find As Date, ByVal FechaCobrar_Inicio As Date, ByVal FechaCobrar_Fin As Date, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Estado As Boolean, ByVal EstadoMov As String) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(12) {}
            p(0) = objDaoMantenedor.getParam(IdCheque, "@IdCheque", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdCliente, "@IdCliente", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdBanco, "@IdBanco", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdUsuario, "@IdUsuario", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(IdUsuarioSupervisor, "@IdUsuarioSupervisor", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(FechaInicio_Find, "@FechaInicio_Find", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(FechaFin_Find, "@FechaFin_Find", SqlDbType.Date)
            p(7) = objDaoMantenedor.getParam(FechaCobrar_Inicio, "@FechaCobrar_Inicio", SqlDbType.Date)
            p(8) = objDaoMantenedor.getParam(FechaCobrar_Fin, "@FechaCobrar_Fin", SqlDbType.Date)
            p(9) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(10) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(11) = objDaoMantenedor.getParam(Estado, "@Estado", SqlDbType.Bit)
            p(12) = objDaoMantenedor.getParam(EstadoMov, "@EstadoMov", SqlDbType.VarChar)

            Dim cmd As New SqlCommand("_Cheque_SelectxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Cheque")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Cheque")

    End Function

    Public Function Cheque_SelectxIdCheque(ByVal IdCheque As Integer) As Entidades.Cheque

        Dim objCheque As Entidades.Cheque = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(12) {}
            p(0) = objDaoMantenedor.getParam(IdCheque, "@IdCheque", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(Nothing, "@IdCliente", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(Nothing, "@IdBanco", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Nothing, "@IdUsuario", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(Nothing, "@IdUsuarioSupervisor", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(Nothing, "@FechaInicio_Find", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(Nothing, "@FechaFin_Find", SqlDbType.Date)
            p(7) = objDaoMantenedor.getParam(Nothing, "@FechaCobrar_Inicio", SqlDbType.Date)
            p(8) = objDaoMantenedor.getParam(Nothing, "@FechaCobrar_Fin", SqlDbType.Date)
            p(9) = objDaoMantenedor.getParam(Nothing, "@IdEmpresa", SqlDbType.Int)
            p(10) = objDaoMantenedor.getParam(Nothing, "@IdTienda", SqlDbType.Int)
            p(11) = objDaoMantenedor.getParam(Nothing, "@Estado", SqlDbType.Bit)
            p(12) = objDaoMantenedor.getParam(Nothing, "@EstadoMov", SqlDbType.VarChar)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Cheque_SelectxParams", p)
            If (reader.Read) Then

                objCheque = New Entidades.Cheque
                With objCheque

                    .IdCheque = objDaoMantenedor.UCInt(reader("IdCheque"))
                    .IdMedioPago = objDaoMantenedor.UCInt(reader("IdMedioPago"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .Monto = objDaoMantenedor.UCDec(reader("Monto"))
                    .IdBanco = objDaoMantenedor.UCInt(reader("IdBanco"))
                    .Numero = objDaoMantenedor.UCStr(reader("NroCheque"))
                    .FechaCobrar = objDaoMantenedor.UCDate(reader("FechaCobrar"))
                    .EstadoMov = objDaoMantenedor.UCStr(reader("EstadoMov"))
                    .EstadoMov_Desc = objDaoMantenedor.UCStr(reader("EstadoMov_Desc"))
                    .Estado = objDaoMantenedor.UCBool(reader("Estado"))
                    .IdTipoMovimiento = objDaoMantenedor.UCInt(reader("IdTipoMovimiento"))
                    .Observacion = objDaoMantenedor.UCStr(reader("Observacion"))
                    .IdCliente = objDaoMantenedor.UCInt(reader("IdCliente"))
                    .IdUsuario = objDaoMantenedor.UCInt(reader("IdUsuario"))
                    .IdUsuarioSupervisor = objDaoMantenedor.UCInt(reader("IdUsuarioSupervisor"))
                    .FechaMov = objDaoMantenedor.UCDate(reader("FechaMov"))
                    .FechaRegistro = objDaoMantenedor.UCDate(reader("FechaRegistro"))
                    .MedioPago = objDaoMantenedor.UCStr(reader("MedioPago"))
                    .Banco = objDaoMantenedor.UCStr(reader("Banco"))
                    .Moneda = objDaoMantenedor.UCStr(reader("Moneda"))
                    .TipoMovimiento = objDaoMantenedor.UCStr(reader("TipoMovimiento"))
                    .IdMedioPagoInterfaz = objDaoMantenedor.UCInt(reader("IdMedioPagoInterfaz"))

                End With

            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objCheque

    End Function

    Public Function Cheque_SelectxParams_ValBanco(ByVal IdCheque As Integer, ByVal IdCliente As Integer, ByVal IdBanco As Integer, ByVal IdUsuario As Integer, ByVal IdUsuarioSupervisor As Integer, ByVal FechaInicio_Find As Date, ByVal FechaFin_Find As Date, ByVal FechaCobrar_Inicio As Date, ByVal FechaCobrar_Fin As Date, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Estado As Boolean, ByVal EstadoMov As String) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(12) {}
            p(0) = objDaoMantenedor.getParam(IdCheque, "@IdCheque", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdCliente, "@IdCliente", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdBanco, "@IdBanco", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdUsuario, "@IdUsuario", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(IdUsuarioSupervisor, "@IdUsuarioSupervisor", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(FechaInicio_Find, "@FechaInicio_Find", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(FechaFin_Find, "@FechaFin_Find", SqlDbType.Date)
            p(7) = objDaoMantenedor.getParam(FechaCobrar_Inicio, "@FechaCobrar_Inicio", SqlDbType.Date)
            p(8) = objDaoMantenedor.getParam(FechaCobrar_Fin, "@FechaCobrar_Fin", SqlDbType.Date)
            p(9) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(10) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(11) = objDaoMantenedor.getParam(Estado, "@Estado", SqlDbType.Bit)
            p(12) = objDaoMantenedor.getParam(EstadoMov, "@EstadoMov", SqlDbType.VarChar)

            Dim cmd As New SqlCommand("_Cheque_SelectxParams_ValBanco", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Cheque")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Cheque")

    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

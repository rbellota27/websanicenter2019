﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAODocReciboCaja
    Inherits DAO.DAODocumento
    Private objConexion As New DAO.Conexion

    Private objDaoMantenedor As New DAO.DAOMantenedor

    '******************* RECIBO EGRESO

    Public Function RPT_MovCajaxParams_DT(ByVal IdEmpresa As Integer, ByVal Idtienda As Integer, ByVal IdCaja As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As DataTable
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try


            Dim p() As SqlParameter = New SqlParameter(4) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(Idtienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdCaja, "@IdCaja", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(4) = objDaoMantenedor.getParam(FechaFin, "@FechaFin", SqlDbType.Date)

            Dim cmd As New SqlCommand("_DocumentoReciboEgreso_RPT_MovCajaxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_RptMovCaja")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_RptMovCaja")

    End Function


    Public Function SelectResumenMontoxSustentar_DT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer) As DataTable

        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try


            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdCaja, "@IdCaja", SqlDbType.Int)

            Dim cmd As New SqlCommand("_DocumentoReciboEgreso_SelectResumenMontoxSustentar", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_ReciboEgreso_ResumenMontoxSustentar")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_ReciboEgreso_ResumenMontoxSustentar")

    End Function

    '*********** Reporte Documento Recibo Egreso
    Public Function getDataSet_DocReciboEgreso(ByVal IdDocumento As Integer) As DataSet

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim DS_DocReciboEgreso As New DataSet
        Try

            Dim da As SqlDataAdapter = Nothing

            '*********** CABECERA
            Dim cmd As New SqlCommand("_CR_DocumentoReciboEgresoCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            da = New SqlDataAdapter(cmd)
            da.Fill(DS_DocReciboEgreso, "DT_DocumentoReciboEgresoCab")

            '********* DETALLE CONCEPTO
            cmd.CommandText = "_CR_DocumentoReciboEgresoDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(DS_DocReciboEgreso, "DT_DocumentoReciboEgresoDet")

            '************ DETALLE DATOS CANCELACION
            cmd.CommandText = "_CR_DocumentoReciboEgresoCancelacion"
            da = New SqlDataAdapter(cmd)
            da.Fill(DS_DocReciboEgreso, "DT_DocumentoReciboEgresoCancelacion")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return DS_DocReciboEgreso

    End Function


    '********* Inserta el Documento ReciboCaja Egreso y retorna el IdDocumento
    Public Function ReciboCajaEgresoInsert(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal objObservaciones As Entidades.Observacion) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************* Insertamos la cabecera
            objDocumento.Id = MyBase.InsertaDocumento(cn, objDocumento, tr)

            '********************* INSERTAMOS DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1

                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, True)

            Next


            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            If (objMovCaja IsNot Nothing) Then

                '*********** Inserto Mov Caja
                Dim objDaoMovCaja As New DAO.DAOMovCaja
                objMovCaja.IdDocumento = objDocumento.Id
                objDaoMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                '*********** Inserto Lista Cancelacion
                Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                For i As Integer = 0 To listaCancelacion.Count - 1
                    listaCancelacion(i).IdDocumento = objDocumento.Id
                    objDaoPagoCaja.InsertaPagoCaja(cn, listaCancelacion(i), tr)
                Next

            End If

            '************ RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            If (objRelacionDocumento IsNot Nothing) Then
                objRelacionDocumento.IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(objRelacionDocumento, cn, tr)
            End If

            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function ReciboCajaEgresoUpdate(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal objObservaciones As Entidades.Observacion) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ Deshacemos lo hecho (DetalleRecibo / MovCaja / PagoCaja) 
            Me.DocumentoReciboEgresoDeshacerMov(objDocumento.Id, True, True, True, True, True, False, cn, tr)

            '************* Actualizamos la cabecera
            MyBase.DocumentoUpdate(objDocumento, cn, tr)

            '********************* INSERTAMOS DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1

                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, True)

            Next


            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            If (objMovCaja IsNot Nothing) Then

                '*********** Inserto Mov Caja
                Dim objDaoMovCaja As New DAO.DAOMovCaja
                objMovCaja.IdDocumento = objDocumento.Id
                objDaoMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                '*********** Inserto Lista Cancelacion
                Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                For i As Integer = 0 To listaCancelacion.Count - 1
                    listaCancelacion(i).IdDocumento = objDocumento.Id
                    objDaoPagoCaja.InsertaPagoCaja(cn, listaCancelacion(i), tr)
                Next

            End If

            '************ RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            If (objRelacionDocumento IsNot Nothing) Then
                objRelacionDocumento.IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(objRelacionDocumento, cn, tr)
            End If

            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)

            End If


            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    Public Function ReciboCajaEgresoAnular(ByVal IdDocumento As Integer) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ Anulamos el documento
            Me.DocumentoReciboEgresoDeshacerMov(IdDocumento, False, True, True, False, False, True, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Private Sub DocumentoReciboEgresoDeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteDetalleConcepto As Boolean, ByVal DeleteMovCaja As Boolean, ByVal DeletePagoCaja As Boolean, ByVal DeleteRelacionDocumento As Boolean, ByVal DeleteObservacion As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoReciboEgresoDeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@DeleteDetalleConcepto", DeleteDetalleConcepto)
        cmd.Parameters.AddWithValue("@DeleteMovCaja", DeleteMovCaja)
        cmd.Parameters.AddWithValue("@DeletePagoCaja", DeletePagoCaja)
        cmd.Parameters.AddWithValue("@DeleteRelacionDocumento", DeleteRelacionDocumento)
        cmd.Parameters.AddWithValue("@DeleteObservacion", DeleteObservacion)
        cmd.Parameters.AddWithValue("@Anular", Anular)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub





    '***************  RECIBO INGRESO
    '********* Actualiza el Documento ReciboCaja Ingreso y retorna el IdDocumento
    Public Function ReciboCajaIngresoUpdate(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objObservaciones As Entidades.Observacion, ByVal listaMovBanco As List(Of Entidades.MovBancoView), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ Deshacemos lo hecho (DetalleRecibo / MovCaja / PagoCaja) 
            Me.DocumentoReciboIngreso_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, True, False, cn, tr)

            '************* ACTUALIZAMOS la cabecera
            MyBase.DocumentoUpdate(objDocumento, cn, tr)

            '************* Insertamos el detalle Recibo
            Dim objDaoDetalleRecibo As New DAO.DAODetalleRecibo
            For i As Integer = 0 To listaDetalleConcepto.Count - 1

                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                Me.DocumentoReciboIngreso_InsertDetalleConcepto(listaDetalleConcepto(i), cn, tr)

            Next

            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            If (objMovCaja IsNot Nothing) Then

                '*********** Inserto Mov Caja
                Dim objDaoMovCaja As New DAO.DAOMovCaja
                objMovCaja.IdDocumento = objDocumento.Id
                objDaoMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                '*********** Inserto Lista Cancelacion
                Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                For i As Integer = 0 To listaCancelacion.Count - 1
                    listaCancelacion(i).IdDocumento = objDocumento.Id
                    objDaoPagoCaja.InsertaPagoCaja(cn, listaCancelacion(i), tr)
                Next

            End If


            If (objObservaciones IsNot Nothing) Then

                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
            Dim objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
            For i As Integer = 0 To listaMovBanco.Count - 1
                listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                If (listaMovBanco(i).IdMovBanco <> Nothing) Then
                    objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdBanco), cn, tr)
                End If

            Next

            '************************ INSERTAMOS EN RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1

                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    '********* Inserta el Documento ReciboCaja Ingreso y retorna el IdDocumento
    Public Function ReciboCajaIngresoInsert(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objObservaciones As Entidades.Observacion, ByVal listaMovBanco As List(Of Entidades.MovBancoView), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************* Insertamos la cabecera
            objDocumento.Id = MyBase.InsertaDocumento(cn, objDocumento, tr)

            '************* Insertamos el DETALLE CONCEPTO
            For i As Integer = 0 To listaDetalleConcepto.Count - 1

                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                Me.DocumentoReciboIngreso_InsertDetalleConcepto(listaDetalleConcepto(i), cn, tr)

            Next

            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            If (objMovCaja IsNot Nothing) Then

                '*********** Inserto Mov Caja
                Dim objDaoMovCaja As New DAO.DAOMovCaja
                objMovCaja.IdDocumento = objDocumento.Id
                objDaoMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                '*********** Inserto Lista Cancelacion
                Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                For i As Integer = 0 To listaCancelacion.Count - 1
                    listaCancelacion(i).IdDocumento = objDocumento.Id
                    objDaoPagoCaja.InsertaPagoCaja(cn, listaCancelacion(i), tr)
                Next

            End If

            If (objObservaciones IsNot Nothing) Then

                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)

            End If

            Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
            Dim objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
            For i As Integer = 0 To listaMovBanco.Count - 1
                listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                If (listaMovBanco(i).IdMovBanco <> Nothing) Then
                    objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdBanco), cn, tr)
                End If

            Next

            '************************ INSERTAMOS EN RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1

                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function



    Public Function DocumentoReciboIngresoDeshacerMov_Edicion(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            DocumentoReciboIngreso_DeshacerMov(IdDocumento, True, True, True, True, True, True, True, True, False, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False
            Throw ex

        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function DocumentoReciboIngresoAnular(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            DocumentoReciboIngreso_DeshacerMov(IdDocumento, True, True, False, True, True, True, True, False, True, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False

        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Sub DocumentoReciboIngreso_InsertDetalleConcepto(ByVal objDetalleConcepto As Entidades.DetalleConcepto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim param() As SqlParameter = New SqlParameter(6) {}
        param(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        param(0).Value = IIf(objDetalleConcepto.IdDocumento = Nothing, DBNull.Value, objDetalleConcepto.IdDocumento)

        param(1) = New SqlParameter("@dr_Concepto", SqlDbType.VarChar)
        param(1).Value = IIf(objDetalleConcepto.Concepto = Nothing, DBNull.Value, objDetalleConcepto.Concepto)

        param(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        param(2).Value = IIf(objDetalleConcepto.IdMoneda = Nothing, DBNull.Value, objDetalleConcepto.IdMoneda)

        param(3) = New SqlParameter("@dr_Monto", SqlDbType.Decimal)
        param(3).Value = IIf(objDetalleConcepto.Monto = Nothing, DBNull.Value, objDetalleConcepto.Monto)

        param(4) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        param(4).Value = IIf(objDetalleConcepto.IdConcepto = Nothing, DBNull.Value, objDetalleConcepto.IdConcepto)

        param(5) = New SqlParameter("@IdDocumentoAfecto", SqlDbType.Int)
        param(5).Value = IIf(objDetalleConcepto.IdDocumentoRef = Nothing, DBNull.Value, objDetalleConcepto.IdDocumentoRef)

        param(6) = New SqlParameter("@IdMovCuentaAfecto", SqlDbType.Int)
        param(6).Value = IIf(objDetalleConcepto.IdMovCuentaRef = Nothing, DBNull.Value, objDetalleConcepto.IdMovCuentaRef)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoReciboIngreso_InsertDetalleConcepto", param)

    End Sub

    Public Sub DocumentoReciboIngreso_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeshacerMovimientos As Boolean, ByVal DeleteMovCuenta As Boolean, ByVal DeleteDetalleConcepto As Boolean, ByVal DeleteMovCaja As Boolean, ByVal DeletePagoCaja As Boolean, ByVal DeleteMovBanco As Boolean, ByVal DeleteRelacionDocumento As Boolean, ByVal DeleteObservacion As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim param() As SqlParameter = New SqlParameter(9) {}

        param(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        param(0).Value = IdDocumento

        param(1) = New SqlParameter("@DeshacerMovimientos", SqlDbType.Bit)
        param(1).Value = DeshacerMovimientos

        param(2) = New SqlParameter("@DeleteMovCuenta", SqlDbType.Bit)
        param(2).Value = DeleteMovCuenta

        param(3) = New SqlParameter("@DeleteDetalleConcepto", SqlDbType.Bit)
        param(3).Value = DeleteDetalleConcepto

        param(4) = New SqlParameter("@DeleteMovCaja", SqlDbType.Bit)
        param(4).Value = DeleteMovCaja


        param(5) = New SqlParameter("@DeletePagoCaja", SqlDbType.Bit)
        param(5).Value = DeletePagoCaja

        param(6) = New SqlParameter("@DeleteMovBanco", SqlDbType.Bit)
        param(6).Value = DeleteMovBanco


        param(7) = New SqlParameter("@DeleteRelacionDocumento", SqlDbType.Bit)
        param(7).Value = DeleteRelacionDocumento

        param(8) = New SqlParameter("@DeleteObservacion", SqlDbType.Bit)
        param(8).Value = DeleteObservacion

        param(9) = New SqlParameter("@Anular", SqlDbType.Bit)
        param(9).Value = Anular

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoReciboIngreso_DeshacerMov", param)

    End Sub

End Class

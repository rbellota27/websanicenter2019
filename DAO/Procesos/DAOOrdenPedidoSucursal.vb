﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Public Class DAOOrdenPedidoSucursal

    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private reader As SqlDataReader
    Dim ObjConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor


    Public Function DetalleDocumento_Ref(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_DetalleDocumento_OPVP_DocRef]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .CantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                        .Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        .TasaDetraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))
                        .TasaPercepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        .Utilidad = CDec(IIf(IsDBNull(lector.Item("dc_Utilidad")) = True, 0, lector.Item("dc_Utilidad")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))

                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))


                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .IdDocumentoRef = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .CantidadDetalleAfecto = CDec(IIf(IsDBNull(lector.Item("CantidadDetalleAfecto")) = True, 0, lector.Item("CantidadDetalleAfecto")))

                        .CantidadTransito = objDaoMantenedor.UCDec(lector("dc_CantidadTransito"))
                        .Moneda = objDaoMantenedor.UCStr(lector("Moneda"))

                        .Peso = objDaoMantenedor.UCDec(lector("dc_Peso"))
                        .UmPeso = objDaoMantenedor.UCStr(lector("dc_UmPeso"))
                        .IdUnidadMedida_Peso = objDaoMantenedor.UCInt(lector("IdUnidadMedida_Peso"))
                        .Kit = objDaoMantenedor.UCBool(lector("prod_Kit"))
                        .IdKit = objDaoMantenedor.UCInt(lector("IdKit"))
                        .kit_Cantidad_Comp = objDaoMantenedor.UCInt(lector("kit_Cantidad_Comp"))
                        .StockTotalxAlmacen = CDec(IIf(IsDBNull(lector("StockTotalxAlmacen")) = True, 0, lector("StockTotalxAlmacen")))
                        .ComponenteKit = CBool(IIf(IsDBNull(lector("add_ComponenteKit")) = True, False, lector("add_ComponenteKit")))
                        .IdTipoCliente = CInt(IIf(IsDBNull(lector("IdRol")) = True, 0, lector("IdRol")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function Documento_BuscarDocumentoRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                ByVal IdtipoDocumentoRef As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Documento_OPVP_DocRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdtipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function Documento_BuscarDocumentoRef2(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                               ByVal IdtipoDocumentoRef As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Documento_OPVP_DocRef2_POR_BORRAR", cn)
        'Dim cmd As New SqlCommand("_Documento_OPVP_DocRef2_POR_BORRAR", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdtipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                        .strTipoOperacionRef = CStr(IIf(IsDBNull(lector.Item("TipoOperacion")) = True, "", lector.Item("TipoOperacion")))
                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function




    Public Sub DocumentoOrdenPedidoSucursal_ValDocumentoRef(ByVal IdDocumentoRef As Integer)

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
            p(0).Value = IdDocumentoRef

            SqlHelper.ExecuteNonQuery(cn, CommandType.StoredProcedure, "_DocumentoOrdenPedidoSucursal_ValDocumentoRef", p)


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

    End Sub

    Public Sub DocumentoOrdenPedido_DeshacerCantidadAprobada(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(0) {}
        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IdDocumento


        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoOrdenPedido_DeshacerCantidadAprobada", p)

    End Sub

    Public Sub DocumentoOrdenPedido_AprobarCantidadOPInsert(ByVal IdDetalleDocumento As Integer, ByVal CantidadAprob As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
        p(0).Value = IdDetalleDocumento
        p(1) = New SqlParameter("@CantidadAprob", SqlDbType.Decimal)
        p(1).Value = CantidadAprob

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoOrdenPedido_AprobarCantidadOP", p)

    End Sub



    Public Function DocumentoOrdenPedidoSucursal_SelectDetalle_Aprobacion_DT(ByVal IdDocumento As Integer) As DataTable
        Dim ds As New DataSet

        Try

            cn = (New DAO.Conexion).ConexionSIGE

            Dim cmd As New SqlCommand("_DocumentoOrdenPedidoSucursal_SelectDetalle_Aprobacion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_OrdenPedidoDetalle")

        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return ds.Tables("DT_OrdenPedidoDetalle")

    End Function
    Public Function DocumentoOrdenPedido_SelectxParams_Aprobacion(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal EstadoAprob As Boolean, ByVal Serie As Integer, ByVal Codigo As Integer) As DataTable

        Dim ds As New DataSet

        Try

            cn = (New DAO.Conexion).ConexionSIGE

            Dim cmd As New SqlCommand("_DocumentoOrdenPedido_SelectxParams_Aprobacion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
            cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
            cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
            cmd.Parameters.AddWithValue("@EstadoAprob", EstadoAprob)

            cmd.Parameters.AddWithValue("@Serie", Serie)
            cmd.Parameters.AddWithValue("@Codigo", Codigo)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_OrdenPedido")

        Catch ex As Exception
            Throw ex
        Finally

        End Try


        Return ds.Tables("DT_OrdenPedido")


    End Function
    Public Sub DocumentoOrdenPedidoSucursal_DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteDetalleDocumento As Boolean, ByVal deleteMovAlmacen As Boolean, ByVal deleteRelacionDoc As Boolean, ByVal deleteObservaciones As Boolean, ByVal DeletePuntoPartida As Boolean, ByVal deletePuntoLlegada As Boolean, ByVal deleteAnexoDocumento As Boolean, ByVal deleteAnexoDetalleD As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoOrdenPedidoSucursal_DeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@deleteDetalleDocumento", deleteDetalleDocumento)
        cmd.Parameters.AddWithValue("@deleteMovAlmacen", deleteMovAlmacen)
        cmd.Parameters.AddWithValue("@deleteRelacionDoc", deleteRelacionDoc)
        cmd.Parameters.AddWithValue("@deleteObservaciones", deleteObservaciones)
        cmd.Parameters.AddWithValue("@DeletePuntoPartida", DeletePuntoPartida)
        cmd.Parameters.AddWithValue("@deletePuntoLlegada", deletePuntoLlegada)
        cmd.Parameters.AddWithValue("@deleteAnexoDocumento", deleteAnexoDocumento)
        cmd.Parameters.AddWithValue("@deleteAnexoDetalleD", deleteAnexoDetalleD)
        cmd.Parameters.AddWithValue("@Anular", Anular)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Public Function DocumentoOrdenPedidoSucursal_SelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim Lista As New List(Of Entidades.DetalleDocumento)

        Try

            cn = (New DAO.Conexion).ConexionSIGE

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Value = IdDocumento

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoOrdenPedidoSucursal_SelectDetalle", p)


            While reader.Read
                Dim obj As New Entidades.DetalleDocumento
                With obj

                    .Cantidad = CDec(IIf(IsDBNull(reader("dc_Cantidad")) = True, 0, reader("dc_Cantidad")))
                    .IdDocumento = CInt(IIf(IsDBNull(reader("IdDocumento")) = True, 0, reader("IdDocumento")))
                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")) = True, 0, reader("IdProducto")))
                    .UMedida = CStr(IIf(IsDBNull(reader("dc_UMedida")) = True, "", reader("dc_UMedida")))
                    .NomProducto = CStr(IIf(IsDBNull(reader("prod_Nombre")) = True, "", reader("prod_Nombre")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(reader("IdUnidadMedida")) = True, 0, reader("IdUnidadMedida")))
                    .IdDetalleDocumento = CInt(IIf(IsDBNull(reader("IdDetalleDocumento")) = True, 0, reader("IdDetalleDocumento")))
                    .IdDetalleAfecto = CInt(IIf(IsDBNull(reader("IdDetalleAfecto")) = True, 0, reader("IdDetalleAfecto")))
                    .CantidadxAtenderText = CStr(IIf(IsDBNull(reader("CantidadRefText")) = True, "", reader("CantidadRefText")))
                    .CantADespacharOriginal = CDec(IIf(IsDBNull(reader("CantidadRef")) = True, 0, reader("CantidadRef")))  '**** CANTIDAD DEL DOCUMENTO REF
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("prod_Codigo")) = True, "", reader("prod_Codigo")))
                    .IdDocumentoRef = CInt(IIf(IsDBNull(reader("IdDocumentoRef")) = True, 0, reader("IdDocumentoRef")))
                    ''.StockTotalxAlmacen = CDec(IIf(IsDBNull(reader("StockAlmacen")) = True, 0, reader("StockAlmacen")))
                    .ComponenteKit = CBool(IIf(IsDBNull(reader("add_ComponenteKit")) = True, False, reader("add_ComponenteKit")))
                    .Kit = CBool(IIf(IsDBNull(reader("prod_Kit")) = True, False, reader("prod_Kit")))

                End With
                Lista.Add(obj)
            End While

        Catch ex As Exception

            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return Lista

    End Function


    Public Function ReporteAprobacionPedido(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idalmacenDA As Integer, ByVal iddocumento As Integer, _
                         ByVal FechaInicio As String, ByVal Fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, _
ByVal yearf As Integer, ByVal semanaF As Integer, _
ByVal FiltraSemana As Integer, ByVal stocktransitoxtienda As Integer, ByVal tpAlmacen As DataTable) As DataSet

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Try
            Dim cmdD As New SqlCommand("[_CR_VentasStock_Analisis]", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idempresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idalmacenOtorgante", idalmacenDA)
            cmdD.Parameters.AddWithValue("@iddocumento", iddocumento)
            cmdD.Parameters.AddWithValue("@fechainicio", FechaInicio)
            cmdD.Parameters.AddWithValue("@fechafin", Fechafin)
            cmdD.Parameters.AddWithValue("@yeari", yeari)
            cmdD.Parameters.AddWithValue("@semanai", semanai)
            cmdD.Parameters.AddWithValue("@yearf", yearf)
            cmdD.Parameters.AddWithValue("@semanaf", semanaF)
            cmdD.Parameters.AddWithValue("@filtrarsemana", FiltraSemana)
            cmdD.Parameters.AddWithValue("@tbTipoAlmacen", tpAlmacen)
            cmdD.Parameters.AddWithValue("@StockTransitoxtienda", stocktransitoxtienda)


            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_DT_VentasStock")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function




    Public Function OrdenPedidoAprobacion(ByVal idempresa As Integer, ByVal iddocumento As Integer, _
                        ByVal FechaInicio As String, ByVal Fechafin As String, _
                        ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaF As Integer, _
                        ByVal FiltraSemana As Integer, ByVal tpAlmacen As DataTable) As DataSet

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Try
            Dim cmdD As New SqlCommand("[dbo].[_OrdenPedidoAprobacion]", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idempresa)
            cmdD.Parameters.AddWithValue("@iddocumento", iddocumento)

            cmdD.Parameters.AddWithValue("@FechaIni", FechaInicio)
            cmdD.Parameters.AddWithValue("@FechaFin", Fechafin)

            cmdD.Parameters.AddWithValue("@yeari", yeari)
            cmdD.Parameters.AddWithValue("@semanai", semanai)
            cmdD.Parameters.AddWithValue("@yearf", yearf)
            cmdD.Parameters.AddWithValue("@semanaf", semanaF)

            cmdD.Parameters.AddWithValue("@filtrarsemana", FiltraSemana)
            cmdD.Parameters.AddWithValue("@TipoAlmacen", tpAlmacen)

            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_DT_VentasStock")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function


End Class

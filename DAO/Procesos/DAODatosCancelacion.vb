﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODatosCancelacion
    Inherits DAOMantenedor


    Public Function DeshacerMov_Edicion(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            DeshacerMov(IdDocumento, True, True, True, True, True, True, True, True, True, True, False, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False
            Throw ex

        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function Anular(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            DeshacerMov(IdDocumento, True, True, False, True, True, True, True, True, False, False, True, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False

        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Sub DeshacerMov(ByVal IdDocumento As Integer, ByVal DeshacerMovimientos As Boolean, _
    ByVal DeleteMovCuenta As Boolean, ByVal DeleteDetalleConcepto As Boolean, _
    ByVal DeleteDatosCancelacion As Boolean, _
    ByVal DeleteMovCaja As Boolean, ByVal DeletePagoCaja As Boolean, _
    ByVal DeleteMovBanco As Boolean, ByVal DeleteRelacionDocumento As Boolean, _
    ByVal DeleteObservacion As Boolean, ByVal DeleteAnexoDocumento As Boolean, ByVal Anular As Boolean, _
    ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            'Aqui se considera eliminar los datos cancelacion
            Dim param() As SqlParameter = New SqlParameter(11) {}

            param(0) = getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
            param(1) = getParam(DeshacerMovimientos, "@DeshacerMovimientos", SqlDbType.Bit)
            param(2) = getParam(DeleteMovCuenta, "@DeleteMovCuentaPorPagar", SqlDbType.Bit)
            param(3) = getParam(DeleteDetalleConcepto, "@DeleteDetalleConcepto", SqlDbType.Bit)
            param(4) = getParam(DeleteDetalleConcepto, "@DeleteDatosCancelacion", SqlDbType.Bit)
            param(5) = getParam(DeleteMovCaja, "@DeleteMovCaja", SqlDbType.Bit)
            param(6) = getParam(DeletePagoCaja, "@DeletePagoCaja", SqlDbType.Bit)
            param(7) = getParam(DeleteMovBanco, "@DeleteMovBanco", SqlDbType.Bit)
            param(8) = getParam(DeleteRelacionDocumento, "@DeleteRelacionDocumento", SqlDbType.Bit)
            param(9) = getParam(DeleteObservacion, "@DeleteObservacion", SqlDbType.Bit)
            param(10) = getParam(Anular, "@Anular", SqlDbType.Bit)
            param(11) = getParam(DeleteAnexoDocumento, "@DeleteAnexoDocumento", SqlDbType.Bit)

            QueryT(param, modo_query.Update, "_DocumentoDatosCancelacion_DeshacerMov", cnx, trx)
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Public Function SelectListxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DatosCancelacion)
        Cn = objConexion.ConexionSIGE()

        'Dim Prm() As SqlParameter = New SqlParameter(0) {}
        'Prm(0) = getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
        Dim Prm As SqlParameter = getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_DatosCancelacionSelectxIdDocumento", Prm)

                Dim Lista As New List(Of Entidades.DatosCancelacion)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.DatosCancelacion

                        obj.Id = UCInt(.Item("IdDatosCanc"))
                        obj.IdDocumento = UCInt(.Item("IdDocumento"))
                        obj.IdMedioPago = UCInt(.Item("IdMedioPago"))
                        obj.IdDocumentoRef = UCInt(.Item("IdDocumentoRef"))
                        obj.IdBanco = UCInt(.Item("IdBanco"))
                        obj.IdCuentaBancaria = UCInt(.Item("IdCuentaBancaria"))
                        obj.IdTipoMovimiento = UCInt(.Item("IdTipoMovimiento"))
                        obj.NroOperacion = UCStr(.Item("dcan_NroOperacion"))
                        obj.FechaCanc = UCDate(.Item("dcan_FechaCanc"))
                        obj.Monto = UCDec(.Item("dcan_monto"))
                        obj.IdMoneda = UCInt(.Item("IdMoneda"))
                        obj.IdCaja = UCInt(.Item("IdCaja"))
                        obj.Observacion = UCStr(.Item("dcan_Observacion"))
                        obj.Factor = UCInt(.Item("dcan_Factor"))
                        obj.FechaCanc = UCDate(.Item("dcan_FechaACobrar"))

                        obj.Banco = UCStr(.Item("Banco"))
                        obj.CuentaBancaria = UCStr(.Item("CuentaBancaria"))
                        obj.TipoMovimiento = UCStr(.Item("TipoMovimiento"))
                        obj.Moneda = UCStr(.Item("mon_Simbolo"))
                        obj.MedioPago = UCStr(.Item("MedioPago"))

                        obj.IdMedioPagoInterfaz = UCInt(.Item("IdMedioPagoInterfaz"))
                        obj.IdConceptoMovBanco = UCInt(.Item("IdConceptoMovBanco"))
                        obj.ConceptoMovBanco = UCStr(.Item("ConceptoMovBancoDescripBreve"))
                        obj.Descripcion = UCStr(.Item("dcan_NumeroCheque"))
                        ' obj.Descripcion = UCStr(.Item("Descripcion"))

                        obj.IdMonedaDestino = UCInt(.Item("IdMonedaDestino"))
                        obj.MonedaSimboloDestino = UCStr(.Item("MonedaSimboloDestino"))
                        obj.MontoEquivalenteDestino = UCDec(.Item("MontoEquivalenteDestino"))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function InsertT_GetID(ByVal x As Entidades.DatosCancelacion, _
Optional ByRef Cnx As SqlConnection = Nothing, _
Optional ByRef Trx As SqlTransaction = Nothing) As Integer

        Return QueryT(GetVectorParametros(x, modo_query.Insert), modo_query.Insert, "_DatosCancelacionInsert", Cnx, Trx)
    End Function

    Public Function InsertT_GetID_V2(ByVal x As Entidades.DatosCancelacion, _
Optional ByRef Cnx As SqlConnection = Nothing, _
Optional ByRef Trx As SqlTransaction = Nothing) As Integer

        Return QueryT(GetVectorParametros(x, modo_query.Insert), modo_query.Insert, "_DatosCancelacionInsert_V2", Cnx, Trx)
    End Function
    Public Function InsertT(ByVal x As Entidades.DatosCancelacion, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean
        If InsertT_GetID_V2(x, Cnx, Trx) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function UpdateT(ByVal x As Entidades.DatosCancelacion, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean

        Return CBool(QueryT(GetVectorParametros(x, modo_query.Update), modo_query.Update, "_DatosCancelacionUpdate", Cnx, Trx))

    End Function

    'Public Function InsertaDatosCancelacion(ByVal listaCancelacion As List(Of Entidades.DatosCancelacion)) As Boolean

    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    cn.Open()
    '    Dim cmd As New SqlCommand("_DatosCancelacionInsert", cn, Tr)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandTimeout = 0

    '    For i As Integer = 0 To listaCancelacion.Count - 1

    '        Dim x As Entidades.DatosCancelacion = listaCancelacion(i)

    '        Dim ArrayParametros() As SqlParameter = New SqlParameter(11) {}


    '        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
    '        ArrayParametros(0).Value = IIf(x.IdDocumento = Nothing, DBNull.Value, x.IdDocumento)

    '        ArrayParametros(1) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
    '        ArrayParametros(1).Value = x.IdDocumentoRef

    '        ArrayParametros(2) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
    '        ArrayParametros(2).Value = IIf(x.IdMedioPago = Nothing, DBNull.Value, x.IdMedioPago)

    '        ArrayParametros(3) = New SqlParameter("@IdBanco", SqlDbType.Int)
    '        ArrayParametros(3).Value = IIf(x.IdBanco = Nothing, DBNull.Value, x.IdBanco)

    '        ArrayParametros(4) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
    '        ArrayParametros(4).Value = IIf(x.IdCuentaBancaria = Nothing, DBNull.Value, x.IdCuentaBancaria)

    '        ArrayParametros(5) = New SqlParameter("@IdTipoMovimiento", SqlDbType.VarChar)
    '        ArrayParametros(5).Value = IIf(x.IdTipoMovimiento = Nothing, DBNull.Value, x.IdTipoMovimiento)

    '        ArrayParametros(6) = New SqlParameter("@dcan_NroOperacion", SqlDbType.VarChar)
    '        ArrayParametros(6).Value = IIf(x.NroOperacion = Nothing, DBNull.Value, x.NroOperacion)

    '        ArrayParametros(7) = New SqlParameter("@dcan_FechaCanc", SqlDbType.Date)
    '        ArrayParametros(7).Value = IIf(x.FechaCanc = Nothing, DBNull.Value, x.FechaCanc)

    '        ArrayParametros(8) = New SqlParameter("@dcan_monto", SqlDbType.Decimal)
    '        ArrayParametros(8).Value = IIf(x.Monto = Nothing, DBNull.Value, x.Monto)

    '        ArrayParametros(9) = New SqlParameter("@IdMoneda", SqlDbType.Int)
    '        ArrayParametros(9).Value = IIf(x.IdMoneda = Nothing, DBNull.Value, x.IdMoneda)

    '        ArrayParametros(10) = New SqlParameter("@IdCaja", SqlDbType.Int)
    '        ArrayParametros(10).Value = IIf(x.IdCaja = Nothing, DBNull.Value, x.IdCaja)

    '        ArrayParametros(11) = New SqlParameter("@dcan_NumeroCheque", SqlDbType.VarChar)
    '        ArrayParametros(11).Value = IIf(x.Descripcion = Nothing, DBNull.Value, x.Descripcion)

    '        cmd.Parameters.Clear()

    '        cmd.Parameters.AddRange(ArrayParametros)
    '        cmd.ExecuteNonQuery()

    '    Next
    '    cn.Close()

    'End Function


    Public Function GetVectorParametros(ByVal x As Entidades.DatosCancelacion, ByVal op As modo_query) As SqlParameter()

        Dim Prm() As SqlParameter

        Select Case op
            Case modo_query.Show, modo_query.Update, modo_query.Insert

                If op <> modo_query.Insert Then
                    Prm = New SqlParameter(13) {}
                    'Id de la Tabla
                    Prm(12) = getParam(x.Id, "@IdDatosCanc", SqlDbType.Int)
                Else
                    Prm = New SqlParameter(12) {}
                End If

                Prm(0) = getParam(x.IdDocumento, "@IdDocumento", SqlDbType.Int)
                Prm(1) = getParam(x.IdDocumentoRef, "@IdDocumentoRef", SqlDbType.Int)
                Prm(2) = getParam(x.IdMedioPago, "@IdMedioPago", SqlDbType.Int)
                Prm(3) = getParam(x.IdBanco, "@IdBanco", SqlDbType.Int)
                Prm(4) = getParam(x.IdCuentaBancaria, "@IdCuentaBancaria", SqlDbType.Int)
                Prm(5) = getParam(x.IdTipoMovimiento, "@IdTipoMovimiento", SqlDbType.VarChar)
                Prm(6) = getParam(x.NroOperacion, "@dcan_NroOperacion", SqlDbType.VarChar)
                Prm(7) = getParam(x.FechaCanc, "@dcan_FechaCanc", SqlDbType.Date)
                Prm(8) = getParam(x.Monto, "@dcan_monto", SqlDbType.Decimal)
                Prm(9) = getParam(x.IdMoneda, "@IdMoneda", SqlDbType.Int)
                Prm(10) = getParam(x.IdCaja, "@IdCaja", SqlDbType.Int)
                Prm(11) = getParam(x.Descripcion, "@dcan_NumeroCheque", SqlDbType.VarChar)
                Prm(12) = getParam(x.idProgrmacion, "@idProgramacion", SqlDbType.VarChar)
                'Case modo_query.Delete
            Case Else
                Prm = Nothing
        End Select

        Return Prm
    End Function

End Class

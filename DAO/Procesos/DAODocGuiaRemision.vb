﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocGuiaRemision
    Private objConexion As New DAO.Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection
    Private reader As SqlDataReader

    Public Function CostoFlete_AnalisisCostoFlete_PrecioVenta(ByVal Tabla_IdDocumento As DataTable, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal Producto As String, ByVal CodigoProducto As String, ByVal IdTiendaDestino As Integer, ByVal IdTipoPV As Integer, ByVal IdTiendaOrigen As Integer, ByVal IdMoneda As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(8) {}

            p(0) = New SqlParameter()
            p(0).Value = Tabla_IdDocumento
            p(0).ParameterName = "@Tabla_IdDocumento"

            p(1) = objDaoMantenedor.getParam(IdLinea, "@IdLinea", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdSubLinea, "@IdSubLinea", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Producto, "@Producto", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(CodigoProducto, "@CodigoProducto", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(IdTiendaOrigen, "@IdTiendaOrigen", SqlDbType.Int)
            p(6) = objDaoMantenedor.getParam(IdTiendaDestino, "@IdTiendaDestino", SqlDbType.Int)
            p(7) = objDaoMantenedor.getParam(IdTipoPV, "@IdTipoPV", SqlDbType.Int)
            p(8) = objDaoMantenedor.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)

            cn = objConexion.ConexionSIGE
            Dim cmd As New SqlCommand("_CostoFlete_AnalisisCostoFlete_PrecioVenta", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CuadroAnalisis_CostoFlete")


        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_CuadroAnalisis_CostoFlete")

    End Function

    Public Function DocumentoGuiaRemision_CostoFlete_SelectxParams(ByVal IdEmpresa As Integer, ByVal IdAlmacenOrigen As Integer, ByVal IdAlmacenDestino As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Serie As Integer, ByVal Codigo As Integer) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)

        Try

            Dim p() As SqlParameter = New SqlParameter(6) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdAlmacenOrigen, "@IdAlmacenOrigen", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdAlmacenDestino, "@IdAlmacenDestino", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(4) = objDaoMantenedor.getParam(FechaFin, "@FechaFin", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(Serie, "@Serie", SqlDbType.Int)
            p(6) = objDaoMantenedor.getParam(Codigo, "@Codigo", SqlDbType.Int)

            cn = objConexion.ConexionSIGE
            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoGuiaRemision_CostoFlete_SelectxParams", p)

            While (reader.Read)

                Dim objDocumento As New Entidades.Documento

                With objDocumento

                    .Id = objDaoMantenedor.UCInt(reader("IdDocumento"))
                    .Serie = objDaoMantenedor.UCStr(reader("Serie"))
                    .Codigo = objDaoMantenedor.UCStr(reader("Codigo"))
                    .FechaEmision = objDaoMantenedor.UCDate(reader("FechaEmision"))
                    .IdPersona = objDaoMantenedor.UCInt(reader("IdPersona"))
                    .IdTransportista = objDaoMantenedor.UCInt(reader("IdTransportista"))
                    .Empresa = objDaoMantenedor.UCStr(reader("Empresa"))
                    .Tienda = objDaoMantenedor.UCStr(reader("Tienda"))
                    .NomTipoDocumento = objDaoMantenedor.UCStr(reader("TipoDocumento"))
                    .DescripcionPersona = objDaoMantenedor.UCStr(reader("DescripcionPersona"))
                    .Transportista = objDaoMantenedor.UCStr(reader("Transportista"))
                    .NroDocumento = .Serie + " - " + .Codigo

                End With

                lista.Add(objDocumento)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function




    Public Sub DocumentoGuiaRemision_DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteDetalleDocumento As Boolean, ByVal deleteMovAlmacen As Boolean, ByVal deletePuntoPartida As Boolean, ByVal deletePuntoLlegada As Boolean, ByVal deleteObservaciones As Boolean, ByVal deleteRelacionDoc As Boolean, ByVal validar_Despacho As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try

            'Dim cmd As New SqlCommand("_DocumentoGuiaRemision_DeshacerMov", cn, tr)

            Dim cmd As New SqlCommand("_DocumentoGuiaRemision_DeshacerMov_V2", cn, tr)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@deleteDetalleDocumento", deleteDetalleDocumento)
            cmd.Parameters.AddWithValue("@deleteMovAlmacen", deleteMovAlmacen)
            cmd.Parameters.AddWithValue("@deletePuntoPartida", deletePuntoPartida)
            cmd.Parameters.AddWithValue("@deletePuntoLlegada", deletePuntoLlegada)
            cmd.Parameters.AddWithValue("@deleteObservaciones", deleteObservaciones)
            cmd.Parameters.AddWithValue("@deleteRelacionDoc", deleteRelacionDoc)
            cmd.Parameters.AddWithValue("@validar_Despacho", validar_Despacho)
            cmd.Parameters.AddWithValue("@Anular", Anular)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub


    Public Function DocumentoGuiaRemisionSelectxParams(ByVal IdSerie As Integer, ByVal codigo As Integer) As Entidades.DocGuiaRemision
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoGuiaRemisionSelectxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        cmd.Parameters.AddWithValue("@doc_Codigo", codigo)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("NO SE HALLARON REGISTROS.")
                End If
                Dim Documento As New Entidades.DocGuiaRemision
                With Documento

                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaIniTraslado = CDate(IIf(IsDBNull(lector.Item("doc_FechaIniTraslado")) = True, Nothing, lector.Item("doc_FechaIniTraslado")))
                    .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    .IdTransportista = CInt(IIf(IsDBNull(lector.Item("IdTransportista")) = True, 0, lector.Item("IdTransportista")))
                    .IdRemitente = CInt(IIf(IsDBNull(lector.Item("IdRemitente")) = True, 0, lector.Item("IdRemitente")))
                    .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, 0, lector.Item("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdChofer = CInt(IIf(IsDBNull(lector.Item("IdChofer")) = True, 0, lector.Item("IdChofer")))
                    .IdMotivoT = CInt(IIf(IsDBNull(lector.Item("IdMotivoT")) = True, 0, lector.Item("IdMotivoT")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .IdVehiculo = CInt(IIf(IsDBNull(lector.Item("IdVehiculo")) = True, 0, lector.Item("IdVehiculo")))
                    .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("NomAlmacen")) = True, "", lector.Item("NomAlmacen")))
                    .PoseeOrdenDespacho = CBool(IIf(IsDBNull(lector.Item("PoseeOrdenDespacho")) = True, 0, lector.Item("PoseeOrdenDespacho")))


                    .IdDocumentoRef = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                    .TipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("TipoDocumento_DocRef")) = True, "", lector.Item("TipoDocumento_DocRef")))
                    .SerieDocumentoRef = CStr(IIf(IsDBNull(lector.Item("NroSerie_DocRef")) = True, "", lector.Item("NroSerie_DocRef")))
                    .CodigoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("NroCodigo_DocRef")) = True, "", lector.Item("NroCodigo_DocRef")))



                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Public Sub DocumentoInsertaDatatableTonos(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idDocumento As Integer, _
    '                                          ByVal dt_tonos As DataTable)
    '    Using cmd As New SqlCommand("SP_INSERT_DETALLE_TONO", cn, tr)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int, ParameterDirection.Input)).Value = idDocumento
    '            .Parameters.Add(New SqlParameter("@dt_tonos", SqlDbType.Structured, ParameterDirection.Input)).Value = dt_tonos
    '        End With
    '        cmd.ExecuteNonQuery()
    '    End Using
    'End Sub

    Public Function DocumentoGuiaRemision_DetalleInsert(ByVal cn As SqlConnection, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal tr As SqlTransaction, _
                                                   ByVal IdDocumento As Integer, ByVal Factor As Integer, ByVal MoverStockFisico As Boolean, ByVal ValidarDespacho As Boolean)
        Try
            Dim cmd As New SqlCommand("_DocumentoGuiaRemision_DetalleInsert_pruebas4", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            'for cantidad de detalle
            For i As Integer = 0 To listaDetalleDocumento.Count - 1

                Dim detalledocumento As Entidades.DetalleDocumento = listaDetalleDocumento(i)

                Dim ArrayParametros() As SqlParameter = New SqlParameter(13) {}
                '****************
                Dim lista As New List(Of Entidades.be_tonoXProducto)
                lista = listaDetalleDocumento(i).listaTonos
                Dim dt As New DataTable
                dt.Columns.Add("id")
                dt.Columns.Add("cantidad")
                If lista IsNot Nothing Then
                    For h As Integer = 0 To lista.Count - 1
                        Dim dr As DataRow = dt.NewRow
                        dr("id") = lista.Item(h).idTono
                        dr("cantidad") = lista.Item(h).cantidadTono
                        dt.Rows.Add(dr)
                    Next
                End If
                '*******************


                ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
                ArrayParametros(0).Direction = ParameterDirection.Output
                ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
                ArrayParametros(1).Value = IdDocumento
                ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
                ArrayParametros(2).Value = detalledocumento.IdProducto
                ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
                ArrayParametros(3).Value = detalledocumento.IdUnidadMedida
                ArrayParametros(4) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
                ArrayParametros(4).Value = IIf(detalledocumento.Cantidad = Nothing, DBNull.Value, detalledocumento.Cantidad)
                ArrayParametros(5) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
                ArrayParametros(5).Value = IIf(detalledocumento.UMedida = Nothing, DBNull.Value, detalledocumento.UMedida)
                ArrayParametros(6) = New SqlParameter("@IdDetalleAfecto", SqlDbType.Int)
                ArrayParametros(6).Value = IIf(detalledocumento.IdDetalleAfecto = Nothing, DBNull.Value, detalledocumento.IdDetalleAfecto)
                ArrayParametros(7) = New SqlParameter("@dc_DetalleGlosa", SqlDbType.VarChar)
                ArrayParametros(7).Value = IIf(detalledocumento.DetalleGlosa = Nothing, DBNull.Value, detalledocumento.DetalleGlosa)
                ArrayParametros(8) = New SqlParameter("@Factor", SqlDbType.Int)
                ArrayParametros(8).Value = Factor
                ArrayParametros(9) = New SqlParameter("@MoverStockFisico", SqlDbType.Bit)
                ArrayParametros(9).Value = MoverStockFisico
                ArrayParametros(10) = New SqlParameter("@ValidarDespacho", SqlDbType.Bit)
                ArrayParametros(10).Value = ValidarDespacho
                ArrayParametros(11) = New SqlParameter("@idTono", SqlDbType.Int)
                ArrayParametros(11).Value = detalledocumento.idTono
                ArrayParametros(12) = New SqlParameter("@idAlmacenActualizaTono", SqlDbType.Int)
                ArrayParametros(12).Value = detalledocumento.IdAlmacen
                ArrayParametros(13) = New SqlParameter("@dt_tonos", SqlDbType.Structured)
                ArrayParametros(13).Value = dt

                cmd.Parameters.Clear()

                cmd.Parameters.AddRange(ArrayParametros)
                'For j As Integer = 0 To dt.Rows.Count - 1
                cmd.ExecuteNonQuery()
                'Next



            Next
            Return CInt(cmd.Parameters("@IdDetalleDocumento").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    Public Function DocumentoGuiaRemisionSelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoGuiaRemisionSelectDetalle", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .IdDocumentoRef = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                        .CantidadxAtenderText = CStr(IIf(IsDBNull(lector.Item("CantidadxAtender_Text")) = True, "", lector.Item("CantidadxAtender_Text")))

                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, False, lector.Item("prod_Kit")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function DocumentoSolicitudPagpSelectDocumentoRef(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSolicitudPagoSelectDocumentoRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Documento)

                While (lector.Read)

                    Dim Documento As New Entidades.Documento
                    With Documento
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        '.Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("NomMoneda")) = True, "", lector.Item("NomMoneda")))
                    End With

                    lista.Add(Documento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function DocumentoGuiaRemisionSelectDocumentoRef(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoGuiaRemisionSelectDocumentoRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Documento)

                While (lector.Read)

                    Dim Documento As New Entidades.Documento
                    With Documento
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .NroDocumento = .Serie + " - " + .Codigo
                    End With

                    lista.Add(Documento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

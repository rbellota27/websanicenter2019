﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports System.Web
Public Class DAOOrdenTrabajo
    Dim ObjConexion As New Conexion
#Region "variables"
    Private objCn As DAO.Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader
    Private trans As SqlTransaction
    Private objDaoDocumento As DAO.DAODocumento
    Private objDaoDetalleDoc As DAO.DAODetalleDocumento
    Private objObservaciones As DAO.DAOObservacion
    Private objCuentaBancaria As DAO.DAOCuentaBancaria
#End Region

#Region "procesos"

    Public Function insertarVehiculo(ByVal obj As Entidades.VehiculoOt, ByVal cn As SqlConnection, _
                                     ByVal tr As SqlTransaction) As Integer
        ' Dim HDAO As New DAO.HelperDAO
        Dim parametros() As SqlParameter = New SqlParameter(6) {}
        parametros(0) = New SqlParameter("@idmodelo", SqlDbType.Int)
        parametros(0).Value = IIf(obj.idmodelo = Nothing, DBNull.Value, obj.idmodelo)
        parametros(1) = New SqlParameter("@placa", SqlDbType.NChar)
        parametros(1).Value = IIf(obj.placa = Nothing, DBNull.Value, obj.placa)
        parametros(2) = New SqlParameter("@ano", SqlDbType.NChar)
        parametros(2).Value = IIf(obj.anoFabric = Nothing, DBNull.Value, obj.anoFabric)
        parametros(3) = New SqlParameter("@nroChasis", SqlDbType.NChar)
        parametros(3).Value = IIf(obj.nrochasis = Nothing, DBNull.Value, obj.nrochasis)
        parametros(4) = New SqlParameter("@tipoMotor", SqlDbType.NChar)
        parametros(4).Value = IIf(obj.tipomotor = Nothing, DBNull.Value, obj.tipomotor)
        parametros(5) = New SqlParameter("@tipoCombustible", SqlDbType.NChar)
        parametros(5).Value = IIf(obj.tipocombustible = Nothing, DBNull.Value, obj.tipocombustible)
        parametros(6) = New SqlParameter("@nroCilindros", SqlDbType.NChar)
        parametros(6).Value = IIf(obj.nrocilindros = Nothing, DBNull.Value, obj.nrocilindros)
        'parametros(7) = New SqlParameter("@id", SqlDbType.Int)
        'parametros(7).Direction = ParameterDirection.Output

        cmd = New SqlCommand("_inserta_vehiculoOT", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            Return CInt(cmd.ExecuteScalar())
        Catch ex As Exception
        Finally

        End Try

        'Return HDAO.InsertaTParameterOutPut(cn, "_inserta_vehiculoOT", parametros, tr)
    End Function

    Public Function insertarProductoOT(ByVal obj As Entidades.VehiculoOt, ByVal cn As SqlConnection, _
                                       ByVal tr As SqlTransaction) As Integer
        Dim HDAO As New DAO.HelperDAO
        Dim parametros() As SqlParameter = New SqlParameter(6) {}
        parametros(0) = New SqlParameter("@iddocumento", SqlDbType.Int)
        parametros(0).Value = IIf(obj.iddocumento = Nothing, DBNull.Value, obj.iddocumento)
        parametros(1) = New SqlParameter("@idproducto", SqlDbType.Int)
        parametros(1).Value = IIf(obj.idproducto = Nothing, DBNull.Value, obj.idproducto)
        parametros(2) = New SqlParameter("@cantidad", SqlDbType.Decimal)
        parametros(2).Value = IIf(obj.cantidad = Nothing, DBNull.Value, obj.cantidad)
        parametros(3) = New SqlParameter("@estado", SqlDbType.Bit)
        parametros(3).Value = IIf(obj.estado = Nothing, DBNull.Value, obj.estado)
        parametros(4) = New SqlParameter("@servicio", SqlDbType.Bit)
        parametros(4).Value = IIf(obj.esservicio = Nothing, DBNull.Value, obj.esservicio)
        parametros(5) = New SqlParameter("@id", SqlDbType.Int)
        parametros(5).Direction = ParameterDirection.Output
        parametros(6) = New SqlParameter("@serie", SqlDbType.NVarChar)
        parametros(6).Value = IIf(obj.serie = Nothing, DBNull.Value, obj.serie)

        Return HDAO.InsertaTParameterOutPut(cn, "_insertar_ProductoOT", parametros, tr)
    End Function

    Private Function insertar_FinanciamientoOT(ByVal obj As Entidades.FinanciamientoOT, ByVal cn As SqlConnection, _
                                               ByVal t As SqlTransaction) As Integer
        Dim HDAO As New DAO.HelperDAO
        Dim parametros() As SqlParameter = New SqlParameter(8) {}
        parametros(0) = New SqlParameter("@idpersona", SqlDbType.Int)
        parametros(0).Value = IIf(obj.idPersona = Nothing, DBNull.Value, obj.idPersona)
        parametros(1) = New SqlParameter("@iddocumento", SqlDbType.Int)
        parametros(1).Value = IIf(obj.iddocumento = Nothing, DBNull.Value, obj.iddocumento)
        parametros(2) = New SqlParameter("@idbanco", SqlDbType.Int)
        parametros(2).Value = IIf(obj.idbanco = Nothing, DBNull.Value, obj.idbanco)
        parametros(3) = New SqlParameter("@Monto", SqlDbType.Decimal)
        parametros(3).Value = IIf(obj.monto = Nothing, DBNull.Value, obj.monto)
        parametros(4) = New SqlParameter("@fechadeposito", SqlDbType.DateTime)
        parametros(4).Value = IIf(obj.fechadeposito = Nothing, DBNull.Value, obj.fechadeposito)
        parametros(5) = New SqlParameter("@numero", SqlDbType.VarChar)
        parametros(5).Value = IIf(obj.numero = Nothing, DBNull.Value, obj.numero)
        parametros(6) = New SqlParameter("@id", SqlDbType.Int)
        parametros(6).Direction = ParameterDirection.Output
        parametros(7) = New SqlParameter("@idvehiculo", SqlDbType.Int)
        parametros(7).Value = IIf(obj.idvehiculo = Nothing, DBNull.Value, obj.idvehiculo)
        parametros(8) = New SqlParameter("@kilometraje", SqlDbType.Decimal)
        parametros(8).Value = IIf(obj.kilometraje = Nothing, DBNull.Value, obj.kilometraje)

        Return HDAO.InsertaTParameterOutPut(cn, "_insertar_FinanciamientoOT", parametros, t)
    End Function

#End Region

#Region "Buscar"

    Public Function listarPersonaNaturalJuridicaX(ByVal idvehiculo As Integer, ByVal nombre As String, ByVal dni As String, ByVal ruc As String, _
                                                 ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.PersonaView)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarPersonaNaturalJuridicaX", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idvehiculo", idvehiculo)
        cmd.Parameters.AddWithValue("@nombre", nombre)
        cmd.Parameters.AddWithValue("@dni", dni)
        cmd.Parameters.AddWithValue("@ruc", ruc)
        cmd.Parameters.AddWithValue("@pageindex", pageindex)
        cmd.Parameters.AddWithValue("@pagesize", pagesize)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .Nombre = CStr(lector.Item("Nombres"))
                        .Dni = CStr(lector.Item("DNI"))
                        .Ruc = CStr(lector.Item("RUC"))
                        .Direccion = CStr(lector.Item("dir_Direccion"))
                        .Telefeono = CStr(lector.Item("Telefono"))
                        Lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarPersonaNaturalJuridica(ByVal nombre As String, ByVal dni As String, ByVal ruc As String, _
                                                 ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.PersonaView)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarPersonaNaturalJuridica", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@nombre", nombre)
        cmd.Parameters.AddWithValue("@dni", dni)
        cmd.Parameters.AddWithValue("@ruc", ruc)
        cmd.Parameters.AddWithValue("@pageindex", pageindex)
        cmd.Parameters.AddWithValue("@pagesize", pagesize)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .Nombre = CStr(lector.Item("Nombres"))
                        .Dni = CStr(lector.Item("DNI"))
                        .Ruc = CStr(lector.Item("RUC"))
                        .Direccion = CStr(lector.Item("dir_Direccion"))
                        .Telefeono = CStr(lector.Item("Telefono"))
                        Lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarVehiculoxPlaca(ByVal placa As String) As Entidades.VehiculoOt
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarVehiculoxPlaca", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@placa", placa)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New Entidades.VehiculoOt
                Do While lector.Read
                    With Lista
                        .id = CInt(IIf(IsDBNull(lector.Item("id")), 0, lector.Item("id")))
                        .linea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                        .marca = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")), "", lector.Item("mar_Nombre")))
                        .modelo = CStr(IIf(IsDBNull(lector.Item("mod_Nombre")), "", lector.Item("mod_Nombre")))
                        .placa = CStr(IIf(IsDBNull(lector.Item("placa")), "", lector.Item("placa")))
                        .anoFabric = CStr(IIf(IsDBNull(lector.Item("ano")), "", lector.Item("ano")))
                        .nrochasis = CStr(IIf(IsDBNull(lector.Item("nrochasis")), "", lector.Item("nrochasis")))
                        .tipomotor = CStr(IIf(IsDBNull(lector.Item("tipomotor")), "", lector.Item("tipomotor")))
                        .tipocombustible = CStr(IIf(IsDBNull(lector.Item("tipoCombustible")), "", lector.Item("tipoCombustible")))
                        .nrocilindros = CStr(IIf(IsDBNull(lector.Item("nroCilindros")), "", lector.Item("nroCilindros")))
                    End With
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

#End Region

#Region "Insertar"

    Public Function inserta_vehiculoOT(ByVal objvehiculo As Entidades.VehiculoOt) As Integer
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        Dim idvehiculo As Integer = -1
        Try
            Using cn
                cn.Open()
                trans = cn.BeginTransaction(IsolationLevel.Serializable)
                idvehiculo = insertarVehiculo(objvehiculo, cn, trans)
                trans.Commit()
                Return idvehiculo
            End Using
        Catch ex As Exception
            trans.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function insertarOrdenTrabajo(ByVal objDocumento As Entidades.Documento, _
                                         ByVal objProductoOt As List(Of Entidades.VehiculoOt), _
                                         ByVal obs As Entidades.Observacion, ByVal objFinanciamientoOt As Entidades.FinanciamientoOT) As Integer
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        Dim iddocumento As Integer = -1
        Try
            Using cn
                cn.Open()
                trans = cn.BeginTransaction(IsolationLevel.Serializable)
                objDaoDocumento = New DAO.DAODocumento
                iddocumento = objDaoDocumento.InsertaDocumento(cn, objDocumento, trans)
                objDaoDetalleDoc = New DAO.DAODetalleDocumento
                For i As Integer = 0 To objProductoOt.Count - 1
                    With objProductoOt.Item(i)
                        .iddocumento = iddocumento
                        Dim iddetalle As Integer = insertarProductoOT(objProductoOt(i), cn, trans)
                    End With
                Next
                objObservaciones = New DAOObservacion
                obs.IdDocumento = iddocumento
                objObservaciones.InsertaObservacionT(cn, trans, obs)
                objFinanciamientoOt.iddocumento = iddocumento
                Dim idFinanciamiento As Integer = insertar_FinanciamientoOT(objFinanciamientoOt, cn, trans)
                trans.Commit()
            End Using
            Return iddocumento
        Catch ex As Exception
            trans.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function insertarDetallesOrdenTrabajo(ByVal listProdusctos As List(Of Entidades.DetalleDocumento), _
                                                 ByVal OBJDocumento As Entidades.Documento, ByVal moverAlmacen As Boolean, _
                                                 ByVal comprometerStock As Boolean, ByVal operativo As Integer) As Boolean
        Try
            objCn = New DAO.Conexion
            cn = objCn.ConexionSIGE
            Using cn
                cn.Open()
                trans = cn.BeginTransaction(IsolationLevel.Serializable)
                Dim ObjDaoDetalleDocumento As New DAO.DAODetalleDocumento

                Select Case operativo
                    Case 1
                        If ActualizarDocumentoAlmacen(cn, trans, OBJDocumento.Id, OBJDocumento.IdAlmacen) = 0 Then
                            trans.Rollback()
                        End If
                    Case 2
                        objDaoDocumento = New DAO.DAODocumento
                        objDaoDocumento.MovAlmacen_ExtornoxIdDocumento(OBJDocumento.Id, cn, trans)
                End Select
                ObjDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listProdusctos, trans, OBJDocumento.Id _
                                                                      , moverAlmacen, comprometerStock, OBJDocumento.FactorMov, OBJDocumento.IdAlmacen)
                trans.Commit()
                Return True
            End Using
        Catch ex As Exception
            trans.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

#End Region

#Region "Actualizar"

    Public Function ActualizarDocumento(ByVal objProductoOt As List(Of Entidades.VehiculoOt), _
                                        ByVal objObs As Entidades.Observacion, _
                                        ByVal objFinanciamientoOt As Entidades.FinanciamientoOT) As Boolean
        Dim iddocumento As Integer = -1
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE

        Try
            Using cn
                cn.Open()
                trans = cn.BeginTransaction(IsolationLevel.Serializable)
                iddocumento = Actualizar_FinanciamientoOT(objFinanciamientoOt, cn, trans)
                Actualizar_ProductoOT(iddocumento, cn, trans)
                For i As Integer = 0 To objProductoOt.Count - 1
                    With objProductoOt.Item(i)
                        .iddocumento = iddocumento
                        Dim iddetalle As Integer = insertarProductoOT(objProductoOt(i), cn, trans)
                    End With
                Next
                objObservaciones = New DAO.DAOObservacion
                objObs.IdDocumento = iddocumento
                objObservaciones.ActualizaObservacion(objObs)

                trans.Commit()
                Return True
            End Using
        Catch ex As Exception
            trans.Rollback()
            Return False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function Actualizar_FinanciamientoOT(ByVal obj As Entidades.FinanciamientoOT, ByVal cn As SqlConnection, _
                                               ByVal tr As SqlTransaction) As Integer
        Dim HDAO As New DAO.HelperDAO
        Dim parametros() As SqlParameter = New SqlParameter(5) {}

        parametros(0) = New SqlParameter("@idfinanciamiento", SqlDbType.Int)
        parametros(0).Value = IIf(obj.id = Nothing, DBNull.Value, obj.id)
        parametros(1) = New SqlParameter("@idbanco", SqlDbType.Int)
        parametros(1).Value = IIf(obj.idbanco = Nothing, DBNull.Value, obj.idbanco)
        parametros(2) = New SqlParameter("@Monto", SqlDbType.Decimal)
        parametros(2).Value = IIf(obj.monto = Nothing, DBNull.Value, obj.monto)
        parametros(3) = New SqlParameter("@fechadeposito", SqlDbType.DateTime)
        parametros(3).Value = IIf(obj.fechadeposito = Nothing, DBNull.Value, obj.fechadeposito)
        parametros(4) = New SqlParameter("@numero", SqlDbType.VarChar)
        parametros(4).Value = IIf(obj.numero = Nothing, DBNull.Value, obj.numero)
        parametros(5) = New SqlParameter("@iddocumento", SqlDbType.Int)
        parametros(5).Direction = ParameterDirection.Output
        cmd = New SqlCommand("_Actualizar_FinanciamientoOT", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try

            Return CInt(cmd.ExecuteScalar())
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ActualizarDocumentoAlmacen(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal iddocumento As Integer, ByVal idalmacen As Integer) As Integer
        cmd = New SqlCommand("_ActualizarDocumentoAlmacen", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@iddocuemento", iddocumento)
        cmd.Parameters.AddWithValue("@idalmacen", idalmacen)
        Try
            Using cmd
                Return CInt(cmd.ExecuteScalar())
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function Actualizar_ProductoOT(ByVal iddocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        cmd = New SqlCommand("_Actualizar_ProductoOT", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@iddocumento", iddocumento)
        Try
            Using cmd
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                    Throw New Exception
                End If
                Return True
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

#End Region

#Region "Anular"

#End Region

#Region "Listar"

    Public Function listarProductosLiquidacion(ByVal tipoPV As Integer, ByVal idtienda As Integer, ByVal idalmacen As Integer, _
                                               ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal descripcion As String, _
                                               ByVal codsublinea As Integer, ByVal pageIndex As Integer, _
                                               ByVal pageSize As Integer) As List(Of Entidades.Catalogo)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarProductoLiquidacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@tipoPV", tipoPV)
        cmd.Parameters.AddWithValue("@idalmacen", idalmacen)
        cmd.Parameters.AddWithValue("@idtienda", idtienda)
        cmd.Parameters.AddWithValue("@linea", idlinea)
        cmd.Parameters.AddWithValue("@sublinea", idsublinea)
        cmd.Parameters.AddWithValue("@descripcion", descripcion)
        cmd.Parameters.AddWithValue("@codsublinea", codsublinea)
        cmd.Parameters.AddWithValue("@pageindex", pageIndex)
        cmd.Parameters.AddWithValue("@pageSize", pageSize)
        Dim lector As SqlDataReader

        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo
                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")), 0, lector.Item("IdProducto")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")), "", lector.Item("sl_Nombre")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")), 0, lector.Item("IdUnidadMedida")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")), "", lector.Item("um_NombreCorto")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")), 0.0, lector.Item("ppv_Valor")))
                        .StockComprometido = CDec(IIf(IsDBNull(lector.Item("StockComprometido")), 0.0, lector.Item("StockComprometido")))
                        .StockAReal = CDec(IIf(IsDBNull(lector.Item("Stock")), 0.0, lector.Item("Stock")))

                    End With
                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarOrdenTrabajoL(ByVal idtienda As Integer, ByVal pageindex As Integer, _
                                        ByVal pagesize As Integer) As List(Of Entidades.OrdenTrabajoView)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarOrdenTrabajoL", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@tienda", idtienda)
        cmd.Parameters.AddWithValue("@pageindex", pageindex)
        cmd.Parameters.AddWithValue("@pageSize", pagesize)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.OrdenTrabajoView)
                Do While lector.Read
                    Dim obj As New Entidades.OrdenTrabajoView
                    With obj
                        .iddocumento = CInt(IIf(IsDBNull(lector.Item("iddocumento")), 0, lector.Item("iddocumento")))
                        .idfinanciamientoOT = CInt(IIf(IsDBNull(lector.Item("id")), 0, lector.Item("id")))
                        .numeroDoc = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")), "", lector.Item("doc_Codigo")))
                        .serie = CStr(IIf(IsDBNull(lector.Item("doc_serie")), "", lector.Item("doc_serie")))
                        .fechaDoc = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")), "", lector.Item("doc_FechaEmision")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")), 0, lector.Item("IdEstadoDoc")))
                        .idestadoCan = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")), 0, lector.Item("IdEstadoCan")))
                        .nombres = CStr(IIf(IsDBNull(lector.Item("nombres")), 0, lector.Item("nombres")))
                        .dni = CStr(IIf(IsDBNull(lector.Item("dni")), "", lector.Item("dni")))
                        .ruc = CStr(IIf(IsDBNull(lector.Item("ruc")), "", lector.Item("ruc")))
                        .placa = CStr(IIf(IsDBNull(lector.Item("placa")), "", lector.Item("placa")))
                        .modelo = CStr(IIf(IsDBNull(lector.Item("mod_Nombre")), "", lector.Item("mod_Nombre")))
                        .trabajoarealizar = CInt(IIf(IsDBNull(lector.Item("IdLinea")), 0, lector.Item("IdLinea")))
                        .tiposerviciostr = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")), "", lector.Item("sl_Nombre")))
                        .servicio = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), "", lector.Item("prod_Nombre")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")), 0, lector("IdAlmacen")))
                    End With
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarProductoOTL(ByVal iddocumento As Integer, ByVal idtienda As Integer, ByVal tipoPV As Integer) As List(Of Entidades.DetalleDocumentoView)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarProductoOTL", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@iddocumento", iddocumento)
        cmd.Parameters.AddWithValue("@idtienda", idtienda)
        cmd.Parameters.AddWithValue("@tipoPV", tipoPV)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumentoView
                    With obj
                        .idAuxiliar = CInt(IIf(IsDBNull(lector.Item("idproductoOT")), 0, lector.Item("idproductoOT")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("idProducto")), 0, lector.Item("idProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), "", lector.Item("prod_Nombre")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("cantidad")), 0.0, lector.Item("cantidad")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")), 0.0, lector.Item("ppv_Valor")))
                        .NomUMPrincipal = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")), "", lector.Item("um_NombreCorto")))
                        .StockDisponible = CDec(IIf(IsDBNull(lector.Item("Stock")), 0, lector.Item("Stock")))
                        .IdUMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")), 0, lector.Item("IdUnidadMedida")))
                        .IdTienda_PV = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                        Lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarProductoOT(ByVal idfinanciamiento As Integer) As List(Of Entidades.DetalleDocumentoView)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarProductoOT", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idfinanciamiento", idfinanciamiento)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumentoView
                    With obj
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("idProducto")), 0, lector.Item("idProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), "", lector.Item("prod_Nombre")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("cantidad")), 0.0, lector.Item("cantidad")))
                        .serie = CStr(IIf(IsDBNull(lector.Item("serie")), "", lector.Item("serie")))
                        Lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarOrdenTrabajo(ByVal idfinanciamiento As Integer) As Entidades.OrdenTrabajoView
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarOrdentrabajoBusqueda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idfinanciamiento", idfinanciamiento)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.OrdenTrabajoView
                Do While lector.Read

                    With obj
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")), 0, lector.Item("IdTipoPV")))
                        .fechadeposito = CStr(IIf(IsDBNull(lector.Item("fechadeposito")), "", lector.Item("fechadeposito")))
                        .monto = CDec(IIf(IsDBNull(lector.Item("monto")), 0, lector.Item("monto")))
                        .numero = CStr(IIf(IsDBNull(lector.Item("numero")), "", lector.Item("numero")))
                        .direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")), "", lector.Item("dir_Direccion")))
                        .telefono = CStr(IIf(IsDBNull(lector.Item("Telefono")), "", lector.Item("Telefono")))
                        .idformapago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")), 0, lector.Item("IdCondicionPago")))
                        .idfinanciera = CInt(IIf(IsDBNull(lector.Item("idbanco")), 0, lector.Item("idbanco")))
                        .idmoneda = CInt(IIf(IsDBNull(lector.Item("Idmoneda")), 0, lector.Item("Idmoneda")))
                        .clase = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                        .marca = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")), "", lector.Item("mar_Nombre")))
                        .tipomotor = CStr(IIf(IsDBNull(lector.Item("tipoMotor")), "", lector.Item("tipoMotor")))
                        .tipocombustible = CStr(IIf(IsDBNull(lector.Item("tipoCombustible")), "", lector.Item("tipoCombustible")))
                        .nrochasis = CStr(IIf(IsDBNull(lector.Item("nrochasis")), "", lector.Item("nrochasis")))
                        .anoFabric = CStr(IIf(IsDBNull(lector.Item("ano")), "", lector.Item("ano")))
                        .observaciones = CStr(IIf(IsDBNull(lector.Item("ob_Observacion")), "", lector.Item("ob_Observacion")))
                        .nrocilindros = CStr(IIf(IsDBNull(lector.Item("nroCilindros")), "", lector.Item("nroCilindros")))
                        .PoseeOrdenDespacho = CInt(IIf(IsDBNull(lector.Item("PoseeOrdenDespacho")), 0, lector.Item("PoseeOrdenDespacho")))
                    End With
                Loop
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarOrdenTrabajo(ByVal nomcli As String, ByVal doccli As String, ByVal placa _
                                       As String, ByVal nrodoc As String, ByVal idtienda As Integer, ByVal pageindex As Integer, _
                                       ByVal pagesize As Integer) As List(Of Entidades.OrdenTrabajoView)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarOrdenTrabajo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@nomcli", nomcli)
        cmd.Parameters.AddWithValue("@doccli", doccli)
        cmd.Parameters.AddWithValue("@placa", placa)
        cmd.Parameters.AddWithValue("@nrodoc", nrodoc)
        cmd.Parameters.AddWithValue("@pageindex", pageindex)
        cmd.Parameters.AddWithValue("@pageSize", pagesize)
        cmd.Parameters.AddWithValue("@tienda", idtienda)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.OrdenTrabajoView)
                Do While lector.Read
                    Dim obj As New Entidades.OrdenTrabajoView
                    With obj
                        .iddocumento = CInt(IIf(IsDBNull(lector.Item("iddocumento")), 0, lector.Item("iddocumento")))
                        .idfinanciamientoOT = CInt(IIf(IsDBNull(lector.Item("id")), 0, lector.Item("id")))
                        .numeroDoc = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")), "", lector.Item("doc_Codigo")))
                        .serie = CStr(IIf(IsDBNull(lector.Item("doc_serie")), "", lector.Item("doc_serie")))
                        .fechaDoc = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")), "", lector.Item("doc_FechaEmision")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")), 0, lector.Item("IdEstadoDoc")))
                        .idestadoCan = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")), 0, lector.Item("IdEstadoCan")))
                        .nombres = CStr(IIf(IsDBNull(lector.Item("nombres")), 0, lector.Item("nombres")))
                        .dni = CStr(IIf(IsDBNull(lector.Item("dni")), "", lector.Item("dni")))
                        .ruc = CStr(IIf(IsDBNull(lector.Item("ruc")), "", lector.Item("ruc")))
                        .placa = CStr(IIf(IsDBNull(lector.Item("placa")), "", lector.Item("placa")))
                        .modelo = CStr(IIf(IsDBNull(lector.Item("mod_Nombre")), "", lector.Item("mod_Nombre")))
                        .trabajoarealizar = CInt(IIf(IsDBNull(lector.Item("IdLinea")), 0, lector.Item("IdLinea")))
                        .tiposerviciostr = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")), "", lector.Item("sl_Nombre")))
                        .servicio = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), "", lector.Item("prod_Nombre")))
                    End With
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarDetalleServicio(ByVal idproducto As Integer, ByVal idtienda As Integer) As List(Of Entidades.Catalogo)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarDetalleServicio", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idproducto", idproducto)
        cmd.Parameters.AddWithValue("@idtienda", idtienda)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim obj As New Entidades.Catalogo
                    With obj
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")), 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), 0, lector.Item("prod_Nombre")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")), "", lector.Item("sl_Nombre")))
                        .StockAReal = CDec(IIf(IsDBNull(lector.Item("Stock")), 0, lector.Item("Stock")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function llenarLineaxTipoExistencia(ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarLineaxTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@id", idexistencia)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim obj As New Entidades.Linea
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")), 0, lector.Item("IdLinea")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarLineaMarca(ByVal idlinea As Integer, ByVal mante As Integer) As List(Of Entidades.Marca)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarLineaMarca", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idlinea", idlinea)
        cmd.Parameters.AddWithValue("@mante", mante)

        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Marca)
                Do While lector.Read
                    Dim obj As New Entidades.Marca
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("ID")), 0, lector.Item("ID")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")), "", lector.Item("mar_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarModeloMarcaLinea(ByVal idlineaMarca As Integer) As List(Of Entidades.ModeloLineaMarca)
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        cmd = New SqlCommand("_listarModeloMarcaLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idlineaMarca", idlineaMarca)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ModeloLineaMarca)
                Do While lector.Read
                    Dim obj As New Entidades.ModeloLineaMarca
                    obj.idmodelo = CInt(IIf(IsDBNull(lector.Item("idmodelo")), 0, lector.Item("idmodelo")))
                    obj.mod_nombre = CStr(IIf(IsDBNull(lector.Item("mod_Nombre")), "", lector.Item("mod_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function AnularOrdenTrabajo(ByVal idDocumento As Integer) As Boolean
        objCn = New DAO.Conexion
        cn = objCn.ConexionSIGE
        Dim tr As SqlTransaction

        cn.Open()
        tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Using cn
                objDaoDocumento = New DAO.DAODocumento
                objDaoDocumento.AnulaT(cn, tr, idDocumento)
                tr.Commit()
            End Using
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

#End Region


    'Public Shared Sub Fill(Of T)(ByVal reader As IDataReader, ByVal list As IList(Of T), ByVal type As Type, ByVal fields As String())
    '    Dim index As Integer = 0
    '    While reader.Read()
    '        Dim item As T = DirectCast(Activator.CreateInstance(type), T)
    '        Dim properties As System.Reflection.ParameterInfo() = DirectCast(item.[GetType](), Type).GetProperties(Reflection.BindingFlags.CreateInstance)

    '        For j As Integer = 0 To fields.Length - 1
    '            index = FindProperyIndexByColumnName(fields(j), properties)
    '            properties(index).SetValue(item, reader(fields(j)), Nothing)
    '        Next
    '        list.Add(item)
    '    End While
    'End Sub

    'Private Shared Function FindProperyIndexByColumnName(ByVal columnName As String, ByVal prop As PropertyInfo()) As Integer
    '    Dim index As Integer = -1
    '    For i As Integer = 0 To prop.Length - 1
    '        If prop(i).Name.Equals(columnName) Then
    '            index = i
    '            Exit For
    '        End If
    '    Next
    '    If index = -1 Then
    '        Throw New ArgumentOutOfRangeException("Column was not found")
    '    End If
    '    Return index
    'End Function

End Class




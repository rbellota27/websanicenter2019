﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM






Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocumentoExterno

    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader
    Private tr As SqlTransaction = Nothing
    Private objConexion As New DAO.Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub DocumentoExterno_DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteRelacionDocumento As Boolean, ByVal deleteAnexoDocumento As Boolean, ByVal deleteDetalleConcepto As Boolean, ByVal deleteObservaciones As Boolean, ByVal deleteMovCuentaCXP As Boolean, ByVal deleteMontoRegimen As Boolean, ByVal anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(7) {}

        p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(deleteAnexoDocumento, "@deleteAnexoDocumento", SqlDbType.Bit)
        p(2) = objDaoMantenedor.getParam(deleteDetalleConcepto, "@deleteDetalleConcepto", SqlDbType.Bit)
        p(3) = objDaoMantenedor.getParam(deleteMovCuentaCXP, "@deleteMovCuentaCXP", SqlDbType.Bit)
        p(4) = objDaoMantenedor.getParam(deleteObservaciones, "@deleteObservaciones", SqlDbType.Bit)
        p(5) = objDaoMantenedor.getParam(deleteRelacionDocumento, "@deleteRelacionDocumento", SqlDbType.Bit)
        p(6) = objDaoMantenedor.getParam(deleteMontoRegimen, "@deleteMontoRegimen", SqlDbType.Bit)
        p(7) = objDaoMantenedor.getParam(anular, "@anular", SqlDbType.Bit)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoExterno_DeshacerMov", p)

    End Sub

#Region "Reportes"

    Public Function getDataSet_Cuentas_Por_Pagar_Cargos(ByVal IdEmpresa As Integer, _
    ByVal IdMoneda As Integer, ByVal IdPersona As Integer, ByVal idTienda As Integer, ByVal fecha As String) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("_CR_Cuentas_Por_Pagar_Cargos", objConexion.ConexionSIGE)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTienda", idTienda)
        cmd.Parameters.AddWithValue("@Fecha", fecha)
        Try
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Cuentas_Por_Pagar_Cargos")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

#End Region

#Region "mantenimiento"
    Public Function Documento_Externo_Insert(ByVal obj As Entidades.Documento, _
                                             ByVal lista_detalle As List(Of Entidades.DetalleDocumento), _
                                             ByVal objObservacion As Entidades.Observacion) As Integer
        Dim IdDocumento As Integer = 0
        Dim parametro() As SqlParameter = New SqlParameter(14) {}

        parametro(0) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametro(0).Value = IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision)

        parametro(1) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar)
        parametro(1).Value = IIf(obj.Codigo = Nothing, DBNull.Value, obj.Codigo)

        parametro(2) = New SqlParameter("@doc_Serie", SqlDbType.VarChar)
        parametro(2).Value = IIf(obj.Serie = Nothing, DBNull.Value, obj.Serie)

        parametro(3) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        parametro(3).Value = IIf(obj.TotalAPagar = Nothing, DBNull.Value, obj.TotalAPagar)

        parametro(4) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        parametro(4).Value = IIf(obj.TotalLetras = Nothing, DBNull.Value, obj.TotalLetras)

        parametro(5) = New SqlParameter("@IdPersona", SqlDbType.Int) '** Proveedor
        parametro(5).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)

        parametro(6) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        parametro(6).Value = IIf(obj.IdUsuario = Nothing, DBNull.Value, obj.IdUsuario)

        parametro(7) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametro(7).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)

        parametro(8) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        parametro(8).Value = IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento)

        parametro(9) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametro(9).Value = IIf(obj.IdCondicionPago = Nothing, DBNull.Value, obj.IdCondicionPago)

        parametro(10) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.DateTime)
        parametro(10).Value = IIf(obj.FechaCancelacion = Nothing, DBNull.Value, obj.FechaCancelacion)

        parametro(11) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        parametro(11).Value = IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc)

        parametro(12) = New SqlParameter("@IdDocRelacionado", SqlDbType.Int)
        parametro(12).Value = IIf(obj.IdDocRelacionado = Nothing, DBNull.Value, obj.IdDocRelacionado)

        parametro(13) = New SqlParameter("@idEmpresa", SqlDbType.Int)
        parametro(13).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)

        parametro(14) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametro(14).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)

        cn = objConexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_Documento_Externo_Insert", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametro)
            IdDocumento = CInt(cmd.ExecuteScalar)

            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To lista_detalle.Count - 1
                With lista_detalle.Item(i)
                    .IdDocumento = IdDocumento
                    Dim iddetalle As Integer = objDaoDetalleDocumento.InsertaDetalleDocumento(cn, lista_detalle(i), tr)
                End With
            Next

            Dim objDocRef As New DAO.DAORelacionDocumento
            Dim objEntRelacionDoc As New Entidades.RelacionDocumento
            With objEntRelacionDoc
                .IdDocumento1 = obj.IdDocRelacionado
                .IdDocumento2 = IdDocumento
            End With
            objDocRef.InsertaRelacionDocumento(objEntRelacionDoc, cn, tr)

            Dim objDAOObservacion As New DAO.DAOObservacion
            objObservacion.IdDocumento = IdDocumento
            objDAOObservacion.InsertaObservacionT(cn, tr, objObservacion)

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return IdDocumento
    End Function

    Public Function Documento_Externo_Update(ByVal obj As Entidades.Documento, _
                                               ByVal lista_detalle As List(Of Entidades.DetalleDocumento), _
                                               ByVal objObservacion As Entidades.Observacion) As Boolean
        Dim parametro() As SqlParameter = New SqlParameter(14) {}

        parametro(0) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametro(0).Value = IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision)

        parametro(1) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar)
        parametro(1).Value = IIf(obj.Codigo = Nothing, DBNull.Value, obj.Codigo)

        parametro(2) = New SqlParameter("@doc_Serie", SqlDbType.VarChar)
        parametro(2).Value = IIf(obj.Serie = Nothing, DBNull.Value, obj.Serie)

        parametro(3) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        parametro(3).Value = IIf(obj.TotalAPagar = Nothing, DBNull.Value, obj.TotalAPagar)

        parametro(4) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        parametro(4).Value = IIf(obj.TotalLetras = Nothing, DBNull.Value, obj.TotalLetras)

        parametro(5) = New SqlParameter("@IdPersona", SqlDbType.Int) '** Proveedor
        parametro(5).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)

        parametro(6) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametro(6).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)

        parametro(7) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        parametro(7).Value = IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento)

        parametro(8) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        parametro(8).Value = IIf(obj.Id = Nothing, DBNull.Value, obj.Id)

        parametro(9) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametro(9).Value = IIf(obj.IdCondicionPago = Nothing, DBNull.Value, obj.IdCondicionPago)

        parametro(10) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.DateTime)
        parametro(10).Value = IIf(obj.FechaCancelacion = Nothing, DBNull.Value, obj.FechaCancelacion)

        parametro(11) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        parametro(11).Value = IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc)

        parametro(12) = New SqlParameter("@IdDocRelacionado", SqlDbType.Int)
        parametro(12).Value = IIf(obj.IdDocRelacionado = Nothing, DBNull.Value, obj.IdDocRelacionado)

        parametro(13) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        parametro(13).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)

        parametro(14) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametro(14).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)

        cn = objConexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_Documento_Externo_Update", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametro)
            cmd.ExecuteNonQuery()

            Dim objDocRef As New DAO.DAORelacionDocumento
            Dim objEntRelacionDoc As New Entidades.RelacionDocumento
            With objEntRelacionDoc
                .IdDocumento1 = obj.IdDocRelacionado
                .IdDocumento2 = obj.Id
            End With
            objDocRef.InsertaRelacionDocumento(objEntRelacionDoc, cn, tr)

            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To lista_detalle.Count - 1
                With lista_detalle.Item(i)
                    .IdDocumento = obj.Id
                    Dim iddetalle As Integer = objDaoDetalleDocumento.InsertaDetalleDocumento(cn, lista_detalle(i), tr)
                End With
            Next

            Dim objObservaciones As New DAO.DAOObservacion
            objObservacion.IdDocumento = obj.Id
            objObservaciones.ActualizaObservacion(objObservacion)

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function

    Public Function DocumentoExterno_Busqueda(ByVal fechas As String, ByVal idpersona As Integer, ByVal idtipodocumento As Integer) As List(Of Entidades.DocumentoView)
        DocumentoExterno_Busqueda = New List(Of Entidades.DocumentoView)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_DocumentoExterno_Busqueda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@fechas", fechas)
        cmd.Parameters.AddWithValue("@idPersona", idpersona)
        cmd.Parameters.AddWithValue("@idTipoDocumento", idtipodocumento)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.DocumentoView
                With obj
                    .IdDocumento = CInt(lector("IdDocumento"))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "---", lector("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "---", lector("doc_Codigo")))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")), "---", lector("doc_FechaEmision")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("Idtipodocumento")), 0, lector("idtipodocumento")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")), "---", lector("tdoc_NombreCorto")))
                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")), 0, lector("IdPersona")))
                    .RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")), "---", lector("jur_Rsocial")))
                    .RUC = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                    .DNI = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                    .IdEstadoDoc = CInt(lector("IdEstadoDoc"))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")), "---", lector("edoc_Nombre")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), "---", lector("doc_TotalAPagar")))
                End With
                DocumentoExterno_Busqueda.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return DocumentoExterno_Busqueda

    End Function

    Public Function DocumentoExterno_SerieNumero(ByVal serie As Integer, ByVal numero As Integer, _
                                                 ByVal idtipodocumento As Integer, ByVal idpersona As Integer) As Entidades.DocumentoView
        DocumentoExterno_SerieNumero = New Entidades.DocumentoView
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_DocumentoExterno_SerieNumero", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@doc_Serie", serie)
        cmd.Parameters.AddWithValue("@doc_Codigo", numero)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", idtipodocumento)
        cmd.Parameters.AddWithValue("@IdPersona", idpersona)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                With DocumentoExterno_SerieNumero
                    .Id = CInt(lector("IdDocumento"))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "---", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "---", lector("doc_Serie")))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")), "", lector("doc_FechaEmision")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")), Nothing, lector("doc_FechaVenc")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), 0, lector("doc_TotalAPagar")))
                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")), 0, lector("IdPersona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")), 0, lector("IdEstadoDoc")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")), 0, lector("IdMoneda")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")), 0, lector("IdTipoDocumento")))
                    .FechaCancelacion = CDate(IIf(IsDBNull(lector("doc_FechaCancelacion")), Nothing, lector("doc_FechaCancelacion")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")), 0, lector("IdCondicionPago")))
                    .RUC = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                    .DNI = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                    .RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")), "---", lector("jur_Rsocial")))
                End With
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return DocumentoExterno_SerieNumero
    End Function

    Public Function DocumentoExterno_RelacionDoc(ByVal idDocumento As Integer) As Entidades.DocumentoView
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_DocumentoExterno_RelacionDoc", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idDocumento", idDocumento)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            DocumentoExterno_RelacionDoc = New Entidades.DocumentoView
            If lector.Read Then
                With DocumentoExterno_RelacionDoc
                    .IdDocRelacionado = CInt(IIf(IsDBNull(lector("IdDocRelacionado")), 0, lector("IdDocRelacionado")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "---", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "---", lector("doc_Serie")))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")), "---", lector("doc_FechaEmision")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")), 0, lector("IdTipoDocumento")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), 0, lector("doc_TotalAPagar")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")), 0, lector("IdTienda")))
                    .NroDocumento = CStr(IIf(IsDBNull(lector("cadDocRelacionado")), "", lector("cadDocRelacionado")))
                End With
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return DocumentoExterno_RelacionDoc
    End Function

#End Region

#Region "Otros"
    Public Function DocumentoExterno_ConsultarGuia(ByVal cadIdDocumento As String, _
                                                    ByVal idProducto As Integer) As List(Of Entidades.DocumentoView)
        DocumentoExterno_ConsultarGuia = New List(Of Entidades.DocumentoView)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_DocumentoExterno_ConsultarGuia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@cadIdDocumento", cadIdDocumento)
        cmd.Parameters.AddWithValue("@idProducto", idProducto)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.DocumentoView
                With obj
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "---", lector("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "---", lector("doc_Codigo")))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")), "---", lector("doc_FechaEmision")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreLargo")), "---", lector("tdoc_NombreLargo")))
                    .NomTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")), "---", lector("tie_Nombre")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector("alm_Nombre")), "---", lector("alm_Nombre")))
                    .Total = CDec(IIf(IsDBNull(lector("dc_Cantidad")), 0, lector("dc_Cantidad")))
                End With
                DocumentoExterno_ConsultarGuia.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return DocumentoExterno_ConsultarGuia
    End Function

    Public Function DocumentoExterno_BuscarReferencia(ByVal tipo As Integer, _
                                         ByVal fechas As String, ByVal doc_Codigo As Integer, _
                                         ByVal doc_Serie As Integer, ByVal idpersona As Integer, _
                                         ByVal idtipodocumento As Integer) As List(Of Entidades.DocumentoView)
        DocumentoExterno_BuscarReferencia = New List(Of Entidades.DocumentoView)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_DocumentoExterno_BuscarReferencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@tipo", tipo)
        cmd.Parameters.AddWithValue("@fechas", fechas)
        cmd.Parameters.AddWithValue("@doc_Codigo", doc_Codigo)
        cmd.Parameters.AddWithValue("@doc_Serie", doc_Serie)
        cmd.Parameters.AddWithValue("@idPersona", idpersona)
        cmd.Parameters.AddWithValue("@idTipoDocumento", idtipodocumento)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.DocumentoView
                With obj
                    .IdDocumento = CInt(lector("IdDocumento"))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "---", lector("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "---", lector("doc_Codigo")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")), 0, lector("IdCondicionPago")))
                    .NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")), "---", lector("cp_Nombre")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("idmoneda")), 0, lector("idmoneda")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")), "---", lector("mon_Simbolo")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), 0, lector("doc_TotalAPagar")))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")), "---", lector("doc_FechaEmision")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("Idtipodocumento")), 0, lector("idtipodocumento")))
                    .IdEmpresa = CInt(lector("IdEmpresa"))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")), 0, lector("IdTienda")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")), "---", lector("tdoc_NombreCorto")))
                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")), 0, lector("IdPersona")))
                    .NroDocumento = CStr(IIf(IsDBNull(lector("cadDocRelacionado")), "", lector("cadDocRelacionado")))
                    .NomTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")), "---", lector("tie_Nombre")))
                End With
                DocumentoExterno_BuscarReferencia.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return DocumentoExterno_BuscarReferencia
    End Function

    Public Function DocumentoExterno_Buscar(ByVal IdDocumento As Integer) As List(Of Entidades.DocumentoView)
        DocumentoExterno_Buscar = New List(Of Entidades.DocumentoView)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_DocumentoExterno_Buscar", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idDocumento", IdDocumento)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.DocumentoView
                With obj
                    .IdDocumento = CInt(lector("IdDocumento"))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "---", lector("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "---", lector("doc_Codigo")))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")), "---", lector("doc_FechaEmision")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")), "---", lector("tdoc_NombreCorto")))
                    '///////////guia recepcion
                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")), "---", lector("IdPersona")))
                    .RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")), "---", lector("jur_Rsocial")))
                    .RUC = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                    '//////////////
                    .NomTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")), "---", lector("tie_Nombre")))
                End With
                DocumentoExterno_Buscar.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return DocumentoExterno_Buscar
    End Function

#End Region
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAODocumentoAjusteInventario
    Private objConexion As New DAO.Conexion
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand
    Private reader As SqlDataReader

    Public Function DocumentoAjusteInventario_SelectDocRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, _
                                    ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal iDTIPOdocumento As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoAjusteInventario_SelectDocRef", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@IdtipoDocumento", iDTIPOdocumento)


        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("FechaEntrega")) = True, Nothing, lector.Item("FechaEntrega")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                        .NroDocumento = .Serie + " - " + .Codigo

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function DocumentoAjusteInventario_FiltroDetalle(ByVal IdDocumento As Integer, ByVal IdDocumentoTomaInv As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                        ByVal CodigoSubLinea As String, ByVal Producto As String, ByVal CodigoProducto As String, ByVal IdtipoExistencia As Integer, _
                                        ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DetalleDocumentoTomaInventario)

        Dim lista As New List(Of Entidades.DetalleDocumentoTomaInventario)

        Try

            Dim p() As SqlParameter = New SqlParameter(9) {}

            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Value = IdDocumento

            p(1) = New SqlParameter("@IdLinea", SqlDbType.Int)
            p(1).Value = IdLinea

            p(2) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
            p(2).Value = IdSubLinea

            p(3) = New SqlParameter("@Producto", SqlDbType.VarChar)
            p(3).Value = Producto

            p(4) = New SqlParameter("@CodigoSubLinea", SqlDbType.VarChar)
            p(4).Value = CodigoSubLinea

            p(5) = New SqlParameter("@CodigoProducto", SqlDbType.VarChar)
            p(5).Value = CodigoProducto

            p(6) = New SqlParameter("@IdDocumentoTomaInv", SqlDbType.Int)
            p(6).Value = IdDocumentoTomaInv

            p(7) = New SqlParameter("@IdtipoExistencia", SqlDbType.Int)
            p(7).Value = IdtipoExistencia

            p(8) = New SqlParameter("@PageIndex", SqlDbType.Int)
            p(8).Value = PageIndex

            p(9) = New SqlParameter("@PageSize", SqlDbType.Int)
            p(9).Value = PageSize

            cn = objConexion.ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoAjusteInventario_FiltroDetalle", p)

            While (reader.Read)

                Dim obj As New Entidades.DetalleDocumentoTomaInventario

                With obj

                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")) = True, 0, reader("IdProducto")))
                    .IdDetalleDocumento = CInt(IIf(IsDBNull(reader("IdDetalleDocumento_TomaInv")) = True, 0, reader("IdDetalleDocumento_TomaInv")))
                    .IdDocumento = CInt(IIf(IsDBNull(reader("IdDocumento_TomaInv")) = True, 0, reader("IdDocumento_TomaInv")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(reader("IdUnidadMedida")) = True, 0, reader("IdUnidadMedida")))


                    .CantidadTomaInv = CDec(IIf(IsDBNull(reader("Cantidad_TomaInv")) = True, 0, reader("Cantidad_TomaInv")))
                    .CantidadSistema = CDec(IIf(IsDBNull(reader("Cantidad_Sistema_TomaInv")) = True, 0, reader("Cantidad_Sistema_TomaInv")))
                    .UMedida = CStr(IIf(IsDBNull(reader("UnidadMedida")) = True, "", reader("UnidadMedida")))
                    .NomProducto = CStr(IIf(IsDBNull(reader("Producto")) = True, "", reader("Producto")))
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("CodigoProducto")) = True, "", reader("CodigoProducto")))

                    .CantidadAjuste = CDec(IIf(IsDBNull(reader("Cantidad_Ajuste")) = True, 0, reader("Cantidad_Ajuste")))
                    .CostoUnit = CDec(IIf(IsDBNull(reader("CostoUnitario")) = True, 0, reader("CostoUnitario")))


                    .Faltante = CDec(IIf(IsDBNull(reader("Faltante")) = True, 0, reader("Faltante")))
                    .Sobrante = CDec(IIf(IsDBNull(reader("Sobrante")) = True, 0, reader("Sobrante")))


                    .CostoFaltante = .Faltante * .CostoUnit
                    .CostoSobrante = .Sobrante * .CostoUnit

                    .DescEstado = CStr(IIf(IsDBNull(reader("Estado_Ajuste_Cadena")) = True, "", reader("Estado_Ajuste_Cadena")))
                    .Ajustado = CStr(IIf(IsDBNull(reader("Estado_Ajuste")) = True, 0, reader("Estado_Ajuste")))

                End With

                lista.Add(obj)

            End While

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista


    End Function
    Public Function EliminarProcesoxIdProductoxIdDocumentoAjusteInv(ByVal IdDocAjusteInv As Integer, ByVal IdProducto As Integer) As Boolean

        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim cmd As New SqlCommand("_DocumentoAjusteInvEliminarProcesoxIdProductoxIdDocumentoAjusteInv", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdDocAjusteInv", IdDocAjusteInv)

            cmd.ExecuteNonQuery()
            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Sub DocumentoAjusteInventario_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteDetalleDocumento As Boolean, ByVal DeleteMovAlmacen As Boolean, ByVal DeleteDocumento_Persona As Boolean, ByVal DeleteRelacionDocumento As Boolean, ByVal DeleteObservaciones As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(6) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IdDocumento

        p(1) = New SqlParameter("@DeleteDetalleDocumento", SqlDbType.Bit)
        p(1).Value = DeleteDetalleDocumento

        p(2) = New SqlParameter("@DeleteRelacionDocumento", SqlDbType.Bit)
        p(2).Value = DeleteRelacionDocumento

        p(3) = New SqlParameter("@DeleteDocumento_Persona", SqlDbType.Bit)
        p(3).Value = DeleteDocumento_Persona

        p(4) = New SqlParameter("@DeleteObservaciones", SqlDbType.Bit)
        p(4).Value = DeleteObservaciones

        p(5) = New SqlParameter("@Anular", SqlDbType.Bit)
        p(5).Value = Anular

        p(6) = New SqlParameter("@DeleteMovAlmacen", SqlDbType.Bit)
        p(6).Value = DeleteMovAlmacen

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoAjusteInventario_DeshacerMov", p)

    End Sub
End Class

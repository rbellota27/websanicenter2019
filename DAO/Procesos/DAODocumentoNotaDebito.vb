﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.








'***********************  JUEVES 04 MARZO 2010 HORA 05_50 PM










Imports System.Data.SqlClient
Public Class DAODocumentoNotaDebito
    Private objConexion As New DAO.Conexion


    Public Sub DocumentoNotaDebito_DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteDetalleConcepto As Boolean, ByVal deleteMovCuenta As Boolean, ByVal deleteObservacion As Boolean, ByVal anular As Boolean, ByVal deleteRelacionDoc As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoNotaDebito_DeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@deleteDetalleConcepto", deleteDetalleConcepto)
        cmd.Parameters.AddWithValue("@deleteMovCuenta", deleteMovCuenta)
        cmd.Parameters.AddWithValue("@deleteObservacion", deleteObservacion)
        cmd.Parameters.AddWithValue("@deleteRelacionDoc", deleteRelacionDoc)
        cmd.Parameters.AddWithValue("@anular", anular)
        cmd.ExecuteNonQuery()

    End Sub

    Public Function DocumentoNotaDebitoSelectCab(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer) As Entidades.DocumentoNotaDebito

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objDocumento As New Entidades.DocumentoNotaDebito

        Try

            cn.Open()

            Dim cmd As New SqlCommand("_DocumentoNotaDebitoSelectCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@Codigo", Codigo)
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim lector As SqlDataReader
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            If (lector.Read) Then
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                    .ImporteTotal = CDec(IIf(IsDBNull(lector("doc_ImporteTotal")) = True, 0, lector("doc_ImporteTotal")))
                    .SubTotal = CDec(IIf(IsDBNull(lector("doc_SubTotal")) = True, 0, lector("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector("doc_Igv")) = True, 0, lector("doc_Igv")))
                    .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector("IdEstadoCan")) = True, 1, lector("IdEstadoCan"))) '******* POR APLICAR
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")) = True, 0, lector("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .Observacion = CStr(IIf(IsDBNull(lector("ob_Observacion")) = True, "", lector("ob_Observacion")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector("DescMoneda")) = True, "", lector("DescMoneda")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector("DescripcionPersona")) = True, "", lector("DescripcionPersona")))
                    .Ruc = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                    .Dni = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))
                    .IdDocumentoRef = CInt(IIf(IsDBNull(lector("IdDocumentoReferencia")) = True, 0, lector("IdDocumentoReferencia")))
                    .TipoDocumentoRef = CStr(IIf(IsDBNull(lector("TipoDocumentoRef")) = True, "", lector("TipoDocumentoRef")))
                    .DocRef_Serie = CStr(IIf(IsDBNull(lector("DocRef_Serie")) = True, "", lector("DocRef_Serie")))
                    .DocRef_Codigo = CStr(IIf(IsDBNull(lector("DocRef_Codigo")) = True, "", lector("DocRef_Codigo")))
                    .DocRef_FechaEmision = CDate(IIf(IsDBNull(lector("DocRef_FechaEmision")) = True, Nothing, lector("DocRef_FechaEmision")))
                    .DocRef_Total = CDec(IIf(IsDBNull(lector("DocRef_Total")) = True, 0, lector("DocRef_Total")))
                    .DocRef_TotalAPagar = CDec(IIf(IsDBNull(lector("DocRef_TotalAPagar")) = True, 0, lector("DocRef_TotalAPagar")))
                    .DocRef_Percepcion = CDec(IIf(IsDBNull(lector("DocRef_Percepcion")) = True, 0, lector("DocRef_Percepcion")))

                    .DocRef_IdMoneda = CInt(IIf(IsDBNull(lector("DocRef_IdMoneda")) = True, 0, lector("DocRef_IdMoneda")))

                End With
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento

    End Function






    Public Function SelectDocReferenciaxParams(ByVal IdCliente As Integer, ByVal FechaI As Date, ByVal FechaF As Date, ByVal IdTienda As Integer, ByVal serie As Integer, ByVal codigo As Integer, ByVal opcion As Integer) As List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoNotaDebitoSelectDocReferenciaxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)
        cmd.Parameters.AddWithValue("@FechaI", FechaI)
        cmd.Parameters.AddWithValue("@FechaF", FechaF)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@Serie", serie)
        cmd.Parameters.AddWithValue("@Codigo", codigo)
        cmd.Parameters.AddWithValue("@Opcion", opcion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoView
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")) = True, "", Format(lector("doc_FechaEmision"), "dd/MM/yyyy")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                        .Percepcion = CDec(IIf(IsDBNull(lector("Percepcion")) = True, 0, lector("Percepcion")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        .NomPropietario = CStr(IIf(IsDBNull(lector("per_NComercial")) = True, "", lector("per_NComercial")))
                        .NomTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")) = True, "", lector("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")) = True, "", lector("edoc_Nombre")))
                        .NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                        .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                        .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")) = True, 0, lector("IdCondicionPago")))
                        .Numero = .Serie + " - " + .Codigo
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


End Class

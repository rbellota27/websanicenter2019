﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOCuentaEmpresaTienda
    Inherits DAOMantenedor

    'Private cn As SqlConnection
    Private cmd As SqlCommand
    'Private tr As SqlTransaction
    Private lector As SqlDataReader
    'Private objconexion As New DAO.Conexion

    Public Function InsertarCuentaEmpresaTienda(ByVal lista As List(Of Entidades.CuentaEmpresaTienda), ByVal idsupervisor As Integer) As Boolean
        cn = objconexion.ConexionSIGE
        cmd = New SqlCommand("_InsertCuentaEmpresaTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cn.Open()
        Try
            For Each obj As Entidades.CuentaEmpresaTienda In lista
                Dim parametros() As SqlParameter = New SqlParameter(7) {}
                parametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
                parametros(0).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)
                parametros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
                parametros(1).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)
                parametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
                parametros(2).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)
                parametros(3) = New SqlParameter("@cet_MontoMax", SqlDbType.Decimal)
                parametros(3).Value = IIf(obj.cet_MontoMax = Nothing, DBNull.Value, obj.cet_MontoMax)
                parametros(4) = New SqlParameter("@IdSupervisor", SqlDbType.Int)
                parametros(4).Value = idsupervisor
                parametros(5) = New SqlParameter("@cet_Estado", SqlDbType.Bit)
                parametros(5).Value = IIf(obj.cet_Estado = Nothing, False, obj.cet_Estado)
                parametros(6) = New SqlParameter("@cet_NroMaxCuentas", SqlDbType.Int)
                parametros(6).Value = IIf(obj.cet_NroMaxCuentas = Nothing, DBNull.Value, obj.cet_NroMaxCuentas)
                parametros(7) = New SqlParameter("@cet_Saldo", SqlDbType.Decimal)
                parametros(7).Value = IIf(obj.saldoAnt = Nothing, DBNull.Value, obj.saldoAnt)
                cmd.Parameters.AddRange(parametros)
                cmd.ExecuteNonQuery()
            Next
        Catch ex As Exception
            Throw ex
            Return False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function

    Public Sub UpdateCuentaEmpresaTienda(ByVal obj As Entidades.CuentaEmpresaTienda, ByVal idsupervisor As Integer, ByVal sqlcn As SqlConnection, ByVal sqltr As SqlTransaction)

        Dim parametros() As SqlParameter = New SqlParameter(7) {}
        parametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        parametros(0).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)
        parametros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametros(1).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)
        parametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametros(2).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)
        parametros(3) = New SqlParameter("@cet_MontoMax", SqlDbType.Decimal)
        parametros(3).Value = IIf(obj.cet_MontoMax = Nothing, DBNull.Value, obj.cet_MontoMax)
        parametros(4) = New SqlParameter("@IdSupervisor", SqlDbType.Int)
        parametros(4).Value = idsupervisor
        parametros(5) = New SqlParameter("@cet_Estado", SqlDbType.Bit)
        parametros(5).Value = IIf(obj.cet_Estado = Nothing, DBNull.Value, obj.cet_Estado)
        parametros(6) = New SqlParameter("@cet_NroMaxCuentas", SqlDbType.Int)
        parametros(6).Value = IIf(obj.cet_NroMaxCuentas = Nothing, DBNull.Value, obj.cet_NroMaxCuentas)
        parametros(7) = New SqlParameter("@cet_Saldo", SqlDbType.Decimal)
        parametros(7).Value = IIf(obj.cet_Saldo = Nothing, DBNull.Value, obj.cet_Saldo)

        Try

            cmd = New SqlCommand("_UpdateCuentaEmpresaTienda", sqlcn, sqltr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Function SelectCuentaEmpresaTienda(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Idmoneda As Integer, ByVal estado As Integer) As List(Of Entidades.CuentaEmpresaTienda)
        Dim Lista As New List(Of Entidades.CuentaEmpresaTienda)
        Cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_SelectCuentaEmpresaTienda", Cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdMoneda", Idmoneda)
        cmd.Parameters.AddWithValue("@Estado", estado)

        Try
            Cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.CuentaEmpresaTienda
                With obj
                    .IdEmpresa = UCInt(lector("IdEmpresa"))
                    .cstrEmpresa = UCStr(lector("jur_Rsocial"))
                    .IdTienda = UCInt(lector("IdTienda"))
                    .cstrTienda = UCStr(lector("tie_Nombre"))
                    .IdMoneda = UCInt(lector("IdMoneda"))
                    .cstrMoneda = UCStr(lector("mon_Simbolo"))
                    .cet_MontoMax = UCDec(IIf(IsDBNull(lector("cet_MontoMax")), 0, lector("cet_MontoMax")))
                    .cet_Saldo = UCDec(IIf(IsDBNull(lector("cet_Saldo")), 0, lector("cet_Saldo")))
                    .cet_NroMaxCuentas = UCInt(IIf(IsDBNull(lector("cet_NroMaxCuentas")), 0, lector("cet_NroMaxCuentas")))
                    .cet_Estado = UCBool(IIf(IsDBNull(lector("cet_Estado")), False, lector("cet_Estado")))
                    .Tipo = 2 'Actualizar
                    .montoAnt = UCDec(IIf(IsDBNull(lector("cet_MontoMax")), 0, lector("cet_MontoMax")))
                    .saldoAnt = UCDec(IIf(IsDBNull(lector("cet_Saldo")), 0, lector("cet_Saldo")))
                    .CuentasActivas = CInt(lector("NumCuentasActivas"))
                    .CargoMaxCtasxTienda = UCInt(lector("CargoMaxCtasxTienda"))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If Cn.State = ConnectionState.Open Then Cn.Close()
        End Try
        Return Lista
    End Function

    Function SelectxIdEmpresaxIdTiendaxIdMoneda(ByVal IdEmpresa As Integer, _
    ByVal IdTienda As Integer, ByVal IdMoneda As Integer) As Entidades.CuentaEmpresaTienda

        Dim obj As New Entidades.CuentaEmpresaTienda

        cn = objconexion.ConexionSIGE
        cmd = New SqlCommand("_CuentaEmpresaTiendaSelectxIdEmpresaxIdTiendaxIdMoneda", Cn)
        'cmd = New SqlCommand("_CuentaEmpresaTiendaSelectxIdEmpresaxIdTiendaxIdMoneda_Prueba", Cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)

        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                With obj
                    .IdEmpresa = UCInt(lector("IdEmpresa"))
                    .cstrEmpresa = UCStr(lector("jur_Rsocial"))
                    .IdTienda = UCInt(lector("IdTienda"))
                    .cstrTienda = UCStr(lector("tie_Nombre"))
                    .IdMoneda = UCInt(lector("IdMoneda"))
                    .cstrMoneda = UCStr(lector("mon_Simbolo"))
                    .cet_MontoMax = UCDec(lector("cet_MontoMax"))
                    .cet_Saldo = UCDec(lector("cet_Saldo"))
                    .cet_NroMaxCuentas = UCInt(lector("cet_NroMaxCuentas"))
                    .cet_Estado = UCBool(IIf(IsDBNull(lector("cet_Estado")), False, lector("cet_Estado")))
                    .Tipo = 2 'Actualizar
                    .montoAnt = UCDec(IIf(IsDBNull(lector("cet_MontoMax")), 0, lector("cet_MontoMax")))
                    .saldoAnt = UCDec(IIf(IsDBNull(lector("cet_Saldo")), 0, lector("cet_Saldo")))
                    .CuentasActivas = UCInt(lector("NumCuentasActivas"))
                    .IdSupervisor = UCInt(lector("IdSupervisor"))
                    .CargoMaxCtasxTienda = UCInt(lector("CargoMaxCtasxTienda"))
                End With
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return obj
    End Function
End Class

﻿Imports System.Data
Imports System.Data.SqlClient
Imports Entidades

Public Class DAO_CotizacionListas

    Public Function obtenerListas(ByVal IdUsuario As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer,
                                  ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdCondicionPago As Integer,
                                  ByVal CodDep As String, ByVal CodProv As String, ByVal IdTipoExistencia As Integer,
                                  ByVal IdMangitud As Integer, ByVal Permisos As String, ByVal Parametros As String, ByVal con As SqlConnection) As beCotizacionListas
        'ByVal IdLinea As Integer, ByVal IdSubLinea As Integer,

        Dim obeCotizacionListas As beCotizacionListas

        Dim cmd As New SqlCommand("uspHte_CotizacionListas", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@IdUsuario", SqlDbType.Int)
        cmd.Parameters.Add(par1)
        par1.Direction = ParameterDirection.Input
        par1.Value = IdUsuario

        Dim par2 As New SqlParameter("@IdEmpresa", SqlDbType.Int)
        cmd.Parameters.Add(par2)
        par2.Direction = ParameterDirection.Input
        par2.Value = IdEmpresa

        Dim par3 As New SqlParameter("@IdTienda", SqlDbType.Int)
        cmd.Parameters.Add(par3)
        par3.Direction = ParameterDirection.Input
        par3.Value = IdTienda

        Dim par4 As New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        cmd.Parameters.Add(par4)
        par4.Direction = ParameterDirection.Input
        par4.Value = IdTipoDocumento

        Dim par5 As New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        cmd.Parameters.Add(par5)
        par5.Direction = ParameterDirection.Input
        par5.Value = IdTipoOperacion

        Dim par6 As New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        cmd.Parameters.Add(par6)
        par6.Direction = ParameterDirection.Input
        par6.Value = IdCondicionPago

        Dim par7 As New SqlParameter("@ub_CodDpto", SqlDbType.Char, 2)
        cmd.Parameters.Add(par7)
        par7.Direction = ParameterDirection.Input
        par7.Value = CodDep

        Dim par8 As New SqlParameter("@ub_CodProv", SqlDbType.Char, 2)
        cmd.Parameters.Add(par8)
        par8.Direction = ParameterDirection.Input
        par8.Value = CodProv

        Dim par9 As New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
        cmd.Parameters.Add(par9)
        par9.Direction = ParameterDirection.Input
        par9.Value = IdTipoExistencia

        'Dim par10 As New SqlParameter("@IdLinea", SqlDbType.Int)
        'par10.Direction = ParameterDirection.Input
        'par10.Value = IdLinea

        'Dim par11 As New SqlParameter("@IdSubLinea", SqlDbType.Int)
        'par11.Direction = ParameterDirection.Input
        'par11.Value = IdSubLinea

        Dim par12 As New SqlParameter("@IdMagnitud", SqlDbType.Int)
        cmd.Parameters.Add(par12)
        par12.Direction = ParameterDirection.Input
        par12.Value = IdMangitud

        Dim par13 As New SqlParameter("@Permisos", SqlDbType.VarChar, 58)
        cmd.Parameters.Add(par13)
        par13.Direction = ParameterDirection.Input
        par13.Value = Permisos

        Dim par14 As New SqlParameter("@Parametros", SqlDbType.VarChar, Integer.MaxValue)
        cmd.Parameters.Add(par14)
        par14.Direction = ParameterDirection.Input
        par14.Value = Parametros

        Dim drd As SqlDataReader = cmd.ExecuteReader()

        If (drd IsNot Nothing) Then
            obeCotizacionListas = New beCotizacionListas
            If (drd.HasRows) Then
                Dim valor As String = ""
                If (drd.Read) Then
                    valor = drd.GetString(0)
                End If
                obeCotizacionListas.valorParametrosGenerales = valor
            End If
            If (drd.NextResult) Then
                Dim lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    lista.Add(obj)
                End While
                obeCotizacionListas.listaEmpresa = lista
            End If
            If (drd.NextResult) Then
                Dim lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    lista.Add(obj)
                End While
                obeCotizacionListas.listaTienda = lista
            End If
            If (drd.NextResult) Then
                Dim lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    lista.Add(obj)
                End While
                obeCotizacionListas.listaSerie = lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero2)
                Dim obj As beCampoEntero2
                While drd.Read
                    obj = New beCampoEntero2
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    obj.campo3 = drd.GetString(2)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoDocumento = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaMoneda = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaEstadoDocumento = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoOperacion = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaMotivoTraslado = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaCondicionPago = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaMedioPago = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoPrecio = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero2)
                Dim obj As beCampoEntero2
                While drd.Read
                    obj = New beCampoEntero2
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    obj.campo3 = drd.GetString(2)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaRolCliente = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoAgente = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboDepartamento)
                Dim obj As beCboDepartamento
                While drd.Read
                    obj = New beCboDepartamento
                    obj.CodDpto = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaDepartamento = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboProvincia)
                Dim obj As beCboProvincia
                While drd.Read
                    obj = New beCboProvincia
                    obj.CodProv = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaProvincia = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboDistrito)
                Dim obj As beCboDistrito
                While drd.Read
                    obj = New beCboDistrito
                    obj.CodDist = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaDistrito = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaAlmacen = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoExistencia = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaLinea = Lista
            End If
            'If (drd.NextResult) Then
            '    Dim Lista As New List(Of beCampoEntero)
            '    Dim obj As beCampoEntero
            '    While drd.Read
            '        obj = New beCampoEntero
            '        obj.campo1 = drd.GetInt32(0)
            '        obj.campo2 = drd.GetString(1)
            '        Lista.Add(obj)
            '    End While
            '    obeCotizacionListas.listaSubLinea = Lista
            'End If
            'If (drd.NextResult) Then
            '    Dim Lista As New List(Of beCampoEntero)
            '    Dim obj As beCampoEntero
            '    While drd.Read
            '        obj = New beCampoEntero
            '        obj.campo1 = drd.GetInt32(0)
            '        obj.campo2 = drd.GetString(1)
            '        Lista.Add(obj)
            '    End While
            '    obeCotizacionListas.listaTipoTabla = Lista
            'End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaPersona = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoAlmacen = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaMagnitud = Lista
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.TipoDiseno = drd.GetInt32(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.IdMedioPago = drd.GetInt32(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.TasaIGV = drd.GetDecimal(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.FechaActual = drd.GetDateTime(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.TipoPrecio = drd.GetInt32(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    Dim Lista As New List(Of Decimal)
                    While drd.Read
                        Lista.Add(drd.GetDecimal(0))
                    End While
                    obeCotizacionListas.listaParametros = Lista
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    Dim obj As New beCampoCadena
                    obj.campo1 = drd.GetString(0)
                    obj.campo2 = drd.GetString(1)
                    obeCotizacionListas.TiendaUbigeo = obj
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.NroDocumento = drd.GetString(0)
                End If
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboDepartamento)
                Dim obj As beCboDepartamento
                While drd.Read
                    obj = New beCboDepartamento
                    obj.CodDpto = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaDepartamentoPartida = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboProvincia)
                Dim obj As beCboProvincia
                While drd.Read
                    obj = New beCboProvincia
                    obj.CodProv = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaProvinciaPartida = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboDistrito)
                Dim obj As beCboDistrito
                While drd.Read
                    obj = New beCboDistrito
                    obj.CodDist = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaDistritoPartida = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of Integer)
                While drd.Read
                    Lista.Add(drd.GetInt32(0))
                End While
                obeCotizacionListas.listaPermisos = Lista
            End If
            drd.Close()
        End If
        Return obeCotizacionListas
    End Function

    Public Function obtenerProductos(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String,
                                        ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer,
                                        ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer,
                                        ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable,
                                        ByVal codigoProducto As String, ByVal filtroProductoCampania As Boolean, ByVal codbarras As String, ByVal IdTipoExistencia As Integer,
                                        ByVal codigoProveedor As String, ByVal IdTipoOperacion As Integer, ByVal con As SqlConnection) As List(Of Entidades.Catalogo)
        Descripcion = Descripcion.Replace("*", "%")

        Dim cmd As New SqlCommand("CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado_HTE", con)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)
        cmd.Parameters.AddWithValue("@Tabla", tableTipoTabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@filtroProductoCampania", filtroProductoCampania)
        cmd.Parameters.AddWithValue("@codbarras", codbarras)
        cmd.Parameters.AddWithValue("@CodigoProveedor", codigoProveedor)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Dim lector As SqlDataReader
        lector = cmd.ExecuteReader()
        Dim Lista As New List(Of Entidades.Catalogo)
        Do While lector.Read
            Dim objCatalogo As New Entidades.Catalogo

            With objCatalogo
                .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))

                .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                .PrecioBaseDcto = CStr(IIf(IsDBNull(lector.Item("PrecioBaseDcto")) = True, "", lector.Item("PrecioBaseDcto")))

                .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                .pvComercial = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                '.ExisteCampania_Producto = CBool(IIf(IsDBNull(lector("ExisteCampania_Producto")) = True, False, True))
                .ExisteCampania_Producto = IIf(lector.GetInt32(lector.GetOrdinal("ExisteCampania_Producto")) = 1, True, False)
                .Prod_CodigoBarras = CStr(IIf(IsDBNull(lector.Item("prod_CodigoBarras")) = True, "", lector.Item("prod_CodigoBarras")))
                .NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                .UMProducto = CStr(lector.Item("UMProducto"))
            End With

            Lista.Add(objCatalogo)
        Loop
        Return Lista

    End Function

End Class

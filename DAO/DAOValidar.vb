'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOValidar

    Public Function ValidarxUnParametro(ByVal NombreTabla As String _
     , ByVal NombreCampo As String, ByVal ValorCampo As String) As Integer

        Dim HDAO As New HelperDAO
        Dim objConexion As New Conexion

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@NombreTabla", SqlDbType.Char)
        ArrayParametros(0).Value = NombreTabla
        ArrayParametros(1) = New SqlParameter("NombreCampo", SqlDbType.Char)
        ArrayParametros(1).Value = NombreCampo
        ArrayParametros(2) = New SqlParameter("@ValorCampo", SqlDbType.Char)
        ArrayParametros(2).Value = ValorCampo

        Return HDAO.ValidarxUnParametro(cn, "_ValidarxUnParametro", ArrayParametros)

    End Function

End Class

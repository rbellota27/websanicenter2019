﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOProgramacionPago_CXP

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection
    Private reader As SqlDataReader
    Private objconexion As New DAO.Conexion


    Public Function DocumentoProgramacion_SelectAprobacion(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal MontoTot As Decimal) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(6) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdArea, "@IdArea", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Opcion_Aprobacion, "@Opcion_Aprobacion", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(FechaFin, "@FechaFin", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(MontoTot, "@monto", SqlDbType.Decimal)

            Dim cmd As New SqlCommand("_DocumentoProgramacionPago_Aprobacion", objconexion.ConexionSIGE)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoProgramaciones")

        Catch ex As Exception
            Throw ex
        End Try

        Return ds.Tables("DT_DocumentoProgramaciones")

    End Function



    Public Sub ProgramacionAlModificar(ByVal idRequerimiento As String, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, _
                                               ByVal objProgramacionPago_CXP As Entidades.ProgramacionPago_CXP, ByVal cadenaDocumentoReferencia As String, _
                                               ByVal idRequerimientoCadena As String)
        'Try
        Using cmd As New SqlCommand("SP_EDITAR_PROGRAMACION_PAGO", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idProgramacion", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdProgramacionPago
                .Parameters.Add(New SqlParameter("@pp_FechaPagoProg", SqlDbType.Date)).Value = objProgramacionPago_CXP.FechaPagoProg
                .Parameters.Add(New SqlParameter("@pp_MontoPagoProg", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.MontoPagoProg
                .Parameters.Add(New SqlParameter("@IdMedioPago", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdMedioPago
                .Parameters.Add(New SqlParameter("@IdBanco", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdBanco
                .Parameters.Add(New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdCuentaBancaria
                .Parameters.Add(New SqlParameter("@pp_Observacion", SqlDbType.NVarChar)).Value = objProgramacionPago_CXP.Observacion
                .Parameters.Add(New SqlParameter("@tipocambio", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.TipoCambio
                .Parameters.Add(New SqlParameter("@idMoneda", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdTipoMoneda
                .Parameters.Add(New SqlParameter("@pp_esDetraccion", SqlDbType.Bit)).Value = objProgramacionPago_CXP.idEsDetraccion
                .Parameters.Add(New SqlParameter("@pp_esRetencion", SqlDbType.Bit)).Value = objProgramacionPago_CXP.idEsRetencion
                .Parameters.Add(New SqlParameter("@strDocumentosRef", SqlDbType.NVarChar)).Value = cadenaDocumentoReferencia
                .Parameters.Add(New SqlParameter("@idConcepto", SqlDbType.Int)).Value = objProgramacionPago_CXP.idConcepto
                .Parameters.Add(New SqlParameter("@idStringDocumentoReq", SqlDbType.NVarChar)).Value = idRequerimientoCadena
                Try
                    .ExecuteNonQuery()
                Catch ex As Exception
                    Throw ex
                End Try

            End With
        End Using
    End Sub

    Public Sub Insert(ByVal cadenaDocumentoReferencia As String, ByVal idRequerimientoCadena As String, ByVal objProgramacionPago_CXP As Entidades.ProgramacionPago_CXP, ByVal idEsDetraccion As Boolean, ByVal idEsRetencion As Boolean, _
                      ByVal idEsPercepcion As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        'Try
        Using cmd As New SqlCommand("_ProgramacionPago_CXP_Insert", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdMovCtaPP", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdMovCtaPP
                .Parameters.Add(New SqlParameter("@pp_FechaPagoProg", SqlDbType.Date)).Value = objProgramacionPago_CXP.FechaPagoProg
                .Parameters.Add(New SqlParameter("@pp_MontoPagoProg", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.MontoPagoProg
                .Parameters.Add(New SqlParameter("@IdMedioPago", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdMedioPago
                .Parameters.Add(New SqlParameter("@IdBanco", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdBanco
                .Parameters.Add(New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdCuentaBancaria
                .Parameters.Add(New SqlParameter("@pp_Observacion", SqlDbType.VarChar)).Value = objProgramacionPago_CXP.Observacion
                .Parameters.Add(New SqlParameter("@idmoneda", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdTipoMoneda
                .Parameters.Add(New SqlParameter("@tipocambio", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.TipoCambio
                .Parameters.Add(New SqlParameter("@porc_agente", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.porc_agente
                .Parameters.Add(New SqlParameter("@idEsDetraccion", SqlDbType.Int)).Value = idEsDetraccion
                .Parameters.Add(New SqlParameter("@idEsRetencion", SqlDbType.Int)).Value = idEsRetencion
                .Parameters.Add(New SqlParameter("@idEsPercepcion", SqlDbType.Int)).Value = idEsPercepcion
                .Parameters.Add(New SqlParameter("@idRequerimientoCadena", SqlDbType.NVarChar)).Value = idRequerimientoCadena
                .Parameters.Add(New SqlParameter("@ResultadoSaldoRQ", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.saldoProgramacion
                .Parameters.Add(New SqlParameter("@montoSujetoDetraccion", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.montoSujetoADetraccion
                .Parameters.Add(New SqlParameter("@montoSujetoRetencion", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.montoSujetoARetencion
                .Parameters.Add(New SqlParameter("@strDocumentoRef", SqlDbType.NVarChar, 400)).Value = cadenaDocumentoReferencia
                .Parameters.Add(New SqlParameter("@idConcepto", SqlDbType.NVarChar, 400)).Value = objProgramacionPago_CXP.idConcepto
                .Parameters.Add(New SqlParameter("@montoTotalRedondeo", SqlDbType.NVarChar, 400)).Value = objProgramacionPago_CXP.montoPagoTotalRedondeo
                Try
                    .ExecuteNonQuery()
                Catch ex As Exception
                    Throw ex
                End Try

            End With
        End Using

        '    Dim p() As SqlParameter = New SqlParameter(12) {}

        '    With objProgramacionPago_CXP

        '        p(0) = objDaoMantenedor.getParam(.IdMovCtaPP, "@IdMovCtaPP", SqlDbType.Int)
        '        p(1) = objDaoMantenedor.getParam(.FechaPagoProg, "@pp_FechaPagoProg", SqlDbType.Date)
        '        p(2) = objDaoMantenedor.getParam(.MontoPagoProg, "@pp_MontoPagoProg", SqlDbType.Decimal)
        '        p(3) = objDaoMantenedor.getParam(.IdMedioPago, "@IdMedioPago", SqlDbType.Int)
        '        p(4) = objDaoMantenedor.getParam(.IdBanco, "@IdBanco", SqlDbType.Int)
        '        p(5) = objDaoMantenedor.getParam(.IdCuentaBancaria, "@IdCuentaBancaria", SqlDbType.Int)
        '        p(6) = objDaoMantenedor.getParam(.Observacion, "@pp_Observacion", SqlDbType.VarChar)
        '        p(7) = objDaoMantenedor.getParam(.IdTipoMoneda, "@idmoneda", SqlDbType.Int)
        '        p(8) = objDaoMantenedor.getParam(.TipoCambio, "@tipocambio", SqlDbType.Decimal)
        '        p(9) = objDaoMantenedor.getParam(.porc_agente, "@porc_agente", SqlDbType.Decimal)
        '        p(10) = objDaoMantenedor.getParam(idEsDetraccion, "@idEsDetraccion", SqlDbType.Int)
        '        p(11) = objDaoMantenedor.getParam(idEsRetencion, "@idEsRetencion", SqlDbType.Int)
        '        p(12) = objDaoMantenedor.getParam(idEsPercepcion, "@idEsPercepcion", SqlDbType.Int)

        '    End With

        '    cn = (New DAO.Conexion).ConexionSIGE
        '    cn.Open()

        '    SqlHelper.ExecuteNonQuery(cn, CommandType.StoredProcedure, "_ProgramacionPago_CXP_Insert", p)

        'Catch ex As Exception
        '    Throw ex
        'Finally
        '    If (cn.State = ConnectionState.Open) Then cn.Close()
        'End Try

    End Sub


    Public Sub InsertParcial(ByVal cadenaDocumentoReferencia As String, ByVal idRequerimientoCadena As String, ByVal objProgramacionPago_CXP As Entidades.ProgramacionPago_CXP, ByVal idEsDetraccion As Boolean, ByVal idEsRetencion As Boolean, _
                     ByVal idEsPercepcion As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        'Try
        Using cmd As New SqlCommand("_ProgramacionPago_CXP_Insert_Parcial", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdMovCtaPP", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdMovCtaPP
                .Parameters.Add(New SqlParameter("@pp_FechaPagoProg", SqlDbType.Date)).Value = objProgramacionPago_CXP.FechaPagoProg
                .Parameters.Add(New SqlParameter("@pp_MontoPagoProg", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.MontoPagoProg
                .Parameters.Add(New SqlParameter("@IdMedioPago", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdMedioPago
                .Parameters.Add(New SqlParameter("@IdBanco", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdBanco
                .Parameters.Add(New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdCuentaBancaria
                .Parameters.Add(New SqlParameter("@pp_Observacion", SqlDbType.VarChar)).Value = objProgramacionPago_CXP.Observacion
                .Parameters.Add(New SqlParameter("@idmoneda", SqlDbType.Int)).Value = objProgramacionPago_CXP.IdTipoMoneda
                .Parameters.Add(New SqlParameter("@tipocambio", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.TipoCambio
                .Parameters.Add(New SqlParameter("@porc_agente", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.porc_agente
                .Parameters.Add(New SqlParameter("@idEsDetraccion", SqlDbType.Int)).Value = idEsDetraccion
                .Parameters.Add(New SqlParameter("@idEsRetencion", SqlDbType.Int)).Value = idEsRetencion
                .Parameters.Add(New SqlParameter("@idEsPercepcion", SqlDbType.Int)).Value = idEsPercepcion
                .Parameters.Add(New SqlParameter("@idRequerimientoCadena", SqlDbType.NVarChar)).Value = idRequerimientoCadena
                .Parameters.Add(New SqlParameter("@ResultadoSaldoRQ", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.saldoProgramacion
                .Parameters.Add(New SqlParameter("@montoSujetoDetraccion", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.montoSujetoADetraccion
                .Parameters.Add(New SqlParameter("@montoSujetoRetencion", SqlDbType.Decimal)).Value = objProgramacionPago_CXP.montoSujetoARetencion
                .Parameters.Add(New SqlParameter("@strDocumentoRef", SqlDbType.NVarChar, 400)).Value = cadenaDocumentoReferencia
                .Parameters.Add(New SqlParameter("@idConcepto", SqlDbType.NVarChar, 400)).Value = objProgramacionPago_CXP.idConcepto
                .Parameters.Add(New SqlParameter("@montoTotalRedondeo", SqlDbType.NVarChar, 400)).Value = objProgramacionPago_CXP.montoPagoTotalRedondeo
                Try
                    .ExecuteNonQuery()
                Catch ex As Exception
                    Throw ex
                End Try

            End With
        End Using


    End Sub



    Public Sub Update(ByVal objProgramacionPago_CXP As Entidades.ProgramacionPago_CXP)

        Try

            Dim p() As SqlParameter = New SqlParameter(9) {}

            With objProgramacionPago_CXP

                p(0) = objDaoMantenedor.getParam(.IdMovCtaPP, "@IdMovCtaPP", SqlDbType.Int)
                p(1) = objDaoMantenedor.getParam(.FechaPagoProg, "@pp_FechaPagoProg", SqlDbType.Date)
                p(2) = objDaoMantenedor.getParam(.MontoPagoProg, "@pp_MontoPagoProg", SqlDbType.Decimal)
                p(3) = objDaoMantenedor.getParam(.IdMedioPago, "@IdMedioPago", SqlDbType.Int)
                p(4) = objDaoMantenedor.getParam(.IdBanco, "@IdBanco", SqlDbType.Int)
                p(5) = objDaoMantenedor.getParam(.IdCuentaBancaria, "@IdCuentaBancaria", SqlDbType.Int)
                p(6) = objDaoMantenedor.getParam(.Observacion, "@pp_Observacion", SqlDbType.VarChar)
                p(7) = objDaoMantenedor.getParam(.IdProgramacionPago, "@IdProgramacionPago", SqlDbType.Int)
                p(8) = objDaoMantenedor.getParam(.IdTipoMoneda, "@idmoneda", SqlDbType.Int)
                p(9) = objDaoMantenedor.getParam(.TipoCambio, "@tipocambio", SqlDbType.Decimal)
            End With

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            SqlHelper.ExecuteNonQuery(cn, CommandType.StoredProcedure, "_ProgramacionPago_CXP_Update", p)

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

    End Sub

    Public Function DeletexIdProgramacionPagoCXP(ByVal cadenaIdRequerimiento As String, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim retornar As Integer = 0
            Using cmd As New SqlCommand("_ProgramacionPago_CXP_DeletexIdProgramacionPago", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                With cmd
                    .Parameters.Add(New SqlParameter("@cadenaRequerimiento", SqlDbType.NVarChar)).Value = cadenaIdRequerimiento
                End With
                Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
                If rdr IsNot Nothing Then
                    While rdr.Read()
                        retornar = CBool(rdr.GetInt32(0))
                    End While
                End If
                rdr.Close()
            End Using
            Return retornar
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectxIdMovCuentaCXP2(ByVal IdMovCuentaCXP As Integer) As List(Of Entidades.ProgramacionPago_CXP)

        Dim lista As New List(Of Entidades.ProgramacionPago_CXP)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdMovCuentaCXP, "@IdMovCuentaCXP", SqlDbType.Int)

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ProgramacionPago_CXP_SelectxIdMovCuentaCXP2", p)

            While (reader.Read)

                Dim obj As New Entidades.ProgramacionPago_CXP

                With obj

                    .IdMovCtaPP = objDaoMantenedor.UCInt(reader("IdMovCtaPP"))
                    .IdProgramacionPago = objDaoMantenedor.UCInt(reader("IdProgramacionPago"))
                    .FechaPagoProg = objDaoMantenedor.UCDate(reader("pp_FechaPagoProg"))
                    .MontoPagoProg = objDaoMantenedor.UCDec(reader("pp_MontoPagoProg"))
                    .IdBanco = objDaoMantenedor.UCInt(reader("IdBanco"))
                    .IdMedioPago = objDaoMantenedor.UCInt(reader("IdMedioPago"))
                    .IdCuentaBancaria = objDaoMantenedor.UCInt(reader("IdCuentaBancaria"))
                    .Observacion = objDaoMantenedor.UCStr(reader("pp_Observacion"))
                    .MedioPago = objDaoMantenedor.UCStr(reader("mp_Nombre"))
                    .Banco = objDaoMantenedor.UCStr(reader("ban_Nombre"))
                    .NroCuentaBancaria = objDaoMantenedor.UCStr(reader("cb_numero"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("Idmon"))
                    .Moneda = objDaoMantenedor.UCStr(reader("mon_Simbolo"))
                    .TipoCambio = objDaoMantenedor.UCDec(reader("tipocambio"))
                    .MonedaNew = objDaoMantenedor.UCStr(reader("idmoneda"))
                    '.NroDocumentoRef = objDaoMantenedor.UCStr(reader("NroDocumentoRef"))
                    .IdDocumentoRef = objDaoMantenedor.UCInt(reader("IdDocumentoRef"))
                End With

                lista.Add(obj)

            End While

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function
    Public Function SelectxIdRetenciones(ByVal cadenaIdRequerimiento As String, ByVal IdUsuario As Integer, ByVal FechaPagoProg As String) As Entidades.be_programacionPagos


        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            Using cmd As New SqlCommand("_DocumentoInsertRetencion", cn)
                With cmd
                    .CommandType = CommandType.StoredProcedure

                    .Parameters.Add(New SqlParameter("@IdRequerimiento", SqlDbType.NVarChar)).Value = cadenaIdRequerimiento
                    .Parameters.Add(New SqlParameter("@IdUsuarioAuditoria", SqlDbType.NVarChar)).Value = IdUsuario
                    .Parameters.Add(New SqlParameter("@FechaCancelacion", SqlDbType.Date)).Value = Convert.ToDateTime(FechaPagoProg)


                    cn = (New DAO.Conexion).ConexionSIGE
                    cn.Open()


                End With
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
            End Using
                Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try




    End Function

    Public Function InsertDocumentosSolucont(ByVal IdUsuario As Integer, ByVal FechaPagoProg As String, ByVal cadenaIdRequerimiento As String) As Entidades.be_programacionPagos
        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            Using cmd As New SqlCommand("SP_ASIENTO_CONTABLE_FINAL_RETENCIONES", cn)
                With cmd
                    .CommandType = CommandType.StoredProcedure


                    .Parameters.Add(New SqlParameter("@idpersona", SqlDbType.NVarChar)).Value = IdUsuario
                    .Parameters.Add(New SqlParameter("@fechaPago", SqlDbType.Date)).Value = Convert.ToDateTime(FechaPagoProg)
                    .Parameters.Add(New SqlParameter("@IdStringProgramacion", SqlDbType.NVarChar)).Value = cadenaIdRequerimiento

                   

                    cn = (New DAO.Conexion).ConexionSIGE
                    cn.Open()


                End With
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try




    End Function


    Public Function SelectxIdMovCuentaCXPTexto(ByVal IdRequerimiento As String) As Entidades.be_programacionPagos
        Dim objeto As Entidades.be_programacionPagos = Nothing
        Try
            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            Using cmd As New SqlCommand("SP_VER_PAGO_RETENCION", cn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add(New SqlParameter("@idRequerimiento", SqlDbType.NVarChar)).Value = IdRequerimiento
                End With
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
                If rdr IsNot Nothing Then
                    'si retorna del servidor inicializo el objeto
                    objeto = New Entidades.be_programacionPagos


                    Dim objetoRegimen As Entidades.be_montoRegimen = Nothing
                    Dim listaRegimen As New List(Of Entidades.be_montoRegimen)

                    While rdr.Read()
                        objetoRegimen = New Entidades.be_montoRegimen
                        With objetoRegimen
                            .idDocumento = rdr.GetInt32(0)
                            .descripcion = rdr.GetString(1)
                            .codigo = rdr.GetString(2)
                            .montoRegimen = rdr.GetDecimal(3)
                            .tipoRegimen = rdr.GetInt32(4)
                            .canNroOperacion = rdr.GetString(5)
                            .montoCancelado = rdr.GetDecimal(6)
                            .fechaCancelacion = rdr.GetString(7)
                            .bancoCancelacion = rdr.GetString(8)
                            .DocumentoRetencion = rdr.GetString(9)
                        End With
                        listaRegimen.Add(objetoRegimen)
                    End While
                    objeto.listaMontoRegimen = listaRegimen




                    rdr.Close()
                End If

            End Using
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return objeto
    End Function

    Public Function SelectxIdMovCuentaCXP(ByVal IdRequerimiento As Integer) As Entidades.be_programacionPagos
        Dim objeto As Entidades.be_programacionPagos = Nothing
        Try
            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            Using cmd As New SqlCommand("SP_VER_PAGO_PROGRAMACION", cn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add(New SqlParameter("@idRequerimiento", SqlDbType.Int)).Value = IdRequerimiento
                End With
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
                If rdr IsNot Nothing Then
                    'si retorna del servidor inicializo el objeto
                    objeto = New Entidades.be_programacionPagos

                    Dim listaProgramacion As List(Of Entidades.ProgramacionPago_CXP) = New List(Of Entidades.ProgramacionPago_CXP)
                    Dim listaRequerimiento As List(Of Entidades.be_Requerimiento_x_pagar) = New List(Of Entidades.be_Requerimiento_x_pagar)
                    Dim objetoProgramacion As Entidades.ProgramacionPago_CXP = Nothing
                    Dim objetoRequerimiento As Entidades.be_Requerimiento_x_pagar = Nothing

                    Dim IdMovCtaPP As Integer = rdr.GetOrdinal("IdMovCtaPP")
                    Dim IdProgramacionPago As Integer = rdr.GetOrdinal("IdProgramacionPago")
                    Dim FechaPagoProg As Integer = rdr.GetOrdinal("pp_FechaPagoProg")
                    Dim MontoPagoProg As Integer = rdr.GetOrdinal("pp_MontoPagoProg")
                    Dim IdBanco As Integer = rdr.GetOrdinal("IdBanco")
                    Dim IdMedioPago As Integer = rdr.GetOrdinal("IdMedioPago")
                    Dim IdCuentaBancaria As Integer = rdr.GetOrdinal("IdCuentaBancaria")
                    Dim Observacion As Integer = rdr.GetOrdinal("pp_Observacion")
                    Dim MedioPago As Integer = rdr.GetOrdinal("mp_Nombre")
                    Dim Banco As Integer = rdr.GetOrdinal("ban_Nombre")
                    Dim NroCuentaBancaria As Integer = rdr.GetOrdinal("cb_numero")
                    Dim IdMoneda As Integer = rdr.GetOrdinal("IdMoneda")
                    Dim Moneda As Integer = rdr.GetOrdinal("mon_Simbolo")
                    Dim tipocambio As Integer = rdr.GetOrdinal("tipocambio")
                    Dim NroDocumentoRef As Integer = rdr.GetOrdinal("NroDocumentoRef")
                    Dim IdDocumentoRef As Integer = rdr.GetOrdinal("IdDocumentoRef")
                    Dim idEsDetraccion As Integer = rdr.GetOrdinal("pp_esDetraccion")
                    Dim idEsRetencion As Integer = rdr.GetOrdinal("pp_esRetencion")
                    Dim can_nrooperacion As Integer = rdr.GetOrdinal("can_nrooperacion")
                    Dim can_montoDeposito As Integer = rdr.GetOrdinal("can_montoDeposito")
                    Dim bancoCancelacion As Integer = rdr.GetOrdinal("bancoCancelacion")
                    Dim can_fechaPago As Integer = rdr.GetOrdinal("can_fechaPago")
                    Dim idConcepto As Integer = rdr.GetOrdinal("idBanco")
                    Dim nroVoucher As Integer = rdr.GetOrdinal("ase_nVoucher")

                    While rdr.Read()
                        objetoProgramacion = New Entidades.ProgramacionPago_CXP
                        With objetoProgramacion
                            .IdMovCtaPP = rdr.GetInt32(IdMovCtaPP)
                            .IdProgramacionPago = rdr.GetInt32(IdProgramacionPago)
                            .FechaPagoProg = rdr.GetDateTime(FechaPagoProg)
                            .MontoPagoProg = rdr.GetDecimal(MontoPagoProg)
                            .IdBanco = rdr.GetInt32(IdBanco)
                            .IdMedioPago = rdr.GetInt32(IdMedioPago)
                            .IdCuentaBancaria = rdr.GetInt32(IdCuentaBancaria)
                            .Observacion = rdr.GetString(Observacion)
                            .MedioPago = rdr.GetString(MedioPago)
                            .Banco = rdr.GetString(Banco)
                            .NroCuentaBancaria = rdr.GetString(NroCuentaBancaria)
                            .IdMoneda = rdr.GetInt32(IdMoneda)
                            .Moneda = rdr.GetString(Moneda)
                            .TipoCambio = rdr.GetDecimal(tipocambio)
                            .NroDocumentoRef = rdr.GetString(NroDocumentoRef)
                            .IdDocumentoRef = rdr.GetInt32(IdDocumentoRef)
                            .idEsDetraccion = rdr.GetBoolean(idEsDetraccion)
                            .idEsRetencion = rdr.GetBoolean(idEsRetencion)
                            .can_NroOperacion = rdr.GetString(can_nrooperacion)
                            .can_montoDeposito = rdr.GetDecimal(can_montoDeposito)
                            .bancoCancelacion = rdr.GetString(bancoCancelacion)
                            .can_fechaPago = rdr.GetString(can_fechaPago)
                            .idConcepto = rdr.GetInt32(IdBanco)
                            .nroVoucher = rdr.GetString(nroVoucher)
                            listaProgramacion.Add(objetoProgramacion)
                        End With
                    End While
                    objeto.listaProgramaciones = listaProgramacion
                    If rdr.NextResult() Then

                        Dim _idMovCtaPP As Integer = rdr.GetOrdinal("idMovCtaPP")
                        Dim _idCuentaProv As Integer = rdr.GetOrdinal("idCuentaProv")
                        Dim _idProveedor As Integer = rdr.GetOrdinal("idProveedor")
                        Dim _idProveedor2 As Integer = rdr.GetOrdinal("idProveedor2")
                        Dim _proveedor As Integer = rdr.GetOrdinal("proveedor")
                        Dim _mcp_Fecha As Integer = rdr.GetOrdinal("mcp_Fecha")
                        Dim _mcp_FechaCanc As Integer = rdr.GetOrdinal("mcp_FechaCanc")
                        Dim _mcp_Monto As Integer = rdr.GetOrdinal("mcp_Monto")
                        Dim _mcp_saldo As Integer = rdr.GetOrdinal("mcp_saldo")
                        Dim _idDocumento As Integer = rdr.GetOrdinal("idDocumento")
                        Dim _idDetalleConcepto As Integer = rdr.GetOrdinal("idDetalleConcepto")
                        Dim _doc_serie As Integer = rdr.GetOrdinal("doc_serie")
                        Dim _doc_codigo As Integer = rdr.GetOrdinal("doc_codigo")
                        Dim _doc_FechaEmision As Integer = rdr.GetOrdinal("doc_FechaEmision")
                        Dim _doc_FechaVenc As Integer = rdr.GetOrdinal("doc_FechaVenc")
                        Dim _doc_TotalAPagar As Integer = rdr.GetOrdinal("doc_TotalAPagar")
                        Dim _idMoneda As Integer = rdr.GetOrdinal("idMoneda")
                        Dim _idEmpresa As Integer = rdr.GetOrdinal("idEmpresa")
                        Dim _tienda As Integer = rdr.GetOrdinal("tienda")
                        Dim _idTienda As Integer = rdr.GetOrdinal("idTienda")
                        Dim _idTipoDocumento As Integer = rdr.GetOrdinal("idTipoDocumento")
                        Dim _tipoDocumento As Integer = rdr.GetOrdinal("tipoDocumento")
                        Dim _edoc_Nombre As Integer = rdr.GetOrdinal("edoc_Nombre")
                        Dim _PagoProgramado As Integer = rdr.GetOrdinal("PagoProgramado")
                        Dim _nroDocumento As Integer = rdr.GetOrdinal("nroDocumento")
                        Dim _nom_simbolo As Integer = rdr.GetOrdinal("mon_Simbolo")
                        Dim _NroItem As Integer = rdr.GetOrdinal("NroItem")
                        Dim _idUsuario As Integer = rdr.GetOrdinal("idUsuario")

                        While rdr.Read()
                            objetoRequerimiento = New Entidades.be_Requerimiento_x_pagar
                            With objetoRequerimiento
                                .idMovCtaPP = rdr.GetInt32(IdMovCtaPP)
                                .idCuentaProv = rdr.GetInt32(_idCuentaProv)
                                .idProveedor = rdr.GetInt32(_idProveedor)
                                .idProveedor2 = rdr.GetInt32(_idProveedor2)
                                .proveedor = rdr.GetString(_proveedor)
                                .mcp_Fecha = rdr.GetDateTime(_mcp_Fecha)
                                .mcp_FechaCanc = rdr.GetString(_mcp_FechaCanc)
                                .mcp_Monto = rdr.GetDecimal(_mcp_Monto)
                                .mcp_saldo = rdr.GetDecimal(_mcp_saldo)
                                .idDocumento = rdr.GetInt32(_idDocumento)
                                .idDetalleConcepto = rdr.GetInt32(_idDetalleConcepto)
                                .doc_serie = rdr.GetString(_doc_serie)
                                .doc_codigo = rdr.GetString(_doc_codigo)
                                .doc_FechaEmision = rdr.GetDateTime(_doc_FechaEmision)
                                .doc_FechaVenc = rdr.GetDateTime(_doc_FechaVenc)
                                .doc_TotalAPagar = rdr.GetDecimal(_doc_TotalAPagar)
                                .idMoneda = rdr.GetInt32(_idMoneda)
                                .idEmpresa = rdr.GetInt32(_idEmpresa)
                                .tienda = rdr.GetString(_tienda)
                                .idTienda = rdr.GetInt32(_idTienda)
                                .idTipoDocumento = rdr.GetInt32(_idTipoDocumento)
                                .tipoDocumento = rdr.GetString(_tipoDocumento)
                                .edoc_Nombre = rdr.GetString(_edoc_Nombre)
                                .PagoProgramado = rdr.GetInt32(_PagoProgramado)
                                .nroDocumento = rdr.GetString(_nroDocumento)
                                .nom_simbolo = rdr.GetString(_nom_simbolo)
                                .NroItem = rdr.GetInt32(_NroItem)
                                .idUsuario = rdr.GetInt32(_idUsuario)
                            End With
                            listaRequerimiento.Add(objetoRequerimiento)
                        End While
                        objeto.listaRequerimientos = listaRequerimiento
                        If rdr.NextResult() Then
                            Dim objetoConcepto As Entidades.Concepto = Nothing
                            Dim listaConcepto As New List(Of Entidades.Concepto)
                            While rdr.Read()
                                objetoConcepto = New Entidades.Concepto
                                With objetoConcepto
                                    .Id = rdr.GetInt32(0)
                                    .Nombre = rdr.GetString(1)
                                    .ConceptoAdelanto = rdr.GetBoolean(2)
                                    .conceptoCtaxRendir = rdr.GetBoolean(3)

                                End With
                                listaConcepto.Add(objetoConcepto)
                            End While
                            objeto.listaConcepto = listaConcepto
                        End If
                        If rdr.NextResult() Then
                            Dim objetoProvisiones As Entidades.Documento = Nothing
                            Dim listaProvisiones As New List(Of Entidades.Documento)
                            While rdr.Read()
                                objetoProvisiones = New Entidades.Documento
                                With objetoProvisiones
                                    .IdDocumento = rdr.GetInt32(0)
                                    .nomproveedor = rdr.GetString(1)
                                    .Codigo = rdr.GetString(2)
                                    .FechaEmision = rdr.GetDateTime(3)
                                    .NomTipoOperacion = rdr.GetString(4)
                                    .NomMoneda = rdr.GetString(5)
                                    .SubTotal = rdr.GetDecimal(6)
                                    .IGV = rdr.GetDecimal(7)
                                    .Total = rdr.GetDecimal(8)
                                    .TotalAPagar = rdr.GetDecimal(9)
                                    .montoPendiente = rdr.GetDecimal(10)
                                    .Detraccion = rdr.GetDecimal(11)
                                    .montoXPagar = rdr.GetDecimal(12)
                                    .MontoOtroTributo = rdr.GetDecimal(13)
                                    .montoAgente = rdr.GetDecimal(14)
                                End With
                                listaProvisiones.Add(objetoProvisiones)
                            End While
                            objeto.listaProvisiones = listaProvisiones
                        End If
                        If rdr.NextResult() Then
                            Dim objetoAplicaciones As Entidades.be_Requerimiento_x_pagar = Nothing
                            Dim listaAplicaciones As New List(Of Entidades.be_Requerimiento_x_pagar)
                            While rdr.Read()
                                objetoAplicaciones = New Entidades.be_Requerimiento_x_pagar
                                With objetoAplicaciones
                                    .idDocumento = rdr.GetInt32(0)
                                    .tipoDocumento = rdr.GetString(1)
                                    .doc_codigo = rdr.GetString(2)
                                    .nom_simbolo = rdr.GetString(3)
                                    .doc_TotalAPagar = rdr.GetDecimal(4)
                                End With
                                listaAplicaciones.Add(objetoAplicaciones)
                            End While
                            objeto.listaAplicaciones = listaAplicaciones
                        End If
                        If rdr.NextResult Then
                            Dim objetoRegimen As Entidades.be_montoRegimen = Nothing
                            Dim listaRegimen As New List(Of Entidades.be_montoRegimen)
                            While rdr.Read()
                                objetoRegimen = New Entidades.be_montoRegimen
                                With objetoRegimen
                                    .idDocumento = rdr.GetInt32(0)
                                    .descripcion = rdr.GetString(1)
                                    .codigo = rdr.GetString(2)
                                    .montoRegimen = rdr.GetDecimal(3)
                                    .tipoRegimen = rdr.GetInt32(4)
                                    .canNroOperacion = rdr.GetString(5)
                                    .montoCancelado = rdr.GetDecimal(6)
                                    .fechaCancelacion = rdr.GetString(7)
                                    .bancoCancelacion = rdr.GetString(8)
                                    .DocumentoRetencion = rdr.GetString(9)
                                End With
                                listaRegimen.Add(objetoRegimen)
                            End While
                            objeto.listaMontoRegimen = listaRegimen
                        End If
                        If rdr.NextResult Then
                            Dim objetoFActurasxAplciar As Entidades.be_Requerimiento_x_pagar = Nothing
                            Dim listaFacturasxAplicar As New List(Of Entidades.be_Requerimiento_x_pagar)

                            Dim ordinal1 As Integer = rdr.GetOrdinal("IdDocumento")
                            Dim ordinal2 As Integer = rdr.GetOrdinal("nomproveedor")
                            Dim ordinal3 As Integer = rdr.GetOrdinal("TotalMonto")
                            Dim ordinal4 As Integer = rdr.GetOrdinal("TotalSaldo")
                            Dim ordinal5 As Integer = rdr.GetOrdinal("TotalAbono")
                            Dim ordinal6 As Integer = rdr.GetOrdinal("NomMoneda")
                            Dim ordinal7 As Integer = rdr.GetOrdinal("doc_Codigo")
                            Dim ordinal8 As Integer = rdr.GetOrdinal("IdTipoDocumento")
                            Dim ordinal9 As Integer = rdr.GetOrdinal("tipodDocumento")
                            Dim ordinal10 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                            While rdr.Read()
                                objetoFActurasxAplciar = New Entidades.be_Requerimiento_x_pagar
                                With objetoFActurasxAplciar
                                    .idDocumento = rdr.GetInt32(ordinal1)
                                    .proveedor = rdr.GetString(ordinal2)
                                    .mcp_Monto = rdr.GetDecimal(ordinal3)
                                    .mcp_saldo = rdr.GetDecimal(ordinal4)
                                    .mcp_abono = rdr.GetDecimal(ordinal5)
                                    .nom_simbolo = rdr.GetString(ordinal6)
                                    .doc_codigo = rdr.GetString(ordinal7)
                                    .idTipoDocumento = rdr.GetInt32(ordinal8)
                                    .tipoDocumento = rdr.GetString(ordinal9)
                                    .doc_FechaEmision = rdr.GetString(ordinal10)
                                End With
                                listaFacturasxAplicar.Add(objetoFActurasxAplciar)
                            End While
                            objeto.listaFacturasxAplicar = listaFacturasxAplicar
                        End If
                        rdr.Close()
                    End If
                End If
            End Using
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return objeto
    End Function

    Public Function SelectxIdProgramacionPago(ByVal IdProgramacionPago_CXP As Integer) As Entidades.ProgramacionPago_CXP

        Dim objProgramacionPagoCXP As Entidades.ProgramacionPago_CXP = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdProgramacionPago_CXP, "@IdProgramacionPagoCXP", SqlDbType.Int)

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ProgramacionPago_CXP_SelectxIdProgPagoCXP", p)

            If (reader.Read) Then

                objProgramacionPagoCXP = New Entidades.ProgramacionPago_CXP

                With objProgramacionPagoCXP

                    .IdMovCtaPP = objDaoMantenedor.UCInt(reader("IdMovCtaPP"))
                    .IdProgramacionPago = objDaoMantenedor.UCInt(reader("IdProgramacionPago"))
                    .FechaPagoProg = objDaoMantenedor.UCDate(reader("pp_FechaPagoProg"))
                    .MontoPagoProg = objDaoMantenedor.UCDec(reader("pp_MontoPagoProg"))
                    .IdBanco = objDaoMantenedor.UCInt(reader("IdBanco"))
                    .IdMedioPago = objDaoMantenedor.UCInt(reader("IdMedioPago"))
                    .IdCuentaBancaria = objDaoMantenedor.UCInt(reader("IdCuentaBancaria"))
                    .Observacion = objDaoMantenedor.UCStr(reader("pp_Observacion"))
                    .MedioPago = objDaoMantenedor.UCStr(reader("mp_Nombre"))
                    .Banco = objDaoMantenedor.UCStr(reader("ban_Nombre"))
                    .NroCuentaBancaria = objDaoMantenedor.UCStr(reader("cb_numero"))
                    .TipoCambio = objDaoMantenedor.UCDec(reader("tipocambio"))
                    .esAdelanto = objDaoMantenedor.UCBool(reader("esAdelanto"))
                    .esCtaPorRendir = objDaoMantenedor.UCBool(reader("esCtaPorRendir"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .Moneda = objDaoMantenedor.UCStr(reader("mon_Simbolo"))

                End With

            End If

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objProgramacionPagoCXP

    End Function

    Public Function buscarfacturasPorAplicar(ByVal cn As SqlConnection, ByVal objetoprogr As Entidades.be_Requerimiento_x_pagar) As List(Of Entidades.be_Requerimiento_x_pagar)
        Dim lista As List(Of Entidades.be_Requerimiento_x_pagar) = Nothing
        Using cmd As New SqlCommand("SP_BUSCAR_FACTURAS_X_APLICAR", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@idProveedor", SqlDbType.Int)).Value = objetoprogr.idProveedor
                .Parameters.Add(New SqlParameter("@doc_serie", SqlDbType.NVarChar)).Value = objetoprogr.doc_serie
                .Parameters.Add(New SqlParameter("@doc_codigo", SqlDbType.NVarChar)).Value = objetoprogr.doc_codigo
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_Requerimiento_x_pagar)
                Dim ordinal1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ordinal2 As Integer = rdr.GetOrdinal("nomproveedor")
                Dim ordinal3 As Integer = rdr.GetOrdinal("TotalMonto")
                Dim ordinal4 As Integer = rdr.GetOrdinal("TotalSaldo")
                Dim ordinal5 As Integer = rdr.GetOrdinal("TotalAbono")
                Dim ordinal6 As Integer = rdr.GetOrdinal("NomMoneda")
                Dim ordinal7 As Integer = rdr.GetOrdinal("doc_Codigo")
                Dim ordinal8 As Integer = rdr.GetOrdinal("IdTipoDocumento")
                Dim ordinal9 As Integer = rdr.GetOrdinal("tipodDocumento")
                Dim ordinal10 As Integer = rdr.GetOrdinal("doc_FechaEmision")

                Dim objeto As Entidades.be_Requerimiento_x_pagar = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_Requerimiento_x_pagar
                    objeto.idDocumento = rdr.GetInt32(ordinal1)
                    objeto.proveedor = rdr.GetString(ordinal2)
                    objeto.mcp_Monto = rdr.GetDecimal(ordinal3)
                    objeto.mcp_saldo = rdr.GetDecimal(ordinal4)
                    objeto.mcp_abono = rdr.GetDecimal(ordinal5)
                    objeto.nom_simbolo = rdr.GetString(ordinal6)
                    objeto.doc_codigo = rdr.GetString(ordinal7)
                    objeto.idTipoDocumento = rdr.GetInt32(ordinal8)
                    objeto.tipoDocumento = rdr.GetString(ordinal9)
                    objeto.doc_FechaEmision = rdr.GetString(ordinal10)
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function
End Class

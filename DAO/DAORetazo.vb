'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAORetazo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaRetazo(ByVal retazo As Entidades.Retazo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = retazo.IdProducto
        ArrayParametros(1) = New SqlParameter("@ret_Escalar", SqlDbType.Decimal)
        ArrayParametros(1).Value = retazo.Escalar
        ArrayParametros(2) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(2).Value = retazo.IdUnidadMedida
        ArrayParametros(3) = New SqlParameter("@ret_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = retazo.Estado
        Return HDAO.Insert(cn, "_RetazoInsert", ArrayParametros)
    End Function
    Public Function ActualizaRetazo(ByVal retazo As Entidades.Retazo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = retazo.IdProducto
        ArrayParametros(1) = New SqlParameter("@ret_Escalar", SqlDbType.Decimal)
        ArrayParametros(1).Value = retazo.Escalar
        ArrayParametros(2) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(2).Value = retazo.IdUnidadMedida
        ArrayParametros(3) = New SqlParameter("@ret_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = retazo.Estado
        ArrayParametros(4) = New SqlParameter("@IdRetazo", SqlDbType.Int)
        ArrayParametros(4).Value = retazo.IdRetazo
        Return HDAO.Update(cn, "_RetazoUpdate", ArrayParametros)
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 30-Set-2009
'Hora    : 09:30 am
'*************************************************
Imports System.Data.SqlClient
Public Class DAOTipoDocumento_TipoOperacion
    Dim HDAO As New DAO.HelperDAO
    Private objConexion As New DAO.Conexion

    Public Sub InsertaTipoDocumento_TipoOperacion(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.TipoDocumento_TipoOperacion), ByVal IdTipoDocumento As Integer)

        Dim fila As New Entidades.TipoDocumento_TipoOperacion

        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = IdTipoDocumento
        For Each fila In lista
            ArrayParametros(1) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            ArrayParametros(1).Value = fila.IdTipoOperacion
            HDAO.InsertaT(cn, "_TipoDocumento_TipoOperacionInsert", ArrayParametros, tr)
        Next
    End Sub
    'Actualizar una GRILLA
    Public Function ActualizaTipoDocumento_TipoOperacion(ByVal UpdateTipoDocOpera As List(Of Entidades.TipoDocumento_TipoOperacion)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim TipoDocOpera As New Entidades.TipoDocumento_TipoOperacion
        For Each TipoDocOpera In UpdateTipoDocOpera
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            ArrayParametros(0).Value = TipoDocOpera.IdTipoDocumento
            ArrayParametros(1) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            ArrayParametros(1).Value = TipoDocOpera.IdTipoOperacion
            Return HDAO.Update(cn, "_TipoDocumento_TipoOperacionUpdate", ArrayParametros)
        Next
    End Function

    Public Function DeletexIdDocumento(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idDocumento As Integer) As Boolean
        Try

            Dim cmd As New SqlCommand("_TipoDocumento_TipoOperacionBorrar", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idTipoDocumento", idDocumento)
            cmd.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try

    End Function

    'Para EDITAR la TipoDocumento_TipoOperacion mediante el IdDocumento
    Public Function _TipoDocumento_TipoOperacionSelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.TipoDocumento_TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumento_TipoOperacionSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idTipoDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TipoDocumento_TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumento_TipoOperacion
                    obj.IdTipoDocumento = CInt(lector.Item("IdTipoDocumento"))
                    obj.IdTipoOperacion = CInt(lector.Item("IdTipoOperacion"))
                    obj.Nombre = CStr(lector.Item("top_Nombre"))
                    lista.Add(obj)
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboTipoOperacion() As List(Of Entidades.TipoDocumento_TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("TipoDocumento_TipoOperacionSelectCboTipoOreacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumento_TipoOperacion)
                Do While lector.Read
                    Dim TipoDocumento As New Entidades.TipoDocumento_TipoOperacion
                    TipoDocumento.IdTipoDocumento = CInt(lector.Item("IdTipoDocumento"))
                    TipoDocumento.Nombre = CStr(lector.Item("Nombre"))
                    Lista.Add(TipoDocumento)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class DAOConcepto_TipoDocumento
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Dim objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub InsertaListaConcepto_TipoDocumento(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.Concepto_TipoDocumento), ByVal IdConcepto As Integer)
        Dim fila As New Entidades.Concepto_TipoDocumento
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        ArrayParametros(0).Value = IdConcepto
        'For Each ----> Para Cada Uno
        For Each fila In lista
            ArrayParametros(1) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(fila.IdTipoDocumento = Nothing, DBNull.Value, fila.IdTipoDocumento)
            ArrayParametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(fila.IdMoneda = Nothing, DBNull.Value, fila.IdMoneda)
            ArrayParametros(3) = New SqlParameter("@ctdoc_Valor", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(fila.CtDoc_Valor = Nothing, DBNull.Value, fila.CtDoc_Valor)
            ArrayParametros(4) = New SqlParameter("@con_FletexKiloxFleteFijo", SqlDbType.Bit)
            ArrayParametros(4).Value = IIf(fila.fletexkilofijo = Nothing Or fila.fletexkilofijo = "NULL", DBNull.Value, fila.fletexkilofijo)
            ArrayParametros(5) = New SqlParameter("@con_TipoCalculo", SqlDbType.Bit)
            ArrayParametros(5).Value = IIf(fila.con_TipoCalculo = Nothing Or fila.con_TipoCalculo = "NULL", DBNull.Value, fila.con_TipoCalculo)
            ArrayParametros(6) = New SqlParameter("@con_ZonaUbigeo", SqlDbType.Bit)
            ArrayParametros(6).Value = IIf(fila.con_ZonaUbigeo = Nothing, False, fila.con_ZonaUbigeo)

            HDAO.InsertaT(cn, "_Concepto_TipoDocumentoInsert", ArrayParametros, tr)
        Next
    End Sub
    Public Function DeletexCpto_TipoDoc(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdConcepto As Integer) As Boolean
        Try 'Tratar
            Dim cmd As New SqlCommand("_Concepto_TipoDocumentoBorrar", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdConcepto", IdConcepto)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception 'Catch ->Capturar; As  --> Como
            Return False
        Finally

        End Try
    End Function
    'Para EDITAR la Tipo de Documento mediante el IdConcepto
    Public Function _Concepto_TipoDocumentoEditarxIdConcepto(ByVal IdConcepto As Integer) As List(Of Entidades.Concepto_TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Concepto_TipoDocumentoEditarxIdConcepto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("IdConcepto", IdConcepto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Concepto_TipoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto_TipoDocumento
                    With obj
                        .IdConcepto = CInt(lector.Item("IdConcepto"))
                        .IdTipoDocumento = CInt(lector.Item("IdTipoDocumento"))
                        .DescTipoDocumento = CStr(lector.Item("tdoc_NombreLargo"))
                        .IdMoneda = CInt(lector("IdMoneda"))
                        .CtDoc_Valor = CDec(IIf(IsDBNull(lector("ctdoc_Valor")), 0, lector("ctdoc_Valor")))
                        .cadMoneda = CStr(IIf(IsDBNull(lector("cadMoneda")), 0, lector("cadMoneda")))
                        .fletexkilofijo = CStr(IIf(IsDBNull(lector("fletexkilofijo")), "NULL", lector("fletexkilofijo")))
                        .con_TipoCalculo = CStr(IIf(IsDBNull(lector("con_TipoCalculo")), "NULL", lector("con_TipoCalculo")))
                        .con_ZonaUbigeo = CBool(IIf(IsDBNull(lector("con_ZonaUbigeo")), False, lector("con_ZonaUbigeo")))
                    End With
                    lista.Add(obj)
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw New Exception
        Finally

        End Try
    End Function
    Public Function listarMoneda() As List(Of Entidades.Concepto_TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_listarMoneda", cn)
                cmd.CommandType = CommandType.StoredProcedure
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto_TipoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto_TipoDocumento
                    obj.IdMoneda = CInt(lector("Id"))
                    obj.SimboloMoneda = CStr(IIf(IsDBNull(lector("Moneda")), "", lector("Moneda")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function SelectCboxIdTipoDocumento(ByVal IdTipoDocumento As Integer) As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectCboxIdTipoDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    With obj
                        .Id = CInt(IIf(IsDBNull(lector("IdConcepto")) = True, 0, lector("IdConcepto")))
                        .Nombre = CStr(IIf(IsDBNull(lector("con_Nombre")) = True, "", lector("con_Nombre")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectConceptoTipoDocxIdTipoGastoxNomConcepto(ByVal IdTipoGasto As Integer, ByVal NomConcepto As String, _
                                                                  ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.Concepto_TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoTipoDocSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoGasto", IdTipoGasto)
        cmd.Parameters.AddWithValue("@NomConcepto", NomConcepto)
        cmd.Parameters.AddWithValue("@pageindex", pageindex)
        cmd.Parameters.AddWithValue("@pageSize", pagesize)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Concepto_TipoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto_TipoDocumento
                    With obj
                        .IdConcepto = CInt(lector.Item("IdConcepto"))
                        .NomConcepto = CStr(IIf(IsDBNull(lector("con_Nombre")), "", lector("con_Nombre")))
                        .IdMoneda = CInt(lector("IdMoneda"))
                        .SimboloMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")), "", lector("mon_Simbolo")))
                        .CtDoc_Valor = CDec(IIf(IsDBNull(lector("ctdoc_Valor")), 0, lector("ctdoc_Valor")))
                    End With
                    lista.Add(obj)
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw New Exception
        End Try
    End Function

    Public Function SelectConceptoDetraccionxIdTipoDocumento(ByVal IdTipoDocumento As Integer) As List(Of Entidades.Concepto)

        Dim lista As New List(Of Entidades.Concepto)
        Dim cn As SqlConnection = (objConexion).ConexionSIGE

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdTipoDocumento, "@IdTipoDocumento", SqlDbType.Int)

            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Concepto_SelectConceptoDetraccionxIdTipoDocumento", p)
            While (reader.Read)

                Dim obj As New Entidades.Concepto

                With obj

                    .Id = objDaoMantenedor.UCInt(reader("IdConcepto"))
                    .Nombre = objDaoMantenedor.UCStr(reader("con_Nombre"))
                    .Moneda = objDaoMantenedor.UCStr(reader("Moneda"))
                    .PorcentDetraccion = objDaoMantenedor.UCDec(reader("con_PorcentDetraccion"))
                    .MontoMinimoDetraccion = objDaoMantenedor.UCDec(reader("con_MontoMinDetraccion"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))

                End With

                lista.Add(obj)


            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function



End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'******************** MARTES 19 MAYO 2010 HORA 11_35 AM
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOCuentaBancaria
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private reader As SqlDataReader


    Public Function VerificarsiExisteCB(ByVal IdBanco As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer) As Entidades.CuentaBancaria

        Dim objCuentaBancaria As Entidades.CuentaBancaria = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdBanco, "@IdBanco", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
            cn = objConexion.ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "VerificarCuentaBancaria", p)
            If (reader.HasRows = False) Then
                objCuentaBancaria = New Entidades.CuentaBancaria
                With objCuentaBancaria
                    objCuentaBancaria.IdCuentaBancaria = 0
                End With
            Else
                If (reader.Read) Then
                    objCuentaBancaria = New Entidades.CuentaBancaria
                    With objCuentaBancaria
                        .IdCuentaBancaria = objDaoMantenedor.UCInt(reader("IdCuentaBancaria"))
                    End With
                End If
            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objCuentaBancaria

    End Function

    Public Function VerificarsiExisteCBDetraccion(ByVal IdBanco As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer) As Entidades.CuentaBancaria

        Dim objCuentaBancaria As Entidades.CuentaBancaria = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdBanco, "@IdBanco", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
            cn = objConexion.ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "VerificarCuentaBancariaDetraccion", p)
            If (reader.HasRows = False) Then
                objCuentaBancaria = New Entidades.CuentaBancaria
                With objCuentaBancaria
                    objCuentaBancaria.IdCuentaBancaria = 0
                End With
            Else
                If (reader.Read) Then
                    objCuentaBancaria = New Entidades.CuentaBancaria
                    With objCuentaBancaria
                        .IdCuentaBancaria = objDaoMantenedor.UCInt(reader("IdCuentaBancaria"))
                    End With
                End If
            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objCuentaBancaria

    End Function

    Public Function SelectxIdBancoxIdCuentaBancaria(ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As Entidades.CuentaBancaria

        Dim objCuentaBancaria As Entidades.CuentaBancaria = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = objDaoMantenedor.getParam(IdBanco, "@IdBanco", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdCuentaBancaria, "@IdCuentaBancaria", SqlDbType.Int)

            cn = objConexion.ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_CuentaBancaria_SelectxIdBancoxIdCuentaBancaria", p)
            If (reader.Read) Then

                objCuentaBancaria = New Entidades.CuentaBancaria

                With objCuentaBancaria

                    .IdBanco = objDaoMantenedor.UCInt(reader("IdBanco"))
                    .IdPersona = objDaoMantenedor.UCInt(reader("IdPersona"))
                    .IdCuentaBancaria = objDaoMantenedor.UCInt(reader("IdCuentaBancaria"))
                    .NroCuentaBancaria = objDaoMantenedor.UCStr(reader("cb_numero"))
                    .NroCuentaContable = objDaoMantenedor.UCStr(reader("cb_CuentaContable"))
                    .NroCuentaInterbanca = objDaoMantenedor.UCStr(reader("cb_CuentaInterbancaria"))
                    .SwiftBancaria = objDaoMantenedor.UCStr(reader("cb_SwiftBancario"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .Estado = objDaoMantenedor.UCStr(reader("cb_Estado"))

                End With


            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objCuentaBancaria

    End Function

    Public Function SelectActivoxIdPersona_DT(ByVal IdPersona As Integer) As DataTable

        Dim ds As New DataSet

        Try

            cn = objConexion.ConexionSIGE

            cmd = New SqlCommand("_CuentaBancaria_SelectActivoxIdPersona", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdPersona", IdPersona)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CuentaBancaria2")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_CuentaBancaria2")

    End Function

    Public Function SelectCboBanco(ByVal IdEmpresa As Integer) As List(Of Entidades.Banco)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.Banco)
        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)

            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_CuentaBancaria_SelectCboBanco", p)
            While (reader.Read)
                Dim obj As New Entidades.Banco

                With obj

                    .Id = objDaoMantenedor.UCInt(reader("IdBanco"))
                    .Nombre = objDaoMantenedor.UCStr(reader("ban_Nombre"))

                End With

                lista.Add(obj)
            End While
            reader.Close()


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

    Public Function InsertaCuentaBancaria(ByVal cuentabancaria As Entidades.CuentaBancaria, ByVal cn As SqlConnection, ByVal t As SqlTransaction, Optional ByVal esCuentaDetraccion As Boolean = False) As Boolean


        Dim p() As SqlParameter = New SqlParameter(8) {}
        p(0) = objDaoMantenedor.getParam(cuentabancaria.IdBanco, "@IdBanco", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(cuentabancaria.IdPersona, "@IdPersona", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(cuentabancaria.NroCuentaBancaria, "@cb_numero", SqlDbType.VarChar)
        p(3) = objDaoMantenedor.getParam(cuentabancaria.NroCuentaContable, "@cb_CuentaContable", SqlDbType.VarChar)
        p(4) = objDaoMantenedor.getParam(cuentabancaria.IdMoneda, "@IdMoneda", SqlDbType.Int)
        p(5) = objDaoMantenedor.getParam(cuentabancaria.Estado, "@cb_Estado", SqlDbType.Char)
        p(6) = objDaoMantenedor.getParam(cuentabancaria.NroCuentaInterbanca, "@cb_CuentaInterbancaria", SqlDbType.VarChar)
        p(7) = objDaoMantenedor.getParam(cuentabancaria.SwiftBancaria, "@cb_SwiftBancario", SqlDbType.VarChar)
        p(8) = objDaoMantenedor.getParam(esCuentaDetraccion, "@cuentaDetraccion", SqlDbType.Bit)

        SqlHelper.ExecuteNonQuery(t, CommandType.StoredProcedure, "_CuentaBancariaInsert", p)

        Return True

    End Function
    Public Function InsertaCuentaBancariaDetraccion(ByVal cuentabancaria As Entidades.CuentaBancaria, ByVal cn As SqlConnection, ByVal t As SqlTransaction, Optional ByVal esCuentaDetraccion As Boolean = False) As Boolean


        Dim p() As SqlParameter = New SqlParameter(8) {}
        p(0) = objDaoMantenedor.getParam(cuentabancaria.IdBanco, "@IdBanco", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(cuentabancaria.IdPersona, "@IdPersona", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(cuentabancaria.NroCuentaBancaria, "@cb_numero", SqlDbType.VarChar)
        p(3) = objDaoMantenedor.getParam(cuentabancaria.NroCuentaContable, "@cb_CuentaContable", SqlDbType.VarChar)
        p(4) = objDaoMantenedor.getParam(cuentabancaria.IdMoneda, "@IdMoneda", SqlDbType.Int)
        p(5) = objDaoMantenedor.getParam(cuentabancaria.Estado, "@cb_Estado", SqlDbType.Char)
        p(6) = objDaoMantenedor.getParam(cuentabancaria.NroCuentaInterbanca, "@cb_CuentaInterbancaria", SqlDbType.VarChar)
        p(7) = objDaoMantenedor.getParam(cuentabancaria.SwiftBancaria, "@cb_SwiftBancario", SqlDbType.VarChar)
        p(8) = objDaoMantenedor.getParam(cuentabancaria.esCuentaDetraccion, "@cuentaDetraccion", SqlDbType.VarChar)

        SqlHelper.ExecuteNonQuery(t, CommandType.StoredProcedure, "_CuentaBancariaInsertDetraccion", p)

        Return True

    End Function


    Public Function ActualizaCuentaBancaria(ByVal cuentabancaria As Entidades.CuentaBancaria, ByVal cn As SqlConnection, ByVal t As SqlTransaction) As Boolean
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
            ArrayParametros(0) = New SqlParameter("@cb_numero", SqlDbType.VarChar)
            ArrayParametros(0).Value = cuentabancaria.NroCuentaBancaria
            ArrayParametros(1) = New SqlParameter("@cb_CuentaContable", SqlDbType.VarChar)
            ArrayParametros(1).Value = cuentabancaria.NroCuentaContable
            ArrayParametros(2) = New SqlParameter("@cb_Estado", SqlDbType.Char)
            ArrayParametros(2).Value = cuentabancaria.Estado
            ArrayParametros(3) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
            ArrayParametros(3).Value = cuentabancaria.IdCuentaBancaria
            ArrayParametros(4) = New SqlParameter("@cb_CuentaInterbancaria", SqlDbType.VarChar)
            ArrayParametros(4).Value = cuentabancaria.NroCuentaInterbanca
            ArrayParametros(5) = New SqlParameter("@cb_SwiftBancario", SqlDbType.VarChar)
            ArrayParametros(5).Value = cuentabancaria.SwiftBancaria

            HDAO.UpdateT(cn, "_updatecuentaBancaria", ArrayParametros, t)
            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ActualizaBancoProgramacion(ByVal idprogramacion As Integer, ByVal idBanco As Integer, ByVal idcuenta As Integer, ByVal cn As SqlConnection, ByVal t As SqlTransaction) As Boolean
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            ArrayParametros(0) = New SqlParameter("@idprogramacion", SqlDbType.Int)
            ArrayParametros(0).Value = idprogramacion
            ArrayParametros(1) = New SqlParameter("@idBanco", SqlDbType.Int)
            ArrayParametros(1).Value = idBanco
            ArrayParametros(2) = New SqlParameter("@idCuenta", SqlDbType.Int)
            ArrayParametros(2).Value = idcuenta

            HDAO.UpdateT(cn, "USP_ACTUALIZAR_BANCO_PROGRAMACION", ArrayParametros, t)
            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function DeleteVoucher(ByVal idprogramacion As Integer, ByVal cn As SqlConnection, ByVal t As SqlTransaction) As Boolean
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
            ArrayParametros(0) = New SqlParameter("@idprogramacion", SqlDbType.Int)
            ArrayParametros(0).Value = idprogramacion
           

            HDAO.UpdateT(cn, "USP_DELETE_VOUCHER", ArrayParametros, t)
            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function




    Public Function SelectCboxIdBancoxIdPersona(ByVal IdBanco As Integer, ByVal IdPersona As Integer) As List(Of Entidades.CuentaBancaria)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaBancariaSelectCboxIdBancoxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdBanco", IdBanco)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaBancaria)
                Do While lector.Read
                    Dim CuentaBancaria As New Entidades.CuentaBancaria
                    CuentaBancaria.IdCuentaBancaria = CInt(lector.Item("IdCuentaBancaria"))
                    CuentaBancaria.NroCuentaBancaria = CStr(lector.Item("cb_numero"))
                    Lista.Add(CuentaBancaria)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.CuentaBancaria)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaBancariaSelectCboxIdBanco", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdBanco", IdBanco)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaBancaria)
                Do While lector.Read
                    Dim CuentaBancaria As New Entidades.CuentaBancaria
                    CuentaBancaria.IdCuentaBancaria = CInt(lector.Item("IdCuentaBancaria"))
                    CuentaBancaria.NroCuentaBancaria = CStr(lector.Item("cb_numero"))
                    Lista.Add(CuentaBancaria)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectcuentaBancaria(ByVal idempresa As Integer, ByVal idbanco As Integer, ByVal estado As Int16, ByVal idmoneda As Int16) As List(Of Entidades.CuentaBancaria)
        Dim lista As List(Of Entidades.CuentaBancaria) = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_selectCtaBancaria", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idempresa", idempresa)
        cmd.Parameters.AddWithValue("@idbanco", idbanco)
        cmd.Parameters.AddWithValue("@estado", estado)
        cmd.Parameters.AddWithValue("@idmoneda", idmoneda)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If lector IsNot Nothing Then
                lista = New List(Of Entidades.CuentaBancaria)

                Dim idCuentaBancaria As Integer = lector.GetOrdinal("idcuentaBancaria")
                Dim IdBancoxxx As Integer = lector.GetOrdinal("idbanco")
                Dim Banco As Integer = lector.GetOrdinal("Banco")
                Dim Empresa As Integer = lector.GetOrdinal("Empresa")
                Dim Moneda As Integer = lector.GetOrdinal("Moneda")
                Dim IdMonedaxxx As Integer = lector.GetOrdinal("IdMoneda")
                Dim cb_numero As Integer = lector.GetOrdinal("cb_numero")
                Dim cb_CuentaContable As Integer = lector.GetOrdinal("cb_CuentaContable")
                Dim Estadoxxx As Integer = lector.GetOrdinal("Estado")
                Dim cb_CuentaInterbancaria As Integer = lector.GetOrdinal("cb_CuentaInterbancaria")
                Dim cb_SwiftBancario As Integer = lector.GetOrdinal("cb_SwiftBancario")
                Dim esCuentaDetraccion As Integer = lector.GetOrdinal("esCuentaDetraccion")

                Dim cuentabancaria As Entidades.CuentaBancaria = Nothing

                While lector.Read()
                    cuentabancaria = New Entidades.CuentaBancaria
                    With cuentabancaria


                        .IdCuentaBancaria = lector.GetInt32(idCuentaBancaria)
                        .IdBanco = lector.GetInt32(IdBancoxxx)
                        .Banco = lector.GetString(Banco)
                        .Empresa = lector.GetString(Empresa)
                        .MonSimbolo = lector.GetString(Moneda)
                        .IdMoneda = lector.GetInt32(IdMonedaxxx)
                        .NroCuentaBancaria = lector.GetString(cb_numero)
                        .NroCuentaContable = lector.GetString(cb_CuentaContable)
                        .Estado = lector.GetString(Estadoxxx)
                        .NroCuentaInterbanca = lector.GetString(cb_CuentaInterbancaria)
                        .SwiftBancaria = lector.GetString(cb_SwiftBancario)
                        .esCuentaDetraccion = lector.GetString(esCuentaDetraccion)
                    End With
                    lista.Add(cuentabancaria)
                End While
                lector.Close()
            End If
            Return lista
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Sub DeleteCuentaBancaria(ByVal idcuentaBancaria As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("deleteCuentaBancaria", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idcuentabancaria", idcuentaBancaria)

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Public Function existeCuentaDetraccion(ByVal idProveedor As Integer) As Integer
        Dim cn As SqlConnection = (New Conexion).ConexionSIGE
        Dim flagCuentaDetraccion As Boolean = 0
        Using cmd As New SqlCommand("SP_DROPDOWNLIST_FILTRO", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@FITLRO", SqlDbType.VarChar)).Value = idProveedor
                .Parameters.Add(New SqlParameter("@TABLA", SqlDbType.VarChar)).Value = "CUENTA_DETRACCION"
            End With
            cn.Open()
            flagCuentaDetraccion = cmd.ExecuteScalar()
        End Using
        Return flagCuentaDetraccion
    End Function
End Class

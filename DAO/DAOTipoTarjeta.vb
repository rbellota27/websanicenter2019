﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 29/01/2010 6:50 pm
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOTipoTarjeta

    Private objConexion As New Conexion
    Private Cn As SqlConnection '= objConexion.ConexionSIGE()
    Private Tr As SqlTransaction

    Public Function SelectCboxIdCaja(ByVal IdCaja As Integer) As List(Of Entidades.TipoTarjeta)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(0) {}

        Prm(0) = New SqlParameter("@IdCaja", SqlDbType.Int)
        Prm(0).Value = IdCaja

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "[_TipoTarjetaxIdCaja]", Prm)

                Dim Lista As New List(Of Entidades.TipoTarjeta)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTarjeta

                    obj.Id = CInt(lector.Item("IdTipoTarjeta"))
                    obj.Nombre = CStr(lector.Item("ttarjeta_nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function InsertT_GetID(ByVal x As Entidades.TipoTarjeta) As Integer
        'InsertT Insert con Transacciones
        Cn = objConexion.ConexionSIGE()
        Dim resul As Integer

        Dim Prm() As SqlParameter = New SqlParameter(1) {}
        'Prm(0) = New SqlParameter("@ttarjeta_nombre", x.Nombre)

        Prm(0) = New SqlParameter("@ttarjeta_nombre", SqlDbType.VarChar)
        Prm(0).Value = x.Nombre
        Prm(1) = New SqlParameter("@ttarjeta_estado", SqlDbType.Bit)
        Prm(1).Value = x.Estado

        Cn.Open()
        Tr = Cn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteScalar(Tr, CommandType.StoredProcedure, "_TipoTarjetaInsertGetID", Prm))

            Tr.Commit()
        Catch e As Exception
            Tr.Rollback()
            Throw (e)
        Finally
            Cn.Close()
            Cn.Dispose()
        End Try
        Return (resul)
    End Function
    Public Function InsertT(ByVal x As Entidades.TipoTarjeta) As Boolean
        If InsertT_GetID(x) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function UpdateT(ByVal x As Entidades.TipoTarjeta) As Boolean
        'UpdateT Update con Transacciones
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(2) {}

        Prm(0) = New SqlParameter("@IdTipoTarjeta", SqlDbType.Int)
        Prm(0).Value = x.Id
        Prm(1) = New SqlParameter("@ttarjeta_nombre", SqlDbType.VarChar)
        Prm(1).Value = x.Nombre
        Prm(2) = New SqlParameter("@ttarjeta_estado", SqlDbType.Bit)
        Prm(2).Value = x.Estado

        Cn.Open()
        Tr = Cn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(Tr, CommandType.StoredProcedure, "_TipoTarjetaUpdate", Prm)
            Tr.Commit()
            Return True
        Catch e As Exception
            Tr.Rollback()
            Throw (e)
            Return False
        Finally
            Cn.Close()
            Cn.Dispose()

        End Try
    End Function
    Public Function SelectxParams(ByVal x As Entidades.TipoTarjeta) As List(Of Entidades.TipoTarjeta)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(2) {}

        Prm(0) = New SqlParameter("@IdTipoTarjeta", SqlDbType.Int)
        Prm(0).Value = x.Id
        Prm(1) = New SqlParameter("@ttarjeta_nombre", SqlDbType.VarChar)
        Prm(1).Value = x.Nombre
        Prm(2) = New SqlParameter("@ttarjeta_Estado", SqlDbType.Bit)
        Prm(2).Value = x.Estado


        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_TipoTarjetaSelectxParams", Prm)

                Dim Lista As New List(Of Entidades.TipoTarjeta)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTarjeta

                    obj.Id = CInt(lector.Item("IdTipoTarjeta"))
                    obj.Nombre = CStr(lector.Item("ttarjeta_nombre"))
                    obj.Estado = CBool(lector.Item("ttarjeta_estado"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.TipoTarjeta)

        Dim x As New Entidades.TipoTarjeta

        Return SelectxParams(x)
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.TipoTarjeta)
        'Esto es utilizado tambien como SelectCbo
        Dim x As New Entidades.TipoTarjeta
        x.Estado = True
        Return SelectxParams(x)
    End Function

    Public Function SelectAllInactivo() As List(Of Entidades.TipoTarjeta)
        Dim x As New Entidades.TipoTarjeta
        x.Estado = False
        Return SelectxParams(x)
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOCampania_DetalleTG

    Dim cmd As SqlCommand
    Dim list_Campania_DetalleTG As List(Of Entidades.Campania_DetalleTG)
    Dim obj_Campania_DetalleTG As Entidades.Campania_DetalleTG
    Dim objConexion As New DAO.Conexion

    Public Sub Campania_DetalleTG_TransactionALL(ByVal objCampania_DetalleTG As Entidades.Campania_DetalleTG, ByVal sqlCN As SqlConnection, ByVal sqlTR As SqlTransaction)

        Dim Parameter() As SqlParameter = New SqlParameter(6) {}

        Parameter(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        Parameter(0).Value = IIf(objCampania_DetalleTG.IdProducto = Nothing, DBNull.Value, objCampania_DetalleTG.IdProducto)

        Parameter(1) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        Parameter(1).Value = IIf(objCampania_DetalleTG.IdUnidadMedida = Nothing, DBNull.Value, objCampania_DetalleTG.IdUnidadMedida)

        Parameter(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        Parameter(2).Value = IIf(objCampania_DetalleTG.IdMoneda = Nothing, DBNull.Value, objCampania_DetalleTG.IdMoneda)

        Parameter(3) = New SqlParameter("@cdtg_Precio", SqlDbType.Decimal)
        Parameter(3).Value = IIf(objCampania_DetalleTG.cdtg_Precio = Nothing, DBNull.Value, objCampania_DetalleTG.cdtg_Precio)

        Parameter(4) = New SqlParameter("@cdtg_Cantidad", SqlDbType.Decimal)
        Parameter(4).Value = IIf(objCampania_DetalleTG.cdtg_Cantidad = Nothing, DBNull.Value, objCampania_DetalleTG.cdtg_Cantidad)

        Parameter(5) = New SqlParameter("@IdCampania", SqlDbType.Int)
        Parameter(5).Value = IIf(objCampania_DetalleTG.IdCampania = Nothing, DBNull.Value, objCampania_DetalleTG.IdCampania)

        Parameter(6) = New SqlParameter("@cdtg_cantCampania", SqlDbType.Int)
        Parameter(6).Value = IIf(objCampania_DetalleTG.cdtg_cantCampania = Nothing, DBNull.Value, objCampania_DetalleTG.cdtg_cantCampania)


        Dim sqlCMD As New SqlCommand("_Campania_DetalleTGALL", sqlCN, sqlTR)
        sqlCMD.CommandType = CommandType.StoredProcedure
        sqlCMD.Parameters.AddRange(Parameter)
        Try
            sqlCMD.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
        End Try

    End Sub

    Public Sub Campania_DetalleTG_Transaction(ByVal objCampania_DetalleTG As Entidades.Campania_DetalleTG, ByVal sqlCN As SqlConnection, ByVal sqlTR As SqlTransaction)

        Dim Parameter() As SqlParameter = New SqlParameter(7) {}

        Parameter(0) = New SqlParameter("@IdCampaniaDetalle", SqlDbType.Int)
        Parameter(0).Value = IIf(objCampania_DetalleTG.IdCampaniaDetalle = Nothing, DBNull.Value, objCampania_DetalleTG.IdCampaniaDetalle)

        Parameter(1) = New SqlParameter("@IdProducto", SqlDbType.Int)
        Parameter(1).Value = IIf(objCampania_DetalleTG.IdProducto = Nothing, DBNull.Value, objCampania_DetalleTG.IdProducto)

        Parameter(2) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        Parameter(2).Value = IIf(objCampania_DetalleTG.IdUnidadMedida = Nothing, DBNull.Value, objCampania_DetalleTG.IdUnidadMedida)

        Parameter(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        Parameter(3).Value = IIf(objCampania_DetalleTG.IdMoneda = Nothing, DBNull.Value, objCampania_DetalleTG.IdMoneda)

        Parameter(4) = New SqlParameter("@cdtg_Precio", SqlDbType.Decimal)
        Parameter(4).Value = IIf(objCampania_DetalleTG.cdtg_Precio = Nothing, DBNull.Value, objCampania_DetalleTG.cdtg_Precio)

        Parameter(5) = New SqlParameter("@cdtg_Cantidad", SqlDbType.Decimal)
        Parameter(5).Value = IIf(objCampania_DetalleTG.cdtg_Cantidad = Nothing, DBNull.Value, objCampania_DetalleTG.cdtg_Cantidad)

        Parameter(6) = New SqlParameter("@IdCampania", SqlDbType.Int)
        Parameter(6).Value = IIf(objCampania_DetalleTG.IdCampania = Nothing, DBNull.Value, objCampania_DetalleTG.IdCampania)

        Parameter(7) = New SqlParameter("@cdtg_cantCampania", SqlDbType.Bit)
        Parameter(7).Value = IIf(objCampania_DetalleTG.cdtg_cantCampania = Nothing, DBNull.Value, objCampania_DetalleTG.cdtg_cantCampania)


        Dim sqlCMD As New SqlCommand("_Campania_DetalleTG", sqlCN, sqlTR)
        sqlCMD.CommandType = CommandType.StoredProcedure
        sqlCMD.Parameters.AddRange(Parameter)
        Try
            sqlCMD.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally            
        End Try

    End Sub

    Public Function Campania_DetalleTGxIdCampaniaDetalle(ByVal IdCampaniaDetalle As Integer) As List(Of Entidades.Campania_DetalleTG)

        list_Campania_DetalleTG = New List(Of Entidades.Campania_DetalleTG)

        Dim cn As SqlConnection = (New Conexion).ConexionSIGE

        Dim reader As SqlDataReader = Nothing
        Dim cmd As New SqlCommand("_Campania_DetalleTGxIdCampaniaDetalle", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCampaniaDetalle", IdCampaniaDetalle)
        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_Campania_DetalleTG = New Entidades.Campania_DetalleTG
                With obj_Campania_DetalleTG

                    .IdCampania = CInt(IIf(IsDBNull(reader("IdCampania")), 0, reader("IdCampania")))
                    .IdCampaniaDetalle = CInt(IIf(IsDBNull(reader("IdCampaniaDetalle")), 0, reader("IdCampaniaDetalle")))
                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")), 0, reader("IdProducto")))
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("prod_Codigo")), "", reader("prod_Codigo")))
                    .Producto = CStr(IIf(IsDBNull(reader("prod_Nombre")), "", reader("prod_Nombre")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(reader("IdUnidadMedida")), 0, reader("IdUnidadMedida")))
                    .UnidadMedida = CStr(IIf(IsDBNull(reader("UnidadMedida")), "", reader("UnidadMedida")))
                    .IdMoneda = CInt(IIf(IsDBNull(reader("IdMoneda")), 0, reader("IdMoneda")))
                    .Moneda_Campania = CStr(IIf(IsDBNull(reader("Moneda")), "", reader("Moneda")))
                    .cdtg_Precio = CDec(IIf(IsDBNull(reader("cdtg_Precio")), Decimal.Zero, reader("cdtg_Precio")))
                    .cdtg_Cantidad = CDec(IIf(IsDBNull(reader("cdtg_Cantidad")), Decimal.Zero, reader("cdtg_Cantidad")))
                    .Cadena_IdUnidadMedida_Prod = CStr(IIf(IsDBNull(reader("Cadena_IdUnidadMedida_Prod")), "", reader("Cadena_IdUnidadMedida_Prod")))
                    .Cadena_UnidadMedida_Prod = CStr(IIf(IsDBNull(reader("Cadena_UnidadMedida_Prod")), "", reader("Cadena_UnidadMedida_Prod")))
                    .cdtg_cantCampania = CBool(IIf(IsDBNull(reader("cdtg_cantCampania")), False, reader("cdtg_cantCampania")))

                End With
                list_Campania_DetalleTG.Add(obj_Campania_DetalleTG)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Open()
        End Try
        Return list_Campania_DetalleTG
    End Function

    Public Function Campania_DetalleTGxIdCampania(ByVal IdProducto As Integer, ByVal IdCampania As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSublinea As Integer, ByVal prod_codigo As String, ByVal prod_nombre As String, ByVal pageNumber As Integer, ByVal pageSize As Integer, ByVal tabla As DataTable) As List(Of Entidades.Campania_DetalleTG)

        list_Campania_DetalleTG = New List(Of Entidades.Campania_DetalleTG)

        Dim cn As SqlConnection = (New Conexion).ConexionSIGE

        Dim reader As SqlDataReader = Nothing
        Dim cmd As New SqlCommand("_Campania_DetalleTGxIdCampania", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdCampania", IdCampania)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSublinea)
        cmd.Parameters.AddWithValue("@prod_Codigo", prod_codigo)
        cmd.Parameters.AddWithValue("@prod_Nombre", prod_nombre)
        cmd.Parameters.AddWithValue("@PageNumber", pageNumber)
        cmd.Parameters.AddWithValue("@PageSize", pageSize)
        cmd.Parameters.AddWithValue("@Tabla", tabla)


        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_Campania_DetalleTG = New Entidades.Campania_DetalleTG
                With obj_Campania_DetalleTG

                    .IdCampania = CInt(IIf(IsDBNull(reader("IdCampania")), 0, reader("IdCampania")))
                    '.IdCampaniaDetalle = CInt(IIf(IsDBNull(reader("IdCampaniaDetalle")), 0, reader("IdCampaniaDetalle")))
                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")), 0, reader("IdProducto")))
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("prod_Codigo")), "", reader("prod_Codigo")))
                    .Producto = CStr(IIf(IsDBNull(reader("prod_Nombre")), "", reader("prod_Nombre")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(reader("IdUnidadMedida")), 0, reader("IdUnidadMedida")))
                    .UnidadMedida = CStr(IIf(IsDBNull(reader("UnidadMedida")), "", reader("UnidadMedida")))
                    .IdMoneda = CInt(IIf(IsDBNull(reader("IdMoneda")), 0, reader("IdMoneda")))
                    .Moneda_Campania = CStr(IIf(IsDBNull(reader("Moneda")), "", reader("Moneda")))
                    .cdtg_Precio = CDec(IIf(IsDBNull(reader("cdtg_Precio")), Decimal.Zero, reader("cdtg_Precio")))
                    .cdtg_Cantidad = CDec(IIf(IsDBNull(reader("cdtg_Cantidad")), Decimal.Zero, reader("cdtg_Cantidad")))
                    .Cadena_IdUnidadMedida_Prod = CStr(IIf(IsDBNull(reader("Cadena_IdUnidadMedida_Prod")), "", reader("Cadena_IdUnidadMedida_Prod")))
                    .Cadena_UnidadMedida_Prod = CStr(IIf(IsDBNull(reader("Cadena_UnidadMedida_Prod")), "", reader("Cadena_UnidadMedida_Prod")))
                    .cdtg_cantCampania = CBool(IIf(IsDBNull(reader("cdtg_cantCampania")), False, reader("cdtg_cantCampania")))
                End With

                If obj_Campania_DetalleTG.IdUnidadMedida = 0 And obj_Campania_DetalleTG.Cadena_IdUnidadMedida_Prod <> "" Then
                    obj_Campania_DetalleTG.IdUnidadMedida = obj_Campania_DetalleTG.getListaUnidadMedida(0).Id
                    obj_Campania_DetalleTG.UnidadMedida = obj_Campania_DetalleTG.getListaUnidadMedida(0).DescripcionCorto
                End If

                list_Campania_DetalleTG.Add(obj_Campania_DetalleTG)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Open()
        End Try
        Return list_Campania_DetalleTG
    End Function

    Public Sub Campania_DetalleTGxDelete(ByVal IdCampania As Integer, ByVal IdProducto As Integer, ByVal IdCampaniaDetalle As Integer, ByVal sqlcn As SqlConnection, ByVal sqltr As SqlTransaction)

        cmd = New SqlCommand("_Campania_DetalleTGxDelete", sqlcn, sqltr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCampania", IdCampania)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdCampaniaDetalle", IdCampaniaDetalle)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally            
        End Try        

    End Sub

End Class

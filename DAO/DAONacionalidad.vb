'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAONacionalidad
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaNacionalidad(ByVal nacionalidad As Entidades.Nacionalidad) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@nac_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = nacionalidad.Descripcion
        ArrayParametros(1) = New SqlParameter("@nac_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = nacionalidad.Estado
        ArrayParametros(2) = New SqlParameter("@IdPaisBase", SqlDbType.Bit)
        ArrayParametros(2).Value = nacionalidad.IdPaisBase

        Return HDAO.Insert(cn, "_NacionalidadInsert", ArrayParametros)
    End Function
    Public Function ActualizaNacionalidad(ByVal nacionalidad As Entidades.Nacionalidad) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@nac_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = nacionalidad.Descripcion
        ArrayParametros(1) = New SqlParameter("@nac_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = nacionalidad.Estado
        ArrayParametros(2) = New SqlParameter("@IdNacionalidad", SqlDbType.Int)
        ArrayParametros(2).Value = nacionalidad.Id
        ArrayParametros(3) = New SqlParameter("@IdPaisBase", SqlDbType.Int)
        ArrayParametros(3).Value = nacionalidad.IdPaisBase

        Return HDAO.Update(cn, "_NacionalidadUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Nacionalidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NacionalidadSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Nacionalidad)
                Do While lector.Read
                    Dim obj As New Entidades.Nacionalidad
                    obj.Descripcion = CStr(lector.Item("nac_Nombre"))
                    obj.Estado = CStr(lector.Item("nac_Estado"))
                    obj.Id = CInt(lector.Item("IdNacionalidad"))
                    obj.IdPaisBase = CBool(IIf(IsDBNull(lector.Item("IdPaisBase")), False, lector.Item("IdPaisBase")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Nacionalidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NacionalidadSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Nacionalidad)
                Do While lector.Read
                    Dim obj As New Entidades.Nacionalidad
                    obj.Descripcion = CStr(lector.Item("nac_Nombre"))
                    obj.Estado = CStr(lector.Item("nac_Estado"))
                    obj.Id = CInt(lector.Item("IdNacionalidad"))
                    obj.IdPaisBase = CBool(IIf(IsDBNull(lector.Item("IdPaisBase")), False, lector.Item("IdPaisBase")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Nacionalidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NacionalidadSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Nacionalidad)
                Do While lector.Read
                    Dim obj As New Entidades.Nacionalidad
                    obj.Descripcion = CStr(lector.Item("nac_Nombre"))
                    obj.Estado = CStr(lector.Item("nac_Estado"))
                    obj.Id = CInt(lector.Item("IdNacionalidad"))
                    obj.IdPaisBase = CBool(IIf(IsDBNull(lector.Item("IdPaisBase")), False, lector.Item("IdPaisBase")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Nacionalidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NacionalidadSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Nacionalidad)
                Do While lector.Read
                    Dim obj As New Entidades.Nacionalidad
                    obj.Descripcion = CStr(lector.Item("nac_Nombre"))
                    obj.Estado = CStr(lector.Item("nac_Estado"))
                    obj.Id = CInt(lector.Item("IdNacionalidad"))
                    obj.IdPaisBase = CBool(IIf(IsDBNull(lector.Item("IdPaisBase")), False, lector.Item("IdPaisBase")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Nacionalidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NacionalidadSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Nacionalidad)
                Do While lector.Read
                    Dim obj As New Entidades.Nacionalidad
                    obj.Descripcion = CStr(lector.Item("nac_Nombre"))
                    obj.Estado = CStr(lector.Item("nac_Estado"))
                    obj.Id = CInt(lector.Item("IdNacionalidad"))
                    obj.IdPaisBase = CBool(IIf(IsDBNull(lector.Item("IdPaisBase")), False, lector.Item("IdPaisBase")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Nacionalidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NacionalidadSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Nacionalidad)
                Do While lector.Read
                    Dim obj As New Entidades.Nacionalidad
                    obj.Descripcion = CStr(lector.Item("nac_Nombre"))
                    obj.Estado = CStr(lector.Item("nac_Estado"))
                    obj.Id = CInt(lector.Item("IdNacionalidad"))
                    obj.IdPaisBase = CBool(IIf(IsDBNull(lector.Item("IdPaisBase")), False, lector.Item("IdPaisBase")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Nacionalidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NacionalidadSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Nacionalidad)
                Do While lector.Read
                    Dim obj As New Entidades.Nacionalidad
                    obj.Descripcion = CStr(lector.Item("nac_Nombre"))
                    obj.Estado = CStr(lector.Item("nac_Estado"))
                    obj.Id = CInt(lector.Item("IdNacionalidad"))
                    obj.IdPaisBase = CBool(IIf(IsDBNull(lector.Item("IdPaisBase")), False, lector.Item("IdPaisBase")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Nacionalidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NacionalidadSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Nacionalidad)
                Do While lector.Read
                    Dim Nacionalidad As New Entidades.Nacionalidad
                    Nacionalidad.Id = CInt(lector.Item("IdNacionalidad"))
                    Nacionalidad.Descripcion = CStr(lector.Item("nac_Nombre"))
                    Nacionalidad.Estado = CStr(lector.Item("nac_Estado"))
                    Nacionalidad.IdPaisBase = CBool(IIf(IsDBNull(lector.Item("IdPaisBase")), False, lector.Item("IdPaisBase")))
                    Lista.Add(Nacionalidad)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTipoDireccion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTipoDireccion(ByVal tipodireccion As Entidades.TipoDireccion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@td_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipodireccion.Nombre
        ArrayParametros(1) = New SqlParameter("@td_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipodireccion.Estado
        Return HDAO.Insert(cn, "_TipoDireccionInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoDireccion(ByVal tipodireccion As Entidades.TipoDireccion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@td_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipodireccion.Nombre
        ArrayParametros(1) = New SqlParameter("@td_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipodireccion.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoDireccion", SqlDbType.Int)
        ArrayParametros(2).Value = tipodireccion.Id
        Return HDAO.Update(cn, "_TipoDireccionUpdate", ArrayParametros)
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoDireccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDireccionSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDireccion)
                Do While lector.Read
                    Dim TipoDireccion As New Entidades.TipoDireccion
                    TipoDireccion.Id = CInt(lector.Item("IdTipoDireccion"))
                    TipoDireccion.Nombre = CStr(lector.Item("td_Nombre"))
                    Lista.Add(TipoDireccion)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoDireccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDireccionSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDireccion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDireccion
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("td_Estado")) = True, "", lector.Item("td_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("td_Nombre")) = True, "", lector.Item("td_Nombre")))
                    obj.Id = CInt(lector.Item("IdTipoDireccion"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoDireccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDireccionSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDireccion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDireccion
                    obj.Estado = CStr(lector.Item("td_Estado"))
                    obj.Nombre = CStr(lector.Item("td_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoDireccion"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoDireccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDireccionSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDireccion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDireccion
                    obj.Estado = CStr(lector.Item("td_Estado"))
                    obj.Nombre = CStr(lector.Item("td_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoDireccion"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoDireccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDireccionSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDireccion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDireccion
                    obj.Estado = CStr(lector.Item("td_Estado"))
                    obj.Nombre = CStr(lector.Item("td_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoDireccion"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoDireccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDireccionSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDireccion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDireccion
                    obj.Estado = CStr(lector.Item("td_Estado"))
                    obj.Nombre = CStr(lector.Item("td_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoDireccion"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoDireccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDireccionSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDireccion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDireccion
                    obj.Estado = CStr(lector.Item("td_Estado"))
                    obj.Nombre = CStr(lector.Item("td_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoDireccion"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoDireccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDireccionSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDireccion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDireccion
                    obj.Estado = CStr(lector.Item("td_Estado"))
                    obj.Nombre = CStr(lector.Item("td_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoDireccion"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

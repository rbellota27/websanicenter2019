'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 22-Oct-2009
'Hora    : 12:22 pm
'*************************************************

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 05-Oct-2009
'Hora    : 09:16 am
'*************************************************
Imports System.Data.SqlClient

Public Class DAOOficina
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    'Public Function InsertaOficina(ByVal oficina As Entidades.Oficina) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
    '    ArrayParametros(0) = New SqlParameter("@of_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = oficina.Descripcion
    '    ArrayParametros(1) = New SqlParameter("@of_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = oficina.Estado
    '    ArrayParametros(2) = New SqlParameter("@IdBanco", SqlDbType.Int)
    '    ArrayParametros(2).Value = oficina.IdBanco
    '    Return HDAO.Insert(cn, "_OficinaInsert", ArrayParametros)
    'End Function
    Public Sub InsertaOficina(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.Oficina), ByVal idBanco As Integer)
        Dim fila As New Entidades.Oficina
        Dim arrayParametros() As SqlParameter = New SqlParameter(3) {}
        arrayParametros(0) = New SqlParameter("IdBanco", SqlDbType.Int)
        arrayParametros(0).Value = idBanco
        For Each fila In lista
            arrayParametros(1) = New SqlParameter("IdOficina", SqlDbType.Int)
            arrayParametros(1).Value = fila.Id
            arrayParametros(2) = New SqlParameter("of_Nombre", SqlDbType.VarChar)
            arrayParametros(2).Value = fila.Descripcion
            arrayParametros(3) = New SqlParameter("of_Estado", SqlDbType.Char)
            arrayParametros(3).Value = fila.Estado
            HDAO.InsertaT(cn, "_OficinaInsert", arrayParametros, tr)
        Next
    End Sub
    Public Function InsertaListaOficina(ByVal IdBanco As Integer, ByVal lista As List(Of Entidades.Oficina), ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        'recibo la conexion abierta
        'inserta con con id bco
        Dim cmd As SqlCommand
        Try

            If lista IsNot Nothing Then
                For i As Integer = 0 To lista.Count - 1
                    ' If tr IsNot Nothing Then
                    cmd = New SqlCommand("_OficinaInsert", cn, tr)
                    'Else
                    'cmd = New SqlCommand("_OficinaInsert", cn)
                    ' End If
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@IdBanco", IdBanco)
                    cmd.Parameters.AddWithValue("@of_Nombre", lista.Item(i).Descripcion)
                    cmd.Parameters.AddWithValue("@IdOficina", lista.Item(i).Id)
                    cmd.Parameters.AddWithValue("@of_Estado", lista.Item(i).Estado)

                    If cmd.ExecuteNonQuery = 0 Then
                        Return False
                    End If
                Next
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ActualizaOficina(ByVal oficina As Entidades.Oficina) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@of_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = oficina.Descripcion
        ArrayParametros(1) = New SqlParameter("@of_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = oficina.Estado
        ArrayParametros(2) = New SqlParameter("@IdBanco", SqlDbType.Int)
        ArrayParametros(2).Value = oficina.IdBanco
        ArrayParametros(3) = New SqlParameter("@IdOficina", SqlDbType.Int)
        ArrayParametros(3).Value = oficina.Id
        Return HDAO.Update(cn, "_OficinaUpdate", ArrayParametros)
    End Function
    Public Function SelectxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.Oficina)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OficinaSelectxIdBanco1", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdBanco", IdBanco)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Oficina)
                Do While lector.Read
                    Dim objOficina As New Entidades.Oficina
                    objOficina.IdBanco = CInt(IIf(IsDBNull(lector.Item("IdBanco")) = True, 0, lector.Item("IdBanco")))
                    objOficina.Id = CInt(IIf(IsDBNull(lector.Item("IdOficina")) = True, 0, lector.Item("IdOficina")))
                    objOficina.Descripcion = CStr(IIf(IsDBNull(lector.Item("of_Nombre")) = True, 0, lector.Item("of_Nombre")))
                    objOficina.Estado = CChar(IIf(IsDBNull(lector.Item("of_Estado")) = True, "", lector.Item("of_Estado")))

                    Lista.Add(objOficina)

                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub DeletexIdBancoxIdOficina(ByVal IdBanco As Integer, ByVal IdOficina As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_BancoDeletexIdBancoxIdOficina", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdBanco", IdBanco)
        cmd.Parameters.AddWithValue("@IdOficina", IdOficina)
        If cmd.ExecuteNonQuery() = 0 Then
            Throw New Exception
        End If
    End Sub


    Public Function SelectCboxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.Oficina)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OficinaSelectCboxIdBanco", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdBanco", IdBanco)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Oficina)
                Do While lector.Read
                    Dim objOficina As New Entidades.Oficina
                    objOficina.Id = CInt(IIf(IsDBNull(lector.Item("IdOficina")) = True, 0, lector.Item("IdOficina")))
                    objOficina.Descripcion = CStr(IIf(IsDBNull(lector.Item("of_Nombre")) = True, 0, lector.Item("of_Nombre")))
                    Lista.Add(objOficina)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class

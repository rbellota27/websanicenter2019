﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAODocImportacion
    Dim ObjConexion As New Conexion
    Dim HDAO As New DAO.HelperDAO
    Public Function OrdenCompraSelectNroOCxIdProveedor(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, _
                                                       ByVal Serie As Integer, ByVal Correlativo As Integer, _
                                                       ByVal IdProveedor As Integer, ByVal pageindex As Integer, _
                                                       ByVal pagesize As Integer) As List(Of Entidades.DocImportacion)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenCompraSelectNroOCxIdProveedor", cn)
        cmd.CommandTimeout = 20000
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Correlativo", Correlativo)
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        cmd.Parameters.AddWithValue("@pageindex", pageindex)
        cmd.Parameters.AddWithValue("@pageSize", pagesize)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.DocImportacion
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .NroOrdenCompra = CStr(IIf(IsDBNull(lector.Item("NroOC")) = True, "", lector.Item("NroOC")))
                        .IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("RazonSocial")) = True, "", lector.Item("RazonSocial")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_direccion")) = True, "", lector.Item("dir_direccion")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDoc = CStr(IIf(IsDBNull(lector.Item("NomTipoDoc")) = True, "", lector.Item("NomTipoDoc")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, "", lector.Item("doc_FechaEmision")))
                        .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                        '.PrecImportacion = CStr(IIf(IsDBNull(lector.Item("anex_PrecImportacion")) = True, "", lector.Item("anex_PrecImportacion")))
                        .IdTPImp = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                        .MonSimbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .IdImportacion = CStr(IIf(IsDBNull(lector.Item("NroImportacion")) = True, "", lector.Item("NroImportacion")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCostosSinPasarxIdDocumento(ByVal IdDocumento As Integer) As Integer
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectCostosSinPasarxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Dim contador As Integer = 0
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()

                contador = CInt(IIf(IsDBNull(lector.Item("contador")) = True, 0, lector.Item("contador")))

                lector.Close()

                Return contador
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function DetalleGastoSelectxIdConcepto(ByVal IdConcepto As Integer) As List(Of Entidades.DocImportacion)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleGastoxIdConcepto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdConcepto", IdConcepto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.DocImportacion
                    With obj
                        .IdConcepto = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                        .NomConcepto = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .MonSimbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .NomCalculo = CStr(IIf(IsDBNull(lector.Item("con_TipoCalculo")) = True, "", lector.Item("con_TipoCalculo")))
                        .SubTotalGasto = CDec(IIf(IsDBNull(lector.Item("ctdoc_Valor")) = True, 0, lector.Item("ctdoc_Valor")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllTipoDocumento() As List(Of Entidades.DocImportacion)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocImportacionSelectAllTipoDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.DocImportacion
                    With obj
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoRef")) = True, 0, lector.Item("IdTipoDocumentoRef")))
                        .NomTipoDoc = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function InsertImportacionT(ByVal cn As SqlConnection, ByVal objimportacion As Entidades.DocImportacion, Optional ByVal T As SqlTransaction = Nothing) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(17) {}
        ArrayParametros(0) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar, 12)
        ArrayParametros(0).Value = IIf(objimportacion.doc_Codigo = Nothing, DBNull.Value, objimportacion.doc_Codigo)

        ArrayParametros(1) = New SqlParameter("@doc_Serie", SqlDbType.VarChar, 4)
        ArrayParametros(1).Value = IIf(objimportacion.doc_Serie = Nothing, DBNull.Value, objimportacion.doc_Serie)

        ArrayParametros(2) = New SqlParameter("@IdSerie", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(objimportacion.Serie = Nothing, DBNull.Value, objimportacion.Serie)

        ArrayParametros(3) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(objimportacion.IdTienda = Nothing, DBNull.Value, objimportacion.IdTienda)

        ArrayParametros(4) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(objimportacion.IdEmpresa = Nothing, DBNull.Value, objimportacion.IdEmpresa)

        ArrayParametros(5) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        ArrayParametros(5).Value = IIf(objimportacion.FechaEmision = Nothing, DBNull.Value, objimportacion.FechaEmision)

        ArrayParametros(6) = New SqlParameter("@doc_FechaSalida", SqlDbType.DateTime)
        ArrayParametros(6).Value = IIf(objimportacion.doc_FechaSalida = Nothing, DBNull.Value, objimportacion.doc_FechaSalida)

        ArrayParametros(7) = New SqlParameter("@doc_FechaLLegada", SqlDbType.DateTime)
        ArrayParametros(7).Value = IIf(objimportacion.doc_FechaLLegada = Nothing, DBNull.Value, objimportacion.doc_FechaLLegada)

        ArrayParametros(8) = New SqlParameter("@doc_FechaIngMilla", SqlDbType.DateTime)
        ArrayParametros(8).Value = IIf(objimportacion.doc_FechaIngMilla = Nothing, DBNull.Value, objimportacion.doc_FechaIngMilla)

        ArrayParametros(9) = New SqlParameter("@ImpTotal_Imp", SqlDbType.Decimal)
        ArrayParametros(9).Value = IIf(objimportacion.ImpTotal_Imp = Nothing, DBNull.Value, objimportacion.ImpTotal_Imp)

        ArrayParametros(10) = New SqlParameter("@ImpTotalOC", SqlDbType.Decimal)
        ArrayParametros(10).Value = IIf(objimportacion.ImpTotalOC = Nothing, DBNull.Value, objimportacion.ImpTotalOC)

        ArrayParametros(11) = New SqlParameter("@ImpPesoTotal", SqlDbType.Decimal)
        ArrayParametros(11).Value = IIf(objimportacion.ImpPesoTotal = Nothing, DBNull.Value, objimportacion.ImpPesoTotal)

        ArrayParametros(12) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(12).Value = IIf(objimportacion.IdUsuario = Nothing, DBNull.Value, objimportacion.IdUsuario)

        ArrayParametros(13) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(13).Value = IIf(objimportacion.IdMoneda = Nothing, DBNull.Value, objimportacion.IdMoneda)

        ArrayParametros(14) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(14).Value = IIf(objimportacion.IdTipoDocumento = Nothing, DBNull.Value, objimportacion.IdTipoDocumento)

        ArrayParametros(15) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(15).Value = IIf(objimportacion.IdTipoOperacion = Nothing, DBNull.Value, objimportacion.IdTipoOperacion)

        ArrayParametros(16) = New SqlParameter("@IdProveedor", SqlDbType.Int)
        ArrayParametros(16).Value = IIf(objimportacion.IdProveedor = Nothing, DBNull.Value, objimportacion.IdProveedor)

        ArrayParametros(17) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(17).Direction = ParameterDirection.Output
        ArrayParametros(17).Value = 0
        If T IsNot Nothing Then
            Return HDAO.InsertaTParameterOutPut(cn, "_ImportacionInsert", ArrayParametros, T)
        Else
            Return HDAO.Insert_ReturnParameter(cn, "_ImportacionInsert", ArrayParametros)
        End If
    End Function
    Public Function ActualizaImportacionT(ByVal objImportacion As Entidades.DocImportacion, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(17) {}
            ArrayParametros(0) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar, 12)
            ArrayParametros(0).Value = IIf(objImportacion.doc_Codigo = Nothing, DBNull.Value, objImportacion.doc_Codigo)

            ArrayParametros(1) = New SqlParameter("@doc_Serie", SqlDbType.VarChar, 4)
            ArrayParametros(1).Value = IIf(objImportacion.doc_Serie = Nothing, DBNull.Value, objImportacion.doc_Serie)

            ArrayParametros(2) = New SqlParameter("@IdSerie", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(objImportacion.Serie = Nothing, DBNull.Value, objImportacion.Serie)

            ArrayParametros(3) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(objImportacion.IdTienda = Nothing, DBNull.Value, objImportacion.IdTienda)

            ArrayParametros(4) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(objImportacion.IdEmpresa = Nothing, DBNull.Value, objImportacion.IdEmpresa)

            ArrayParametros(5) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
            ArrayParametros(5).Value = IIf(objImportacion.FechaEmision = Nothing, DBNull.Value, objImportacion.FechaEmision)

            ArrayParametros(6) = New SqlParameter("@doc_FechaSalida", SqlDbType.DateTime)
            ArrayParametros(6).Value = IIf(objImportacion.doc_FechaSalida = Nothing, DBNull.Value, objImportacion.doc_FechaSalida)

            ArrayParametros(7) = New SqlParameter("@doc_FechaLLegada", SqlDbType.DateTime)
            ArrayParametros(7).Value = IIf(objImportacion.doc_FechaLLegada = Nothing, DBNull.Value, objImportacion.doc_FechaLLegada)

            ArrayParametros(8) = New SqlParameter("@doc_FechaIngMilla", SqlDbType.DateTime)
            ArrayParametros(8).Value = IIf(objImportacion.doc_FechaIngMilla = Nothing, DBNull.Value, objImportacion.doc_FechaIngMilla)

            ArrayParametros(9) = New SqlParameter("@ImpTotal_Imp", SqlDbType.Decimal)
            ArrayParametros(9).Value = IIf(objImportacion.ImpTotal_Imp = Nothing, DBNull.Value, objImportacion.ImpTotal_Imp)

            ArrayParametros(10) = New SqlParameter("@ImpTotalOC", SqlDbType.Decimal)
            ArrayParametros(10).Value = IIF(objImportacion.ImpTotalOC = Nothing, DBNull.Value, objImportacion.ImpTotalOC)

            ArrayParametros(11) = New SqlParameter("@ImpPesoTotal", SqlDbType.Decimal)
            ArrayParametros(11).Value = IIf(objImportacion.ImpPesoTotal = Nothing, DBNull.Value, objImportacion.ImpPesoTotal)

            ArrayParametros(12) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            ArrayParametros(12).Value = IIf(objImportacion.IdUsuario = Nothing, DBNull.Value, objImportacion.IdUsuario)

            ArrayParametros(13) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            ArrayParametros(13).Value = IIf(objImportacion.IdMoneda = Nothing, DBNull.Value, objImportacion.IdMoneda)

            ArrayParametros(14) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            ArrayParametros(14).Value = IIf(objImportacion.IdTipoDocumento = Nothing, DBNull.Value, objImportacion.IdTipoDocumento)

            ArrayParametros(15) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            ArrayParametros(15).Value = IIf(objImportacion.IdTipoOperacion = Nothing, DBNull.Value, objImportacion.IdTipoOperacion)

            ArrayParametros(16) = New SqlParameter("@IdProveedor", SqlDbType.Int)
            ArrayParametros(16).Value = IIf(objImportacion.IdProveedor = Nothing, DBNull.Value, objImportacion.IdProveedor)

            ArrayParametros(17) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(17).Value = IIf(objImportacion.IdDocumento = Nothing, DBNull.Value, objImportacion.IdDocumento)

            HDAO.UpdateT(cn, "_ImportacionUpdate", ArrayParametros, tr)
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        Finally

        End Try
    End Function

    Public Function ImportacionSelectId(ByVal IdDocumento As Integer) As Entidades.DocImportacion
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ImportacionSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objImpDetalle As New Entidades.DocImportacion
                If lector.Read Then
                    With objImpDetalle
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .Serie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, 0, lector.Item("IdSerie")))
                        .NroOrdenCompra = CStr(IIf(IsDBNull(lector.Item("NroImportacion")) = True, "", lector.Item("NroImportacion")))
                        .IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("RazonSocial")) = True, "", lector.Item("RazonSocial")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("Direccion")) = True, "", lector.Item("Direccion")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .doc_FechaSalida = CDate(IIf(IsDBNull(lector.Item("doc_FechaIniTraslado")) = True, Nothing, lector.Item("doc_FechaIniTraslado")))
                        .doc_FechaLLegada = CDate(IIf(IsDBNull(lector.Item("doc_FechaAEntregar")) = True, Nothing, lector.Item("doc_FechaAEntregar")))
                        .doc_FechaIngMilla = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .doc_Importacion = CBool(IIf(IsDBNull(lector.Item("doc_Importacion")) = True, 0, lector.Item("doc_Importacion")))
                        '.PrecImportacion = CStr(IIf(IsDBNull(lector.Item("anex_PrecImportacion")) = True, "", lector.Item("anex_PrecImportacion")))
                        .IdTPImp = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                        .NroContenedores = CStr(IIf(IsDBNull(lector.Item("anex_NroContenedores")) = True, "", lector.Item("anex_NroContenedores")))
                        .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                        .IdDocumento2 = CInt(IIf(IsDBNull(lector.Item("IdDocumento2")) = True, 0, lector.Item("IdDocumento2")))
                        .doc_Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .NroDoc = CStr(IIf(IsDBNull(lector.Item("NroOC")) = True, "", lector.Item("NroOC")))
                        .MontoxSustentar = CDec(IIf(IsDBNull(lector.Item("anex_MontoxSustentar")) = True, 0, lector.Item("anex_MontoxSustentar")))
                    End With
                End If
                lector.Close()
                Return objImpDetalle
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DetalleGastoImportacionSelectxId(ByVal IdDocumento As Integer) As List(Of Entidades.DocImportacion)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleGastoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.DocImportacion
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdDetalleConcepto = CInt(IIf(IsDBNull(lector.Item("IdDetalleConcepto")) = True, 0, lector.Item("IdDetalleConcepto")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoRef")) = True, 0, lector.Item("IdTipoDocumentoRef")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("dg_NroDocumento")) = True, "", lector.Item("dg_NroDocumento")))
                        .IdConcepto = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                        .NomConcepto = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .MonSimbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .NomCalculo = CStr(IIf(IsDBNull(lector.Item("con_TipoCalculo")) = True, "", lector.Item("con_TipoCalculo")))
                        .SubTotalGasto = CDec(IIf(IsDBNull(lector.Item("dr_Monto")) = True, 0, lector.Item("dr_Monto")))
                        .IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("NomProveedor")) = True, "", lector.Item("NomProveedor")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function AnularImportacion(ByVal objImportacion As Entidades.DocImportacion) As Boolean
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = objImportacion.IdDocumento
        Return HDAO.Update(cn, "_AnularImportacionSelectxId", ArrayParametros)
    End Function


    Public Function ImportacionProyectadoSelectAllxSeriexCodigoxProveedor(ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdProveedor As Integer) As List(Of Entidades.DocImportacion)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ImportacionProyectadaSelectAllxSeriexCodigoxProveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@doc_Serie", Serie)
        cmd.Parameters.AddWithValue("@doc_Codigo", Codigo)
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.DocImportacion
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .NomEmpresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                        .doc_Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .Serie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, 0, lector.Item("IdSerie")))
                        .IdImportacion = CStr(IIf(IsDBNull(lector.Item("NroImportacion")) = True, "", lector.Item("NroImportacion")))
                        .IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("RazonSocial")) = True, "", lector.Item("RazonSocial")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("Direccion")) = True, "", lector.Item("Direccion")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, "", lector.Item("doc_FechaEmision")))
                        .doc_Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .NroDoc = CStr(IIf(IsDBNull(lector.Item("NroOC")) = True, "", lector.Item("NroOC")))
                        .FechaEmisionOC = CDate(IIf(IsDBNull(lector.Item("FechaEmisionOC")) = True, "", lector.Item("FechaEmisionOC")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ImportacionSelectAllxSeriexCodigoxProveedor(ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdProveedor As Integer) As List(Of Entidades.DocImportacion)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ImportacionSelectAllxSeriexCodigoxProveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@doc_Serie", Serie)
        cmd.Parameters.AddWithValue("@doc_Codigo", Codigo)
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.DocImportacion
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .NomEmpresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                        .doc_Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .Serie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, 0, lector.Item("IdSerie")))
                        .IdImportacion = CStr(IIf(IsDBNull(lector.Item("NroImportacion")) = True, "", lector.Item("NroImportacion")))
                        .IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("RazonSocial")) = True, "", lector.Item("RazonSocial")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("Direccion")) = True, "", lector.Item("Direccion")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, "", lector.Item("doc_FechaEmision")))
                        .doc_Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .NroDoc = CStr(IIf(IsDBNull(lector.Item("NroOC")) = True, "", lector.Item("NroOC")))
                        .FechaEmisionOC = CDate(IIf(IsDBNull(lector.Item("FechaEmisionOC")) = True, "", lector.Item("FechaEmisionOC")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

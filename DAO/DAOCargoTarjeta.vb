﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOCargoTarjeta
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function InsertaCargoTarjeta(ByVal CargoTarjeta As Entidades.CargoTarjeta) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}

        'ArrayParametros(0) = New SqlParameter("@ct_Nombre", SqlDbType.VarChar)
        'ArrayParametros(0).Value = IIf(CargoTarjeta.ct_Nombre = Nothing, DBNull.Value, CargoTarjeta.ct_Nombre)

        ArrayParametros(0) = New SqlParameter("@ct_Valor", SqlDbType.Decimal)
        ArrayParametros(0).Value = IIf(CargoTarjeta.ct_Valor = Nothing, DBNull.Value, CargoTarjeta.ct_Valor)

        ArrayParametros(1) = New SqlParameter("@ct_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(CargoTarjeta.ct_Estado = Nothing, DBNull.Value, CargoTarjeta.Estado)

        Return HDAO.Insert(cn, "_CargoTarjetaInsert", ArrayParametros)
    End Function
    Public Function ActualizaCargoTarjeta(ByVal CargoTarjeta As Entidades.CargoTarjeta) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdCargoTarjeta", SqlDbType.Int)
        ArrayParametros(0).Value = CargoTarjeta.IdCargoTarjeta
        'ArrayParametros(1) = New SqlParameter("@ct_Nombre", SqlDbType.VarChar)
        'ArrayParametros(1).Value = IIf(CargoTarjeta.ct_Nombre = Nothing, DBNull.Value, CargoTarjeta.ct_Nombre)
        'ArrayParametros(1) = New SqlParameter("@ct_Valor", SqlDbType.Decimal)
        'ArrayParametros(1).Value = IIf(CargoTarjeta.ct_Valor = Nothing, DBNull.Value, CargoTarjeta.ct_Valor)
        ArrayParametros(1) = New SqlParameter("@ct_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(CargoTarjeta.ct_Estado = Nothing, DBNull.Value, CargoTarjeta.Estado)

        Return HDAO.Update(cn, "_CargoTarjetaUpdate", ArrayParametros)
    End Function
    'Public Function SelectAll() As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_CargoTarjetaSelectAll", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    Dim lector As SqlDataReader 'LEE SECUENCIA DE FILAS
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection) 'Envía CommandText a Connection y crea un SqlDataReader mediante uno de los valores de CommandBehavior. 
    '            Dim Lista As New List(Of Entidades.CargoTarjeta)
    '            Do While lector.Read
    '                Dim CargoTarjeta As New Entidades.CargoTarjeta

    '                CargoTarjeta.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
    '                CargoTarjeta.ct_Nombre = CStr(lector.Item("ct_Nombre"))
    '                CargoTarjeta.ct_Estado = CStr(lector.Item("ct_Estado"))
    '                CargoTarjeta.ct_Valor = CDec(IIf(IsDBNull(lector.Item("ct_Valor")), 0, lector.Item("ct_Valor")))

    '                Lista.Add(CargoTarjeta)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Function SelectxIdCargoTarjeta(ByVal idCargoTarjeta As Integer) As Entidades.CargoTarjeta
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoTarjetaSelectxIdCargoTarjeta", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idCargoTarjeta", idCargoTarjeta)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.CargoTarjeta
                If lector.Read Then
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "0", lector.Item("sl_Estado")))
                    obj.ct_Valor = CDec(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "0", lector.Item("sl_CostoCompra")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCargoTarjeta")) = True, 0, lector.Item("IdCargoTarjeta")))
                    'obj.ct_Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "0", lector.Item("sl_Nombre")))
                    obj.IdCargoTarjeta = CInt(IIf(IsDBNull(lector.Item("sl_Codigo")) = True, "0", lector.Item("sl_Codigo")))
                    obj.CodigoCargoTarjeta = CInt(IIf(IsDBNull(lector.Item("sl_Codigo")) = True, "0", lector.Item("sl_Codigo")))

                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    'Public Function SelectAllActivo() As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_CargoTarjetaSelectAllActivo", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection) ''Envía CommandText a Connection y crea un SqlDataReader mediante uno de los valores de CommandBehavior. 
    '            Dim Lista As New List(Of Entidades.CargoTarjeta) 'permite listar simple listado de strings hasta LISTADO clase más compleja que hayáis creado
    '            Do While lector.Read
    '                Dim CargoTarjeta As New Entidades.CargoTarjeta

    '                CargoTarjeta.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
    '                CargoTarjeta.ct_Nombre = CStr(lector.Item("ct_Nombre"))
    '                CargoTarjeta.ct_Estado = CStr(lector.Item("ct_Estado"))
    '                CargoTarjeta.ct_Valor = CDec(IIf(IsDBNull(lector.Item("ct_Valor")) = True, 0, lector.Item("ct_Valor")))

    '                Lista.Add(CargoTarjeta)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function SelectAllInactivo() As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_CargoTarjetaSelectAllInactivo", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.CargoTarjeta)
    '            Do While lector.Read
    '                Dim CargoTarjeta As New Entidades.CargoTarjeta

    '                CargoTarjeta.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
    '                CargoTarjeta.ct_Nombre = CStr(lector.Item("ct_Nombre"))
    '                CargoTarjeta.ct_Estado = CStr(lector.Item("ct_Estado"))
    '                CargoTarjeta.ct_Valor = CDec(IIf(IsDBNull(lector.Item("ct_Valor")) = True, 0, lector.Item("ct_Valor")))
    '                Lista.Add(CargoTarjeta)

    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_CargoTarjetaSelectAllxNombre", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@Nombre", nombre)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.CargoTarjeta)
    '            Do While lector.Read
    '                Dim obj As New Entidades.CargoTarjeta
    '                obj.ct_Estado = CStr(lector.Item("ct_Estado"))
    '                obj.ct_Nombre = CStr(lector.Item("ct_Nombre"))
    '                obj.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
    '                obj.ct_Valor = CDec(IIf(IsDBNull(lector.Item("ct_Valor")) = True, 0, lector.Item("ct_Valor")))

    '                Lista.Add(obj)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_CargoTarjetaSelectActivoxNombre", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@Nombre", nombre)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.CargoTarjeta)
    '            Do While lector.Read
    '                Dim obj As New Entidades.CargoTarjeta
    '                obj.ct_Estado = CStr(lector.Item("ct_Estado"))
    '                obj.ct_Nombre = CStr(lector.Item("ct_Nombre"))
    '                obj.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
    '                obj.ct_Valor = CDec(IIf(IsDBNull(lector.Item("ct_Valor")) = True, 0, lector.Item("ct_Valor")))

    '                Lista.Add(obj)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_CargoTarjetaSelectInactivoxNombre", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@Nombre", nombre)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.CargoTarjeta)
    '            Do While lector.Read
    '                Dim obj As New Entidades.CargoTarjeta

    '                obj.ct_Estado = CStr(lector.Item("ct_Estado"))
    '                obj.ct_Nombre = CStr(lector.Item("ct_Nombre"))
    '                obj.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
    '                obj.ct_Valor = CDec(IIf(IsDBNull(lector.Item("ct_Valor")) = True, 0, lector.Item("ct_Valor")))

    '                Lista.Add(obj)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_CargoTarjetaSelectxId", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@Id", id)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.CargoTarjeta)
    '            Do While lector.Read

    '                Dim obj As New Entidades.CargoTarjeta
    '                obj.ct_Estado = CStr(lector.Item("_ct_Estado"))
    '                obj.ct_Nombre = CStr(lector.Item("_ct_Nombre"))
    '                obj.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
    '                obj.ct_Valor = CDec(IIf(IsDBNull(lector.Item("_ct_Valor")) = True, 0, lector.Item("_ct_Valor")))

    '                Lista.Add(obj)
    '            Loop
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function SelectCbo() As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_CargoTarjetaSelectCbo", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.CargoTarjeta)
    '            Do While lector.Read
    '                Dim obj As New Entidades.CargoTarjeta

    '                obj.ct_Nombre = CStr(lector.Item("ct_Nombre"))
    '                obj.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))

    '                Lista.Add(obj)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function SelectAllxValor(ByVal valor As String) As List(Of Entidades.CargoTarjeta)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_ZonaTipoSelectAllxNombre", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@ct_Valor", valor)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.CargoTarjeta)
    '            Do While lector.Read
    '                Dim CargoTarjeta As New Entidades.CargoTarjeta
    '                CargoTarjeta.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
    '                CargoTarjeta.ct_Valor = CDec(lector.Item("ct_Valor"))
    '                CargoTarjeta.ct_Estado = CStr(CBool(IIf(IsDBNull(lector.Item("ct_Estado")) = True, "", lector.Item("ct_Estado"))))
    '                Lista.Add(CargoTarjeta)
    '            Loop
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Function SelectCodigoxIdCargoTarjeta(ByVal IdCargoTarjeta As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_CargoTarjetaSelectxIdCargoTarjeta]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCargoTarjeta", IdCargoTarjeta)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                Dim Codigo As String = CStr(IIf(IsDBNull(lector.Item("sl_Codigo")) = True, "", lector.Item("sl_Codigo")))
                Return Codigo
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCargoTarjeta(ByVal estado As Integer) As List(Of Entidades.CargoTarjeta)
        Dim Lista As New List(Of Entidades.CargoTarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoTarjetaSelect", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@estado", estado)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.CargoTarjeta
                With obj
                    .IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
                    '.ct_Nombre = CStr(IIf(IsDBNull(lector("ct_Nombre")), "---", lector("ct_Nombre")))
                    .ct_Valor = CDec(IIf(IsDBNull(lector("ct_Valor")), "---", lector("ct_Valor")))
                    .ct_Estado = CStr(IIf(IsDBNull(lector("ct_Estado")), "---", lector("ct_Estado")))

                    .ct_FechaReg = CDate(lector.Item("ct_FechaReg"))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function
    Public Function SelectxEstadoxNombre(ByVal nombre As String, ByVal estado As String) As List(Of Entidades.CargoTarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.CargoTarjeta)
        Try
            cn.Open()
            Dim cmd As New SqlCommand("[_CargoTarjetaSelectAllActivoxEstado]", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ct_Nombre", nombre)
            cmd.Parameters.AddWithValue("@ct_Estado", estado)
            Dim lector As SqlDataReader = cmd.ExecuteReader
            While lector.Read
                Dim obj As New Entidades.CargoTarjeta
                obj.IdCargoTarjeta = CInt(IIf(IsDBNull(lector.Item("IdCargoTarjeta")) = True, "", lector.Item("IdCargoTarjeta")))
                'obj.ct_Nombre = CStr(IIf(IsDBNull(lector.Item("ct_Nombre")) = True, "", lector.Item("ct_Nombre")))
                obj.EstadoAE = CBool(IIf(IsDBNull(lector.Item("ct_Estado")) = True, 0, lector.Item("ct_Estado")))
                lista.Add(obj)
            End While
            lector.Close()
            cn.Close()
        Catch ex As Exception
        Finally

        End Try
        Return lista
    End Function
    Public Function SelectAllActivoxNombre_Cbo(ByVal Nombre As String) As List(Of Entidades.CargoTarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoTarjetaSelectAllActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ct_Nombre", Nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CargoTarjeta)
                Do While lector.Read
                    Dim obj As New Entidades.CargoTarjeta
                    obj.IdCargoTarjeta = CInt(lector.Item("IdCargoTarjeta"))
                    'obj.ct_Nombre = CStr(IIf(IsDBNull(lector("ct_Nombre")), "---", lector("ct_Nombre")))
                    obj.ct_Valor = CInt(IIf(IsDBNull(lector("ct_Valor")), "---", lector("ct_Valor")))
                    obj.ct_Estado = CStr(IIf(IsDBNull(lector("ct_Estado")), "---", lector("ct_Estado")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


End Class

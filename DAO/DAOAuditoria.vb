'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOAuditoria
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaAuditoria(ByVal auditoria As Entidades.Auditoria) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = auditoria.IdPersona
        ArrayParametros(1) = New SqlParameter("@aud_NomTabla", SqlDbType.VarChar)
        ArrayParametros(1).Value = auditoria.NomTabla
        ArrayParametros(2) = New SqlParameter("@aud_IdTabla", SqlDbType.VarChar)
        ArrayParametros(2).Value = auditoria.IdTabla
        ArrayParametros(3) = New SqlParameter("@aud_FechaTrans", SqlDbType.DateTime)
        ArrayParametros(3).Value = auditoria.FechaTrans
        ArrayParametros(4) = New SqlParameter("@aud_TipoTransaccion", SqlDbType.Char)
        ArrayParametros(4).Value = auditoria.TipoTransaccion
        Return HDAO.Insert(cn, "_AuditoriaInsert", ArrayParametros)
    End Function
    Public Function ActualizaAuditoria(ByVal auditoria As Entidades.Auditoria) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = auditoria.IdPersona
        ArrayParametros(1) = New SqlParameter("@aud_NomTabla", SqlDbType.VarChar)
        ArrayParametros(1).Value = auditoria.NomTabla
        ArrayParametros(2) = New SqlParameter("@aud_IdTabla", SqlDbType.VarChar)
        ArrayParametros(2).Value = auditoria.IdTabla
        ArrayParametros(3) = New SqlParameter("@aud_FechaTrans", SqlDbType.DateTime)
        ArrayParametros(3).Value = auditoria.FechaTrans
        ArrayParametros(4) = New SqlParameter("@aud_TipoTransaccion", SqlDbType.Char)
        ArrayParametros(4).Value = auditoria.TipoTransaccion
        ArrayParametros(5) = New SqlParameter("@IdAuditoria", SqlDbType.Int)
        ArrayParametros(5).Value = auditoria.IdAuditoria
        Return HDAO.Update(cn, "_AuditoriaUpdate", ArrayParametros)
    End Function
End Class

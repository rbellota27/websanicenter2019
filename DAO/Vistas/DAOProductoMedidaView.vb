﻿Imports System.Data.SqlClient
Public Class DAOProductoMedidaView
    Private objConexion As New Conexion





    Public Function SelectxIdProducto(ByVal idProducto As Integer) As List(Of Entidades.ProductoMedidaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoMedidaViewSelectxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoMedidaView)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoMedidaView
                    obj.Escalar = CDec(IIf(IsDBNull(lector.Item("pm_Escalar")) = True, 0, lector.Item("pm_Escalar")))
                    obj.IdMagnitud = CInt(IIf(IsDBNull(lector.Item("IdMagnitud")) = True, 0, lector.Item("IdMagnitud")))
                    obj.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    obj.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                    obj.NombreMagnitud = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.pm_principal = CBool(IIf(IsDBNull(lector.Item("pm_principal")) = True, False, lector.Item("pm_principal")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectPesoxIdProducto(ByVal idProducto As Integer) As Entidades.ProductoMedidaView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoMedidaViewSelectPesoxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.ProductoMedidaView
                If lector.Read Then
                    obj.Escalar = CDec(IIf(IsDBNull(lector.Item("pm_Escalar")) = True, 0, lector.Item("pm_Escalar")))
                    obj.NombreCortoUMedida = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                Else
                    obj = Nothing
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

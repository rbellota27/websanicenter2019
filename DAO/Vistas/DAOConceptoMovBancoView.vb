﻿''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 03/02/2010 12:55 am

Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOConceptoMovBancoView
    Inherits DAOConceptoMovBanco

    Public Function SelectxParams(ByVal x As Entidades.ConceptoMovBancoView) As List(Of Entidades.ConceptoMovBancoView)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(5) {}

        Prm(0) = New SqlParameter("@IdConceptoMovBanco", SqlDbType.Int)
        Prm(0).Value = x.Id
        Prm(1) = New SqlParameter("@cban_Descripcion", SqlDbType.VarChar)
        Prm(1).Value = x.Descripcion
        Prm(2) = New SqlParameter("@cban_DescripcionBreve", SqlDbType.VarChar)
        Prm(2).Value = x.DescripcionBreve
        Prm(3) = New SqlParameter("@cban_EstadoAutoAprobacion", SqlDbType.Bit)
        Prm(3).Value = x.EstadoAutoAprobacion
        Prm(4) = New SqlParameter("@cban_Estado", SqlDbType.Bit)
        Prm(4).Value = x.Estado
        Prm(5) = New SqlParameter("@IdTipoConceptoBanco", SqlDbType.Int)
        'Prm(5).Value = x.IdTipoConceptoBanco
        Prm(5).Value = IIf(IsNothing(x.IdTipoConceptoBanco), DBNull.Value, x.IdTipoConceptoBanco)
        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_ConceptoMovBancoViewSelectxParams", Prm)

                Dim Lista As New List(Of Entidades.ConceptoMovBancoView)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.ConceptoMovBancoView

                        obj.Id = CInt(.Item("IdConceptoMovBanco"))
                        obj.Descripcion = CStr(.Item("cban_Descripcion"))
                        obj.DescripcionBreve = CStr(.Item("cban_DescripcionBreve"))
                        obj.EstadoAutoAprobacion = CBool(.Item("cban_EstadoAutoAprobacion"))
                        obj.Estado = CBool(.Item("cban_Estado"))
                        obj.IdTipoConceptoBanco = CInt(.Item("IdTipoConceptoBanco"))

                        obj.TipoConceptoBanco = CStr(.Item("TipoConceptoBanco"))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.ConceptoMovBancoView)

        Dim x As New Entidades.ConceptoMovBancoView
        'En el new de la clase base ConceptoMovBanco se especifica esos valores

        Return SelectxParams(x)

    End Function

    Public Function SelectAllActivo() As List(Of Entidades.ConceptoMovBancoView)

        Dim x As New Entidades.ConceptoMovBancoView
        x.Estado = True
        Return SelectxParams(x)

    End Function

    Public Function SelectAllInactivo() As List(Of Entidades.ConceptoMovBancoView)

        Dim x As New Entidades.ConceptoMovBancoView
        x.Estado = False
        Return SelectxParams(x)

    End Function

    Public Function SelectxIdTipoConceptoBanco(ByVal Id As Integer) As List(Of Entidades.ConceptoMovBancoView)

        Dim x As New Entidades.ConceptoMovBancoView
        x.Estado = True
        x.IdTipoConceptoBanco = Id
        Return SelectxParams(x)

    End Function
End Class

﻿Imports System.Data.SqlClient
Public Class DAOTelefonoView
    Dim objConexion As New Conexion
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.TelefonoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TelefonoViewxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TelefonoView)
                Do While lector.Read
                    Dim objTelefonoView As New Entidades.TelefonoView
                    objTelefonoView.Id = CInt(IIf(IsDBNull(lector.Item("idTelefono")) = True, 0, lector.Item("idTelefono")))
                    objTelefonoView.Numero = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    objTelefonoView.Anexo = CStr(IIf(IsDBNull(lector.Item("tel_Anexo")) = True, "", lector.Item("tel_Anexo")))
                    objTelefonoView.Prioridad = CBool(IIf(IsDBNull(lector.Item("tel_Prioridad")) = True, Nothing, lector.Item("tel_Prioridad")))
                    objTelefonoView.IdTipoTelefono = CInt(IIf(IsDBNull(lector.Item("IdTipoTelefono")) = True, 0, lector.Item("IdTipoTelefono")))
                    objTelefonoView.DescTipoTel = CStr(IIf(IsDBNull(lector.Item("ttel_Nombre")) = True, "", lector.Item("ttel_Nombre")))
                    Lista.Add(objTelefonoView)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

﻿Imports System.Data.SqlClient
Public Class DAOTipoDocRef_TipoDoc_View
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Dim cn As SqlConnection = objConexion.ConexionSIGE

    Public Function SelectxIdTipoDoc(ByVal IdTipoDoc As Integer) As List(Of Entidades.TipoDocRef_TipoDoc_View)

        Dim cmd As New SqlCommand("_TipoDocumentoRef_TipoDocumentoViewSelectxIdDoc", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDoc)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocRef_TipoDoc_View)
                Do While lector.Read
                    Dim TipoDocRef_TipoDoc_View As New Entidades.TipoDocRef_TipoDoc_View
                    TipoDocRef_TipoDoc_View.IdTipoDoc = CInt(lector.Item("IdTipoDocumento"))
                    TipoDocRef_TipoDoc_View.IdTipoDocRef = CInt(lector.Item("IdTipoDocumentoRef"))
                    TipoDocRef_TipoDoc_View.Nombre = CStr(lector.Item("tdoc_NombreCorto"))

                    Lista.Add(TipoDocRef_TipoDoc_View)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    Public Sub InsertaListaTipoDocumentoRef(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.TipoDocRef_TipoDoc_View), ByVal IdTipoDocumento As Integer)

        Dim fila As New Entidades.TipoDocRef_TipoDoc_View

        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = IdTipoDocumento
        For Each fila In lista
            ArrayParametros(1) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.Int)
            ArrayParametros(1).Value = fila.IdTipoDocRef
            HDAO.InsertaT(cn, "_TipoDocumentoRefInsert", ArrayParametros, tr)
        Next
    End Sub

    Public Function DeletexIdDocumento(ByVal IdDocumento As Integer, Optional ByVal IdTipoDocumentoRef As Integer = 0) As Boolean
        Try

            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            ArrayParametros(0).Value = IdDocumento
            ArrayParametros(1) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.Int)
            ArrayParametros(1).Value = IdTipoDocumentoRef
            Return HDAO.DeletexParams(cn, "_TipoDocumentoRefDelete", ArrayParametros)
        Catch ex As Exception
            Return False
        Finally

        End Try

    End Function
End Class

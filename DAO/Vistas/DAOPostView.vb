﻿

Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOPostView
    Inherits DAOPost

    Public Function SelectIdPosxTipoTarjetaCaja(ByVal IdTarjeta As Integer, ByVal IdTipoTarjeta As Integer, ByVal IdCaja As Integer) As Integer
        Dim IdPos As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_IdPosxTipoTarjetaCaja]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTarjeta", IdTarjeta)
        cmd.Parameters.AddWithValue("@IdTipoTarjeta", IdTipoTarjeta)
        cmd.Parameters.AddWithValue("@IdCaja", IdCaja)

        Try
            Using cn
                cn.Open()
                IdPos = CInt(cmd.ExecuteScalar())
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return IdPos
    End Function

    Public Function SelectCboxIdCaja(ByVal IdCaja As Integer) As List(Of Entidades.Post)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PostSelectCboxIdCaja", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCaja", IdCaja)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Post)
                Do While lector.Read
                    Dim obj As New Entidades.Post
                    With obj
                        .Id = CInt(IIf(IsDBNull(lector("IdPost")) = True, 0, lector("IdPost")))
                        .Descripcion = CStr(IIf(IsDBNull(lector("po_Descripcion")) = True, "", lector("po_Descripcion")))
                    End With

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxParams(ByVal x As Entidades.PostView) As List(Of Entidades.PostView)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(5) {}

        Prm(0) = New SqlParameter("@IdPost", SqlDbType.Int)
        Prm(0).Value = x.Id
        Prm(1) = New SqlParameter("@po_Descripcion", SqlDbType.VarChar)
        Prm(1).Value = x.Descripcion
        Prm(2) = New SqlParameter("@po_Identificador", SqlDbType.VarChar)
        Prm(2).Value = x.Identificador
        Prm(3) = New SqlParameter("@po_Estado", SqlDbType.Bit)
        Prm(3).Value = x.Estado
        Prm(4) = New SqlParameter("@IdBanco", SqlDbType.Int)
        Prm(4).Value = x.IdBanco
        Prm(5) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        Prm(5).Value = x.IdCuentaBancaria

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_PostViewSelectxParams", Prm)


                Dim Lista As New List(Of Entidades.PostView)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.PostView

                        obj.Id = CInt(.Item("IdPost"))
                        obj.Descripcion = CStr(.Item("po_Descripcion"))
                        obj.Identificador = CStr(.Item("po_Identificador"))
                        obj.Estado = CBool(.Item("po_Estado"))
                        obj.IdBanco = CInt(.Item("IdBanco"))
                        obj.IdCuentaBancaria = CInt(.Item("IdCuentaBancaria"))

                        obj.Banco = CStr(.Item("ban_Nombre"))
                        obj.CuentaBancaria = CStr(.Item("cb_numero"))

                        obj.CuentaBancariaCtaContable = CStr(.Item("cb_CuentaContable"))
                        obj.BancoCodigoSunat = CStr(.Item("ban_CodigoSunat"))

                        obj.IdPersona = CInt(.Item("IdPersona"))
                        obj.Persona = CStr(.Item("per_Ncomercial"))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.PostView)

        Dim x As New Entidades.PostView
        'En el new de la clase base Post se especifica esos valores
        'x.Id = 0
        'x.Descripcion = ""
        'x.Identificador = ""
        'x.Estado = Nothing
        'x.IdBanco = 0
        'x.IdCuentaBancaria = 0
        'x.IdTipoTarjeta = 0

        Return SelectxParams(x)

    End Function

    Public Function SelectAllActivo() As List(Of Entidades.PostView)

        Dim x As New Entidades.PostView
        x.Estado = True
        Return SelectxParams(x)

    End Function

    Public Function SelectAllInactivo() As List(Of Entidades.PostView)

        Dim x As New Entidades.PostView
        x.Estado = False
        Return SelectxParams(x)

    End Function

    Public Function SelectxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.PostView)

        Dim x As New Entidades.PostView
        x.Estado = True
        x.IdBanco = IdBanco
        Return SelectxParams(x)

    End Function

    Public Function SelectxIdCuentaBancaria(ByVal IdCuentaBancaria As Integer) As List(Of Entidades.PostView)

        Dim x As New Entidades.PostView
        x.Estado = True
        x.IdCuentaBancaria = IdCuentaBancaria
        Return SelectxParams(x)

    End Function

    Public Function SelectxIdBancoxIdCuentaBancaria(ByVal IdBanco As Integer, _
        ByVal IdCuentaBancaria As Integer) As List(Of Entidades.PostView)

        Dim x As New Entidades.PostView
        x.Estado = True
        x.IdBanco = IdBanco
        x.IdCuentaBancaria = IdCuentaBancaria
        Return SelectxParams(x)

    End Function
End Class

﻿





'********************  MARTES 13 ABRIL 2010 HORA 02_26 PM







Imports System.Data.SqlClient
Public Class DAOProductoUMView
    Dim objConexion As New Conexion
    Public Function SelectUMPrincipalxIdProducto(ByVal IdProducto As Integer) As Entidades.ProductoUMView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMViewSelectUMPrincipalxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objPUMView As Entidades.ProductoUMView = Nothing
                If lector.Read Then
                    objPUMView = New Entidades.ProductoUMView
                    objPUMView.Equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))
                    objPUMView.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objPUMView.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                    objPUMView.NombreCortoUM = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objPUMView.Retazo = CBool(IIf(IsDBNull(lector.Item("pum_Retazo")) = True, False, lector.Item("pum_Retazo")))
                    objPUMView.UnidadPrincipal = CBool(IIf(IsDBNull(lector.Item("pum_UnidadPrincipal")) = True, False, lector.Item("pum_UnidadPrincipal")))
                    objPUMView.Estado = CStr(IIf(IsDBNull(lector.Item("pum_Estado")) = True, "", lector.Item("pum_Estado")))
                End If
                lector.Close()
                Return objPUMView
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdProducto(ByVal IdProducto As Integer) As List(Of Entidades.ProductoUMView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMViewSelectxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoUMView)
                Do While lector.Read
                    Dim objPUMView As New Entidades.ProductoUMView
                    objPUMView.Equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))
                    objPUMView.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objPUMView.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                    objPUMView.NombreCortoUM = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objPUMView.Retazo = CBool(IIf(IsDBNull(lector.Item("pum_Retazo")) = True, False, lector.Item("pum_Retazo")))
                    objPUMView.UnidadPrincipal = CBool(IIf(IsDBNull(lector.Item("pum_UnidadPrincipal")) = True, False, lector.Item("pum_UnidadPrincipal")))
                    objPUMView.Estado = CStr(IIf(IsDBNull(lector.Item("pum_Estado")) = True, "", lector.Item("pum_Estado")))
                    objPUMView.CodBarraXUm = CStr(IIf(IsDBNull(lector.Item("pum_CodBarraXUm")) = True, "", lector.Item("pum_CodBarraXUm")))
                    objPUMView.pum_PorcentRetazo = CDec(IIf(IsDBNull(lector.Item("pum_PorcentRetazo")) = True, 0, lector.Item("pum_PorcentRetazo")))
                    Lista.Add(objPUMView)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboxIdProducto(ByVal IdProducto As Integer) As List(Of Entidades.ProductoUMView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMViewSelectCboxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoUMView)
                Do While lector.Read
                    Dim objPUMView As New Entidades.ProductoUMView
                    objPUMView.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                    objPUMView.NombreCortoUM = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objPUMView.UnidadMedida = CStr(IIf(IsDBNull(lector.Item("um_NombreLargo")) = True, "", lector.Item("um_NombreLargo")))
                    Lista.Add(objPUMView)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

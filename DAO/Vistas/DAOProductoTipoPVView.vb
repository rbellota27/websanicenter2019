﻿








'**************************    MARTES  30 MARZO 2010 HORA 02_14 PM

'*****4 de febrero danitza







Imports System.Data.SqlClient
Public Class DAOProductoTipoPVView
    Private objConexion As New Conexion
    Public Function SelectxSubLineaxTiendaxTipoPV(ByVal IdSubLinea As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, _
                                                  ByVal IdtipoExistencia As Integer, ByVal tabla As DataTable) As List(Of Entidades.ProductoTipoPVView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectxSubLineaxTiendaxTipoPV", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdTipoPv", IdTipoPV)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdtipoExistencia", IdtipoExistencia)
        cmd.Parameters.AddWithValue("@Tabla", tabla)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoTipoPVView)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoTipoPVView
                    obj.FlagUMPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                    obj.IdProducto = CInt(lector.Item("IdProducto"))
                    obj.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    obj.IdUMPrincipal = CInt(lector.Item("IdUnidadMedida"))
                    obj.NomProducto = CStr(lector.Item("prod_Nombre"))
                    obj.NomUMPrincipal = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("prod_Orden")) = True, 0, lector.Item("prod_Orden")))
                    obj.PorcentUtilFijo = CDec(IIf(IsDBNull(lector.Item("PorcentUtilFijo")) = True, 0, lector.Item("PorcentUtilFijo")))
                    obj.PorcentUtilVar = CDec(IIf(IsDBNull(lector.Item("PorcentUtilVar")) = True, 0, lector.Item("PorcentUtilVar")))
                    obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    obj.PrecioFinal = CDec(IIf(IsDBNull(lector.Item("PrecioFinal")) = True, 0, lector.Item("PrecioFinal")))
                    obj.UtilValor = CDec(IIf(IsDBNull(lector.Item("UtilValor")) = True, 0, lector.Item("UtilValor")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, True, lector.Item("Estado")))
                    obj.NomMoneda = CStr(IIf(IsDBNull(lector.Item("NomMoneda")) = True, "", lector.Item("NomMoneda")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.IdMonedaPV = CInt(IIf(IsDBNull(lector.Item("IdMonedaPV")) = True, 0, lector.Item("IdMonedaPV")))

                    '*********** Valores PV Público
                    obj.PVPublico = CDec(IIf(IsDBNull(lector.Item("PVPublico")) = True, 0, lector.Item("PVPublico")))
                    obj.IdMonedaPVPublico = CInt(IIf(IsDBNull(lector.Item("IdMonedaPVPublico")) = True, 0, lector.Item("IdMonedaPVPublico")))
                    obj.NomMonedaPVPublico = CStr(IIf(IsDBNull(lector.Item("NomMonedaPVPublico")) = True, "", lector.Item("NomMonedaPVPublico")))


                    '**************************** 
                    obj.CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                    obj.CodigoAntiguo = CStr(IIf(IsDBNull(lector.Item("codantiguo")) = True, "", lector.Item("codantiguo")))
                    obj.CalOtraMoneda = CDec(IIf(IsDBNull(lector.Item("CalOtraMoneda")) = True, 0, lector.Item("CalOtraMoneda")))


                    obj.IdTiendaPrincipal = CInt(IIf(IsDBNull(lector("IdTiendaPrincipal")), 0, lector("IdTiendaPrincipal")))
                    obj.IdMonedaTiendaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdMonedaTiendaPrincipal")) = True, 0, lector.Item("IdMonedaTiendaPrincipal")))
                    obj.PrecioTiendaPrincipal = CDec(IIf(IsDBNull(lector.Item("PrecioTiendaPrincipal")) = True, 0, lector.Item("PrecioTiendaPrincipal")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxSubLineaxTiendaxUnidadMedidaTipoPV(ByVal IdSubLinea As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, _
                                                 ByVal IdtipoExistencia As Integer, ByVal tabla As DataTable) As List(Of Entidades.ProductoTipoPVView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectxSubLineaxTiendaxUnidadMedidaTipoPV", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdTipoPv", IdTipoPV)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdtipoExistencia", IdtipoExistencia)
        cmd.Parameters.AddWithValue("@Tabla", tabla)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoTipoPVView)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoTipoPVView
                    obj.FlagUMPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                    obj.IdProducto = CInt(lector.Item("IdProducto"))
                    obj.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    obj.IdUMPrincipal = CInt(lector.Item("IdUnidadMedida"))
                    obj.NomProducto = CStr(lector.Item("prod_Nombre"))

                    '*********EQUIVALENCIAS
                    obj.NomUMPrincipal = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    obj.Equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))


                    obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("prod_Orden")) = True, 0, lector.Item("prod_Orden")))
                    obj.PrecioFinal = CDec(IIf(IsDBNull(lector.Item("PrecioFinal")) = True, 0, lector.Item("PrecioFinal")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, True, lector.Item("Estado")))
                    obj.NomMoneda = CStr(IIf(IsDBNull(lector.Item("NomMoneda")) = True, "", lector.Item("NomMoneda")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.IdMonedaPV = CInt(IIf(IsDBNull(lector.Item("IdMonedaPV")) = True, 0, lector.Item("IdMonedaPV")))

                    '*********** Valores PV Público
                    obj.PVPublico = CDec(IIf(IsDBNull(lector.Item("PVPublico")) = True, 0, lector.Item("PVPublico")))
                    obj.IdMonedaPVPublico = CInt(IIf(IsDBNull(lector.Item("IdMonedaPVPublico")) = True, 0, lector.Item("IdMonedaPVPublico")))
                    obj.NomMonedaPVPublico = CStr(IIf(IsDBNull(lector.Item("NomMonedaPVPublico")) = True, "", lector.Item("NomMonedaPVPublico")))


                    '**************************** 
                    obj.CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                    obj.CodigoAntiguo = CStr(IIf(IsDBNull(lector.Item("codantiguo")) = True, "", lector.Item("codantiguo")))


                    obj.IdTiendaPrincipal = CInt(IIf(IsDBNull(lector("IdTiendaPrincipal")), 0, lector("IdTiendaPrincipal")))
                    obj.IdMonedaTiendaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdMonedaTiendaPrincipal")) = True, 0, lector.Item("IdMonedaTiendaPrincipal")))
                    obj.PrecioTiendaPrincipal = CDec(IIf(IsDBNull(lector.Item("PrecioTiendaPrincipal")) = True, 0, lector.Item("PrecioTiendaPrincipal")))


                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

﻿Imports System.Data.SqlClient
Public Class DAOTiendaAreaPerfilView
    Private objConexion As New Conexion

    Public Function SelectCboTienda() As List(Of Entidades.TiendaAreaPerfilView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaAreaPerfilViewSelectCboTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TiendaAreaPerfilView)
                Do While lector.Read
                    Dim obj As New Entidades.TiendaAreaPerfilView
                    With obj
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("idtienda")) = True, 0, lector.Item("idtienda")))
                        .NomTienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboAreaxIdTienda(ByVal idtienda As Integer) As List(Of Entidades.TiendaAreaPerfilView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaAreaPerfilViewSelectCboAreaxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TiendaAreaPerfilView)
                Do While lector.Read
                    Dim obj As New Entidades.TiendaAreaPerfilView
                    With obj
                        .IdArea = CInt(IIf(IsDBNull(lector.Item("idarea")) = True, 0, lector.Item("idarea")))
                        .NomArea = CStr(IIf(IsDBNull(lector.Item("ar_nombrelargo")) = True, "", lector.Item("ar_nombrelargo")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboPerfilxIdTiendaxIdArea(ByVal idtienda As Integer, ByVal idarea As Integer) As List(Of Entidades.TiendaAreaPerfilView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaAreaPerfilViewSelectCboPerfilxIdTiendaxIdArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdArea", idarea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TiendaAreaPerfilView)
                Do While lector.Read
                    Dim obj As New Entidades.TiendaAreaPerfilView
                    With obj
                        .IdPerfil = CInt(IIf(IsDBNull(lector.Item("idperfil")) = True, 0, lector.Item("idperfil")))
                        .NomPerfil = CStr(IIf(IsDBNull(lector.Item("perf_Nombre")) = True, "", lector.Item("perf_Nombre")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboTiendaxIdPerfilxIdArea(ByVal idperfil As Integer, ByVal idarea As Integer) As List(Of Entidades.TiendaAreaPerfilView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaAreaPerfilViewSelectCboTiendaxIdPerfilxIdArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPerfil", idperfil)
        cmd.Parameters.AddWithValue("@IdArea", idarea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TiendaAreaPerfilView)
                Do While lector.Read
                    Dim obj As New Entidades.TiendaAreaPerfilView
                    obj.IdTienda = CInt(lector.Item("IdTienda"))
                    obj.NomTienda = CStr(lector.Item("tie_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

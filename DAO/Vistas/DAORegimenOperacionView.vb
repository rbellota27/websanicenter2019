﻿Imports System.Data.SqlClient

Public Class DAORegimenOperacionView
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAll() As List(Of Entidades.RegimenOperacionView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionViewSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacionView)
                Do While lector.Read
                    Dim RegOpView As New Entidades.RegimenOperacionView

                    RegOpView.IdRegimen = CInt(lector.Item("ro_id"))
                    RegOpView.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegOpView.Estado = CStr(lector.Item("ro_Estado"))
                    RegOpView.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    RegOpView.RegimenNombre = CStr(IIf(IsDBNull(lector.Item("reg_nombre")) = True, "", lector.Item("reg_nombre")))
                    RegOpView.RegimenAbv = CStr(IIf(IsDBNull(lector.Item("reg_abv")) = True, "", lector.Item("reg_abv")))
                    RegOpView.TipoOperacionNombre = CStr(IIf(IsDBNull(lector.Item("top_nombre")) = True, "", lector.Item("top_nombre")))
                    RegOpView.TipoOperacionCodSunat = CStr(IIf(IsDBNull(lector.Item("top_CodigoSunat")) = True, "", lector.Item("top_CodigoSunat")))

                    Lista.Add(RegOpView)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

﻿Imports System.Data.SqlClient
Public Class DAOCorreoView
    Dim objConexion As New Conexion
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.CorreoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CorreoViewxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CorreoView)
                Do While lector.Read
                    Dim objCorreoView As New Entidades.CorreoView
                    objCorreoView.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    objCorreoView.Id = CInt(IIf(IsDBNull(lector.Item("IdCorreo")) = True, 0, lector.Item("IdCorreo")))
                    objCorreoView.Nombre = CStr(IIf(IsDBNull(lector.Item("corr_Nombre")) = True, "", lector.Item("corr_Nombre")))
                    objCorreoView.IdTipoCorreo = CInt(IIf(IsDBNull(lector.Item("IdTipoCorreo")) = True, 0, lector.Item("IdTipoCorreo")))
                    objCorreoView.Tipo = CStr(IIf(IsDBNull(lector.Item("tcorr_Nombre")) = True, "", lector.Item("tcorr_Nombre")))
                    Lista.Add(objCorreoView)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOPais
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdProcedencia_Cbo(ByVal IdProcedencia As String) As List(Of Entidades.Pais)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PaisSelectAllActivoxProcedencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProcedencia", IdProcedencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Pais)
                Do While lector.Read
                    Dim objPais As New Entidades.Pais
                    objPais.IdPais = CStr(lector.Item("IdPais"))
                    objPais.IdProcedencia = CStr(lector.Item("IdProcedencia"))
                    objPais.Nombre = CStr(lector.Item("pa_Nombre"))
                    objPais.Estado = CBool((lector.Item("pa_Estado")))
                    Lista.Add(objPais)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectxEstadoxNombre(ByVal nombre As String, ByVal estado As String) As List(Of Entidades.Pais)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.Pais)
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_PaisSelectAllActivoxEstado", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Nombre", nombre)
            cmd.Parameters.AddWithValue("@Estado", estado)
            Dim lector As SqlDataReader = cmd.ExecuteReader
            While lector.Read
                Dim obj As New Entidades.Pais
                obj.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                obj.Nombre = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, "", lector.Item("pa_Nombre")))
                obj.IdProcedencia = CStr(IIf(IsDBNull(lector.Item("proc_Nombre")) = True, "", lector.Item("proc_Nombre")))
                obj.Estado = CBool(IIf(IsDBNull(lector.Item("pa_Estado")) = True, 0, lector.Item("pa_Estado")))
                obj.pa_Codigo = CStr(IIf(IsDBNull(lector.Item("pa_Codigo")) = True, "", lector.Item("pa_Codigo")))
                obj.pa_Base = CStr(IIf(IsDBNull(lector.Item("pa_Base")) = True, 0, lector.Item("pa_Base")))
                If obj.pa_Base = "1" Then
                    obj.pa_BaseBit = True
                ElseIf obj.pa_Base = "0" Then
                    obj.pa_BaseBit = False
                End If
                lista.Add(obj)
            End While
            lector.Close()
            cn.Close()
        Catch ex As Exception
        End Try
        Return lista
    End Function

    Public Function SelectAllActivoxNoPais_Cbo(ByVal NomPais As String) As List(Of Entidades.Pais)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PaisSelectAllActivoxNomPais", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NomPais", NomPais)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Pais)
                Do While lector.Read
                    Dim objPais As New Entidades.Pais
                    objPais.IdPais = CStr(lector.Item("IdPais"))
                    objPais.IdProcedencia = CStr(lector.Item("proc_Nombre"))
                    objPais.Nombre = CStr(lector.Item("pa_Nombre"))
                    objPais.Estado = CBool((lector.Item("pa_Estado")))
                    objPais.pa_Codigo = CStr(IIf(IsDBNull(lector.Item("pa_Codigo")) = True, "", lector.Item("pa_Codigo")))
                    objPais.pa_Base = CStr(IIf(IsDBNull(lector.Item("pa_Base")) = True, 0, lector.Item("pa_Base")))
                    Lista.Add(objPais)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.Pais)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PaisSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Pais)
                Do While lector.Read
                    Dim objPais As New Entidades.Pais
                    objPais.IdPais = LTrim(RTrim(CStr((lector.Item("IdPais")))))
                    objPais.Nombre = CStr(lector.Item("pa_Nombre"))
                    Lista.Add(objPais)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectAllActivoTodos() As List(Of Entidades.Pais)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PaisSelectAllActivoTodos", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Pais)
                Do While lector.Read
                    Dim objPais As New Entidades.Pais
                    objPais.IdPais = LTrim(RTrim(CStr((lector.Item("IdPais")))))
                    objPais.Nombre = CStr(lector.Item("pa_Nombre"))
                    Lista.Add(objPais)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectxIdProcedencia(ByVal IdPersona As Integer) As Entidades.Pais
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objUsuario As Entidades.Pais = Nothing
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_PaisSelectAllActivoxProcedencia", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProcedencia", IdPersona)
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                objUsuario = New Entidades.Pais
                With objUsuario
                    .IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    .Estado = CBool(IIf(IsDBNull(lector.Item("pa_Estado")) = True, "", lector.Item("pa_Estado")))
                    .Nombre = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, Nothing, lector.Item("pa_Nombre")))
                    .IdProcedencia = CStr(IIf(IsDBNull(lector.Item("IdProcedencia")) = True, Nothing, lector.Item("IdProcedencia")))

                End With
            End If

            lector.Close()
        Catch ex As Exception
            Throw ex
        End Try
        Return objUsuario
    End Function

    Public Function SelectxIdPais(ByVal IdPais As Integer) As Entidades.Pais
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objUsuario As Entidades.Pais = Nothing
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_PaisSelectAllActivoxIdPais", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdPais", IdPais)
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                objUsuario = New Entidades.Pais
                With objUsuario
                    .IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    .Estado = CBool(IIf(IsDBNull(lector.Item("pa_Estado")) = True, "", lector.Item("pa_Estado")))
                    .Nombre = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, Nothing, lector.Item("pa_Nombre")))
                    .IdProcedencia = CStr(IIf(IsDBNull(lector.Item("IdProcedencia")) = True, Nothing, lector.Item("IdProcedencia")))
                    .pa_Codigo = CStr(IIf(IsDBNull(lector.Item("pa_Codigo")) = True, "", lector.Item("pa_Codigo")))
                    .pa_Base = CStr(IIf(IsDBNull(lector.Item("pa_Base")) = True, "", lector.Item("pa_Base")))

                End With
            End If

            lector.Close()
        Catch ex As Exception
            Throw ex
        End Try
        Return objUsuario
    End Function

    Public Function InsertaPais(ByVal pais As Entidades.Pais, ByVal lista As List(Of Entidades.Procedencia)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        'Dim tr As SqlTransaction = Nothing
        'Try
        '    cn.Open()
        '    tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdPais", SqlDbType.Char)
        ArrayParametros(0).Value = IIf(pais.IdPais = Nothing, DBNull.Value, pais.IdPais)
        ArrayParametros(1) = New SqlParameter("@IdProcedencia", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(pais.IdProcedencia = Nothing, DBNull.Value, pais.IdProcedencia)
        ArrayParametros(2) = New SqlParameter("@pa_Nombre", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(pais.Nombre = Nothing, DBNull.Value, pais.Nombre)

        ArrayParametros(3) = New SqlParameter("@pa_Codigo", SqlDbType.Char, 1)
        ArrayParametros(3).Value = IIf(pais.pa_Codigo = Nothing, DBNull.Value, pais.pa_Codigo)

        ArrayParametros(4) = New SqlParameter("@pa_Base", SqlDbType.Char, 1)
        ArrayParametros(4).Value = IIf(pais.pa_Base = Nothing, DBNull.Value, pais.pa_Base)

        ArrayParametros(5) = New SqlParameter("@pa_Estado", SqlDbType.Bit)
        ArrayParametros(5).Value = IIf(pais.Estado = Nothing, DBNull.Value, pais.Estado)


        'HDAO.InsertaT(cn, "_PaisInsert", ArrayParametros, tr)

        Dim cmd As New SqlCommand("_PaisInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
        Return True

        '**************inserto en Perfilusuario

        'Dim objDaoProcedencia As New DAO.DAOProcedencia
        'For i As Integer = 0 To lista.Count - 1
        'objDaoProcedencia.Insert(cn, lista(i), tr)
        ' Next
        'tr.Commit()
        'Return True
        'Catch ex As Exception
        '    tr.Rollback()
        '    Return False
        'Finally
        '    If cn.State = ConnectionState.Open Then cn.Close()
        'End Try
    End Function
    Public Function ActualizaUsuario(ByVal usuario As Entidades.Pais, ByVal lista As List(Of Entidades.Procedencia)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim objDaoProcedencia As New DAO.DAOProcedencia

        '************************* obtengo la lista original
        Dim listaOld As List(Of Entidades.Procedencia) = objDaoProcedencia.PerfilUsuarioSelectxIdPersona(CInt(usuario.IdPais))
        Dim i As Integer = listaOld.Count - 1
        While i >= 0
            Dim j As Integer = lista.Count - 1
            While j >= 0
                If listaOld.Item(i).IdProcedencia = lista.Item(j).IdProcedencia And listaOld.Item(i).IdPais = lista.Item(j).IdPais Then
                    listaOld.RemoveAt(i)
                    Exit While
                End If
                j = j - 1
            End While
            i = i - 1
        End While
        Dim PEstado As Byte = 0
        'Select Case usuario.Estado
        '   Case True
        'PEstado = "1"
        '   Case False
        'PEstado = "0"
        'End Select
        If (usuario.Estado = True) Then
            PEstado = 1
        End If
        If (usuario.Estado = False) Then
            PEstado = 0
        End If

        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_PaisUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
            ArrayParametros(0) = New SqlParameter("@IdPais", SqlDbType.Char)
            ArrayParametros(0).Value = IIf(usuario.IdPais = Nothing, DBNull.Value, usuario.IdPais)
            ArrayParametros(1) = New SqlParameter("@IdProcedencia", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(usuario.IdProcedencia = Nothing, DBNull.Value, usuario.IdProcedencia)
            ArrayParametros(2) = New SqlParameter("@pa_Nombre", SqlDbType.VarChar)
            ArrayParametros(2).Value = IIf(usuario.Nombre = Nothing, DBNull.Value, usuario.Nombre)

            ArrayParametros(3) = New SqlParameter("@pa_Codigo", SqlDbType.Char, 1)
            ArrayParametros(3).Value = IIf(usuario.pa_Codigo = Nothing, DBNull.Value, usuario.pa_Codigo)
            ArrayParametros(4) = New SqlParameter("@pa_Base", SqlDbType.Char, 1)
            ArrayParametros(4).Value = IIf(usuario.pa_Base = Nothing, DBNull.Value, usuario.pa_Base)

            ArrayParametros(5) = New SqlParameter("@pa_Estado", SqlDbType.Bit)
            ArrayParametros(5).Value = PEstado
            cmd.Parameters.AddRange(ArrayParametros)
            If cmd.ExecuteNonQuery = 0 Then
                Throw New Exception
            End If

            '********************* elimino los registros no existentes
            'For i = 0 To listaOld.Count - 1
            'objDaoProcedencia.DeletexIdPersonaxIdPerfil(cn, listaOld(i), tr)
            ' Next

            '********************* registro los nuevos productos
            Dim objUtil As New DAO.DAOUtil
            'For i = 0 To lista.Count - 1
            'If objUtil.ValidarxDosParametros(cn, tr, "PerfilUsuario", "IdPersona", lista(i).IdPersona.ToString, "IdPerfil", lista(i).IdPerfil.ToString) > 0 Then
            '***************** actualizo
            'objDaoProcedencia.Update(cn, lista(i), tr)
            'Else
            '****************** inserto
            'objDaoProcedencia.Insert(cn, lista(i), tr)
            'End If
            'Next
            tr.Commit()
            Return True
        Catch ex As Exception
            tr.Rollback()
            Return False
        End Try
    End Function
    
End Class

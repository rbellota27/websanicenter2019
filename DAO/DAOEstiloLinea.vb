﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOEstiloLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.EstiloLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstiloLinea)
                Do While lector.Read
                    Dim objEstiloLinea As New Entidades.EstiloLinea
                    objEstiloLinea.IdEstilo = CInt(IIf(IsDBNull(lector.Item("IdEstilo")) = True, 0, lector.Item("IdEstilo")))
                    objEstiloLinea.NomEstilo = CStr(IIf(IsDBNull(lector.Item("NomEstilo")) = True, "", lector.Item("NomEstilo")))
                    objEstiloLinea.Abv = CStr(IIf(IsDBNull(lector.Item("NomCorto")) = True, "", lector.Item("NomCorto")))
                    objEstiloLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objEstiloLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objEstiloLinea.Estado = CBool(IIf(IsDBNull(lector.Item("el_Estado")) = True, "", lector.Item("el_Estado")))
                    Lista.Add(objEstiloLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.EstiloLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloLineaSelectAllActivoxIdLineaIdTipoExistenciav2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstiloLinea)
                Do While lector.Read
                    Dim objEstiloLinea As New Entidades.EstiloLinea
                    objEstiloLinea.IdEstiloOrden = CStr(IIf(IsDBNull(lector.Item("IdEstilo")) = True, "", lector.Item("IdEstilo")))
                    objEstiloLinea.NomEstilo = CStr(IIf(IsDBNull(lector.Item("NomEstilo")) = True, "", lector.Item("NomEstilo")))
                    objEstiloLinea.Abv = CStr(IIf(IsDBNull(lector.Item("NomCorto")) = True, "", lector.Item("NomCorto")))
                    Lista.Add(objEstiloLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaEstiloLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal listaEstiloLinea As List(Of Entidades.EstiloLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To listaEstiloLinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_EstiloLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_EstiloLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdEstilo", listaEstiloLinea.Item(i).IdEstilo)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", listaEstiloLinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@elEstado", listaEstiloLinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@elOrden", listaEstiloLinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function
End Class

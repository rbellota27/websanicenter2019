'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTelefono
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Sub InsertaTelefono(ByVal cn As SqlConnection, ByVal Ltel As List(Of Entidades.Telefono), ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Telefono As New Entidades.Telefono
        For Each Telefono In Ltel
            Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
            ArrayParametros(0) = New SqlParameter("@tel_Numero", SqlDbType.VarChar)
            ArrayParametros(0).Value = Telefono.Numero
            ArrayParametros(1) = New SqlParameter("@tel_Anexo", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(Telefono.Anexo = Nothing, DBNull.Value, Telefono.Anexo)
            ArrayParametros(2) = New SqlParameter("@tel_Prioridad", SqlDbType.Bit)
            ArrayParametros(2).Value = Telefono.Prioridad
            ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(3).Value = Telefono.IdPersona
            ArrayParametros(4) = New SqlParameter("@IdTipoTelefono", SqlDbType.Int)
            ArrayParametros(4).Value = Telefono.IdTipoTelefono

            HDAO.InsertaT(cn, "_TelefonoInsert", ArrayParametros, T)

        Next
    End Sub

    Public Sub InsertaListaTelefono(ByVal cn As SqlConnection, ByVal Ltel As List(Of Entidades.TelefonoView), ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Telefono As New Entidades.TelefonoView
        For Each Telefono In Ltel
            Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
            ArrayParametros(0) = New SqlParameter("@tel_Numero", SqlDbType.VarChar)
            ArrayParametros(0).Value = Telefono.Numero
            ArrayParametros(1) = New SqlParameter("@tel_Anexo", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(Telefono.Anexo = Nothing, DBNull.Value, Telefono.Anexo)
            ArrayParametros(2) = New SqlParameter("@tel_Prioridad", SqlDbType.Bit)
            ArrayParametros(2).Value = Telefono.Prioridad
            ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(3).Value = Telefono.IdPersona
            ArrayParametros(4) = New SqlParameter("@IdTipoTelefono", SqlDbType.Int)
            ArrayParametros(4).Value = Telefono.IdTipoTelefono

            HDAO.InsertaT(cn, "_TelefonoInsert", ArrayParametros, T)

        Next
    End Sub


    Public Function ActualizaTelefono(ByVal telefono As Entidades.Telefono) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@tel_Numero", SqlDbType.VarChar)
        ArrayParametros(0).Value = telefono.Numero
        ArrayParametros(1) = New SqlParameter("@tel_Anexo", SqlDbType.VarChar)
        ArrayParametros(1).Value = telefono.Anexo
        ArrayParametros(2) = New SqlParameter("@tel_Prioridad", SqlDbType.Bit)
        ArrayParametros(2).Value = telefono.Prioridad
        ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(3).Value = telefono.IdPersona
        ArrayParametros(4) = New SqlParameter("@IdTipoTelefono", SqlDbType.Int)
        ArrayParametros(4).Value = telefono.IdTipoTelefono
        ArrayParametros(5) = New SqlParameter("@IdTelefono", SqlDbType.Int)
        ArrayParametros(5).Value = telefono.Id
        Return HDAO.Update(cn, "_TelefonoUpdate", ArrayParametros)
    End Function
    Public Function DeletexIdPersona(ByVal cn As SqlConnection, ByVal IdPersona As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_TelefonoDelete", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

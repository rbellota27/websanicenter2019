'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOCondicionComercial
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAll(ByVal idDocumento As Integer, ByVal tipo As Integer) As List(Of Entidades.CondicionComercial)
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_listarCondicionComercialxDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idtipodocumento", idDocumento)
        'cmd.Parameters.AddWithValue("@tipo", tipo)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionComercial)
                Do While lector.Read
                    Dim CondicionComercial As New Entidades.CondicionComercial
                    CondicionComercial.Id = CInt(lector.Item("IdCondicionCC"))
                    CondicionComercial.Descripcion = CStr(lector.Item("cct_Nombre"))
                    Lista.Add(CondicionComercial)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllCondicionCoimercial() As List(Of Entidades.CondicionComercial)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionComercialSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.CondicionComercial)
                Do While Lector.Read
                    Dim obj As New Entidades.CondicionComercial
                    obj.Descripcion = CStr(IIf(IsDBNull(Lector.Item("cct_Nombre")) = True, "", Lector.Item("cct_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(Lector.Item("cct_Estado")) = True, "", Lector.Item("cct_Estado")))
                    obj.Id = CInt(IIf(IsDBNull(Lector.Item("IdCondicionCC")) = True, "", Lector.Item("IdCondicionCC")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(Lector.Item("IdTipoDocumento")) = True, "", Lector.Item("IdTipoDocumento")))
                    obj.NomTipoDocumento = CStr(IIf(IsDBNull(Lector.Item("tdoc_NombreLargo")) = True, "", Lector.Item("tdoc_NombreLargo")))
                    lista.Add(obj)
                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.CondicionComercial)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionComercialSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionComercial)
                Do While lector.Read
                    Dim obj As New Entidades.CondicionComercial
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("cct_Nombre")) = True, "", lector.Item("cct_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("cct_Estado")) = True, "", lector.Item("cct_Estado")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCondicionCC")) = True, "", lector.Item("IdCondicionCC")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, "", lector.Item("IdTipoDocumento")))
                    obj.NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreLargo")) = True, "", lector.Item("tdoc_NombreLargo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaCondicionComercial(ByVal condicioncomercial As Entidades.CondicionComercial) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@cct_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = condicioncomercial.Descripcion
        ArrayParametros(1) = New SqlParameter("@cct_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = condicioncomercial.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(2).Value = condicioncomercial.IdTipoDocumento
        Return HDAO.Insert(cn, "_CondicionComercialInsert", ArrayParametros)
    End Function
    'Public Function ActualizaCondicionComercial(ByVal condicioncomercial As Entidades.CondicionComercial) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
    '    ArrayParametros(0) = New SqlParameter("@cct_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = condicioncomercial.Descripcion
    '    ArrayParametros(1) = New SqlParameter("@cct_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = condicioncomercial.Estado
    '    ArrayParametros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
    '    ArrayParametros(2).Value = condicioncomercial.IdTipoDocumento
    '    ArrayParametros(3) = New SqlParameter("@IdCondicionCC", SqlDbType.Int)
    '    ArrayParametros(3).Value = condicioncomercial.Id
    '    Return HDAO.Update(cn, "_CondicionComercialUpdate", ArrayParametros)
    'End Function
    Public Function ActualizaCondicionComercial(ByVal condicioncomercial As Entidades.CondicionComercial) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@cct_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = condicioncomercial.Descripcion
        ArrayParametros(1) = New SqlParameter("@cct_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = condicioncomercial.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(2).Value = condicioncomercial.IdTipoDocumento
        ArrayParametros(3) = New SqlParameter("@IdCondicionCC", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(condicioncomercial.Id = Nothing, DBNull.Value, condicioncomercial.Id)
        Return HDAO.Update(cn, "_CondicionComercialUpdate", ArrayParametros)
    End Function
    'Public Function ActualizaCondicionComercial(ByVal condicioncomercial As Entidades.CondicionComercial) As Boolean ', ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
    '    ArrayParametros(0) = New SqlParameter("@cct_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = condicioncomercial.Descripcion
    '    ArrayParametros(1) = New SqlParameter("@cct_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = condicioncomercial.Estado
    '    ArrayParametros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
    '    ArrayParametros(2).Value = condicioncomercial.IdTipoDocumento
    '    ArrayParametros(3) = New SqlParameter("@IdCondicionCC", SqlDbType.Int)
    '    ArrayParametros(3).Value = IIf(condicioncomercial.Id = Nothing, DBNull.Value, condicioncomercial.Id)
    '    Dim cmd As New SqlCommand(cn, "") ', cn, tr)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddRange(ArrayParametros)
    '    If (cmd.ExecuteNonQuery = 0) Then
    '        Return False
    '    End If
    '    Return True
    'End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.CondicionComercial)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionComercialSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionComercial)
                Do While lector.Read
                    Dim obj As New Entidades.CondicionComercial
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("cct_Nombre")) = True, "", lector.Item("cct_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("cct_Estado")) = True, "", lector.Item("cct_Estado")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCondicionCC")) = True, "", lector.Item("IdCondicionCC")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, "", lector.Item("IdTipoDocumento")))
                    obj.NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreLargo")) = True, "", lector.Item("tdoc_NombreLargo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.CondicionComercial)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionComercialSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionComercial)
                Do While lector.Read
                    Dim obj As New Entidades.CondicionComercial
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("cct_Nombre")) = True, "", lector.Item("cct_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("cct_Estado")) = True, "", lector.Item("cct_Estado")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCondicionCC")) = True, "", lector.Item("IdCondicionCC")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, "", lector.Item("IdTipoDocumento")))
                    obj.NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreLargo")) = True, "", lector.Item("tdoc_NombreLargo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.CondicionComercial)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionComercialSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionComercial)
                Do While lector.Read
                    Dim obj As New Entidades.CondicionComercial
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("cct_Nombre")) = True, "", lector.Item("cct_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("cct_Estado")) = True, "", lector.Item("cct_Estado")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCondicionCC")) = True, "", lector.Item("IdCondicionCC")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, "", lector.Item("IdTipoDocumento")))
                    obj.NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreLargo")) = True, "", lector.Item("tdoc_NombreLargo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdCondicionComercial(ByVal id As Integer) As Entidades.CondicionComercial
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectxIdCondicionComercial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCondicionCC", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.CondicionComercial
                If (lector.HasRows = True) Then
                    lector.Read()
                    With obj
                        obj.NroDiasCondi = CInt(IIf(IsDBNull(lector.Item("cct_dias")) = True, 0, lector.Item("cct_dias")))
                        obj.FlagCC = CInt(IIf(IsDBNull(lector.Item("cct_flag")) = True, 0, lector.Item("cct_flag")))
                    End With
                Else
                    obj.NroDiasCondi = 0
                    obj.FlagCC = 1
                End If
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.CondicionComercial)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionComercialSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionComercial)
                Do While lector.Read
                    Dim obj As New Entidades.CondicionComercial
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("cct_Nombre")) = True, "", lector.Item("cct_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("cct_Estado")) = True, "", lector.Item("cct_Estado")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCondicionCC")) = True, "", lector.Item("IdCondicionCC")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, "", lector.Item("IdTipoDocumento")))
                    obj.NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreLargo")) = True, "", lector.Item("tdoc_NombreLargo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.CondicionComercial)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionComerciaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionComercial)
                Do While lector.Read
                    Dim obj As New Entidades.CondicionComercial
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("cct_Nombre")) = True, "", lector.Item("cct_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("cct_Estado")) = True, "", lector.Item("cct_Estado")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCondicionCC")) = True, "", lector.Item("IdCondicionCC")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, "", lector.Item("IdTipoDocumento")))
                    obj.NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreLargo")) = True, "", lector.Item("tdoc_NombreLargo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOModeloLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.ModeloLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ModeloLinea)
                Do While lector.Read
                    Dim modelo As New Entidades.ModeloLinea
                    modelo.IdModelo = LTrim(RTrim(CStr(lector.Item("IdModelo"))))
                    modelo.NomModelo = CStr(IIf(IsDBNull(lector.Item("NomModelo")) = True, "", lector.Item("NomModelo")))
                    modelo.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    modelo.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    modelo.Estado = CBool(IIf(IsDBNull(lector.Item("ml_Estado")) = True, "", lector.Item("ml_Estado")))
                    Lista.Add(modelo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.ModeloLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloLineaSelectAllActivoxIdLineaIdTipoExistenciav2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ModeloLinea)
                Do While lector.Read
                    Dim modelo As New Entidades.ModeloLinea
                    modelo.IdModelo = LTrim(RTrim(CStr(lector.Item("IdModelo"))))
                    modelo.NomModelo = CStr(IIf(IsDBNull(lector.Item("NomModelo")) = True, "", lector.Item("NomModelo")))
                    Lista.Add(modelo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaModeloLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal listaModeloLinea As List(Of Entidades.ModeloLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To listaModeloLinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_ModeloLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_ModeloLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdModelo", listaModeloLinea.Item(i).IdModelo)
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", listaModeloLinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@mlEstado", listaModeloLinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@mlOrden", listaModeloLinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function
End Class

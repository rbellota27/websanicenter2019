'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data


Public Class DAODocumento
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Dim objDaoMantenedor As New DAO.DAOMantenedor

    Public Function SelectPasesxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Pase)
        Dim ListaPase As List(Of Entidades.Pase) = New List(Of Entidades.Pase)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectPasesxIdEmpresa", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    While (lector.Read)
                        Dim objDocumento As New Entidades.Pase
                        With objDocumento
                            .IdPase = CInt(IIf(IsDBNull(lector.Item("IdPase")) = True, 0, lector.Item("IdPase")))
                            .CodigoPase = CStr(IIf(IsDBNull(lector.Item("CodigoPase")) = True, "", lector.Item("CodigoPase")))
                            .FechaInicio = CDate(IIf(IsDBNull(lector.Item("FechaInicioPase")) = True, Nothing, lector.Item("FechaInicioPase")))
                            .FechaFin = CDate(IIf(IsDBNull(lector.Item("FechaFinPase")) = True, Nothing, lector.Item("FechaFinPase")))
                            .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("FechaEmisionPase")) = True, Nothing, lector.Item("FechaEmisionPase")))
                            .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                            .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuarioPase")) = True, 0, lector.Item("IdUsuarioPase")))
                        End With
                        ListaPase.Add(objDocumento)
                    End While
                Else
                    cmd.Connection.Close()
                    ListaPase = Nothing
                End If
                cmd.Connection.Close()
                Return ListaPase
            End Using

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function VerificandoTiendaOPVP(ByVal IdTienda As Integer) As Integer
        Dim lista As New List(Of Entidades.Documento)
        Dim Contador As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("ValidandoTiendaxOPedidoVP", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                Contador = CInt(IIf(IsDBNull(lector.Item("contador")) = True, 0, lector.Item("contador")))
            End Using
            Return Contador
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    Public Function EliminarDocumentoxIdDocumento(ByVal IdDocumento As String) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim hecho As Boolean = False
        Try
            Dim cmd As New SqlCommand("EliminarDocumentoxIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.CommandTimeout = 0
            cn.Open()

            Using cmd
                cmd.ExecuteNonQuery()
                cmd.Connection.Close()

                Return hecho = True
            End Using
        Catch ex As Exception
            Throw ex

            Return hecho = False
        Finally

        End Try

        Return hecho

    End Function
    Public Function SelectDetallexIdPase(ByVal IdPase As Integer) As List(Of Entidades.Documento)
        Dim ListaDetallePase As List(Of Entidades.Documento) = New List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectDetallexIdPase", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdPase", IdPase)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    While (lector.Read)
                        Dim objDocumento As New Entidades.Documento
                        With objDocumento
                            .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                            .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                            .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                            .Total = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                            .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                            .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                            '.IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuarioPase")) = True, 0, lector.Item("IdUsuarioPase")))
                            .NomMoneda = CStr(IIf(IsDBNull(lector.Item("Moneda")) = True, "", lector.Item("Moneda")))
                            .CadenaDocumentoRelacionado = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                        End With
                        ListaDetallePase.Add(objDocumento)
                    End While
                Else
                    ListaDetallePase = Nothing
                End If
                cmd.Connection.Close()
                Return ListaDetallePase

            End Using

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function DocumentoOCSelectDT(ByVal obj As Entidades.Documento) As DataTable
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ds As New DataSet
        Try
            Dim p() As SqlParameter = New SqlParameter(4) {}

            p(0) = objDaoMantenedor.getParam(obj.Serie, "@Serie", SqlDbType.VarChar)
            p(0).Value = IIf(obj.Serie = Nothing, DBNull.Value, obj.Serie)
            p(1) = objDaoMantenedor.getParam(obj.Codigo, "@Codigo", SqlDbType.VarChar)
            p(1).Value = IIf(obj.Codigo = Nothing, DBNull.Value, obj.Codigo)
            p(2) = objDaoMantenedor.getParam(obj.FechaFin, "@FechaFin", SqlDbType.Date)
            p(2).Value = IIf(obj.FechaFin = Nothing, DBNull.Value, obj.FechaFin)
            p(3) = objDaoMantenedor.getParam(obj.FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(3).Value = IIf(obj.FechaInicio = Nothing, DBNull.Value, obj.FechaInicio)
            p(4) = objDaoMantenedor.getParam(obj.DescripcionPersona, "@beneficiario", SqlDbType.VarChar)
            p(4).Value = IIf(obj.DescripcionPersona = Nothing, DBNull.Value, obj.DescripcionPersona)

            Dim cmd As New SqlCommand("_DocumentoOrdenCompraSelectAtencionPag", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_AtencionPago")
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return ds.Tables("DT_AtencionPago")
    End Function
    Public Function ValidarDocExterno(ByVal serie As String, ByVal codigo As String, ByVal IdTipoDocumento As Integer, ByVal IdPersona As Integer) As Entidades.Documento
        Dim objDocumento As New Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("ValidarDocExterno", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@serie", serie)
        cmd.Parameters.AddWithValue("@codigo", codigo)
        cmd.Parameters.AddWithValue("@idtipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    lector.Read()
                    With objDocumento
                        .Contador = CInt(IIf(IsDBNull(lector.Item("contador")) = True, 0, lector.Item("contador")))
                    End With
                End If

            End Using
            Return objDocumento
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectDocEOCxCodigoSerie(ByVal serie As String, ByVal codigo As String, ByVal fechainicio As Date, ByVal fechafin As Date, ByVal IdPersona As Integer) As List(Of Entidades.Documento)
        Dim ListaDocCancelacion As List(Of Entidades.Documento) = New List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectDocEOCxCodigoSerie", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@serie", serie)
        cmd.Parameters.AddWithValue("@codigo", codigo)
        cmd.Parameters.AddWithValue("@fechainicio", IIf(fechainicio = Nothing, DBNull.Value, fechainicio))
        cmd.Parameters.AddWithValue("@fechafin", IIf(fechafin = Nothing, DBNull.Value, fechafin))
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    While (lector.Read)
                        Dim objDocumento As New Entidades.Documento
                        With objDocumento
                            .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                            .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                            .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                            .Total = CDec(IIf(IsDBNull(lector.Item("Total")) = True, 0, lector.Item("Total")))
                            .Saldo = CDec(IIf(IsDBNull(lector.Item("Saldo")) = True, 0, lector.Item("Saldo")))
                            .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                            .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                            .NomMoneda = CStr(IIf(IsDBNull(lector.Item("Moneda")) = True, "", lector.Item("Moneda")))
                            .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        End With
                        ListaDocCancelacion.Add(objDocumento)
                    End While
                Else
                    ListaDocCancelacion = Nothing
                End If
            End Using
            Return ListaDocCancelacion
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectDocExternoxCodigoSerie(ByVal serie As String, ByVal codigo As String, ByVal fechainicio As Date, ByVal fechafin As Date, ByVal IdPersona As Integer) As List(Of Entidades.Documento)
        Dim ListaDocCancelacion As List(Of Entidades.Documento) = New List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectDocExternoxCodigoSerie", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@serie", serie)
        cmd.Parameters.AddWithValue("@codigo", codigo)
        cmd.Parameters.AddWithValue("@fechainicio", IIf(fechainicio = Nothing, DBNull.Value, fechainicio))
        cmd.Parameters.AddWithValue("@fechafin", IIf(fechafin = Nothing, DBNull.Value, fechafin))
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    While (lector.Read)
                        Dim objDocumento As New Entidades.Documento
                        With objDocumento
                            .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                            .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                            .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                            .Total = CDec(IIf(IsDBNull(lector.Item("Total")) = True, 0, lector.Item("Total")))
                            .Saldo = CDec(IIf(IsDBNull(lector.Item("Saldo")) = True, 0, lector.Item("Saldo")))
                            .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                            .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                            .NomMoneda = CStr(IIf(IsDBNull(lector.Item("Moneda")) = True, "", lector.Item("Moneda")))
                            .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        End With
                        ListaDocCancelacion.Add(objDocumento)
                    End While
                Else
                    ListaDocCancelacion = Nothing
                End If
            End Using
            Return ListaDocCancelacion
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectReporteDocExterno(ByVal serie As Integer, ByVal codigo As Integer, ByVal fechainicio As Date, ByVal fechafin As Date, ByVal Idtienda As Integer) As List(Of Entidades.Documento)
        Dim ListaDocCancelacion As List(Of Entidades.Documento) = New List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectReporteDocExterno", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@serie", serie)
        cmd.Parameters.AddWithValue("@codigo", codigo)
        cmd.Parameters.AddWithValue("@fechainicio", IIf(fechainicio = Nothing, DBNull.Value, fechainicio))
        cmd.Parameters.AddWithValue("@fechafin", IIf(fechafin = Nothing, DBNull.Value, fechafin))
        cmd.Parameters.AddWithValue("@IdTienda", Idtienda)


        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    While (lector.Read)
                        Dim objDocumento As New Entidades.Documento
                        With objDocumento
                            .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                            .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                            .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                            .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                            .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                            .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                            .NomMoneda = CStr(IIf(IsDBNull(lector.Item("Moneda")) = True, "", lector.Item("Moneda")))
                            .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                        End With
                        ListaDocCancelacion.Add(objDocumento)
                    End While
                End If
            End Using

            Return ListaDocCancelacion
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectDocCancelacionBxIdRQ(ByVal IdDocumento As String) As List(Of Entidades.Documento)
        Dim ListaDocCancelacion As List(Of Entidades.Documento) = New List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectDocCancelacionBxIdRQ", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@iddocumento", IdDocumento)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    lector.Read()
                    Dim objDocumento As New Entidades.Documento
                    With objDocumento
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("moneda")) = True, "", lector.Item("moneda")))
                    End With
                    ListaDocCancelacion.Add(objDocumento)
                Else
                    ListaDocCancelacion = Nothing
                End If
                Return ListaDocCancelacion
            End Using

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectReciboEgresoxIdRQ(ByVal IdDocumento As String) As List(Of Entidades.Documento)
        Dim ListaEgreso As List(Of Entidades.Documento) = New List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectReciboEgresoxIdRQ", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@iddocumento", IdDocumento)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    lector.Read()
                    Dim objDocumento As New Entidades.Documento
                    With objDocumento
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("Tipodocumento")) = True, "", lector.Item("Tipodocumento")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("Beneficiario")) = True, "", lector.Item("Beneficiario")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("Moneda")) = True, "", lector.Item("Moneda")))
                    End With
                    ListaEgreso.Add(objDocumento)
                Else
                    ListaEgreso = Nothing
                End If
                Return ListaEgreso
            End Using

        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Function
    Public Function SelectDocExtxIdRQinProgPago(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)
        Dim List As List(Of Entidades.Documento) = New List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectDocExternoxRQProgPago", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@iddocumento", IdDocumento)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    lector.Read()
                    Dim objDocumento As New Entidades.Documento
                    With objDocumento
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("fechaEmision")) = True, Nothing, lector.Item("fechaEmision")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("moneda")) = True, "", lector.Item("moneda")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("TotalApagarDocExt")) = True, 0, lector.Item("TotalApagarDocExt")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumentoRef")) = True, "", lector.Item("NroDocumentoRef")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .Saldo = CDec(IIf(IsDBNull(lector.Item("SaldoDocExt")) = True, 0, lector.Item("SaldoDocExt")))
                        .IdDocRelacionado = CInt(IIf(IsDBNull(lector.Item("IdDocumentoDocExt")) = True, 0, lector.Item("IdDocumentoDocExt")))
                    End With
                    List.Add(objDocumento)
                Else
                    List = Nothing
                End If
                Return List
            End Using

        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function SelectPermisosRQxIdPersona(ByVal IdUsuario As Integer) As Integer
        Dim lista As New List(Of Entidades.Documento)
        Dim ContadorUsu As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectPermisosRQxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@per_IdPersona", IdUsuario)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                ContadorUsu = CInt(IIf(IsDBNull(lector.Item("contador")) = True, 0, lector.Item("contador")))

            End Using
            Return ContadorUsu
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function SelectDocumentotoInsertRQ(ByVal IdDocumentoRef As Integer) As Entidades.Documento
        Dim lista As New List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectIdDocumentotoInsertRQ", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumentoRef)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()

                Dim objDocumento As New Entidades.Documento
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                End With
                Return objDocumento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function SelectDocumentoOCxIdDocRef(ByVal IdDocumentoRef As Integer) As Entidades.Documento

        Dim lista As New List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoOrdenCompraxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdDocumentoRef", IdDocumentoRef)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()

                Dim objDocumento As New Entidades.Documento
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("TotalAPagar")) = True, 0, lector.Item("TotalAPagar")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector.Item("Moneda")) = True, "", lector.Item("Moneda")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("Idusuario")) = True, 0, lector.Item("Idusuario")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                    .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("estadodoc")) = True, "", lector.Item("estadodoc")))
                End With
                Return objDocumento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function DocumentoOCSelect(ByVal serie As Integer, ByVal codigo As Integer, ByVal fechaini As Date, ByVal fechafin As Date) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoOrdenCompraSelectAtencionPag", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@Serie", serie)
        cmd.Parameters.AddWithValue("@Codigo", codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(fechafin = Nothing, DBNull.Value, fechafin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(fechaini = Nothing, DBNull.Value, fechaini))
        'cmd.Parameters.AddWithValue("@idPersona", IIf(IdPersona = Nothing, DBNull.Value, IdPersona))
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.Documento
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("TotalAPagar")) = True, 0, lector.Item("TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("Moneda")) = True, "", lector.Item("Moneda")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector.Item("Idusuario")) = True, 0, lector.Item("Idusuario")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("estadodoc")) = True, "", lector.Item("estadodoc")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                    End With
                    lista.Add(objDocumento)
                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

    End Function
    Public Function CambiarPuntoLLegada(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False
        Dim resultado As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AgregarCheckDocumentoxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cn.Open()
        Try
            If cmd.ExecuteNonQuery() <= 0 Then
                Throw New Exception
            End If
            hecho = True
        Catch ex As Exception
            hecho = False
        Finally

        End Try
        Return hecho
    End Function
    Public Function EliminarPuntoLlegada(ByVal IdDocumento As Integer) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim hecho As Boolean = False
        Try
            Dim cmd As New SqlCommand("_EliminarPuntoLlegada", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cn.Open()

            Using cmd
                cmd.ExecuteNonQuery()
                Return hecho = True
            End Using
        Catch ex As Exception
            Throw ex
            Return hecho = False
        Finally

        End Try

        Return hecho

    End Function
    Public Function InsertaPuntoLlegadaT(ByVal puntollegada As Entidades.PuntoLlegada) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(puntollegada.IdAlmacen = Nothing, DBNull.Value, puntollegada.IdAlmacen)
        ArrayParametros(1) = New SqlParameter("@pll_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = puntollegada.pll_Ubigeo
        ArrayParametros(2) = New SqlParameter("@pll_Direccion", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(puntollegada.pll_Direccion = Nothing, DBNull.Value, puntollegada.pll_Direccion)
        ArrayParametros(3) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(3).Value = puntollegada.IdDocumento
        ArrayParametros(4) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(puntollegada.IdTienda = Nothing, DBNull.Value, puntollegada.IdTienda)


        Return HDAO.Insert(cn, "_PuntoLlegadaInsert", ArrayParametros)
    End Function
    Public Function SelectIdCotizacion(ByVal IdDocumento As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectxIdCotizacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim IdDocumentoCotizacion As Integer = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    IdDocumentoCotizacion = 0
                Else
                    IdDocumentoCotizacion = CInt(IIf(IsDBNull(lector.Item("IdDocumento1")) = True, 0, lector.Item("IdDocumento1")))
                End If

                Return IdDocumentoCotizacion

            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function UpdateVendedorCotizacion(ByVal objx As Entidades.Documento) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = objx.IdDocumento
        ArrayParametros(1) = New SqlParameter("@IdUsuario", SqlDbType.Char)
        ArrayParametros(1).Value = objx.IdUsuario
        ArrayParametros(2) = New SqlParameter("@IdUsuarioComision", SqlDbType.Char)
        ArrayParametros(2).Value = objx.IdUsuarioComision

        Return HDAO.Update(cn, "_UpdateVendedorCotizacion", ArrayParametros)

    End Function
    Public Function UpdateVendedorBolFact(ByVal objx As Entidades.Documento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = objx.IdDocumento
        ArrayParametros(1) = New SqlParameter("@IdUsuarioComision", SqlDbType.Char)
        ArrayParametros(1).Value = objx.IdUsuarioComision

        Return HDAO.Update(cn, "_UpdateVendedorBolFact", ArrayParametros)
    End Function
    Public Function Documento_BuscarxTraza(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Val_FechaVcto As Boolean, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal IdEstadoEnt As Integer, ByVal IdEstadoCan As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_Documento_BuscarxTraza]", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Val_FechaVcto", Val_FechaVcto) '****** SI SE MUESTRA O NO LOS DOCUMENTOS CON FECHA DE VCTO MENOR
        cmd.Parameters.AddWithValue("@PageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)
        cmd.Parameters.AddWithValue("@IdEstadoEnt", IdEstadoEnt)
        cmd.Parameters.AddWithValue("@IdEstadoCan", IdEstadoCan)


        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))

                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))

                        .NroDocumento = .Serie + " - " + .Codigo

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function Documento_BuscarDocumentoRef_Externo(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As String, ByVal Codigo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                 ByVal Val_FechaVcto As Boolean, Optional ByVal val_CotizacionCanjeUnico As Boolean = False, Optional ByVal IdtipoDocumentoRef As Integer = 0) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_Documento_BuscarDocumentoRef_Externo]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdtipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Val_FechaVcto", Val_FechaVcto) '****** SI SE MUESTRA O NO LOS DOCUMENTOS CON FECHA DE VCTO MENOR
        cmd.Parameters.AddWithValue("@val_CotizacionCanjeUnico", val_CotizacionCanjeUnico) '****** VALIDA LAS COTIZACIONES

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExt(ByVal IdTipoDocumento As Integer, ByVal IdPersona As Integer, ByVal Serie As String, ByVal Codigo As String, ByVal pageIndex As Integer, ByVal pageSize As Integer) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)
        Dim cn As SqlConnection = (objConexion.ConexionSIGE)

        Try

            Dim p() As SqlParameter = New SqlParameter(5) {}
            p(0) = objDaoMantenedor.getParam(IdTipoDocumento, "@IdTipoDocumento", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(Serie, "@Serie", SqlDbType.VarChar)
            p(3) = objDaoMantenedor.getParam(Codigo, "@Codigo", SqlDbType.VarChar)
            p(4) = objDaoMantenedor.getParam(pageIndex, "@pageindex", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(pageSize, "@pageSize", SqlDbType.Int)

            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Documento_SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExt", p)
            While (reader.Read)

                Dim objDocumento As New Entidades.Documento
                With objDocumento

                    .Id = objDaoMantenedor.UCInt(reader("IdDocumento"))
                    .Serie = objDaoMantenedor.UCStr(reader("doc_Serie"))
                    .Codigo = objDaoMantenedor.UCStr(reader("doc_Codigo"))
                    .FechaEmision = objDaoMantenedor.UCDate(reader("doc_FechaEmision"))
                    .IdPersona = objDaoMantenedor.UCInt(reader("IdPersona"))
                    .IdEstadoDoc = objDaoMantenedor.UCInt(reader("IdEstadoDoc"))
                    .IdTienda = objDaoMantenedor.UCInt(reader("IdTienda"))
                    .IdEmpresa = objDaoMantenedor.UCInt(reader("IdEmpresa"))
                    .IdTipoDocumento = objDaoMantenedor.UCInt(reader("IdTipoDocumento"))
                    .Tienda = objDaoMantenedor.UCStr(reader("tie_Nombre"))
                    .NomEstadoDocumento = objDaoMantenedor.UCStr(reader("edoc_Nombre"))
                    .NomTipoDocumento = objDaoMantenedor.UCStr(reader("tdoc_NombreCorto"))
                    .Empresa = objDaoMantenedor.UCStr(reader("per_NComercial"))
                    .DescripcionPersona = objDaoMantenedor.UCStr(reader("DescripcionPersona"))
                    .NroDocumento = .Serie + " - " + .Codigo
                End With
                lista.Add(objDocumento)
            End While
            reader.Read()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function
    Public Function SelectxIdTipoDocumentoxIdPersonaxSeriexCodigo(ByVal IdTipoDocumento As Integer, ByVal IdPersona As Integer, ByVal Serie As Integer, ByVal Codigo As Integer) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)
        Dim cn As SqlConnection = (objConexion.ConexionSIGE)
        Try
            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(IdTipoDocumento, "@IdTipoDocumento", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(Serie, "@Serie", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Codigo, "@Codigo", SqlDbType.Int)

            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Documento_SelectxIdTipoDocumentoxIdPersonaxSeriexCodigo", p)
            While (reader.Read)

                Dim objDocumento As New Entidades.Documento
                With objDocumento

                    .Id = objDaoMantenedor.UCInt(reader("IdDocumento"))
                    .Serie = objDaoMantenedor.UCStr(reader("doc_Serie"))
                    .Codigo = objDaoMantenedor.UCStr(reader("doc_Codigo"))
                    .FechaEmision = objDaoMantenedor.UCDate(reader("doc_FechaEmision"))
                    .IdPersona = objDaoMantenedor.UCInt(reader("IdPersona"))
                    .IdEstadoDoc = objDaoMantenedor.UCInt(reader("IdEstadoDoc"))
                    .IdTienda = objDaoMantenedor.UCInt(reader("IdTienda"))
                    .IdEmpresa = objDaoMantenedor.UCInt(reader("IdEmpresa"))
                    .IdTipoDocumento = objDaoMantenedor.UCInt(reader("IdTipoDocumento"))
                    .Tienda = objDaoMantenedor.UCStr(reader("tie_Nombre"))
                    .NomEstadoDocumento = objDaoMantenedor.UCStr(reader("edoc_Nombre"))
                    .NomTipoDocumento = objDaoMantenedor.UCStr(reader("tdoc_NombreCorto"))
                    .Empresa = objDaoMantenedor.UCStr(reader("per_NComercial"))
                    .DescripcionPersona = objDaoMantenedor.UCStr(reader("DescripcionPersona"))
                    .NroDocumento = .Serie + " - " + .Codigo
                End With
                lista.Add(objDocumento)
            End While
            reader.Read()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function
    Public Function _Documento_BuscarDocumentoRefDocExterno(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                ByVal Val_FechaVcto As Boolean, Optional ByVal val_CotizacionCanjeUnico As Boolean = False, Optional ByVal IdtipoDocumentoRef As Integer = 0, Optional ByVal IdTipoOperacion As Integer = 0) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Documento_BuscarDocumentoRefDocExterno", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdtipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Val_FechaVcto", Val_FechaVcto) '****** SI SE MUESTRA O NO LOS DOCUMENTOS CON FECHA DE VCTO MENOR
        cmd.Parameters.AddWithValue("@val_CotizacionCanjeUnico", val_CotizacionCanjeUnico) '****** VALIDA LAS COTIZACIONES
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                        .strIdDocumentoRef = CStr(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, "", lector.Item("IdDocumentoRef")))
                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function Documento_BuscarDocumentoRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                 ByVal Val_FechaVcto As Boolean, Optional ByVal val_CotizacionCanjeUnico As Boolean = False, Optional ByVal IdtipoDocumentoRef As Integer = 0, Optional ByVal IdTipoOperacion As Integer = 0) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Documento_BuscarDocumentoRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdtipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Val_FechaVcto", Val_FechaVcto) '****** SI SE MUESTRA O NO LOS DOCUMENTOS CON FECHA DE VCTO MENOR
        cmd.Parameters.AddWithValue("@val_CotizacionCanjeUnico", val_CotizacionCanjeUnico) '****** VALIDA LAS COTIZACIONES
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function Documento_BuscarDocumentoRef2(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                ByVal Val_FechaVcto As Boolean, Optional ByVal val_CotizacionCanjeUnico As Boolean = False, Optional ByVal IdtipoDocumentoRef As Integer = 0, Optional ByVal IdTipoOperacion As Integer = 0) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Documento_BuscarDocumentoRefCot", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdtipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Val_FechaVcto", Val_FechaVcto) '****** SI SE MUESTRA O NO LOS DOCUMENTOS CON FECHA DE VCTO MENOR
        cmd.Parameters.AddWithValue("@val_CotizacionCanjeUnico", val_CotizacionCanjeUnico) '****** VALIDA LAS COTIZACIONES
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function Documento_Buscar(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Val_FechaVcto As Boolean, ByVal IdEstadoDoc As Integer) As List(Of Entidades.DocumentoView)
        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Documento_Buscar", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Val_FechaVcto", Val_FechaVcto) '****** SI SE MUESTRA O NO LOS DOCUMENTOS CON FECHA DE VCTO MENOR
        cmd.Parameters.AddWithValue("@IdEstadoDoc", IdEstadoDoc)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))

                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))

                        .NroDocumento = .Serie + " - " + .Codigo

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(ByVal IdTipoDocumento As Integer, ByVal IdSerie As Integer, ByVal IdPersona As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@FechaFin", FechaFin)
        cmd.Parameters.AddWithValue("@FechaInicio", FechaInicio)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)


                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))

                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .Caja = CStr(IIf(IsDBNull(lector.Item("Caja")) = True, "", lector.Item("Caja")))
                        .NroDocumento = .Serie + " - " + .Codigo

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub DocumentoVenta_DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteObservaciones As Boolean, ByVal deleteDetalleDocumento As Boolean, ByVal deleteMovALmacen As Boolean, ByVal deleteMontoRegimen As Boolean, ByVal deletePuntoLlegada As Boolean, ByVal deleteMovCaja As Boolean, ByVal deleteMovCuenta As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoVenta_DeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@deleteObservaciones", deleteObservaciones)
        cmd.Parameters.AddWithValue("@deleteMovALmacen", deleteMovALmacen)
        cmd.Parameters.AddWithValue("@deleteDetalleDocumento", deleteDetalleDocumento)
        cmd.Parameters.AddWithValue("@deleteMontoRegimen", deleteMontoRegimen)
        cmd.Parameters.AddWithValue("@deletePuntoLlegada", deletePuntoLlegada)
        cmd.Parameters.AddWithValue("@deleteMovCaja", deleteMovCaja)
        cmd.Parameters.AddWithValue("@deleteMovCuenta", deleteMovCuenta)
        cmd.Parameters.AddWithValue("@Anular", Anular)

        cmd.ExecuteNonQuery()

    End Sub
    Public Function SelectDocxDespachar(ByVal IdTienda As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try

            Dim cmd As New SqlCommand("_DocumentoSelectDocxDespachar", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)

            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While lector.Read

                Dim objDocumentoView As New Entidades.DocumentoView

                With objDocumentoView

                    .IdDocumento = CInt(lector("IdDocumento"))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")) = True, "", lector("doc_FechaEmision")))
                    .NomTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")) = True, "", lector("tie_Nombre")))
                    .RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")) = True, "", lector("jur_Rsocial")))
                    .RUC = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                    .NombreNatural = CStr(IIf(IsDBNull(lector("Nombre")) = True, "", lector("Nombre")))
                    .DNI = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))
                    .NomEstadoEntrega = CStr(IIf(IsDBNull(lector("eent_Nombre")) = True, "", lector("eent_Nombre")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector("alm_Nombre")) = True, "", lector("alm_Nombre")))
                    .NroDocumento = .Serie + " - " + .Codigo
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector("jur_Rsocial")) = True, "", lector("jur_Rsocial")))
                End With

                lista.Add(objDocumentoView)

            End While
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return lista

    End Function
    Public Function GenerarPaseVentasCompras(ByVal FechaInicioPase As String, ByVal FechaFinPase As String, ByVal Anio As String, ByVal IdUsuario As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            Dim cmd As New SqlCommand("spCn_ComprasSolucontALSige", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@desde", FechaInicioPase)
            cmd.Parameters.AddWithValue("@hasta", FechaFinPase)
            cmd.Parameters.AddWithValue("@Pan_cAnio", Anio)
            cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            tr.Commit()
            hecho = True
            cmd.Connection.Close()
        Catch ex As Exception
            tr.Rollback()
            hecho = False

            Throw ex

        Finally
            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho
    End Function
    Public Function VerificandoHistorialPases(ByVal FechaInicioPase As String, ByVal FechaFinPase As String, ByVal Anio As String) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("ValidandoPases", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@FechaInicial", FechaInicioPase)
        cmd.Parameters.AddWithValue("@FechaFinal", FechaFinPase)
        cmd.Parameters.AddWithValue("@Anio", Anio)
        cmd.CommandTimeout = 0
        Dim Contador As Integer = 0
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.Read() Then
                    Contador = CInt(IIf(IsDBNull(lector("contador")) = True, 0, lector("contador")))
                End If
            End Using
            cmd.Connection.Close()
            lector.Close()
            Return Contador
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function VerificandoSinPasar(ByVal FechaInicioPase As String, ByVal FechaFinPase As String, ByVal Anio As String) As List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("spCn_ComprasSolucontALSige_SinPasar", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@desde", FechaInicioPase)
        cmd.Parameters.AddWithValue("@hasta", FechaFinPase)
        cmd.Parameters.AddWithValue("@Pan_cAnio", Anio)
        cmd.CommandTimeout = 0
        Dim lista As New List(Of Entidades.Documento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    Do While lector.Read
                        Dim obj As New Entidades.Documento
                        With obj
                            .NroVoucherConta = CStr(IIf(IsDBNull(lector("ase_nVoucher")) = True, "", lector("ase_nVoucher")))
                            .Ruc = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                            .DescripcionPersona = CStr(IIf(IsDBNull(lector("razonSocial")) = True, "", lector("razonSocial")))
                            .Serie = CStr(IIf(IsDBNull(lector("Serie")) = True, 0, lector("Serie")))
                            .NroDocumento = CStr(IIf(IsDBNull(lector("Numero")) = True, "", lector("Numero")))
                            .FechaEmision = CDate(IIf(IsDBNull(lector("Fecha")) = True, Nothing, lector("Fecha")))
                            .CentroCosto = CStr(IIf(IsDBNull(lector("CentroCosto")) = True, "", lector("CentroCosto")))
                            .Total = CDec(IIf(IsDBNull(lector("TotalSoles")) = True, 0, lector("TotalSoles")))
                            .Concepto = CStr(IIf(IsDBNull(lector("Concepto")) = True, "", lector("Concepto")))
                            .BaseSoles = CDec(IIf(IsDBNull(lector("BaseSoles")) = True, 0, lector("BaseSoles")))
                        End With
                        lista.Add(obj)
                    Loop
                Else
                    lista = Nothing
                End If
                lector.Close()
                Return lista
            End Using

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Function
    Public Function AutoGenerarDocumentos_Salteados(ByVal IdSerie As Integer, ByVal CodigoInicio As Integer, ByVal CodigoFin As Integer, ByVal IdUsuario As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            Dim cmd As New SqlCommand("_A_AutoGenerarCodigo", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@CodigoInicio", CodigoInicio)
            cmd.Parameters.AddWithValue("@CodigoFin", CodigoFin)
            cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)

            cmd.ExecuteNonQuery()

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function GenerarNroDocumentoxIdSerie(ByVal IdSerie As Integer) As String

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoGenerarCodigoxIdSerie", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        cmd.Parameters.Add("@Codigo", SqlDbType.VarChar, 12)
        cmd.Parameters("@Codigo").Direction = ParameterDirection.Output
        cn.Open()
        cmd.ExecuteScalar()

        Dim codigo As String = CStr(IIf(IsDBNull(cmd.Parameters("@Codigo").Value) = True, "", cmd.Parameters("@Codigo").Value))
        If cn.State = ConnectionState.Open Then cn.Close()
        Return codigo
    End Function
    Public Function DeleteDocSalteadoxIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim hecho As Boolean = False
        Try
            Dim cmd As New SqlCommand("_DocumentoSalteadoDeletexIdDocumento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            If cmd.ExecuteNonQuery() <= 0 Then
                Throw New Exception
            End If
            hecho = True
        Catch ex As Exception
            hecho = False
        Finally

        End Try
        Return hecho
    End Function
    Public Function DocumentoCabSelectxId(ByVal iddocumento As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoCabSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objDocumento As New Entidades.Documento
                If lector.Read Then

                    With objDocumento

                        .Id = CInt(lector("IdDocumento"))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector("IdUsuario")) = True, "", lector("IdUsuario")))
                        .IdUsuarioComision = CInt(IIf(IsDBNull(lector("IdUsuarioComision")) = True, "", lector("IdUsuarioComision")))
                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, "", lector("IdPersona")))

                    End With

                End If
                lector.Close()
                Return objDocumento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub UpdateFechaEmision(ByVal IdDocumento As Integer, ByVal FechaEmisionNew As Date, ByVal cn As SqlConnection)
        Try
            Dim cmd As New SqlCommand("_DocumentoUpdateFechaEmision", cn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@FechaEmision", FechaEmisionNew)


            If cmd.ExecuteNonQuery <= 0 Then
                Throw New Exception
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub
    Public Function InsertaDocumento_Venta(ByVal cn As SqlConnection, ByVal documento As Entidades.Documento, ByVal T As SqlTransaction, ByVal autoGenerarCodigo As Boolean, ByVal comprometerPercepcion As Boolean) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(42) {}
        ArrayParametros(0) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(documento.Codigo = Nothing, DBNull.Value, documento.Codigo)
        ArrayParametros(1) = New SqlParameter("@doc_Serie", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(documento.Serie = Nothing, DBNull.Value, documento.Serie)
        ArrayParametros(2) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(documento.FechaEmision = Nothing, DBNull.Value, documento.FechaEmision)
        ArrayParametros(3) = New SqlParameter("@doc_FechaIniTraslado", SqlDbType.DateTime)
        ArrayParametros(3).Value = IIf(documento.FechaIniTraslado = Nothing, DBNull.Value, documento.FechaIniTraslado)
        'ArrayParametros(4) = New SqlParameter("@doc_FechaRegistro", SqlDbType.DateTime)
        'ArrayParametros(4).Value = IIf(documento.FechaRegistro = Nothing, DBNull.Value, documento.FechaRegistro)
        ArrayParametros(4) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        ArrayParametros(4).Value = IIf(documento.FechaAEntregar = Nothing, DBNull.Value, documento.FechaAEntregar)
        ArrayParametros(5) = New SqlParameter("@doc_FechaEntrega", SqlDbType.DateTime)
        ArrayParametros(5).Value = IIf(documento.FechaEntrega = Nothing, DBNull.Value, documento.FechaEntrega)
        ArrayParametros(6) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        ArrayParametros(6).Value = IIf(documento.FechaVenc = Nothing, DBNull.Value, documento.FechaVenc)
        ArrayParametros(7) = New SqlParameter("@doc_ImporteTotal", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(documento.ImporteTotal = Nothing, DBNull.Value, documento.ImporteTotal)
        ArrayParametros(8) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
        ArrayParametros(8).Value = IIf(documento.Descuento = Nothing, DBNull.Value, documento.Descuento)
        ArrayParametros(9) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
        ArrayParametros(9).Value = IIf(documento.SubTotal = Nothing, DBNull.Value, documento.SubTotal)
        ArrayParametros(10) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
        ArrayParametros(10).Value = IIf(documento.IGV = Nothing, DBNull.Value, documento.IGV)
        ArrayParametros(11) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        ArrayParametros(11).Value = IIf(documento.Total = Nothing, DBNull.Value, documento.Total)
        ArrayParametros(12) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        ArrayParametros(12).Value = IIf(documento.TotalLetras = Nothing, DBNull.Value, documento.TotalLetras)
        ArrayParametros(13) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
        ArrayParametros(13).Value = IIf(documento.TotalAPagar = Nothing, DBNull.Value, documento.TotalAPagar)
        ArrayParametros(14) = New SqlParameter("@doc_ValorReferencial", SqlDbType.Decimal)
        ArrayParametros(14).Value = IIf(documento.ValorReferencial = Nothing, DBNull.Value, documento.ValorReferencial)
        ArrayParametros(15) = New SqlParameter("@doc_Utilidad", SqlDbType.Decimal)
        ArrayParametros(15).Value = IIf(documento.Utilidad = Nothing, DBNull.Value, documento.Utilidad)
        ArrayParametros(16) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(16).Value = IIf(documento.IdPersona = Nothing, DBNull.Value, documento.IdPersona)
        ArrayParametros(17) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(17).Value = IIf(documento.IdUsuario = Nothing, DBNull.Value, documento.IdUsuario)
        ArrayParametros(18) = New SqlParameter("@IdTransportista", SqlDbType.Int)
        ArrayParametros(18).Value = IIf(documento.IdTransportista = Nothing, DBNull.Value, documento.IdTransportista)
        ArrayParametros(19) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        ArrayParametros(19).Value = IIf(documento.IdRemitente = Nothing, DBNull.Value, documento.IdRemitente)
        ArrayParametros(20) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        ArrayParametros(20).Value = IIf(documento.IdDestinatario = Nothing, DBNull.Value, documento.IdDestinatario)
        ArrayParametros(21) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        ArrayParametros(21).Value = IIf(documento.IdEstadoDoc = Nothing, DBNull.Value, documento.IdEstadoDoc)
        ArrayParametros(22) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        ArrayParametros(22).Value = IIf(documento.IdCondicionPago = Nothing, DBNull.Value, documento.IdCondicionPago)
        ArrayParametros(23) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(23).Value = IIf(documento.IdMoneda = Nothing, DBNull.Value, documento.IdMoneda)
        ArrayParametros(24) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
        ArrayParametros(24).Value = IIf(documento.LugarEntrega = Nothing, DBNull.Value, documento.LugarEntrega)
        ArrayParametros(25) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(25).Value = IIf(documento.IdTipoOperacion = Nothing, DBNull.Value, documento.IdTipoOperacion)
        ArrayParametros(26) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(26).Value = IIf(documento.IdTienda = Nothing, DBNull.Value, documento.IdTienda)
        ArrayParametros(27) = New SqlParameter("@IdSerie", SqlDbType.Int)
        ArrayParametros(27).Value = IIf(documento.IdSerie = Nothing, DBNull.Value, documento.IdSerie)
        ArrayParametros(28) = New SqlParameter("@doc_ExportadoConta", SqlDbType.Char)
        ArrayParametros(28).Value = IIf(documento.ExportadoConta = Nothing, DBNull.Value, documento.ExportadoConta)
        ArrayParametros(29) = New SqlParameter("@doc_NroVoucherConta", SqlDbType.VarChar)
        ArrayParametros(29).Value = IIf(documento.NroVoucherConta = Nothing, DBNull.Value, documento.NroVoucherConta)
        ArrayParametros(30) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(30).Value = IIf(documento.IdEmpresa = Nothing, DBNull.Value, documento.IdEmpresa)
        ArrayParametros(31) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(31).Value = IIf(documento.IdChofer = Nothing, DBNull.Value, documento.IdChofer)
        ArrayParametros(32) = New SqlParameter("@IdMotivoT", SqlDbType.Int)
        ArrayParametros(32).Value = IIf(documento.IdMotivoT = Nothing, DBNull.Value, documento.IdMotivoT)
        ArrayParametros(33) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(33).Value = IIf(documento.IdTipoDocumento = Nothing, DBNull.Value, documento.IdTipoDocumento)
        ArrayParametros(34) = New SqlParameter("@IdVehiculo", SqlDbType.Int)
        ArrayParametros(34).Value = IIf(documento.IdVehiculo = Nothing, DBNull.Value, documento.IdVehiculo)
        ArrayParametros(35) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(35).Direction = ParameterDirection.Output
        ArrayParametros(36) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
        ArrayParametros(36).Value = IIf(documento.IdEstadoCancelacion = Nothing, DBNull.Value, documento.IdEstadoCancelacion)
        ArrayParametros(37) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
        ArrayParametros(37).Value = IIf(documento.IdEstadoEntrega = Nothing, DBNull.Value, documento.IdEstadoEntrega)

        ArrayParametros(38) = New SqlParameter("@AutoGenerarCodigo", SqlDbType.Bit)
        ArrayParametros(38).Value = autoGenerarCodigo

        ArrayParametros(39) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
        ArrayParametros(39).Value = IIf(documento.IdTipoPV = Nothing, DBNull.Value, documento.IdTipoPV)

        ArrayParametros(40) = New SqlParameter("@IdCaja", SqlDbType.Int)
        ArrayParametros(40).Value = IIf(documento.IdCaja = Nothing, DBNull.Value, documento.IdCaja)

        ArrayParametros(41) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(41).Value = IIf(documento.IdAlmacen = Nothing, DBNull.Value, documento.IdAlmacen)

        ArrayParametros(42) = New SqlParameter("@doc_CompPercepcion", SqlDbType.Bit)
        ArrayParametros(42).Value = IIf(comprometerPercepcion = Nothing, DBNull.Value, comprometerPercepcion)

        Return HDAO.InsertaTParameterOutPut(cn, "_DocumentoInsert_Venta", ArrayParametros, T)

    End Function
    Public Function InsertaDocumento(ByVal cn As SqlConnection, ByVal documento As Entidades.Documento, ByVal T As SqlTransaction) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(44) {}
        ArrayParametros(0) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(documento.Codigo = Nothing, DBNull.Value, documento.Codigo)
        ArrayParametros(1) = New SqlParameter("@doc_Serie", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(documento.Serie = Nothing, DBNull.Value, documento.Serie)
        ArrayParametros(2) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(documento.FechaEmision = Nothing, DBNull.Value, documento.FechaEmision)
        ArrayParametros(3) = New SqlParameter("@doc_FechaIniTraslado", SqlDbType.DateTime)
        ArrayParametros(3).Value = IIf(documento.FechaIniTraslado = Nothing, DBNull.Value, documento.FechaIniTraslado)
        'ArrayParametros(4) = New SqlParameter("@doc_FechaRegistro", SqlDbType.DateTime)
        'ArrayParametros(4).Value = IIf(documento.FechaRegistro = Nothing, DBNull.Value, documento.FechaRegistro)
        ArrayParametros(4) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        ArrayParametros(4).Value = IIf(documento.FechaAEntregar = Nothing, DBNull.Value, documento.FechaAEntregar)
        ArrayParametros(5) = New SqlParameter("@doc_FechaEntrega", SqlDbType.DateTime)
        ArrayParametros(5).Value = IIf(documento.FechaEntrega = Nothing, DBNull.Value, documento.FechaEntrega)
        ArrayParametros(6) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        ArrayParametros(6).Value = IIf(documento.FechaVenc = Nothing, DBNull.Value, documento.FechaVenc)
        ArrayParametros(7) = New SqlParameter("@doc_ImporteTotal", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(documento.ImporteTotal = Nothing, DBNull.Value, documento.ImporteTotal)
        ArrayParametros(8) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
        ArrayParametros(8).Value = IIf(documento.Descuento = Nothing, DBNull.Value, documento.Descuento)
        ArrayParametros(9) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
        ArrayParametros(9).Value = IIf(documento.SubTotal = Nothing, DBNull.Value, documento.SubTotal)
        ArrayParametros(10) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
        ArrayParametros(10).Value = IIf(documento.IGV = Nothing, DBNull.Value, documento.IGV)
        ArrayParametros(11) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        ArrayParametros(11).Value = IIf(documento.Total = Nothing, DBNull.Value, documento.Total)
        ArrayParametros(12) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        ArrayParametros(12).Value = IIf(documento.TotalLetras = Nothing, DBNull.Value, documento.TotalLetras)
        ArrayParametros(13) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
        ArrayParametros(13).Value = IIf(documento.TotalAPagar = Nothing, DBNull.Value, documento.TotalAPagar)
        ArrayParametros(14) = New SqlParameter("@doc_ValorReferencial", SqlDbType.Decimal)
        ArrayParametros(14).Value = IIf(documento.ValorReferencial = Nothing, DBNull.Value, documento.ValorReferencial)
        ArrayParametros(15) = New SqlParameter("@doc_Utilidad", SqlDbType.Decimal)
        ArrayParametros(15).Value = IIf(documento.Utilidad = Nothing, DBNull.Value, documento.Utilidad)
        ArrayParametros(16) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(16).Value = IIf(documento.IdPersona = Nothing, DBNull.Value, documento.IdPersona)
        ArrayParametros(17) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(17).Value = IIf(documento.IdUsuario = Nothing, DBNull.Value, documento.IdUsuario)
        ArrayParametros(18) = New SqlParameter("@IdTransportista", SqlDbType.Int)
        ArrayParametros(18).Value = IIf(documento.IdTransportista = Nothing, DBNull.Value, documento.IdTransportista)
        ArrayParametros(19) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        ArrayParametros(19).Value = IIf(documento.IdRemitente = Nothing, DBNull.Value, documento.IdRemitente)
        ArrayParametros(20) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        ArrayParametros(20).Value = IIf(documento.IdDestinatario = Nothing, DBNull.Value, documento.IdDestinatario)
        ArrayParametros(21) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        ArrayParametros(21).Value = IIf(documento.IdEstadoDoc = Nothing, DBNull.Value, documento.IdEstadoDoc)
        ArrayParametros(22) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        ArrayParametros(22).Value = IIf(documento.IdCondicionPago = Nothing, DBNull.Value, documento.IdCondicionPago)
        ArrayParametros(23) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(23).Value = IIf(documento.IdMoneda = Nothing, DBNull.Value, documento.IdMoneda)
        ArrayParametros(24) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
        ArrayParametros(24).Value = IIf(documento.LugarEntrega = Nothing, DBNull.Value, documento.LugarEntrega)
        ArrayParametros(25) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(25).Value = IIf(documento.IdTipoOperacion = Nothing, DBNull.Value, documento.IdTipoOperacion)
        ArrayParametros(26) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(26).Value = IIf(documento.IdTienda = Nothing, DBNull.Value, documento.IdTienda)
        ArrayParametros(27) = New SqlParameter("@IdSerie", SqlDbType.Int)
        ArrayParametros(27).Value = IIf(documento.IdSerie = Nothing, DBNull.Value, documento.IdSerie)
        ArrayParametros(28) = New SqlParameter("@doc_ExportadoConta", SqlDbType.Char)
        ArrayParametros(28).Value = IIf(documento.ExportadoConta = Nothing, DBNull.Value, documento.ExportadoConta)
        ArrayParametros(29) = New SqlParameter("@doc_NroVoucherConta", SqlDbType.VarChar)
        ArrayParametros(29).Value = IIf(documento.NroVoucherConta = Nothing, DBNull.Value, documento.NroVoucherConta)
        ArrayParametros(30) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(30).Value = IIf(documento.IdEmpresa = Nothing, DBNull.Value, documento.IdEmpresa)
        ArrayParametros(31) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(31).Value = IIf(documento.IdChofer = Nothing, DBNull.Value, documento.IdChofer)
        ArrayParametros(32) = New SqlParameter("@IdMotivoT", SqlDbType.Int)
        ArrayParametros(32).Value = IIf(documento.IdMotivoT = Nothing, DBNull.Value, documento.IdMotivoT)
        ArrayParametros(33) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(33).Value = IIf(documento.IdTipoDocumento = Nothing, DBNull.Value, documento.IdTipoDocumento)
        ArrayParametros(34) = New SqlParameter("@IdVehiculo", SqlDbType.Int)
        ArrayParametros(34).Value = IIf(documento.IdVehiculo = Nothing, DBNull.Value, documento.IdVehiculo)
        ArrayParametros(35) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(35).Direction = ParameterDirection.Output
        ArrayParametros(36) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
        ArrayParametros(36).Value = IIf(documento.IdEstadoCancelacion = Nothing, DBNull.Value, documento.IdEstadoCancelacion)
        ArrayParametros(37) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
        ArrayParametros(37).Value = IIf(documento.IdEstadoEntrega = Nothing, DBNull.Value, documento.IdEstadoEntrega)

        ArrayParametros(38) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(38).Value = IIf(documento.IdAlmacen = Nothing, DBNull.Value, documento.IdAlmacen)

        ArrayParametros(39) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
        ArrayParametros(39).Value = IIf(documento.IdTipoPV = Nothing, DBNull.Value, documento.IdTipoPV)

        ArrayParametros(40) = New SqlParameter("@IdCaja", SqlDbType.Int)
        ArrayParametros(40).Value = IIf(documento.IdCaja = Nothing, DBNull.Value, documento.IdCaja)

        ArrayParametros(41) = New SqlParameter("@IdMedioPagoCredito", SqlDbType.Int)
        ArrayParametros(41).Value = IIf(documento.IdMedioPagoCredito = Nothing, DBNull.Value, documento.IdMedioPagoCredito)

        ArrayParametros(42) = New SqlParameter("@doc_CompPercepcion", SqlDbType.Bit)
        ArrayParametros(42).Value = IIf(documento.CompPercepcion = Nothing, DBNull.Value, documento.CompPercepcion)

        ArrayParametros(43) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.Date)
        ArrayParametros(43).Value = IIf(documento.FechaCancelacion = Nothing, DBNull.Value, documento.FechaCancelacion)

        ArrayParametros(44) = New SqlParameter("@IdUsuarioComision", SqlDbType.Int)
        ArrayParametros(44).Value = IIf(documento.IdUsuarioComision = Nothing, DBNull.Value, documento.IdUsuarioComision)

        Return HDAO.InsertaTParameterOutPut(cn, "_DocumentoInsert", ArrayParametros, T)

    End Function
    Public Function InsertaDocumentoVentaT(ByVal Documento As Entidades.Documento, ByVal LDetalle As List(Of Entidades.DetalleDocumento), _
                                 ByVal MontoRegimen As Entidades.MontoRegimen, ByVal LMovAlmacen As List(Of Entidades.MovAlmacen), ByVal PLlegada As Entidades.PuntoLlegada, _
                                Optional ByVal MovCaja As Entidades.MovCaja = Nothing, Optional ByVal listaPagoCaja As List(Of Entidades.PagoCaja) = Nothing, _
                                 Optional ByVal listaMovCuenta As List(Of Entidades.MovCuenta) = Nothing) As Integer
        Dim IdDocumento As Integer = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Using cn
            cn.Open()
            Dim T As SqlTransaction = cn.BeginTransaction(Data.IsolationLevel.Serializable)

            Try
                IdDocumento = InsertaDocumento(cn, Documento, T)

                '***Agregar DetalleDocumento y Movimiento Almac�n
                Dim daoDetalle As New DAO.DAODetalleDocumento

                For i As Integer = 0 To LDetalle.Count - 1
                    LDetalle(i).IdDocumento = IdDocumento
                    ' Dim DetalleDocumento As New Entidades.DetalleDocumento
                    'DetalleDocumento = LDetalle(i)
                    Dim IdDetalleDocumento As Integer = daoDetalle.InsertaDetalleDocumento(cn, LDetalle(i), T)
                    LMovAlmacen(i).IdDocumento = IdDocumento
                    LMovAlmacen(i).IdDetalleDocumento = IdDetalleDocumento
                Next
                '***** Agrega el Movimiento de Almac�n
                Dim daoMovAlmacen As New DAO.DAOMovAlmacen
                daoMovAlmacen.InsertaLMovAlmacen(cn, LMovAlmacen, T)

                '***Agregar EL Monto del Regimjen: Percepci�n,Detracci�n, Retenci�n, etc
                If MontoRegimen.Monto <> 0 Then
                    Dim daoMontoRegimen As New DAO.DAOMontoRegimen
                    MontoRegimen.IdDocumento = IdDocumento
                    daoMontoRegimen.InsertaMontoRegimen(cn, MontoRegimen, T)
                End If
                '*Agrega los Datos de entrega(Punto de Llegada)
                If PLlegada.pll_Direccion IsNot Nothing Or PLlegada.pll_Ubigeo <> "000000" Then
                    Dim daoPLlegada As New DAO.DAOPuntoLlegada
                    PLlegada.IdDocumento = IdDocumento
                    daoPLlegada.InsertaPuntoLlegadaT(cn, PLlegada, T)
                End If
                '***Agrega el Movimiento de Caja

                If MovCaja.IdCaja <> 0 Then
                    Dim daoMovCaja As New DAO.DAOMovCaja
                    MovCaja.IdDocumento = IdDocumento
                    daoMovCaja.InsertaMovCaja(cn, MovCaja, T)

                    '**Agrega el Pago de la Caja
                    Dim daoPagoCaja As New DAO.DAOPagoCaja
                    For i As Integer = 0 To listaPagoCaja.count - 1

                        listaPagoCaja(i).IdDocumento = IdDocumento
                        daoPagoCaja.InsertaPagoCaja(cn, listaPagoCaja(i), T)

                    Next

                End If
                '***Agrega el Movimiento de a la Cuenta de la Persona
                Dim daoMovCuenta As New DAO.DAOMovCuenta
                For i As Integer = 0 To listaMovCuenta.Count - 1

                    listaMovCuenta(i).IdDocumento = IdDocumento
                    daoMovCuenta.InsertaMovCuenta(cn, listaMovCuenta(i), T)

                Next

                T.Commit()
            Catch ex As Exception
                T.Rollback()
                IdDocumento = -1
            Finally

            End Try
            Return IdDocumento
        End Using
    End Function
    Public Function InsertaDocumentoT(ByVal documento As Entidades.Documento, ByVal LDet As List(Of Entidades.DetalleGuia), Optional ByVal afectaMovAlmacen As Boolean = False, Optional ByVal factorMov As Integer = 0, Optional ByVal IdAlmacen As Integer = 0, Optional ByVal IdMetValuacion As Integer = 0, Optional ByVal inventarioInicial As Boolean = False) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim IdDocumento As Integer = -1
        cn.Open()
        Dim T As SqlTransaction = cn.BeginTransaction(Data.IsolationLevel.Serializable)
        Try
            IdDocumento = Me.InsertaDocumento(cn, documento, T)
            Dim objDAODetalleGuia As New DAO.DAODetalleGuia
            Dim objDetGuia As New Entidades.DetalleGuia
            Dim objDaoMovAlmacen As New DAO.DAOMovAlmacen
            Dim objDaoDetMovAlmacen As New DAO.DAODetalleMovAlmacen
            Dim objDaoSaldoKardex As New DAO.DAOSaldoKardex
            For Each objDetGuia In LDet
                objDetGuia.IdDocumento = IdDocumento
                objDAODetalleGuia.InsertaDetalleGuia(cn, objDetGuia, T)
                If afectaMovAlmacen Then 'bajo el metVal PP
                    Dim objDaoProducto As New DAOProducto
                    Dim objSaldoKardexOld As Entidades.SaldoKardex = objDaoSaldoKardex.SelectxEmpresaxAlmacenxProducto(cn, documento.IdEmpresa, IdAlmacen, objDetGuia.IdProducto, T)
                    If objSaldoKardexOld Is Nothing Then
                        objSaldoKardexOld = New Entidades.SaldoKardex
                        With objSaldoKardexOld
                            .CantidadSaldo = 0
                            .CostoSaldo = 0
                            .TotalSaldo = 0
                        End With
                    End If
                    'SI ES SALIDA EL COSTO DE SALIDA ES IGUAL AL COSTO ANTERIOR
                    Dim costoCompraProducto As Decimal = objSaldoKardexOld.CostoSaldo
                    If inventarioInicial Then
                        costoCompraProducto = objDetGuia.CostoUnitSaldoInicial
                    Else
                        'SI ES INGRESO EL COSTO DE INGRESO DEBEMOS RECUPERARLO DE LA TABLA PRODUCTO
                        If factorMov = 1 Then costoCompraProducto = objDaoProducto.SelectPrecioCompra(cn, objDetGuia.IdProducto, T)
                    End If
                    Dim objMovAlmacen As New Entidades.MovAlmacen
                    'Obtengo el equivalente
                    With objMovAlmacen
                        .CantidadMov = objDaoProducto.SelectEquivalenciaUMPrincipal(cn, T, objDetGuia.IdProducto, objDetGuia.IdUnidadMedida, objDetGuia.Cantidad)
                        .Factor = factorMov
                        .IdAlmacen = IdAlmacen
                        .IdDocumento = IdDocumento
                        .IdEmpresa = documento.IdEmpresa
                        .IdMetodoV = IdMetValuacion
                        .IdProducto = objDetGuia.IdProducto
                        .IdTienda = documento.IdTienda
                        .IdTipoOperacion = documento.IdTipoOperacion
                        'Obtengo la Unidad de Medida Principal
                        .UMPrincipal = objDaoProducto.SelectUMPrincipal(cn, T, objDetGuia.IdProducto)
                        .IdDocumento = IdDocumento
                        .Comprometido = False
                        '.IdDetalleDocumento = objDaoMovAlmacen.InsertaMovAlmacen(cn, objMovAlmacen, T)
                    End With
                    Dim objDetalleMov As New Entidades.DetalleMovAlmacen
                    With objDetalleMov
                        .Cantidad = objMovAlmacen.CantidadMov
                        .Costo = costoCompraProducto
                        .Factor = objMovAlmacen.Factor
                        .IdAlmacen = objMovAlmacen.IdAlmacen
                        .IdEmpresa = objMovAlmacen.IdEmpresa
                        .IdMovAlmacen = objMovAlmacen.IdDetalleDocumento
                        .IdProducto = objMovAlmacen.IdProducto
                        .IdTienda = objMovAlmacen.IdTienda
                        .Total = .Cantidad * .Costo
                        .IdDocumento = IdDocumento
                        objDaoDetMovAlmacen.InsertaDetalleMovAlmacen(cn, objDetalleMov, T)
                    End With
                    Dim objSaldoKardexNew As New Entidades.SaldoKardex
                    With objSaldoKardexNew
                        .CantidadSaldo = objSaldoKardexOld.CantidadSaldo + factorMov * objMovAlmacen.CantidadMov
                        If .CantidadSaldo <> 0 Then
                            .CostoSaldo = (objSaldoKardexOld.CantidadSaldo * objSaldoKardexOld.CostoSaldo + factorMov * objMovAlmacen.CantidadMov * costoCompraProducto) / (.CantidadSaldo)
                        Else
                            .CostoSaldo = 0
                        End If
                        .IdAlmacen = objMovAlmacen.IdAlmacen
                        .IdEmpresa = objMovAlmacen.IdEmpresa
                        .IdMovAlmacen = objMovAlmacen.IdDetalleDocumento
                        .IdProducto = objMovAlmacen.IdProducto
                        .IdTienda = objMovAlmacen.IdTienda
                        .TotalSaldo = .CantidadSaldo * .CostoSaldo
                        .IdDocumento = IdDocumento
                        objDaoSaldoKardex.InsertaSaldoKardex(cn, objSaldoKardexNew, T)
                    End With
                End If
            Next
            T.Commit()
        Catch ex As Exception
            T.Rollback()
            IdDocumento = -1
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return IdDocumento
    End Function
    Public Function ActualizarDocumentoGuia(ByVal documento As Entidades.Documento, ByVal LDet As List(Of Entidades.DetalleGuia), ByVal IdDocumentoOld As Integer, Optional ByVal factorMov As Integer = 0, Optional ByVal IdAlmacen As Integer = 0, Optional ByVal IdMetValuacion As Integer = 0, Optional ByVal inventarioInicial As Boolean = False) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim IdDocumento As Integer = -1
        cn.Open()
        Dim T As SqlTransaction = cn.BeginTransaction(Data.IsolationLevel.Serializable)
        Try
            '********** inserto el documento
            IdDocumento = Me.InsertaDocumento(cn, documento, T)
            '********** creo objetos para trabajar en el proceso
            Dim objDAODetalleGuia As New DAO.DAODetalleGuia
            Dim objDaoMovAlmacen As New DAO.DAOMovAlmacen
            Dim objDaoDetMovAlmacen As New DAO.DAODetalleMovAlmacen
            Dim objDaoSaldoKardex As New DAO.DAOSaldoKardex
            Dim objDaoProducto As New DAO.DAOProducto
            Dim objDetGuia As New Entidades.DetalleGuia
            Dim objSaldoKardex As New Entidades.SaldoKardex
            '******** obtengo el IdMovAlmacenBase
            'Dim IdMovAlmacenBase As Integer = objDaoMovAlmacen.SelectMinIdMovAlmacenxIdDocumento(cn, T, IdDocumentoOld)
            '*********DEBO ANULAR LOS MOV PRODUCIDOS POR EL DOCUMENTO


            '************************************************************

            '****************** Elimino los Movimientos
            objDaoSaldoKardex.DeletexIdDocumento(cn, T, IdDocumentoOld)
            objDaoDetMovAlmacen.DeletexIdDocumento(cn, T, IdDocumentoOld)
            objDaoMovAlmacen.DeletexIdDocumento(cn, T, IdDocumentoOld)
            '************* recorro el detalle de la guia
            For Each objDetGuia In LDet
                'INSERTO EN MOV ALMACEN
                Dim objMovAlmacen As New Entidades.MovAlmacen(documento.IdEmpresa, IdAlmacen, documento.IdTienda, IdDocumento, objDetGuia.IdProducto, , , objDetGuia.UMedida, objDetGuia.Cantidad, , , factorMov, documento.IdTipoOperacion, IdMetValuacion)
                '  If objDaoMovAlmacen.InsertaMovAlmacen(cn, objMovAlmacen, T) <= 0 Then
                'Throw New Exception
                '  End If

                'objDetGuia.IdDocumento = IdDocumento
                'Dim cantSaldo As Decimal = 0
                'Dim costoSaldo As Decimal = 0
                'Dim objSaldokardexOld As Entidades.SaldoKardex = objDaoSaldoKardex.SelectUltimoSaldoKardexxIdMovAlmacen(IdMovAlmacenBase, objDetGuia.IdProducto, documento.IdEmpresa, IdAlmacen, cn, T)
                'If factorMov = 1 Then
                '    costoSaldo = objDaoProducto.SelectPrecioCompra(cn, objDetGuia.IdProducto, T)
                'ElseIf factorMov = -1 Then
                '    '******* Obtengo el costo del ultimo saldo
                '    costoSaldo = objSaldokardexOld.CostoSaldo
                'End If
                'objSaldokardexOld.CantidadSaldo = objSaldokardexOld.CantidadSaldo

                ''********* inserto el detalle de la guia
                'objDAODetalleGuia.InsertaDetalleGuia(cn, objDetGuia, T)







                '********** trabajamos movimientos de almacen bajo el metodo de valuaci�n de promedios ponderados
                '******** obtenemos toda la lista de los movalmacen a partir del mov a reemplazar
                'Dim listaMovAlmacen As List(Of Entidades.MovAlmacen) = objDaoMovAlmacen.SelectxEmpresaxAlmacenxProductoxMovAlmacenBase(cn, T, documento.IdEmpresa, IdAlmacen, objDetGuia.IdProducto, IdMovAlmacenBase)
                'Dim objMovAlmacen As Entidades.MovAlmacen
                '********** recorremos toda la lista
                'For Each objMovAlmacen In listaMovAlmacen
                '    If objMovAlmacen.Factor = 1 Then
                '        objSaldoKardex.CantidadSaldo = cantSaldo + objMovAlmacen.CantidadMov
                '        objSaldoKardex.CostoSaldo = cantSaldo * costoSaldo
                '    ElseIf objMovAlmacen.Factor = -1 Then
                '    End If
                'Next
            Next
            T.Commit()
        Catch ex As Exception
            T.Rollback()
            IdDocumento = -1
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return IdDocumento
    End Function
    Public Sub AnulaT(ByVal cn As SqlConnection, ByVal T As SqlTransaction, ByVal IdDocumento As Integer)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = IdDocumento

        HDAO.UpdateT(cn, "_DocumentoAnula", ArrayParametros, T)
    End Sub
    Public Function SelectMaxCodxIdSerie(ByVal IdSerie As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectMaxCodxIdSerie", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Codigo As String = Nothing
                If lector.Read() Then
                    Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                End If
                Return Codigo
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInvInicialIdDocxEmpresaxAlmacen(ByVal idempresa As Integer, ByVal idalmacen As Integer) As Integer
        Dim IdDocumento As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectInvInicialIdDocxEmpresaxAlmacen", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.Read Then
                    IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                End If
                lector.Close()
                Return IdDocumento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInvInicialCabxIdDocumento(ByVal iddocumento As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectInvInicialCabxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.Documento = Nothing
                If lector.Read Then
                    obj = New Entidades.Documento
                    With obj
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .NomTipoOperacion = CStr(IIf(IsDBNull(lector.Item("top_Nombre")) = True, "", lector.Item("top_Nombre")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                    End With
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectIdDocumentoxIdSeriexCodigo(ByVal IdSerie As Integer, ByVal codigo As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim IdDocumento As Integer = -1
        Dim cmd As New SqlCommand("_DocumentoSelectIdxIdSeriexCodigo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        cmd.Parameters.AddWithValue("@Codigo", codigo)
        Try
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, -1, lector.Item("IdDocumento")))
            Else
                IdDocumento = -1
            End If
            lector.Close()
        Catch ex As Exception
            IdDocumento = -1
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return IdDocumento
    End Function
    Public Function SelectxIdSeriexCodigo(ByVal IdSerie As Integer, ByVal codigo As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdSeriexCodigo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        cmd.Parameters.AddWithValue("@doc_Codigo", codigo)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese c�digo")
                End If
                Dim Documento As New Entidades.Documento
                With Documento
                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaIniTraslado = CDate(IIf(IsDBNull(lector.Item("doc_FechaIniTraslado")) = True, Nothing, lector.Item("doc_FechaIniTraslado")))
                    .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .Descuento = CDec(IIf(IsDBNull(lector.Item("doc_Descuento")) = True, Nothing, lector.Item("doc_Descuento")))
                    .SubTotal = CDec(IIf(IsDBNull(lector.Item("doc_SubTotal")) = True, Nothing, lector.Item("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector.Item("doc_IGV")) = True, Nothing, lector.Item("doc_IGV")))
                    .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, Nothing, lector.Item("doc_Total")))
                    .NroVoucherConta = CStr(IIf(IsDBNull(lector.Item("doc_NroVoucherConta")) = True, 0, lector.Item("doc_NroVoucherConta")))
                    '.TotalLetras = CStr(IIf(IsDBNull(lector.Item("doc_TotalLetras")) = True, Nothing, lector.Item("doc_TotalLetras")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, Nothing, lector.Item("doc_TotalAPagar")))
                    .ValorReferencial = CDec(IIf(IsDBNull(lector.Item("doc_ValorReferencial")) = True, Nothing, lector.Item("doc_ValorReferencial")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    .IdTransportista = CInt(IIf(IsDBNull(lector.Item("IdTransportista")) = True, 0, lector.Item("IdTransportista")))
                    .IdRemitente = CInt(IIf(IsDBNull(lector.Item("IdRemitente")) = True, 0, lector.Item("IdRemitente")))
                    .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")) = True, 0, lector.Item("IdCondicionPago")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    .LugarEntrega = CInt(IIf(IsDBNull(lector.Item("LugarEntrega")) = True, 0, lector.Item("LugarEntrega")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdChofer = CInt(IIf(IsDBNull(lector.Item("IdChofer")) = True, 0, lector.Item("IdChofer")))
                    .IdMotivoT = CInt(IIf(IsDBNull(lector.Item("IdMotivoT")) = True, 0, lector.Item("IdMotivoT")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .IdVehiculo = CInt(IIf(IsDBNull(lector.Item("IdVehiculo")) = True, 0, lector.Item("IdVehiculo")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")) = True, 0, lector.Item("IdEstadoCan")))
                    .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                    .FechaCancelacion = CDate(IIf(IsDBNull(lector.Item("doc_FechaCancelacion")) = True, Nothing, lector.Item("doc_FechaCancelacion")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("NomAlmacen")) = True, "", lector.Item("NomAlmacen")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                    .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                    .Retencion = CDec(IIf(IsDBNull(lector.Item("Retencion")) = True, 0, lector.Item("Retencion")))
                    .PoseeOrdenDespacho = CBool(IIf(IsDBNull(lector.Item("PoseeOrdenDespacho")) = True, 0, lector.Item("PoseeOrdenDespacho")))
                    .PoseeCompPercepcion = CBool(IIf(IsDBNull(lector.Item("PoseeCompPercepcion")) = True, 0, lector.Item("PoseeCompPercepcion")))
                    .PoseeAmortizaciones = CBool(IIf(IsDBNull(lector.Item("PoseeAmortizaciones")) = True, 0, lector.Item("PoseeAmortizaciones")))
                    .PoseeGRecepcion = CBool(IIf(IsDBNull(lector.Item("PoseeGRecepcion")) = True, 0, lector.Item("PoseeGRecepcion")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, 0, lector.Item("mon_Simbolo")))
                    .IdUsuarioComision = CInt(IIf(IsDBNull(lector.Item("IdUsuarioComision")) = True, 0, lector.Item("IdUsuarioComision")))

                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectActivoxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            cn.Open()
            Using cn
                Dim Documento As Entidades.Documento = Nothing
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If lector.Read Then
                    Documento = New Entidades.Documento
                    With Documento
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("NomTipoDocumento")) = True, "", lector.Item("NomTipoDocumento")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    End With

                End If
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub DocumentoUpdate(ByVal documento As Entidades.Documento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Try
            Dim param() As SqlParameter = New SqlParameter(44) {}
            param(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            param(0).Value = documento.Id
            param(1) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar)
            param(1).Value = IIf(documento.Codigo = Nothing, DBNull.Value, documento.Codigo)
            param(2) = New SqlParameter("@doc_Serie", SqlDbType.VarChar)
            param(2).Value = IIf(documento.Serie = Nothing, DBNull.Value, documento.Serie)
            param(3) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
            param(3).Value = IIf(documento.FechaEmision = Nothing, DBNull.Value, documento.FechaEmision)
            param(4) = New SqlParameter("@doc_FechaIniTraslado", SqlDbType.DateTime)
            param(4).Value = IIf(documento.FechaIniTraslado = Nothing, DBNull.Value, documento.FechaIniTraslado)
            param(5) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
            param(5).Value = IIf(documento.IdTipoPV = Nothing, DBNull.Value, documento.IdTipoPV)
            param(6) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
            param(6).Value = IIf(documento.FechaAEntregar = Nothing, DBNull.Value, documento.FechaAEntregar)
            param(7) = New SqlParameter("@doc_FechaEntrega", SqlDbType.DateTime)
            param(7).Value = IIf(documento.FechaEntrega = Nothing, DBNull.Value, documento.FechaEntrega)
            param(8) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
            param(8).Value = IIf(documento.FechaVenc = Nothing, DBNull.Value, documento.FechaVenc)
            param(9) = New SqlParameter("@doc_ImporteTotal", SqlDbType.Decimal)
            param(9).Value = IIf(documento.ImporteTotal = Nothing, DBNull.Value, documento.ImporteTotal)
            param(10) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
            param(10).Value = IIf(documento.Descuento = Nothing, DBNull.Value, documento.Descuento)
            param(11) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
            param(11).Value = IIf(documento.SubTotal = Nothing, DBNull.Value, documento.SubTotal)
            param(12) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
            param(12).Value = IIf(documento.IGV = Nothing, DBNull.Value, documento.IGV)
            param(13) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
            param(13).Value = IIf(documento.Total = Nothing, DBNull.Value, documento.Total)
            param(14) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
            param(14).Value = IIf(documento.TotalLetras = Nothing, DBNull.Value, documento.TotalLetras)
            param(15) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
            param(15).Value = IIf(documento.TotalAPagar = Nothing, DBNull.Value, documento.TotalAPagar)
            param(16) = New SqlParameter("@doc_ValorReferencial", SqlDbType.Decimal)
            param(16).Value = IIf(documento.ValorReferencial = Nothing, DBNull.Value, documento.ValorReferencial)
            param(17) = New SqlParameter("@doc_Utilidad", SqlDbType.Decimal)
            param(17).Value = IIf(documento.Utilidad = Nothing, DBNull.Value, documento.Utilidad)
            param(18) = New SqlParameter("@IdPersona", SqlDbType.Int)
            param(18).Value = IIf(documento.IdPersona = Nothing, DBNull.Value, documento.IdPersona)
            param(19) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            param(19).Value = IIf(documento.IdUsuario = Nothing, DBNull.Value, documento.IdUsuario)
            param(20) = New SqlParameter("@IdTransportista", SqlDbType.Int)
            param(20).Value = IIf(documento.IdTransportista = Nothing, DBNull.Value, documento.IdTransportista)
            param(21) = New SqlParameter("@IdRemitente", SqlDbType.Int)
            param(21).Value = IIf(documento.IdRemitente = Nothing, DBNull.Value, documento.IdRemitente)
            param(22) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
            param(22).Value = IIf(documento.IdDestinatario = Nothing, DBNull.Value, documento.IdDestinatario)
            param(23) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
            param(23).Value = IIf(documento.IdEstadoDoc = Nothing, DBNull.Value, documento.IdEstadoDoc)
            param(24) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
            param(24).Value = IIf(documento.IdCondicionPago = Nothing, DBNull.Value, documento.IdCondicionPago)
            param(25) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            param(25).Value = IIf(documento.IdMoneda = Nothing, DBNull.Value, documento.IdMoneda)
            param(26) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
            param(26).Value = IIf(documento.LugarEntrega = Nothing, DBNull.Value, documento.LugarEntrega)
            param(27) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            param(27).Value = IIf(documento.IdTipoOperacion = Nothing, DBNull.Value, documento.IdTipoOperacion)
            param(28) = New SqlParameter("@IdTienda", SqlDbType.Int)
            param(28).Value = IIf(documento.IdTienda = Nothing, DBNull.Value, documento.IdTienda)
            param(29) = New SqlParameter("@IdSerie", SqlDbType.Int)
            param(29).Value = IIf(documento.IdSerie = Nothing, DBNull.Value, documento.IdSerie)
            param(30) = New SqlParameter("@doc_ExportadoConta", SqlDbType.Char)
            param(30).Value = IIf(documento.ExportadoConta = Nothing, DBNull.Value, documento.ExportadoConta)
            param(31) = New SqlParameter("@doc_NroVoucherConta", SqlDbType.VarChar)
            param(31).Value = IIf(documento.NroVoucherConta = Nothing, DBNull.Value, documento.NroVoucherConta)
            param(32) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            param(32).Value = IIf(documento.IdEmpresa = Nothing, DBNull.Value, documento.IdEmpresa)
            param(33) = New SqlParameter("@IdChofer", SqlDbType.Int)
            param(33).Value = IIf(documento.IdChofer = Nothing, DBNull.Value, documento.IdChofer)
            param(34) = New SqlParameter("@IdMotivoT", SqlDbType.Int)
            param(34).Value = IIf(documento.IdMotivoT = Nothing, DBNull.Value, documento.IdMotivoT)
            param(35) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            param(35).Value = IIf(documento.IdTipoDocumento = Nothing, DBNull.Value, documento.IdTipoDocumento)
            param(36) = New SqlParameter("@IdVehiculo", SqlDbType.Int)
            param(36).Value = IIf(documento.IdVehiculo = Nothing, DBNull.Value, documento.IdVehiculo)
            param(37) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
            param(37).Value = IIf(documento.IdEstadoCancelacion = Nothing, DBNull.Value, documento.IdEstadoCancelacion)
            param(38) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
            param(38).Value = IIf(documento.IdEstadoEntrega = Nothing, DBNull.Value, documento.IdEstadoEntrega)
            param(39) = New SqlParameter("@IdCaja", SqlDbType.Int)
            param(39).Value = IIf(documento.IdCaja = Nothing, DBNull.Value, documento.IdCaja)
            param(40) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            param(40).Value = IIf(documento.IdAlmacen = Nothing, DBNull.Value, documento.IdAlmacen)
            param(41) = New SqlParameter("@doc_CompPercepcion", SqlDbType.Bit)
            param(41).Value = IIf(documento.CompPercepcion = Nothing, DBNull.Value, documento.CompPercepcion)
            param(42) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.Date)
            param(42).Value = IIf(documento.FechaCancelacion = Nothing, DBNull.Value, documento.FechaCancelacion)

            param(43) = New SqlParameter("@IdUsuarioComision", SqlDbType.Int)
            param(43).Value = IIf(documento.IdUsuarioComision = Nothing, DBNull.Value, documento.IdUsuarioComision)

            param(44) = New SqlParameter("@IdMedioPagoCredito", SqlDbType.Int)
            param(44).Value = IIf(documento.IdMedioPagoCredito = Nothing, DBNull.Value, documento.IdMedioPagoCredito)

            Dim cmd As New SqlCommand("_DocumentoUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddRange(param)

            If cmd.ExecuteNonQuery <= 0 Then

                Throw New Exception

            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try

    End Sub
    '********************************************************
    'M�todo Din, para obtener Documento por cualquier campo
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 3-Set-2009
    'Parametros : psWhere, psOrder
    'Retorna : Lista de tipo Documento
    '********************************************************
    Public Function fnSelTblDocumentoDin(ByVal psWhere As String, ByVal psOrder As String) As List(Of Entidades.Documento)
        Dim loList As New List(Of Entidades.Documento)()
        Dim par As SqlParameter() = New SqlParameter(1) {}
        par(0) = New SqlParameter("@WhereCondition", System.Data.SqlDbType.VarChar, 500)
        par(0).Value = psWhere
        par(1) = New SqlParameter("@OrderByExpression", System.Data.SqlDbType.VarChar, 250)
        par(1).Value = psOrder
        Try
            Dim loDr As SqlDataReader = SqlHelper.ExecuteReader(objConexion.ConexionSIGE, CommandType.StoredProcedure, "usp_SelDocumentoDin", par)
            While (loDr.Read())
                Dim loDocumento As New Entidades.Documento
                loDocumento.Codigo = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_Codigo"))) = True, "", loDr.GetValue(loDr.GetOrdinal("doc_Codigo")))))
                loDocumento.Serie = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_Serie"))) = True, "", loDr.GetValue(loDr.GetOrdinal("doc_Serie")))))
                loDocumento.FechaEmision = Convert.ToDateTime(CDate(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_FechaEmision"))) = True, Date.Now, loDr.GetValue(loDr.GetOrdinal("doc_FechaEmision")))))
                loDocumento.FechaIniTraslado = Convert.ToDateTime(CDate(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_FechaIniTraslado"))) = True, Date.Now, loDr.GetValue(loDr.GetOrdinal("doc_FechaIniTraslado")))))
                loDocumento.FechaAEntregar = Convert.ToDateTime(CDate(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_FechaAentregar"))) = True, Date.Now, loDr.GetValue(loDr.GetOrdinal("doc_FechaAentregar")))))
                loDocumento.FechaEntrega = Convert.ToDateTime(CDate(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_FechaEntrega"))) = True, Date.Now, loDr.GetValue(loDr.GetOrdinal("doc_FechaEntrega")))))
                loDocumento.FechaRegistro = Convert.ToDateTime(CDate(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_FechaRegistro"))) = True, Date.Now, loDr.GetValue(loDr.GetOrdinal("doc_FechaRegistro")))))
                loDocumento.FechaVenc = Convert.ToDateTime(CDate(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_FechaVenc"))) = True, Date.Now, loDr.GetValue(loDr.GetOrdinal("doc_FechaVenc")))))
                loDocumento.Total = Convert.ToDecimal(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_Total"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("doc_Total")))))
                loDocumento.Descuento = Convert.ToDecimal(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_Descuento"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("doc_Descuento")))))
                loDocumento.SubTotal = Convert.ToDecimal(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_SubTotal"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("doc_SubTotal")))))
                loDocumento.IGV = Convert.ToDecimal(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_Igv"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("doc_Igv")))))
                loDocumento.ImporteTotal = Convert.ToDecimal(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_Total"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("doc_Total")))))
                loDocumento.TotalAPagar = Convert.ToDecimal(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_TotalAPagar"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("doc_TotalAPagar")))))
                loDocumento.TotalLetras = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_TotalLetras"))) = True, "", loDr.GetValue(loDr.GetOrdinal("doc_TotalLetras")))))
                loDocumento.Utilidad = Convert.ToDecimal(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_Utilidad"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("doc_Utilidad")))))
                loDocumento.ValorReferencial = Convert.ToDecimal(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_ValorReferencial"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("doc_ValorReferencial")))))
                loDocumento.IdPersona = Convert.ToInt32(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdPersona"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("IdPersona")))))
                loDocumento.IdUsuario = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdUsuario"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdUsuario")))))
                loDocumento.IdTransportista = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdTransportista"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdTransportista")))))
                loDocumento.IdRemitente = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdRemitente"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdRemitente")))))
                loDocumento.IdDestinatario = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdDestinatario"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdDestinatario")))))
                loDocumento.IdEstadoDoc = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdEstadoDoc"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdEstadoDoc")))))
                loDocumento.IdCondicionPago = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdCondicionPago"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdCondicionPago")))))
                loDocumento.IdMoneda = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdMoneda"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdMoneda")))))
                loDocumento.LugarEntrega = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("LugarEntrega"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("LugarEntrega")))))
                loDocumento.IdTipoOperacion = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdTipoOperacion"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdTipoOperacion")))))
                loDocumento.IdTienda = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdTienda"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdTienda")))))
                loDocumento.IdSerie = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdSerie"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdSerie")))))
                loDocumento.ExportadoConta = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_ExportadoConta"))) = True, "", loDr.GetValue(loDr.GetOrdinal("doc_ExportadoConta")))))
                loDocumento.NroVoucherConta = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("doc_NroVoucherConta"))) = True, "", loDr.GetValue(loDr.GetOrdinal("doc_NroVoucherConta")))))
                loDocumento.IdEmpresa = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdEmpresa"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdEmpresa")))))
                loDocumento.IdChofer = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdChofer"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdChofer")))))
                loDocumento.IdMotivoT = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdMotivoT"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdMotivoT")))))
                loDocumento.IdTipoDocumento = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdTipoDocumento"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdTipoDocumento")))))
                loDocumento.IdVehiculo = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdVehiculo"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdVehiculo")))))
                loDocumento.IdEstadoCancelacion = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdEstadoCan"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdEstadoCan")))))
                loDocumento.IdEstadoEntrega = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdEstadoEnt"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdEstadoEnt")))))
                loDocumento.IdTipoPV = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdTipoPV"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdTipoPV")))))
                loDocumento.Id = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdDocumento"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdDocumento")))))
                loDocumento.IdArea = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdCaja"))) = True, 0, loDr.GetValue(loDr.GetOrdinal("IdCaja")))))
                loList.Add(loDocumento)
            End While
            loDr.Close()
        Catch e As Exception
            Throw e
        Finally            
        End Try
        Return loList
    End Function
    '********************************************************
    'M�todo Din, para obtener Documento por cualquier campo
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 3-Set-2009
    'Parametros : psWhere, psOrder
    'Retorna : Lista de tipo Documento
    '********************************************************
    Public Function fnInsTblDocumento(ByVal documento As Entidades.Documento) As Integer
        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        Dim ArrayParametros() As SqlParameter = New SqlParameter(38) {}
        ArrayParametros(0) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(documento.Codigo = Nothing, DBNull.Value, documento.Codigo)
        ArrayParametros(1) = New SqlParameter("@doc_Serie", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(documento.Serie = Nothing, DBNull.Value, documento.Serie)
        ArrayParametros(2) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(documento.FechaEmision = Nothing, DBNull.Value, documento.FechaEmision)
        ArrayParametros(3) = New SqlParameter("@doc_FechaIniTraslado", SqlDbType.DateTime)
        ArrayParametros(3).Value = IIf(documento.FechaIniTraslado = Nothing, DBNull.Value, documento.FechaIniTraslado)
        ArrayParametros(4) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        ArrayParametros(4).Value = IIf(documento.FechaAEntregar = Nothing, DBNull.Value, documento.FechaAEntregar)
        ArrayParametros(5) = New SqlParameter("@doc_FechaEntrega", SqlDbType.DateTime)
        ArrayParametros(5).Value = IIf(documento.FechaEntrega = Nothing, DBNull.Value, documento.FechaEntrega)
        ArrayParametros(6) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        ArrayParametros(6).Value = IIf(documento.FechaVenc = Nothing, DBNull.Value, documento.FechaVenc)
        ArrayParametros(7) = New SqlParameter("@doc_ImporteTotal", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(documento.ImporteTotal = Nothing, DBNull.Value, documento.ImporteTotal)
        ArrayParametros(8) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
        ArrayParametros(8).Value = IIf(documento.Descuento = Nothing, DBNull.Value, documento.Descuento)
        ArrayParametros(9) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
        ArrayParametros(9).Value = IIf(documento.SubTotal = Nothing, DBNull.Value, documento.SubTotal)
        ArrayParametros(10) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
        ArrayParametros(10).Value = IIf(documento.IGV = Nothing, DBNull.Value, documento.IGV)
        ArrayParametros(11) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        ArrayParametros(11).Value = IIf(documento.Total = Nothing, DBNull.Value, documento.Total)
        ArrayParametros(12) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        ArrayParametros(12).Value = IIf(documento.TotalLetras = Nothing, DBNull.Value, documento.TotalLetras)
        ArrayParametros(13) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
        ArrayParametros(13).Value = IIf(documento.TotalAPagar = Nothing, DBNull.Value, documento.TotalAPagar)
        ArrayParametros(14) = New SqlParameter("@doc_ValorReferencial", SqlDbType.Decimal)
        ArrayParametros(14).Value = IIf(documento.ValorReferencial = Nothing, DBNull.Value, documento.ValorReferencial)
        ArrayParametros(15) = New SqlParameter("@doc_Utilidad", SqlDbType.Decimal)
        ArrayParametros(15).Value = IIf(documento.Utilidad = Nothing, DBNull.Value, documento.Utilidad)
        ArrayParametros(16) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(16).Value = IIf(documento.IdPersona = Nothing, DBNull.Value, documento.IdPersona)
        ArrayParametros(17) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(17).Value = IIf(documento.IdUsuario = Nothing, DBNull.Value, documento.IdUsuario)
        ArrayParametros(18) = New SqlParameter("@IdTransportista", SqlDbType.Int)
        ArrayParametros(18).Value = IIf(documento.IdTransportista = Nothing, DBNull.Value, documento.IdTransportista)
        ArrayParametros(19) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        ArrayParametros(19).Value = IIf(documento.IdRemitente = Nothing, DBNull.Value, documento.IdRemitente)
        ArrayParametros(20) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        ArrayParametros(20).Value = IIf(documento.IdDestinatario = Nothing, DBNull.Value, documento.IdDestinatario)
        ArrayParametros(21) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        ArrayParametros(21).Value = IIf(documento.IdEstadoDoc = Nothing, DBNull.Value, documento.IdEstadoDoc)
        ArrayParametros(22) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        ArrayParametros(22).Value = IIf(documento.IdCondicionPago = Nothing, DBNull.Value, documento.IdCondicionPago)
        ArrayParametros(23) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(23).Value = IIf(documento.IdMoneda = Nothing, DBNull.Value, documento.IdMoneda)
        ArrayParametros(24) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
        ArrayParametros(24).Value = IIf(documento.LugarEntrega = Nothing, DBNull.Value, documento.LugarEntrega)
        ArrayParametros(25) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(25).Value = IIf(documento.IdTipoOperacion = Nothing, DBNull.Value, documento.IdTipoOperacion)
        ArrayParametros(26) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(26).Value = IIf(documento.IdTienda = Nothing, DBNull.Value, documento.IdTienda)
        ArrayParametros(27) = New SqlParameter("@IdSerie", SqlDbType.Int)
        ArrayParametros(27).Value = IIf(documento.IdSerie = Nothing, DBNull.Value, documento.IdSerie)
        ArrayParametros(28) = New SqlParameter("@doc_ExportadoConta", SqlDbType.Char)
        ArrayParametros(28).Value = IIf(documento.ExportadoConta = Nothing, DBNull.Value, documento.ExportadoConta)
        ArrayParametros(29) = New SqlParameter("@doc_NroVoucherConta", SqlDbType.VarChar)
        ArrayParametros(29).Value = IIf(documento.NroVoucherConta = Nothing, DBNull.Value, documento.NroVoucherConta)
        ArrayParametros(30) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(30).Value = IIf(documento.IdEmpresa = Nothing, DBNull.Value, documento.IdEmpresa)
        ArrayParametros(31) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(31).Value = IIf(documento.IdChofer = Nothing, DBNull.Value, documento.IdChofer)
        ArrayParametros(32) = New SqlParameter("@IdMotivoT", SqlDbType.Int)
        ArrayParametros(32).Value = IIf(documento.IdMotivoT = Nothing, DBNull.Value, documento.IdMotivoT)
        ArrayParametros(33) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(33).Value = IIf(documento.IdTipoDocumento = Nothing, DBNull.Value, documento.IdTipoDocumento)
        ArrayParametros(34) = New SqlParameter("@IdVehiculo", SqlDbType.Int)
        ArrayParametros(34).Value = IIf(documento.IdVehiculo = Nothing, DBNull.Value, documento.IdVehiculo)
        ArrayParametros(35) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(35).Direction = ParameterDirection.Output
        ArrayParametros(36) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
        ArrayParametros(36).Value = IIf(documento.IdEstadoCancelacion = Nothing, DBNull.Value, documento.IdEstadoCancelacion)
        ArrayParametros(37) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
        ArrayParametros(37).Value = IIf(documento.IdEstadoEntrega = Nothing, DBNull.Value, documento.IdEstadoEntrega)

        ArrayParametros(38) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(38).Value = IIf(documento.IdAlmacen = Nothing, DBNull.Value, documento.IdAlmacen)

        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteScalar(loTx, CommandType.StoredProcedure, "_DocumentoInsert", ArrayParametros))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function
    Public Sub MovAlmacen_ExtornoxIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_MovAlmacen_ExtornoxIdDocumento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cmd.ExecuteNonQuery()
        Catch ex As Exception
        Finally

        End Try
    End Sub
    Public Sub AnularGuiaRecepcionxIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_DocumentoAnularGuiaRecepcionxIdDocumento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
        Finally

        End Try
    End Sub
    Public Function GenerarDocTomaInventario(ByVal IdDocumento As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal FechaFin As Date, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim cmd As New SqlCommand("_DocumentoGenerarTomaInventario", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
            cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
            cmd.Parameters.AddWithValue("@FechaFin", FechaFin)

            If cmd.ExecuteNonQuery <= 0 Then
                Return False
            End If

            Return True

        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function FacturaSelectCab_Impresion(ByVal IdDocumento As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoFacturaSelectCab_Impresion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objDocumento As Entidades.Documento = Nothing
                If lector.Read Then
                    objDocumento = New Entidades.Documento
                    With objDocumento
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))

                        .DescripcionPersona = CStr(IIf(IsDBNull(lector("DescripcionPersona")) = True, "", lector("DescripcionPersona")))
                        .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")) = True, 0, lector("IdCondicionPago")))
                        .NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        .SubTotal = CDec(IIf(IsDBNull(lector("doc_SubTotal")) = True, 0, lector("doc_SubTotal")))
                        .IGV = CDec(IIf(IsDBNull(lector("doc_Igv")) = True, 0, lector("doc_Igv")))
                        .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))

                        .TotalLetras = CStr(IIf(IsDBNull(lector("LetrasTotalAPagar")) = True, "", lector("LetrasTotalAPagar")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")) = True, "", lector("edoc_Nombre")))
                        .PorcentIGV = CDec(IIf(IsDBNull(lector("PorcentIGV")) = True, 0, lector("PorcentIGV")))

                        .Percepcion = CDec(IIf(IsDBNull(lector("Percepcion")) = True, 0, lector("Percepcion")))

                        .PorcentPercepcion = CDec(IIf(IsDBNull(lector("PorcentPercepcion")) = True, 0, lector("PorcentPercepcion")))

                        .DocumentoI = CStr(IIf(IsDBNull(lector("DocumentoI_Persona")) = True, "", lector("DocumentoI_Persona")))

                        .Direccion = CStr(IIf(IsDBNull(lector("dir_Direccion")) = True, "", lector("dir_Direccion")))
                    End With

                End If
                lector.Close()
                Return objDocumento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function FacturaSelectDet_Impresion(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoFacturaSelectDet_Impresion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj

                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .NomProducto = CStr(IIf(IsDBNull(lector("prod_Nombre")) = True, "", lector("prod_Nombre")))
                        .Cantidad = CDec(IIf(IsDBNull(lector("dc_Cantidad")) = True, 0, lector("dc_Cantidad")))
                        .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")) = True, 0, lector("IdProducto")))
                        .UMedida = CStr(IIf(IsDBNull(lector("dc_UMedida")) = True, "", lector("dc_UMedida")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector("dc_PrecioCD")) = True, 0, lector("dc_PrecioCD")))
                        .Importe = CDec(IIf(IsDBNull(lector("dc_Importe")) = True, 0, lector("dc_Importe")))
                        .DetalleGlosa = CStr(IIf(IsDBNull(lector("dc_DetalleGlosa")) = True, "", lector("dc_DetalleGlosa")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdDocumentoProgramacionPago(ByVal IdDocumento As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdDocumentoProgramacionPago", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese c�digo")
                End If
                Dim Documento As New Entidades.Documento
                With Documento
                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, Nothing, lector.Item("doc_Total")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")) = True, 0, lector.Item("IdCondicionPago")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))

                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumentoReferenciaCheckes(ByVal IdDocumento As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoRef_checkes", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese c�digo")
                End If
                Dim Documento As New Entidades.Documento
                With Documento
                    .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("mcp_Fecha")) = True, Nothing, lector.Item("mcp_Fecha")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .Total = CDec(IIf(IsDBNull(lector.Item("mcp_Monto")) = True, Nothing, lector.Item("mcp_Monto")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, Nothing, lector.Item("doc_TotalAPagar")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdCuentaProv")) = True, 0, lector.Item("IdCuentaProv")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .FechaCancelacion = CDate(IIf(IsDBNull(lector.Item("mcp_FechaCanc")) = True, Nothing, lector.Item("mcp_FechaCanc")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                    .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                    .Saldo = CDec(IIf(IsDBNull(lector.Item("mcp_Saldo")) = True, Nothing, lector.Item("mcp_Saldo")))


                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumentoExternotwo(ByVal IdDocumento As String) As Entidades.Documento
        Dim lista As New List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdDocumentoExterno02", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()

                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese c�digo")
                End If

                Dim Documento As New Entidades.Documento
                With Documento

                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, Nothing, lector.Item("doc_Total")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, Nothing, lector.Item("doc_TotalAPagar")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")) = True, 0, lector.Item("IdCondicionPago")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    .LugarEntrega = CInt(IIf(IsDBNull(lector.Item("LugarEntrega")) = True, 0, lector.Item("LugarEntrega")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")) = True, 0, lector.Item("IdEstadoCan")))
                    .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                    .FechaCancelacion = CDate(IIf(IsDBNull(lector.Item("doc_FechaCancelacion")) = True, Nothing, lector.Item("doc_FechaCancelacion")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                    .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                    .NroDocumento = .Serie + " - " + .Codigo
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                    .IdUsuarioComision = CInt(IIf(IsDBNull(lector.Item("IdUsuarioComision")) = True, 0, lector.Item("IdUsuarioComision")))

                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumentoExternoOne(ByVal IdDocumento As String) As Entidades.Documento
        Dim lista As New List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdDocumentoExterno", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                Dim Documento As New Entidades.Documento
                If lector.HasRows = False Then
                    'Throw New Exception("No existe documento con Ese c�digo")
                Else

                    With Documento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                        .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                        .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, Nothing, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, Nothing, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdCondicionPago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")) = True, 0, lector.Item("IdCondicionPago")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .LugarEntrega = CInt(IIf(IsDBNull(lector.Item("LugarEntrega")) = True, 0, lector.Item("LugarEntrega")))
                        .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")) = True, 0, lector.Item("IdEstadoCan")))
                        .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                        .FechaCancelacion = CDate(IIf(IsDBNull(lector.Item("doc_FechaCancelacion")) = True, Nothing, lector.Item("doc_FechaCancelacion")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .IdUsuarioComision = CInt(IIf(IsDBNull(lector.Item("IdUsuarioComision")) = True, 0, lector.Item("IdUsuarioComision")))

                    End With

                End If
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdDocumentoExternoLista(ByVal IdDocumento As String) As List(Of Entidades.Documento)
        Dim lista As New List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdDocumentoExterno02", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    While (lector.Read)

                        Dim Documento As New Entidades.Documento
                        With Documento
                            .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                            .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                            .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                            .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                            .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                            .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                            .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, Nothing, lector.Item("doc_Total")))
                            .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, Nothing, lector.Item("doc_TotalAPagar")))
                            .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                            .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                            .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                            .IdCondicionPago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")) = True, 0, lector.Item("IdCondicionPago")))
                            .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                            .LugarEntrega = CInt(IIf(IsDBNull(lector.Item("LugarEntrega")) = True, 0, lector.Item("LugarEntrega")))
                            .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                            .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                            .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                            .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                            .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")) = True, 0, lector.Item("IdEstadoCan")))
                            .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                            .FechaCancelacion = CDate(IIf(IsDBNull(lector.Item("doc_FechaCancelacion")) = True, Nothing, lector.Item("doc_FechaCancelacion")))
                            .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                            .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                            .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                            .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                            .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                            .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                            .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                            .NroDocumento = .Serie + " - " + .Codigo
                            .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                            .IdUsuarioComision = CInt(IIf(IsDBNull(lector.Item("IdUsuarioComision")) = True, 0, lector.Item("IdUsuarioComision")))

                        End With
                        lista.Add(Documento)
                    End While
                Else
                    lista = Nothing
                End If
                Return lista
            End Using
            cn.Close()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumentoExterno(ByVal IdDocumento As String) As List(Of Entidades.Documento)
        Dim lista As New List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdDocumentoExterno", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese c�digo")
                End If

                While (lector.Read)

                    Dim Documento As New Entidades.Documento
                    With Documento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                        .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                        .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, Nothing, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, Nothing, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdCondicionPago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")) = True, 0, lector.Item("IdCondicionPago")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .LugarEntrega = CInt(IIf(IsDBNull(lector.Item("LugarEntrega")) = True, 0, lector.Item("LugarEntrega")))
                        .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")) = True, 0, lector.Item("IdEstadoCan")))
                        .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                        .FechaCancelacion = CDate(IIf(IsDBNull(lector.Item("doc_FechaCancelacion")) = True, Nothing, lector.Item("doc_FechaCancelacion")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .IdUsuarioComision = CInt(IIf(IsDBNull(lector.Item("IdUsuarioComision")) = True, 0, lector.Item("IdUsuarioComision")))

                    End With
                    lista.Add(Documento)
                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumentoDocReferencia(ByVal IdDocumento As Integer) As Entidades.Documento

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdDocumentoReferenciaOPVP", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese c�digo")
                End If
                Dim Documento As New Entidades.Documento
                With Documento
                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("NomAlmacen")) = True, "", lector.Item("NomAlmacen")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                    .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                    .NroDocumento = .Serie + " - " + .Codigo
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Documento

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese c�digo")
                End If
                Dim Documento As New Entidades.Documento
                With Documento
                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaIniTraslado = CDate(IIf(IsDBNull(lector.Item("doc_FechaIniTraslado")) = True, Nothing, lector.Item("doc_FechaIniTraslado")))
                    .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .Descuento = CDec(IIf(IsDBNull(lector.Item("doc_Descuento")) = True, Nothing, lector.Item("doc_Descuento")))
                    .SubTotal = CDec(IIf(IsDBNull(lector.Item("doc_SubTotal")) = True, Nothing, lector.Item("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector.Item("doc_IGV")) = True, Nothing, lector.Item("doc_IGV")))
                    .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, Nothing, lector.Item("doc_Total")))
                    '.TotalLetras = CStr(IIf(IsDBNull(lector.Item("doc_TotalLetras")) = True, Nothing, lector.Item("doc_TotalLetras")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, Nothing, lector.Item("doc_TotalAPagar")))
                    .ValorReferencial = CDec(IIf(IsDBNull(lector.Item("doc_ValorReferencial")) = True, Nothing, lector.Item("doc_ValorReferencial")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    .IdTransportista = CInt(IIf(IsDBNull(lector.Item("IdTransportista")) = True, 0, lector.Item("IdTransportista")))
                    .IdRemitente = CInt(IIf(IsDBNull(lector.Item("IdRemitente")) = True, 0, lector.Item("IdRemitente")))
                    .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")) = True, 0, lector.Item("IdCondicionPago")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    .LugarEntrega = CInt(IIf(IsDBNull(lector.Item("LugarEntrega")) = True, 0, lector.Item("LugarEntrega")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdChofer = CInt(IIf(IsDBNull(lector.Item("IdChofer")) = True, 0, lector.Item("IdChofer")))
                    .IdMotivoT = CInt(IIf(IsDBNull(lector.Item("IdMotivoT")) = True, 0, lector.Item("IdMotivoT")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .IdVehiculo = CInt(IIf(IsDBNull(lector.Item("IdVehiculo")) = True, 0, lector.Item("IdVehiculo")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")) = True, 0, lector.Item("IdEstadoCan")))
                    .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                    .FechaCancelacion = CDate(IIf(IsDBNull(lector.Item("doc_FechaCancelacion")) = True, Nothing, lector.Item("doc_FechaCancelacion")))

                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("NomAlmacen")) = True, "", lector.Item("NomAlmacen")))

                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))

                    .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                    .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                    .Retencion = CDec(IIf(IsDBNull(lector.Item("Retencion")) = True, 0, lector.Item("Retencion")))

                    .PoseeOrdenDespacho = CBool(IIf(IsDBNull(lector.Item("PoseeOrdenDespacho")) = True, 0, lector.Item("PoseeOrdenDespacho")))
                    .PoseeAmortizaciones = CBool(IIf(IsDBNull(lector.Item("PoseeAmortizaciones")) = True, 0, lector.Item("PoseeAmortizaciones")))
                    .PoseeGRecepcion = CBool(IIf(IsDBNull(lector.Item("PoseeGRecepcion")) = True, 0, lector.Item("PoseeGRecepcion")))

                    .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))

                    .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                    .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                    .NroDocumento = .Serie + " - " + .Codigo
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                    .IdUsuarioComision = CInt(IIf(IsDBNull(lector.Item("IdUsuarioComision")) = True, 0, lector.Item("IdUsuarioComision")))


                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DocumentoSelectDocxDespachar_V2(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdPersona As Integer, _
                                                    ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdTipoDocumentoRef As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectDocxDespachar_V2", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdTipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)



        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                        .NomEstadoEntrega = CStr(IIf(IsDBNull(lector.Item("eent_Nombre")) = True, "", lector.Item("eent_Nombre")))

                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))

                        .NroDocumento = .Serie + " - " + .Codigo
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                        .MotivoTraslado = CStr(IIf(IsDBNull(lector.Item("MotivoRef")) = True, "", lector.Item("MotivoRef")))
                        .TipoOperacion = CStr(IIf(IsDBNull(lector.Item("TipoOperacion")) = True, "", lector.Item("TipoOperacion")))

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DocumentoSelectDocxDespachar_V2_Detalle(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdPersona As Integer, _
                                                    ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdTipoDocumentoRef As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectDocxDespachar_V2_Detalle_porborrar", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdTipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)



        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                        .NomEstadoEntrega = CStr(IIf(IsDBNull(lector.Item("eent_Nombre")) = True, "", lector.Item("eent_Nombre")))

                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))

                        .NroDocumento = .Serie + " - " + .Codigo
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                        .MotivoTraslado = CStr(IIf(IsDBNull(lector.Item("MotivoRef")) = True, "", lector.Item("MotivoRef")))
                        .TipoOperacion = CStr(IIf(IsDBNull(lector.Item("TipoOperacion")) = True, "", lector.Item("TipoOperacion")))
                        .FechaRegistro = CStr(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DocumentoSelectDocxDespachar_picking(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdPersona As Integer, _
                                                    ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdTipoDocumentoRef As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_DocumentoSelectDocxDespachar_picking]", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdTipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)



        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                        .NomEstadoEntrega = CStr(IIf(IsDBNull(lector.Item("eent_Nombre")) = True, "", lector.Item("eent_Nombre")))

                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))

                        .NroDocumento = .Serie + " - " + .Codigo
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                        .MotivoTraslado = CStr(IIf(IsDBNull(lector.Item("MotivoRef")) = True, "", lector.Item("MotivoRef")))
                        .TipoOperacion = CStr(IIf(IsDBNull(lector.Item("TipoOperacion")) = True, "", lector.Item("TipoOperacion")))

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub InsertObservacion(ByVal obj As Entidades.Observacion, ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            If (obj IsNot Nothing) Then
                Dim objDAO As New DAO.DAOObservacion
                obj.IdDocumento = IdDocumento
                objDAO.InsertaObservacionT(cnx, trx, obj)
            End If
        Catch ex As Exception
            Throw ex
        Finally            
        End Try
    End Sub

    Public Sub InsertListCancelacion_PagoCaja(ByVal list As List(Of Entidades.PagoCaja), ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            Dim objDAO As New DAO.DAOPagoCaja
            For i As Integer = 0 To list.Count - 1
                list(i).IdDocumento = IdDocumento
                objDAO.InsertaPagoCaja(cnx, list(i), trx)
            Next
        Catch ex As Exception
            Throw ex
        Finally            
        End Try
    End Sub
    Public Sub InsertListCancelacion_DatosCancelacion(ByVal list As List(Of Entidades.DatosCancelacion), ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            Dim objDAO As New DAO.DAODatosCancelacion
            For i As Integer = 0 To list.Count - 1
                list(i).IdDocumento = IdDocumento
                objDAO.InsertT(list(i), cnx, trx)
            Next
        Catch ex As Exception
            Throw ex
        Finally            
        End Try
    End Sub

    Public Sub InsertMovCajaxIdDocumento(ByVal obj As Entidades.MovCaja, ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            Dim objDAO As New DAO.DAOMovCaja
            obj.IdDocumento = IdDocumento
            objDAO.InsertaMovCaja(cnx, obj, trx)

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub
    ''SP PARA QUITAR CHECK AL DOCUMENTO
    Public Function QuitarCheckDocumentoxIdDocumento(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False
        Dim resultado As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_QuitarCheckDocumentoxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@resultado", resultado)
        cn.Open()
        Try
            If cmd.ExecuteNonQuery() <= 0 Then
                Throw New Exception
            End If
            hecho = True
        Catch ex As Exception
            hecho = False
        Finally

        End Try
        Return hecho
    End Function
    ''SP PARA AGREGAR CHECK AL DOCUMENTO
    Public Function AgregarCheckDocumentoxIdDocumento(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False
        Dim resultado As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AgregarCheckDocumentoxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cn.Open()
        Try
            If cmd.ExecuteNonQuery() <= 0 Then
                Throw New Exception
            End If
            hecho = True
        Catch ex As Exception
            hecho = False
        Finally

        End Try
        Return hecho
    End Function

    Public Function selectxidval_req_aprob(ByVal obj As Entidades.Anexo_Documento) As Entidades.Anexo_Documento

        Dim cn As SqlConnection = objConexion.ConexionSIGE


        Dim cmd As New SqlCommand("selectxidval_req_aprob", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", obj.IdDocumento)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese c�digo")
                End If
                Dim Documento As New Entidades.Anexo_Documento
                With Documento
                    .EstadoRequerimiento = CInt(IIf(IsDBNull(lector.Item("est_reque")) = True, 0, lector.Item("est_reque")))
                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Sub crearFacturaElectronica(ByVal idDocumento As Integer, ByVal cn As SqlConnection)
        Using cmd As New SqlCommand("SP_FACTURACION_V2", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
            End With
            cmd.ExecuteNonQuery()
        End Using

        Using cmd As New SqlCommand("SP_FACTURACION_V3", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
            End With
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Public Sub validarNotaCredito(ByVal idDocumento As Integer, ByVal idDocumentoRecep As Integer, ByVal idTipoOperacion As Integer, ByVal idMotivo As Integer, _
                                  ByVal paramNumerico As Decimal, ByVal paramString As String, ByVal cn As SqlConnection)

        Using cmd As New SqlCommand("SP_VALIDAR_NC", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idDocumentoRef", SqlDbType.Int)).Value = idDocumento
                .Parameters.Add(New SqlParameter("@idDocumentoRecep", SqlDbType.Int)).Value = idDocumentoRecep
                .Parameters.Add(New SqlParameter("@IdTipoOperacion", SqlDbType.Int)).Value = idTipoOperacion
                .Parameters.Add(New SqlParameter("@IdMotivo", SqlDbType.Int)).Value = idMotivo
                .Parameters.Add(New SqlParameter("@paramNumerico", SqlDbType.Int)).Value = paramNumerico
                .Parameters.Add(New SqlParameter("@paramString", SqlDbType.NVarChar)).Value = paramString
            End With
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Function activarCamposNC(ByVal idTipoOperacion As Integer, ByVal idMotivo As Integer, ByVal cn As SqlConnection) As DataTable
        Dim dt As New DataTable
        Using cmd As New SqlCommand("select * from dbo.fn_validaNC(@idTipoOperacion,@idMotivo)", cn)
            With cmd
                .Parameters.Add(New SqlParameter("@idTipoOperacion", SqlDbType.Int)).Value = idTipoOperacion
                .Parameters.Add(New SqlParameter("@idMotivo", SqlDbType.Int)).Value = idMotivo
            End With
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
        End Using
        Return dt
    End Function

    Public Function crearArchivoFacContingencia(ByVal idMotivo As Integer, ByVal fecha As Date, ByVal cn As SqlConnection, ByVal direccion As String)
        Dim ruta As String = ""
        Using cmd As New SqlCommand("SP_RESUMEN_CONTINGENCIA", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@fechaEmision", SqlDbType.Date)).Value = fecha
                .Parameters.Add(New SqlParameter("@idMotivo", SqlDbType.Int)).Value = idMotivo
                .Parameters.Add(New SqlParameter("@direccion", SqlDbType.NVarChar)).Value = direccion
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader()
            If rdr IsNot Nothing Then
                rdr.NextResult()
                While rdr.Read()
                    ruta = rdr.GetString(0)
                End While
                rdr.Close()
            End If
        End Using
        Return ruta
    End Function


    Public Sub CambiarEstadoAnticipo(ByVal idDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Using cmd As New SqlCommand("USP_ANTICIPO_ESTADO", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
            End With
            cmd.ExecuteNonQuery()
        End Using
    End Sub
End Class


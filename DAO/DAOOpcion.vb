'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Public Class DAOOpcion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Function SelectOpcionxDocumentoRef(ByVal IdDocumento As Integer) As List(Of Entidades.Opcion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DOCUMENTO_REFERENCIA", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Opcion)
                Do While lector.Read
                    Dim obj As New Entidades.Opcion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, "0", lector.Item("IdDocumento")))
                    obj.IdMenu = CInt(IIf(IsDBNull(lector.Item("IDREF")) = True, "0", lector.Item("IDREF")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    obj.Formulario = CStr(IIf(IsDBNull(lector.Item("IdDocumento")) = True, "", lector.Item("IdDocumento")))
                    ' obj.IsPermitido = CStr(IIf(IsDBNull(lector.Item("op_Permitido")) = True, "", lector.Item("op_Permitido")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectxIdOpcion(ByVal IdOpcion As Integer) As Entidades.Opcion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim obj As New Entidades.Opcion
        Dim cmd As New SqlCommand("_OpcionSelectxIdOpcion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdOpcion", IdOpcion)
        Dim lector As SqlDataReader
        Try

            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            If lector.Read Then
                obj.Id = CInt(IIf(IsDBNull(lector.Item("IdOpcion")) = True, "0", lector.Item("IdOpcion")))
                obj.IdMenu = CInt(IIf(IsDBNull(lector.Item("IdMenu")) = True, "0", lector.Item("IdMenu")))
                obj.Nombre = CStr(IIf(IsDBNull(lector.Item("op_Nombre")) = True, "", lector.Item("op_Nombre")))
                obj.Formulario = CStr(IIf(IsDBNull(lector.Item("op_Formulario")) = True, "", lector.Item("op_Formulario")))
                obj.Estado = CStr(IIf(IsDBNull(lector.Item("op_Estado")) = True, "", lector.Item("op_Estado")))
                obj.Orden = CDec(IIf(IsDBNull(lector.Item("op_Orden")) = True, 0, lector.Item("op_Orden")))
                obj.ParentName = CStr(IIf(IsDBNull(lector.Item("ParentName")) = True, "", lector.Item("ParentName")))
            Else
                Throw New Exception("No se encontr� la Opci�n.")
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return obj
    End Function








    Public Sub AplicarCambiosAllUsers(ByVal cn As SqlConnection, ByVal t As SqlTransaction, ByVal idperfil As Integer)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
        ArrayParametros(0) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        ArrayParametros(0).Value = idperfil
        HDAO.InsertaT(cn, "_OpcionPerfilAplicarCambiosAllUsers", ArrayParametros, t)

    End Sub






    Public Function InsertaOpcion(ByVal opcion As Entidades.Opcion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdMenu", SqlDbType.Int)
        ArrayParametros(0).Value = opcion.IdMenu
        ArrayParametros(1) = New SqlParameter("@op_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = opcion.Nombre
        ArrayParametros(2) = New SqlParameter("@op_Formulario", SqlDbType.VarChar)
        ArrayParametros(2).Value = opcion.Formulario
        ArrayParametros(3) = New SqlParameter("@op_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = opcion.Estado
        ArrayParametros(4) = New SqlParameter("@op_Orden", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(opcion.Orden = Nothing, DBNull.Value, opcion.Orden)

        Return HDAO.Insert(cn, "_OpcionInsert", ArrayParametros)
    End Function
    Public Function ActualizaOpcion(ByVal opcion As Entidades.Opcion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdMenu", SqlDbType.Int)
        ArrayParametros(0).Value = opcion.IdMenu
        ArrayParametros(1) = New SqlParameter("@op_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = opcion.Nombre
        ArrayParametros(2) = New SqlParameter("@op_Formulario", SqlDbType.VarChar)
        ArrayParametros(2).Value = opcion.Formulario
        ArrayParametros(3) = New SqlParameter("@op_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = opcion.Estado
        ArrayParametros(4) = New SqlParameter("@IdOpcion", SqlDbType.Int)
        ArrayParametros(4).Value = opcion.Id
        ArrayParametros(5) = New SqlParameter("@op_Orden", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(opcion.Orden = Nothing, DBNull.Value, opcion.Orden)
        Return HDAO.Update(cn, "_OpcionUpdate", ArrayParametros)

    End Function
    Public Function Elimina(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idopcion As Integer, ByVal deleteAll As Boolean) As Boolean
        Dim cmd As New SqlCommand("_OpcionDelete", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idOpcion", idopcion)
        cmd.Parameters.AddWithValue("@deleteAll", deleteAll)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectAllOpcion() As List(Of Entidades.Opcion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("selectAllOpcion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Opcion)
                Do While lector.Read
                    Dim obj As New Entidades.Opcion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdOpcion")) = True, "0", lector.Item("IdOpcion")))
                    obj.IdMenu = CInt(IIf(IsDBNull(lector.Item("IdMenu")) = True, "0", lector.Item("IdMenu")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("op_Nombre")) = True, "", lector.Item("op_Nombre")))
                    obj.Formulario = CStr(IIf(IsDBNull(lector.Item("op_Formulario")) = True, "", lector.Item("op_Formulario")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("op_Estado")) = True, "", lector.Item("op_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CargaNodoTreeView() As DataTable
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            Using cn
                cn.Open()
                Dim dtTreeNodo As New DataTable
                Dim daMenu As New SqlDataAdapter("selectAllOpcion", cn)
                daMenu.SelectCommand.CommandType = CommandType.StoredProcedure
                daMenu.Fill(dtTreeNodo)
                Return dtTreeNodo
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'obtiene las opciones x usuario y para uno de sus perfiles
    Public Function SelectOpcionxUsuarioxPerfil(ByVal idperfil As Integer, ByVal idusuario As Integer) As List(Of Entidades.Opcion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("proc_getOpcionesxUsuarioxPerfil", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idusuario", idusuario)
        cmd.Parameters.AddWithValue("@idperfil", idperfil)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Opcion)
                Do While lector.Read
                    Dim obj As New Entidades.Opcion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdOpcion")) = True, "0", lector.Item("IdOpcion")))
                    obj.IdMenu = CInt(IIf(IsDBNull(lector.Item("IdMenu")) = True, "0", lector.Item("IdMenu")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("op_Nombre")) = True, "", lector.Item("op_Nombre")))
                    obj.Formulario = CStr(IIf(IsDBNull(lector.Item("op_Formulario")) = True, "", lector.Item("op_Formulario")))
                    obj.IsPermitido = CStr(IIf(IsDBNull(lector.Item("op_Permitido")) = True, "", lector.Item("op_Permitido")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''obtiene las opcione para cada uno de los perfiles
    Public Function SelectOpcionxPerfil(ByVal idperfil As Integer) As List(Of Entidades.Opcion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("proc_getOpcionesxPerfil", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idperfil", idperfil)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Opcion)
                Do While lector.Read
                    Dim obj As New Entidades.Opcion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdOpcion")) = True, "0", lector.Item("IdOpcion")))
                    obj.IdMenu = CInt(IIf(IsDBNull(lector.Item("IdMenu")) = True, "0", lector.Item("IdMenu")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("op_Nombre")) = True, "", lector.Item("op_Nombre")))
                    obj.Formulario = CStr(IIf(IsDBNull(lector.Item("op_Formulario")) = True, "", lector.Item("op_Formulario")))
                    obj.IsPermitido = CStr(IIf(IsDBNull(lector.Item("op_Permitido")) = True, "", lector.Item("op_Permitido")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'llena el combo, con los perfiles de los usuarios
    Public Function SelectPerfilxUsuario(ByVal idusuario As Integer) As List(Of Entidades.Perfil)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("proc_PerfilxUsuario", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idusuario", idusuario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Perfil)
                Do While lector.Read
                    Dim obj As New Entidades.Perfil
                    obj.IdPerfil = CInt(IIf(IsDBNull(lector.Item("IdPerfil")) = True, "0", lector.Item("IdPerfil")))
                    obj.NomPerfil = CStr(IIf(IsDBNull(lector.Item("perf_Nombre")) = True, "", lector.Item("perf_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'obtiene las opciones para cada usuario,obs debe obtnener ,las opciones 
    'q le corresponde por perfil mas las opciones q le corresxpsonde ,por usuario.
    Public Function SelectOpcionxUsuario(ByVal idusuario As Integer) As List(Of Entidades.Opcion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("OpcionesxUsuarioxperfil", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idusuario", idusuario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Opcion)
                Do While lector.Read
                    Dim obj As New Entidades.Opcion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdOpcion")) = True, "0", lector.Item("IdOpcion")))
                    obj.IdMenu = CInt(IIf(IsDBNull(lector.Item("IdMenu")) = True, "0", lector.Item("IdMenu")))
                    obj.IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, "0", lector.Item("IdUsuario")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("op_Nombre")) = True, "", lector.Item("op_Nombre")))
                    obj.Formulario = CStr(IIf(IsDBNull(lector.Item("op_Formulario")) = True, "", lector.Item("op_Formulario")))
                    obj.dir_imagen = CStr(IIf(IsDBNull(lector.Item("dir_img_menu")) = True, "", lector.Item("dir_img_menu")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectOpcionMantenimiento() As List(Of Entidades.Opcion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("Proc_MantMenuDinamico", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Opcion)
                Do While lector.Read
                    Dim obj As New Entidades.Opcion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdOpcion")) = True, "0", lector.Item("IdOpcion")))
                    obj.IdMenu = CInt(IIf(IsDBNull(lector.Item("IdMenu")) = True, "0", lector.Item("IdMenu")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("op_Estado")) = True, "0", lector.Item("op_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("op_Nombre")) = True, "", lector.Item("op_Nombre")))
                    obj.Formulario = CStr(IIf(IsDBNull(lector.Item("op_Formulario")) = True, "", lector.Item("op_Formulario")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertaPerfilOpcion(ByVal listaOpcion As List(Of Entidades.Opcion), ByVal cn As SqlConnection, ByVal t As SqlTransaction, ByVal idperfil As Integer) As Boolean
        Dim perfilopcion As Entidades.Opcion
        Try
            For Each perfilopcion In listaOpcion
                If perfilopcion.IsPermitido = "1" Then
                    Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
                    ArrayParametros(0) = New SqlParameter("@IdPerfil", SqlDbType.Int)
                    ArrayParametros(0).Value = idperfil
                    ArrayParametros(1) = New SqlParameter("@Idopcion", SqlDbType.Int)
                    ArrayParametros(1).Value = perfilopcion.Id
                    HDAO.InsertaT(cn, "proc_insertaPerfilOpcion", ArrayParametros, t)
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Sub DeletePerfilOpcion(ByVal Idperfil As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("proc_deletePerfilOpcion", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idperfil", Idperfil)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function InsertaPerfilOpcionUsuario(ByVal listaOpcion As List(Of Entidades.Opcion), ByVal cn As SqlConnection, ByVal t As SqlTransaction, ByVal idperfil As Integer, ByVal idUsuario As Integer) As Boolean
        Dim perfilopcion As Entidades.Opcion
        Try
            For Each perfilopcion In listaOpcion
                If perfilopcion.IsPermitido = "1" Then
                    Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
                    ArrayParametros(0) = New SqlParameter("@IdPerfil", SqlDbType.Int)
                    ArrayParametros(0).Value = idperfil
                    ArrayParametros(1) = New SqlParameter("@Idopcion", SqlDbType.Int)
                    ArrayParametros(1).Value = perfilopcion.Id
                    ArrayParametros(2) = New SqlParameter("@idUsuario", SqlDbType.Int)
                    ArrayParametros(2).Value = idUsuario
                    HDAO.InsertaT(cn, "proc_InsertPerfilUsuarioOpcion", ArrayParametros, t)
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Sub DeletePerfilOpcionUsuario(ByVal Idperfil As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idUsuario As Integer)
        Try
            Dim cmd As New SqlCommand("proc_DeletePerfilUsuarioOpcion", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idperfil", Idperfil)
            cmd.Parameters.AddWithValue("@idUsuario", idUsuario)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub UpdateInsertaOpcion(ByVal Listaopcion As List(Of Entidades.Opcion), ByVal cn As SqlConnection, ByVal t As SqlTransaction)
        For Each opcion As Entidades.Opcion In Listaopcion
            Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
            ArrayParametros(0) = New SqlParameter("@IdOpcion", SqlDbType.Int)
            ArrayParametros(0).Value = opcion.Id
            ArrayParametros(1) = New SqlParameter("@IdMenu", SqlDbType.Int)
            ArrayParametros(1).Value = opcion.IdMenu
            ArrayParametros(2) = New SqlParameter("@op_Nombre", SqlDbType.VarChar)
            ArrayParametros(2).Value = opcion.Nombre
            ArrayParametros(3) = New SqlParameter("@op_Formulario", SqlDbType.VarChar)
            ArrayParametros(3).Value = opcion.Formulario
            ArrayParametros(4) = New SqlParameter("@op_Estado", SqlDbType.Char)
            ArrayParametros(4).Value = opcion.Estado
            HDAO.InsertaT(cn, "proc_UpdateInsertOpcion", ArrayParametros, t)
        Next
    End Sub

End Class

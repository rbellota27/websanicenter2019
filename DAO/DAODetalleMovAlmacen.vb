'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAODetalleMovAlmacen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaDetalleMovAlmacen(ByVal cn As SqlConnection, ByVal detallemovalmacen As Entidades.DetalleMovAlmacen, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}
        ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(detallemovalmacen.IdTienda = Nothing, DBNull.Value, detallemovalmacen.IdTienda)
        ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(detallemovalmacen.IdAlmacen = Nothing, DBNull.Value, detallemovalmacen.IdAlmacen)
        ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(detallemovalmacen.IdEmpresa = Nothing, DBNull.Value, detallemovalmacen.IdEmpresa)
        ArrayParametros(3) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(detallemovalmacen.IdMovAlmacen = Nothing, DBNull.Value, detallemovalmacen.IdMovAlmacen)
        ArrayParametros(4) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(detallemovalmacen.IdProducto = Nothing, DBNull.Value, detallemovalmacen.IdProducto)
        ArrayParametros(5) = New SqlParameter("@dma_Factor", SqlDbType.Int)
        ArrayParametros(5).Value = IIf(detallemovalmacen.Factor = Nothing, DBNull.Value, detallemovalmacen.Factor)
        ArrayParametros(6) = New SqlParameter("@dma_Cantidad", SqlDbType.Decimal)
        ArrayParametros(6).Value = IIf(detallemovalmacen.Cantidad = Nothing, DBNull.Value, detallemovalmacen.Cantidad)
        ArrayParametros(7) = New SqlParameter("@dma_Costo", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(detallemovalmacen.Costo = Nothing, DBNull.Value, detallemovalmacen.Costo)
        ArrayParametros(8) = New SqlParameter("@dma_Total", SqlDbType.Decimal)
        ArrayParametros(8).Value = IIf(detallemovalmacen.Total = Nothing, DBNull.Value, detallemovalmacen.Total)
        ArrayParametros(9) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(9).Value = detallemovalmacen.IdDocumento
        HDAO.InsertaT(cn, "_DetalleMovAlmacenInsert", ArrayParametros, T)
    End Sub
    'Public Sub InsertaDetalleMovAlmacenT(ByVal cn As SqlConnection, ByVal detallemovalmacen As Entidades.DetalleMovAlmacen, ByVal T As SqlTransaction)
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
    '    ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
    '    ArrayParametros(0).Value = detallemovalmacen.IdTienda
    '    ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
    '    ArrayParametros(1).Value = detallemovalmacen.IdAlmacen
    '    ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
    '    ArrayParametros(2).Value = detallemovalmacen.IdEmpresa
    '    ArrayParametros(3) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
    '    ArrayParametros(3).Value = detallemovalmacen.IdMovAlmacen
    '    ArrayParametros(4) = New SqlParameter("@IdProducto", SqlDbType.Int)
    '    ArrayParametros(4).Value = detallemovalmacen.IdProducto
    '    ArrayParametros(5) = New SqlParameter("@dma_Factor", SqlDbType.Int)
    '    ArrayParametros(5).Value = detallemovalmacen.Factor
    '    ArrayParametros(6) = New SqlParameter("@dma_Cantidad", SqlDbType.Decimal)
    '    ArrayParametros(6).Value = detallemovalmacen.Cantidad
    '    ArrayParametros(7) = New SqlParameter("@dma_Costo", SqlDbType.Decimal)
    '    ArrayParametros(7).Value = detallemovalmacen.Costo
    '    ArrayParametros(8) = New SqlParameter("@dma_Total", SqlDbType.Decimal)
    '    ArrayParametros(8).Value = detallemovalmacen.Total
    '    HDAO.InsertaT(cn, "_DetalleMovAlmacenInsert", ArrayParametros, T)
    'End Sub
    Public Function ActualizaDetalleMovAlmacen(ByVal detallemovalmacen As Entidades.DetalleMovAlmacen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}
        ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(0).Value = detallemovalmacen.IdTienda
        ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(1).Value = detallemovalmacen.IdAlmacen
        ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(2).Value = detallemovalmacen.IdEmpresa
        ArrayParametros(3) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
        ArrayParametros(3).Value = detallemovalmacen.IdMovAlmacen
        ArrayParametros(4) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(4).Value = detallemovalmacen.IdProducto
        ArrayParametros(5) = New SqlParameter("@dma_Factor", SqlDbType.Int)
        ArrayParametros(5).Value = detallemovalmacen.Factor
        ArrayParametros(6) = New SqlParameter("@dma_Cantidad", SqlDbType.Decimal)
        ArrayParametros(6).Value = detallemovalmacen.Cantidad
        ArrayParametros(7) = New SqlParameter("@dma_Costo", SqlDbType.Decimal)
        ArrayParametros(7).Value = detallemovalmacen.Costo
        ArrayParametros(8) = New SqlParameter("@dma_Total", SqlDbType.Decimal)
        ArrayParametros(8).Value = detallemovalmacen.Total
        ArrayParametros(9) = New SqlParameter("@IdDetalleMovAlmacen", SqlDbType.Int)
        ArrayParametros(9).Value = detallemovalmacen.IdDetalleMovAlmacen
        Return HDAO.Update(cn, "_DetalleMovAlmacenUpdate", ArrayParametros)
    End Function
    Public Function DeletexIdDocumento(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal iddocumento As Integer) As Boolean
        Dim cmd As New SqlCommand("_DetalleMovAlmacenDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function UpdateCostoxIdMovAlmacen(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idmovalmacen As Integer, ByVal costo As Decimal) As Boolean
        Dim cmd As New SqlCommand("_DetalleMovAlmacenUpdateCostoxIdMovAlmacen", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMovAlmacen", idmovalmacen)
        cmd.Parameters.AddWithValue("@Costo", costo)
        Try
            If cmd.ExecuteNonQuery() = 0 Then
                Throw New Exception
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

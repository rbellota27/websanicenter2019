'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOOcupacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaOcupacion(ByVal ocupacion As Entidades.Ocupacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ocu_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = ocupacion.Nombre
        ArrayParametros(1) = New SqlParameter("@ocu_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = ocupacion.Estado
        ArrayParametros(2) = New SqlParameter("@IdGiro", SqlDbType.Int)
        ArrayParametros(2).Value = ocupacion.IdGiro
        Return HDAO.Insert(cn, "_OcupacionInsert", ArrayParametros)
    End Function
    Public Function ActualizaOcupacion(ByVal ocupacion As Entidades.Ocupacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@ocu_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = ocupacion.Nombre
        ArrayParametros(1) = New SqlParameter("@ocu_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = ocupacion.Estado
        ArrayParametros(2) = New SqlParameter("@IdGiro", SqlDbType.Int)
        ArrayParametros(2).Value = ocupacion.IdGiro
        ArrayParametros(3) = New SqlParameter("@IdOcupacion", SqlDbType.Int)
        ArrayParametros(3).Value = ocupacion.Id
        Return HDAO.Update(cn, "_OcupacionUpdate", ArrayParametros)
    End Function
    Public Function SelectCbo() As List(Of Entidades.Ocupacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OcupacionSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Ocupacion)
                Do While lector.Read
                    Dim obj As New Entidades.Ocupacion
                    obj.Id = CInt(lector.Item("IdOcupacion"))
                    obj.Nombre = CStr(lector.Item("ocu_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class

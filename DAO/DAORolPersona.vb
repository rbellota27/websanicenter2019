'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAORolPersona
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Sub InsertaRolPersona(ByVal cn As SqlConnection, ByVal rolpersona As Entidades.RolPersona, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(rolpersona.IdPersona = Nothing, DBNull.Value, rolpersona.IdPersona)
        ArrayParametros(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(rolpersona.IdUsuario = Nothing, DBNull.Value, rolpersona.IdUsuario)
        ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(rolpersona.IdEmpresa = Nothing, DBNull.Value, rolpersona.IdEmpresa)
        ArrayParametros(3) = New SqlParameter("@IdRol", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(rolpersona.IdRol = Nothing, DBNull.Value, rolpersona.IdRol)
        ArrayParametros(4) = New SqlParameter("@rp_FechaBaja", SqlDbType.DateTime)
        ArrayParametros(4).Value = IIf(rolpersona.FechaBaja = Nothing, DBNull.Value, rolpersona.FechaBaja)
        ArrayParametros(5) = New SqlParameter("@rp_Estado", SqlDbType.Char)
        ArrayParametros(5).Value = IIf(rolpersona.Estado = Nothing, DBNull.Value, rolpersona.Estado)
        ArrayParametros(6) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(rolpersona.IdMotivoBaja = Nothing, DBNull.Value, rolpersona.IdMotivoBaja)

        HDAO.InsertaT(cn, "_RolPersonaInsert", ArrayParametros, T)

    End Sub

    Public Sub InsertaListRolPersona(ByVal cn As SqlConnection, ByVal rolpersona As List(Of Entidades.RolPersona), ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        Dim objrolpersona As Entidades.RolPersona
        For Each objrolpersona In rolpersona
            ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(objrolpersona.IdPersona = Nothing, DBNull.Value, objrolpersona.IdPersona)
            ArrayParametros(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(objrolpersona.IdUsuario = Nothing, DBNull.Value, objrolpersona.IdUsuario)
            ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(objrolpersona.IdEmpresa = Nothing, DBNull.Value, objrolpersona.IdEmpresa)
            ArrayParametros(3) = New SqlParameter("@IdRol", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(objrolpersona.IdRol = Nothing, DBNull.Value, objrolpersona.IdRol)
            ArrayParametros(4) = New SqlParameter("@rp_FechaBaja", SqlDbType.DateTime)
            ArrayParametros(4).Value = IIf(objrolpersona.FechaBaja = Nothing, DBNull.Value, objrolpersona.FechaBaja)
            ArrayParametros(5) = New SqlParameter("@rp_Estado", SqlDbType.Char)
            ArrayParametros(5).Value = IIf(objrolpersona.Estado = Nothing, DBNull.Value, objrolpersona.Estado)
            ArrayParametros(6) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
            ArrayParametros(6).Value = IIf(objrolpersona.IdMotivoBaja = Nothing, DBNull.Value, objrolpersona.IdMotivoBaja)

            HDAO.InsertaT(cn, "_RolPersonaInsert", ArrayParametros, T)
        Next
    End Sub


    Public Function ActualizaRolPersona(ByVal rolpersona As Entidades.RolPersona) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = rolpersona.IdPersona
        ArrayParametros(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(1).Value = rolpersona.IdUsuario
        ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(2).Value = rolpersona.IdEmpresa
        ArrayParametros(3) = New SqlParameter("@IdRol", SqlDbType.Int)
        ArrayParametros(3).Value = rolpersona.IdRol
        ArrayParametros(4) = New SqlParameter("@rp_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(4).Value = rolpersona.FechaAlta
        ArrayParametros(5) = New SqlParameter("@rp_FechaBaja", SqlDbType.DateTime)
        ArrayParametros(5).Value = rolpersona.FechaBaja
        ArrayParametros(6) = New SqlParameter("@rp_Estado", SqlDbType.Char)
        ArrayParametros(6).Value = rolpersona.Estado
        ArrayParametros(7) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        ArrayParametros(7).Value = rolpersona.IdMotivoBaja
        Return HDAO.Update(cn, "_RolPersonaUpdate", ArrayParametros)
    End Function
    Public Sub ActualizaRolPersonaT(ByVal cn As SqlConnection, ByVal rolpersona As Entidades.RolPersona, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(rolpersona.IdPersona = Nothing, DBNull.Value, rolpersona.IdPersona)
        ArrayParametros(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(rolpersona.IdUsuario = Nothing, DBNull.Value, rolpersona.IdUsuario)
        ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(rolpersona.IdEmpresa = Nothing, DBNull.Value, rolpersona.IdEmpresa)
        ArrayParametros(3) = New SqlParameter("@IdRol", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(rolpersona.IdRol = Nothing, DBNull.Value, rolpersona.IdRol)
        ArrayParametros(4) = New SqlParameter("@rp_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(4).Value = IIf(rolpersona.FechaAlta = Nothing, DBNull.Value, rolpersona.FechaAlta)
        ArrayParametros(5) = New SqlParameter("@rp_FechaBaja", SqlDbType.DateTime)
        ArrayParametros(5).Value = IIf(rolpersona.FechaBaja = Nothing, DBNull.Value, rolpersona.FechaBaja)
        ArrayParametros(6) = New SqlParameter("@rp_Estado", SqlDbType.Char)
        ArrayParametros(6).Value = IIf(rolpersona.Estado = Nothing, DBNull.Value, rolpersona.Estado)
        ArrayParametros(7) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(rolpersona.IdMotivoBaja = Nothing, DBNull.Value, rolpersona.IdMotivoBaja)

        HDAO.UpdateT(cn, "_RolPersonaUpdate", ArrayParametros, T)
    End Sub

End Class

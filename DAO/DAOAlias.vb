﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports Entidades
Public Class DAOAlias
    Dim objConexion As New DAO.Conexion
    Public Function InsertaAlias(ByVal obj As Entidades.AliasMant) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@al_Descripcion", SqlDbType.VarChar, 250)
        ArrayParametros(0).Value = IIf(obj.Descripcion = Nothing, DBNull.Value, obj.Descripcion)
        ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)


        Dim cmd As New SqlCommand("_AliasInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizaAlias(ByVal obj As Entidades.AliasMant) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdAlias", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(obj.IdAlias = Nothing, DBNull.Value, obj.IdAlias)
        ArrayParametros(1) = New SqlParameter("@al_Descripcion", SqlDbType.VarChar, 250)
        ArrayParametros(1).Value = IIf(obj.Descripcion = Nothing, DBNull.Value, obj.Descripcion)
        ArrayParametros(2) = New SqlParameter("@al_Estado", SqlDbType.Bit)
        ArrayParametros(2).Value = IIf(obj.Estado = Nothing, DBNull.Value, obj.Estado)
        ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)

        Dim cmd As New SqlCommand("_AliasUpdate", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.AliasMant)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AliasSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.AliasMant)
                Do While lector.Read
                    Dim obj As New Entidades.AliasMant
                    obj.IdAlias = CInt(IIf(IsDBNull(lector.Item("IdAlias")) = True, 0, lector.Item("IdAlias")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("al_Estado")) = True, 0, lector.Item("al_Estado")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("al_Descripcion")) = True, "", lector.Item("al_Descripcion")))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Persona = CStr(IIf(IsDBNull(lector.Item("Persona")) = True, "", lector.Item("Persona")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.AliasMant)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AliasSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.AliasMant)
                Do While lector.Read
                    Dim obj As New Entidades.AliasMant
                    obj.IdAlias = CInt(IIf(IsDBNull(lector.Item("IdAlias")) = True, 0, lector.Item("IdAlias")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("al_Estado")) = True, 0, lector.Item("al_Estado")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("al_Descripcion")) = True, "", lector.Item("al_Descripcion")))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Persona = CStr(IIf(IsDBNull(lector.Item("Persona")) = True, "", lector.Item("Persona")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.AliasMant)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AliasSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.AliasMant)
                Do While lector.Read
                    Dim obj As New Entidades.AliasMant
                    obj.IdAlias = CInt(IIf(IsDBNull(lector.Item("IdAlias")) = True, 0, lector.Item("IdAlias")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("al_Estado")) = True, 0, lector.Item("al_Estado")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("al_Descripcion")) = True, "", lector.Item("al_Descripcion")))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Persona = CStr(IIf(IsDBNull(lector.Item("Persona")) = True, "", lector.Item("Persona")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.AliasMant)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AliasSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdAlias", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.AliasMant)
                Do While lector.Read
                    Dim obj As New Entidades.AliasMant
                    obj.IdAlias = CInt(lector.Item("IdAlias"))
                    obj.Estado = CBool(lector.Item("al_Estado"))
                    obj.Descripcion = CStr(lector.Item("al_Descripcion"))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Persona = CStr(IIf(IsDBNull(lector.Item("Persona")) = True, "", lector.Item("Persona")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal Persona As String) As List(Of Entidades.AliasMant)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AliasSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Persona", Persona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.AliasMant)
                Do While lector.Read
                    Dim obj As New Entidades.AliasMant
                    obj.IdAlias = CInt(lector.Item("IdAlias"))
                    obj.Estado = CBool(lector.Item("al_Estado"))
                    obj.Descripcion = CStr(lector.Item("al_Descripcion"))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Persona = CStr(IIf(IsDBNull(lector.Item("Persona")) = True, "", lector.Item("Persona")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal Persona As String) As List(Of Entidades.AliasMant)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AliasSelectAllActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Persona", Persona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.AliasMant)
                Do While lector.Read
                    Dim obj As New Entidades.AliasMant
                    obj.IdAlias = CInt(lector.Item("IdAlias"))
                    obj.Estado = CBool(lector.Item("al_Estado"))
                    obj.Descripcion = CStr(lector.Item("al_Descripcion"))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Persona = CStr(IIf(IsDBNull(lector.Item("Persona")) = True, "", lector.Item("Persona")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal Persona As String) As List(Of Entidades.AliasMant)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AliasSelectAllInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Persona", Persona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.AliasMant)
                Do While lector.Read
                    Dim obj As New Entidades.AliasMant
                    obj.IdAlias = CInt(lector.Item("IdAlias"))
                    obj.Estado = CBool(lector.Item("al_Estado"))
                    obj.Descripcion = CStr(lector.Item("al_Descripcion"))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Persona = CStr(IIf(IsDBNull(lector.Item("Persona")) = True, "", lector.Item("Persona")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectIdPersonaAliasRandom(Optional ByVal cn As SqlConnection = Nothing, Optional ByVal sqltr As SqlTransaction = Nothing) As Integer
        Dim cmd As New SqlCommand
        cmd.CommandType = CommandType.StoredProcedure
        If cn Is Nothing Then
            cmd.Connection = objConexion.ConexionSIGE
            cmd.Connection.Open()
        Else
            cmd.Connection = cn
            cmd.Transaction = sqltr
        End If
        cmd.CommandText = "[_GetIdPersonaAliasRandom]"
        Try
            Return CInt(cmd.ExecuteScalar())
        Catch ex As Exception
            Return 0
        Finally

        End Try
    End Function

End Class

﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_EstadoLetras

    Public Function ListarEstadoLetras(ByVal cn As SqlConnection, ByVal IdEstado As Integer) As List(Of Entidades.be_EstadoLetras)

        Dim lista As List(Of Entidades.be_EstadoLetras) = Nothing
        Using cmd As New SqlCommand()
            With cmd

                .CommandText = "SP_SELECT_LETRASCOBRAR"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@Estado", SqlDbType.Int)).Value = IdEstado

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_EstadoLetras)
                Dim ind1 As Integer = rdr.GetOrdinal("Id_Estadolc")
                Dim ind2 As Integer = rdr.GetOrdinal("Nomb_Estado")


                Dim objeto As Entidades.be_EstadoLetras = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_EstadoLetras
                    With objeto

                        .Id_Estadolc = rdr.GetInt32(ind1)
                        .Nomb_Estado = rdr.GetString(ind2)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class

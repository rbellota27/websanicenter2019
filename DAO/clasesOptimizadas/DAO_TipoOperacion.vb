﻿Imports System.Data.SqlClient
Public Class DAO_TipoOperacion
    Inherits Conexion

    ' (ByVal cn As SqlConnection) As List(Of Entidades.Documento)
    Public Function listarTipoOperacion(ByVal cn As SqlConnection, ByVal IdMotivotraslado As Integer) As List(Of Entidades.be_TipoOperacion)

        Dim lista As List(Of Entidades.be_TipoOperacion) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTARTIPOOPERACION"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdMotivoT", SqlDbType.Int)).Value = IdMotivotraslado

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_TipoOperacion)
                Dim ind1 As Integer = rdr.GetOrdinal("IdTipoOperacion")
                Dim ind2 As Integer = rdr.GetOrdinal("top_Nombre")


                Dim objeto As Entidades.be_TipoOperacion = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_TipoOperacion
                    With objeto

                        .IdTipoOperacion = rdr.GetInt32(ind1)
                        .top_Nombre = rdr.GetString(ind2)
                      
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class




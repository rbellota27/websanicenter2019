﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_Concepto

    Public Function listarconcepto(ByVal cn As SqlConnection, ByVal IdTipoGasto As Integer) As List(Of Entidades.be_Concepto)

        Dim lista As List(Of Entidades.be_Concepto) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTARCONCEPTO_DETRACCIONES"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdTipoGast", SqlDbType.Int)).Value = IdTipoGasto

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_Concepto)
                Dim ind1 As Integer = rdr.GetOrdinal("IdConcepto")
                Dim ind2 As Integer = rdr.GetOrdinal("con_Nombre")


                Dim objeto As Entidades.be_Concepto = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_Concepto
                    With objeto

                        .IdConcepto = rdr.GetInt32(ind1)
                        .con_Nombre = rdr.GetString(ind2)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class

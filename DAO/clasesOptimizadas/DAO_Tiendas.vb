﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_Tiendas
    Public Function selectTiendas(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer) As List(Of Entidades.be_Tienda)

        Dim lista As List(Of Entidades.be_Tienda) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_TiendaSelectCboxEmpresa"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_Tienda)
                Dim ind1 As Integer = rdr.GetOrdinal("IdTienda")
                Dim ind2 As Integer = rdr.GetOrdinal("tie_Nombre")


                Dim objeto As Entidades.be_Tienda = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_Tienda
                    With objeto
                        .IdTienda = rdr.GetInt32(ind1)
                        .tie_Nombre = rdr.GetString(ind2)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista


    End Function

    Public Function selectTiendasN(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer) As List(Of Entidades.be_Tienda)

        Dim lista As List(Of Entidades.be_Tienda) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_TiendaSelectCboxEmpresaN"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_Tienda)
                Dim ind1 As Integer = rdr.GetOrdinal("IdTienda")
                Dim ind2 As Integer = rdr.GetOrdinal("tie_Nombre")


                Dim objeto As Entidades.be_Tienda = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_Tienda
                    With objeto
                        .IdTienda = rdr.GetInt32(ind1)
                        .tie_Nombre = rdr.GetString(ind2)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class

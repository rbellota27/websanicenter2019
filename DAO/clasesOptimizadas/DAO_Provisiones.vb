﻿Imports System.Data.SqlClient
Public Class DAO_Provisiones
    Inherits Conexion

    Public Function listaProvisiones(ByVal cn As SqlConnection, ByVal docSerie As String, ByVal docCodigo As String, ByVal fecInicio As Date, _
                                     ByVal fecFin As Date, ByVal flagFiltro As Integer, ByVal nroruc As String) As List(Of Entidades.Documento)
        Dim lista As List(Of Entidades.Documento) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTAR_PROVISIONES"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@par1", SqlDbType.NVarChar)).Value = docSerie
                .Parameters.Add(New SqlParameter("@par2", SqlDbType.NVarChar)).Value = docCodigo
                .Parameters.Add(New SqlParameter("@par3", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@par4", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@par5", SqlDbType.Int)).Value = flagFiltro
                .Parameters.Add(New SqlParameter("@par6", SqlDbType.NVarChar)).Value = nroruc

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.Documento)
                Dim ind1 As Integer = rdr.GetOrdinal("idDocumento")
                Dim ind2 As Integer = rdr.GetOrdinal("doc_fechaEmision")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_subTotal")
                Dim ind4 As Integer = rdr.GetOrdinal("doc_igv")
                Dim ind5 As Integer = rdr.GetOrdinal("totalAPagar")
                Dim ind6 As Integer = rdr.GetOrdinal("doc_total")
                Dim ind7 As Integer = rdr.GetOrdinal("proveedor")
                Dim ind8 As Integer = rdr.GetOrdinal("top_nombre")
                Dim ind9 As Integer = rdr.GetOrdinal("codigo")
                Dim ind10 As Integer = rdr.GetOrdinal("mr_monto")
                Dim ind11 As Integer = rdr.GetOrdinal("regimen")
                Dim ind12 As Integer = rdr.GetOrdinal("mon_simbolo")
                Dim ind13 As Integer = rdr.GetOrdinal("anex_deuda")

                Dim objetoProvision As Entidades.Documento = Nothing
                While rdr.Read()
                    objetoProvision = New Entidades.Documento
                    With objetoProvision
                        .IdDocumento = rdr.GetInt32(ind1)
                        .FechaEmision = rdr.GetDateTime(ind2)
                        .SubTotal = rdr.GetDecimal(ind3)
                        .IGV = rdr.GetDecimal(ind4)
                        .TotalAPagar = rdr.GetDecimal(ind5)
                        .Total = rdr.GetDecimal(ind6)
                        .regNombre = rdr.GetString(ind7)
                        .NomTipoOperacion = rdr.GetString(ind8)
                        .Codigo = rdr.GetString(ind9)
                        .Detraccion = rdr.GetDecimal(ind10)
                        .NomMoneda = rdr.GetString(ind12)
                        .montoPendiente = rdr.GetDecimal(ind13)
                        lista.Add(objetoProvision)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function listarDocumentosProvisionadosV2(ByVal cn As SqlConnection, ByVal docSerie As String, ByVal docCodigo As String, ByVal fecInicio As Date, _
                                     ByVal fecFin As Date, ByVal flagFiltro As Integer, ByVal nroruc As String, ByVal nrovoucher As String) As List(Of Entidades.Documento)
        Dim lista As List(Of Entidades.Documento) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTAR_PROVISIONES_CONTA"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@par1", SqlDbType.NVarChar)).Value = docSerie
                .Parameters.Add(New SqlParameter("@par2", SqlDbType.NVarChar)).Value = docCodigo
                .Parameters.Add(New SqlParameter("@par3", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@par4", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@par5", SqlDbType.Int)).Value = flagFiltro
                .Parameters.Add(New SqlParameter("@par6", SqlDbType.NVarChar)).Value = nroruc
                .Parameters.Add(New SqlParameter("@par7", SqlDbType.NVarChar)).Value = nrovoucher
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.Documento)
                Dim ind1 As Integer = rdr.GetOrdinal("idDocumento")
                Dim ind2 As Integer = rdr.GetOrdinal("doc_fechaEmision")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_subTotal")
                Dim ind4 As Integer = rdr.GetOrdinal("doc_igv")
                Dim ind5 As Integer = rdr.GetOrdinal("totalAPagar")
                Dim ind6 As Integer = rdr.GetOrdinal("doc_total")
                Dim ind7 As Integer = rdr.GetOrdinal("proveedor")
                Dim ind8 As Integer = rdr.GetOrdinal("top_nombre")
                Dim ind9 As Integer = rdr.GetOrdinal("codigo")
                Dim ind10 As Integer = rdr.GetOrdinal("mr_monto")
                Dim ind11 As Integer = rdr.GetOrdinal("regimen")
                Dim ind12 As Integer = rdr.GetOrdinal("mon_simbolo")
                Dim ind13 As Integer = rdr.GetOrdinal("anex_deuda")

                Dim objetoProvision As Entidades.Documento = Nothing
                While rdr.Read()
                    objetoProvision = New Entidades.Documento
                    With objetoProvision
                        .IdDocumento = rdr.GetInt32(ind1)
                        .FechaEmision = rdr.GetDateTime(ind2)
                        .SubTotal = rdr.GetDecimal(ind3)
                        .IGV = rdr.GetDecimal(ind4)
                        .TotalAPagar = rdr.GetDecimal(ind5)
                        .Total = rdr.GetDecimal(ind6)
                        .regNombre = rdr.GetString(ind7)
                        .NomTipoOperacion = rdr.GetString(ind8)
                        .Codigo = rdr.GetString(ind9)
                        .Detraccion = rdr.GetDecimal(ind10)
                        .NomMoneda = rdr.GetString(ind12)
                        .montoPendiente = rdr.GetDecimal(ind13)
                        lista.Add(objetoProvision)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function listarRelacionRequetimento_Provision(ByVal cn As SqlConnection, ByVal idRequerimiento As Integer) As List(Of Entidades.Documento)
        Dim lista As List(Of Entidades.Documento) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_SELECT_PROVI_X_REQUE"
                .Connection = cn
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdDocumentoRequerimiento", SqlDbType.Int)).Value = idRequerimiento
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.Documento)
                Dim ind1 As Integer = rdr.GetOrdinal("idDocumento")
                Dim ind2 As Integer = rdr.GetOrdinal("doc_fechaEmision")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_subTotal")
                Dim ind4 As Integer = rdr.GetOrdinal("doc_igv")
                Dim ind5 As Integer = rdr.GetOrdinal("totalAPagar")
                Dim ind6 As Integer = rdr.GetOrdinal("doc_total")
                Dim ind7 As Integer = rdr.GetOrdinal("proveedor")
                Dim ind8 As Integer = rdr.GetOrdinal("top_nombre")
                Dim ind9 As Integer = rdr.GetOrdinal("codigo")
                Dim ind10 As Integer = rdr.GetOrdinal("mr_monto")
                Dim ind11 As Integer = rdr.GetOrdinal("regimen")
                Dim ind12 As Integer = rdr.GetOrdinal("mon_simbolo")

                Dim objetoProvision As Entidades.Documento = Nothing
                While rdr.Read()
                    objetoProvision = New Entidades.Documento
                    With objetoProvision
                        .IdDocumento = rdr.GetInt32(ind1)
                        .FechaEmision = rdr.GetDateTime(ind2)
                        .SubTotal = rdr.GetDecimal(ind3)
                        .IGV = rdr.GetDecimal(ind4)
                        .TotalAPagar = rdr.GetDecimal(ind5)
                        .Total = rdr.GetDecimal(ind6)
                        .regNombre = rdr.GetString(ind7)
                        .NomTipoOperacion = rdr.GetString(ind8)
                        .Codigo = rdr.GetString(ind9)
                        .Detraccion = rdr.GetDecimal(ind10)
                        .NomMoneda = rdr.GetString(ind12)
                        lista.Add(objetoProvision)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function
End Class

﻿Imports System.Data.SqlClient
Imports Entidades
Public Class DAO_Anexo_Doc


    Public Sub ActualizarConstancia(ByVal cn As SqlConnection, ByVal idDocumento As Integer, ByVal Constancia As String, ByVal periodo As Integer)

        Using cmd As New SqlCommand("UpdateAnexo_Constancia", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
                .Parameters.Add(New SqlParameter("@Constancia", SqlDbType.NVarChar)).Value = Constancia
                .Parameters.Add(New SqlParameter("@periodo", SqlDbType.Int)).Value = periodo
            End With
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub




End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL.
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOReporteImagen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaImagenReporte(ByVal objReporteImagen As Entidades.ReporteImagen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("InsUpd_ReporteImagen", cn)
        Try
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdImagen", SqlDbType.Int)).Value = objReporteImagen.IdImagen
                .Parameters.Add(New SqlParameter("@Nombre", SqlDbType.VarChar, 50)).Value = objReporteImagen.Nombre
                .Parameters.Add(New SqlParameter("@Imagen", SqlDbType.Image)).Value = objReporteImagen.Imagen
            End With
            cn.Open()
            cmd.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListNomImagen() As Entidades.ReporteImagen
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("List_ImagenReporte", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objRptImagen As New Entidades.ReporteImagen
                If lector.Read Then
                    objRptImagen.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    objRptImagen.Imagen = CType(lector.Item("Imagen"), Byte())
                End If
                lector.Close()
                Return objRptImagen
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function RptNomImagen() As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("List_ImagenReporte", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTipoTelefono
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTipoTelefono(ByVal tipotelefono As Entidades.TipoTelefono) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@ttel_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipotelefono.Nombre
        ArrayParametros(1) = New SqlParameter("@ttel_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipotelefono.Estado
        Return HDAO.Insert(cn, "_TipoTelefonoInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoTelefono(ByVal tipotelefono As Entidades.TipoTelefono) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ttel_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipotelefono.Nombre
        ArrayParametros(1) = New SqlParameter("@ttel_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipotelefono.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoTelefono", SqlDbType.Int)
        ArrayParametros(2).Value = tipotelefono.Id
        Return HDAO.Update(cn, "_TipoTelefonoUpdate", ArrayParametros)
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoTelefono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTelefonoCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTelefono)
                Do While lector.Read
                    Dim TipoTelefono As New Entidades.TipoTelefono
                    TipoTelefono.Id = CInt(lector.Item("IdTipoTelefono"))
                    TipoTelefono.Nombre = CStr(lector.Item("ttel_Nombre"))
                    Lista.Add(TipoTelefono)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoTelefono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTelefonoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTelefono)
                Do While lector.Read
                    Dim TipoTelefono As New Entidades.TipoTelefono
                    TipoTelefono.Id = CInt(lector.Item("IdTipoTelefono"))
                    TipoTelefono.Nombre = CStr(lector.Item("ttel_Nombre"))
                    TipoTelefono.Estado = CStr(lector.Item("ttel_Estado"))
                    Lista.Add(TipoTelefono)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoTelefono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTelefonoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTelefono)
                Do While lector.Read
                    Dim TipoTelefono As New Entidades.TipoTelefono
                    TipoTelefono.Id = CInt(lector.Item("IdTipoTelefono"))
                    TipoTelefono.Nombre = CStr(lector.Item("ttel_Nombre"))
                    TipoTelefono.Estado = CStr(lector.Item("ttel_Estado"))
                    Lista.Add(TipoTelefono)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoTelefono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTelefonoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTelefono)
                Do While lector.Read
                    Dim TipoTelefono As New Entidades.TipoTelefono
                    TipoTelefono.Id = CInt(lector.Item("IdTipoTelefono"))
                    TipoTelefono.Nombre = CStr(lector.Item("ttel_Nombre"))
                    TipoTelefono.Estado = CStr(lector.Item("ttel_Estado"))
                    Lista.Add(TipoTelefono)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoTelefono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTelefonoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTelefono)
                Do While lector.Read
                    Dim TipoTelefono As New Entidades.TipoTelefono
                    TipoTelefono.Id = CInt(lector.Item("IdTipoTelefono"))
                    TipoTelefono.Nombre = CStr(lector.Item("ttel_Nombre"))
                    TipoTelefono.Estado = CStr(lector.Item("ttel_Estado"))
                    Lista.Add(TipoTelefono)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoTelefono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTelefonoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTelefono)
                Do While lector.Read
                    Dim TipoTelefono As New Entidades.TipoTelefono
                    TipoTelefono.Id = CInt(lector.Item("IdTipoTelefono"))
                    TipoTelefono.Nombre = CStr(lector.Item("ttel_Nombre"))
                    TipoTelefono.Estado = CStr(lector.Item("ttel_Estado"))
                    Lista.Add(TipoTelefono)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoTelefono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTelefonoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTelefono)
                Do While lector.Read
                    Dim TipoTelefono As New Entidades.TipoTelefono
                    TipoTelefono.Id = CInt(lector.Item("IdTipoTelefono"))
                    TipoTelefono.Nombre = CStr(lector.Item("ttel_Nombre"))
                    TipoTelefono.Estado = CStr(lector.Item("ttel_Estado"))
                    Lista.Add(TipoTelefono)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoTelefono)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTelefonoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTelefono)
                Do While lector.Read
                    Dim TipoTelefono As New Entidades.TipoTelefono
                    TipoTelefono.Id = CInt(lector.Item("IdTipoTelefono"))
                    TipoTelefono.Nombre = CStr(lector.Item("ttel_Nombre"))
                    TipoTelefono.Estado = CStr(lector.Item("ttel_Estado"))
                    Lista.Add(TipoTelefono)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

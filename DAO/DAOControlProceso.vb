﻿Imports System.Data.SqlClient
Public Class DAOControlProceso
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function SelectxIdControl(ByVal idproceso As Integer) As List(Of Entidades.ControlProceso)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ControlSelectxIdproceso", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProceso", idproceso)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ControlProceso)
                Do While lector.Read
                    Dim control As New Entidades.ControlProceso
                    control.proc = CInt(IIf(IsDBNull(lector.Item("Idproceso")) = True, 0, lector.Item("Idproceso")))
                    control.Proceso = CStr(IIf(IsDBNull(lector.Item("Proceso")) = True, "", lector.Item("Proceso")))
                    control.proc1 = CInt(IIf(IsDBNull(lector.Item("Idproceso1")) = True, 0, lector.Item("Idproceso1")))
                    control.proceso1 = CStr(IIf(IsDBNull(lector.Item("Proceso1")) = True, "", lector.Item("Proceso1")))
                    control.proc2 = CInt(IIf(IsDBNull(lector.Item("Idproceso2")) = True, 0, lector.Item("Idproceso2")))
                    control.proceso2 = CStr(IIf(IsDBNull(lector.Item("Proceso2")) = True, "", lector.Item("Proceso2")))

                    Lista.Add(control)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxNombreControl(ByVal nombre As String)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ControlSelectxNombre ", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ControlProceso)
                Do While lector.Read
                    Dim control As New Entidades.ControlProceso
                    control.proc = CInt(IIf(IsDBNull(lector.Item("Idproceso")) = True, 0, lector.Item("Idproceso")))
                    control.proc1 = CInt(IIf(IsDBNull(lector.Item("Proceso1")) = True, 0, lector.Item("Proceso1")))
                    control.proc2 = CInt(IIf(IsDBNull(lector.Item("Proceso2")) = True, 0, lector.Item("Proceso2")))

                    Lista.Add(control)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectControlProc(ByVal var As String) As List(Of Entidades.ControlProceso)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_MANT_CONT_PROC", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@var", var)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ControlProceso)
                Do While lector.Read
                    Dim proceso As New Entidades.ControlProceso
                    proceso.Proceso = CStr(IIf(IsDBNull(lector.Item("Proceso")) = True, "", lector.Item("Proceso")))
                    proceso.proceso1 = CStr(IIf(IsDBNull(lector.Item("Proceso1")) = True, "", lector.Item("Proceso1")))
                    proceso.proceso2 = CStr(IIf(IsDBNull(lector.Item("Proceso2")) = True, "", lector.Item("Proceso2")))
                    Lista.Add(proceso)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function AgregarControlProceso(ByVal control As Entidades.ControlProceso, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Integer

        Try
            Dim cmd As New SqlCommand("USP_MANT_CONT_PROC", cn, tr)
            Dim a As Integer
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@id", control.proc)
            cmd.Parameters.AddWithValue("@proceso1", control.proc1)
            cmd.Parameters.AddWithValue("@proceso2", control.proc2)
            cmd.Parameters.AddWithValue("@var", var)
            a = cmd.ExecuteNonQuery
            If a = 0 Then
                Return False
            End If
            Return a
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ActualizaControlProceso(ByVal control As Entidades.ControlProceso, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Integer

        Try
            Dim cmd As New SqlCommand("USP_MANT_CONT_PROC", cn, tr)
            Dim a As Integer
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@id", control.proc)
            cmd.Parameters.AddWithValue("@proceso1", control.proc1)
            cmd.Parameters.AddWithValue("@proceso2", control.proc2)
            cmd.Parameters.AddWithValue("@var", var)
            a = cmd.ExecuteNonQuery
            If a = 0 Then
                Return False
            End If
            Return a
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function EliminaControlProceso(ByVal id As Integer, ByVal cn As SqlConnection, ByVal var As String, Optional ByVal tr As SqlTransaction = Nothing) As Boolean

        Try
            Dim cmd As New SqlCommand("USP_MANT_CONT_PROC", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@id", id)
            cmd.Parameters.AddWithValue("@var", var)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Function
End Class

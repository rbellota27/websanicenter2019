﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTransito
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivo() As List(Of Entidades.Transito)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Transito)
                Do While lector.Read
                    Dim objTransito As New Entidades.Transito
                    objTransito.Abv = CStr(IIf(IsDBNull(lector.Item("Tran_Abv")) = True, "", lector.Item("Tran_Abv")))
                    objTransito.Nombre = CStr(IIf(IsDBNull(lector.Item("Tran_Nombre")) = True, "", lector.Item("Tran_Nombre")))
                    objTransito.Estado = CBool(IIf(IsDBNull(lector.Item("Tran_Estado")) = True, "", lector.Item("Tran_Estado")))
                    objTransito.IdTransito = CInt(IIf(IsDBNull(lector.Item("IdTransito")) = True, 0, lector.Item("IdTransito")))
                    objTransito.Tran_Codigo = CStr(IIf(IsDBNull(lector.Item("Tran_Codigo")) = True, "", lector.Item("Tran_Codigo")))
                    Lista.Add(objTransito)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Transito)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Transito)
                Do While lector.Read
                    Dim objTransito As New Entidades.Transito
                    objTransito.Abv = CStr(IIf(IsDBNull(lector.Item("Tran_Abv")) = True, "", lector.Item("Tran_Abv")))
                    objTransito.Nombre = CStr(IIf(IsDBNull(lector.Item("Tran_Nombre")) = True, "", lector.Item("Tran_Nombre")))
                    objTransito.Estado = CBool(IIf(IsDBNull(lector.Item("Tran_Estado")) = True, "", lector.Item("Tran_Estado")))
                    objTransito.IdTransito = CInt(IIf(IsDBNull(lector.Item("IdTransito")) = True, 0, lector.Item("IdTransito")))
                    objTransito.Tran_Codigo = CStr(IIf(IsDBNull(lector.Item("Tran_Codigo")) = True, "", lector.Item("Tran_Codigo")))
                    Lista.Add(objTransito)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Transito)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Transito)
                Do While lector.Read
                    Dim objTransito As New Entidades.Transito
                    objTransito.Abv = CStr(IIf(IsDBNull(lector.Item("Tran_Abv")) = True, "", lector.Item("Tran_Abv")))
                    objTransito.Nombre = CStr(IIf(IsDBNull(lector.Item("Tran_Nombre")) = True, "", lector.Item("Tran_Nombre")))
                    objTransito.Estado = CBool(IIf(IsDBNull(lector.Item("Tran_Estado")) = True, "", lector.Item("Tran_Estado")))
                    objTransito.IdTransito = CInt(IIf(IsDBNull(lector.Item("IdTransito")) = True, 0, lector.Item("IdTransito")))
                    objTransito.Tran_Codigo = CStr(IIf(IsDBNull(lector.Item("Tran_Codigo")) = True, "", lector.Item("Tran_Codigo")))
                    Lista.Add(objTransito)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaTransito(ByVal transito As Entidades.Transito) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@Tran_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = transito.Nombre
        ArrayParametros(1) = New SqlParameter("@Tran_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = transito.Abv

        ArrayParametros(2) = New SqlParameter("@Tran_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = transito.Tran_Codigo

        ArrayParametros(3) = New SqlParameter("@Tran_Estado", SqlDbType.Bit)
        ArrayParametros(3).Value = transito.Estado
        'Return HDAO.Insert(cn, "_TransitoInsert", ArrayParametros)
        Dim cmd As New SqlCommand("_TransitoInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizaTransito(ByVal transito As Entidades.Transito) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@Tran_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = transito.Nombre
        ArrayParametros(1) = New SqlParameter("@Tran_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = transito.Abv

        ArrayParametros(2) = New SqlParameter("@Tran_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = transito.Tran_Codigo

        ArrayParametros(3) = New SqlParameter("@Tran_Estado", SqlDbType.Bit)
        ArrayParametros(3).Value = transito.Estado
        ArrayParametros(4) = New SqlParameter("@IdTransito", SqlDbType.Int)
        ArrayParametros(4).Value = transito.IdTransito
        Return HDAO.Update(cn, "_TransitoUpdate", ArrayParametros)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Transito)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Transito)
                Do While lector.Read
                    Dim objTransito As New Entidades.Transito
                    objTransito.Abv = CStr(IIf(IsDBNull(lector.Item("Tran_Abv")) = True, "", lector.Item("Tran_Abv")))
                    objTransito.Nombre = CStr(IIf(IsDBNull(lector.Item("Tran_Nombre")) = True, "", lector.Item("Tran_Nombre")))
                    objTransito.Estado = CBool(IIf(IsDBNull(lector.Item("Tran_Estado")) = True, "", lector.Item("Tran_Estado")))
                    objTransito.IdTransito = CInt(IIf(IsDBNull(lector.Item("IdTransito")) = True, 0, lector.Item("IdTransito")))
                    Lista.Add(objTransito)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Transito)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Transito)
                Do While lector.Read
                    Dim objTransito As New Entidades.Transito
                    objTransito.Abv = CStr(IIf(IsDBNull(lector.Item("Tran_Abv")) = True, "", lector.Item("Tran_Abv")))
                    objTransito.Nombre = CStr(IIf(IsDBNull(lector.Item("Tran_Nombre")) = True, "", lector.Item("Tran_Nombre")))
                    objTransito.Estado = CBool(IIf(IsDBNull(lector.Item("Tran_Estado")) = True, "", lector.Item("Tran_Estado")))
                    objTransito.IdTransito = CInt(IIf(IsDBNull(lector.Item("IdTransito")) = True, 0, lector.Item("IdTransito")))
                    objTransito.Tran_Codigo = CStr(IIf(IsDBNull(lector.Item("Tran_Codigo")) = True, "", lector.Item("Tran_Codigo")))
                    Lista.Add(objTransito)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Transito)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Transito)
                Do While lector.Read
                    Dim objTransito As New Entidades.Transito
                    objTransito.Abv = CStr(IIf(IsDBNull(lector.Item("Tran_Abv")) = True, "", lector.Item("Tran_Abv")))
                    objTransito.Nombre = CStr(IIf(IsDBNull(lector.Item("Tran_Nombre")) = True, "", lector.Item("Tran_Nombre")))
                    objTransito.Estado = CBool(IIf(IsDBNull(lector.Item("Tran_Estado")) = True, "", lector.Item("Tran_Estado")))
                    objTransito.IdTransito = CInt(IIf(IsDBNull(lector.Item("IdTransito")) = True, 0, lector.Item("IdTransito")))
                    objTransito.Tran_Codigo = CStr(IIf(IsDBNull(lector.Item("Tran_Codigo")) = True, "", lector.Item("Tran_Codigo")))
                    Lista.Add(objTransito)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Transito)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Transito)
                Do While lector.Read
                    Dim objTransito As New Entidades.Transito
                    objTransito.Abv = CStr(IIf(IsDBNull(lector.Item("Tran_Abv")) = True, "", lector.Item("Tran_Abv")))
                    objTransito.Nombre = CStr(IIf(IsDBNull(lector.Item("Tran_Nombre")) = True, "", lector.Item("Tran_Nombre")))
                    objTransito.Estado = CBool(IIf(IsDBNull(lector.Item("Tran_Estado")) = True, "", lector.Item("Tran_Estado")))
                    objTransito.IdTransito = CInt(IIf(IsDBNull(lector.Item("IdTransito")) = True, 0, lector.Item("IdTransito")))
                    objTransito.Tran_Codigo = CStr(IIf(IsDBNull(lector.Item("Tran_Codigo")) = True, "", lector.Item("Tran_Codigo")))
                    Lista.Add(objTransito)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

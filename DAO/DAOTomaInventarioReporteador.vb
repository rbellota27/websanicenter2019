﻿Imports System.Data.SqlClient

Public Class DAOTomaInventarioReporteador
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Function GetTomaInventario(ByVal idInventario As Integer) As Entidades.TomaInventarioReporteador

        Dim objDocumento As Entidades.TomaInventarioReporteador = Nothing

        Dim cn As SqlConnection = objConexion.ConexionREPORTEADOR

        Try
            Dim cmd As New SqlCommand("SIGE_TomaInventario_Get", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Idtinventario", idInventario)

            cn.Open()

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (lector.Read) Then

                objDocumento = New Entidades.TomaInventarioReporteador
                With objDocumento

                    .Idtinventario = CInt(IIf(IsDBNull(lector("Idtinventario")) = True, 0, lector("Idtinventario")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))

                End With

            End If

            lector.Close()

        Catch ex As Exception

            objDocumento = Nothing

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try


        Return objDocumento

    End Function

    Public Function SelectAll(ByVal idTienda As Integer, ByVal fecha As Date) As List(Of Entidades.TomaInventarioReporteador)
        Dim cn As SqlConnection = objConexion.ConexionREPORTEADOR
        Dim cmd As New SqlCommand("", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TomaInventarioReporteador)
                Do While lector.Read
                    Dim obj As New Entidades.TomaInventarioReporteador

                    obj.Idtinventario = CInt(IIf(IsDBNull(lector("Idtinventario")) = True, 0, lector("Idtinventario")))
                    obj.IdTienda = CStr(IIf(IsDBNull(lector("ar_NombreCorto")) = True, "", lector("ar_NombreCorto")))
                    obj.IdAlmacen = CStr(IIf(IsDBNull(lector("ar_NombreCorto")) = True, "", lector("ar_NombreCorto")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function UpdateTomaInventario(ByVal objDocumento As Entidades.TomaInventarioReporteador) As Integer

        Dim cn As SqlConnection = objConexion.ConexionREPORTEADOR
        Dim tr As SqlTransaction = Nothing
        Dim IdDocumento As Integer = 0
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("SIGE_TomaInventario_Update", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@Idtinventario", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(objDocumento.Idtinventario = Nothing, DBNull.Value, objDocumento.Idtinventario)
            ArrayParametros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(objDocumento.IdTienda = Nothing, DBNull.Value, objDocumento.IdTienda)

            cmd.Parameters.AddRange(ArrayParametros)
            If cmd.ExecuteNonQuery = 0 Then
                Throw New Exception
            Else
                IdDocumento = CInt(cmd.ExecuteScalar)
            End If

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            IdDocumento = -1

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try


        Return IdDocumento


    End Function
End Class

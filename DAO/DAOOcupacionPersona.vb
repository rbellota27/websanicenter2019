'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOOcupacionPersona
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaOcupacionPersona(ByVal cn As SqlConnection, ByVal OcupacionPersona As Entidades.OcupacionPersona, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdOcupacion", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(OcupacionPersona.IdOcupacion = Nothing, DBNull.Value, OcupacionPersona.IdOcupacion)
        ArrayParametros(1) = New SqlParameter("@op_Principal", SqlDbType.Bit)
        ArrayParametros(1).Value = IIf(OcupacionPersona.Principal = Nothing, DBNull.Value, OcupacionPersona.Principal)
        ArrayParametros(2) = New SqlParameter("@IdGiro", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(OcupacionPersona.IdGiro = Nothing, DBNull.Value, OcupacionPersona.IdGiro)
        ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(OcupacionPersona.IdPersona = Nothing, DBNull.Value, OcupacionPersona.IdPersona)
        HDAO.InsertaT(cn, "_OcupacionPersonaInsert", ArrayParametros, T)
    End Sub

    Public Sub InsertaListaOcupacionPersona(ByVal cn As SqlConnection, ByVal OcupacionPersona As List(Of Entidades.OcupacionPersona), ByVal T As SqlTransaction)
        Dim objOcupacionPersona As Entidades.OcupacionPersona
        For Each objOcupacionPersona In OcupacionPersona
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@IdOcupacion", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(objOcupacionPersona.IdOcupacion = Nothing, DBNull.Value, objOcupacionPersona.IdOcupacion)
            ArrayParametros(1) = New SqlParameter("@op_Principal", SqlDbType.Bit)
            ArrayParametros(1).Value = IIf(objOcupacionPersona.Principal = Nothing, DBNull.Value, objOcupacionPersona.Principal)
            ArrayParametros(2) = New SqlParameter("@IdGiro", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(objOcupacionPersona.IdGiro = Nothing, DBNull.Value, objOcupacionPersona.IdGiro)
            ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(objOcupacionPersona.IdPersona = Nothing, DBNull.Value, objOcupacionPersona.IdPersona)
            HDAO.InsertaT(cn, "_OcupacionPersonaInsert", ArrayParametros, T)
        Next
    End Sub

    Public Function ActualizaOcupacionPersona(ByVal ocupacionpersona As Entidades.OcupacionPersona) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdOcupacion", SqlDbType.Int)
        ArrayParametros(0).Value = ocupacionpersona.IdOcupacion
        ArrayParametros(1) = New SqlParameter("@op_Principal", SqlDbType.Bit)
        ArrayParametros(1).Value = ocupacionpersona.Principal
        ArrayParametros(2) = New SqlParameter("@IdGiro", SqlDbType.Int)
        ArrayParametros(2).Value = ocupacionpersona.IdGiro
        ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(3).Value = ocupacionpersona.IdPersona
        Return HDAO.Update(cn, "_OcupacionPersonaUpdate", ArrayParametros)
    End Function
    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.OcupacionPersona
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OcupacionPersonaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objOcupacionPersona As New Entidades.OcupacionPersona
            If lector.Read Then
                objOcupacionPersona = New Entidades.OcupacionPersona
                objOcupacionPersona.IdOcupacion = CInt(IIf(IsDBNull(lector.Item("IdOcupacion")) = True, 0, lector.Item("IdOcupacion")))
                objOcupacionPersona.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                objOcupacionPersona.Principal = CBool(IIf(IsDBNull(lector.Item("op_Principal")) = True, Nothing, lector.Item("op_Principal")))
                objOcupacionPersona.IdGiro = CInt(IIf(IsDBNull(lector.Item("IdGiro")) = True, 0, lector.Item("IdGiro")))
            End If
            lector.Close()
            Return objOcupacionPersona
        End Using
    End Function
    Public Sub ActualizaOcupacionPersonaT(ByVal cn As SqlConnection, ByVal ocupacionpersona As Entidades.OcupacionPersona, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdOcupacion", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(ocupacionpersona.IdOcupacion = Nothing, DBNull.Value, ocupacionpersona.IdOcupacion)
        ArrayParametros(1) = New SqlParameter("@op_Principal", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(ocupacionpersona.Principal = Nothing, DBNull.Value, ocupacionpersona.Principal)
        ArrayParametros(2) = New SqlParameter("@IdGiro", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(ocupacionpersona.IdGiro = Nothing, DBNull.Value, ocupacionpersona.IdGiro)
        ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(ocupacionpersona.IdPersona = Nothing, DBNull.Value, ocupacionpersona.IdPersona)

        HDAO.UpdateT(cn, "_OcupacionPersonaUpdate", ArrayParametros, T)
    End Sub


    Public Function Selectx_Id(ByVal IdPersona As Integer) As List(Of Entidades.OcupacionPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OcupacionPersonaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.OcupacionPersona)
                Do While lector.Read
                    Dim objOcupacionPersona As New Entidades.OcupacionPersona
                    objOcupacionPersona = New Entidades.OcupacionPersona
                    objOcupacionPersona.IdOcupacion = CInt(IIf(IsDBNull(lector.Item("IdOcupacion")) = True, 0, lector.Item("IdOcupacion")))
                    objOcupacionPersona.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    objOcupacionPersona.Principal = CBool(IIf(IsDBNull(lector.Item("op_Principal")) = True, Nothing, lector.Item("op_Principal")))
                    objOcupacionPersona.IdGiro = CInt(IIf(IsDBNull(lector.Item("IdGiro")) = True, 0, lector.Item("IdGiro")))
                    Lista.Add(objOcupacionPersona)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function



End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'''''''''''''''LUNES 15 MARZO 7:12 PM

Imports System.Data.SqlClient
Public Class DAOReportes
    Dim objConexion As New Conexion

    Public Function getResumidoGlobal(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As String) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet

        Try
            Dim cmd As New SqlCommand("_VentaResumenGerencial", objConexion.ConexionSIGE)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            cmd.Parameters.AddWithValue("@Fecha", Fecha)
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)


            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Venta")


            cmd.CommandText = "_CompraResumenGerencial"

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Compra")


            cmd.Parameters.RemoveAt("@Fecha")
            cmd.CommandText = "_CxCResumenGerencial"

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CxC")


            cmd.CommandText = "_CxPResumenGerencial"

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CxP")

            cmd.Parameters.RemoveAt("@IdEmpresa")
            cmd.Parameters.RemoveAt("@IdTienda")
            cmd.CommandText = "_Inventario_test"

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Inv")

          
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function

    Public Function getDataSetReciboIngreso(ByVal IdDocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_ReciboIngresoCab", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "DT_ReciboIngresoCab")



            cmd.CommandText = "_CR_ReciboIngresoDet"
            da_D = New SqlDataAdapter(cmd)
            da_D.Fill(ds, "DT_ReciboIngresoDet")


            cmd.CommandText = "_CR_ReciboIngresoCancelacion"
            da_D = New SqlDataAdapter(cmd)
            da_D.Fill(ds, "DT_ReciboIngresoCancelacion")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function getDataSetDocCotizacion(ByVal IdDocumento As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_CotizacionCab", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CotizacionCab")

            '*********** lleno detalle
            cmd.CommandText = "_CR_CotizacionDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CotizacionDet")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function getReporteDocCotizacion(ByVal IdDocumento As Integer, ByVal tipoimpresion As Integer) As DataSet
        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet

        Try
            Dim cmd As New SqlCommand("_CR_CotizacionCab", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CabCotizacion")

            '*********** lleno detalle
            If tipoimpresion = 3 Then
                cmd.CommandText = "_CR_CotizacionDetGroup"
            Else
                cmd.CommandText = "_CR_CotizacionDet"
            End If
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DetCotizacion")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", objConexion.ConexionSIGE)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function


    Public Function getReporteComprobantePercepcion(ByVal IdDocumento As Integer) As DataSet
        'Dim da, da2 As SqlDataAdapter
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_DocComprobantePercepcionCab", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")

            'detalle
            cmd.CommandText = "_CR_DocComprobantePercepcionDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalle")

            'Dim cmd2 As New SqlCommand("List_ImagenReporte", objConexion.ConexionSIGE)
            'cmd2.CommandType = CommandType.StoredProcedure
            'da2 = New SqlDataAdapter(cmd2)
            'da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function

    Public Function getDataSetInvNOValorizado(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_InventarioNOValorizado", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
            cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)

            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_InvNOValorizado")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getResumenCXCResumen(ByVal IdEmpresa As Integer, ByVal idtienda As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_CuentasxCobrarResumen", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", idtienda)
            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_InvNOValorizado")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function

    Public Function getDataSetMovCajaxParams(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idcaja As Integer, ByVal idmediopago As Integer, ByVal idtipomov As Integer, ByVal fechaI As Date, ByVal fechaF As Date) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_MovCajaxParams", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdTienda", idtienda)

            cmd.Parameters.AddWithValue("@IdCaja", idcaja)
            cmd.Parameters.AddWithValue("@IdMedioPago", idmediopago)
            cmd.Parameters.AddWithValue("@IdTipoMovimiento", idtipomov)


            cmd.Parameters.AddWithValue("@FechaInicio", fechaI)
            cmd.Parameters.AddWithValue("@FechaFin", fechaF)
            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_MovCaja")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetMovCajaxParams2(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idtipodocumento As Integer, ByVal idcaja As Integer, ByVal idmediopago As Integer, ByVal idtipomov As Integer, ByVal fechaI As Date, ByVal fechaF As Date) As DataSet
        Dim da As SqlDataAdapter
        Dim daQ As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_MovCajaxParams2", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdTienda", idtienda)
            cmd.Parameters.AddWithValue("@idtipoDocumento", idtipodocumento)
            cmd.Parameters.AddWithValue("@IdCaja", idcaja)
            cmd.Parameters.AddWithValue("@IdMedioPago", idmediopago)
            cmd.Parameters.AddWithValue("@IdTipoMovimiento", idtipomov)
            cmd.Parameters.AddWithValue("@FechaInicio", fechaI)
            cmd.Parameters.AddWithValue("@FechaFin", fechaF)
            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_MovCajaFechas")
            '***********

            'cmd.CommandText = "CR_MovCajaCantDoc"
            'daQ = New SqlDataAdapter(cmd)
            'daQ.Fill(ds, "DT_CantidadDocumentos")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetMovCajaCantDocumento(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idcaja As Integer, ByVal idmediopago As Integer, ByVal idtipomov As Integer, ByVal fechaI As Date, ByVal fechaF As Date) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("CR_MovCajaCantDoc", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdTienda", idtienda)
            cmd.Parameters.AddWithValue("@IdCaja", idcaja)
            cmd.Parameters.AddWithValue("@IdMedioPago", idmediopago)
            cmd.Parameters.AddWithValue("@IdTipoMovimiento", idtipomov)


            cmd.Parameters.AddWithValue("@FechaInicio", fechaI)
            cmd.Parameters.AddWithValue("@FechaFin", fechaF)
            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CantidadDocumentos")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function getDataSetCatalogoProducto_UM(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal UMedida As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try

            Dim cmd As New SqlCommand("_CR_CatalogoProducto_UM", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
            cmd.Parameters.AddWithValue("@UMedida", UMedida)

            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CatalogoProducto_UM")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetDocGuiaRecepcion(ByVal IdDocumento As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_DocGuiaRecepcionCabxIdDocumento", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocGuiaRecepcionCabxIdDocumento")

            '*********** lleno detalle
            cmd.CommandText = "_CR_DocDetalleGuia_AlmacenxIdDocumento"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalleGuia_AlmacenxIdDocumento")

            '*********** lleno doc relacionado
            cmd.CommandText = "_CR_DocGuiaRecepcion_DocRelacionados"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocGuiaRecepcion_DocRelacionados")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetDocGuiaRemision(ByVal IdDocumento As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_DocGuiaRemisionCab", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocGuiaRemisionCab")

            '*********** lleno detalle
            cmd.CommandText = "_CR_DocDetalleGuia_AlmacenxIdDocumento"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalleGuia_Almacen")

            '*********** lleno doc relacionado
            cmd.CommandText = "_CR_DocGuiaRecepcion_DocRelacionado"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocGuiaRemision_DocRelacionado")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetBoleta(ByVal IdDocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_SelectDocBoletaCabecera", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_DocBoletaCabecera")

            Dim cmd1 As New SqlCommand("_CR_SelectDocBoletaDetalle", objConexion.ConexionSIGE)
            cmd1.CommandType = CommandType.StoredProcedure
            cmd1.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            'cmd.CommandText = "_CR_SelectDocBoletaDetalle"
            da_D = New SqlDataAdapter(cmd1)
            da_D.Fill(ds, "CR_DocBoletaDetalle")

            For i As Integer = 0 To ds.Tables("CR_DocBoletaDetalle").Rows.Count - 1




            Next



        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetCotizacion(ByVal IdDocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_SelectDocPedidoCabecera", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_DocPedidoCabecera")
            cmd.CommandText = "_CR_SelectDocPedidoDetalle"
            da_D = New SqlDataAdapter(cmd)
            da_D.Fill(ds, "CR_DocPedidoDetalle")
            cmd.CommandText = "_CR_SelectDocPedidoClienteCondComercial"
            da_D = New SqlDataAdapter(cmd)
            da_D.Fill(ds, "CR_DocPedidoCondicionComercial")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function getDataSetPedidoCliente(ByVal IdDocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_SelectDocPedidoCabecera", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_DocPedidoCabecera")
            cmd.CommandText = "_CR_SelectDocPedidoDetalle"
            da_D = New SqlDataAdapter(cmd)
            da_D.Fill(ds, "CR_DocPedidoDetalle")
            cmd.CommandText = "_CR_SelectDocPedidoClienteCondComercial"
            da_D = New SqlDataAdapter(cmd)
            da_D.Fill(ds, "CR_DocPedidoCondicionComercial")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetFactura(ByVal IdDocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_SelectFacturaCabecera", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "_CR_SelectFacturaCabecera")
            cmd.CommandText = "_CR_SelectFacturaDetalle"
            da_D = New SqlDataAdapter(cmd)
            da_D.Fill(ds, "_CR_SelectFacturaDetalle")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetOrdenDespacho(ByVal IdDocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_DocOrdenDespachoCabecera", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "DT_DocOrdenDespachoCabecera")
            cmd.CommandText = "_CR_DocOrdenDespachoDetalle"
            da_D = New SqlDataAdapter(cmd)
            da_D.Fill(ds, "DT_DocOrdenDespachoDetalle")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetCatalogo() As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CatalogoSelectAll", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "_CatalogoSelectAll")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetInvInicial(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_Inventario_InicialxIdEmpresaxIdAlmacen", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
            cmd.Parameters.AddWithValue("@IdLinea", idlinea)
            cmd.Parameters.AddWithValue("@IdSublinea", idsublinea)

            da = New SqlDataAdapter(cmd)
            'da.SelectCommand = cmd
            da.Fill(ds, "CR_InventarioInicial")
        Catch ex As Exception
            Throw ex
        End Try
        Return ds
    End Function
    Public Function getDataSetDocInventarioInicial(ByVal idDocumento As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            'LLENO LA CABECERA
            Dim cmd As New SqlCommand("_CR_DocInvInicialCabeceraSelectxIdDocumento", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", idDocumento)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocInvInicialCabecera")
            'LLENO EL DETALLE
            cmd.CommandText = "_CR_DocInvInicialDetalleSelectxIdDocumento"
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.AddWithValue("@IdDocumento", idDocumento)
            da.SelectCommand = cmd
            da.Fill(ds, "DT_DocInvInicialDetalle")
        Catch ex As Exception
            Throw ex
        End Try
        Return ds
    End Function

    Public Function getResumendiarioVentas(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal fechaI As String, ByVal fechaF As String) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("CR_ResumenDiarioVentas", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdTienda", idtienda)
            cmd.Parameters.AddWithValue("@FechaInicio", fechaI)
            cmd.Parameters.AddWithValue("@FechaFin", fechaF)

            da = New SqlDataAdapter(cmd)
            'da.SelectCommand = cmd
            da.Fill(ds, "DT_ResumenDiarioVentas")

            'cmd.CommandText = "_CR_IngresosXTienda"
            'cmd.CommandType = CommandType.StoredProcedure
            'da.Fill(ds, "DT_IngresoxTienda")

        Catch ex As Exception
            Throw ex
        End Try
        Return ds
    End Function
    Public Function getDataSetChequeEgreso(ByVal IdDocumento As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_ChequeEgresoxId", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Id", IdDocumento)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_ChequeEgresoxId")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetChequeImpresion(ByVal IdDocumento As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_ChequeEgresoxIdxBancoImpresion", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Id", IdDocumento)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_ChequeEgresoxId")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetChequeImpresionV2(ByVal IdDocumento As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_ChequeEgresoxIdxBancoImpresion", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Id", IdDocumento)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_ChequeEgresoxId")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function



    Public Function getDataSetDocEgresos(ByVal IdDocumento As Integer, ByVal flag_banco As Boolean) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_ChequeEgresoxId", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Id", IdDocumento)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_ChequeEgresoxId1")

            Dim cmd1 As New SqlCommand("_CR_Cancelacion_Cab_SelectxId", objConexion.ConexionSIGE)
            cmd1.CommandType = CommandType.StoredProcedure
            cmd1.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da_C = New SqlDataAdapter(cmd1)
            da_C.Fill(ds, "Cabecera1")

            cmd1.CommandText = "_CR_egresos_Detalle_Select"
            cmd1.Parameters.AddWithValue("@flag_banco", flag_banco)
            da_D = New SqlDataAdapter(cmd1)
            da_D.Fill(ds, "Detalle1")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetDocCancelacion(ByVal IdDocumento As Integer, ByVal flag_banco As Boolean) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter

        Dim ds As New DataSet
        Try


            Dim cmd1 As New SqlCommand("_CR_Cancelacion_Cab_SelectxId", objConexion.ConexionSIGE)
            cmd1.CommandType = CommandType.StoredProcedure
            cmd1.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da_C = New SqlDataAdapter(cmd1)
            da_C.Fill(ds, "Cabecera")

            cmd1.CommandText = "_CR_Cancelacion_Detalle_Select"
            cmd1.Parameters.AddWithValue("@flag_banco", flag_banco)
            da_D = New SqlDataAdapter(cmd1)
            da_D.Fill(ds, "Detalle")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getDataSetDocCancelBancos(ByVal IdDocumento As Integer) As DataSet
        Return getDataSetDocCancelacion(IdDocumento, True)
    End Function
    Public Function getDataSetDocEgresos(ByVal IdDocumento As Integer) As DataSet
        Return getDataSetDocEgresos(IdDocumento, True)
    End Function
    Public Function getDataSetDocCancelCaja(ByVal IdDocumento As Integer) As DataSet
        Return getDataSetDocCancelacion(IdDocumento, False)
    End Function
    Public Function rptListarProductos(ByVal linea As Integer, ByVal sublinea As Integer, ByVal producto As Integer) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("CR_listaProductos", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Idlinea", linea)
            cmd.Parameters.AddWithValue("@Idsublinea", sublinea)
            cmd.Parameters.AddWithValue("@Idproducto", producto)



            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "CR_listaProductos")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function
End Class


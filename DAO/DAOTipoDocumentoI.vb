'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTipoDocumentoI
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTipoDocumentoI(ByVal tipodocumentoi As Entidades.TipoDocumentoI) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@tdoc_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = tipodocumentoi.CodigoSunat
        ArrayParametros(1) = New SqlParameter("@tdoc_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = tipodocumentoi.Nombre
        ArrayParametros(2) = New SqlParameter("@tdoc_Abv", SqlDbType.VarChar)
        ArrayParametros(2).Value = tipodocumentoi.Abv
        ArrayParametros(3) = New SqlParameter("@tdoc_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = tipodocumentoi.Estado
        Return HDAO.Insert(cn, "_TipoDocumentoIInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoDocumentoI(ByVal tipodocumentoi As Entidades.TipoDocumentoI) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@tdoc_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = tipodocumentoi.CodigoSunat
        ArrayParametros(1) = New SqlParameter("@tdoc_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = tipodocumentoi.Nombre
        ArrayParametros(2) = New SqlParameter("@tdoc_Abv", SqlDbType.VarChar)
        ArrayParametros(2).Value = tipodocumentoi.Abv
        ArrayParametros(3) = New SqlParameter("@tdoc_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = tipodocumentoi.Estado
        ArrayParametros(4) = New SqlParameter("@IdTipoDocumentoI", SqlDbType.Int)
        ArrayParametros(4).Value = tipodocumentoi.Id
        Return HDAO.Update(cn, "_TipoDocumentoIUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoISelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tdoc_Abv")) = True, "", lector.Item("tdoc_Abv")))
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("tdoc_CodigoSunat")) = True, "", lector.Item("tdoc_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tdoc_Estado")) = True, "", lector.Item("tdoc_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tdoc_Nombre")) = True, "", lector.Item("tdoc_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, "0", lector.Item("IdTipoDocumentoI")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoISelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tdoc_Abv")) = True, "", lector.Item("tdoc_Abv")))
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("tdoc_CodigoSunat")) = True, "", lector.Item("tdoc_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tdoc_Estado")) = True, "", lector.Item("tdoc_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tdoc_Nombre")) = True, "", lector.Item("tdoc_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, "0", lector.Item("IdTipoDocumentoI")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoISelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tdoc_Abv")) = True, "", lector.Item("tdoc_Abv")))
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("tdoc_CodigoSunat")) = True, "", lector.Item("tdoc_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tdoc_Estado")) = True, "", lector.Item("tdoc_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tdoc_Nombre")) = True, "", lector.Item("tdoc_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, "0", lector.Item("IdTipoDocumentoI")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoISelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tdoc_Abv")) = True, "", lector.Item("tdoc_Abv")))
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("tdoc_CodigoSunat")) = True, "", lector.Item("tdoc_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tdoc_Estado")) = True, "", lector.Item("tdoc_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tdoc_Nombre")) = True, "", lector.Item("tdoc_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, "0", lector.Item("IdTipoDocumentoI")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoISelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tdoc_Abv")) = True, "", lector.Item("tdoc_Abv")))
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("tdoc_CodigoSunat")) = True, "", lector.Item("tdoc_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tdoc_Estado")) = True, "", lector.Item("tdoc_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tdoc_Nombre")) = True, "", lector.Item("tdoc_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, "0", lector.Item("IdTipoDocumentoI")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoISelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tdoc_Abv")) = True, "", lector.Item("tdoc_Abv")))
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("tdoc_CodigoSunat")) = True, "", lector.Item("tdoc_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tdoc_Estado")) = True, "", lector.Item("tdoc_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tdoc_Nombre")) = True, "", lector.Item("tdoc_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, "0", lector.Item("IdTipoDocumentoI")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoISelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tdoc_Abv")) = True, "", lector.Item("tdoc_Abv")))
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("tdoc_CodigoSunat")) = True, "", lector.Item("tdoc_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tdoc_Estado")) = True, "", lector.Item("tdoc_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tdoc_Nombre")) = True, "", lector.Item("tdoc_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, "0", lector.Item("IdTipoDocumentoI")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxCodSunat(ByVal codsunat As String) As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoISelectxCodSunat", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod", codsunat)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tdoc_Abv")) = True, "", lector.Item("tdoc_Abv")))
                    obj.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("tdoc_CodigoSunat")) = True, "", lector.Item("tdoc_CodigoSunat")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tdoc_Estado")) = True, "", lector.Item("tdoc_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tdoc_Nombre")) = True, "", lector.Item("tdoc_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, "0", lector.Item("IdTipoDocumentoI")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

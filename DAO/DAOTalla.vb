'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTalla
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTalla(ByVal talla As Entidades.Talla) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tall_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = talla.Nombre
        ArrayParametros(1) = New SqlParameter("@talla_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = talla.Abv
        ArrayParametros(2) = New SqlParameter("@tall_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = talla.Estado
        Return HDAO.Insert(cn, "_TallaInsert", ArrayParametros)
    End Function
    Public Function ActualizaTalla(ByVal talla As Entidades.Talla) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@tall_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = talla.Nombre
        ArrayParametros(1) = New SqlParameter("@talla_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = talla.Abv
        ArrayParametros(2) = New SqlParameter("@tall_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = talla.Estado
        ArrayParametros(3) = New SqlParameter("@IdTalla", SqlDbType.Int)
        ArrayParametros(3).Value = talla.Id
        Return HDAO.Update(cn, "_TallaUpdate", ArrayParametros)
    End Function
    Public Function SelectAllActivo_Cbo() As List(Of Entidades.Talla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TallaSelectAllActivo_Cbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Talla)
                Do While lector.Read
                    Dim objTalla As New Entidades.Talla
                    objTalla.Id = CInt(lector.Item("IdTalla"))
                    objTalla.Nombre = CStr(lector.Item("tall_Nombre"))
                    Lista.Add(objTalla)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Talla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TallaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Talla)
                Do While lector.Read
                    Dim obj As New Entidades.Talla
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("talla_Abv")) = True, "", lector.Item("talla_Abv")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tall_Estado")) = True, "", lector.Item("tall_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tall_Nombre")) = True, "", lector.Item("tall_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTalla")) = True, "", lector.Item("IdTalla")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Talla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TallaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Talla)
                Do While lector.Read
                    Dim obj As New Entidades.Talla
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("talla_Abv")) = True, "", lector.Item("talla_Abv")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tall_Estado")) = True, "", lector.Item("tall_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tall_Nombre")) = True, "", lector.Item("tall_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTalla")) = True, "", lector.Item("IdTalla")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Talla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TallaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Talla)
                Do While lector.Read
                    Dim obj As New Entidades.Talla
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("talla_Abv")) = True, "", lector.Item("talla_Abv")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tall_Estado")) = True, "", lector.Item("tall_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tall_Nombre")) = True, "", lector.Item("tall_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTalla")) = True, "", lector.Item("IdTalla")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Talla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TallaSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Talla)
                Do While lector.Read
                    Dim obj As New Entidades.Talla
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("talla_Abv")) = True, "", lector.Item("talla_Abv")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tall_Estado")) = True, "", lector.Item("tall_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tall_Nombre")) = True, "", lector.Item("tall_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTalla")) = True, "", lector.Item("IdTalla")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Talla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TallaSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Talla)
                Do While lector.Read
                    Dim obj As New Entidades.Talla
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("talla_Abv")) = True, "", lector.Item("talla_Abv")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tall_Estado")) = True, "", lector.Item("tall_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tall_Nombre")) = True, "", lector.Item("tall_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTalla")) = True, "", lector.Item("IdTalla")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Talla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TallaSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Talla)
                Do While lector.Read
                    Dim obj As New Entidades.Talla
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("talla_Abv")) = True, "", lector.Item("talla_Abv")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tall_Estado")) = True, "", lector.Item("tall_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tall_Nombre")) = True, "", lector.Item("tall_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTalla")) = True, "", lector.Item("IdTalla")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Talla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TallaSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Talla)
                Do While lector.Read
                    Dim obj As New Entidades.Talla
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("talla_Abv")) = True, "", lector.Item("talla_Abv")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tall_Estado")) = True, "", lector.Item("tall_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tall_Nombre")) = True, "", lector.Item("tall_Nombre")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTalla")) = True, "", lector.Item("IdTalla")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

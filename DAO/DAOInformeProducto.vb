﻿Imports System.Data.SqlClient
Public Class DAOInformeProducto
    Dim ObjConexion As New Conexion
    Dim HDAO As New DAO.HelperDAO
    Dim objInforme As DAO.DAOInformeProducto
    Private Util As New DAO.DAOUtil


    Public Function Insert(ByVal cn As SqlConnection, ByVal obj As Entidades.Documento, ByVal T As SqlTransaction) As Integer


        Dim Parametro() As SqlParameter = New SqlParameter(25) {}


        Parametro(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        Parametro(0).Direction = ParameterDirection.Output

        Parametro(1) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        Parametro(1).Value = IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision)

        Parametro(2) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        Parametro(2).Value = IIf(obj.FechaAEntregar = Nothing, DBNull.Value, obj.FechaAEntregar)

        Parametro(3) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        Parametro(3).Value = IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc)

        Parametro(4) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
        Parametro(4).Value = IIf(obj.Descuento = Nothing, DBNull.Value, obj.Descuento)

        Parametro(5) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
        Parametro(5).Value = IIf(obj.SubTotal = Nothing, DBNull.Value, obj.SubTotal)

        Parametro(6) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
        Parametro(6).Value = IIf(obj.IGV = Nothing, DBNull.Value, obj.IGV)

        Parametro(7) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        Parametro(7).Value = IIf(obj.Total = Nothing, DBNull.Value, obj.Total)

        Parametro(8) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        Parametro(8).Value = IIf(obj.TotalLetras = Nothing, DBNull.Value, obj.TotalLetras)

        Parametro(9) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
        Parametro(9).Value = IIf(obj.TotalAPagar = Nothing, DBNull.Value, obj.TotalAPagar)

        Parametro(10) = New SqlParameter("@IdPersona", SqlDbType.Int) '** Proveedor
        Parametro(10).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)

        Parametro(11) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        Parametro(11).Value = IIf(obj.IdUsuario = Nothing, DBNull.Value, obj.IdUsuario)

        Parametro(12) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        Parametro(12).Value = IIf(obj.IdDestinatario = Nothing, DBNull.Value, obj.IdDestinatario)

        Parametro(13) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        Parametro(13).Value = IIf(obj.IdEstadoDoc = Nothing, DBNull.Value, obj.IdEstadoDoc)

        Parametro(14) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        Parametro(14).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)

        Parametro(15) = New SqlParameter("@Idalmacen", SqlDbType.Int)
        Parametro(15).Value = IIf(obj.IdAlmacen = Nothing, DBNull.Value, obj.IdAlmacen)

        Parametro(16) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        Parametro(16).Value = IIf(obj.IdTipoOperacion = Nothing, DBNull.Value, obj.IdTipoOperacion)

        Parametro(17) = New SqlParameter("@IdTienda", SqlDbType.Int)
        Parametro(17).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)

        Parametro(18) = New SqlParameter("@IdSerie", SqlDbType.Int)
        Parametro(18).Value = IIf(obj.IdSerie = Nothing, DBNull.Value, obj.IdSerie)

        Parametro(19) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        Parametro(19).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)

        Parametro(20) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        Parametro(20).Value = IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento)

        Parametro(21) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
        Parametro(21).Value = IIf(obj.IdEstadoCancelacion = Nothing, DBNull.Value, obj.IdEstadoCancelacion)

        Parametro(22) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
        Parametro(22).Value = IIf(obj.IdEstadoEntrega = Nothing, DBNull.Value, obj.IdEstadoEntrega)


        Parametro(23) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        Parametro(23).Value = IIf(obj.IdRemitente = Nothing, DBNull.Value, obj.IdRemitente)


        Parametro(24) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.Date)
        Parametro(24).Value = IIf(obj.FechaCancelacion = Nothing, DBNull.Value, obj.FechaCancelacion)

        Parametro(25) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        Parametro(25).Value = IIf(obj.IdCondicionPago = Nothing, DBNull.Value, obj.IdCondicionPago)

        Try

            Return HDAO.InsertaTParameterOutPut(cn, "_InsertInformeProducto", Parametro, T)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function Insert_Informe_Producto(ByVal ObjDocumento As Entidades.Documento, _
                                             ByVal listaDetalle As List(Of Entidades.DetalleDocumentoView)) _
                                       As String
        Dim cad As String = String.Empty
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

        Try
            ' ****************** DOCUMENTO
            cad = Me.Insert(cn, ObjDocumento, tr)
            ObjDocumento.Id = CInt(cad.Split(CChar(","))(0))


            ' ****************** DETALLEDOCUMENTO
           
            ' ****************** DETALLEDOCUMENTO
           

            For i As Integer = 0 To listaDetalle.Count - 1
                With listaDetalle.Item(i)
                    .IdDocumento = ObjDocumento.Id
                    Dim iddetalle As Integer = Me.InsertaDetalleDocumento_InformeProducto(cn, listaDetalle(i), ObjDocumento.Id, tr)

                End With
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function



    Public Function InsertaDetalleDocumento_InformeProducto(ByVal cn As SqlConnection, ByVal listaDetalleDocumento As Entidades.DetalleDocumentoView, ByVal IdDocumento As Integer, ByVal Tr As SqlTransaction) As Integer
        Try

            Dim cmd As New SqlCommand("_DetalleInformeInsert_Tonos", cn, Tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            'for cantidad de detalle
            'For i As Integer = 0 To listaDetalleDocumento.Count - 1

            Dim detalledocumento As Entidades.DetalleDocumentoView = listaDetalleDocumento

            Dim ArrayParametros() As SqlParameter = New SqlParameter(42) {}
            '****************
            Dim lista As New List(Of Entidades.Tono)
            lista = listaDetalleDocumento.listaTo

            Dim dt As New DataTable
            dt.Columns.Add("id")
            dt.Columns.Add("cantidad")
            If lista IsNot Nothing Then
                For h As Integer = 0 To lista.Count - 1
                    If (lista.Item(h).cantidadTono > 0) Then
                        Dim dr As DataRow = dt.NewRow
                        dr("id") = lista.Item(h).idTono
                        dr("cantidad") = lista.Item(h).cantidadTono
                        dt.Rows.Add(dr)
                    End If

                Next
            End If
            '***************
            ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
            ArrayParametros(0).Direction = ParameterDirection.Output
            ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(1).Value = IdDocumento
            ArrayParametros(2) = New SqlParameter("@idproducto", SqlDbType.Int)
            ArrayParametros(2).Value = detalledocumento.IdProducto
            ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
            ArrayParametros(3).Value = detalledocumento.IdUMedida
            ArrayParametros(4) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
            ArrayParametros(4).Value = IIf(detalledocumento.Cantidad = Nothing, DBNull.Value, detalledocumento.Cantidad)
            ArrayParametros(5) = New SqlParameter("@dc_CantxAtender", SqlDbType.Decimal)
            ArrayParametros(5).Value = IIf(detalledocumento.CantxAtender = Nothing, DBNull.Value, detalledocumento.CantxAtender)
            ArrayParametros(6) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
            ArrayParametros(6).Value = IIf(detalledocumento.UM = Nothing, DBNull.Value, detalledocumento.UM)
            ArrayParametros(7) = New SqlParameter("@dc_PrecioSD", SqlDbType.Decimal)
            ArrayParametros(7).Value = IIf(detalledocumento.PrecioSD = Nothing, DBNull.Value, detalledocumento.PrecioSD)
            ArrayParametros(8) = New SqlParameter("@dc_Descuento", SqlDbType.Decimal)
            ArrayParametros(8).Value = IIf(detalledocumento.Descuento = Nothing, DBNull.Value, detalledocumento.Descuento)
            ArrayParametros(9) = New SqlParameter("@dc_PrecioCD", SqlDbType.Decimal)
            ArrayParametros(9).Value = IIf(detalledocumento.PrecioCD = Nothing, DBNull.Value, detalledocumento.PrecioCD)
            ArrayParametros(10) = New SqlParameter("@dc_Importe", SqlDbType.Decimal)
            ArrayParametros(10).Value = IIf(detalledocumento.Importe = Nothing, DBNull.Value, detalledocumento.Importe)
            ArrayParametros(11) = New SqlParameter("@dc_Utilidad", SqlDbType.Decimal)
            ArrayParametros(11).Value = IIf(detalledocumento.Utilidad = Nothing, DBNull.Value, detalledocumento.Utilidad)
            ArrayParametros(12) = New SqlParameter("@dc_TasaPercepcion", SqlDbType.Decimal)
            ArrayParametros(12).Value = IIf(detalledocumento.TasaPercepcion = Nothing, DBNull.Value, detalledocumento.TasaPercepcion)
            ArrayParametros(13) = New SqlParameter("@dc_TasaDetraccion", SqlDbType.Decimal)
            ArrayParametros(13).Value = IIf(detalledocumento.TasaDetraccion = Nothing, DBNull.Value, detalledocumento.TasaDetraccion)
            ArrayParametros(14) = New SqlParameter("@dc_Peso", SqlDbType.Decimal)
            ArrayParametros(14).Value = IIf(detalledocumento.Peso = Nothing, DBNull.Value, detalledocumento.Peso)
            ArrayParametros(15) = New SqlParameter("@dc_UmPeso", SqlDbType.VarChar)
            ArrayParametros(15).Value = IIf(detalledocumento.UmPeso = Nothing, DBNull.Value, detalledocumento.UmPeso)
            ArrayParametros(16) = New SqlParameter("@IdDetalleAfecto", SqlDbType.Int)
            ArrayParametros(16).Value = IIf(detalledocumento.IdDetalleAfecto = Nothing, DBNull.Value, detalledocumento.IdDetalleAfecto)
            ArrayParametros(17) = New SqlParameter("@dc_DetalleGlosa", SqlDbType.VarChar)
            ArrayParametros(17).Value = IIf(detalledocumento.DetalleGlosa = Nothing, DBNull.Value, detalledocumento.DetalleGlosa)
            ArrayParametros(18) = New SqlParameter("@dc_PrecioLista", SqlDbType.Decimal)
            ArrayParametros(18).Value = IIf(detalledocumento.PrecioLista = Nothing, DBNull.Value, detalledocumento.PrecioLista)
            ArrayParametros(19) = New SqlParameter("@IdUsuarioSupervisor", SqlDbType.Int)
            ArrayParametros(19).Value = IIf(detalledocumento.IdUsuarioSupervisor = Nothing, DBNull.Value, detalledocumento.IdUsuarioSupervisor)
            ArrayParametros(20) = New SqlParameter("@dc_CantidadTransito", SqlDbType.Decimal)
            ArrayParametros(20).Value = IIf(detalledocumento.dc_CantidadTransito = Nothing, DBNull.Value, detalledocumento.dc_CantidadTransito)
            ArrayParametros(21) = New SqlParameter("@dc_PrecioSinIGV", SqlDbType.Decimal)
            ArrayParametros(21).Value = IIf(detalledocumento.dc_PrecioSinIGV = Nothing, DBNull.Value, detalledocumento.dc_PrecioSinIGV)
            ArrayParametros(22) = New SqlParameter("dc_Descuento1", SqlDbType.Decimal)
            ArrayParametros(22).Value = IIf(detalledocumento.dc_Descuento1 = Nothing, DBNull.Value, detalledocumento.dc_Descuento1)
            ArrayParametros(23) = New SqlParameter("@dc_Descuento2", SqlDbType.Decimal)
            ArrayParametros(23).Value = IIf(detalledocumento.dc_Descuento2 = Nothing, DBNull.Value, detalledocumento.dc_Descuento2)
            ArrayParametros(24) = New SqlParameter("@dc_Descuento3", SqlDbType.Decimal)
            ArrayParametros(24).Value = IIf(detalledocumento.dc_Descuento3 = Nothing, DBNull.Value, detalledocumento.dc_Descuento3)
            ArrayParametros(25) = New SqlParameter("@dt_tonos", SqlDbType.Structured)
            ArrayParametros(25).Value = dt

            'DetalleProducto
            ArrayParametros(26) = New SqlParameter("@Codigo", SqlDbType.Bit)
            ArrayParametros(26).Value = IIf(detalledocumento.codigo = Nothing, DBNull.Value, detalledocumento.codigo)
            ArrayParametros(27) = New SqlParameter("@Nan", SqlDbType.Bit)
            ArrayParametros(27).Value = IIf(detalledocumento.nan = Nothing, DBNull.Value, detalledocumento.nan)
            ArrayParametros(28) = New SqlParameter("@CAD", SqlDbType.Decimal)
            ArrayParametros(28).Value = IIf(detalledocumento.CAD = Nothing, DBNull.Value, detalledocumento.CAD)
            ArrayParametros(29) = New SqlParameter("@PesoCaja", SqlDbType.Decimal)
            ArrayParametros(29).Value = IIf(detalledocumento.PesoCaja = Nothing, DBNull.Value, detalledocumento.PesoCaja)
            ArrayParametros(30) = New SqlParameter("@MCaja", SqlDbType.Decimal)
            ArrayParametros(30).Value = IIf(detalledocumento.MCaja = Nothing, DBNull.Value, detalledocumento.MCaja)
            ArrayParametros(31) = New SqlParameter("@PzaCaja", SqlDbType.Decimal)
            ArrayParametros(31).Value = IIf(detalledocumento.PzaCaja = Nothing, DBNull.Value, detalledocumento.PzaCaja)
            ArrayParametros(32) = New SqlParameter("@NroCaja", SqlDbType.Decimal)
            ArrayParametros(32).Value = IIf(detalledocumento.NroCaja = Nothing, DBNull.Value, detalledocumento.NroCaja)
            ArrayParametros(33) = New SqlParameter("@Pzas", SqlDbType.Decimal)
            ArrayParametros(33).Value = IIf(detalledocumento.Pzas = Nothing, DBNull.Value, detalledocumento.Pzas)
            ArrayParametros(34) = New SqlParameter("@CalibRe", SqlDbType.Bit)
            ArrayParametros(34).Value = IIf(detalledocumento.CalibRe = Nothing, DBNull.Value, detalledocumento.CalibRe)
            ArrayParametros(35) = New SqlParameter("@JGO", SqlDbType.Decimal)
            ArrayParametros(35).Value = IIf(detalledocumento.JGO = Nothing, DBNull.Value, detalledocumento.JGO)
            ArrayParametros(36) = New SqlParameter("@NPallet", SqlDbType.Int)
            ArrayParametros(36).Value = IIf(detalledocumento.NPallet = Nothing, DBNull.Value, detalledocumento.NPallet)
            ArrayParametros(37) = New SqlParameter("@Total", SqlDbType.Decimal)
            ArrayParametros(37).Value = IIf(detalledocumento.Total = Nothing, DBNull.Value, detalledocumento.Total)
            ArrayParametros(38) = New SqlParameter("@Sucursal", SqlDbType.VarChar)
            ArrayParametros(38).Value = IIf(detalledocumento.Sucursal = Nothing, DBNull.Value, detalledocumento.Sucursal)
            ArrayParametros(39) = New SqlParameter("@TipoEmpaque", SqlDbType.VarChar)
            ArrayParametros(39).Value = IIf(detalledocumento.TipoEmpaque = Nothing, DBNull.Value, detalledocumento.TipoEmpaque)
            ArrayParametros(40) = New SqlParameter("@EstadoPallet", SqlDbType.VarChar)
            ArrayParametros(40).Value = IIf(detalledocumento.EstadoPallet = Nothing, DBNull.Value, detalledocumento.EstadoPallet)
            ArrayParametros(41) = New SqlParameter("@Rotulado", SqlDbType.Bit)
            ArrayParametros(41).Value = IIf(detalledocumento.Rotulado = Nothing, DBNull.Value, detalledocumento.Rotulado)
            ArrayParametros(42) = New SqlParameter("@Observaciones", SqlDbType.VarChar)
            ArrayParametros(42).Value = IIf(detalledocumento.Observaciones = Nothing, DBNull.Value, detalledocumento.Observaciones)


            cmd.Parameters.Clear()
            cmd.Parameters.AddRange(ArrayParametros)
            cmd.ExecuteNonQuery()

            Return CInt(cmd.Parameters("@IdDetalleDocumento").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectInformeProducto(ByVal idserie As Integer, ByVal codigo As Integer) As Entidades.Documento
        Dim obj As New Entidades.Documento
        Dim lector As SqlDataReader = Nothing
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_selectInforme", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@idserie", idserie)
        cmd.Parameters.AddWithValue("@codigo", codigo)
        'cmd.Parameters.AddWithValue("@IdDocumento", IIf(idDocumento = Nothing, DBNull.Value, idDocumento))
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.HasRows = False Then
                Throw New Exception("No existe documento con Ese código")
            End If
            If lector.Read Then
                With obj

                    .Id = CInt(lector("IdDocumento"))
                    .Codigo = CStr(lector("doc_Codigo"))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")), Nothing, lector("doc_FechaEmision")))
                    .IdEmpresa = CInt(lector("IdEmpresa"))
                    .IdTienda = CInt(lector("Tienda"))
                    .IdSerie = CInt(lector("IdSerie"))
                    .IdAlmacen = CInt(lector("IdAlmacen"))
                    .Pais = CStr(IIf(IsDBNull(lector("Pais")), Nothing, lector("Pais")))
                    .Mes = CStr(IIf(IsDBNull(lector("Mes")), Nothing, lector("Mes")))
                    .IdPersona = CInt(lector("IdPersona"))

                End With
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return obj
    End Function



    Public Function DetalleInformeProducto(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("USP_DETALLE_INFORME", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumentoView
                    With obj
                        .IdInforme = CInt(IIf(IsDBNull(lector.Item("IdInforme")) = True, 0, lector.Item("IdInforme")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("Cantidad")) = True, 0, lector.Item("Cantidad")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("Cod_Prod")), "", lector.Item("Cod_Prod")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("Descripcion")), "", lector.Item("Descripcion")))
                        .nan = CBool(IIf(IsDBNull(lector.Item("Nan")) = True, 0, lector.Item("Nan")))
                        .CAD = CBool(IIf(IsDBNull(lector.Item("CAD")) = True, 0, lector.Item("CAD")))
                        .PesoCaja = CDec(IIf(IsDBNull(lector.Item("PesoCaja")) = True, 0, lector.Item("PesoCaja")))
                        .MCaja = CDec(IIf(IsDBNull(lector.Item("MCaja")) = True, 0, lector.Item("MCaja")))
                        .PzaCaja = CDec(IIf(IsDBNull(lector.Item("PzaCaja")) = True, 0, lector.Item("PzaCaja")))
                        .NroCaja = CDec(IIf(IsDBNull(lector.Item("NroCaja")) = True, 0, lector.Item("NroCaja")))
                        .Pzas = CDec(IIf(IsDBNull(lector.Item("Pzas")) = True, 0, lector.Item("Pzas")))
                        .CalibRe = CBool(IIf(IsDBNull(lector.Item("CalibRe")) = True, 0, lector.Item("CalibRe")))
                        .JGO = CDec(IIf(IsDBNull(lector.Item("JGO")) = True, 0, lector.Item("JGO")))
                        .NPallet = CInt(IIf(IsDBNull(lector.Item("NPallet")) = True, 0, lector.Item("NPallet")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("Total")) = True, 0, lector.Item("Total")))
                        .Sucursal = CStr(IIf(IsDBNull(lector.Item("Sucursal")), "", lector.Item("Sucursal")))
                        .TipoEmpaque = CStr(IIf(IsDBNull(lector.Item("TipoEmpaque")), "", lector.Item("TipoEmpaque")))
                        .EstadoPallet = CStr(IIf(IsDBNull(lector.Item("EstadoPallet")), "", lector.Item("EstadoPallet")))
                        .Rotulado = CBool(IIf(IsDBNull(lector.Item("Rotulado")) = True, 0, lector.Item("Rotulado")))
                        .Observaciones = CStr(IIf(IsDBNull(lector.Item("Observaciones")), "", lector.Item("Observaciones")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function ActualizaInforme(ByVal objInforme As Entidades.DetalleDocumentoView, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try
            Dim cmd As New SqlCommand("USP_ACTUALIZAR_INFORME", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@iddocumento", objInforme.IdDocumento)
            cmd.Parameters.AddWithValue("@iddetalleDocumento", objInforme.IdDetalleDocumento)
            cmd.Parameters.AddWithValue("@nan", objInforme.nan)
            cmd.Parameters.AddWithValue("@CAD", objInforme.CAD)
            cmd.Parameters.AddWithValue("@PesoCaja", objInforme.IdInforme)
            cmd.Parameters.AddWithValue("@MCaja", objInforme.MCaja)
            cmd.Parameters.AddWithValue("@PzaCaja", objInforme.PzaCaja)
            cmd.Parameters.AddWithValue("@NroCaja", objInforme.NroCaja)
            cmd.Parameters.AddWithValue("@Pzas", objInforme.Pzas)
            cmd.Parameters.AddWithValue("@CalibRe", objInforme.CalibRe)
            cmd.Parameters.AddWithValue("@NPallet", objInforme.NPallet)
            cmd.Parameters.AddWithValue("@Total", objInforme.Total)
            cmd.Parameters.AddWithValue("@Sucursal", objInforme.Sucursal)
            cmd.Parameters.AddWithValue("@TipoEmpaque", objInforme.TipoEmpaque)
            cmd.Parameters.AddWithValue("@EstadoPallet", objInforme.EstadoPallet)
            cmd.Parameters.AddWithValue("@Rotulado", objInforme.Rotulado)
            cmd.Parameters.AddWithValue("@Observaciones", objInforme.Observaciones)

            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally
        End Try

    End Function


    Public Function ActualizaInforme(ByVal cn As SqlConnection, ByVal listaDetalleDocumento As Entidades.DetalleDocumentoView, ByVal IdDocumento As Integer, ByVal Tr As SqlTransaction) As Integer
        Try

            Dim cmd As New SqlCommand("USP_ACTUALIZAR_INFORME", cn, Tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            'for cantidad de detalle
            'For i As Integer = 0 To listaDetalleDocumento.Count - 1

            Dim detalledocumento As Entidades.DetalleDocumentoView = listaDetalleDocumento

            Dim ArrayParametros() As SqlParameter = New SqlParameter(19) {}
            '****************
            Dim lista As New List(Of Entidades.Tono)
            lista = listaDetalleDocumento.listaTo

            Dim dt As New DataTable
            dt.Columns.Add("id")
            dt.Columns.Add("cantidad")
            If lista IsNot Nothing Then
                For h As Integer = 0 To lista.Count - 1
                    If (lista.Item(h).cantidadTono > 0) Then
                        Dim dr As DataRow = dt.NewRow
                        dr("id") = lista.Item(h).idTono
                        dr("cantidad") = lista.Item(h).cantidadTono
                        dt.Rows.Add(dr)
                    End If

                Next
            End If
            '***************
            ArrayParametros(0) = New SqlParameter("@iddocumento", SqlDbType.Int)
            ArrayParametros(0).Value = IdDocumento
            ArrayParametros(1) = New SqlParameter("@iddetalleDocumento", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(detalledocumento.IdDetalleDocumento = Nothing, DBNull.Value, detalledocumento.IdDetalleDocumento)
            ArrayParametros(2) = New SqlParameter("@Idproducto", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(detalledocumento.IdProducto = Nothing, DBNull.Value, detalledocumento.IdProducto)
            ArrayParametros(3) = New SqlParameter("@Nan", SqlDbType.Bit)
            ArrayParametros(3).Value = IIf(detalledocumento.nan = Nothing, DBNull.Value, detalledocumento.nan)
            ArrayParametros(4) = New SqlParameter("@CAD", SqlDbType.Bit)
            ArrayParametros(4).Value = IIf(detalledocumento.CAD = Nothing, DBNull.Value, detalledocumento.CAD)
            ArrayParametros(5) = New SqlParameter("@PesoCaja", SqlDbType.Decimal)
            ArrayParametros(5).Value = IIf(detalledocumento.PesoCaja = Nothing, DBNull.Value, detalledocumento.PesoCaja)
            ArrayParametros(6) = New SqlParameter("@MCaja", SqlDbType.Decimal)
            ArrayParametros(6).Value = IIf(detalledocumento.MCaja = Nothing, DBNull.Value, detalledocumento.MCaja)
            ArrayParametros(7) = New SqlParameter("@PzaCaja", SqlDbType.Decimal)
            ArrayParametros(7).Value = IIf(detalledocumento.PzaCaja = Nothing, DBNull.Value, detalledocumento.PzaCaja)
            ArrayParametros(8) = New SqlParameter("@NroCaja", SqlDbType.Decimal)
            ArrayParametros(8).Value = IIf(detalledocumento.NroCaja = Nothing, DBNull.Value, detalledocumento.NroCaja)
            ArrayParametros(9) = New SqlParameter("@Pzas", SqlDbType.Decimal)
            ArrayParametros(9).Value = IIf(detalledocumento.Pzas = Nothing, DBNull.Value, detalledocumento.Pzas)
            ArrayParametros(10) = New SqlParameter("@CalibRe", SqlDbType.Bit)
            ArrayParametros(10).Value = IIf(detalledocumento.CalibRe = Nothing, DBNull.Value, detalledocumento.CalibRe)
            ArrayParametros(11) = New SqlParameter("@NPallet", SqlDbType.Int)
            ArrayParametros(11).Value = IIf(detalledocumento.NPallet = Nothing, DBNull.Value, detalledocumento.NPallet)
            ArrayParametros(12) = New SqlParameter("@Total", SqlDbType.Decimal)
            ArrayParametros(12).Value = IIf(detalledocumento.Total = Nothing, DBNull.Value, detalledocumento.Total)
            ArrayParametros(13) = New SqlParameter("@Sucursal", SqlDbType.VarChar)
            ArrayParametros(13).Value = IIf(detalledocumento.Sucursal = Nothing, DBNull.Value, detalledocumento.Sucursal)
            ArrayParametros(14) = New SqlParameter("@TipoEmpaque", SqlDbType.VarChar)
            ArrayParametros(14).Value = IIf(detalledocumento.TipoEmpaque = Nothing, DBNull.Value, detalledocumento.TipoEmpaque)
            ArrayParametros(15) = New SqlParameter("@EstadoPallet", SqlDbType.VarChar)
            ArrayParametros(15).Value = IIf(detalledocumento.EstadoPallet = Nothing, DBNull.Value, detalledocumento.EstadoPallet)
            ArrayParametros(16) = New SqlParameter("@Rotulado", SqlDbType.Bit)
            ArrayParametros(16).Value = IIf(detalledocumento.Rotulado = Nothing, DBNull.Value, detalledocumento.Rotulado)
            ArrayParametros(17) = New SqlParameter("@Observaciones", SqlDbType.VarChar)
            ArrayParametros(17).Value = IIf(detalledocumento.Observaciones = Nothing, DBNull.Value, detalledocumento.Observaciones)

            ArrayParametros(18) = New SqlParameter("@dt_tonos", SqlDbType.Structured)
            ArrayParametros(18).Value = dt
            ArrayParametros(19) = New SqlParameter("@cantidad", SqlDbType.Decimal)
            ArrayParametros(19).Value = IIf(detalledocumento.Cantidad = Nothing, DBNull.Value, detalledocumento.Cantidad)

            cmd.Parameters.Clear()
            cmd.Parameters.AddRange(ArrayParametros)
            cmd.ExecuteNonQuery()

            Return CInt(cmd.Parameters("@IdDetalleDocumento").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ActualizarInf(ByVal ObjDocumento As Entidades.Documento, _
                                             ByVal listaDetalle As List(Of Entidades.DetalleDocumentoView)) _
                                       As String
        Dim cad As String = String.Empty
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

        Try
           
            'ObjDocumento.Id = CInt(cad.Split(CChar(","))(0))


            ' ****************** DETALLEDOCUMENTO

            ' ****************** DETALLEDOCUMENTO

            For i As Integer = 0 To listaDetalle.Count - 1
                With listaDetalle.Item(i)
                    .IdDocumento = ObjDocumento.Id
                    Dim iddetalle As Integer = Me.ActualizaInforme(cn, listaDetalle(i), ObjDocumento.Id, tr)

                End With
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return ObjDocumento.IdDocumento
    End Function

End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports Entidades
Public Class DAOCotizacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaCotizacion(ByVal cotizacion As Entidades.Cotizacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = cotizacion.IdDocumento
        ArrayParametros(1) = New SqlParameter("@cot_IncluyeTransporte", SqlDbType.Char)
        ArrayParametros(1).Value = cotizacion.IncluyeTransporte
        ArrayParametros(2) = New SqlParameter("@cot_PreciosConIgv", SqlDbType.Char)
        ArrayParametros(2).Value = cotizacion.PreciosConIgv
        ArrayParametros(3) = New SqlParameter("@cot_DiasOferta", SqlDbType.Int)
        ArrayParametros(3).Value = cotizacion.DiasOferta
        Return HDAO.Insert(cn, "_CotizacionInsert", ArrayParametros)
    End Function

    Public Function ActualizaCotizacion(ByVal cotizacion As Entidades.Cotizacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = cotizacion.IdDocumento
        ArrayParametros(1) = New SqlParameter("@cot_IncluyeTransporte", SqlDbType.Char)
        ArrayParametros(1).Value = cotizacion.IncluyeTransporte
        ArrayParametros(2) = New SqlParameter("@cot_PreciosConIgv", SqlDbType.Char)
        ArrayParametros(2).Value = cotizacion.PreciosConIgv
        ArrayParametros(3) = New SqlParameter("@cot_DiasOferta", SqlDbType.Int)
        ArrayParametros(3).Value = cotizacion.DiasOferta
        Return HDAO.Update(cn, "_CotizacionUpdate", ArrayParametros)
    End Function

    Public Function CotizacionesXAprobar(ByVal cn As SqlConnection, ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal idPersona As Integer, _
                                      ByVal fechaInicio As Date, ByVal fechaFin As Date, idAprobarCoti As String) As List(Of Entidades.DocumentoView)
        Dim lista As List(Of DocumentoView) = Nothing
        Using cmd As New SqlCommand("SP_COTIZACION_X_APROBAR", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.VarChar)).Value = idEmpresa
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.VarChar)).Value = idTienda
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.VarChar)).Value = CDate(fechaInicio)
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.VarChar)).Value = CDate(fechaFin)
                .Parameters.Add(New SqlParameter("@Aprobacion", SqlDbType.VarChar)).Value = idAprobarCoti
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of DocumentoView)
                Dim ordinal1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ordinal2 As Integer = rdr.GetOrdinal("doc_Serie")
                Dim ordinal3 As Integer = rdr.GetOrdinal("doc_Codigo")
                Dim ordinal4 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                Dim ordinal5 As Integer = rdr.GetOrdinal("doc_FechaVenc")
                Dim ordinal6 As Integer = rdr.GetOrdinal("tdoc_NombreCorto")
                Dim ordinal7 As Integer = rdr.GetOrdinal("tie_Nombre")
                Dim ordinal8 As Integer = rdr.GetOrdinal("DescripcionPersona")
                Dim ordinal9 As Integer = rdr.GetOrdinal("anex_aprobar")
                Dim objeto As DocumentoView = Nothing

                While rdr.Read()
                    objeto = New DocumentoView
                    With objeto
                        .Id = rdr.GetInt32(ordinal1)
                        .NroDocumento = CStr(rdr.GetString(ordinal2)) & "-" & CStr(rdr.GetString(ordinal3))
                        .FechaEmision = rdr.GetString(ordinal4)
                        .FechaVenc = rdr.GetDateTime(ordinal5)
                        .NomTipoDocumento = rdr.GetString(ordinal6)
                        .Tienda = rdr.GetString(ordinal7)
                        .DescripcionPersona = rdr.GetString(ordinal8)
                        .Aprobar = rdr.GetBoolean(ordinal9)
                    End With
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function aprobarCotizacion(ByVal cn As SqlConnection, idDocumento As Integer)
        Dim retornar As Boolean = False
        Using cmd As New SqlCommand("SP_APROBAR_COTIZACION", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
            End With
            Try
                retornar = cmd.ExecuteNonQuery()
                retornar = True
            Catch ex As Exception
                retornar = False
            End Try
        End Using
        Return retornar
    End Function
End Class

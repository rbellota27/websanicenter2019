﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*********************   LUNES 24052010


Imports System.Data.SqlClient
Public Class DAOMagnitudUnidad
    Dim HDAO As New DAO.HelperDAO
    Private objConexion As New DAO.Conexion


    Public Sub InsertaMagnitudUnidad(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.MagnitudUnidad), ByVal IdMagnitud As Integer)

        Dim fila As New Entidades.MagnitudUnidad

        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@idMagnitud", SqlDbType.Int)
        ArrayParametros(0).Value = IdMagnitud
        For Each fila In lista
            ArrayParametros(1) = New SqlParameter("@idUnidadMedida", SqlDbType.Int)
            ArrayParametros(1).Value = fila.idUnidadMedida
            ArrayParametros(2) = New SqlParameter("@mu_UnidadPrincipal", SqlDbType.Bit)
            ArrayParametros(2).Value = IIf(fila.UnidadPrincipal = Nothing, DBNull.Value, fila.UnidadPrincipal)
            ArrayParametros(3) = New SqlParameter("@Equivalencia", SqlDbType.Decimal)
            ArrayParametros(3).Value = IIf(fila.Equivalencia = Nothing, DBNull.Value, fila.Equivalencia)
            HDAO.InsertaT(cn, "_MagnitudUnidadInsert", ArrayParametros, tr)
        Next
    End Sub
    'Actualizar una GRILLA
    Public Function ActualizaMagnitudUnidad(ByVal UpdateMU As List(Of Entidades.MagnitudUnidad)) As Boolean
        'Public Function ActualizaMagnitudUnidad(ByVal UpdateMU As Entidades.MagnitudUnidad, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim MagnitudUnidad As New Entidades.MagnitudUnidad
        For Each MagnitudUnidad In UpdateMU
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@IdMagnitud", SqlDbType.Int)
            ArrayParametros(0).Value = MagnitudUnidad.idUnidadMedida
            ArrayParametros(1) = New SqlParameter("@IdMagnitudUnidad", SqlDbType.Bit)
            ArrayParametros(1).Value = MagnitudUnidad.UnidadPrincipal
            ArrayParametros(2) = New SqlParameter("@equivalencia", SqlDbType.Decimal)
            ArrayParametros(2).Value = MagnitudUnidad.Equivalencia
            ArrayParametros(3) = New SqlParameter("@UnidadPrincipal", SqlDbType.Int)
            ArrayParametros(3).Value = MagnitudUnidad.idMagnitud
            Return HDAO.Update(cn, "_MagnitudUnidadUpdate", ArrayParametros)
        Next
    End Function

    Public Function DeletexIdMagnitud(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idMagnitud As Integer) As Boolean
        Try

            Dim cmd As New SqlCommand("_MagnitudUnidadselectxBorrar", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idMagnitud", idMagnitud)
            cmd.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try

    End Function

    'Para EDITAR la Magnitud Unidad mediante el IdMagnitud
    Public Function _MagnitudUnidadselectxIdMagnitud(ByVal IdMagnitud As Integer) As List(Of Entidades.MagnitudUnidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudUnidadselectxIdMagnitud", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMagnitud", IdMagnitud)
        Dim lector_mu As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector_mu = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.MagnitudUnidad)
                Do While lector_mu.Read
                    Dim obj As New Entidades.MagnitudUnidad
                    obj.idMagnitud = CInt(lector_mu.Item("IdMagnitud"))
                    obj.idUnidadMedida = CInt(lector_mu.Item("IdUnidadMedida"))
                    obj.Equivalencia = CDec(IIf(IsDBNull(lector_mu.Item("mu_Equivalencia")) = True, 0, lector_mu.Item("mu_Equivalencia")))
                    obj.UnidadPrincipal = CBool(IIf(IsDBNull(lector_mu.Item("mu_UnidadPrincipal")) = True, 0, lector_mu("mu_UnidadPrincipal")))
                    'que es lo que llamo a la grilla DGV_MU?..... Mediante el idUnidadMedida
                    obj.NombreCortoUM = CStr(lector_mu.Item("um_NombreCorto"))
                    lista.Add(obj)
                Loop
                lector_mu.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboUM(ByVal IdMagnitud As Integer) As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudUnidadSelectCboUM", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMagnitud", IdMagnitud)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim obj As New Entidades.UnidadMedida
                    obj.Id = CInt(lector.Item("IdUnidadMedida"))
                    obj.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

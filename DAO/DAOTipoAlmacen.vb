﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** '29Marzo 2010 01_00
Imports System.Data.SqlClient
Public Class DAOTipoAlmacen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader
    Public Function RotacionInventarioAllLineaSubLineaAnio(ByVal Anio As String, ByVal MesInicial As String, ByVal MesFinal As String, ByVal CadenaTipoAlmacen As String, _
                                                           ByVal Linea As String, ByVal Sublinea As String, ByVal Pais As String, ByVal idTienda As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("RotacionInventario2", objConexion.ConexionSIGE)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idtipoalmacen", CadenaTipoAlmacen)
            cmd.Parameters.AddWithValue("@mesinicial", MesInicial)
            cmd.Parameters.AddWithValue("@mesfinal", MesFinal)
            cmd.Parameters.AddWithValue("@anio", Anio)
            cmd.Parameters.AddWithValue("@Linea", IIf(Linea = Nothing, DBNull.Value, Linea))
            cmd.Parameters.AddWithValue("@SubLinea", IIf(Sublinea = Nothing, DBNull.Value, Sublinea))
            cmd.Parameters.AddWithValue("@Pais", IIf(Pais = Nothing, DBNull.Value, Pais))
            cmd.Parameters.AddWithValue("@ID_TIENDA", IIf(idTienda = Nothing, DBNull.Value, idTienda))

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Rotacion")

            '*********** lleno detalle

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function


    Public Function ResumenIndicadoresLineasubLineaAnio(ByVal Anio As String, ByVal MesInicial As String, ByVal MesFinal As String, ByVal CadenaTipoAlmacen As String, ByVal Linea As String, ByVal Sublinea As String, ByVal Pais As String) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("RotacionInventarioResumenInventarioxMesesIRINDI2", objConexion.ConexionSIGE)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idtipoalmacen", CadenaTipoAlmacen)
            cmd.Parameters.AddWithValue("@mesinicial", MesInicial)
            cmd.Parameters.AddWithValue("@mesfinal", MesFinal)
            cmd.Parameters.AddWithValue("@anio", Anio)
            cmd.Parameters.AddWithValue("@Linea", IIf(Linea = Nothing, DBNull.Value, Linea))
            cmd.Parameters.AddWithValue("@SubLinea", IIf(Sublinea = Nothing, DBNull.Value, Sublinea))
            cmd.Parameters.AddWithValue("@Pais", IIf(Pais = Nothing, DBNull.Value, Pais))

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Rotacion")

            '*********** lleno detalle
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function ResumenIndicadores(ByVal objx As Entidades.TipoAlmacen) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("RotacionInventarioResumenInventarioxMesesIRINDI", objConexion.ConexionSIGE)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idtipoalmacen", objx.CadenaTipoAlmacen)
            cmd.Parameters.AddWithValue("@mesinicial", objx.MesInicial)
            cmd.Parameters.AddWithValue("@mesfinal", objx.MesFinal)
            cmd.Parameters.AddWithValue("@anio", objx.Anio)
            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Rotacion")

            '*********** lleno detalle
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function ReporteRotacionInvxAnioxMesLineaSubLinea(ByVal cadenaalmacenado As String, ByVal anio As String, ByVal mes As String, ByVal Linea As String, ByVal SubLinea As String, ByVal Pais As String) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("RotacionInventarioIndicadorIRINDI2", objConexion.ConexionSIGE)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idtipoalmacen", cadenaalmacenado)
            cmd.Parameters.AddWithValue("@mesinicial", mes)
            cmd.Parameters.AddWithValue("@mesfinal", mes)
            cmd.Parameters.AddWithValue("@anio", anio)
            cmd.Parameters.AddWithValue("@Linea", IIf(Linea = Nothing, DBNull.Value, Linea))
            cmd.Parameters.AddWithValue("@SubLinea", IIf(SubLinea = Nothing, DBNull.Value, SubLinea))
            cmd.Parameters.AddWithValue("@Pais", IIf(Pais = Nothing, DBNull.Value, Pais))
            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Rotacion")

            '*********** lleno detalle

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function ReporteRotacionInvxAnioxMes(ByVal cadenaalmacenado As String, ByVal anio As String, ByVal mes As String) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("RotacionInventarioIndicadorIRINDI", objConexion.ConexionSIGE)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idtipoalmacen", cadenaalmacenado)
            cmd.Parameters.AddWithValue("@mesinicial", mes)
            cmd.Parameters.AddWithValue("@mesfinal", mes)
            cmd.Parameters.AddWithValue("@anio", anio)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Rotacion")

            '*********** lleno detalle

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function ResumenIndicadoresGrillaIRILineaSubLineaAnio(ByVal Anio As String, ByVal MesInicial As String, ByVal MesFinal As String, ByVal CadenaTipoAlmacen As String, ByVal Linea As String, ByVal Sublinea As String, ByVal Pais As String) As List(Of Entidades.TipoAlmacen)
        Dim Lista As New List(Of Entidades.TipoAlmacen)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("RotacionInventarioResumenInventarioxMesesIRINDI2", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@anio", Anio)
        cmd.Parameters.AddWithValue("@mesinicial", MesInicial)
        cmd.Parameters.AddWithValue("@mesfinal", MesFinal)
        cmd.Parameters.AddWithValue("@idtipoalmacen", CadenaTipoAlmacen)
        cmd.Parameters.AddWithValue("@Linea", IIf(Linea = Nothing, DBNull.Value, Linea))
        cmd.Parameters.AddWithValue("@SubLinea", IIf(Sublinea = Nothing, DBNull.Value, Sublinea))
        cmd.Parameters.AddWithValue("@Pais", IIf(Pais = Nothing, DBNull.Value, Pais))


        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim almacen As New Entidades.TipoAlmacen
                almacen.Tienda = CStr(IIf(IsDBNull(lector("Tienda")), 0, lector("Tienda")))
                almacen.Enero = CDec(IIf(IsDBNull(lector("Enero")), 0, lector("Enero")))
                almacen.Febrero = CDec(IIf(IsDBNull(lector("Febrero")), 0, lector("Febrero")))
                almacen.Marzo = CDec(IIf(IsDBNull(lector("Marzo")), 0, lector("Marzo")))
                almacen.Abril = CDec(IIf(IsDBNull(lector("Abril")), 0, lector("Abril")))
                almacen.Mayo = CDec(IIf(IsDBNull(lector("Mayo")), 0, lector("Mayo")))
                almacen.Junio = CDec(IIf(IsDBNull(lector("Junio")), 0, lector("Junio")))
                almacen.Julio = CDec(IIf(IsDBNull(lector("Julio")), 0, lector("Julio")))
                almacen.Agosto = CDec(IIf(IsDBNull(lector("Agosto")), 0, lector("Agosto")))
                almacen.Septiembre = CDec(IIf(IsDBNull(lector("Septiembre")), 0, lector("Septiembre")))
                almacen.Octubre = CDec(IIf(IsDBNull(lector("Octubre")), 0, lector("Octubre")))
                almacen.Noviembre = CDec(IIf(IsDBNull(lector("Noviembre")), 0, lector("Noviembre")))
                almacen.Diciembre = CDec(IIf(IsDBNull(lector("Diciembre")), 0, lector("Diciembre")))
                almacen.Iri = CDec(IIf(IsDBNull(lector("IRI")), 0, lector("IRI")))
                almacen.Ndi = CDec(IIf(IsDBNull(lector("NDi")), 0, lector("NDi")))
                almacen.TotalCompras = CDec(IIf(IsDBNull(lector("totalcomprasgeneral")), 0, lector("totalcomprasgeneral")))
                almacen.StockInicial = CDec(IIf(IsDBNull(lector("stockInicialgeneral")), 0, lector("stockInicialgeneral")))
                almacen.StockFinal = CDec(IIf(IsDBNull(lector("stockfinalgeneral")), 0, lector("stockfinalgeneral")))
                almacen.Numdias = CDec(IIf(IsDBNull(lector("nrodias")), 0, lector("nrodias")))
                Lista.Add(almacen)
            Loop
            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function


    Public Function ResumenIndicadoresGrillaIRI(ByVal obj As Entidades.TipoAlmacen) As List(Of Entidades.TipoAlmacen)
        Dim Lista As New List(Of Entidades.TipoAlmacen)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("RotacionInventarioResumenInventarioxMesesIRINDI", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@anio", obj.Anio)
        cmd.Parameters.AddWithValue("@mesinicial", obj.MesInicial)
        cmd.Parameters.AddWithValue("@mesfinal", obj.MesFinal)
        cmd.Parameters.AddWithValue("@idtipoalmacen", obj.CadenaTipoAlmacen)

        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim almacen As New Entidades.TipoAlmacen
                almacen.Tienda = CStr(IIf(IsDBNull(lector("Tienda")), 0, lector("Tienda")))
                almacen.Enero = CDec(IIf(IsDBNull(lector("Enero")), 0, lector("Enero")))
                almacen.Febrero = CDec(IIf(IsDBNull(lector("Febrero")), 0, lector("Febrero")))
                almacen.Marzo = CDec(IIf(IsDBNull(lector("Marzo")), 0, lector("Marzo")))
                almacen.Abril = CDec(IIf(IsDBNull(lector("Abril")), 0, lector("Abril")))
                almacen.Mayo = CDec(IIf(IsDBNull(lector("Mayo")), 0, lector("Mayo")))
                almacen.Junio = CDec(IIf(IsDBNull(lector("Junio")), 0, lector("Junio")))
                almacen.Julio = CDec(IIf(IsDBNull(lector("Julio")), 0, lector("Julio")))
                almacen.Agosto = CDec(IIf(IsDBNull(lector("Agosto")), 0, lector("Agosto")))
                almacen.Septiembre = CDec(IIf(IsDBNull(lector("Septiembre")), 0, lector("Septiembre")))
                almacen.Octubre = CDec(IIf(IsDBNull(lector("Octubre")), 0, lector("Octubre")))
                almacen.Noviembre = CDec(IIf(IsDBNull(lector("Noviembre")), 0, lector("Noviembre")))
                almacen.Diciembre = CDec(IIf(IsDBNull(lector("Diciembre")), 0, lector("Diciembre")))
                almacen.Iri = CDec(IIf(IsDBNull(lector("IRI")), 0, lector("IRI")))
                almacen.Ndi = CDec(IIf(IsDBNull(lector("NDi")), 0, lector("NDi")))
                almacen.TotalCompras = CDec(IIf(IsDBNull(lector("totalcomprasgeneral")), 0, lector("totalcomprasgeneral")))
                almacen.StockInicial = CDec(IIf(IsDBNull(lector("stockInicialgeneral")), 0, lector("stockInicialgeneral")))
                almacen.StockFinal = CDec(IIf(IsDBNull(lector("stockfinalgeneral")), 0, lector("stockfinalgeneral")))
                almacen.Numdias = CDec(IIf(IsDBNull(lector("nrodias")), 0, lector("nrodias")))
                Lista.Add(almacen)
            Loop
            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function SelectxTipoAlmacenxanioxmesLineaSubLinea(ByVal cadenaalmacenado As String, ByVal anio As String, ByVal mes As String, ByVal Linea As String, ByVal SubLinea As String, ByVal Pais As String) As List(Of Entidades.TipoAlmacen)
        Dim Lista As New List(Of Entidades.TipoAlmacen)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("RotacionInventarioIndicadorIRINDI2", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@anio", anio)
        cmd.Parameters.AddWithValue("@mesinicial", mes)
        cmd.Parameters.AddWithValue("@mesfinal", mes)
        cmd.Parameters.AddWithValue("@idtipoalmacen", cadenaalmacenado)
        cmd.Parameters.AddWithValue("@Linea", IIf(Linea = Nothing, DBNull.Value, Linea))
        cmd.Parameters.AddWithValue("@SubLinea", IIf(SubLinea = Nothing, DBNull.Value, SubLinea))
        cmd.Parameters.AddWithValue("@Pais", IIf(Pais = Nothing, DBNull.Value, Pais))

        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim almacen As New Entidades.TipoAlmacen
                almacen.Mes = CStr(IIf(IsDBNull(lector("mes")), "", lector("mes")))
                almacen.Tienda = CStr(IIf(IsDBNull(lector("tienda")), "", lector("tienda")))
                almacen.StockInicial = CDec(IIf(IsDBNull(lector("stockinicial")), 0, lector("stockinicial")))
                almacen.StockFinal = CDec(IIf(IsDBNull(lector("stockfinal")), 0, lector("stockfinal")))
                almacen.Compras = CDec(IIf(IsDBNull(lector("compras")), 0, lector("compras")))
                almacen.Transferencias = CDec(IIf(IsDBNull(lector("transferencias")), 0, lector("transferencias")))
                almacen.Transito = CDec(IIf(IsDBNull(lector("transito")), 0, lector("transito")))
                almacen.TotalCompras = CDec(IIf(IsDBNull(lector("totalcompras")), 0, lector("totalcompras")))
                almacen.Iri = CDec(IIf(IsDBNull(lector("IRI")), 0, lector("IRI")))
                almacen.Ndi = CDec(IIf(IsDBNull(lector("NDI")), 0, lector("NDI")))
                almacen.Escala = CStr(IIf(IsDBNull(lector("Escala")), 0, lector("Escala")))
                almacen.ObjetivoxTienda = CInt(IIf(IsDBNull(lector("num_objetivo")), 0, lector("num_objetivo")))
                Lista.Add(almacen)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function SelectxTipoAlmacenxanioxmes(ByVal cadenaalmacenado As String, ByVal anio As String, ByVal mes As String) As List(Of Entidades.TipoAlmacen)
        Dim Lista As New List(Of Entidades.TipoAlmacen)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("RotacionInventarioIndicadorIRINDI", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@anio", anio)
        cmd.Parameters.AddWithValue("@mesinicial", mes)
        cmd.Parameters.AddWithValue("@mesfinal", mes)
        cmd.Parameters.AddWithValue("@idtipoalmacen", cadenaalmacenado)

        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim almacen As New Entidades.TipoAlmacen
                almacen.Mes = CStr(IIf(IsDBNull(lector("mes")), "", lector("mes")))
                almacen.Tienda = CStr(IIf(IsDBNull(lector("tienda")), "", lector("tienda")))
                almacen.StockInicial = CDec(IIf(IsDBNull(lector("stockinicial")), 0, lector("stockinicial")))
                almacen.StockFinal = CDec(IIf(IsDBNull(lector("stockfinal")), 0, lector("stockfinal")))
                almacen.Compras = CDec(IIf(IsDBNull(lector("compras")), 0, lector("compras")))
                almacen.Transferencias = CDec(IIf(IsDBNull(lector("transferencias")), 0, lector("transferencias")))
                almacen.Transito = CDec(IIf(IsDBNull(lector("transito")), 0, lector("transito")))
                almacen.TotalCompras = CDec(IIf(IsDBNull(lector("totalcompras")), 0, lector("totalcompras")))
                almacen.Iri = CDec(IIf(IsDBNull(lector("IRI")), 0, lector("IRI")))
                almacen.Ndi = CDec(IIf(IsDBNull(lector("NDI")), 0, lector("NDI")))
                almacen.Escala = CStr(IIf(IsDBNull(lector("Escala")), 0, lector("Escala")))
                almacen.ObjetivoxTienda = CInt(IIf(IsDBNull(lector("num_objetivo")), 0, lector("num_objetivo")))
                Lista.Add(almacen)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function
    Public Function SelectxIdTipoAlmacen() As List(Of Entidades.TipoAlmacen)
        Dim Lista As New List(Of Entidades.TipoAlmacen)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_SelectxIdTipoAlmacen", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim almacen As New Entidades.TipoAlmacen
                almacen.IdTipoAlmacen = CInt(IIf(IsDBNull(lector("IdTipoAlmacen")), 0, lector("IdTipoAlmacen")))
                almacen.Tipoalm_Nombre = CStr(IIf(IsDBNull(lector("Tipoalm_Nombre")), "", lector("Tipoalm_Nombre")))

                Lista.Add(almacen)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function
    Public Function SelectxIdTipoAlmacenAll(ByVal IdtipoAlmacen As Integer) As List(Of Entidades.TipoAlmacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectxIdTipoAlmacenAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@idtipoalmacen", IdtipoAlmacen)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoAlmacen)
                Do While lector.Read
                    Dim obj As New Entidades.TipoAlmacen
                    obj.IdTipoAlmacen = CInt(IIf(IsDBNull(lector("IdTipoAlmacen")), 0, lector("IdTipoAlmacen")))
                    obj.Tipoalm_Nombre = CStr(IIf(IsDBNull(lector("Tipoalm_Nombre")), "", lector("Tipoalm_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                cn.Close()

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function RotacioninventarioAll(ByVal objx As Entidades.TipoAlmacen) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("RotacionInventario", objConexion.ConexionSIGE)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idtipoalmacen", objx.CadenaTipoAlmacen)
            cmd.Parameters.AddWithValue("@mesinicial", objx.MesInicial)
            cmd.Parameters.AddWithValue("@mesfinal", objx.MesFinal)
            cmd.Parameters.AddWithValue("@anio", objx.Anio)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Rotacion")

            '*********** lleno detalle

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoAlmacen)
        Dim Lista As New List(Of Entidades.TipoAlmacen)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_SelectTipoAlmacen", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim almacen As New Entidades.TipoAlmacen
                almacen.IdTipoAlmacen = CInt(IIf(IsDBNull(lector("IdTipoAlmacen")), 0, lector("IdTipoAlmacen")))
                almacen.Tipoalm_Nombre = CStr(IIf(IsDBNull(lector("Tipoalm_Nombre")), "", lector("Tipoalm_Nombre")))
                almacen.Tipoalm_NombreCorto = CStr(IIf(IsDBNull(lector("Tipoalm_NombreCorto")), "", lector("Tipoalm_NombreCorto")))
                almacen.Tipoalm_Principal = CStr(IIf(IsDBNull(lector("Tipoalm_Principal")), "", lector("Tipoalm_Principal")))
                almacen.Tipoalm_Estado = CShort(IIf(IsDBNull(lector("Tipoalm_Estado")), "", lector("Tipoalm_Estado")))

                Lista.Add(almacen)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function SelectAllActivoInactivo() As List(Of Entidades.TipoAlmacen)
        Dim Lista As New List(Of Entidades.TipoAlmacen)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("TipoAlmacenSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim almacen As New Entidades.TipoAlmacen
                almacen.IdTipoAlmacen = CInt(IIf(IsDBNull(lector("IdTipoAlmacen")), 0, lector("IdTipoAlmacen")))
                almacen.Tipoalm_Nombre = CStr(IIf(IsDBNull(lector("Tipoalm_Nombre")), "", lector("Tipoalm_Nombre")))
                almacen.Tipoalm_NombreCorto = CStr(IIf(IsDBNull(lector("Tipoalm_NombreCorto")), "", lector("Tipoalm_NombreCorto")))
                almacen.Tipoalm_Principal = CStr(IIf(IsDBNull(lector("Tipoalm_Principal")), "", lector("Tipoalm_Principal")))
                almacen.Tipoalm_Estado = CShort(IIf(IsDBNull(lector("Tipoalm_Estado")), 0, lector("Tipoalm_Estado")))

                Lista.Add(almacen)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function InsertaTipoAlmacen(ByVal talmacen As Entidades.TipoAlmacen) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@Tipoalm_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(talmacen.Tipoalm_Nombre = Nothing, DBNull.Value, talmacen.Tipoalm_Nombre)
        ArrayParametros(1) = New SqlParameter("@Tipoalm_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(talmacen.Tipoalm_NombreCorto = Nothing, DBNull.Value, talmacen.Tipoalm_NombreCorto)
        ArrayParametros(2) = New SqlParameter("@Tipoalm_Principal", SqlDbType.Bit)
        ArrayParametros(2).Value = IIf(talmacen.Tipoalm_Principal = Nothing, "0", talmacen.Tipoalm_Principal)
        ArrayParametros(3) = New SqlParameter("@Tipoalm_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = IIf(talmacen.Tipoalm_Estado = Nothing, 0, talmacen.Tipoalm_Estado)

        Return HDAO.Insert(cn, "_TipoAlmacenInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoAlmacen(ByVal talmacen As Entidades.TipoAlmacen) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        Try
            ArrayParametros(0) = New SqlParameter("@idtipoalmacen", SqlDbType.Int)
            ArrayParametros(0).Value = talmacen.IdTipoAlmacen
            ArrayParametros(1) = New SqlParameter("@Tipoalm_Nombre", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(talmacen.Tipoalm_Nombre = Nothing, DBNull.Value, talmacen.Tipoalm_Nombre)
            ArrayParametros(2) = New SqlParameter("@Tipoalm_NombreCorto", SqlDbType.VarChar)
            ArrayParametros(2).Value = IIf(talmacen.Tipoalm_NombreCorto = Nothing, DBNull.Value, talmacen.Tipoalm_NombreCorto)
            ArrayParametros(3) = New SqlParameter("@Tipoalm_Principal", SqlDbType.Bit)
            ArrayParametros(3).Value = IIf(talmacen.Tipoalm_Principal = Nothing, DBNull.Value, talmacen.Tipoalm_Principal)
            ArrayParametros(4) = New SqlParameter("@Tipoalm_Estado", SqlDbType.Char)
            ArrayParametros(4).Value = IIf(talmacen.Tipoalm_Estado = Nothing, DBNull.Value, talmacen.Tipoalm_Estado)

            Return HDAO.Update(cn, "TipoAlmacenUpdate", ArrayParametros)

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

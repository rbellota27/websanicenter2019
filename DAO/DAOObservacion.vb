'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOObservacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub Registrar(ByVal objObservacion As Entidades.Observacion, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = objDaoMantenedor.getParam(objObservacion.Observacion, "@ob_Observacion", SqlDbType.VarChar)
        p(1) = objDaoMantenedor.getParam(objObservacion.IdDocumento, "@IdDocumento", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_Observacion_Registrar", p)

    End Sub


    Public Sub Insert_Update(ByVal objObservacion As Entidades.Observacion, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(2) {}
        p(0) = objDaoMantenedor.getParam(objObservacion.IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objObservacion.Id, "@IdObservacion", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objObservacion.Observacion, "@ob_Observacion", SqlDbType.VarChar)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_Observacion_Insert_Update", p)

    End Sub







    Public Function InsertaObservacionT(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal observacion As Entidades.Observacion) As Boolean
        Dim cmd As New SqlCommand("_ObservacionInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@ob_Observacion", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(observacion.Observacion = Nothing, DBNull.Value, observacion.Observacion)
        ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(observacion.IdDocumento = Nothing, DBNull.Value, observacion.IdDocumento)

        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function

    Public Function ActualizaObservacionT(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal observacion As Entidades.Observacion) As Boolean
        Dim cmd As New SqlCommand("_ObservacionUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure

        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ob_Observacion", SqlDbType.VarChar)
        ArrayParametros(0).Value = observacion.Observacion
        ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(1).Value = observacion.IdDocumento
        ArrayParametros(2) = New SqlParameter("@IdObservacion", SqlDbType.Int)
        ArrayParametros(2).Value = observacion.Id

        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function

    Public Function ActualizaObservacion(ByVal observacion As Entidades.Observacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ob_Observacion", SqlDbType.VarChar)
        ArrayParametros(0).Value = observacion.Observacion
        ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(1).Value = observacion.IdDocumento
        ArrayParametros(2) = New SqlParameter("@IdObservacion", SqlDbType.Int)
        ArrayParametros(2).Value = observacion.Id
        Return HDAO.Update(cn, "_ObservacionUpdate", ArrayParametros)
    End Function

    Public Function listarObservacionDocumento(ByVal iddocumento As Integer) As Entidades.Observacion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarObservacionDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@iddocumento", iddocumento)
        Dim lector As SqlDataReader
        Dim listaObs As New Entidades.Observacion
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.Read Then
                    With listaObs
                        .Id = CInt(lector.Item("IdObservacion"))
                        .Observacion = CStr(IIf(IsDBNull(lector.Item("ob_Observacion")), "", lector.Item("ob_Observacion")))
                    End With
                End If
                lector.Close()
            End Using
            Return listaObs
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Sub DeletexIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_ObservacionesDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.ExecuteNonQuery()


    End Sub

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Observacion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ObservacionesSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objObservacion As Entidades.Observacion = Nothing
                If lector.Read Then

                    objObservacion = New Entidades.Observacion

                    With objObservacion

                        .Id = CInt(lector("IdObservacion"))
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .Observacion = CStr(IIf(IsDBNull(lector("ob_Observacion")) = True, "", lector("ob_Observacion")))

                    End With

                End If
                lector.Close()
                Return objObservacion
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class

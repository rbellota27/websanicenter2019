'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTipoCorreo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTipoCorreo(ByVal tipocorreo As Entidades.TipoCorreo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@tcorr_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipocorreo.Nombre
        ArrayParametros(1) = New SqlParameter("@tcorr_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipocorreo.Estado
        Return HDAO.Insert(cn, "_TipoCorreoInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoCorreo(ByVal tipocorreo As Entidades.TipoCorreo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tcorr_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipocorreo.Nombre
        ArrayParametros(1) = New SqlParameter("@tcorr_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipocorreo.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoCorreo", SqlDbType.Int)
        ArrayParametros(2).Value = tipocorreo.Id
        Return HDAO.Update(cn, "_TipoCorreoUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoCorreo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCorreoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoCorreo)
                Do While lector.Read
                    Dim obj As New Entidades.TipoCorreo
                    obj.Estado = CStr(lector.Item("tcorr_Estado"))
                    obj.Id = CInt(lector.Item("IdTipoCorreo"))
                    obj.Nombre = CStr(lector.Item("tcorr_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoCorreo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCorreoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoCorreo)
                Do While lector.Read
                    Dim obj As New Entidades.TipoCorreo
                    obj.Estado = CStr(lector.Item("tcorr_Estado"))
                    obj.Id = CInt(lector.Item("IdTipoCorreo"))
                    obj.Nombre = CStr(lector.Item("tcorr_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoCorreo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCorreoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoCorreo)
                Do While lector.Read
                    Dim obj As New Entidades.TipoCorreo
                    obj.Estado = CStr(lector.Item("tcorr_Estado"))
                    obj.Id = CInt(lector.Item("IdTipoCorreo"))
                    obj.Nombre = CStr(lector.Item("tcorr_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoCorreo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCorreoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoCorreo)
                Do While lector.Read
                    Dim obj As New Entidades.TipoCorreo
                    obj.Estado = CStr(lector.Item("tcorr_Estado"))
                    obj.Id = CInt(lector.Item("IdTipoCorreo"))
                    obj.Nombre = CStr(lector.Item("tcorr_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoCorreo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCorreoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoCorreo)
                Do While lector.Read
                    Dim obj As New Entidades.TipoCorreo
                    obj.Estado = CStr(lector.Item("tcorr_Estado"))
                    obj.Id = CInt(lector.Item("IdTipoCorreo"))
                    obj.Nombre = CStr(lector.Item("tcorr_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoCorreo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCorreoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoCorreo)
                Do While lector.Read
                    Dim obj As New Entidades.TipoCorreo
                    obj.Estado = CStr(lector.Item("tcorr_Estado"))
                    obj.Id = CInt(lector.Item("IdTipoCorreo"))
                    obj.Nombre = CStr(lector.Item("tcorr_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoCorreo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCorreoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoCorreo)
                Do While lector.Read
                    Dim obj As New Entidades.TipoCorreo
                    obj.Estado = CStr(lector.Item("tcorr_Estado"))
                    obj.Id = CInt(lector.Item("IdTipoCorreo"))
                    obj.Nombre = CStr(lector.Item("tcorr_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoCorreo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoCorreoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoCorreo)
                Do While lector.Read
                    Dim TipoCorreo As New Entidades.TipoCorreo
                    TipoCorreo.Id = CInt(lector.Item("IdTipoCorreo"))
                    TipoCorreo.Nombre = CStr(lector.Item("tcorr_Nombre"))
                    Lista.Add(TipoCorreo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

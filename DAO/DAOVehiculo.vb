'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 29-Octubre-2009
'Hora    : 06:00 am
'*************************************************
'*************************************************
Imports System.Data.SqlClient

Public Class DAOVehiculo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function SelectxIdVehiculo(ByVal IdVehiculo As Integer) As Entidades.Vehiculo
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_VehiculoSelectxIdVehiculo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdVehiculo", IdVehiculo)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objVehiculo As Entidades.Vehiculo = Nothing
                If lector.Read Then
                    objVehiculo = New Entidades.Vehiculo
                    With objVehiculo
                        .ConfVeh = CStr(IIf(IsDBNull(lector.Item("veh_ConfVeh")) = True, "", lector.Item("veh_ConfVeh")))
                        .ConstanciaIns = CStr(IIf(IsDBNull(lector.Item("veh_NConstanciaIns")) = True, "", lector.Item("veh_NConstanciaIns")))
                        .IdProducto = CInt(lector.Item("IdProducto"))
                        .Modelo = CStr(IIf(IsDBNull(lector.Item("veh_Modelo")) = True, "", lector.Item("veh_Modelo")))
                        .Placa = CStr(IIf(IsDBNull(lector.Item("veh_Placa")) = True, "", lector.Item("veh_Placa")))
                        .Tara = CDec(IIf(IsDBNull(lector.Item("veh_Tara")) = True, 0, lector.Item("veh_Tara")))
                        .veh_Capacidad = CDec(IIf(IsDBNull(lector.Item("capacidad")) = True, 0, lector.Item("capacidad")))
                    End With
                End If
                lector.Close()
                Return objVehiculo
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Sub InsertaVehiculo(ByVal vehiculo As Entidades.Vehiculo, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@veh_Placa", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(vehiculo.Placa = Nothing, DBNull.Value, vehiculo.Placa)
        ArrayParametros(1) = New SqlParameter("@veh_Modelo", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(vehiculo.Modelo = Nothing, DBNull.Value, vehiculo.Modelo)
        ArrayParametros(2) = New SqlParameter("@veh_NConstanciaIns", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(vehiculo.ConstanciaIns = Nothing, DBNull.Value, vehiculo.ConstanciaIns)
        ArrayParametros(3) = New SqlParameter("@veh_ConfVeh", SqlDbType.VarChar)
        ArrayParametros(3).Value = IIf(vehiculo.ConfVeh = Nothing, DBNull.Value, vehiculo.ConfVeh)
        ArrayParametros(4) = New SqlParameter("@veh_Tara", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(vehiculo.Tara = Nothing, DBNull.Value, vehiculo.Tara)
        ArrayParametros(5) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(5).Value = IIf(vehiculo.IdProducto = Nothing, DBNull.Value, vehiculo.IdProducto)
        HDAO.InsertaT(cn, "_VehiculoInsert", ArrayParametros, tr)
    End Sub

    Public Function ActualizaVehiculo(ByVal vehiculo As Entidades.Vehiculo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@veh_Placa", SqlDbType.VarChar)
        ArrayParametros(0).Value = vehiculo.Placa
        ArrayParametros(1) = New SqlParameter("@veh_Modelo", SqlDbType.VarChar)
        ArrayParametros(1).Value = vehiculo.Modelo
        ArrayParametros(2) = New SqlParameter("@veh_NConstanciaIns", SqlDbType.VarChar)
        ArrayParametros(2).Value = vehiculo.ConstanciaIns
        ArrayParametros(3) = New SqlParameter("@veh_ConfVeh", SqlDbType.VarChar)
        ArrayParametros(3).Value = vehiculo.ConfVeh
        ArrayParametros(4) = New SqlParameter("@veh_Tara", SqlDbType.Decimal)
        'El campo TARA es decimal y no se actualizara si no se ingresan datos para esto se realiza....
        ArrayParametros(4).Value = CDec(IIf(vehiculo.Tara = Nothing, DBNull.Value, vehiculo.Tara))
        ArrayParametros(5) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(5).Value = vehiculo.IdProducto
        Return HDAO.Update(cn, "_VehiculoUpdate", ArrayParametros)
    End Function

    Public Function SeleccionarTransportistas() As DataTable
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SP_TRANSPORTISTAS", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt)
        Return dt
    End Function
    Public Function SeleccionarTransporte(ByVal cn As SqlConnection) As List(Of Entidades.be_transportista)
        Using cmd As New SqlCommand("SP_TRANSPORTISTAS", cn)
            Dim lista As List(Of Entidades.be_transportista) = Nothing
            cmd.CommandType = CommandType.StoredProcedure
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_transportista)
                Dim idPersona As Integer = rdr.GetOrdinal("IdPersona")
                Dim transportista As Integer = rdr.GetOrdinal("Transportista")
                Dim doc_identidad As Integer = rdr.GetOrdinal("Doc_Identidad")
                Dim telefono As Integer = rdr.GetOrdinal("tel_Numero")
                Dim distrito As Integer = rdr.GetOrdinal("distrito")
                Dim objTransportista As Entidades.be_transportista
                While rdr.Read()
                    objTransportista = New Entidades.be_transportista
                    With objTransportista
                        .idPersona = rdr.GetInt32(idPersona)
                        .transportista = rdr.GetString(transportista)
                        .doc_identidad = rdr.GetString(doc_identidad)
                        .tel_numero = rdr.GetString(telefono)
                        .distrito = rdr.GetString(distrito)
                    End With
                    lista.Add(objTransportista)
                End While
                rdr.Close()
            End If
            Return lista
        End Using
    End Function

    Public Function SeleccionarTransportistas_prueba(ByVal inicial As Integer, ByVal nroPaginas As Integer) As DataSet        
        inicial = inicial + nroPaginas
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SP_TRANSPORTISTAS", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim dt As New DataSet
        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt, "transportistas")        
        Return dt
    End Function

    Public Function SelectActivoxNroPlaca(ByVal placa As String) As List(Of Entidades.Vehiculo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_VehiculoSelectActivoxNroPlaca", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Placa", placa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Vehiculo)
                Do While lector.Read
                    Dim obj As New Entidades.Vehiculo
                    With obj
                        .ConfVeh = CStr(IIf(IsDBNull(lector.Item("veh_ConfVeh")) = True, "", lector.Item("veh_ConfVeh")))
                        .ConstanciaIns = CStr(IIf(IsDBNull(lector.Item("veh_NConstanciaIns")) = True, "", lector.Item("veh_NConstanciaIns")))
                        .IdProducto = CInt(lector.Item("IdProducto"))
                        .Modelo = CStr(IIf(IsDBNull(lector.Item("veh_Modelo")) = True, "", lector.Item("veh_Modelo")))
                        .Placa = CStr(IIf(IsDBNull(lector.Item("veh_Placa")) = True, "", lector.Item("veh_Placa")))
                        .Tara = CDec(IIf(IsDBNull(lector.Item("veh_Tara")) = True, 0, lector.Item("veh_Tara")))
                        .veh_Capacidad = CDec(IIf(IsDBNull(lector.Item("capacidad")) = True, 0, lector.Item("capacidad")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllVehiculos() As List(Of Entidades.Vehiculo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_VehiculoLista", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Vehiculo)
                Do While lector.Read
                    Dim VehiculoSelect As New Entidades.Vehiculo
                    VehiculoSelect.Placa = CStr(IIf(IsDBNull(lector.Item("veh_Placa")) = True, "", lector.Item("veh_Placa")))
                    VehiculoSelect.Modelo = CStr(IIf(IsDBNull(lector.Item("veh_Modelo")) = True, "", lector.Item("veh_Modelo")))
                    VehiculoSelect.ConstanciaIns = CStr(IIf(IsDBNull(lector.Item("veh_NConstanciaIns")) = True, "", lector.Item("veh_NConstanciaIns")))
                    VehiculoSelect.ConfVeh = CStr(IIf(IsDBNull(lector.Item("veh_ConfVeh")) = True, "", lector.Item("veh_ConfVeh")))
                    VehiculoSelect.Tara = CDec(IIf(IsDBNull(lector.Item("veh_Tara")) = True, 0, lector.Item("veh_Tara")))
                    VehiculoSelect.NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    VehiculoSelect.IdProducto = CInt(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    Lista.Add(VehiculoSelect)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelecFiltradoGrilla(ByVal placa As String) As List(Of Entidades.Vehiculo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_VehiculoFiltradoGrilla", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Placa", placa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Vehiculo)
                Do While lector.Read
                    Dim obj As New Entidades.Vehiculo
                    With obj
                        .ConfVeh = CStr(IIf(IsDBNull(lector.Item("veh_ConfVeh")) = True, "", lector.Item("veh_ConfVeh")))
                        .ConstanciaIns = CStr(IIf(IsDBNull(lector.Item("veh_NConstanciaIns")) = True, "", lector.Item("veh_NConstanciaIns")))
                        .IdProducto = CInt(lector.Item("IdProducto"))
                        .Modelo = CStr(IIf(IsDBNull(lector.Item("veh_Modelo")) = True, "", lector.Item("veh_Modelo")))
                        .Placa = CStr(IIf(IsDBNull(lector.Item("veh_Placa")) = True, "", lector.Item("veh_Placa")))
                        .Tara = CDec(IIf(IsDBNull(lector.Item("veh_Tara")) = True, 0, lector.Item("veh_Tara")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function InsertaEstadoVehiculo(ByVal EstadoVehiculo As Entidades.Vehiculo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}

        ArrayParametros(0) = New SqlParameter("@veh_Placa", SqlDbType.VarChar)
        ArrayParametros(0).Value = EstadoVehiculo.Placa

        ArrayParametros(1) = New SqlParameter("@veh_Modelo", SqlDbType.VarChar)
        ArrayParametros(1).Value = EstadoVehiculo.Modelo

        ArrayParametros(2) = New SqlParameter("@veh_NConstanciaIns", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(EstadoVehiculo.ConstanciaIns = Nothing, DBNull.Value, EstadoVehiculo.ConstanciaIns)

        ArrayParametros(3) = New SqlParameter("@veh_ConfVeh", SqlDbType.VarChar)
        ArrayParametros(3).Value = EstadoVehiculo.ConfVeh

        ArrayParametros(4) = New SqlParameter("@veh_Tara", SqlDbType.Decimal)
        ArrayParametros(4).Value = EstadoVehiculo.Tara

        ArrayParametros(5) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(5).Value = EstadoVehiculo.IdProducto

        Return HDAO.Insert(cn, "_VehiculoInsert", ArrayParametros)

    End Function

    Public Function SelectCboEstadoVehiculo() As List(Of Entidades.EstadoComboVehiculo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoComboVehiculo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoComboVehiculo)
                Do While lector.Read
                    Dim VehiculoSelect As New Entidades.EstadoComboVehiculo
                    VehiculoSelect.IdCboVehiculo = CInt(lector.Item("IdEstadoProd"))
                    VehiculoSelect.NombreEstadoVahiculo = CStr(IIf(IsDBNull(lector.Item("ep_Nombre")) = True, "", lector.Item("ep_Nombre")))
                    Lista.Add(VehiculoSelect)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaIdProductoVehiculo(ByVal EstVehiculo As Entidades.Vehiculo) As Boolean
        'Inserta IdProducto de vehiculo
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@estado", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(EstVehiculo.EstadosVehiculo = Nothing, DBNull.Value, EstVehiculo.EstadosVehiculo)
        ArrayParametros(1) = New SqlParameter("@IdProd", SqlDbType.Int)
        ArrayParametros(1).Direction = ParameterDirection.Output
        ArrayParametros(1).Value = 0
        'Return HDAO.Insert(cn, "_ProductoVehiculoEstado", ArrayParametros)
        'Return HDAO.Insert_ReturnParameter(cn, "_ProductoVehiculoEstado", ArrayParametros)
        Return CBool(HDAO.Insert_ReturnParameter(cn, "_ProductoVehiculoEstado", ArrayParametros))
    End Function
    Public Function SelectAllVehiculosXtipoexistenciaXlineaXsublinea() As List(Of Entidades.Vehiculo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_VehiculoxTipoExistenciaxLineaxSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.SingleResult)
                Dim Lista As List(Of Entidades.Vehiculo) = Nothing
                If lector IsNot Nothing Then
                    Lista = New List(Of Entidades.Vehiculo)
                    Dim idTipoExistencia As Integer = lector.GetOrdinal("IdTipoExistencia")
                    Dim Placa As Integer = lector.GetOrdinal("veh_Placa")
                    Dim Modelo As Integer = lector.GetOrdinal("veh_Modelo")
                    Dim ConstanciaIns As Integer = lector.GetOrdinal("veh_NConstanciaIns")
                    Dim ConfVeh As Integer = lector.GetOrdinal("veh_ConfVeh")
                    Dim Tara As Integer = lector.GetOrdinal("veh_Tara")
                    Dim nombreTipoExistencia As Integer = lector.GetOrdinal("tex_Nombre")
                    Dim NomLinea As Integer = lector.GetOrdinal("lin_Nombre")
                    Dim NomSubLinea As Integer = lector.GetOrdinal("sl_Nombre")
                    Dim IdProducto As Integer = lector.GetOrdinal("IdProducto")
                    Dim NomEstadoProd As Integer = lector.GetOrdinal("ep_Nombre")
                    Dim IdEstadoProd As Integer = lector.GetOrdinal("IdEstadoProd")
                    Dim IdLinea As Integer = lector.GetOrdinal("IdLinea")
                    Dim IdSubLinea As Integer = lector.GetOrdinal("IdSubLInea")
                    Dim capacidad As Integer = lector.GetOrdinal("capacidad")
                    'Objeto vehiculo
                    Dim VehiculoSelect As Entidades.Vehiculo
                    Do While lector.Read
                        'instancio con la finalidad de setear variables
                        VehiculoSelect = New Entidades.Vehiculo()
                        VehiculoSelect.IdTipoExistencia = lector.GetInt32(idTipoExistencia)
                        VehiculoSelect.Placa = lector.GetString(Placa)
                        VehiculoSelect.Modelo = lector.GetString(Modelo)
                        VehiculoSelect.ConstanciaIns = lector.GetString(ConstanciaIns)
                        VehiculoSelect.ConfVeh = lector.GetString(ConfVeh)
                        VehiculoSelect.Tara = lector.GetDecimal(Tara)
                        VehiculoSelect.nombreTipoExistencia = lector.GetString(nombreTipoExistencia)
                        VehiculoSelect.NomLinea = lector.GetString(NomLinea)
                        VehiculoSelect.NomSubLinea = lector.GetString(NomSubLinea)
                        VehiculoSelect.IdProducto = lector.GetInt32(IdProducto)
                        VehiculoSelect.NomEstadoProd = lector.GetString(NomEstadoProd)
                        VehiculoSelect.IdEstadoProd = lector.GetInt32(IdEstadoProd)
                        VehiculoSelect.IdLinea = lector.GetInt32(IdLinea)
                        VehiculoSelect.IdSubLinea = lector.GetInt32(IdSubLinea)
                        VehiculoSelect.veh_Capacidad = lector.GetInt32(capacidad)
                        Lista.Add(VehiculoSelect)
                    Loop
                    lector.Close()
                End If
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboTipoExistencia() As List(Of Entidades.Vehiculo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarTipoExistenciaVehiculo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Vehiculo)
                Do While lector.Read
                    Dim obj As New Entidades.Vehiculo
                    obj.IdTipoExistencia = CInt(lector.Item("IdTipoExistencia"))
                    obj.nombreTipoExistencia = CStr(lector.Item("tex_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaVehiculoValidadoxPlaca(ByVal Vehiculo As Entidades.Vehiculo) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
        ArrayParametros(0) = New SqlParameter("@Veh_SubLinea", SqlDbType.Int)
        ArrayParametros(0).Value = Vehiculo.IdSubLinea
        ArrayParametros(1) = New SqlParameter("@Veh_EstadoPro", SqlDbType.Int)
        ArrayParametros(1).Value = Vehiculo.IdEstadoProd
        ArrayParametros(2) = New SqlParameter("@Veh_Placa", SqlDbType.VarChar)
        ArrayParametros(2).Value = Vehiculo.Placa
        ArrayParametros(3) = New SqlParameter("@veh_Modelo", SqlDbType.VarChar)
        ArrayParametros(3).Value = Vehiculo.Modelo
        ArrayParametros(4) = New SqlParameter("@veh_NConstanciaIns", SqlDbType.VarChar)
        ArrayParametros(4).Value = Vehiculo.ConstanciaIns
        ArrayParametros(5) = New SqlParameter("@veh_ConfVeh", SqlDbType.VarChar)
        ArrayParametros(5).Value = Vehiculo.ConfVeh
        ArrayParametros(6) = New SqlParameter("@veh_Tara", SqlDbType.VarChar)
        ArrayParametros(6).Value = Vehiculo.Tara
        ArrayParametros(7) = New SqlParameter("@Veh_capacidad", SqlDbType.Int)
        ArrayParametros(7).Value = Vehiculo.veh_Capacidad
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand
        Dim codigo As Integer = -1
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '******** Validar
            If ((New DAO.DAOUtil).ValidarExistenciaxTablax1Campo("Vehiculo", "Veh_Placa", Vehiculo.Placa) > 0) Then
                Throw New Exception("No se puede Guardar porque la placa ya existe en la Base de Datos .")
            End If
            '*****************
            cmd = New SqlCommand("_VehivuloInsetxIdEstadoProdxIdSubLInea", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)

            codigo = CInt(cmd.ExecuteScalar())

            tr.Commit()

            Return codigo

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function ActualizaVehiculoValidadoxPlaca(ByVal Vehiculo As Entidades.Vehiculo) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@Veh_SubLinea", SqlDbType.Int)
        ArrayParametros(0).Value = Vehiculo.IdSubLinea
        ArrayParametros(1) = New SqlParameter("@Veh_EstadoPro", SqlDbType.Int)
        ArrayParametros(1).Value = Vehiculo.IdEstadoProd
        ArrayParametros(2) = New SqlParameter("@Veh_Placa", SqlDbType.VarChar)
        ArrayParametros(2).Value = Vehiculo.Placa
        ArrayParametros(3) = New SqlParameter("@veh_Modelo", SqlDbType.VarChar)
        ArrayParametros(3).Value = Vehiculo.Modelo
        ArrayParametros(4) = New SqlParameter("@veh_NConstanciaIns", SqlDbType.VarChar)
        ArrayParametros(4).Value = Vehiculo.ConstanciaIns
        ArrayParametros(5) = New SqlParameter("@veh_ConfVeh", SqlDbType.VarChar)
        ArrayParametros(5).Value = Vehiculo.ConfVeh
        ArrayParametros(6) = New SqlParameter("@veh_Tara", SqlDbType.VarChar)
        ArrayParametros(6).Value = Vehiculo.Tara
        ArrayParametros(7) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(7).Value = Vehiculo.IdProducto
        ArrayParametros(8) = New SqlParameter("@capacidad", SqlDbType.Decimal)
        ArrayParametros(8).Value = Vehiculo.veh_Capacidad
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand
        Dim codigo As Integer = -1
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '*****************
            cmd = New SqlCommand("_VehiculoUpdatexIdProducto", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)

            codigo = CInt(cmd.ExecuteScalar())

            tr.Commit()

            Return codigo

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function DeleteVehiculoxIdDocumento(ByVal Vehiculo As Integer) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
        ArrayParametros(0) = New SqlParameter("@IdVehiculo", SqlDbType.Int)
        ArrayParametros(0).Value = Vehiculo

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand
        Dim codigo As Integer = -1
        Try

            Using cn
                cn.Open()
                tr = cn.BeginTransaction(IsolationLevel.Serializable)

                cmd = New SqlCommand("_VehiculoDeletexDocumento", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(ArrayParametros)

                codigo = CInt(cmd.ExecuteScalar())

                tr.Commit()

                Return codigo

            End Using
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function SelectGrillaVehiculo_Buscar(ByVal Placa As String, ByVal Modelo As String, ByVal Constancia As String, ByVal EstadoPro As String) As List(Of Entidades.Vehiculo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectVehiculoGrilla_Buscar", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Placa", Placa)
        cmd.Parameters.AddWithValue("@modelo", Modelo)
        cmd.Parameters.AddWithValue("@Constancia", Constancia)
        cmd.Parameters.AddWithValue("@EstadoProd", EstadoPro)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.SingleResult)
                Dim Lista As List(Of Entidades.Vehiculo) = Nothing
                If lector IsNot Nothing Then
                    Lista = New List(Of Entidades.Vehiculo)
                    Dim IdTipoExistencia As Integer = lector.GetOrdinal("IdTipoExistencia")
                    Dim Placass As Integer = lector.GetOrdinal("veh_Placa")
                    Dim Modeloss As Integer = lector.GetOrdinal("veh_Modelo")
                    Dim ConstanciaIns As Integer = lector.GetOrdinal("veh_NConstanciaIns")
                    Dim ConfVeh As Integer = lector.GetOrdinal("veh_ConfVeh")
                    Dim Tara As Integer = lector.GetOrdinal("veh_Tara")
                    Dim nombreTipoExistencia As Integer = lector.GetOrdinal("tex_Nombre")
                    Dim NomLinea As Integer = lector.GetOrdinal("lin_Nombre")
                    Dim NomSubLinea As Integer = lector.GetOrdinal("sl_Nombre")
                    Dim IdProducto As Integer = lector.GetOrdinal("IdProducto")
                    Dim NomEstadoProd As Integer = lector.GetOrdinal("ep_Nombre")
                    Dim IdEstadoProd As Integer = lector.GetOrdinal("IdEstadoProd")
                    Dim IdLinea As Integer = lector.GetOrdinal("IdLinea")
                    Dim IdSubLinea As Integer = lector.GetOrdinal("IdSubLInea")
                    Dim capacidad As Integer = lector.GetOrdinal("capacidad")
                    Dim VehiculoSelect As Entidades.Vehiculo

                    Do While lector.Read
                        VehiculoSelect = New Entidades.Vehiculo
                        VehiculoSelect.IdTipoExistencia = lector.GetInt32(IdTipoExistencia)
                        VehiculoSelect.Placa = lector.GetString(Placass)
                        VehiculoSelect.Modelo = lector.GetString(Modeloss)
                        VehiculoSelect.ConstanciaIns = lector.GetString(ConstanciaIns)
                        VehiculoSelect.ConfVeh = lector.GetString(ConfVeh)
                        VehiculoSelect.Tara = lector.GetDecimal(Tara)
                        VehiculoSelect.nombreTipoExistencia = lector.GetString(nombreTipoExistencia)
                        VehiculoSelect.NomLinea = lector.GetString(NomLinea)
                        VehiculoSelect.NomSubLinea = lector.GetString(NomSubLinea)
                        VehiculoSelect.IdProducto = lector.GetInt32(IdProducto)
                        VehiculoSelect.NomEstadoProd = lector.GetString(NomEstadoProd)
                        VehiculoSelect.IdEstadoProd = lector.GetInt32(IdEstadoProd)
                        VehiculoSelect.IdLinea = lector.GetInt32(IdLinea)
                        VehiculoSelect.IdSubLinea = lector.GetInt32(IdSubLinea)
                        VehiculoSelect.veh_Capacidad = lector.GetInt32(capacidad)
                        Lista.Add(VehiculoSelect)
                    Loop
                    lector.Close()
                End If
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function seleccionarPlacasActivas(ByVal cn As SqlConnection) As List(Of Entidades.Vehiculo)
        Using cmd As New SqlCommand("SP_DROPDOWNLIST_FILTRO", cn)
            Dim lista As List(Of Entidades.Vehiculo) = Nothing
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@FITLRO", SqlDbType.VarChar, 200)).Value = ""
                .Parameters.Add(New SqlParameter("@TABLA", SqlDbType.VarChar, 50)).Value = "TBL_PLACA"
                Dim rdr As SqlDataReader = .ExecuteReader(CommandBehavior.SingleResult)
                If rdr IsNot Nothing Then
                    lista = New List(Of Entidades.Vehiculo)
                    Dim veh_Placa As Integer = rdr.GetOrdinal("veh_Placa")
                    Dim objPlaca As Entidades.Vehiculo
                    While rdr.Read()
                        objPlaca = New Entidades.Vehiculo
                        With objPlaca
                            .Placa = rdr.GetString(veh_Placa)
                        End With
                        lista.Add(objPlaca)
                    End While
                    rdr.Close()
                End If
                Return lista
            End With
        End Using
    End Function
End Class

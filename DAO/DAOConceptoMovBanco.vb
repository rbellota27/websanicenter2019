﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 03/02/2010 12:47 pm
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOConceptoMovBanco
    Protected objConexion As New Conexion
    Protected Cn As SqlConnection '= objConexion.ConexionSIGE()
    Protected Tr As SqlTransaction
    Private reader As SqlDataReader = Nothing




    Public Function ConceptoMovBanco_MedioPago(ByVal IdMediopago As Integer, ByVal IdTipoConceptoBanco As Integer) As List(Of Entidades.ConceptoMovBanco)

        Dim lista As List(Of Entidades.ConceptoMovBanco)
        Dim obj As Entidades.ConceptoMovBanco


        Cn = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_ConceptoMovBanco_MedioPago", Cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMediopago)
        cmd.Parameters.AddWithValue("@IdTipoConceptoBanco", IdTipoConceptoBanco)

        Try
            Cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            lista = New List(Of Entidades.ConceptoMovBanco)
            While reader.Read

                obj = New Entidades.ConceptoMovBanco
                With obj
                    .Id = CInt(reader("IdConceptoMovBanco"))
                    .Nombre = CStr(reader("cban_Descripcion"))
                    .DescripcionBreve = CStr(IIf(IsDBNull(reader("cban_DescripcionBreve")), "", reader("cban_DescripcionBreve")))
                End With

                lista.Add(obj)
                obj = Nothing

            End While
            reader.Close()

            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            If Cn.State = ConnectionState.Open Then Cn.Close()
        End Try

        Return Nothing

    End Function






    Public Function InsertT_GetID(ByVal x As Entidades.ConceptoMovBanco) As Integer
        'InsertT Insert con Transacciones
        Cn = objConexion.ConexionSIGE()
        Dim resul As Integer

        Dim Prm() As SqlParameter = New SqlParameter(4) {}

        Prm(0) = New SqlParameter("@cban_Descripcion", SqlDbType.VarChar)
        Prm(0).Value = x.Descripcion
        Prm(1) = New SqlParameter("@cban_DescripcionBreve", SqlDbType.VarChar)
        Prm(1).Value = x.DescripcionBreve
        Prm(2) = New SqlParameter("@cban_EstadoAutoAprobacion", SqlDbType.Bit)
        Prm(2).Value = x.EstadoAutoAprobacion
        Prm(3) = New SqlParameter("@cban_Estado", SqlDbType.Bit)
        Prm(3).Value = x.Estado
        Prm(4) = New SqlParameter("@IdTipoConceptoBanco", SqlDbType.Int)
        Prm(4).Value = x.IdTipoConceptoBanco

        Cn.Open()
        Tr = Cn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteScalar(Tr, CommandType.StoredProcedure, "_ConceptoMovBancoInsertGetID", Prm))

            Tr.Commit()
        Catch e As Exception
            Tr.Rollback()
            Throw (e)
        Finally
            Cn.Close()
            Cn.Dispose()

        End Try
        Return (resul)
    End Function
    Public Function InsertT(ByVal x As Entidades.ConceptoMovBanco) As Boolean
        If InsertT_GetID(x) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function UpdateT(ByVal x As Entidades.ConceptoMovBanco) As Boolean
        'UpdateT Update con Transacciones
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(5) {}

        Prm(0) = New SqlParameter("@IdConceptoMovBanco", SqlDbType.Int)
        Prm(0).Value = x.Id
        Prm(1) = New SqlParameter("@cban_Descripcion", SqlDbType.VarChar)
        Prm(1).Value = x.Descripcion
        Prm(2) = New SqlParameter("@cban_DescripcionBreve", SqlDbType.VarChar)
        Prm(2).Value = x.DescripcionBreve
        Prm(3) = New SqlParameter("@cban_EstadoAutoAprobacion", SqlDbType.Bit)
        Prm(3).Value = x.EstadoAutoAprobacion
        Prm(4) = New SqlParameter("@cban_Estado", SqlDbType.Bit)
        Prm(4).Value = x.Estado
        Prm(5) = New SqlParameter("@IdTipoConceptoBanco", SqlDbType.Int)
        Prm(5).Value = x.IdTipoConceptoBanco

        Cn.Open()
        Tr = Cn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(Tr, CommandType.StoredProcedure, "_ConceptoMovBancoUpdate", Prm)
            Tr.Commit()
            Return True
        Catch e As Exception
            Tr.Rollback()
            Throw (e)
            Return False
        Finally
            Cn.Close()
            Cn.Dispose()

        End Try
    End Function
End Class



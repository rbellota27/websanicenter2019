﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*********************   LUNES 24052010

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOCostoFletexUnd

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private cmd As SqlCommand
    Private reader As SqlDataReader
    Dim objConexion As New DAO.Conexion


    Public Sub CostoFletexUnd_Delete(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, ByVal IdProducto As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(2) {}
        p(0) = objDaoMantenedor.getParam(IdTiendaOrigen, "@IdTiendaOrigen", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(IdTiendaDestino, "@IdTiendaDestino", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(IdProducto, "@IdProducto", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_CostoFletexUnd_DeletexParams", p)

    End Sub
    'becker
    Public Function CostoFletexUnd_SelectxParams_DT(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, ByVal IdProducto As Integer, ByVal IdLinea As Integer, _
                                                    ByVal IdSubLinea As Integer, ByVal Producto As String, _
                                                    ByVal CodigoProd As String, ByVal IdTipoExistencia As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(7) {}

            p(0) = objDaoMantenedor.getParam(IdTiendaOrigen, "@IdTiendaOrigen", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTiendaDestino, "@IdTiendaDestino", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdProducto, "@IdProducto", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdLinea, "@IdLinea", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(IdSubLinea, "@IdSubLinea", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(Producto, "@Producto", SqlDbType.VarChar)
            p(6) = objDaoMantenedor.getParam(CodigoProd, "@CodigoProd", SqlDbType.VarChar)
            p(7) = objDaoMantenedor.getParam(IdTipoExistencia, "@IdTipoExistencia", SqlDbType.Int)

            cmd = New SqlCommand("_CostoFletexUnd_SelectxParams", (New DAO.Conexion).ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CostoFletexUnd")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_CostoFletexUnd")

    End Function

    Public Sub CostoFletexUnd_Registrar(ByVal objCostoFletexUnd As Entidades.CostoFletexUnd, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(5) {}
        With objCostoFletexUnd
            p(0) = objDaoMantenedor.getParam(.IdTiendaOrigen, "@IdTiendaOrigen", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(.IdProducto, "@IdProducto", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(.IdUnidadMedida, "@IdUnidadMedida", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(.CostoxUnd, "@cfu_costoxUnd", SqlDbType.Decimal)
            p(4) = objDaoMantenedor.getParam(.IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(.IdTiendaDestino, "@IdTiendaDestino", SqlDbType.Int)
        End With

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_CostoFletexUnd_Registrar", p)

    End Sub

    'becker
    Public Function CostoFletexUnd_SelectProductoBasexParams(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, ByVal IdProducto As Integer, ByVal IdLinea As Integer, _
                                                             ByVal IdSubLinea As Integer, ByVal Producto As String, _
                                                             ByVal CodigoProd As String, ByVal IdTipoExistencia As Integer, _
                                                             ByVal tabla As DataTable) As List(Of Entidades.CostoFletexUnd)

        Dim lista As New List(Of Entidades.CostoFletexUnd)

        Try
            cmd = New SqlCommand("_CostoFletexUnd_SelectProductoBasexParams", cn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@IdTiendaOrigen", IdTiendaOrigen)
            cmd.Parameters.AddWithValue("@IdTiendaDestino", IdTiendaDestino)
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
            cmd.Parameters.AddWithValue("@Producto", Producto)
            cmd.Parameters.AddWithValue("@CodigoProd", CodigoProd)
            cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
            cmd.Parameters.AddWithValue("@tabla", tabla)

            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            While (reader.Read)

                Dim obj As New Entidades.CostoFletexUnd
                With obj

                    .IdTiendaOrigen = objDaoMantenedor.UCInt(reader("IdTiendaOrigen"))
                    .IdTiendaDestino = objDaoMantenedor.UCInt(reader("IdTiendaDestino"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .CostoxUnd = objDaoMantenedor.UCDec(reader("CostoxUnd"))
                    .IdUnidadMedida = objDaoMantenedor.UCInt(reader("IdUnidadMedida"))
                    .Moneda = objDaoMantenedor.UCStr(reader("Moneda"))
                    .UnidadMedida = objDaoMantenedor.UCStr(reader("UnidadMedida"))
                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .Producto = objDaoMantenedor.UCStr(reader("Producto"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("CodigoProducto"))
                    .Cadena_IdUnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_IdUnidadMedida_Prod"))
                    .Cadena_UnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_UnidadMedida_Prod"))

                End With
                lista.Add(obj)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

End Class

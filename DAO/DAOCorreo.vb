'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOCorreo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaCorreo(ByVal cn As SqlConnection, ByVal Lcor As List(Of Entidades.Correo), ByVal T As SqlTransaction)
        Dim Correo As New Entidades.Correo
        For Each Correo In Lcor
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            ArrayParametros(0) = New SqlParameter("@corr_Nombre", SqlDbType.VarChar)
            ArrayParametros(0).Value = Correo.Nombre
            ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(1).Value = Correo.IdPersona
            ArrayParametros(2) = New SqlParameter("@IdTipoCorreo", SqlDbType.Int)
            ArrayParametros(2).Value = Correo.IdTipoCorreo
            'ArrayParametros(3) = New SqlParameter("@IdCorreo", SqlDbType.Int)
            'ArrayParametros(3).Value = Correo.Id
            HDAO.InsertaT(cn, "_CorreoInsert", ArrayParametros, T)
        Next
    End Sub

    Public Sub InsertaListaCorreo(ByVal cn As SqlConnection, ByVal Lcor As List(Of Entidades.CorreoView), ByVal T As SqlTransaction)
        Dim Correo As New Entidades.CorreoView
        For Each Correo In Lcor
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            ArrayParametros(0) = New SqlParameter("@corr_Nombre", SqlDbType.VarChar)
            ArrayParametros(0).Value = Correo.Nombre
            ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(1).Value = Correo.IdPersona
            ArrayParametros(2) = New SqlParameter("@IdTipoCorreo", SqlDbType.Int)
            ArrayParametros(2).Value = Correo.IdTipoCorreo
            'ArrayParametros(3) = New SqlParameter("@IdCorreo", SqlDbType.Int)
            'ArrayParametros(3).Value = Correo.Id
            HDAO.InsertaT(cn, "_CorreoInsert", ArrayParametros, T)
        Next
    End Sub

    Public Function ActualizaCorreo(ByVal correo As Entidades.Correo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@corr_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = correo.Nombre
        ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(1).Value = correo.IdPersona
        ArrayParametros(2) = New SqlParameter("@IdTipoCorreo", SqlDbType.Int)
        ArrayParametros(2).Value = correo.IdTipoCorreo
        ArrayParametros(3) = New SqlParameter("@IdCorreo", SqlDbType.Int)
        ArrayParametros(4).Value = correo.Id
        Return HDAO.Update(cn, "_CorreoUpdate", ArrayParametros)
    End Function
    Public Function DeletexIdPersona(ByVal cn As SqlConnection, ByVal IdPersona As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_CorreoDelete", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

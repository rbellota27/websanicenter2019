'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 30-Oct-2009
'Hora    : 09:50 am
'*************************************************
Imports System.Data.SqlClient

Public Class DAOMagnitud
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function InsertaMagnitud(ByVal magnitud As Entidades.Magnitud, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@mag_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(magnitud.Descripcion = Nothing, DBNull.Value, magnitud.Descripcion)
        ArrayParametros(1) = New SqlParameter("@mag_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(magnitud.Abv = Nothing, DBNull.Value, magnitud.Abv)
        ArrayParametros(2) = New SqlParameter("@mag_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = IIf(magnitud.Estado = Nothing, DBNull.Value, magnitud.Estado)
        ArrayParametros(3) = New SqlParameter("@IdMagnitud", SqlDbType.Int)
        ArrayParametros(3).Direction = ParameterDirection.Output
        Dim cmd As New SqlCommand("_MagnitudInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        cmd.ExecuteNonQuery()
        Return CInt(cmd.Parameters("@IdMagnitud").Value)
    End Function

    Public Function ActualizaMagnitud(ByVal magnitud As Entidades.Magnitud, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean

        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@mag_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(magnitud.Descripcion = Nothing, DBNull.Value, magnitud.Descripcion)
        ArrayParametros(1) = New SqlParameter("@mag_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(magnitud.Abv = Nothing, DBNull.Value, magnitud.Abv)
        ArrayParametros(2) = New SqlParameter("@mag_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = IIf(magnitud.Estado = Nothing, DBNull.Value, magnitud.Estado)
        ArrayParametros(3) = New SqlParameter("@IdMagnitud", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(magnitud.Id = Nothing, DBNull.Value, magnitud.Id)

        Dim cmd As New SqlCommand("_MagnitudUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If (cmd.ExecuteNonQuery = 0) Then
            Return False
        End If
        Return True

    End Function
    Public Function SelectAll() As List(Of Entidades.Magnitud)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Magnitud)
                Do While lector.Read
                    Dim obj As New Entidades.Magnitud
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("mag_Abv")) = True, "", lector.Item("mag_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("mag_Estado")) = True, "", lector.Item("mag_Estado")))
                    obj.NombreUnidadMedida = CStr(IIf(IsDBNull(lector.Item("um_NombreLargo")) = True, "---", lector.Item("um_NombreLargo")))
                    obj.Equivalencia = CDec(IIf(IsDBNull(lector.Item("mu_Equivalencia")) = True, 0, lector.Item("mu_Equivalencia")))
                    obj.Id = CInt(lector.Item("IdMagnitud"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Magnitud)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Magnitud)
                Do While lector.Read
                    Dim obj As New Entidades.Magnitud
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("mag_Abv")) = True, "", lector.Item("mag_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("mag_Estado")) = True, "", lector.Item("mag_Estado")))
                    obj.Equivalencia = CDec(IIf(IsDBNull(lector.Item("mu_Equivalencia")) = True, 0, lector.Item("mu_Equivalencia")))
                    obj.NombreUnidadMedida = CStr(IIf(IsDBNull(lector.Item("um_NombreLargo")) = True, "----", lector.Item("um_NombreLargo")))
                    obj.Id = CInt(lector.Item("IdMagnitud"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Magnitud)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Magnitud)
                Do While lector.Read
                    Dim obj As New Entidades.Magnitud
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("mag_Abv")) = True, "", lector.Item("mag_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("mag_Estado")) = True, "", lector.Item("mag_Estado")))
                    obj.Equivalencia = CDec(IIf(IsDBNull(lector.Item("mu_Equivalencia")) = True, 0, lector.Item("mu_Equivalencia")))
                    obj.NombreUnidadMedida = CStr(IIf(IsDBNull(lector.Item("um_NombreLargo")) = True, "---", lector.Item("um_NombreLargo")))
                    obj.Id = CInt(lector.Item("IdMagnitud"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Magnitud)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Magnitud)
                Do While lector.Read
                    Dim obj As New Entidades.Magnitud
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("mag_Abv")) = True, "", lector.Item("mag_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("mag_Estado")) = True, "", lector.Item("mag_Estado")))
                    obj.Id = CInt(lector.Item("IdMagnitud"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Magnitud)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Magnitud)
                Do While lector.Read
                    Dim obj As New Entidades.Magnitud
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("mag_Abv")) = True, "", lector.Item("mag_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("mag_Estado")) = True, "", lector.Item("mag_Estado")))
                    obj.Id = CInt(lector.Item("IdMagnitud"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Magnitud)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Magnitud)
                Do While lector.Read
                    Dim obj As New Entidades.Magnitud
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("mag_Abv")) = True, "", lector.Item("mag_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("mag_Estado")) = True, "", lector.Item("mag_Estado")))
                    obj.Id = CInt(lector.Item("IdMagnitud"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Magnitud)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Magnitud)
                Do While lector.Read
                    Dim obj As New Entidades.Magnitud
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("mag_Abv")) = True, "", lector.Item("mag_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("mag_Estado")) = True, "", lector.Item("mag_Estado")))
                    obj.Id = CInt(lector.Item("IdMagnitud"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Magnitud)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MagnitudSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Magnitud)
                Do While lector.Read
                    Dim obj As New Entidades.Magnitud
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("mag_Nombre")) = True, "", lector.Item("mag_Nombre")))
                    obj.Id = CInt(lector.Item("IdMagnitud"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

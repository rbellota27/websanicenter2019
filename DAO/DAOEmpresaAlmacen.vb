﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOEmpresaAlmacen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaEmpresaAlmacenT(ByVal cn As SqlConnection, ByVal lempresalmacen As List(Of Entidades.EmpresaAlmacen), ByVal T As SqlTransaction)
        Dim EmpresaAlmacen As Entidades.EmpresaAlmacen
        For Each EmpresaAlmacen In lempresalmacen
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(0).Value = EmpresaAlmacen.IdEmpresa
            ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            ArrayParametros(1).Value = EmpresaAlmacen.IdAlmacen
            HDAO.InsertaT(cn, "_EmpresaAlmacenInsert", ArrayParametros, T)
        Next
    End Sub
    Public Function DeletexIdEmpresa(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_EmpresaAlmacenDeleteAllByIdEmpresa", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub GrabaEmpresaAlmacenT(ByVal cn As SqlConnection, ByVal lempresalmacen As List(Of Entidades.EmpresaAlmacen), ByVal T As SqlTransaction)
        Dim EmpresaAlmacen As Entidades.EmpresaAlmacen
        For Each EmpresaAlmacen In lempresalmacen
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(0).Value = EmpresaAlmacen.IdEmpresa
            ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            ArrayParametros(1).Value = EmpresaAlmacen.IdAlmacen
            ArrayParametros(2) = New SqlParameter("@Estado", SqlDbType.Bit)
            ArrayParametros(2).Value = EmpresaAlmacen.EstadoAI
            HDAO.InsertaT(cn, "InsUpd_EmpresaAlmacen", ArrayParametros, T)
        Next
    End Sub
End Class


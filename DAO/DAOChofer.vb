'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'**************** VIERNES 19 FEB 2010 HORA 02_58 PM








Imports System.Data.SqlClient

Public Class DAOChofer
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function InsertaChoferTransact(ByVal chofer As Entidades.Chofer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ch_NLicencia", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(chofer.Licencia = Nothing, DBNull.Value, chofer.Licencia)
        ArrayParametros(1) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(1).Value = chofer.Id
        ArrayParametros(2) = New SqlParameter("@ch_Categoria", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(chofer.Categoria = Nothing, DBNull.Value, chofer.Categoria)

        Dim cmd As New SqlCommand("_InsertChofer", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery = 0 Then
            Return False
        End If
        Return True
    End Function

    Public Function InsertaChofer(ByVal chofer As Entidades.Chofer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ch_NLicencia", SqlDbType.VarChar)
        ArrayParametros(0).Value = chofer.Licencia
        ArrayParametros(1) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(1).Value = chofer.Id
        ArrayParametros(2) = New SqlParameter("@ch_Categoria", SqlDbType.VarChar)
        ArrayParametros(2).Value = chofer.Categoria
        Return HDAO.Insert(cn, "_ChoferInsert", ArrayParametros)
    End Function

    Public Sub InsertaChofer(ByVal chofer As Entidades.Chofer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ch_NLicencia", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(chofer.Licencia = Nothing, DBNull.Value, chofer.Licencia)
        ArrayParametros(1) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(1).Value = chofer.Id
        ArrayParametros(2) = New SqlParameter("@ch_Categoria", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(chofer.Categoria = Nothing, DBNull.Value, chofer.Categoria)
        HDAO.InsertaT(cn, "_ChoferInsert", ArrayParametros, tr)
    End Sub


    Public Function ActualizaChofer(ByVal chofer As Entidades.Chofer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@ch_NLicencia", SqlDbType.VarChar)
        ArrayParametros(0).Value = chofer.Licencia
        ArrayParametros(1) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(1).Value = chofer.Id
        ArrayParametros(2) = New SqlParameter("@ch_Categoria", SqlDbType.VarChar)
        ArrayParametros(2).Value = chofer.Categoria
        ArrayParametros(3) = New SqlParameter("@estado", SqlDbType.VarChar)
        ArrayParametros(3).Value = chofer.estado
        Return HDAO.Update(cn, "_ChoferUpdate", ArrayParametros)
    End Function

    Public Function ActualizaChoferTransact(ByVal chofer As Entidades.Chofer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ch_NLicencia", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(chofer.Licencia = Nothing, DBNull.Value, chofer.Licencia)
        ArrayParametros(1) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(1).Value = chofer.Id
        ArrayParametros(2) = New SqlParameter("@ch_Categoria", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(chofer.Categoria = Nothing, DBNull.Value, chofer.Categoria)
        Dim cmd As New SqlCommand("_UpdateChofer", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery = 0 Then
            Return False
        End If
        Return True
    End Function

    Public Function listarChoferxID(ByVal idChofer As Integer) As Entidades.Chofer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarChoferxID", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idchofer", idChofer)
        Dim lector As SqlDataReader
        Dim obj As Entidades.Chofer
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                obj = New Entidades.Chofer
                With obj
                    .Id = CInt(lector("IdChofer"))
                    .Licencia = CStr(IIf(IsDBNull(lector("ch_NLicencia")), "---", lector("ch_NLicencia")))
                    .Categoria = CStr(IIf(IsDBNull(lector("ch_Categoria")), "---", lector("ch_Categoria")))
                    .esChofer = True
                    .TotalRegEnDocumento = CInt(IIf(IsDBNull(lector("totalReg")), "---", lector("totalReg")))
                End With
            End If
            lector.Close()
            Return obj
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Chofer)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ChoferSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Chofer)
                Do While lector.Read
                    Dim obj As New Entidades.Chofer
                    obj.Id = CInt(lector.Item("IdChofer"))
                    obj.Categoria = CStr(IIf(IsDBNull(lector.Item("ch_Categoria")) = True, "", lector.Item("ch_Categoria")))
                    obj.DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                    obj.Licencia = CStr(IIf(IsDBNull(lector.Item("ch_NLicencia")) = True, "", lector.Item("ch_NLicencia")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function deleteChofer(ByVal idchofer As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim cmd As New SqlCommand("_deleteChofer", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idchofer", idchofer)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ChoferSelectActivoxParams(ByVal dni As String, ByVal ruc As String, _
                                  ByVal RazonApe As String, ByVal tipo As Integer, _
                                  ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                  ByVal estado As Integer) As List(Of Entidades.PersonaView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim paramentros() As SqlParameter = New SqlParameter(6) {}
        paramentros(0) = New SqlParameter("@Dni", SqlDbType.VarChar)
        paramentros(0).Value = dni
        paramentros(1) = New SqlParameter("@Ruc", SqlDbType.VarChar)
        paramentros(1).Value = ruc
        paramentros(2) = New SqlParameter("@razonApe", SqlDbType.VarChar)
        paramentros(2).Value = RazonApe
        paramentros(3) = New SqlParameter("@tipo", SqlDbType.Int)
        paramentros(3).Value = tipo
        paramentros(4) = New SqlParameter("@pageindex", SqlDbType.Int)
        paramentros(4).Value = pageindex
        paramentros(5) = New SqlParameter("@pageSize", SqlDbType.Int)
        paramentros(5).Value = pagesize
        paramentros(6) = New SqlParameter("@estado", SqlDbType.Int)
        paramentros(6).Value = estado

        Dim cmd As New SqlCommand("_ChoferSelectActivoxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(paramentros)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                        .Ruc = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                        .Estado = CStr(IIf(IsDBNull(lector("estado")), "---", lector("estado")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

End Class


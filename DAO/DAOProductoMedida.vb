'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM

Imports System.Data.SqlClient

Public Class DAOProductoMedida
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function getMagnitudProducto(ByVal IdProducto As Integer, ByVal IdUMProducto As Integer, ByVal Cantidad As Decimal, ByVal IdMagnitud As Integer, ByVal IdUMMagnitud_Destino As Integer) As Decimal

        Dim valorMagnitud As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try

            cn.Open()
            Dim cmd As New SqlCommand(" select dbo.fx_getMagnitudProducto(" + CStr(IdProducto) + "," + CStr(IdUMProducto) + "," + CStr(Cantidad) + "," + CStr(IdMagnitud) + "," + CStr(IdUMMagnitud_Destino) + ")  ", cn)
            cmd.CommandType = CommandType.Text

            valorMagnitud = CDec(cmd.ExecuteScalar())

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return valorMagnitud

    End Function

    Public Sub InsertaProductoMedida(idusuario As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal productomedida As Entidades.ProductoMedida)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = productomedida.IdProducto
        ArrayParametros(1) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(1).Value = productomedida.IdUnidadMedida
        ArrayParametros(2) = New SqlParameter("@IdMagnitud", SqlDbType.Int)
        ArrayParametros(2).Value = productomedida.IdMagnitud
        ArrayParametros(3) = New SqlParameter("@pm_Escalar", SqlDbType.Decimal)
        ArrayParametros(3).Value = productomedida.Escalar
        ArrayParametros(4) = New SqlParameter("@pm_principal", SqlDbType.Bit)
        ArrayParametros(4).Value = productomedida.pm_principal
        ArrayParametros(5) = New SqlParameter("@idUsuario", SqlDbType.Bit)
        ArrayParametros(5).Value = idusuario
        HDAO.InsertaT(cn, "_ProductoMedidaInsert", ArrayParametros, tr)
    End Sub
    Public Sub ActualizaProductoMedida(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal productomedida As Entidades.ProductoMedida)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = productomedida.IdProducto
        ArrayParametros(1) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(1).Value = productomedida.IdUnidadMedida
        ArrayParametros(2) = New SqlParameter("@IdMagnitud", SqlDbType.Int)
        ArrayParametros(2).Value = productomedida.IdMagnitud
        ArrayParametros(3) = New SqlParameter("@pm_Escalar", SqlDbType.Decimal)
        ArrayParametros(3).Value = productomedida.Escalar
        HDAO.Update(cn, "_ProductoMedidaUpdate", ArrayParametros)
    End Sub
    Public Sub DeletexIdProducto(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idProducto As Integer)
        Dim cmd As New SqlCommand("_DeleteProductoMedidaxIdProducto", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idProducto)
        cmd.ExecuteNonQuery()
    End Sub
    Public Function DeletexIdProducto(ByVal IdProducto As Integer, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        'recibo la conexion abierta
        Dim cmd As SqlCommand
        Try
            If tr IsNot Nothing Then
                cmd = New SqlCommand("_DeleteProductoMedidaxIdProducto", cn, tr)
            Else
                cmd = New SqlCommand("_DeleteProductoMedidaxIdProducto", cn)
            End If
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class

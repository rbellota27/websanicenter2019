﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM

Imports System.Data.SqlClient

Public Class DAOVolumenVenta
    Inherits DAOMantenedor

    Dim HDAO As New DAO.HelperDAO
    'Dim objConexion As New Conexion

    Public Function InsertaVolumenVenta(ByVal obj As Entidades.VolumenVenta) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Prm() As SqlParameter = New SqlParameter(4) {}
        Prm(0) = getParam(obj.IdProducto, "@IdProducto", SqlDbType.Int)
        Prm(1) = getParam(obj.IdTipoPV, "@IdTipoPV", SqlDbType.Int)
        Prm(2) = getParam(obj.IdTienda, "@IdTienda", SqlDbType.Int)
        Prm(3) = getParam(obj.vven_CantidadMin_UMPr, "@vven_CantidadMin_UMPr", SqlDbType.Decimal)
        Prm(4) = getParam(obj.vven_FechaRegistro, "@vven_FechaRegistro", SqlDbType.Date)

        Return HDAO.Insert(cn, "_VolumenVentaInsert", Prm)
    End Function
    Public Function ActualizaVolumenVenta(ByVal obj As Entidades.VolumenVenta) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Prm() As SqlParameter = New SqlParameter(4) {}
        Prm(0) = getParam(obj.IdProducto, "@IdProducto", SqlDbType.Int)
        Prm(1) = getParam(obj.IdTipoPV, "@IdTipoPV", SqlDbType.Int)
        Prm(2) = getParam(obj.IdTienda, "@IdTienda", SqlDbType.Int)
        Prm(3) = getParam(obj.vven_CantidadMin_UMPr, "@vven_CantidadMin_UMPr", SqlDbType.Decimal)
        Prm(4) = getParam(obj.vven_FechaRegistro, "@vven_FechaRegistro", SqlDbType.Date)

        Return HDAO.Update(cn, "_VolumenVentaUpdate", Prm)
    End Function

    Public Function DeleteVolumenVenta(ByVal obj As Entidades.VolumenVenta) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Prm() As SqlParameter = New SqlParameter(2) {}

        Prm(0) = getParam(obj.IdProducto, "@IdProducto", SqlDbType.Int)
        Prm(1) = getParam(obj.IdTipoPV, "@IdTipoPV", SqlDbType.Int)
        Prm(2) = getParam(obj.IdTienda, "@IdTienda", SqlDbType.Int)

        Return HDAO.DeletexParams(cn, "_VolumenVentaDelete", Prm)
    End Function

    Public Function DeleteVolumenVentaxParametros(ByVal IdLinea As Integer, _
    ByVal IdSubLinea As Integer, ByVal IdTipoPV As Integer, ByVal IdTienda As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_VolumenVentaDeletexParametros", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim Prm() As SqlParameter = New SqlParameter(3) {}

        Prm(0) = getParam(IdLinea, "@IdLinea", SqlDbType.Int)
        Prm(1) = getParam(IdSubLinea, "@IdSublinea", SqlDbType.Int)
        Prm(2) = getParam(IdTipoPV, "@IdTipoPV", SqlDbType.Int)
        Prm(3) = getParam(IdTienda, "@IdTienda", SqlDbType.Int)

        cmd.Parameters.AddRange(Prm)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        'Return HDAO.DeletexParams(cn, "", Prm)
    End Function

    Public Function InsertaVolumenVentaxParametros(ByVal obj As Entidades.VolumenVenta, ByVal sqlcn As SqlConnection, ByVal sqltr As SqlTransaction) As Boolean

        Dim Prm() As SqlParameter = New SqlParameter(3) {}

        Prm(0) = getParam(obj.IdProducto, "@IdProducto", SqlDbType.Int)
        Prm(1) = getParam(obj.IdTipoPV, "@IdTipoPV", SqlDbType.Int)
        Prm(2) = getParam(obj.IdTienda, "@IdTienda", SqlDbType.Int)
        Prm(3) = getParam(obj.vven_CantidadMin_UMPr, "@vven_CantidadMin_UMPr", SqlDbType.Decimal)

        Dim cmd As New SqlCommand("_VolumenVentaInsertaxParametros", sqlcn, sqltr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(Prm)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return True

    End Function

End Class

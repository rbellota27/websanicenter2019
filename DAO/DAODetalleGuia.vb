'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAODetalleGuia
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaDetalleGuia(ByVal cn As SqlConnection, ByVal detalleguia As Entidades.DetalleGuia, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(detalleguia.IdProducto = Nothing, DBNull.Value, detalleguia.IdProducto)
        ArrayParametros(1) = New SqlParameter("@dg_cantidad", SqlDbType.Decimal)
        ArrayParametros(1).Value = IIf(detalleguia.Cantidad = Nothing, DBNull.Value, detalleguia.Cantidad)
        ArrayParametros(2) = New SqlParameter("@dg_UMedida", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(detalleguia.UMedida = Nothing, DBNull.Value, detalleguia.UMedida)
        ArrayParametros(3) = New SqlParameter("@dg_Peso", SqlDbType.Decimal)
        ArrayParametros(3).Value = IIf(detalleguia.Peso = Nothing, DBNull.Value, detalleguia.Peso)
        ArrayParametros(4) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(detalleguia.IdDocumento = Nothing, DBNull.Value, detalleguia.IdDocumento)
        ArrayParametros(5) = New SqlParameter("@dg_UmPeso", SqlDbType.VarChar)
        ArrayParametros(5).Value = IIf(detalleguia.UMPeso = Nothing, DBNull.Value, detalleguia.UMPeso)
        HDAO.InsertaT(cn, "_DetalleGuiaInsert", ArrayParametros, T)
    End Sub
    Public Function ActualizaDetalleGuia(ByVal detalleguia As Entidades.DetalleGuia) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = detalleguia.IdProducto
        ArrayParametros(1) = New SqlParameter("@dg_cantidad", SqlDbType.Decimal)
        ArrayParametros(1).Value = detalleguia.Cantidad
        ArrayParametros(2) = New SqlParameter("@dg_UMedida", SqlDbType.VarChar)
        ArrayParametros(2).Value = detalleguia.UMedida
        ArrayParametros(3) = New SqlParameter("@dg_Peso", SqlDbType.Decimal)
        ArrayParametros(3).Value = detalleguia.Peso
        ArrayParametros(4) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(4).Value = detalleguia.IdDocumento
        ArrayParametros(5) = New SqlParameter("@dg_UmPeso", SqlDbType.VarChar)
        ArrayParametros(5).Value = detalleguia.UMPeso
        Return HDAO.Update(cn, "_DetalleGuiaUpdate", ArrayParametros)
    End Function
    Public Function SelectxIdDocumento(ByVal iddocumento As Integer) As List(Of Entidades.DetalleGuia)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleGuiaSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleGuia)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleGuia
                    With obj
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dg_cantidad")) = True, 0, lector.Item("dg_cantidad")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dg_UMedida")) = True, "", lector.Item("dg_UMedida")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Peso = CDec(IIf(IsDBNull(lector.Item("dg_Peso")) = True, 0, lector.Item("dg_Peso")))
                        .UMPeso = CStr(IIf(IsDBNull(lector.Item("dg_UmPeso")) = True, "", lector.Item("dg_UmPeso")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

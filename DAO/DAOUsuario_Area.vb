﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

' viernes 29 de enero 2010

Imports System.Data.SqlClient

Public Class DAOUsuario_Area
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Function ListarAreaxIdusuario(ByVal IdUsuario As Integer, ByVal addIdArea As Integer) As List(Of Entidades.Usuario_Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Lista As New List(Of Entidades.Usuario_Area)
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarAreaxIdusuario", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@addIdArea", IIf(addIdArea = Nothing, DBNull.Value, addIdArea))
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim Obj As New Entidades.Usuario_Area
                With Obj
                    .IdArea = CInt(lector("IdArea"))
                    .ar_NombreCorto = CStr(IIf(IsDBNull(lector("ar_NombreCorto")), "---", lector("ar_NombreCorto")))
                End With
                Lista.Add(Obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function InsertaUsuario_Area(ByVal Usuario_Area As Entidades.Usuario_Area) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdUsuario", SqlDbType.VarChar)
        ArrayParametros(0).Value = Usuario_Area.IdUsuario
        ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(1).Value = Usuario_Area.IdArea
        ArrayParametros(2) = New SqlParameter("@ua_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = Usuario_Area.Estado
        Return HDAO.Insert(cn, "_Usuario_AreaInsert", ArrayParametros)
    End Function
    Public Function ActualizaUsuario_Area(ByVal Usuario_Area As Entidades.Usuario_Area) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdUsuario", SqlDbType.VarChar)
        ArrayParametros(0).Value = Usuario_Area.IdUsuario
        ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(1).Value = Usuario_Area.IdArea
        ArrayParametros(2) = New SqlParameter("@ua_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = Usuario_Area.Estado

        Return HDAO.Update(cn, "_Usuario_AreaUpdate", ArrayParametros)
    End Function

    Public Function Insert(ByVal cn As SqlConnection, ByVal obj As Entidades.Usuario_Area, ByVal tr As SqlTransaction) As Boolean

        Dim cmd As New SqlCommand("_Usuario_AreaInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", obj.IdUsuario)
        cmd.Parameters.AddWithValue("@IdArea", obj.IdArea)
        cmd.Parameters.AddWithValue("@ua_Estado", obj.Estado)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function
    Public Function Update(ByVal cn As SqlConnection, ByVal obj As Entidades.Usuario_Area, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_Usuario_AreaUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", obj.IdUsuario)
        cmd.Parameters.AddWithValue("@IdArea", obj.IdArea)
        cmd.Parameters.AddWithValue("@ua_Estado", obj.Estado)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function
    Public Function DeletexIdUsuarioxIdArea(ByVal cn As SqlConnection, ByVal obj As Entidades.Usuario_Area, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_Usuario_AreaDeletexIdUsuarioxIdArea", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", obj.IdUsuario)
        cmd.Parameters.AddWithValue("@IdArea", obj.IdArea)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function
    Public Function SelectAll() As List(Of Entidades.Usuario_Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Usuario_AreaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Usuario_Area)
                Do While lector.Read
                    Dim Usuario_Area As New Entidades.Usuario_Area

                    Usuario_Area.IdUsuario = CInt(lector.Item("IdUsuario"))
                    Usuario_Area.IdArea = CInt(lector.Item("IdArea"))
                    Usuario_Area.Estado = CBool(lector.Item("ua_Estado"))
                    Usuario_Area.NombreArea = CStr(lector.Item("ar_NombreLargo"))

                    Lista.Add(Usuario_Area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Usuario_Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Usuario_AreaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Usuario_Area)
                Do While lector.Read
                    Dim Usuario_Area As New Entidades.Usuario_Area
                    Usuario_Area.IdUsuario = CInt(lector.Item("IdUsuario"))
                    Usuario_Area.IdArea = CInt(lector.Item("IdArea"))
                    Usuario_Area.Estado = CBool(lector.Item("ua_Estado"))
                    Usuario_Area.NombreArea = CStr(lector.Item("ar_NombreLargo"))

                    Lista.Add(Usuario_Area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Usuario_Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Usuario_AreaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Usuario_Area)
                Do While lector.Read
                    Dim Usuario_Area As New Entidades.Usuario_Area
                    Usuario_Area.IdUsuario = CInt(lector.Item("IdUsuario"))
                    Usuario_Area.IdArea = CInt(lector.Item("IdArea"))
                    Usuario_Area.Estado = CBool(lector.Item("ua_Estado"))
                    Usuario_Area.NombreArea = CStr(lector.Item("ar_NombreLargo"))

                    Lista.Add(Usuario_Area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdUsuario(ByVal IdUsuario As Integer) As List(Of Entidades.Usuario_Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Usuario_AreaSelectxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdArea", 0)
        cmd.Parameters.AddWithValue("@ua_Estado", Nothing)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Usuario_Area)
                Do While lector.Read
                    Dim Usuario_Area As New Entidades.Usuario_Area
                    Usuario_Area.IdUsuario = CInt(lector.Item("IdUsuario"))
                    Usuario_Area.IdArea = CInt(lector.Item("IdArea"))
                    Usuario_Area.Estado = CBool(lector.Item("ua_Estado"))
                    Usuario_Area.NombreArea = CStr(lector.Item("ar_NombreLargo"))

                    Lista.Add(Usuario_Area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdArea(ByVal IdArea As Integer) As List(Of Entidades.Usuario_Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Usuario_AreaSelectxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", 0)
        cmd.Parameters.AddWithValue("@IdArea", IdArea)
        cmd.Parameters.AddWithValue("@ua_Estado", Nothing)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Usuario_Area)
                Do While lector.Read
                    Dim Usuario_Area As New Entidades.Usuario_Area
                    Usuario_Area.IdUsuario = CInt(lector.Item("IdUsuario"))
                    Usuario_Area.IdArea = CInt(lector.Item("IdArea"))
                    Usuario_Area.Estado = CBool(lector.Item("ua_Estado"))
                    Usuario_Area.NombreArea = CStr(lector.Item("ar_NombreLargo"))

                    Lista.Add(Usuario_Area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


End Class

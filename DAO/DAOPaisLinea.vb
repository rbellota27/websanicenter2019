﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOPaisLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.PaisLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PaisLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PaisLinea)
                Do While lector.Read
                    Dim objPaisLinea As New Entidades.PaisLinea
                    objPaisLinea.IdPais = LTrim(RTrim(CStr((lector.Item("IdPais")))))
                    objPaisLinea.NomPais = CStr(lector.Item("NomPais"))
                    objPaisLinea.IdLinea = CInt(lector.Item("IdLinea"))
                    objPaisLinea.IdTipoExistencia = CInt(lector.Item("IdTipoExistencia"))
                    objPaisLinea.Estado = CBool(lector.Item("pl_Estado"))
                    Lista.Add(objPaisLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivoxIdLineaIdTipoExistenciaV2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.PaisLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PaisLineaSelectAllActivoxIdLineaIdTipoExistenciaV2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PaisLinea)
                Do While lector.Read
                    Dim objPaisLinea As New Entidades.PaisLinea
                    objPaisLinea.IdPais = LTrim(RTrim(CStr((lector.Item("IdPais")))))
                    objPaisLinea.NomPais = CStr(lector.Item("NomPais"))
                    objPaisLinea.IdLinea = CInt(lector.Item("IdLinea"))
                    objPaisLinea.IdTipoExistencia = CInt(lector.Item("IdTipoExistencia"))
                    objPaisLinea.Estado = CBool(lector.Item("pl_Estado"))
                    Lista.Add(objPaisLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GrabaPaisLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal lpaislinea As List(Of Entidades.PaisLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To lpaislinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_PaisLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_PaisLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdPais", lpaislinea.Item(i).IdPais)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", lpaislinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@plEstado", lpaislinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@plOrden", lpaislinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class

﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 11-Noviembre-2009
'Hora    : 03:00 pm
'*************************************************
Imports System.Data.SqlClient

Public Class DAOTipoDocumentoAE
    Private objConexion As New DAO.Conexion

    Public Function SelectxIds(ByVal IdEmpresa As Integer, ByVal IdTipoDocumento As Integer, ByVal IdArea As Integer) As Entidades.TipoDocumentoAE
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoAESelectxIds", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@IdArea", IdArea)

        Dim lector As SqlDataReader

        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)


                Dim objTipoDocumentoAE As Entidades.TipoDocumentoAE = Nothing

                If lector.Read Then

                    objTipoDocumentoAE = New Entidades.TipoDocumentoAE

                    With objTipoDocumentoAE

                        .CantidadFilas = CInt(IIf(IsDBNull(lector("tdae_CantidadFilas")) = True, 0, lector("tdae_CantidadFilas")))
                        .MontoMaximoAfecto = CDec(IIf(IsDBNull(lector("tdae_MontoMaximoAfecto")) = True, 0, lector("tdae_MontoMaximoAfecto")))
                        .MontoMaximoNoAfecto = CDec(IIf(IsDBNull(lector("tdae_MontoMaximoNoAfecto")) = True, 0, lector("tdae_MontoMaximoNoAfecto")))

                    End With

                End If
               
                lector.Close()
                Return objTipoDocumentoAE
            End Using

        Catch ex As Exception

            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectValoresxParams(ByVal IdEmpresa As Integer, ByVal IdTipoDocumento As Integer, ByVal IdMonedaDestino As Integer) As Entidades.TipoDocumentoAE
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoAESelectValoresxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)

        Dim lector As SqlDataReader

        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)


                Dim objTipoDocumentoAE As Entidades.TipoDocumentoAE = Nothing

                If lector.Read Then

                    objTipoDocumentoAE = New Entidades.TipoDocumentoAE

                    With objTipoDocumentoAE

                        .CantidadFilas = CInt(IIf(IsDBNull(lector("tdae_CantidadFilas")) = True, 0, lector("tdae_CantidadFilas")))
                        .MontoMaximoAfecto = CDec(IIf(IsDBNull(lector("MontoMaximoAfecto")) = True, 0, lector("MontoMaximoAfecto")))
                        .MontoMaximoNoAfecto = CDec(IIf(IsDBNull(lector("MontoMaximoNoAfecto")) = True, 0, lector("MontoMaximoNoAfecto")))

                    End With

                End If

                lector.Close()
                Return objTipoDocumentoAE
            End Using

        Catch ex As Exception

            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCboxIdEmpresaxIdArea(ByVal IdEmpresa As Integer, ByVal IdArea As Integer) As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectCboxIdEmpresaxIdArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdArea", IdArea)

        Dim lector As SqlDataReader

        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                Dim lista As New List(Of Entidades.TipoDocumento)

                While lector.Read

                    Dim objTipoDocumento As New Entidades.TipoDocumento
                    objTipoDocumento.Id = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    objTipoDocumento.DescripcionCorto = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))

                    lista.Add(objTipoDocumento)

                End While

                lector.Close()

                Return lista
            End Using

        Catch ex As Exception

            Throw ex
        Finally

        End Try
    End Function

    Public Function InsertaTipoDocumentoAE(ByVal TipoDocumentoAE As Entidades.TipoDocumentoAE) As String
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(TipoDocumentoAE.IdEmpresa = Nothing, DBNull.Value, TipoDocumentoAE.IdEmpresa)
        ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(TipoDocumentoAE.IdArea = Nothing, DBNull.Value, TipoDocumentoAE.IdArea)
        ArrayParametros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(TipoDocumentoAE.IdTipoDocumento = Nothing, DBNull.Value, TipoDocumentoAE.IdTipoDocumento)
        ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(TipoDocumentoAE.IdMoneda = Nothing, DBNull.Value, TipoDocumentoAE.IdMoneda)
        ArrayParametros(4) = New SqlParameter("@tdae_MontoMaximoAfecto", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(TipoDocumentoAE.MontoMaximoAfecto = Nothing, DBNull.Value, TipoDocumentoAE.MontoMaximoAfecto)
        ArrayParametros(5) = New SqlParameter("@tdae_MontoMaximoNoAfecto", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(TipoDocumentoAE.MontoMaximoNoAfecto = Nothing, DBNull.Value, TipoDocumentoAE.MontoMaximoNoAfecto)
        ArrayParametros(6) = New SqlParameter("@tdae_CantidadFilas", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(TipoDocumentoAE.CantidadFilas = Nothing, DBNull.Value, TipoDocumentoAE.CantidadFilas)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand

        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_TipoDocumentoAEInset", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            Dim cad$ = cmd.ExecuteScalar().ToString()
            tr.Commit()
            Return cad
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function ActualizaTipoDocumentoAE(ByVal TipoDocumentoAE As Entidades.TipoDocumentoAE) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(TipoDocumentoAE.IdEmpresa = Nothing, DBNull.Value, TipoDocumentoAE.IdEmpresa)
        ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(TipoDocumentoAE.IdArea = Nothing, DBNull.Value, TipoDocumentoAE.IdArea)
        ArrayParametros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(TipoDocumentoAE.IdTipoDocumento = Nothing, DBNull.Value, TipoDocumentoAE.IdTipoDocumento)
        ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(TipoDocumentoAE.IdMoneda = Nothing, DBNull.Value, TipoDocumentoAE.IdMoneda)
        ArrayParametros(4) = New SqlParameter("@tdae_MontoMaximoAfecto", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(TipoDocumentoAE.MontoMaximoAfecto = Nothing, DBNull.Value, TipoDocumentoAE.MontoMaximoAfecto)
        ArrayParametros(5) = New SqlParameter("@tdae_MontoMaximoNoAfecto", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(TipoDocumentoAE.MontoMaximoNoAfecto = Nothing, DBNull.Value, TipoDocumentoAE.MontoMaximoNoAfecto)
        ArrayParametros(6) = New SqlParameter("@tdae_CantidadFilas", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(TipoDocumentoAE.CantidadFilas = Nothing, DBNull.Value, TipoDocumentoAE.CantidadFilas)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand
        Dim codigo As Integer = -1
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '*****************
            cmd = New SqlCommand("_TipoDocumentoAEUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)

            codigo = CInt(cmd.ExecuteScalar())

            tr.Commit()

            Return codigo

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function EliminarxEmpresaxAreaxTipoDocumento(ByVal IdEmpresa As Integer, ByVal IdArea As Integer, ByVal IdTipoDocumento As Integer) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(0).Value = IdEmpresa
        ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(1).Value = IdArea
        ArrayParametros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(2).Value = IdTipoDocumento

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand
        Dim codigo As Integer = -1
        Try

            Using cn
                cn.Open()
                tr = cn.BeginTransaction(IsolationLevel.Serializable)

                cmd = New SqlCommand("_TipoDocumentoAEDelete", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(ArrayParametros)

                codigo = CInt(cmd.ExecuteScalar())

                tr.Commit()

                Return codigo

            End Using
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function SelectAllTipoDocumentoAE() As List(Of Entidades.TipoDocumentoAE)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoAESelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoAE)
                Do While lector.Read
                    Dim TipoDocumentoAESelect As New Entidades.TipoDocumentoAE
                    TipoDocumentoAESelect.IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, "", lector.Item("IdPersona")))
                    TipoDocumentoAESelect.NomEmpresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "per_NComercial", lector.Item("per_NComercial")))
                    TipoDocumentoAESelect.IdArea = CInt(IIf(IsDBNull(lector.Item("IdArea")) = True, "", lector.Item("IdArea")))
                    TipoDocumentoAESelect.NomArea = CStr(IIf(IsDBNull(lector.Item("ar_NombreCorto")) = True, "", lector.Item("ar_NombreCorto")))
                    TipoDocumentoAESelect.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, "", lector.Item("IdTipoDocumento")))
                    TipoDocumentoAESelect.NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    TipoDocumentoAESelect.CantidadFilas = CInt(IIf(IsDBNull(lector.Item("tdae_CantidadFilas")) = True, 0, lector.Item("tdae_CantidadFilas")))
                    TipoDocumentoAESelect.MontoMaximoAfecto = CDec(IIf(IsDBNull(lector.Item("tdae_MontoMaximoAfecto")) = True, 0.0, lector.Item("tdae_MontoMaximoAfecto")))
                    TipoDocumentoAESelect.MontoMaximoNoAfecto = CDec(IIf(IsDBNull(lector.Item("tdae_MontoMaximoNoAfecto")) = True, 0.0, lector.Item("tdae_MontoMaximoNoAfecto")))
                    TipoDocumentoAESelect.NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    TipoDocumentoAESelect.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, "", lector.Item("IdMoneda")))

                    Lista.Add(TipoDocumentoAESelect)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function TipoDocumentoAEBuscarxIdArea(ByVal IdArea As Integer, ByVal IdEmpresa As Integer) As List(Of Entidades.TipoDocumentoAE)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoAEBuscarxIdArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdArea", IdArea)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoAE)
                Do While lector.Read
                    Dim TipoDocumentoAESelect As New Entidades.TipoDocumentoAE
                    TipoDocumentoAESelect.IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    TipoDocumentoAESelect.NomEmpresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    TipoDocumentoAESelect.IdArea = CInt(IIf(IsDBNull(lector.Item("IdArea")) = True, 0, lector.Item("IdArea")))
                    TipoDocumentoAESelect.NomArea = CStr(IIf(IsDBNull(lector.Item("ar_NombreCorto")) = True, "", lector.Item("ar_NombreCorto")))
                    TipoDocumentoAESelect.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    TipoDocumentoAESelect.NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    TipoDocumentoAESelect.CantidadFilas = CInt(IIf(IsDBNull(lector.Item("tdae_CantidadFilas")) = True, 0, lector.Item("tdae_CantidadFilas")))
                    TipoDocumentoAESelect.MontoMaximoAfecto = CDec(IIf(IsDBNull(lector.Item("tdae_MontoMaximoAfecto")) = True, 0, lector.Item("tdae_MontoMaximoAfecto")))
                    TipoDocumentoAESelect.MontoMaximoNoAfecto = CDec(IIf(IsDBNull(lector.Item("tdae_MontoMaximoNoAfecto")) = True, 0, lector.Item("tdae_MontoMaximoNoAfecto")))
                    TipoDocumentoAESelect.NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    TipoDocumentoAESelect.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))

                    Lista.Add(TipoDocumentoAESelect)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOArea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

#Region "Marzo del 2010"

    Public Function Area_Select_TiendaArea_UsuarioArea(ByVal idtienda As Integer, ByVal idusuario As Integer) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Area_Select_TiendaArea_UsuarioArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idTienda", idtienda)
        cmd.Parameters.AddWithValue("@Idusuario", idusuario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim obj As New Entidades.Area
                    obj.IdCompuesto = CStr(IIf(IsDBNull(lector("idCompuesto")) = True, 0, lector("idCompuesto")))
                    obj.DescripcionCorta = CStr(IIf(IsDBNull(lector("ar_NombreCorto")) = True, "", lector("ar_NombreCorto")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

#End Region

    Public Function SelectCbo() As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim obj As New Entidades.Area

                    obj.Id = CInt(IIf(IsDBNull(lector("IdArea")) = True, 0, lector("IdArea")))
                    obj.DescripcionCorta = CStr(IIf(IsDBNull(lector("ar_NombreCorto")) = True, "", lector("ar_NombreCorto")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaArea(ByVal area As Entidades.Area) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@ar_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(area.Descripcion = Nothing, DBNull.Value, area.Descripcion)
        ArrayParametros(1) = New SqlParameter("@ar_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(area.DescripcionCorta = Nothing, DBNull.Value, area.DescripcionCorta)
        ArrayParametros(2) = New SqlParameter("@ar_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = IIf(area.Estado = Nothing, DBNull.Value, area.Estado)
        ArrayParametros(3) = New SqlParameter("@idcentrocosto", SqlDbType.VarChar)
        ArrayParametros(3).Value = IIf(area.IdCentroCosto = "0000000000", DBNull.Value, area.IdCentroCosto)
        Return HDAO.Insert(cn, "_AreaInsert", ArrayParametros)
    End Function
    Public Function ActualizaArea(ByVal area As Entidades.Area) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(0).Value = area.Id
        ArrayParametros(1) = New SqlParameter("@ar_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(area.Descripcion = Nothing, DBNull.Value, area.Descripcion)
        ArrayParametros(2) = New SqlParameter("@ar_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(area.DescripcionCorta = Nothing, DBNull.Value, area.DescripcionCorta)
        ArrayParametros(3) = New SqlParameter("@ar_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = IIf(area.Estado = Nothing, DBNull.Value, area.Estado)
        ArrayParametros(4) = New SqlParameter("@idcentrocosto", SqlDbType.VarChar)
        ArrayParametros(4).Value = IIf(area.IdCentroCosto = "0000000000", DBNull.Value, area.IdCentroCosto)

        Return HDAO.Update(cn, "_AreaUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim area As New Entidades.Area
                    area.Id = CInt(IIf(IsDBNull(lector.Item("IdArea")) = True, 0, lector.Item("IdArea")))
                    area.Descripcion = CStr(IIf(IsDBNull(lector.Item("ar_NombreLargo")) = True, "", lector.Item("ar_NombreLargo")))
                    area.DescripcionCorta = CStr(lector.Item("ar_NombreCorto"))
                    area.Estado = CStr(lector.Item("ar_Estado"))
                    area.IdCentroCosto = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "---", lector("IdCentroCosto")))

                    Lista.Add(area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim area As New Entidades.Area
                    area.Id = CInt(lector.Item("IdArea"))
                    area.Descripcion = CStr(lector.Item("ar_NombreLargo"))
                    area.DescripcionCorta = CStr(lector.Item("ar_NombreCorto"))
                    area.Estado = CStr(lector.Item("ar_Estado"))
                    area.IdCentroCosto = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "---", lector("IdCentroCosto")))
                    Lista.Add(area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim area As New Entidades.Area
                    area.Id = CInt(lector.Item("IdArea"))
                    area.Descripcion = CStr(lector.Item("ar_NombreLargo"))
                    area.DescripcionCorta = CStr(lector.Item("ar_NombreCorto"))
                    area.Estado = CStr(lector.Item("ar_Estado"))
                    area.IdCentroCosto = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "---", lector("IdCentroCosto")))
                    Lista.Add(area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectAllXNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim area As New Entidades.Area
                    area.Id = CInt(lector.Item("IdArea"))
                    area.Descripcion = CStr(lector.Item("ar_NombreLargo"))
                    area.DescripcionCorta = CStr(lector.Item("ar_NombreCorto"))
                    area.Estado = CStr(lector.Item("ar_Estado"))
                    area.IdCentroCosto = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "---", lector("IdCentroCosto")))
                    Lista.Add(area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectXNombreXActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim area As New Entidades.Area
                    area.Id = CInt(lector.Item("IdArea"))
                    area.Descripcion = CStr(lector.Item("ar_NombreLargo"))
                    area.DescripcionCorta = CStr(lector.Item("ar_NombreCorto"))
                    area.Estado = CStr(lector.Item("ar_Estado"))
                    area.IdCentroCosto = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "---", lector("IdCentroCosto")))
                    Lista.Add(area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectXNombreXInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim area As New Entidades.Area
                    area.Id = CInt(lector.Item("IdArea"))
                    area.Descripcion = CStr(lector.Item("ar_NombreLargo"))
                    area.DescripcionCorta = CStr(lector.Item("ar_NombreCorto"))
                    area.Estado = CStr(lector.Item("ar_Estado"))
                    area.IdCentroCosto = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "---", lector("IdCentroCosto")))
                    Lista.Add(area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal idArea As Integer) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectXId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdArea", idArea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim area As New Entidades.Area
                    area.Id = CInt(lector.Item("IdArea"))
                    area.Descripcion = CStr(lector.Item("ar_NombreLargo"))
                    area.DescripcionCorta = CStr(lector.Item("ar_NombreCorto"))
                    area.Estado = CStr(lector.Item("ar_Estado"))
                    area.IdCentroCosto = CStr(IIf(IsDBNull(lector("idcentrocosto")), "---", lector("idcentrocosto")))
                    Lista.Add(area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectCboxIdEmpresa", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim objArea As New Entidades.Area
                    objArea.Id = CInt(IIf(IsDBNull(lector.Item("IdArea")) = True, 0, lector.Item("IdArea")))
                    objArea.DescripcionCorta = CStr(IIf(IsDBNull(lector.Item("ar_NombreCorto")) = True, "", lector.Item("ar_NombreCorto")))
                    objArea.DescripcionCorta = CStr(IIf(IsDBNull(lector.Item("ar_NombreCorto")) = True, "", lector.Item("ar_NombreCorto")))
                    objArea.EstadoAE = CBool(IIf(IsDBNull(lector.Item("ae_Estado")) = True, 1, lector.Item("ae_Estado")))
                    Lista.Add(objArea)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboxIdTienda(ByVal IdTienda As Integer) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectCboxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim objArea As New Entidades.Area
                    objArea.Id = CInt(IIf(IsDBNull(lector.Item("IdArea")) = True, 0, lector.Item("IdArea")))
                    objArea.DescripcionCorta = CStr(IIf(IsDBNull(lector.Item("ar_NombreCorto")) = True, "", lector.Item("ar_NombreCorto")))
                    objArea.EstadoAE = CBool(IIf(IsDBNull(lector.Item("ta_Estado")) = True, "", lector.Item("ta_Estado")))
                    objArea.IdCentroCosto = CStr(IIf(IsDBNull(lector.Item("IdCentroCosto")) = True, "", lector.Item("IdCentroCosto")))
                    Lista.Add(objArea)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombreActivoInactivo(ByVal nombre As String) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectAllXNombreActivoInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim area As New Entidades.Area
                    area.Id = CInt(lector.Item("IdArea"))
                    area.Descripcion = CStr(lector.Item("ar_NombreLargo"))
                    area.DescripcionCorta = CStr(lector.Item("ar_NombreCorto"))
                    area.Estado = CStr(lector.Item("ar_Estado"))
                    Lista.Add(area)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectIdcentroCostoxIdarea(ByVal idarea As Integer) As String
        Dim idcentrocosto As String = String.Empty
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarIdcentroCostoxIdarea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idarea", idarea)
        Try
            cn.Open()
            idcentrocosto = cmd.ExecuteScalar.ToString
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return idcentrocosto
    End Function
    Public Function AreaSelectxNombre(ByVal IdArea As Integer) As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AreaSelectxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdArea", IdArea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim objArea As New Entidades.Area
                    objArea.Id = CInt(IIf(IsDBNull(lector.Item("IdArea")) = True, 0, lector.Item("IdArea")))
                    objArea.DescripcionCorta = CStr(IIf(IsDBNull(lector.Item("ar_NombreCorto")) = True, "---", lector.Item("ar_NombreCorto")))
                    objArea.Estado = CStr(IIf(IsDBNull(lector.Item("ar_Estado")) = True, "", lector.Item("ar_Estado")))
                    Lista.Add(objArea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTiendaEmpresaAlmacen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTiendaEmpresaAlmacen(ByVal tiendaempresaalmacen As Entidades.TiendaEmpresaAlmacen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(0).Value = tiendaempresaalmacen.IdAlmacen
        ArrayParametros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(1).Value = tiendaempresaalmacen.IdTienda
        ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(2).Value = tiendaempresaalmacen.IdEmpresa
        ArrayParametros(3) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(3).Value = tiendaempresaalmacen.IdUsuario
        Return HDAO.Insert(cn, "_TiendaEmpresaAlmacenInsert", ArrayParametros)
    End Function
    Public Function ActualizaTiendaEmpresaAlmacen(ByVal tiendaempresaalmacen As Entidades.TiendaEmpresaAlmacen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(0).Value = tiendaempresaalmacen.IdAlmacen
        ArrayParametros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(1).Value = tiendaempresaalmacen.IdTienda
        ArrayParametros(2) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(2).Value = tiendaempresaalmacen.IdEmpresa
        ArrayParametros(3) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(3).Value = tiendaempresaalmacen.IdUsuario
        Return HDAO.Update(cn, "_TiendaEmpresaAlmacenUpdate", ArrayParametros)
    End Function
    'Public Function SelectAlmacenxIdEmpresa(ByVal idempresa As Integer) As List(Of Entidades.Almacen)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_TiendaEmpresaAlmacenSelectAlmacenxIdEmpresa", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.Almacen)
    '            Do While lector.Read
    '                Dim obj As New Entidades.Almacen
    '                obj.Nombre = CStr(lector.Item("alm_Nombre"))
    '                obj.IdAlmacen = CInt(lector.Item("IdAlmacen"))
    '                Lista.Add(obj)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function




End Class
